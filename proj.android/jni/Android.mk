LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp

HELLOCPP_FILES  := $(wildcard $(LOCAL_PATH)/hellocpp/*.cpp)
HELLOCPP_FILES  := $(HELLOCPP_FILES:$(LOCAL_PATH)/%=%)

CLASSES_FILES_CPP   := $(wildcard $(LOCAL_PATH)/../../Classes/*.cpp)
CLASSES_FILES_CPP   := $(CLASSES_FILES_CPP:$(LOCAL_PATH)/%=%)

CLASSES_FILES   := $(wildcard $(LOCAL_PATH)/../../Classes/*.c)
CLASSES_FILES   := $(CLASSES_FILES:$(LOCAL_PATH)/%=%)

CLASSES_SUB_FILES_CPP := $(wildcard $(LOCAL_PATH)/../../Classes/**/*.cpp)
CLASSES_SUB_FILES_CPP := $(CLASSES_SUB_FILES_CPP:$(LOCAL_PATH)/%=%)

CLASSES_SUB_FILES := $(wildcard $(LOCAL_PATH)/../../Classes/**/*.c)
CLASSES_SUB_FILES := $(CLASSES_SUB_FILES:$(LOCAL_PATH)/%=%)

LOCAL_SRC_FILES := $(HELLOCPP_FILES)
LOCAL_SRC_FILES += $(CLASSES_FILES_CPP)
LOCAL_SRC_FILES += $(CLASSES_FILES)
LOCAL_SRC_FILES += $(CLASSES_SUB_FILES_CPP)
LOCAL_SRC_FILES += $(CLASSES_SUB_FILES)

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes \
	$(LOCAL_PATH)/../../cocos2d/cocos \
	$(LOCAL_PATH)/../../cocos2d/cocos/ui \
	$(LOCAL_PATH)/../../cocos2d/cocos/editor-support \
	$(LOCAL_PATH)/../../Classes/include \

LOCAL_LDLIBS  := -landroid $(LOCAL_PATH)/../../lib/libffmpeg.so

LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static
LOCAL_WHOLE_STATIC_LIBRARIES += box2d_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_ui_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_extension_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosbuilder_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocostudio_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_network_static

include $(BUILD_SHARED_LIBRARY)

$(call import-module,2d)
$(call import-module,audio/android)
$(call import-module,Box2D)
$(call import-module,ui)
$(call import-module,extensions)
$(call import-module,editor-support/cocostudio)
$(call import-module,editor-support/cocosbuilder)
$(call import-module,network)