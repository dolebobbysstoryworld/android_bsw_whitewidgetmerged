/****************************************************************************
Copyright (c) 2008-2010 Ricardo Quesada
Copyright (c) 2010-2012 cocos2d-x.org
Copyright (c) 2011      Zynga Inc.
Copyright (c) 2013-2014 Chukong Technologies Inc.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package com.dole.bobbysstoryworld;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.SystemClock;
import android.provider.Settings.Secure;
import android.util.Log;
import android.util.Xml;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.Toast;

import com.android.vending.expansion.zipfile.APKExpansionSupport;
import com.android.vending.expansion.zipfile.ZipResourceFile;
import com.android.vending.expansion.zipfile.ZipResourceFile.ZipEntryRO;
import com.dole.bobbysstoryworld.server.GetAppversion;
import com.dole.bobbysstoryworld.server.ServerTask;
import com.dole.bobbysstoryworld.server.ServerTask.IServerResponseCallback;
import com.dole.bobbysstoryworld.ui.CustomDialog;
import com.dole.bobbysstoryworld.ui.CustomDialog.CustomDialogButtonClickListener;
import com.dole.bobbysstoryworld.ui.FontTextHelper;
import com.dole.bobbysstoryworld.ui.FontTextView;
import com.dole.bobbysstoryworld.ui.LoadingImageView;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.vending.expansion.downloader.DownloadProgressInfo;
import com.google.android.vending.expansion.downloader.DownloaderClientMarshaller;
import com.google.android.vending.expansion.downloader.DownloaderServiceMarshaller;
import com.google.android.vending.expansion.downloader.Helpers;
import com.google.android.vending.expansion.downloader.IDownloaderClient;
import com.google.android.vending.expansion.downloader.IDownloaderService;
import com.google.android.vending.expansion.downloader.IStub;
import com.google.android.vending.licensing.AESObfuscator;
import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.ServerManagedPolicy;

public class SplashActivity extends Activity implements IDownloaderClient, IServerResponseCallback {
	private static final String TAG = "BobbysStoryWorld";

	private static final String INFO_FILE = "info.xml";

    private static final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnwcpIugRvwy32Jt/vEBpJ3D1+jztNYRkBPicgv3fBgTTr8ZKsmSODOEV8mB//ROoSdspjnDuvlMzzb8RfIXEqAfbknhNrLjmGS2kPGM/HctDgYdHiDeqCufEF7JnbMsXsgdsAZy+IarRJ5PW2B7J9E8NGv+QXhLOqhOO1cFpjLgBESJ2ncgj9Q7j+p9kYG4q9HebOLbHw5XSEe/r3Mao6thKNzM9G8b6xZbZiVOWC25YvFrJuAz9UTGW+3tF3jrhFlfR+KbunZq1QHRWPYqkCD56hnZ0wql8N80KwEwhBR0pTYjYMmOMlqDm1HKjOCDP831P6DeGPev9qOJyaIoDnQIDAQAB";

    // Generate your own 20 random bytes, and put them here.
    private static final byte[] SALT = new byte[] {
        -46, 65, 30, -128, -103, -57, 74, -64, 51, 88, -95, -45, 77, -117, -36, -113, -11, 32, -64,
        89
    };

	private final int HANDLE_PROGRESS = 0x100;
	private final int UPDATE_PROGRESS = 0x101;
	private final int DONE_PROGRESS   = 0x102;
	private final int FINISH_PROGRESS = 0x103;
	private final int START_CHECK     = 0x104;
	private final int VERSION_CHECK   = 0x105;
	private final int DOWNLOAD_FAIL   = 0x106;

	private int LOADING_MAX = 100;
	private int PROGRESS_UPDATE_PERIOD = 500;

	private LoadingImageView mLoading = null;
	private ProgressHandler mHandler = new ProgressHandler();
	private boolean mUserCancel = false;

	private ServerManagedPolicy mPolicy;
    private LicenseCheckerCallback mLicenseCheckerCallback;
    private LicenseChecker mChecker;
    private IDownloaderService mRemoteService;
    private IStub mDownloaderClientStub;
    private boolean mCancelValidation;
    private boolean mNeedDownload = false;
    private boolean mDownloadFinished = false;

    static private final float SMOOTHING_FACTOR = 0.005f;

    private XAPKFile[] mAPKS;

	private UnzipRunnable mUnzipRunnable;

	private FrameLayout mContainer;

	private class ProgressHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			if(mUserCancel)
				return;

			switch(msg.what) {
				case HANDLE_PROGRESS:
					if(mUnzipRunnable != null)
						mUnzipRunnable.needUpdate();
					return;

				case UPDATE_PROGRESS:
					if(mLoading != null) {
						if(mNeedDownload) {
							if(mDownloadFinished)
								mLoading.progress(50 + msg.arg1 / 2);
							else
								mLoading.progress(msg.arg1 / 2);
						} else {
							mLoading.progress(msg.arg1);
						}
						sendEmptyMessageDelayed(HANDLE_PROGRESS, PROGRESS_UPDATE_PERIOD);
					}
					return;

				case DONE_PROGRESS:
					if(mLoading != null) {
						mLoading.progress(LOADING_MAX);
						sendEmptyMessageDelayed(FINISH_PROGRESS, PROGRESS_UPDATE_PERIOD);
					}
					return;

				case FINISH_PROGRESS:
					launch();
					return;

				case START_CHECK:
					startCheck();
					return;

				case VERSION_CHECK:
					versionCheck();
					return;

				case DOWNLOAD_FAIL:
					if (null != mDownloaderClientStub) {
			            mDownloaderClientStub.disconnect(SplashActivity.this);
			        }
					showFailDownloadWarning();
					return;
			}
			super.handleMessage(msg);
		}
	}

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mContainer = new FrameLayout(this);
		mContainer.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		mContainer.setBackgroundColor(Color.WHITE);

		ImageView logo = new ImageView(this);
		FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
				FrameLayout.LayoutParams.MATCH_PARENT);
		lp.gravity = Gravity.CENTER;
		logo.setLayoutParams(lp);
		logo.setBackgroundResource(R.drawable.splash_logo);
		logo.setScaleType(ScaleType.CENTER);

		mContainer.addView(logo);

		setContentView(mContainer);
	}

    @Override
	protected void onResume() {
		super.onResume();

		if(Constants.DEBUG)
			mHandler.sendEmptyMessageDelayed(START_CHECK, 100);
		else
		mHandler.sendEmptyMessageDelayed(VERSION_CHECK, 100);
	}
	
	private void versionCheck() {
		if(!Constants.DEBUG) {
			Cocos2dxActivity.setDebugMode(Constants.DEBUG);
			Cocos2dxActivity.setExtensionFileName(APKExpansionSupport.getAPKExpansionPath(this));
		}
		if(Util.isInternetAvailable(this)) {
			HashMap<String, String> request = new HashMap<String, String>();
			request.put(Constants.TAG_SERVICE_CODE, Constants.SERVICE_CODE);
			request.put(Constants.TAG_MOBILE_OS, Constants.OS_STRING);
			request.put(Constants.TAG_CLIENT_IP, Util.getIp(this));
			
			GetAppversion versionCheck = new GetAppversion(this, request);
			versionCheck.setCallback(this);
			Model.runOnWorkerThread(versionCheck);
		} else {
			postDownload();
		}
	}
	
	@Override
	public void onSuccess(ServerTask parser, Map<String, String> result) {
		if(parser instanceof GetAppversion) {
			try {
				String packageName = getPackageName();
				PackageManager pm = getPackageManager();
				PackageInfo packageInfo = pm.getPackageInfo(packageName, 0);
				String appVersion = packageInfo.versionName;
				String serverVersion = result.get(Constants.TAG_MAJOR_VERSION);
				if(serverVersion == null)
					serverVersion = "1.0.0";
				if(Util.isCriticalVersionUpdateRequired(appVersion, serverVersion)) {
					showVersionWarning();
				}
				else {
					mHandler.sendEmptyMessage(START_CHECK);
				}
			} catch(NameNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void onFailed(ServerTask parser, Map<String, String> result,
			int returnCode) {
		if(parser instanceof GetAppversion) {
			if(returnCode == ServerTask.RESULT_ERROR_NETWORK)
				mHandler.sendEmptyMessage(START_CHECK);
			else
				Toast.makeText(this, Util.switchErrorCode(returnCode, getResources()), Toast.LENGTH_LONG).show();
			this.finish();
		}
	}

	@Override
    protected void onStart() {
    	if (null != mDownloaderClientStub) {
            mDownloaderClientStub.connect(this);
        }
        super.onStart();
        EasyTracker.getInstance().activityStart(this);
    }

    @Override
    protected void onStop() {
    	if (null != mDownloaderClientStub) {
            mDownloaderClientStub.disconnect(this);
        }
        super.onStop();
        EasyTracker.getInstance().activityStop(this);
    }

    @Override
    protected void onDestroy() {
        mCancelValidation = true;

        super.onDestroy();

        if(mChecker != null)
        mChecker.onDestroy();
    }

    @Override
    public void onServiceConnected(Messenger m) {
        mRemoteService = DownloaderServiceMarshaller.CreateProxy(m);
        mRemoteService.onClientUpdated(mDownloaderClientStub.getMessenger());
    }

    @Override
    public void onDownloadStateChanged(int newState) {
        boolean paused;
        switch (newState) {
            case IDownloaderClient.STATE_IDLE:
                // STATE_IDLE means the service is listening, so it's
                // safe to start making calls via mRemoteService.
                paused = false;
                break;
            case IDownloaderClient.STATE_CONNECTING:
            case IDownloaderClient.STATE_FETCHING_URL:
                paused = false;
                break;
            case IDownloaderClient.STATE_DOWNLOADING:
            	mHandler.removeMessages(DOWNLOAD_FAIL);
                paused = false;
                break;
                
            case IDownloaderClient.STATE_PAUSED_NETWORK_UNAVAILABLE:
            	if(!mHandler.hasMessages(DOWNLOAD_FAIL))
            		mHandler.sendEmptyMessageDelayed(DOWNLOAD_FAIL, 30000);
            	paused = true;
            	break;

            case IDownloaderClient.STATE_FAILED_CANCELED:
            case IDownloaderClient.STATE_FAILED:
            case IDownloaderClient.STATE_FAILED_FETCHING_URL:
            case IDownloaderClient.STATE_FAILED_UNLICENSED:
            	showFailDownloadWarning();
                paused = true;
                break;
            case IDownloaderClient.STATE_PAUSED_NEED_CELLULAR_PERMISSION:
            case IDownloaderClient.STATE_PAUSED_WIFI_DISABLED_NEED_CELLULAR_PERMISSION:
                paused = true;
                break;

            case IDownloaderClient.STATE_PAUSED_BY_REQUEST:
                paused = true;
                break;
            case IDownloaderClient.STATE_PAUSED_ROAMING:
            case IDownloaderClient.STATE_PAUSED_SDCARD_UNAVAILABLE:
                paused = true;
                break;
            case IDownloaderClient.STATE_COMPLETED:
                paused = false;
                if(!mDownloadFinished) {
                    validateXAPKZipFiles();
                    String file = APKExpansionSupport.getAPKExpansionPath(this) + File.separator + mPolicy.getMainExpansionName();
        			BobbysStoryWorldPreference.putString(Constants.PREF_EXPANSION_MAIN_FILE, file);
                    runOnUiThread(new Runnable() {
                    	@Override
                    	public void run() {
                            postDownload();
                    	}
                    });
                }
                mDownloadFinished = true;
                return;
            default:
                paused = true;
        }
    }

    @Override
    public void onDownloadProgress(DownloadProgressInfo progress) {
    	Message msg = Message.obtain(mHandler, UPDATE_PROGRESS);
		msg.arg1 = (int)(((float)progress.mOverallProgress / (float)progress.mOverallTotal) * 100);
		mHandler.sendMessage(msg);
    }

	@Override
	public void onBackPressed() {
		if(mUnzipRunnable != null)
			mUnzipRunnable.cancel();
		mUserCancel = true;

		super.onBackPressed();
	}

	private void startCheck() {
		if(Constants.DEBUG) {
			final long freeSpace = getCacheDir().getFreeSpace();
            if(freeSpace < (500 * 1024 * 1024)) { // temp code for debug mode
            	showStorageWarning();
            	return;
            }

			checkBGM();
			launch();
		} else {
	        // Try to use more data here. ANDROID_ID is a single point of attack.
	        String deviceId = Secure.getString(getContentResolver(), Secure.ANDROID_ID);
	        mLicenseCheckerCallback = new MyLicenseCheckerCallback();
	        // Construct the LicenseChecker with a policy.
	        mPolicy = new ServerManagedPolicy(this, new AESObfuscator(SALT, getPackageName(), deviceId));
    		mChecker = new LicenseChecker(this, mPolicy, BASE64_PUBLIC_KEY);
    		if(Constants.USE_LOCAL_OBB)
    			postDownload();
    		else
    			licenseCheck();
		}
	}

    private void licenseCheck() {
        mChecker.checkAccess(mLicenseCheckerCallback);
    }

    private void initializeDownload() {
        mDownloaderClientStub = DownloaderClientMarshaller.CreateStub(this, ExpansionDownloaderService.class);
        if (null != mDownloaderClientStub) {
            mDownloaderClientStub.connect(this);
        }
    }

	private void checkBGM() {
		final String expPath = APKExpansionSupport.getAPKExpansionPath(this);
		File bgmPath = new File(expPath + "/common");
		if(!bgmPath.exists())
			bgmPath.mkdirs();

		File preloadPath = new File(expPath + "/common/preload_sounds");
		if(!preloadPath.exists())
			preloadPath.mkdirs();

		File preloadEnPath = new File(expPath + "/common/preload_sounds/en");
		if(!preloadEnPath.exists())
			preloadEnPath.mkdirs();

		File preloadJaPath = new File(expPath + "/common/preload_sounds/ja");
		if(!preloadJaPath.exists())
			preloadJaPath.mkdirs();

		File preloadKorPath = new File(expPath + "/common/preload_sounds/kor");
		if(!preloadKorPath.exists())
			preloadKorPath.mkdirs();

		Resources res = getResources();
		AssetManager manager = res.getAssets();
		String[] list = res.getStringArray(R.array.bgm_list);
		File bgmFile = null;
		for(String item: list) {
			bgmFile = new File(new String(bgmPath.toString() + File.separator + item));
			if(!bgmFile.exists()) {
				try {
					bgmFile.createNewFile();
					byte[] buffer = new byte[4096];
					InputStream in = manager.open("common/" + item);
					FileOutputStream out = new FileOutputStream(bgmFile);
					BufferedOutputStream bout = new BufferedOutputStream(out, buffer.length);
					int c = 0;
					while((c = in.read(buffer, 0, buffer.length)) != -1) {
						bout.write(buffer, 0, c);
					}
					out.flush();
					bout.flush();

					out.close();
					bout.close();
					in.close();
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
    }

	private void initViews() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				LayoutInflater li = (LayoutInflater)getSystemService(Service.LAYOUT_INFLATER_SERVICE);
				ViewGroup frame = (ViewGroup)li.inflate(R.layout.splash, null);

				mContainer.addView(frame);

				mLoading = (LoadingImageView)frame.findViewById(R.id.splash_progress_image);
				mLoading.setLoadingImage(R.drawable.splash_loading, R.drawable.splash_loading_02, R.dimen.splash_progress_size);
				mLoading.setMax(LOADING_MAX);
				mLoading.startLoading();

				FontTextView text = (FontTextView)frame.findViewById(R.id.splash_progress_description);
				FontTextHelper.setFont(SplashActivity.this, text, FontTextHelper.sEnglishBoldFontPath);
			}
		});
	}

	private void mountObb() {
		if(needDownload()) {
			if(mPolicy != null) {
				final long freeSpace = getCacheDir().getFreeSpace();
	            final long fileSize = mPolicy.getMainExpansionSize();
	            if(freeSpace < (fileSize + fileSize * 1.2f + 100 * 1024 * 1024)) {
	            	showStorageWarning();
	            	return;
	            }
			}
			
			if(!Util.isWifiAvailable(this)) {
				showUseWifiWarning();
				return;
			}
			startDownload();
		}
		else {
			validateXAPKZipFiles();
			postDownload();
		}
    }
	
	private void startDownload() {
		mNeedDownload = true;
		initViews();

        try {
            Intent launchIntent = SplashActivity.this.getIntent();
            Intent intentToLaunchThisActivityFromNotification = new Intent(SplashActivity.this, SplashActivity.this.getClass());
            intentToLaunchThisActivityFromNotification.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                    Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intentToLaunchThisActivityFromNotification.setAction(launchIntent.getAction());

            if (launchIntent.getCategories() != null) {
                for (String category : launchIntent.getCategories()) {
                    intentToLaunchThisActivityFromNotification.addCategory(category);
                }
            }

            PendingIntent pendingIntent = PendingIntent.getActivity(
                    SplashActivity.this,
                    0, intentToLaunchThisActivityFromNotification,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            // Request to start the download
            int startResult = DownloaderClientMarshaller.startDownloadServiceIfRequired(this,
            		pendingIntent, ExpansionDownloaderService.class);

            if (startResult != DownloaderClientMarshaller.NO_DOWNLOAD_REQUIRED) {
    			initializeDownload();
                return;
            }
        } catch (NameNotFoundException e) {
            Log.e(TAG, "Cannot find own package! MAYDAY!");
            e.printStackTrace();
        }
        
		validateXAPKZipFiles();
		postDownload();
	}

	private void postDownload() {
		if(needExtract()) {
			if(mPolicy != null) {
	            final long freeSpace = getCacheDir().getFreeSpace();
	            final long fileSize = mPolicy.getMainExpansionSize();
	            if(freeSpace < (fileSize * 1.2f + 100 * 1024 * 1024)) {
					showStorageWarning();
					return;
				}
			}

			if(mLoading == null && !mNeedDownload) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						initViews();
					}
				});
			}
			mHandler.sendEmptyMessage(UPDATE_PROGRESS);
			mUnzipRunnable = new UnzipRunnable();
			mUnzipRunnable.needUpdate();
			new Thread(mUnzipRunnable).start();
		} else {
			launch();
		}
    }

	private boolean needDownload() {
		try {
			final String packageName = getPackageName();
			PackageManager pm = getPackageManager();
			PackageInfo packageInfo = pm.getPackageInfo(packageName, 0);

			mAPKS = new XAPKFile[] { new XAPKFile(true, packageInfo.versionCode, mPolicy.getMainExpansionSize()) };
//			String[] files = APKExpansionSupport.getAPKExpansionFiles(this, packageInfo.versionCode, 0);
			String file = getMainFileName();
//			if(Constants.USE_LOCAL_OBB)
//				file = APKExpansionSupport.getAPKExpansionFiles(this, packageInfo.versionCode, 0)[0];
//			else
//				file = APKExpansionSupport.getAPKExpansionPath(this) + File.separator + mPolicy.getMainExpansionName();
			String lastFile = BobbysStoryWorldPreference.getString(Constants.PREF_EXPANSION_MAIN_FILE, "");

//			if(files.length <= 0)
//				return true;
			if(!lastFile.equals(file))
				return true;

			// currently main supports only
//			File test = new File(files[0]);
			File test = new File(file);
			if(!test.exists())
				return true;
    	} catch(NameNotFoundException e) {
    		e.printStackTrace();
    	}
		return false;
	}

	private boolean needExtract() {
		String prefix = APKExpansionSupport.getAPKExpansionPath(this);

		try {
			final String packageName = getPackageName();
			PackageManager pm = getPackageManager();
			PackageInfo packageInfo = pm.getPackageInfo(packageName, 0);
//			int savedVersion = BobbysStoryWorldPreference.getInt(Constants.PREF_EXPANSION, 0);
//			if(savedVersion < packageInfo.versionCode) {
//				android.util.Log.d(TAG, "First installation of Bobbys Story World expansion package (saved : "
//								+ savedVersion + ", current : " + packageInfo.versionCode + ")");
//				return true;
//			}

//			String[] files = APKExpansionSupport.getAPKExpansionFiles(this, packageInfo.versionCode, 0);
			String file = getMainFileName();
//			if(Constants.USE_LOCAL_OBB)
//				file = APKExpansionSupport.getAPKExpansionFiles(this, packageInfo.versionCode, 0)[0];
//			else
//				file = BobbysStoryWorldPreference.getString(Constants.PREF_EXPANSION_MAIN_FILE, "");
//			for(int i = 0 ; i < files.length ; i++) {
//				String path = file.substring(0, file.lastIndexOf(File.separator));
//				String obbName = file.substring(prefix.length() + 1).substring(0, path.indexOf(packageInfo.packageName));
//				if(obbName.startsWith("main")) {
					final long mainHash = BobbysStoryWorldPreference.getLong(Constants.PREF_EXPANSION_HASH_MAIN, -1L);
					final long currentHash = new File(file).lastModified();
					if(mainHash != currentHash) {
						android.util.Log.d(TAG, "Bobbys Story World expansion package file has changed");
						return true;
					}
//				} else {
//					// TODO: patch handling
//				}
//    		}
    	} catch(NameNotFoundException e) {
    		e.printStackTrace();
    	}

		File info = new File(prefix + File.separator + INFO_FILE);
		if(!info.exists()) {
			android.util.Log.d(TAG, "Bobbys Story World expansion package file list check failed");
			return true;
		}

		HashMap<String, Long> infoMap = new HashMap<String, Long>();
		HashMap<String, Long> fileMap = new HashMap<String, Long>();
		TreeMap<String, Long> infoTree = null;
		TreeMap<String, Long> fileTree = null;

		try {
			FileInputStream fis = new FileInputStream(info);
			XmlPullParser parser = Xml.newPullParser();
			parser.setInput(fis, "UTF-8");
			parser.nextTag();

			long fileSize = 0L;
			while(parser.next() != XmlPullParser.END_DOCUMENT) {
				if(parser.getEventType() != XmlPullParser.START_TAG)
					continue;

				fileSize = 0L;
				if(parser.getName().equals("file"))
					fileSize = Long.parseLong(parser.getAttributeValue(0));

				if(parser.next() == XmlPullParser.TEXT)
					infoMap.put(parser.getText(), Long.valueOf(fileSize));
			}

			File[] fileList = new File(prefix).listFiles(new ExpansionFileNameFilter());
			if(fileList == null || fileList.length <= 1) {
				android.util.Log.d(TAG, "Bobbys Story World expansion package file existance check failed");
				return true;
			}

			listUpFiles(fileMap, prefix, fileList);

			if(infoMap.size() != fileMap.size()) {
				android.util.Log.d(TAG, "Bobbys Story World expansion package file count check failed (" + infoMap.size() + ", " + fileMap.size() + ")");
				return true;
			}

			infoTree = new TreeMap<String, Long>(infoMap);
			Iterator<String> infoItr = infoTree.keySet().iterator();
			fileTree = new TreeMap<String, Long>(fileMap);
			Iterator<String> fileItr = fileTree.keySet().iterator();
			int cnt = 0;
			while(infoItr.hasNext()) {
				String infoStr = infoItr.next();
				String fileStr = fileItr.next();
				cnt++;
				if(!infoStr.equals(fileStr)) {
					android.util.Log.d(TAG, "Bobbys Story World expansion package file name has changed");
					return true;
				}
				if(!infoTree.get(infoStr).equals(fileTree.get(fileStr))) {
					android.util.Log.d(TAG, "Bobbys Story World expansion package file size has changed");
					return true;
				}
			}
		} catch(XmlPullParserException e) {
			e.printStackTrace();
		} catch(IOException ioe) {
			ioe.printStackTrace();
		} finally {
			if(infoMap != null)
				infoMap.clear();
			if(fileMap != null)
				fileMap.clear();
			if(infoTree != null)
				infoTree.clear();
			if(fileTree != null)
				fileTree.clear();
		}

		return false;
	}

	private void listUpFiles(HashMap<String, Long> map, String prefix, File[] files) {
		if(files != null && files.length > 0) {
			int cnt = 0;
			for(int i = 0 ; i < files.length ; i++) {
				String filename = files[i].toString();
				if(files[i].isDirectory()) {
					File[] temp = files[i].listFiles();
					listUpFiles(map, prefix, temp);
				} else {
					String filenameStr = files[i].toString();
					map.put(filenameStr.substring(prefix.length() + 1), Long.valueOf(files[i].length()));
					cnt++;
				}
			}
		}
    }

	private void showStorageWarning() {
    	runOnUiThread(new Runnable() {
    		@Override
    		public void run() {
    			Bundle args = new Bundle();
    			args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
    			args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.cc_journeymap_popup_warning_notenough_storage);
    			args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
    			CustomDialog dialog = new CustomDialog(SplashActivity.this, args, null);
    			dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
					@Override
					public void onDismiss(DialogInterface dialog) {
						finish();
					}
    			});
    			dialog.show();
    		}
    	});
	}

	private void showVersionWarning() {
    	runOnUiThread(new Runnable() {
    		@Override
    		public void run() {
    			Bundle args = new Bundle();
    			args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
    			args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.cc_app_update_notice);
    			args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
    			args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_TEXT_RES, R.string.cc_appupdate_button_title_ok);
    			CustomDialog dialog = new CustomDialog(SplashActivity.this, args, null);
    			dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
					@Override
					public void onDismiss(DialogInterface dialog) {
						final String appPackageName = SplashActivity.this.getPackageName(); 
						try {
						    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
						} catch (android.content.ActivityNotFoundException anfe) {
						    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
						}
						
						finish();
					}
    			});
    			dialog.show();
    		}
    	});
	}

	private void showFailDownloadWarning() {
    	runOnUiThread(new Runnable() {
    		@Override
    		public void run() {
    			Bundle args = new Bundle();
    			args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
    			args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.pf_net_popup_warning_network_unstable);
    			args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
    			CustomDialog dialog = new CustomDialog(SplashActivity.this, args, null);
    			dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
					@Override
					public void onDismiss(DialogInterface dialog) {
						finish();
					}
    			});
    			dialog.show();
    		}
    	});
	}

	private void showUseWifiWarning() {
    	runOnUiThread(new Runnable() {
    		@Override
    		public void run() {
    			Bundle args = new Bundle();
    			args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
    			args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.pf_net_warning_3g);
    			args.putInt(Constants.CUSTOM_DIALOG_LEFT_BUTTON_RES, R.drawable.popup_cancel_btn);
    			args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
    			CustomDialog dialog = new CustomDialog(SplashActivity.this, args, new CustomDialogButtonClickListener() {
					@Override
					public void onClickOK(int requestCode) {
						startDownload();
					}

					@Override
					public void onClickCancel(int requestCode) {
						finish();
					}
				});
    			dialog.setCancelable(false);
    			dialog.show();
    		}
    	});
	}

	private void launch() {
    	Intent i = new Intent();
    	i.setClass(this, BobbysStoryWorldActivity.class);
    	startActivity(i);
    	overridePendingTransition(0, 0);
    	finish();
	}

	private static void dirChecker(String extractPath, String filename) { 
		File f = new File(extractPath + File.separator + filename);
		if(!f.isDirectory()) {
			f.mkdirs();
		}
	}

    private void validateXAPKZipFiles() {
        AsyncTask<Object, DownloadProgressInfo, Boolean> validationTask = new AsyncTask<Object, DownloadProgressInfo, Boolean>() {
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//            }

            @Override
            protected Boolean doInBackground(Object... params) {
                for (XAPKFile xf : mAPKS) {
                    try {
	                	final String packageName = getPackageName();
	        			PackageManager pm = getPackageManager();
	        			PackageInfo packageInfo = pm.getPackageInfo(packageName, 0);
	//                    String fileName = Helpers.getExpansionAPKFileName(
	//                            SplashActivity.this,
	//                            xf.mIsMain, xf.mFileVersion);
	                    String fileName;
	//                    if(xf.mIsMain)
	//                    	fileName = mPolicy.getMainExpansionName();
	//                    else
	//                    	fileName = mPolicy.getPatchExpansionName();
	                    if(Constants.USE_LOCAL_OBB)
	                    	fileName = APKExpansionSupport.getAPKExpansionFiles(SplashActivity.this, packageInfo.versionCode, 0)[0];
	        			else
	        				fileName = BobbysStoryWorldPreference.getString(Constants.PREF_EXPANSION_MAIN_FILE, "");
	                    	
	                    if (!Helpers.doesFileExist(SplashActivity.this, fileName,
	                            xf.mFileSize, false))
	                        return false;
	                    fileName = Helpers
	                            .generateSaveFileName(SplashActivity.this, fileName);
	                    ZipResourceFile zrf;
	                    byte[] buf = new byte[1024 * 256];
                        zrf = new ZipResourceFile(fileName);
                        ZipEntryRO[] entries = zrf.getAllEntries();
                        /**
                         * First calculate the total compressed length
                         */
                        long totalCompressedLength = 0;
                        for (ZipEntryRO entry : entries) {
                            totalCompressedLength += entry.mCompressedLength;
                        }
                        float averageVerifySpeed = 0;
                        long totalBytesRemaining = totalCompressedLength;
                        long timeRemaining;
                        /**
                         * Then calculate a CRC for every file in the Zip file,
                         * comparing it to what is stored in the Zip directory.
                         * Note that for compressed Zip files we must extract
                         * the contents to do this comparison.
                         */
                        for (ZipEntryRO entry : entries) {
                            if (-1 != entry.mCRC32) {
                                long length = entry.mUncompressedLength;
                                CRC32 crc = new CRC32();
                                DataInputStream dis = null;
                                try {
                                    dis = new DataInputStream(
                                            zrf.getInputStream(entry.mFileName));

                                    long startTime = SystemClock.uptimeMillis();
                                    while (length > 0) {
                                        int seek = (int) (length > buf.length ? buf.length
                                                : length);
                                        dis.readFully(buf, 0, seek);
                                        crc.update(buf, 0, seek);
                                        length -= seek;
                                        long currentTime = SystemClock.uptimeMillis();
                                        long timePassed = currentTime - startTime;
                                        if (timePassed > 0) {
                                            float currentSpeedSample = (float) seek
                                                    / (float) timePassed;
                                            if (0 != averageVerifySpeed) {
                                                averageVerifySpeed = SMOOTHING_FACTOR
                                                        * currentSpeedSample
                                                        + (1 - SMOOTHING_FACTOR)
                                                        * averageVerifySpeed;
                                            } else {
                                                averageVerifySpeed = currentSpeedSample;
                                            }
                                            totalBytesRemaining -= seek;
                                            timeRemaining = (long) (totalBytesRemaining / averageVerifySpeed);
                                            this.publishProgress(
                                                    new DownloadProgressInfo(
                                                            totalCompressedLength,
                                                            totalCompressedLength
                                                                    - totalBytesRemaining,
                                                            timeRemaining,
                                                            averageVerifySpeed)
                                                    );
                                        }
                                        startTime = currentTime;
                                        if (mCancelValidation)
                                            return true;
                                    }
                                    if (crc.getValue() != entry.mCRC32) {
                                        Log.e(TAG,
                                                "CRC does not match for entry: "
                                                        + entry.mFileName);
                                        Log.e(TAG,
                                                "In file: " + entry.getZipFileName());
                                        return false;
                                    }
                                } finally {
                                    if (null != dis) {
                                        dis.close();
                                    }
                                }
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        return false;
                    } catch (NameNotFoundException e) {
                    	e.printStackTrace();
                        return false;
                    }
                }
                return true;
            }

            @Override
            protected void onProgressUpdate(DownloadProgressInfo... values) {
//                onDownloadProgress(values[0]);
                super.onProgressUpdate(values);
            }

//            @Override
//            protected void onPostExecute(Boolean result) {
//                super.onPostExecute(result);
//            }

        };
        validationTask.execute(new Object());
    }

    private static class XAPKFile {
        public final boolean mIsMain;
        public final int mFileVersion;
        public final long mFileSize;

        XAPKFile(boolean isMain, int fileVersion, long fileSize) {
            mIsMain = isMain;
            mFileVersion = fileVersion;
            mFileSize = fileSize;
        }
    }

	private class searchObbFilter implements FilenameFilter {
		@Override
		public boolean accept(File dir, String filename) {
			if(filename.endsWith(".obb"))
				return true;
			return false;
		}
	}

	private int parseVersionCode(String filename) {
		int startOffset = filename.indexOf(".");
		if(startOffset > -1) {
			int endOffset = filename.indexOf(".", startOffset+1);
			if(endOffset > -1) {
				try {
					int ver = Integer.parseInt(filename.substring(startOffset+1, endOffset));
					return ver;
				}
				catch (NumberFormatException e) {
					e.printStackTrace();
					return -1;
				}
			}
		}
		return -1;
	}
	
	private String getMainFileName() {
		try {
			String packageName = getPackageName();
			PackageManager pm = getPackageManager();
			PackageInfo packageInfo = pm.getPackageInfo(packageName, 0);
			String file;
			if(Constants.USE_LOCAL_OBB)
				file = APKExpansionSupport.getAPKExpansionFiles(SplashActivity.this, packageInfo.versionCode, 0)[0];
			else
				file = BobbysStoryWorldPreference.getString(Constants.PREF_EXPANSION_MAIN_FILE, "");
			
			if(file.equals("")) {
				String prefix = APKExpansionSupport.getAPKExpansionPath(SplashActivity.this);
				File[] obbfileList = new File(prefix).listFiles(new searchObbFilter());
				int maxVer = 0;
				for(File curFile : obbfileList) {
					String fileName = curFile.getName();
					int ver = parseVersionCode(fileName.substring(fileName.lastIndexOf("/")+1));
					if(ver > maxVer) {
						maxVer = ver;
						file = prefix+"/"+curFile.getName();
					}
				}
			}
	
			return file;
		}
		catch(NameNotFoundException e) {
			e.printStackTrace();
			return "";
		}
	}

	private class ExpansionFileNameFilter implements FilenameFilter {
		@Override
		public boolean accept(File dir, String filename) {
			if(filename.endsWith(".obb"))
				return false;
			if(filename.equals(INFO_FILE))
				return false;
			return true;
		}
	}

    private class MyLicenseCheckerCallback implements LicenseCheckerCallback {
        public void allow(int policyReason) {
            if (isFinishing()) {
                // Don't update UI if Activity is finishing.
                return;
            }
            // Should allow user access.
//            displayResult(getString(R.string.allow));
            mountObb();
        }

        public void dontAllow(int policyReason) {
            if (isFinishing()) {
                // Don't update UI if Activity is finishing.
                return;
            }
//            displayResult(getString(R.string.dont_allow));
            // Should not allow access. In most cases, the app should assume
            // the user has access unless it encounters this. If it does,
            // the app should inform the user of their unlicensed ways
            // and then either shut down the app or limit the user to a
            // restricted set of features.
            // In this example, we show a dialog that takes the user to Market.
            // If the reason for the lack of license is that the service is
            // unavailable or there is another problem, we display a
            // retry button on the dialog and a different message.
//            displayDialog(policyReason == Policy.RETRY);
            finish();
        }

        public void applicationError(int errorCode) {
            if (isFinishing()) {
                // Don't update UI if Activity is finishing.
                return;
            }
            // This is a polite way of saying the developer made a mistake
            // while setting up or calling the license checker library.
            // Please examine the error code and fix the error.
//            String result = String.format(getString(R.string.application_error), errorCode);
//            displayResult(result);
            finish();
        }
    }

	private class UnzipRunnable implements Runnable {
		private boolean mUpdateProgress;
		private boolean mCanceled;

		public void needUpdate() {
			mUpdateProgress = true;
		}

		public void cancel() {
			mCanceled = true;
		}
		
		private void deleteFile(File file) {
			if(file.isFile()) {
				file.delete();
			} else if(file.isDirectory()) {
				File[] list = file.listFiles(new ExpansionFileNameFilter());
				for(File f : list) {
        			deleteFile(f);
        		}
				file.delete();
			}
		}

		@Override
		public void run() {
	    	try {
	    		final String packageName = getPackageName();
	    		PackageManager pm = getPackageManager();
	    		PackageInfo info = pm.getPackageInfo(packageName, 0);

	    		String prefix = APKExpansionSupport.getAPKExpansionPath(SplashActivity.this);
//        		String[] files = APKExpansionSupport.getAPKExpansionFiles(SplashActivity.this, info.versionCode, 0);
	    		String file = getMainFileName();
//	    		if(Constants.USE_LOCAL_OBB)
//	    			file = APKExpansionSupport.getAPKExpansionFiles(SplashActivity.this, info.versionCode, 0)[0];
//	    		else
//	    			file = BobbysStoryWorldPreference.getString(Constants.PREF_EXPANSION_MAIN_FILE, "");

        		//delete file
        		File[] delFileList = new File(prefix).listFiles(new ExpansionFileNameFilter());
        		for(File f : delFileList) {
        			deleteFile(f);
        		}
        		
        		int entryCount = 0;
        		int processCount = 0;
        		try {
//            		for(int i = 0 ; i < files.length ; i++) {
            			ZipFile zip = new ZipFile(file);
            			entryCount += zip.size();
//            		}
        		} catch(IOException e) {
        			showFailDownloadWarning();
        			e.printStackTrace();
        			return;
        		}

//        		for(int i = 0 ; i < files.length ; i++) {
        			String path = file.substring(0, file.lastIndexOf(File.separator));
        			try {
        				FileInputStream fin = new FileInputStream(file);
        				ZipInputStream zin = new ZipInputStream(fin);
        				ZipEntry ze = null;
        				byte[] buffer = new byte[4096];
        				while((ze = zin.getNextEntry()) != null) {
        					if(ze.isDirectory()) {
        						dirChecker(path, ze.getName());
        					} else {
        						FileOutputStream fout = new FileOutputStream(path + File.separator + ze.getName());
        						BufferedOutputStream bos = new BufferedOutputStream(fout, buffer.length);
        						int c = 0;
        						while((c = zin.read(buffer, 0, buffer.length)) != -1) {
        							bos.write(buffer, 0, c);
        						}
        						fout.flush();
        						bos.flush();
        						fout.close();
        						bos.close();
        						zin.closeEntry();
        					}
        					processCount++;

        					if(mUpdateProgress) {
            					mUpdateProgress = false;
            					Message msg = Message.obtain(mHandler, UPDATE_PROGRESS);
            					if(entryCount > 0)
            						msg.arg1 = (int)(((float)processCount / (float)entryCount) * 100);
            					mHandler.sendMessage(msg);
            				}

        					if(mCanceled) {
        						android.util.Log.d(TAG, "Bobbys Story World extracting expansion package canceled.");
        						return;
        					}
        				}
        				zin.close();
        			} catch(Exception e) {
        				e.printStackTrace();
        			}

					if(mCanceled) {
						android.util.Log.d(TAG, "Bobbys Story World extracting expansion package canceled.");
						return;
					}

        			String obbName = file.substring(prefix.length() + 1).substring(0, path.indexOf(info.packageName));
    				if(obbName.startsWith("main")) {
            			BobbysStoryWorldPreference.putLong(Constants.PREF_EXPANSION_HASH_MAIN, new File(file).lastModified());
    				} else {
    					// TODO: patch handling
    				}
//        		}

				if(mCanceled) {
					android.util.Log.d(TAG, "Bobbys Story World extracting expansion package canceled.");
					return;
				}

        		BobbysStoryWorldPreference.putInt(Constants.PREF_EXPANSION, info.versionCode);

				android.util.Log.d(TAG, "After first installation of Bobbys Story World version " + info.versionCode);
	    	} catch(NameNotFoundException e) {
	    		e.printStackTrace();
	    	}

	    	mUnzipRunnable = null;
			mUpdateProgress = false;
			mHandler.sendEmptyMessage(DONE_PROGRESS);
		}
	};
}