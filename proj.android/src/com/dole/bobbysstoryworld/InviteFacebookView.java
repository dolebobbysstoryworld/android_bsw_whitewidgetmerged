package com.dole.bobbysstoryworld;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dole.bobbysstoryworld.server.RequestChargeCoin;
import com.dole.bobbysstoryworld.server.ServerTask;
import com.dole.bobbysstoryworld.server.ServerTask.IServerResponseCallback;
import com.dole.bobbysstoryworld.ui.FontTextView;
import com.facebook.FacebookException;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphObject;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.facebook.widget.WebDialog.RequestsDialogBuilder;

public class InviteFacebookView extends RelativeLayout implements IServerResponseCallback {
	private static final String TAG = "InviteFacebookView";
	
	private static final String ID = "id";
	private static final String NAME = "name";
	private static final String PICTURE = "picture";
	private static final String DATA = "data";
	private static final String URL = "url";

	private class FriendInfo {
		private String id;
		private String iconUrl;
		private String name;
		private boolean checked;
		
		public FriendInfo(String id, String iconUrl, String name) {
			this.id = id;
			this.iconUrl = iconUrl;
			this.name = name;
			this.checked = false;
		}
	}
	
	List<FriendInfo> mInfos;
	
	private LruCache<String, Bitmap> mIconCache;
	private Bitmap mDefaultIcon;
	private Bitmap mDefaultFrame;
	private ListView mListView;
	private ProgressDialog mProgressDialog;
	private int mInviteCount;

    public InviteFacebookView(Context context) {
        super(context);
    }

    public InviteFacebookView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    
    public InviteFacebookView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();

		mInviteCount = 0;
		
		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
		final int cacheSize = maxMemory / 8;
		mIconCache = new LruCache<String, Bitmap>(cacheSize)  {
	        @Override
	        protected int sizeOf(String key, Bitmap bitmap) {
	            return bitmap.getByteCount() / 1024;
	        }
	    };
	    mDefaultIcon = BitmapFactory.decodeResource(getResources(), R.drawable.invite_no_image);
	    mDefaultFrame = BitmapFactory.decodeResource(getResources(), R.drawable.invite_facenook_image);
	    
		Session session = Session.getActiveSession();
		if (session == null) {
//			if (savedInstanceState != null) {
//				session = Session.restoreSession(getActivity(), null, null, savedInstanceState);
//			}
//			if (session == null) {
				session = new Session(getContext());
//			}
			Session.setActiveSession(session);
		}
		
		if(mInfos == null) {
			mInfos = new ArrayList<FriendInfo>();
		}
		
		mListView = (ListView)findViewById(R.id.invite_list);
		mListView.setOnItemClickListener(mOnListItemClickListener);
		final CheckBox checkAll = (CheckBox)findViewById(R.id.invite_select_all);
		checkAll.setOnCheckedChangeListener(mOnAllCheckChangeListener);
		checkAll.setEnabled(false);
		
		requestFacebookFriends(session);
		
		final Button inviteButton = (Button)findViewById(R.id.invite_button);
		inviteButton.setEnabled(false);
		inviteButton.setOnClickListener(mInviteClickListener);

		final FontTextView description = (FontTextView)findViewById(R.id.invite_info);
		if(description != null) {
//			String descString = getResources().getString(R.string.select_friends_for_coin);
//			if(descString != null)
//				description.setText(Html.fromHtml(descString));
		}
	}
	
	private OnClickListener mInviteClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			StringBuilder idBuilder = new StringBuilder();
			for(FriendInfo info : mInfos) {
				if(info.checked) {
					mInviteCount++;
					idBuilder.append(info.id);	
					if(mInfos.get(mInfos.size() - 1) != info) {
						idBuilder.append(",");
					}
				}
			}
//			//TODO Clear check
			final Activity activity = (BobbysStoryWorldActivity)getContext();
			RequestsDialogBuilder builder = new WebDialog.RequestsDialogBuilder(activity);
			builder.setTitle("Send a Request");
			builder.setTo(idBuilder.toString());
			builder.setMessage("http://doleapps.com");
			builder.setOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(Bundle values, FacebookException error) {

                	if(error == null) {
//            		    final CheckBox checkAll = (CheckBox)findViewById(R.id.invite_select_all);
//            			checkAll.setChecked(false);
            			
                		if(mInviteCount > 0) {
                			Util.requestChargeCoin(getContext(), InviteFacebookView.this, mInviteCount);
                			mProgressDialog = Util.openProgressDialog(activity);
                		}
                		android.util.Log.d(TAG, " error values = " + error);	
                	} else {
                		android.util.Log.d(TAG, " not error values = " + values);	                		
                	}
                }   
            });
			
			WebDialog requestsDialog = builder.build();
		    requestsDialog.show();
		}
	};

	private Request createRequest(Session session) {
        Request request = Request.newGraphPathRequest(session, "me/friends", null);
        Set<String> fields = new HashSet<String>();
        String[] requiredFields = new String[]{
                ID,
                NAME,
                PICTURE
        };
        fields.addAll(Arrays.asList(requiredFields));

        Bundle parameters = request.getParameters();
        parameters.putString("fields", ID + "," + NAME + "," + PICTURE);
        request.setParameters(parameters);

        return request;
    }

	private void requestFacebookFriends(Session session) {
		Request friendRequest = createRequest(session);
		friendRequest.setCallback(new RequestFriendsListSessionStatusCallback());
		friendRequest.executeAsync();
	}
	
	private class RequestFriendsListSessionStatusCallback implements Request.Callback {
		@Override
		public void onCompleted(final Response response) {
			android.util.Log.d(TAG, "request callback");
			android.util.Log.d(TAG, "Result: " + response.toString());

			Model.runOnWorkerThread(new Runnable() {
				@Override
				public void run() {
					try{
			            GraphObject graphObject = response.getGraphObject();
			            JSONObject jsonObject = graphObject.getInnerJSONObject();
			            JSONArray array = jsonObject.getJSONArray("data");
			            
			            android.util.Log.d(TAG, " array len = " + array.length());

			            for(int i = 0 ; i < array.length() ; i++){
			                JSONObject friend = array.getJSONObject(i);
			                final String id = friend.getString(ID);
			                final String name = friend.getString(NAME);
			                
			                JSONObject jsonPicture = friend.getJSONObject(PICTURE);
			                JSONObject jsonPictureData = jsonPicture.getJSONObject(DATA);
			                final String picUrl = jsonPictureData.getString(URL);
			                
			                android.util.Log.d(TAG," id = " + id + " | name = " + name + " | pic = " + picUrl);
			                FriendInfo info = new FriendInfo(id, picUrl, name);
			                mInfos.add(info);
			            }
			        } catch(JSONException e){
			        	android.util.Log.e(TAG, " JSONException ", e);
			        }
					
					post(new Runnable() {
						@Override
						public void run() {
							final ListView listView = (ListView)findViewById(R.id.invite_list);
							listView.setAdapter(new FriendsAdapter());
							final CheckBox checkAll = (CheckBox)findViewById(R.id.invite_select_all);
							checkAll.setEnabled(true);
						}
					});
				}
			});
		}
    }
	
	private class FriendsAdapter extends BaseAdapter {
		@Override
		public int getCount() {
			if(mInfos == null)
				return 0;
			return mInfos.size();
		}

		@Override
		public Object getItem(int position) {
			if(mInfos == null)
				return null;
			return mInfos.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null) {
				final LayoutInflater inflater = LayoutInflater.from(getContext());
				convertView = inflater.inflate(R.layout.facebook_invite_item, mListView, false);
			}
			
			Object tag = mInfos.get(position);
			if(tag instanceof FriendInfo) {
				FriendInfo info = (FriendInfo) tag;
				convertView.setTag(info);
				
				final ImageView imageView = (ImageView)convertView.findViewById(R.id.invite_item_img);
				final TextView nameText = (TextView)convertView.findViewById(R.id.invite_item_name);
				final CheckBox checkBox = (CheckBox)convertView.findViewById(R.id.invite_item_check);
				
				final Bitmap icon = getBitmapFromCache(info);
				imageView.setTag(info.iconUrl);
				imageView.setImageBitmap(icon);
				
				nameText.setText(info.name);

				checkBox.setTag(info);
				checkBox.setChecked(info.checked);
			}
			return convertView;
		}
	}
	
	private OnCheckedChangeListener mOnAllCheckChangeListener = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			if(mListView == null)
				return;
			
			for(FriendInfo info : mInfos) {
				info.checked = isChecked;
			}
			
			final View inviteButton = findViewById(R.id.invite_button);
			if(inviteButton != null && mInfos.size() > 0)
				inviteButton.setEnabled(isChecked);
			
			ListAdapter listAdapter = mListView.getAdapter();
			if(listAdapter != null) {
				((BaseAdapter) listAdapter).notifyDataSetChanged();
			}
		}
	};
	
	private OnItemClickListener mOnListItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			final CheckBox checkBox = (CheckBox) view.findViewById(R.id.invite_item_check);
			checkBox.toggle();
			final boolean isChecked = checkBox.isChecked();

			android.util.Log.d(TAG, " onItemClick");
			final Object tag = checkBox.getTag();
			if(tag instanceof FriendInfo) {
				final FriendInfo info = (FriendInfo) tag;
				info.checked = isChecked;
			}

			boolean isAtLeastOnChecked = false;
			for(FriendInfo info : mInfos) {
				if(info.checked) {
					isAtLeastOnChecked = true;
					break;
				}
			}
			
			final Button inviteButton = (Button)findViewById(R.id.invite_button);
			inviteButton.setEnabled(isAtLeastOnChecked);

			boolean isAllChecked = true;
			for(FriendInfo info : mInfos) {
				if(!info.checked) {
					isAllChecked = false;
					break;
				}
			}
			
			final CheckBox checkAll = (CheckBox)findViewById(R.id.invite_select_all);
			if(checkAll.isChecked() != isAllChecked) {
				checkAll.setOnCheckedChangeListener(null);
				checkAll.setChecked(isAllChecked);
				checkAll.setOnCheckedChangeListener(mOnAllCheckChangeListener);
			}
		}
	};
	
	private class BitmapLoaderTask extends AsyncTask<FriendInfo, Void, Bitmap> {
		private FriendInfo mInfo;
		private String mKey;

	    @Override
	    protected Bitmap doInBackground(FriendInfo... infos) {
	    	Bitmap icon = null;
	    	Object param = infos[0];
	    	if(param instanceof FriendInfo) {
	    		mInfo = (FriendInfo) param;
	    		mKey = mInfo.iconUrl;
	    		HttpURLConnection connection = null;
	    		InputStream is = null;
	    		try {
	    			URL url = new URL(mKey.replaceAll("\\\\", ""));
	    			connection = (HttpURLConnection) url.openConnection();
	    			connection.setDoInput(true);
	    			connection.connect();
	    			is = connection.getInputStream();
	    			icon = BitmapFactory.decodeStream(is);
	    		} catch (MalformedURLException e) {
	    			android.util.Log.d(TAG, " URL not well-formed", e);
	    		} catch (IOException e) {
	    			android.util.Log.d(TAG, " IO error", e);
	    		} finally {
	    			if(connection != null)
	    				connection.disconnect();

	    			if(is != null) {
	    				try {
	    					is.close();
	    				} catch (IOException e) {
	    					android.util.Log.d(TAG, " IOexception occurred on InputStream close", e);
	    				}
	    			}
	    		}
	    	}

	    	if(icon != null) {
	    		View foundView = mListView.findViewWithTag(mInfo);
	    		if(foundView != null) {
					ImageView iconView = (ImageView)foundView.findViewById(R.id.invite_item_img);
					if(iconView != null) {
						final int width = iconView.getWidth();
						final int height = iconView.getHeight();
						icon = Bitmap.createScaledBitmap(icon, width, height, false);

						Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
						Canvas canvas = new Canvas(bitmap);
						canvas.drawBitmap(mDefaultFrame, 0, 0, null);

						Paint paint = new Paint();
						paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
						canvas.drawBitmap(icon, 0, 0, paint);

						icon = bitmap;
					}
	    		}
	    	}

	    	return icon;
	    }

	    @Override
		protected void onPostExecute(Bitmap result) {
			super.onPostExecute(result);
			View foundView = mListView.findViewWithTag(mInfo);
			if(foundView != null) {
				final ImageView iconView = (ImageView)foundView.findViewById(R.id.invite_item_img);
				iconView.setImageBitmap(result);
				setBitmapToCache(mInfo, result);
			}
		}
	}
	
	public void setBitmapToCache(FriendInfo info, Bitmap bitmap) {
		String key = null;
		if(info instanceof FriendInfo) {
			key = info.iconUrl;
			final Bitmap icon = mIconCache.get(key);
			if(icon == null)
				mIconCache.put(key, bitmap);		
		}
	}

	public Bitmap getBitmapFromCache(FriendInfo info) {
		Bitmap icon = null;
		if(info instanceof FriendInfo) {
			final String key = info.iconUrl;
			icon = mIconCache.get(key);
			if(icon == null) {
				BitmapLoaderTask task = new BitmapLoaderTask();
				task.execute(info);
			}
		}

		if(icon == null) {
			return mDefaultIcon;
		}
		return icon;
	}

	@Override
	public void onSuccess(ServerTask parser, Map<String, String> result) {
		if(parser instanceof RequestChargeCoin) {
			int coin = BobbysStoryWorldPreference.getInt(Constants.PREF_LAST_DOLE_COIN, 0);
			coin+=mInviteCount;
			BobbysStoryWorldPreference.putInt(Constants.PREF_LAST_DOLE_COIN, coin);
			if(mProgressDialog != null)
				mProgressDialog.dismiss();
			
			Bundle args = new Bundle();
//			args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_coin);
//			args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.congratulation_for_coin);
//			args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES_PARAM1, mInviteCount);
//			args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
//
//			Util.showDialogFragment(
//					getFragmentManager(), 
//					new CustomDialog(), 
//					Constants.TAG_DIALOG_CUSTOM, 
//					args, 
//					InviteFacebookFragment.this, 
//					Constants.REQUEST_DOLE_COIN);
			
			mInviteCount = 0;
		}
	}

	@Override
	public void onFailed(ServerTask parser, Map<String, String> result,
			int returnCode) {
		int failAmount = BobbysStoryWorldPreference.getInt(Constants.PREF_COIN_TO_ADDED, 0);
		failAmount += 100;
		BobbysStoryWorldPreference.putInt(Constants.PREF_COIN_TO_ADDED, failAmount);
		mInviteCount = 0;
		if(mProgressDialog != null)
			mProgressDialog.dismiss();
		
	}
}