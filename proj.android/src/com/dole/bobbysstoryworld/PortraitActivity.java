package com.dole.bobbysstoryworld;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.text.Html;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnLayoutChangeListener;
import android.widget.EditText;

//import com.dole.bobbysstoryworld.server.RequestNewAuthKey;
//import com.dole.bobbysstoryworld.server.ServerTask;
//import com.dole.bobbysstoryworld.server.ServerTask.IServerResponseCallback;
import com.dole.bobbysstoryworld.ui.CustomDialog;
import com.dole.bobbysstoryworld.ui.FontTextView;

import org.cocos2dx.lib.Cocos2dxHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.facebook.Session;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Log;

public class PortraitActivity extends Activity implements TextureView.SurfaceTextureListener/* , IServerResponseCallback */{
	public static final String			MENU_MODE					= "MENU_MODE";
	public static final String			MENU_PENDING				= "MENU_PENDING";
	private final int					QRCODE_LENGTH				= 16;
	private final int					FLAG_CAMERA_PREVIEW			= 0x00F00;
	private final int					FLAG_CHECK_CANCEL			= 0x0F000;

	public static final int				ENTRY_MENU_SETTING			= 0x20001;
	public static final int				ENTRY_MENU_LOGIN			= 0x20002;
	public static final int				ENTRY_MENU_FIND_PASSWORD	= 0x20003;
	public static final int				ENTRY_MENU_SIGNUP			= 0x21004;
	public static final int				ENTRY_MENU_SIGNUP_TERMS		= 0x20014;
	public static final int				ENTRY_MENU_LEAVE			= 0x20005;
	public static final int				ENTRY_MENU_DOLE_COIN		= 0x20006;
	public static final int				ENTRY_MENU_DOLE_COIN_QRCODE	= 0x20106;
	public static final int				ENTRY_MENU_INVITE_FACEBOOK	= 0x20007;
	public static final int				ENTRY_MENU_ABOUT			= 0x20008;
	public static final int				ENTRY_MENU_HELP				= 0x20009;
	public static final int				ENTRY_MENU_ABOUT_TERMS		= 0x2000A;
	public static final int				ENTRY_MENU_DOLE_COIN_USAGE	= 0x2000B;

	public static final String			FIRST_LAUNCH				= "FIRST_LAUNCH";

	private int							mPendingEntryPoint			= -1;
	private int							mEntryPoint					= -1;
	private ArrayList<Integer>			mMenuStack					= new ArrayList<Integer>();
	private ViewGroup					mMenuView;
	private Bundle						mSavedData					= new Bundle();
	private QRCodeManager				mQRCodeManager				= null;
	private View						mFocusView;
	private SurfaceTexture				mSurface;
	private Camera						mCamera;
	private int							mSurfaceWidth;
	private int							mSurfaceHeight;
	private int							mPreviewWidth;
	private int							mPreviewHeight;
	private boolean						mPendingCameraOpen;
	private boolean						mIsPreviewing				= false;

	private SoundPool					mSoundPool;
	private SoundLoadListener			mSoundLoadListener			= new SoundLoadListener();
	private HashMap<String, Integer>	mSoundPoolMap				= new HashMap<String, Integer>();
	private SparseArray<String>			mSoundPoolWaitPlay			= new SparseArray<String>();

	private class SoundLoadListener implements SoundPool.OnLoadCompleteListener {
		@Override
		public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
			mSoundPool.play( sampleId, Constants.DEFAULT_VOLUME_BACKGROUND_SOUND, Constants.DEFAULT_VOLUME_BACKGROUND_SOUND, 1, 0, 1.f );
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate( savedInstanceState );

		setVolumeControlStream( AudioManager.STREAM_MUSIC );
		setContentView( R.layout.portrait_main );

		mSoundPool = new SoundPool( 10, AudioManager.STREAM_MUSIC, 44100 );
		mSoundPool.setOnLoadCompleteListener( mSoundLoadListener );

		int entry = getIntent().getIntExtra( MENU_MODE, -1 );
		mPendingEntryPoint = getIntent().getIntExtra( MENU_PENDING, -1 );
		mMenuView = (ViewGroup) findViewById( R.id.portrait_main );
		showMenu( entry );
	}

	@Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart( this );
	}

	@Override
	protected void onStop() {
		super.onStop();
		EasyTracker.getInstance().activityStop( this );
	}

	public void showMenu(int entry) {
		int tempEntry = entry;
		if ( entryNeedLogin( tempEntry ) && !isLoggedIn() ) {
			mPendingEntryPoint = entry;
			tempEntry = ENTRY_MENU_LOGIN;
			// } else if(entryNeedReAuth(tempEntry) && isLoggedIn() && !mIsAuth)
			// {
			// mPendingEntryPoint = entry;
			// String registrationId =
			// "";//BobbysStoryWorldPreference.getString(Constants.PROPERTY_REG_ID,
			// "");
			//
			// final Map<String, String> request = new HashMap<String,
			// String>();
			// request.put(Constants.TAG_USER_NO,
			// String.valueOf(BobbysStoryWorldPreference.getInt(Constants.TAG_USER_NO,
			// 0)));
			// request.put(Constants.TAG_AUTHKEY,
			// BobbysStoryWorldPreference.getString(Constants.PREF_AUTH_KEY,
			// ""));
			// request.put(Constants.TAG_SERVICE_CODE, Constants.SERVICE_CODE);
			// request.put(Constants.TAG_MOBILE_OS, Constants.OS_STRING);
			// request.put(Constants.TAG_DEVICE_TOKEN, registrationId);
			// request.put(Constants.TAG_UUID, Util.getUUID(this));
			// request.put(Constants.TAG_CLIENT_IP, Util.getIp(this));
			//
			// ServerTask task = new RequestNewAuthKey(this, request);
			// task.setCallback(this);
			// Model.runOnWorkerThread(task);
			// return;
		}
		// mIsAuth = false;

		final int entryPoint = tempEntry;
		runOnUiThread( new Runnable() {
			@Override
			public void run() {
				// if(mMenuStack.contains(entryPoint)) {
				// int idx = mMenuStack.indexOf(entryPoint);
				// mMenuStack.subList(idx+1, mMenuStack.size()).clear();
				// } else {
				mMenuStack.add( entryPoint );
				// }
				mEntryPoint = entryPoint;
				setRequestedOrientation( ActivityInfo.SCREEN_ORIENTATION_PORTRAIT );

				LayoutInflater li = (LayoutInflater) getSystemService( Context.LAYOUT_INFLATER_SERVICE );

				if ( li != null && mMenuView != null ) {
					mMenuView.removeAllViews();
					int layoutId = -1;

					switch ( entryPoint ) {
						case ENTRY_MENU_SETTING:
							layoutId = R.layout.menu_setting;
							break;

						case ENTRY_MENU_LOGIN:
							layoutId = R.layout.menu_login;
							break;

						case ENTRY_MENU_FIND_PASSWORD:
							layoutId = R.layout.menu_find_password;
							break;

						case ENTRY_MENU_SIGNUP:
							layoutId = R.layout.user_detail;
							break;

						case ENTRY_MENU_LEAVE:
							layoutId = R.layout.menu_leave;
							break;

						case ENTRY_MENU_DOLE_COIN:
							layoutId = R.layout.dole_coin_main;
							break;

						case ENTRY_MENU_INVITE_FACEBOOK:
							layoutId = R.layout.facebook_invite;
							break;

						case ENTRY_MENU_ABOUT:
							layoutId = R.layout.menu_about;
							break;

						case ENTRY_MENU_HELP:
							layoutId = R.layout.menu_help;
							break;

						case ENTRY_MENU_SIGNUP_TERMS:
							layoutId = R.layout.agree_personal_main;
							break;

						case ENTRY_MENU_ABOUT_TERMS:
							layoutId = R.layout.agree_personal_about;
							break;

						case ENTRY_MENU_DOLE_COIN_USAGE:
							layoutId = R.layout.dole_coin_usage;
							break;

						case ENTRY_MENU_DOLE_COIN_QRCODE:
							layoutId = R.layout.camera_qrcode;
							break;
					}

					li.inflate( layoutId, mMenuView );
					mMenuView.setVisibility( View.VISIBLE );
					if ( ( mEntryPoint & FLAG_CAMERA_PREVIEW ) != 0 ) {
						initCameraView();
						startCameraPreview();
					}
				}
			}
		} );
	}

	public void removeMenuStack(int menu) {
		int index = mMenuStack.indexOf( menu );
		if ( index > -1 ) {
			mMenuStack.remove( index );
		}
	}

	public void loginFinished(boolean showSetting) {
		Cocos2dxHelper.runOnGLThread( new Runnable() {
			@Override
			public void run() {
				nativeSyncData();
			}
		} );

		int current = 0;
		do {
			if ( mMenuStack.size() > 0 )
				current = mMenuStack.remove( mMenuStack.size() - 1 );
			else
				current = ENTRY_MENU_LOGIN;
		} while ( current != ENTRY_MENU_LOGIN );

		if ( mPendingEntryPoint < 0 ) {
			if ( showSetting ) {
				showMenu( ENTRY_MENU_SETTING );
				removeMenuStack( ENTRY_MENU_SIGNUP );
			} else {
				if ( handleBackPress() )
					super.onBackPressed();
			}
		} else {
			switch ( mPendingEntryPoint ) {
				case BobbysStoryWorldActivity.SHARE:
				case BobbysStoryWorldActivity.DOWNLOAD_TO_DEVICE:
				case BobbysStoryWorldActivity.PENDING_PURCHASE:
					final int entryBackup = mPendingEntryPoint;
					mPendingEntryPoint = -1;

					Cocos2dxHelper.runOnGLThread( new Runnable() {
						@Override
						public void run() {
							nativeHandleRecallType( entryBackup );
						}
					} );

					handleBackPress();
					super.onBackPressed();
					break;

				default:
					// mIsAuth = true;
					showMenu( mPendingEntryPoint );
					mPendingEntryPoint = -1;
					break;
			}
		}
	}

	private boolean entryNeedLogin(int entry) {
		if ( entry == ENTRY_MENU_DOLE_COIN_QRCODE ) {
			return true;
		}
		return false;
	}

	// private boolean entryNeedReAuth(int entry) {
	// if(entry == ENTRY_MENU_DOLE_COIN || entry == ENTRY_MENU_SIGNUP) {
	// return true;
	// }
	// return false;
	// }

	private boolean isLoggedIn() {
		if ( BobbysStoryWorldPreference.getString( Constants.PREF_AUTH_KEY, null ) != null )
			return true;
		else
			return false;
	}

	private boolean handleBackPress() {
		if ( mMenuStack.size() > 1 ) {
			int currentMenu = mMenuStack.remove( mMenuStack.size() - 1 );

			if ( ( currentMenu & FLAG_CHECK_CANCEL ) != 0 ) {
				View destView = mMenuView.getChildAt( 0 );
				if ( destView instanceof CheckCancelInterface ) {
					if ( ( (CheckCancelInterface) destView ).onCheckCancel() ) {
						mMenuStack.add( currentMenu );
						return false;
					}
				}
			}

			if ( ( currentMenu & FLAG_CAMERA_PREVIEW ) != 0 ) {
				stopCameraPreview();
			}

			int lastMenu = mMenuStack.remove( mMenuStack.size() - 1 );
			showMenu( lastMenu );
			return false;
		} else {
			if ( ( mEntryPoint & FLAG_CAMERA_PREVIEW ) != 0 ) {
				stopCameraPreview();
			}

			if ( mEntryPoint == ENTRY_MENU_LOGIN ) {
				// No more login after first launch
				BobbysStoryWorldPreference.putBoolean( Constants.PREF_IS_FIRST_APP_LAUNCH, false );
				if ( !nativeShowGuideHome() )
					BobbysStoryWorldPreference.putBoolean( Constants.PREF_PENDING_GUIDE_HOME, true );
			}
			return true;
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

		Cocos2dxHelper.runOnGLThread( new Runnable() {
			@Override
			public void run() {
				nativePauseAudioEngine();
			}
		} );

		if ( ( mEntryPoint & FLAG_CAMERA_PREVIEW ) != 0 )
			stopCameraPreview();
	}

	@Override
	protected void onResume() {
		super.onResume();

		Cocos2dxHelper.runOnGLThread( new Runnable() {
			@Override
			public void run() {
				nativeResumeAudioEngine();
			}
		} );

		if ( ( mEntryPoint & FLAG_CAMERA_PREVIEW ) != 0 )
			startCameraPreview();

	}

	@Override
	public void onBackPressed() {
		if ( handleBackPress() )
			super.onBackPressed();
	}

	protected Bundle getSavedDataBundle() {
		return mSavedData;
	}

	public void saveAuthData() {
		final int userNo = BobbysStoryWorldPreference.getInt( Constants.PREF_USER_NO, -1 );
		final String authKey = BobbysStoryWorldPreference.getString( Constants.PREF_AUTH_KEY, null );
		Cocos2dxHelper.runOnGLThread( new Runnable() {
			@Override
			public void run() {
				if ( userNo >= 0 && authKey != null )
					nativeSaveAuthData( authKey, userNo );
			}
		} );
	}

	public void saveDoleCoinBalance() {
		final int balance = BobbysStoryWorldPreference.getInt( Constants.PREF_LAST_DOLE_COIN, -1 );
		Cocos2dxHelper.runOnGLThread( new Runnable() {
			@Override
			public void run() {
				if ( balance >= 0 ) {
					if ( !nativeDoleCoinSetBalance( balance ) )
						BobbysStoryWorldPreference.putInt( Constants.PREF_PENDING_DOLE_COIN, balance );
				}
			}
		} );
	}

	public void backPress(View view) {
		if ( !mSoundPoolMap.containsKey( "tap.wav" ) )
			mSoundPoolMap.put( "tap.wav", Integer.valueOf( mSoundPool.load( this, R.raw.tap, 1 ) ) );
		else
			mSoundPool.play( mSoundPoolMap.get( "tap.wav" ).intValue(), Constants.DEFAULT_VOLUME_BACKGROUND_SOUND, Constants.DEFAULT_VOLUME_BACKGROUND_SOUND, 1, 0, 1.f );

		Util.hideSoftInput( this, view );
		onBackPressed();
	}

	private void initCameraView() {
		runOnUiThread( new Runnable() {
			@Override
			public void run() {
				View qrcodeContainer = findViewById( R.id.qrcode_container );
				mFocusView = findViewById( R.id.qrcode_center_focus );
				TextureView textureView = (TextureView) findViewById( R.id.qr_camera_preview );
				mQRCodeManager = new QRCodeManager( PortraitActivity.this, qrcodeContainer, textureView );
				final EditText directInputCode = (EditText) qrcodeContainer.findViewById( R.id.qrcode_direct_input );
				SurfaceTexture surfaceTexture = ( (TextureView) textureView ).getSurfaceTexture();
				if ( surfaceTexture == null ) {
					textureView.setSurfaceTextureListener( PortraitActivity.this );
					textureView.addOnLayoutChangeListener( new OnLayoutChangeListener() {
						@Override
						public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
							int width = right - left;
							int height = bottom - top;
							if ( mPreviewWidth != width || mPreviewHeight != height ) {
								mPreviewWidth = width;
								mPreviewHeight = height;
								mQRCodeManager.setTransformMatrix( width, height );
							}
						}
					} );
				}

				if ( mEntryPoint == ENTRY_MENU_DOLE_COIN_QRCODE ) {
					FontTextView tv = (FontTextView) qrcodeContainer.findViewById( R.id.qrcode_manual_text );
					if ( tv != null ) {
						String str = getResources().getString( R.string.pf_qrcode_scan_code );
						if ( str != null )
							tv.setText( Html.fromHtml( str ) );
					}
				}
			}
		} );
	}

	public void startCameraPreview() {
		runOnUiThread( new Runnable() {
			@Override
			public void run() {
				if ( mSurface == null ) {
					mPendingCameraOpen = true;
				} else {
					openCamera();
					if ( mQRCodeManager != null )
						mQRCodeManager.onResume();
				}
			}
		} );
	}

	public void stopCameraPreview() {
		closeCamera();

		if ( mQRCodeManager != null )
			mQRCodeManager.onPause();
	}

	private void openCamera() {
		mCamera = Camera.open();
		try {
			Camera.Parameters parameters = mCamera.getParameters();
			if ( parameters != null ) {
				List<Camera.Size> previewSizes = parameters.getSupportedPreviewSizes();
				Camera.Size previewSize = previewSizes.get( 0 );
				Camera.Size bestSize = null;

				List<Camera.Size> sizeList = mCamera.getParameters().getSupportedPreviewSizes();
				bestSize = sizeList.get( 0 );

				try {
					parameters.setPreviewSize( bestSize.width, bestSize.height );
					mSurfaceWidth = bestSize.width;
					mSurfaceHeight = bestSize.height;
					mCamera.setParameters( parameters );
				} catch ( Exception e ) {
					for ( int i = 1; i < sizeList.size(); i++ ) {
						try {
							parameters.setPreviewSize( sizeList.get( i ).width, sizeList.get( i ).height );
							mSurfaceWidth = sizeList.get( i ).width;
							mSurfaceHeight = sizeList.get( i ).height;
							mCamera.setParameters( parameters );
							break;
						} catch ( Exception ex ) {
						}
					}
				}

				// parameters.setPreviewSize(bestSize.width, bestSize.height); // commented
				// parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
				// setupPreviewAndPictureSize(parameters);
				// mCamera.setParameters(parameters); // commented

				/*
				 * for (int i = 1; i < previewSizes.size(); i++) {
				 * if ((previewSizes.get(i).width * previewSizes.get(i).height) > (previewSize.width * previewSize.height)) {
				 * previewSize = previewSizes.get(i);
				 * }
				 * }
				 * parameters.setPreviewSize(previewSize.width, previewSize.height);
				 * parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
				 * setupPreviewAndPictureSize(parameters);
				 * mCamera.setParameters(parameters);
				 */
			}
			mCamera.setPreviewTexture( mSurface );
			mCamera.startPreview();
			mIsPreviewing = true;
			mQRCodeManager.setCamera( mCamera );
			mQRCodeManager.initFocus( mFocusView );
		} catch ( IOException ioe ) {
			ioe.printStackTrace();
		}

		setupCameraParameters();
	}

	private void closeCamera() {
		if ( mCamera != null ) {
			mCamera.stopPreview();
			mIsPreviewing = false;
			mCamera.release();
			mCamera = null;
		}
		mPendingCameraOpen = false;
	}

	private void setupPreviewAndPictureSize(Parameters parameters) {
		mPreviewWidth = Integer.MAX_VALUE;
		mPreviewHeight = Integer.MAX_VALUE;
		boolean match = false;
		ArrayList<Size> tempList = new ArrayList<Size>();
		List<Size> previewList = parameters.getSupportedPreviewSizes();
		for ( Size previewSize : previewList ) {
			if ( previewSize.width == mSurfaceWidth && previewSize.height == mSurfaceHeight ) {
				match = true;
				break;
			}
		}
		if ( match ) {
			mPreviewWidth = mSurfaceWidth;
			mPreviewHeight = mSurfaceHeight;
		} else {
			final float ratio = mSurfaceHeight / (float) mSurfaceWidth;
			for ( Size previewSize : previewList ) {
				if ( previewSize.height / (float) previewSize.width == ratio )
					tempList.add( previewSize );
			}

			for ( Size item : tempList ) {
				if ( item.width >= mSurfaceWidth && item.height >= mSurfaceHeight ) {
					if ( item.width <= mPreviewWidth && item.height <= mPreviewHeight ) {
						mPreviewWidth = item.width;
						mPreviewHeight = item.height;
					}
				}
			}

			if ( mPreviewWidth == Integer.MAX_VALUE || mPreviewHeight == Integer.MAX_VALUE ) {
				for ( Size previewSize : previewList ) {
					if ( previewSize.width >= mSurfaceWidth && previewSize.height >= mSurfaceHeight ) {
						if ( previewSize.width <= mPreviewWidth && previewSize.height <= mPreviewHeight ) {
							mPreviewWidth = previewSize.width;
							mPreviewHeight = previewSize.height;
						}
					}
				}
			}
		}
		parameters.setPreviewSize( mPreviewWidth, mPreviewHeight );
		// parameters.setPreviewSize(1080, 1920);
	}

	private void setupCameraParameters() {
		if ( mCamera != null ) {
			// for now, consider all camera works with back face.
			int displayOrientation = getCameraDisplayOrientation( Camera.CameraInfo.CAMERA_FACING_BACK );
			mCamera.setDisplayOrientation( displayOrientation );
		}
	}

	private int getCameraDisplayOrientation(int cameraId) {
		Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
		Camera.getCameraInfo( cameraId, info );
		int rotation = getWindowManager().getDefaultDisplay().getRotation();
		int degrees = 0;
		switch ( rotation ) {
			case Surface.ROTATION_0:
				degrees = 0;
				break;
			case Surface.ROTATION_90:
				degrees = 90;
				break;
			case Surface.ROTATION_180:
				degrees = 180;
				break;
			case Surface.ROTATION_270:
				degrees = 270;
				break;
		}

		int result;
		if ( info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT ) {
			result = ( info.orientation + degrees ) % 360;
			result = ( 360 - result ) % 360; // compensate the mirror
		} else { // back-facing
			result = ( info.orientation - degrees + 360 ) % 360;
		}
		return result;
	}

	public boolean isPreviewing() {
		return mIsPreviewing;
	}

	public void finishApplication() {
		finish();
	}

	@Override
	public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
		mSurface = surface;
		mSurfaceWidth = Math.max( width, height );
		mSurfaceHeight = Math.min( width, height );
		if ( mPendingCameraOpen ) {
			mPendingCameraOpen = false;
			mQRCodeManager.setTransformMatrix( width, height );
			openCamera();
			if ( mQRCodeManager != null )
				mQRCodeManager.onResume();
		}
	}

	@Override
	public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
		mSurfaceWidth = width;
		mSurfaceHeight = height;
	}

	@Override
	public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
		mSurface = null;
		return true;
	}

	@Override
	public void onSurfaceTextureUpdated(SurfaceTexture surface) {
	}

	public native int nativeDoleCoinGetBalance();

	public native int nativeClearAuthData();

	private native void nativeSaveAuthData(String auth, int userNo);

	private native boolean nativeDoleCoinSetBalance(int balance);

	private native void nativeSyncData();

	private native boolean nativeShowGuideHome();

	private native void nativeHandleRecallType(int type);

	public native void nativeChangeBGMPlayStatus(int status);

	public native void nativePlayBGM();

	public native void nativeStopBGM();

	public native void nativeResumeBGM();

	public native void nativePauseBGM();

	public native void nativeResumeAudioEngine();

	public native void nativePauseAudioEngine();

	// @Override
	// public void onSuccess(ServerTask parser, Map<String, String> result) {
	// mIsAuth = true;
	// BobbysStoryWorldPreference.putString(Constants.PREF_AUTH_KEY,
	// result.get(Constants.TAG_AUTHKEY));
	// showMenu(mPendingEntryPoint);
	// }
	//
	// @Override
	// public void onFailed(ServerTask parser, Map<String, String> result,
	// int returnCode) {
	// showMenu(ENTRY_MENU_LOGIN);
	// }

	protected void onQRCodeDetected(final int balance, int addedCoin) {
		Cocos2dxHelper.runOnGLThread( new Runnable() {
			@Override
			public void run() {
				nativeDoleCoinSetBalance( balance );
			}
		} );
		onBackPressed();
		Bundle args = new Bundle();
		args.putInt( Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_coin );
		args.putInt( Constants.CUSTOM_DIALOG_TEXT_RES, R.string.pf_popup_received_dolecoin );
		args.putInt( Constants.CUSTOM_DIALOG_TEXT_RES_PARAM1, addedCoin );
		args.putInt( Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn );
		args.putBoolean( Constants.CUSTOM_DIALOG_ALLOW_CANCEL, true );

		CustomDialog dialog = new CustomDialog( this, args, null );
		dialog.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult( requestCode, resultCode, data );

		switch ( requestCode ) {
			case Session.DEFAULT_AUTHORIZE_ACTIVITY_CODE:
				android.util.Log.d( "", " Return from facebook login activity" );
				Session.getActiveSession().onActivityResult( PortraitActivity.this, requestCode, resultCode, data );
				break;
		}
	}
}
