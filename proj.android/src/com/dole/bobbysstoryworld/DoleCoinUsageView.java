package com.dole.bobbysstoryworld;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dole.bobbysstoryworld.server.GetPurchaseList;
import com.dole.bobbysstoryworld.server.ServerTask;
import com.dole.bobbysstoryworld.server.ServerTask.IServerResponseCallback;
import com.dole.bobbysstoryworld.ui.FontTextView;

public class DoleCoinUsageView extends RelativeLayout implements IServerResponseCallback {
	private static final String TAG = "DoleCoinUsageView";
	private static String[] limitedPeriodList = new String[] {"Rilla", "Lion", "Crocodile", "Cave", "The dock"};
	
	private class UsageHistory {
		private String coin;
		private String detail;
		private String date;
		
		public UsageHistory(String coin, String detail, String date) {
			this.coin = coin;
			this.detail = detail;
			this.date = date;
		}
	}
	
	List<UsageHistory> mHistory;
	
	private ListView mListView;
	private ProgressDialog mProgressDialog;

    public DoleCoinUsageView(Context context) {
        super(context);
    }

    public DoleCoinUsageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    
    public DoleCoinUsageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();

		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
		final int cacheSize = maxMemory / 8;
	    
		final Button helpButton = (Button)findViewById(R.id.usage_button);
		helpButton.setOnClickListener(mHelpClickListener);

		final FontTextView description = (FontTextView)findViewById(R.id.usage_info);
		if(description != null) {
			String descString = getResources().getString(R.string.pf_usagecoin_notice);
			if(descString != null)
				description.setText(Html.fromHtml(descString));
		}
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -6);
		Date begin = new Date(cal.getTimeInMillis());
		cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 1);
		Date end = new Date(cal.getTimeInMillis());
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		final HashMap<String, String> request = new HashMap<String, String>();
		request.put(Constants.TAG_USER_NO, String.valueOf(BobbysStoryWorldPreference.getInt(Constants.PREF_USER_NO, 0)));
		request.put(Constants.TAG_BEGIN_DATE, format.format(begin));
		request.put(Constants.TAG_END_DATE, format.format(end));
		request.put(Constants.TAG_PAGE_INDEX, String.valueOf(1));
		request.put(Constants.TAG_ROW_PER_PAGE, String.valueOf(30));
		request.put(Constants.TAG_CLIENT_IP, Util.getIp(getContext()));
		
		GetPurchaseList requestTask = new GetPurchaseList(getContext(), request);
		requestTask.setCallback(this);
		Model.runOnWorkerThread(requestTask);
		
		mProgressDialog = Util.openProgressDialog(getContext());
	}
	
	private OnClickListener mHelpClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
//			((PortraitActivity)getContext()).showMenu(PortraitActivity.ENTRY_MENU_HELP);
			((PortraitActivity)getContext()).showMenu(PortraitActivity.ENTRY_MENU_DOLE_COIN);
		}
	};

	private class HistoryAdapter extends BaseAdapter {
		@Override
		public int getCount() {
			if(mHistory == null)
				return 0;
			return mHistory.size();
		}

		@Override
		public Object getItem(int position) {
			if(mHistory == null)
				return null;
			return mHistory.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null) {
				final LayoutInflater inflater = LayoutInflater.from(getContext());
				convertView = inflater.inflate(R.layout.dole_coin_usage_item, parent, false);
			}
			
			Object tag = mHistory.get(position);
			if(tag instanceof UsageHistory) {
				UsageHistory info = (UsageHistory) tag;
				convertView.setTag(info);
				
				final TextView coinView = (TextView)convertView.findViewById(R.id.usage_item_coin);
				final TextView nameView = (TextView)convertView.findViewById(R.id.usage_item_name);
				final TextView periodView = (TextView)convertView.findViewById(R.id.usage_item_period);
				final TextView dateView = (TextView)convertView.findViewById(R.id.usage_item_date);
				
				Locale locale = getResources().getConfiguration().locale;
				String country = locale.getCountry();
				String dateText;
				try {
					Date date = new SimpleDateFormat("yyyy-MM-dd").parse(info.date);
					SimpleDateFormat format;
					if(country.equals(Constants.COUNTRY_KR) || country.equals(Constants.COUNTRY_JP))
						format = new SimpleDateFormat("yyyy.MM.dd");
					else
						format = new SimpleDateFormat("dd.MM.yyyy");
					dateText = format.format(date);
					
				} catch (ParseException e) {
					e.printStackTrace();
					dateText = info.date.substring(0, 10);
				}
				coinView.setText(info.coin);
				nameView.setText(info.detail);
				periodView.setText(R.string.pf_usagecoin_unlimited);
				for(int i = 0; i < limitedPeriodList.length; i++) {
					if(limitedPeriodList[i].equals(info.detail)) {
						periodView.setText(R.string.pf_usagecoin_limited_period);
						break;
					}
				}
				dateView.setText(dateText);
			}
			return convertView;
		}
	}
	
	@Override
	public void onSuccess(ServerTask parser, Map<String, String> result) {
		if(parser instanceof GetPurchaseList) {
			mHistory = new ArrayList<UsageHistory>();
			try {
				JSONArray array = new JSONArray(result.get(Constants.TAG_PURCHASE_LIST));
				for(int i = 0; i < array.length(); i++) {
					JSONObject cur = array.getJSONObject(i);
					UsageHistory history = new UsageHistory(cur.getString(Constants.TAG_PURCHASE_AMOUNT), 
							cur.getString(Constants.TAG_PRODUCT_NAME), cur.getString("RegisterDateTime"));
					mHistory.add(history);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			HistoryAdapter adapter = new HistoryAdapter();
			mListView = (ListView)findViewById(R.id.usage_list);
			mListView.setAdapter(adapter);
			mListView.setEmptyView(findViewById(R.id.usage_empty_list));
			
			if(mProgressDialog != null)
				mProgressDialog.dismiss();
		}
	}

	@Override
	public void onFailed(ServerTask parser, Map<String, String> result,
			int returnCode) {
		int failAmount = BobbysStoryWorldPreference.getInt(Constants.PREF_COIN_TO_ADDED, 0);
		failAmount += 100;
		BobbysStoryWorldPreference.putInt(Constants.PREF_COIN_TO_ADDED, failAmount);
		if(mProgressDialog != null)
			mProgressDialog.dismiss();
		
	}
}