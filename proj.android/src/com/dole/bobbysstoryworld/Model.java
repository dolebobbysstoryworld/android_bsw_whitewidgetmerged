package com.dole.bobbysstoryworld;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Process;
import android.support.v4.content.LocalBroadcastManager;

public class Model {
	private static final String TAG = Model.class.getName();
	
	private Context mContext;
//	private ArrayList<NickNameInfo> mNickList;
	private boolean mIsDbLoadFinished = false;
	private Bitmap mImportedImage = null;

    private static final HandlerThread sWorkerThread = new HandlerThread("bobbysworld-loader");
    static {
        sWorkerThread.start();
    }
    private static final Handler sWorker = new Handler(sWorkerThread.getLooper());
	
	public Model(Context context) {
		mContext = context;
	}

	public static void runOnMainThread(Activity activity, Runnable r) {
		activity.runOnUiThread(r);
	}
	
    public static void runOnWorkerThread(Runnable r) {
        if (sWorkerThread.getThreadId() == Process.myTid()) {
            r.run();
        } else {
            sWorker.post(r);
        }
    }
	
	public void loadDB() {
		runOnWorkerThread(new Runnable() {
			@Override
			public void run() {
//				mNickList = new ArrayList<NickNameInfo>();
//				Cursor c = mContext.getContentResolver().query(TableColumns.Nickname.URI, null, null, null, null);
//				while(c != null && c.moveToNext()) {
//					NickNameInfo nick = NickNameInfo.loadFromCursor(mContext, c);
//					mNickList.add(nick);
//				}
				dbLoadFinished();
			}
		});
	}
	
//	public void addNickName(NickNameInfo info) {
//		mNickList.add(info);
//	}
	
	private void dbLoadFinished() {
		//TODO 
		mIsDbLoadFinished = true;
		android.util.Log.e(TAG, "dbLoadFinished context : " + mContext);
		LocalBroadcastManager lbMan = LocalBroadcastManager.getInstance(mContext);
		lbMan.sendBroadcast(new Intent(Constants.ACTION_LOAD_FINISHED));
	}
	
	public boolean isDbLoadFinished() {
		return mIsDbLoadFinished;
	}
	
//	public static int deleteNicks(Context context, List<NickNameInfo> nickNameInfos) {
//		int deletedNickCount = 0;
//		for(NickNameInfo nickNameInfo : nickNameInfos) {
//			List<HeightInfo> heights = nickNameInfo.getHeightList();
//			if(heights != null) {
//				deleteHeightsByNick(context, nickNameInfo);
//			}
//
//			deletedNickCount += context.getContentResolver().delete(
//					TableColumns.Nickname.URI, 
//					TableColumns.Nickname._ID + "=?", 
//					new String[]{String.valueOf(nickNameInfo.getNickNameId())});
//		}
//		
//		if(deletedNickCount > 0) {
//			final Model model = ((ApplicationImpl)context.getApplicationContext()).getModel();
//			model.getNickList().removeAll(nickNameInfos);
//		}
//		
//		LocalBroadcastManager lbMan = LocalBroadcastManager.getInstance(context.getApplicationContext());
//		lbMan.sendBroadcast(new Intent(Constants.ACTION_DELETE_NICK_FINISHED));
//
//		return deletedNickCount;
//	}
	
//	public static int deleteHeightsByNick(Context context, NickNameInfo nickInfo) {
//		
//		int deletedCount = 0;
//		for(HeightInfo heightInfo : nickInfo.getHeightList()) {
//			// Delete image files
//			String fileName = heightInfo.getImagePath();
//			Util.removeFile(fileName + Storage.POSTFIX_LEFT + Storage.IMAGE_FACE_EXTENSION);
//			Util.removeFile(fileName + Storage.POSTFIX_RIGHT + Storage.IMAGE_FACE_EXTENSION);
//			Util.removeFile(fileName + Storage.POSTFIX_CHART + Storage.IMAGE_FACE_EXTENSION);
//			Util.removeFile(fileName + Storage.IMAGE_EXTENSION);
//		}
//
//		// Delete db
//		deletedCount = context.getContentResolver().delete(
//				TableColumns.Height.URI, 
//				TableColumns.Height.NICK_ID + "=?", 
//				new String[]{String.valueOf(nickInfo.mNickNameId)});
//		
//		return deletedCount;
//	}
//	
//	public static int deleteHeights(Context context, NickNameInfo nickInfo, List<HeightInfo> heightInfos) {
//		
//		int deletedCount = 0;
//		for(HeightInfo heightInfo : heightInfos) {
//			// Delete image files
//			String fileName = heightInfo.getImagePath();
//			Util.removeFile(fileName + Storage.POSTFIX_LEFT + Storage.IMAGE_FACE_EXTENSION);
//			Util.removeFile(fileName + Storage.POSTFIX_RIGHT + Storage.IMAGE_FACE_EXTENSION);
//			Util.removeFile(fileName + Storage.POSTFIX_CHART + Storage.IMAGE_FACE_EXTENSION);
//			Util.removeFile(fileName + Storage.IMAGE_EXTENSION);
//
//			// Delete db
//			deletedCount += context.getContentResolver().delete(
//					TableColumns.Height.URI, 
//					TableColumns.Height._ID + "=?", 
//					new String[]{String.valueOf(heightInfo.getHeightId())});
//		}
//		
//		if(deletedCount > 0) {
//			nickInfo.removeHeightInfos(heightInfos);
//		}
//		
//		return deletedCount;
//	}
	
//	public ArrayList<NickNameInfo> getNickList() {
//		return mNickList;
//	}
	
//	public NickNameInfo getNickNameInfoById(long nickId) {
//		if(mNickList == null)
//			return null;
//		
//		for(NickNameInfo info : mNickList) {
//			if(info.mNickNameId == nickId)
//				return info;
//		}
//
//		return null;
//	}
	
//	public HeightInfo getHeightInfoById(long heightId) {
//		if(mNickList == null)
//			return null;
//
//		for(NickNameInfo nickInfo : mNickList) {
//			final List<HeightInfo> heights = nickInfo.getHeightList();
//			if(heights == null)
//				continue;
//			
//			for(HeightInfo heightInfo : heights) {
//				if(heightInfo.mHeightId == heightId)
//					return heightInfo;
//			}
//		}
//		return null;
//	}
	
//	public int getMaxNonameIndex() {
//		if(mNickList == null)
//			return 0;
//
//		int maxId = 0;
//		final String nonamePattern = "Noname(\\([0-9]+\\))";
//		for(NickNameInfo nickInfo : mNickList) {
//			final String name = nickInfo.getNickName();
//			if(name.equals("Noname"))
//				maxId = 0;
//			else if(name.matches(nonamePattern)) {
//				String idxInStr = name.replaceAll(nonamePattern, "$");
//				int idx = Integer.valueOf(idxInStr);
//				maxId = Math.max(maxId, idx);
//			}
//		}
//		
//		return maxId + 1;
//	}
	
	public void setImportedImage(Bitmap image) {
		mImportedImage = image;
	}
	
	public Bitmap getImportedImage() {
		return mImportedImage;
	}
}