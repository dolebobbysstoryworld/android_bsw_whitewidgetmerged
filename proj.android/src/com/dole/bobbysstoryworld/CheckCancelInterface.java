package com.dole.bobbysstoryworld;

public interface CheckCancelInterface {
	public boolean onCheckCancel();
}