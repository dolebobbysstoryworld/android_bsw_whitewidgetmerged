package com.dole.bobbysstoryworld;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

public class AgreePersonalInfoView extends RelativeLayout {

	public static final String HIDE_CHECKBOX = "hide_checkbox";
	public static final String FROM_FACEBOOK_LOGIN = "from_facebook_login";
	private boolean mIsHideCheckButton = false;
	private boolean mIsFromFacebook = false;
	private boolean mAgreed = false;
	private String mCountry;

	// @Override
	// public View onCreateView(LayoutInflater inflater, ViewGroup container,
	// Bundle savedInstanceState) {
	//
	// final Bundle args = getArguments();
	// if(args != null) {
	// mIsHideCheckButton =
	// args.getBoolean(AgreePersonalInfoFragment.HIDE_CHECKBOX, false);
	// mIsFromFacebook =
	// args.getBoolean(AgreePersonalInfoFragment.FROM_FACEBOOK_LOGIN, false);
	// }
	//
	// int layoutId = R.layout.agree_personal_main;
	// if(mIsHideCheckButton) {
	// layoutId = R.layout.agree_personal_about;
	// }
	// return inflater.inflate(layoutId, null);
	// }

	public AgreePersonalInfoView(Context context) {
		super(context);
	}

	public AgreePersonalInfoView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public AgreePersonalInfoView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();

		Bundle data = ((PortraitActivity) getContext()).getSavedDataBundle();
		if (data != null) {
			mIsHideCheckButton = data.getBoolean(HIDE_CHECKBOX, false);
			mIsFromFacebook = data.getBoolean(FROM_FACEBOOK_LOGIN, false);
			mAgreed = data.getBoolean(
					UserDetailView.EXTRA_USER_DETAIL_MODE_TERMS_AGREED, false);
			mCountry = data.getString(Constants.TAG_STANDARD_COUNTRY_CODE, "");
			data.remove(HIDE_CHECKBOX);
			data.remove(FROM_FACEBOOK_LOGIN);
			data.remove(UserDetailView.EXTRA_USER_DETAIL_MODE_TERMS_AGREED);
		}

		String termsUrl = null;
		String privacyUrl = null;
		
		Log.d("AgreePersonalInfoView", "Country is: " + mCountry);
		
		if (mCountry.equals("SG") || mCountry.equals("AE")) {
			termsUrl = "terms_and_conditions_sg.html";
			privacyUrl = "privacy_policy_sg.html";
		} else if (mCountry.equals("NZ")) {
			termsUrl = "terms_and_conditions_nz.html";
			privacyUrl = "privacy_policy_nz.html";
		} else if (mCountry.equals("KR")) {
			termsUrl = "terms_and_conditions_ko.html";
			privacyUrl = "privacy_policy_ko.html";
		} else if (mCountry.equals("JP")) {
			termsUrl = "terms_and_conditions_ja.html";
			privacyUrl = "privacy_policy_ja.html";
		}else if (mCountry.equals("AU")) {
			termsUrl = "terms_and_conditions_au.html";
			privacyUrl = "privacy_policy_au.html";
		} else {
			termsUrl = "terms_and_conditions.html";
			privacyUrl = "privacy_policy.html";
		}
		
		final WebView termsConditionHtml = (WebView) findViewById(R.id.agree_personal_terms_condition_text);
		termsConditionHtml.getSettings().setJavaScriptEnabled(true);
		termsConditionHtml.loadUrl("file:///android_res/raw/" + termsUrl);

		final WebView privatePolicyHtml = (WebView) findViewById(R.id.agree_personal_policy_privacy_text);
		privatePolicyHtml.getSettings().setJavaScriptEnabled(true);
		privatePolicyHtml.loadUrl("file:///android_res/raw/" + privacyUrl);

		if (!mIsHideCheckButton) {
			final Button next = (Button) findViewById(R.id.agree_personal_btn_next);
			final CheckBox useTerm = (CheckBox) findViewById(R.id.agree_personal_use_term);
			final CheckBox infoTerm = (CheckBox) findViewById(R.id.sign_up_text_personal_info_term);
			final Button cancel = (Button) findViewById(R.id.agree_personal_btn_cancel);

			useTerm.setChecked(mAgreed);
			infoTerm.setChecked(mAgreed);
			if (mAgreed)
				next.setEnabled(true);
			else
				next.setEnabled(false);

			if (mIsFromFacebook) {
				next.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Bundle data = ((PortraitActivity) getContext())
								.getSavedDataBundle();
						data.putInt(UserDetailView.EXTRA_USER_DETAIL_MODE_NAME,
								UserDetailView.SIGN_UP_FACEBOOK);
						data.putBoolean(
								UserDetailView.EXTRA_USER_DETAIL_MODE_TERMS_AGREED,
								next.isEnabled());
						((PortraitActivity) getContext())
								.showMenu(PortraitActivity.ENTRY_MENU_SIGNUP);
						// ((PortraitActivity)getContext()).removeMenuStack(PortraitActivity.ENTRY_MENU_SIGNUP_TERMS);
					}
				});

				cancel.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						PortraitActivity activity = (PortraitActivity) getContext();
						activity.onBackPressed();
					}
				});
			} else {
				next.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// Fragment target = getTargetFragment();
						// if(target != null) {
						// Intent i = new Intent();
						// i.putExtra(Constants.CLICK_NEXT, true);
						// target.onActivityResult(Constants.REQUEST_CODE_AGREE_PERSONAL,
						// Activity.RESULT_OK, i);
						// getFragmentManager().popBackStack();
						// }
						PortraitActivity activity = (PortraitActivity) getContext();
						Bundle data = activity.getSavedDataBundle();
						data.putBoolean(
								UserDetailView.EXTRA_USER_DETAIL_MODE_TERMS_AGREED,
								true);
						activity.onBackPressed();
					}
				});

				cancel.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// Fragment target = getTargetFragment();
						// if(target != null) {
						// Intent i = new Intent();
						// i.putExtra(Constants.CLICK_NEXT, false);
						// target.onActivityResult(Constants.REQUEST_CODE_AGREE_PERSONAL,
						// Activity.RESULT_OK, i);
						// getFragmentManager().popBackStack();
						// }
						PortraitActivity activity = (PortraitActivity) getContext();
						Bundle data = activity.getSavedDataBundle();
						data.putBoolean(
								UserDetailView.EXTRA_USER_DETAIL_MODE_TERMS_AGREED,
								false);
						activity.onBackPressed();
					}
				});
			}

			useTerm.setOnCheckedChangeListener(mCheckedChangeListener);
			infoTerm.setOnCheckedChangeListener(mCheckedChangeListener);
		}

		FrameLayout guardPrivacyFrame = (FrameLayout) findViewById(R.id.sign_up_text_personal_info_term_privacy_frame);
		FrameLayout guardPromoFrame = (FrameLayout) findViewById(R.id.sign_up_text_personal_info_term_promo_frame);
		
		final CheckBox guardPrivacy = (CheckBox) findViewById(R.id.sign_up_text_personal_info_term_privacy);
		
		if(guardPrivacy != null){
			guardPrivacy.setOnCheckedChangeListener(mCheckedChangeListener);
		}
		
		final CheckBox guardPromo = (CheckBox) findViewById(R.id.sign_up_text_personal_info_term_promo);
		final CheckBox useTerm = (CheckBox) findViewById(R.id.agree_personal_use_term);
		final CheckBox infoTerm = (CheckBox) findViewById(R.id.sign_up_text_personal_info_term);
		
		if (mCountry.equalsIgnoreCase("AU")) {
			// modify for Australia
			termsConditionHtml.getLayoutParams().height = (int) TypedValue
					.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 150,
							getResources().getDisplayMetrics());
			privatePolicyHtml.getLayoutParams().height = (int) TypedValue
					.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 150,
							getResources().getDisplayMetrics());
			termsConditionHtml.requestLayout();
			privatePolicyHtml.requestLayout();
			
			if(guardPrivacyFrame != null){
				guardPrivacyFrame.setVisibility(View.VISIBLE);
			}
//			guardPromoFrame.setVisibility(View.VISIBLE);

			if(useTerm != null){
				useTerm.setText(getResources().getString(
						R.string.pf_signup_checkbox_title_guardian_info_overseas));
				useTerm.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 11);
			}
			if(infoTerm !=null){
				infoTerm.setText(getResources().getString(
						R.string.pf_signup_checkbox_title_guardian_promo));
				infoTerm.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 11);
			}
			
			if(guardPrivacy != null){
				guardPrivacy.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 9);
			}
			if(guardPromo != null){
				guardPromo.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 11);
			}
			
		}else{
			if(guardPrivacyFrame != null){
				guardPrivacyFrame.setVisibility(View.GONE);
			}
			
			if(guardPromoFrame != null){
				guardPromoFrame.setVisibility(View.GONE);
			}
			if(infoTerm != null){
				infoTerm.setText(getResources().getString(R.string.pf_signup_checkbox_title_agree_privacy));
			}
		}
		
		Log.i("", "wat: " + mCountry);
	}

	private OnCheckedChangeListener mCheckedChangeListener = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			Button next = (Button) findViewById(R.id.agree_personal_btn_next);
			CheckBox useTerm = (CheckBox) findViewById(R.id.agree_personal_use_term);
			CheckBox infoTerm = (CheckBox) findViewById(R.id.sign_up_text_personal_info_term);
			CheckBox guardPrivacy = (CheckBox) findViewById(R.id.sign_up_text_personal_info_term_privacy);
			
			if (mCountry.equalsIgnoreCase("AU")) {
				if (useTerm.isChecked() && infoTerm.isChecked() && guardPrivacy.isChecked())
					next.setEnabled(true);
				else
					next.setEnabled(false);
				
				if (buttonView == guardPrivacy) {
					final WebView privatePolicyHtml = (WebView) findViewById(R.id.agree_personal_policy_privacy_text);
					privatePolicyHtml.loadUrl("file:///android_res/raw/privacy_policy_au.html#TRANSNATIONALDISCLOSURE");
				}
				
			}else{
				if (useTerm.isChecked() && infoTerm.isChecked())
					next.setEnabled(true);
				else
					next.setEnabled(false);
			}
		}
	};
}