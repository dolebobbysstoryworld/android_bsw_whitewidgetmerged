package com.dole.bobbysstoryworld;

public class Constants {
	public static final String MIXPANEL_TOKEN = "d2a741b648a2db4f6e67543d2210dae2";
	public static final String GCM_SENDER_ID = "1002416322639";
	
	// Play Store Setting (Small APK with Expansion file)
	public static final boolean DEBUG = false;
	public static final boolean USE_LOCAL_OBB = true;
	
	// Client Testing Setting (Large APK)
//	public static final boolean DEBUG = false;
//	public static final boolean USE_LOCAL_OBB = false;
	
	public static final boolean USE_STAGING_SERVER = false;
//	public static final boolean DEBUG_KEEP_APP_FIRST_LAUNCH = false;
//	public static final boolean DEBUG_BYPASS_ALL_QRCODE = true;
	
	public static final String FAQ_URL = "http://www.doleapps.com";

	static final String SERVICE_CODE = "SVR004";
	static final String PAYMENT_METHOD_CODE = "PMC003";
	static final String PAYMENT_VALIDITY_PERIOD = "3650";
	static final String COUPON_NUMBER = "CP";
	static final String COUPON_TYPE = "T";
	public static final String OS_STRING = "Android";
	public static final String SNS_CODE = "SNS001";
	
//	public static final String NONAME = "No name";
	
	static final long UNAVAILABLE_ID = -1;
	
	public static final float DEFAULT_VOLUME_BACKGROUND_SOUND = 0.7f;

//	public enum Avatars {
//		DOLEKEY(0, R.drawable.add_nickname_center_monkey_01, R.string.dolkey),
//		RILLA(1, R.drawable.add_nickname_center_monkey_02, R.string.rilla),
//		TEENY(2, R.drawable.add_nickname_center_monkey_03, R.string.teeny),
//		COCO(3, R.drawable.add_nickname_center_monkey_04, R.string.coco),
//		PANZEE(4, R.drawable.add_nickname_center_monkey_05, R.string.panzee);
//		
//		private int index;
//		private int imageId;
//		private int nameId;
//		Avatars(int index, int imageResId, int nameResId) {
//			this.index = index;
//			this.imageId = imageResId;
//			this.nameId = nameResId;
//		}
//		
//		public int getIndex() {
//			return  index;
//		}
//		
//		public int getImageId() {
//			return imageId;
//		}
//		
//		public int getNameId() {
//			return nameId;
//		}
//	}
//
//	public enum Characters {
//		CHARACTER_DOLEKEY(0, R.color.name_tag_text_dolekey),
//		CHARACTER_RILLA(1, R.color.name_tag_text_rilla),
//		CHARACTER_TEENY(2, R.color.name_tag_text_smong),
//		CHARACTER_COCO(3, R.color.name_tag_text_faka),
//		CHARACTER_PANZEE(4, R.color.name_tag_text_panzee),
//		ADD_NICKNAME_OWL(5, 0),
//		CHARACTER_OWL(6, 0);
//		
//		private int index;
//		private int tagNameColor;
//		private Characters(int index, int tagNameColor) {
//			this.index = index;
//			this.tagNameColor = tagNameColor;
//		}
//		
//		public int getCharacterIndex() {
//			return index;
//		}
//		
//		public int getTagNameColor() {
//			return tagNameColor;
//		}
//				
//		static Characters getCharacterAt(int index) {
//			for(Characters character : Characters.values()) {
//				if(character.index == index)
//					return character;
//			}
//			return CHARACTER_DOLEKEY; //Default is first value
//		}
//	}
//
//	enum CouponType {
//		LION("A01"),
//		BIRD("A02"),
//		FOX("A03"),
//		ALLIGATOR("A04"),
//		RACCOON("A05"),
//		;
//
//		String type;
//		private CouponType(String t) {
//			type = t;
//		}
//		
//		public static CouponType getType(String typeStr) {
//			for(CouponType cType : values()) {
//				if(cType.type.equals(typeStr))
//					return cType;
//			}
//			return null;
//		}
//	}
//	
	public enum GenderType {
		MALE(1),
		FEMALE(2);
		
		int type;
		GenderType(int type) {
			this.type = type;
		}
	}
	
//	public static final String NICKNAME_GENDER_BOY = "boy";
//	public static final String NICKNAME_GENDER_GIRL = "girl";
	
	// LocalBroadCast
	public static final String ACTION_LOAD_FINISHED = "action_load_finished";
//	public static final String ACTION_ADD_NICK_FINISHED = "action_add_finished";
//	public static final String ACTION_DELETE_NICK_FINISHED = "action_delete_nick_finished";
//	public static final String ACTION_DELETE_HEIGHT_FINISHED = "action_dekete_height_finished";
	
	// Extra keys
//	public static final String NICKNAME_ID = "nickname_id";
//	public static final String HEIGHT_ID = "height_id";
//	public static final String HEIGHT = "height";
//	public static final String URI = "uri";
//	public static final String MODE = "mode";
//	public static final String IS_IMPORTED = "is_imported";
//	public static final String CLICK_NEXT = "click_next";
//	public static final String EVENT_URL = "event_url";
//	public static final String EMAIL = "email";
//	public static final String QRCODE = "qrcode";
//	public static final String PROMOTION_TYPE = "promotion_type";
//	public static final String IS_VIEWER_CONFIRM = "is_viewer_confirm";
//	public static final String FACEBOOK_USER_ID = "facebook_user_id";
//	public static final String REUSE_HEIGHT_ID = "reuse_height_id";
//	public static final String BYPASS_DETAIL_FRAG = "bypass_detail_frag";
	public static final String CITY = "city";
	
//	public static final int MAX_HEIGHT = 170;
//	public static final int MIN_HEIGHT = 50;
	
	//Requests through apps
	public static final int REQUEST_CODE_DATE_DIALOG = 1;
//	public static final int REQUEST_CODE_HEIGHT = 2;
//	public static final int REQUEST_PICK_ALBUM_PHOTO = 3;
//	public static final int REQUEST_IMPORT_IMAGE = 4;
	public static final int REQUEST_CODE_AGREE_PERSONAL = 5;
	public static final int REQUEST_USER_CONFIRM = 6;
//	public static final int REQUEST_DELETE_USER_INFO = 7;
//	public static final int REQUEST_UPDATE_NICK_INFO = 8;
//	public static final int REQUEST_FORGOTTEN_PASS_EMAIL = 9;
//	public static final int REQUEST_USER_NOTICE_FAILURE = 10;
//	public static final int REQUEST_USER_CONFIRM_SHARE = 11;
//	public static final int REQUEST_VIEW_GRAPH = 12;
//	public static final int REQUEST_SHARE_VIEWER = 13;
//	public static final int REQUEST_USER_NOTICE_REUSE_USED_CODE = 14;
//	public static final int REQUEST_DOLE_COIN = 15;
	public static final int REQUEST_CITY = 16;
//	public static final int REQUEST_CONFIRM_SHARE = 17;
//	public static final int REQUEST_DIALOG_UNAVAILABLE_CODE = 18;
	
	// Fragments
//	public static final String TAG_FRAGMENT_SPLASH = "frag_splash";
//	public static final String TAG_DIALOG_BIRTHDAY_PICKER = "dialog_birthday_picker";
//	public static final String TAG_DIALOG_ADD_NICK = "frag_dialog_choose_nick";	
//	public static final String TAG_DIALOG_GUIDE= "frag_dialog_guide";
//	public static final String TAG_DIALOG_CUSTOM = "frag_dialog_custom";	
//	public static final String TAG_DIALOG_CITY = "frag_dialog_city";	
	
//	public static final String TAG_FRAGMENT_LOGIN = "frag_login";
//	public static final String TAG_FRAGMENT_FORGOT_PASSWORD = "frag_forgot_password";
//	public static final String TAG_FRAGMENT_USER_DETAIL = "frag_user_detail";
//	public static final String TAG_FRAGMENT_FACEBOOK_LOGIN = "frag_facebook_login";
//	public static final String TAG_FRAGMENT_ADD_NICK = "frag_add_nick";
//	public static final String TAG_FRAGMENT_LOGIN_BEFORE_OPTION = "frag_login_before_option";
	
//	public static final String TAG_FRAGMENT_DETAIL = "frag_detail";
//	public static final String TAG_FRAGMENT_CHART = "frag_chart";
//	public static final String TAG_FRAGMENT_CAMERA = "frag_camera";
//	public static final String TAG_FRAGMENT_QRCODE = "frag_qrcode";
//	public static final String TAG_FRAGMENT_INPUT_HEIGHT = "frag_input_height";
//	public static final String TAG_FRAGMENT_IMPORT_PHOTO = "frag_import_photo";
//	public static final String TAG_FRAGMENT_ABOUT = "frag_about";
//	public static final String TAG_FRAGMENT_HELP = "frag_help";
//	public static final String TAG_FRAGMENT_DELETE_ACCOUNT = "frag_delete_account";
//	public static final String TAG_FRAGMENT_HEIGHT_VIEWER = "frag_height_viewer";
//	public static final String TAG_FRAGMENT_DOLE_COIN = "frag_dole_coin";
//	public static final String TAG_FRAGMENT_AGREE_PERSONAL_INFO = "frag_agree_personal_info";
//	public static final String TAG_FRAGMENT_INVITE_FACEBOOK = "frag_invite_facebook";
//	public static final String TAG_FRAGMENT_EVENT = "frag_event";
//	public static final String TAG_FRAGMENT_REQUEST_PAPER = "frag_request_paper";
	
	// Server api tags
	public static final String TAG_RETURN = "Return";
	public static final String TAG_RETURN_CODE = "ReturnCode";

	public static final String TAG_USER_ID = "UserID";
	public static final String TAG_PASSWORD = "Password";
	public static final String TAG_OLD_PASSWORD = "OldPassword";
	public static final String TAG_NEW_PASSWORD = "NewPassword";
	public static final String TAG_EMAIL = "Email";
	public static final String TAG_BIRTHDAY = "BirthDay";
	public static final String TAG_GENDER = "Gender";
	public static final String TAG_LANGUAGE = "Language";
	public static final String TAG_SNS_CODE = "SnsCode";
	public static final String TAG_SNS_ID = "SnsID";
	public static final String TAG_SNSID = "SNSID";
	public static final String TAG_SNS_USER_NAME = "SnsUserName";
	public static final String TAG_STANDARD_COUNTRY_CODE = "StandardCountryCode";
	public static final String TAG_UUID = "UUID";
	public static final String TAG_ADDRESS_MAIN = "AddressMain";
	public static final String TAG_CLIENT_IP = "ClientIP";
	public static final String TAG_USER_NO = "UserNo";
	public static final String TAG_AUTHKEY = "AuthKey";
	public static final String TAG_REASON = "Reason";
	public static final String TAG_COUNTRY = "Country";
	public static final String TAG_SNS_TYPE = "SNSType";
	public static final String TAG_SERVICE_CODE = "ServiceCode";
	public static final String TAG_MOBILE_OS = "MobileOS";
	public static final String TAG_NICKNAME = "Nickname";
	public static final String TAG_EMAIL_VALIFICATION = "EmailValification";
	public static final String TAG_EMAIL_ACTIVATE_DATE_TIME = "EmailActivateDatetime";
	public static final String TAG_CHANGE_PASSWROD_DATE_TIME = "ChangePasswrodDateTime";
	public static final String TAG_REGISTER_DATE_TIME = "RegisterDatetime";
	public static final String TAG_PROVINCES = "Provinces";
	public static final String TAG_MAJOR_VERSION = "MajorVersion";
	public static final String TAG_DEVICE_TOKEN = "DeviceToken";
	public static final String TAG_USER_TYPE = "UserType";
	public static final String TAG_SERIAL = "Serial";
	public static final String TAG_PROMOTION_INFO = "PromotionInfo";
	public static final String TAG_CAN_USE = "CanUse";
	public static final String TAG_PRODUCTI_NFOS = "ProductInfos";
	public static final String TAG_ORDER_ID = "OrderID";
	public static final String TAG_PAYMENT_METHOD_CODE = "PaymentMethodCode";
	public static final String TAG_PAYMENT_AMOUNT = "PaymentAmount";
	public static final String TAG_CHARGE_AMOUNT = "ChargeAmount";
	public static final String TAG_VALIDITY_PERIOD = "ValidityPeriod";
	public static final String TAG_DESCRIPTION = "Description";
	public static final String TAG_REAL_BALANCE = "RealBalance";
	public static final String TAG_EVENT_BALANCE = "EventBalance";
	public static final String TAG_APPROVAL_ID = "ApprovalID";
	public static final String TAG_CONTENT_TYPE = "ContentType";
	public static final String TAG_CONTENT_GROUP_NO = "ContentGroupNo";
	public static final String TAG_LANGUAGE_NO = "LanguageNo";
	public static final String TAG_CONTENT_GROUP_CLASS_NO = "ContentGroupClassNo";
	public static final String TAG_NOTICE_STATUS = "NoticeStatus";
	public static final String TAG_TOTAL_ROW_COUNT = "TotalRowCount";
	public static final String TAG_NOTICE_ITEMS = "NoticeItems";
	public static final String TAG_PAGE_INDEX = "PageIndex";
	public static final String TAG_ROW_PER_PAGE = "RowPerPage";
	public static final String TAG_IS_RECEIVE_EMAIL = "ISReceiveEmail";
	public static final String TAG_CATEGORY_NO = "CategoryNo";
	public static final String TAG_SUB_CATEGORY_NO = "SubCategoryNo";
	public static final String TAG_INQUIRY_TITLE = "InquiryTitle";
	public static final String TAG_INQUIRY_CONTENT = "InquiryContent";
	public static final String TAG_OCCURRENCE_DATE_TIME = "OccurrenceDateTime";
	public static final String TAG_CS_CATEGORY_ITEMS = "CSCategoryItems";
	public static final String TAG_SPLASH_ANIMATION = "SplashAnimation";
	public static final String TAG_BEGIN_DATE = "BeginDate";
	public static final String TAG_END_DATE = "EndDate";
	public static final String TAG_PRODUCT_NAME = "ProductName";
	public static final String TAG_PURCHASE_AMOUNT = "PurchaseAmount";
	public static final String TAG_PURCHASE_NO = "PurchaseNo";
	public static final String TAG_PURCHASE_QUANTITY = "PurchaseQuantity";
	public static final String TAG_PURCHASE_STATUS = "PurchaseStatus";
	public static final String TAG_PURCHASE_LIST = "PurchaseList";
	
	public static final String TAG_OPENED_SIGNUP = "Opened Sign-Up";
	public static final String TAG_INPUT_REGISTRATION = "Input Data on Registration Field";
	//public static final String TAG_REGISTRATION_TIME_EMAIL = "Registration Time Email";
	//public static final String TAG_REGISTRATION_TIME_FACEBOOK = "Registration Time Facebook";
	
	// Only used for Email; Facebook Uses "Direct Login"
	public static final String TAG_SUCCESSFULLY_CREATED_ACCOUNT = "Successfully Created Account";
	
	//public static final String TAG_SUCCESSFUL_EMAIL = "Successful Email Account Creation";
	//public static final String TAG_SUCCESSFUL_FACEBOOK = "Successful Facebook Account Creation";
	//public static final String TAG_COMPLETED_EMAIL = "Completed Email Account Profile";
	//public static final String TAG_COMPLETED_FACEBOOK = "Completed Facebook Profile";
	//public static final String TAG_AUTHENTICATED_EMAIL = "Authenticated Email";
	//public static final String TAG_AUTHENTICATED_FACEBOOK = "Authenticated Facebook";
	
	public static final String TAG_SUBMITTED_FEEDBACK = "Submitted Feedback";
	public static final String TAG_DIRECT_LOGIN = "Direct Login";
	
	// Preference keys
	public static final String PREF_IS_BGM_ON = "is_bgm_on_pref";
	public static final String PREF_IS_FIRST_APP_LAUNCH = "is_first_app_launch";
//	public static final String PREF_SAMPLE_QRCODE_USED = "sample_qrcode_used";
	public static final String PREF_USER_ID = TAG_USER_ID;
	public static final String PREF_USER_NO = TAG_USER_NO;
	public static final String PREF_AUTH_KEY = TAG_AUTHKEY;
	public static final String PREF_EMAIL_VALIFICATION = TAG_EMAIL_VALIFICATION;
	public static final String PREF_EMAIL_ACTIVATE_DATE_TIME = TAG_EMAIL_ACTIVATE_DATE_TIME;
	public static final String PREF_CHANGE_PASSWROD_DATE_TIME = TAG_CHANGE_PASSWROD_DATE_TIME;
	public static final String PREF_REGISTER_DATE_TIME = TAG_REGISTER_DATE_TIME;
	public static final String PREF_COUNTRY = TAG_COUNTRY;
	public static final String PREF_LANGUAGE = TAG_LANGUAGE;
	public static final String PREF_GENDER = TAG_GENDER;
	public static final String PREF_BIRTHDAY = TAG_BIRTHDAY;
	public static final String PREF_ADDRESS_MAIN = TAG_ADDRESS_MAIN;
	public static final String PREF_LOGIN_TYPE = "LoginType";
	public static final String PREF_LAST_DOLE_COIN = "LastDoleCoin";
	public static final String PREF_NOT_SHOW_EVENT = "NotShowEvent";
	public static final String PREF_COIN_TO_ADDED = "CoinToAdded";
	public static final String PREF_REQUEST_PAPER = "RequestPaper";
	public static final String PREF_PENDING_DOLE_COIN = "PendingDoleCoin";
	public static final String PREF_EXPANSION = "ExpansionPackageResource";
	public static final String PREF_PROMOTION_DATE = "PromotionDate";
	public static final String PREF_PENDING_GUIDE_HOME = "PendingGuideHome";
	public static final String PREF_EXPANSION_HASH_MAIN = "ExpansionHashMain";
	public static final String PREF_EXPANSION_MAIN_FILE = "ExpansionMainFile";
	public static final String PREF_LAST_VERSION = "LastVersion";

	//login type
	public static final int LOGIN_TYPE_EMAIL = 0;
	public static final int LOGIN_TYPE_FACEBOOK = 1;
	
	//return code from server
	public static final int ERROR_CODE_PARAM = 90001;	//파라미터 오류
	public static final int ERROR_CODE_NO_RECORD = 90002;	//해당 레코드 없음
	public static final int ERROR_CODE_EXIST_ID = 90101;	//user id 중복 오류
	public static final int ERROR_CODE_EXIST_EMAIL = 90102;	//이메일 중복 오류
	public static final int ERROR_CODE_NO_DATA = 90105;	//사용자데이터 없음
	public static final int ERROR_CODE_CHECK_PWD = 90106;	//비밀번호확인 오류
	public static final int ERROR_CODE_UNREGIST_SNS = 90107;	//등록되지 않은 SNS계정
	public static final int ERROR_CODE_USER_REG = 90108;	//사용자 등록 실패
	public static final int ERROR_CODE_USER_PROF_REG = 90109;	//사용자 프로필 등록 실패
	public static final int ERROR_CODE_USER_LV_REG = 90110;	//사용자 등급 등록 실패
	public static final int ERROR_CODE_SNS_REG = 90111;	//SNS 계정 등록 실패
	public static final int ERROR_CODE_SNS_ID = 90113;	//SNS 계정 등록 실패
	public static final int ERROR_CODE_FACEBOOK_ID = 90114;	//SNS 계정 등록 실패
	public static final int ERROR_CODE_ID_LEN = 90126;	//user id 길이 오류
	public static final int ERROR_CODE_PWD_LEN = 90127;	//password 길이 오류
	public static final int ERROR_CODE_EMAIL_UNAVAILABLE = 90137;	//탈퇴한 sns 계정
	public static final int ERROR_CODE_AUTHKEY_EXPIRED = 90202;	//인증키 만료
	public static final int ERROR_CODE_LOGIN_LIMIT = 90211;	//로그인 실패 횟수 초과
	public static final int ERROR_CODE_USELESS_AUTH_KEY = 90213;	//유효하지 않은 인증키
	public static final int ERROR_CODE_INVALID_AUTH_KEY = 90214;	//인증키 정보 불일치
	
	//custom dialog
	public static final String CUSTOM_DIALOG_IS_TITLE = "custom_dialog_is_title";
	public static final String CUSTOM_DIALOG_TITLE_RES = "custom_dialog_title_res";
	public static final String CUSTOM_DIALOG_IMG_RES = "custom_dialog_img_res";
	public static final String CUSTOM_DIALOG_TEXT_RES = "custom_dialog_text_res";
	public static final String CUSTOM_DIALOG_TEXT_RES_PARAM1 = "custom_dialog_text_res_param1";
	public static final String CUSTOM_DIALOG_TEXT_STRING = "custom_dialog_text_string";
	public static final String CUSTOM_DIALOG_LEFT_BUTTON_RES = "custom_dialog_left_button_res";
	public static final String CUSTOM_DIALOG_RIGHT_BUTTON_RES = "custom_dialog_right_button_res";
	public static final String CUSTOM_DIALOG_LEFT_BUTTON_TEXT_RES = "custom_dialog_left_button_text_res";
	public static final String CUSTOM_DIALOG_RIGHT_BUTTON_TEXT_RES = "custom_dialog_right_button_text_res";
	public static final String CUSTOM_DIALOG_CHANGE_TEXT_YES_NO = "custom_dialog_yes_no";
	public static final String CUSTOM_DIALOG_ALLOW_CANCEL = "custom_dialog_allow_cancel";
	public static final String CUSTOM_DIALOG_REQUEST_CODE = "custom_dialog_request_code";
	
	public static final int PASSWORD_MIN_LENGTH = 6;
	
	public static final String COUNTRY_KR = "KR";
//	public static final String COUNTRY_NZ = "NZ";
//	public static final String COUNTRY_PH = "PH";
	public static final String COUNTRY_JP = "JP";
//	public static final String COUNTRY_SG = "SG";

	public static final int IMAGE_TAKE_PICTURE_BACKGROUND			= 0x18000;
	public static final int IMAGE_CREATE_CHARACTER_FRAME_BACKGROUND	= 0x18001;

	protected static final int CC_APP_UPDATE_NOTICE = 0;
	protected static final int CC_APPUPDATE_BUTTON_TITLE_OK = 1;
	protected static final int PF_NET_WARNING_3G = 2;
	protected static final int CM_POPUP_BUTTON_TITLE_CANCEL = 3;
	protected static final int CM_POPUP_BUTTON_TITLE_OK = 4;
	protected static final int PF_NET_POPUP_WARNING_NETWORK_UNSTABLE = 5;
	protected static final int PF_NET_POPUP_NOTICCE_UPDATE_AND_RESTART = 6;
	protected static final int PF_NET_POPUP_NOTICCE_DOWNLOAD_RESOURCCE = 7;
	protected static final int CM_NET_WARNING_NETWORK_UNSTABLE = 8;
	protected static final int PF_NET_ERROR_EMAIL_ADDRESS = 9;
	protected static final int PF_NET_ERROR_EMAIL_FOR_SNS = 10;
	protected static final int PF_NET_ERROR__PASSWORD_INCORRECT = 11;
	protected static final int PF_NET_ERROR_EMAIL_ENTER_FULL_ADDRESS = 12;
	protected static final int PF_NET_ERROR_EMAIL_ALREADY_USE = 13;
	protected static final int PF_NET_ERROR__PASSWORD_SHORT = 14;
	protected static final int PF_NET_ERROR__QRCODE_INPUT = 15;
	protected static final int CC_EVENTNOTICCE_DISPLAY_A_DAY = 16;
	protected static final int CM_POPUP_BUTTON_TITLE_CLOSE = 17;
	protected static final int CC_HOME_BOOKTITLE_NEW = 18;
	protected static final int CC_HOME_MENUTITLE_SETTING = 19;
	protected static final int CC_HOME_POPUP_DELETE_STORY = 20;
	protected static final int CC_HOME_POPUP_NEED_LOGIN = 21;
	protected static final int CC_HOME_POPUP_NEED_LOGIN_BUTTON_TITLE_LOGIN = 22;
	protected static final int PF_DOLECOIN_ABOUTCOIN_Q = 23;
	protected static final int PF_DOLECOIN_HOWEARN_Q = 24;
	protected static final int PF_DOLECOIN_BUTTONTITLE_SCAN = 25;
	protected static final int PF_DOLECOIN_NEED_LOGIN = 26;
	protected static final int PF_POPUP_RECEIVED_DOLECOIN = 27;
	protected static final int PF_QRCODE_INPUT_CODE = 28;
	protected static final int PF_QRCODE_SCAN_CODE = 29;
	protected static final int PF_QRCODE_ERROR_USED = 30;
	protected static final int PF_QRCODE_ERROR_NETWORK_NOT_CONNCET = 31;
	protected static final int CC_LIST_CHARACTER_MAKE = 32;
	protected static final int CC_LIST_NEED_UNLOCK_COIN = 33;
	protected static final int CC_LIST_CHARACTER_WARNING_DELETE = 34;
	protected static final int CC_EXPANDEDITEM_NEED_UNLOCK_COIN = 35;
	protected static final int CC_EXPANDEDITEM_NEED_UNLOCK_COIN_1 = 36;
	protected static final int CC_EXPANDEDITEM_NEED_UNLOCK_COIN_2 = 37;
	protected static final int CC_EXPANDEDITEM_NEED_UNLOCK_COIN_LIMITED_PERIOD = 38;
	protected static final int CC_EXPANDEDITEM_NEED_UNLOCK_COIN_LIMITED_NUMBER = 39;
	protected static final int CC_EXPANDEDITEM_POPUP_WARNING_USE_UNLOCK_COIN = 40;
	protected static final int CC_EXPANDEDITEM_POPUP_COIN_BUTTON_TITLE_UNLOCK = 41;
	protected static final int CC_EXPANDEDITEM_POPUP_NOT_ENOUGH_COIN = 42;
	protected static final int CC_EXPANDEDITEM_POPUP_BUTTON_TITLE_EARN_COIN = 43;
	protected static final int CC_EXPANDEDITEM_POPUP_NEED_LOGIN = 44;
	protected static final int CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD = 45;
	protected static final int CC_EXPANDEDITEM_NOTICE_LIMITED_NUMBER = 46;
	protected static final int PF_CREATECHAR_BUTTON_TITLE_RETAKE = 47;
	protected static final int PF_CREATECHAR_BUTTON_TITLE_SAVE = 48;
	protected static final int PF_CREATECHAR_BUTTON_TITLE_CANCEL = 49;
	protected static final int CC_LIST_BACKGROUND_MAKE = 50;
	protected static final int CC_LIST_BACKGROUND_WARNING_DELETE = 51;
	protected static final int CC_EXPANDEDBG_NEED_UNLOCK_COIN = 52;
	protected static final int CC_EXPANDEDBG_NEED_UNLOCK_COIN_1 = 53;
	protected static final int CC_EXPANDEDBG_NEED_UNLOCK_COIN_2 = 54;
	protected static final int CC_EXPANDEDBG_NEED_UNLOCK_COIN_LIMITED_PERIOD = 55;
	protected static final int CC_EXPANDEDBG_NEED_UNLOCK_COIN_NUMBER = 56;
	protected static final int PF_LOGIN_TEXTFIELD_EMAIL = 57;
	protected static final int PF_LOGIN_TEXTFIELD_PASSWORD = 58;
	protected static final int PF_LOGIN_BUTTON_TITLE_LOGIN = 59;
	protected static final int PF_LOGIN_BUTTON_TITLE_FORGOT = 60;
	protected static final int PF_LOGIN_BUTTON_TITLE_SIGNUP = 61;
	protected static final int PF_LGOIN_BUTTON_FBLOGIN = 62;
	protected static final int PF_LOGIN_POPUP_ERROR_FBLOGIN = 63;
	protected static final int PF_LOGIN_BUTTON_TITLE_SKIP = 64;
	protected static final int PF_FORGOT_NOTICE_FORGOT_PASSWORD = 65;
	protected static final int PF_FORGOT_TEXTFIELD_EMAIL = 66;
	protected static final int PF_FORGOT_BUTTON_TITLE_SEND = 67;
	protected static final int PF_FORGOT_POPUP_CONFIRM_PASSWORD = 68;
	protected static final int PF_SIGNUP_TEXTFIELD_EMAIL = 69;
	protected static final int PF_SIGNUP_TEXTFIELD_PASSWORD = 70;
	protected static final int PF_SIGNUP_TEXTFIELD_PASSWORD_CONDITION = 71;
	protected static final int PF_SIGNUP_TEXTFIELD_PASSWORD_CONFIRM = 72;
	protected static final int PF_SIGNUP_TEXTFIELD_BIRTHYEAR = 73;
	protected static final int PF_SIGNUP_TEXTFIELD_CITY = 74;
	protected static final int PF_SIGNUP_RADIOTITLE_MALE = 75;
	protected static final int PF_SIGNUP_RADIOTITLE_FEMALE = 76;
	protected static final int PF_SIGNUP_CHECKBOX_TITLE_AGREE = 77;
	protected static final int PF_SIGNUP_BUTTON_TITLE_SIGNUP = 78;
	protected static final int PF_SIGNUP_POPUP_CANCEL_SIGNUP = 79;
	protected static final int PF_SIGNUP_POPUP_BUTTON_TITLE_OK = 80;
	protected static final int PF_SIGNUP_POPUP_BUTTON_TITLE_CANCEL = 81;
	protected static final int PF_SIGNUP_POPUP_TITLE_BIRTHYEAR = 82;
	protected static final int PF_SIGNUP_BUTTON_TITLE_SET = 83;
	protected static final int PF_SIGNUP_PUPUP_TITLE_SELECT_COUNTRY = 84;
	protected static final int PF_SIGNUP_COUNTRY_LIST_OTHER = 85;
	protected static final int PF_SIGNUP_PUPUP_TITLE_SELECT_CITY = 86;
	protected static final int PF_SIGNUP_CHECKBOX_TITLE_AGREE_TERMS = 87;
	protected static final int PF_SIGNUP_CHECKBOX_TITLE_AGREE_PRIVACY = 88;
	protected static final int PF_SIGNUP_BUTTON_TITLE_NEXT = 89;
	protected static final int CC_JOURNEYMAP_BUTTON_INTRO = 90;
	protected static final int CC_JOURNEYMAP_BUTTON_FINAL = 91;
	protected static final int CC_JOURNEYMAP_BOOKTITLE_DEFAULT = 92;
	protected static final int CC_JOURNEYMAP_SEPARATOR_BOOKNAME = 93;
	protected static final int CC_JOURNEYMAP_BOOKAUTHOR_DEFAULT = 94;
	protected static final int CC_JOURNEYMAP_BUTTON_TITLE_PLAY = 95;
	protected static final int CC_JOURNEYMAP_MENU_TITLE_DOWNLOADDEVICE = 96;
	protected static final int CC_JOURNEYMAP_MENU_TITLE_SHARE = 97;
	protected static final int CC_JOURNEYMAP_POPUP_NOTICCE_DOWNLOADDEVICE = 98;
	protected static final int CC_JOURNEYMAP_POPUP_NOTICE_SHARE = 99;
	protected static final int CC_JOURNEYMAP_POPUP_TITLE_SHARE_LIST = 100;
	protected static final int CC_JOURNEYMAP_POPUP_SHARELIST_FACEBOOK = 101;
	protected static final int CC_JOURNEYMAP_POPUP_SHARELIST_YOUTUBE = 102;
	protected static final int CC_JOURNEYMAP_POPUP_ERROR_SHARE = 103;
	protected static final int CC_JOURNEYMAP_POPUP_WARNING_DELETE_PAGE = 104;
	protected static final int CC_JOURNEYMAP_BUTTON_TITLE_CANCEL = 105;
	protected static final int CC_JOURNEYMAP_POPUP_WARNING_CANCEL_CONVERT = 106;
	protected static final int CC_JOURNEYMAP_POPUP_WARNING_NOTENOUGH_STORAGE = 107;
	protected static final int PF_SHARECOMMENT_POPUP_TITLE = 108;
	protected static final int PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_FACEBOOK = 109;
	protected static final int PF_SHARECOMMNET_POPUP_HASHTAG_FACEBOOK = 110;
	protected static final int PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_YOUTUBE = 111;
	protected static final int PF_SHARECOMMNET_POPUP_HASHTAG_YOUTUBE = 112;
	protected static final int CC_SETPOSITION_BUTTON_TITLE_RECORD = 113;
	protected static final int CC_SETPOSITION_POPUP_NOTICE_AUTOSELECT_LASTPAGE = 114;
	protected static final int CC_SETPOSITION_POPUP_WARNING_LIMITED_REDUCED = 115;
	protected static final int CC_SETPOSITION_POPUP_WARNING_ITEM_REUNLOCK = 116;
	protected static final int CC_SETPOSTION_POPUP_WARNING_DELETE_CHARACTER = 117;
	protected static final int CC_SETPOSTION_POPUP_WARNING_DELETE_BACKGROUND = 118;
	protected static final int CC_LIST_MUSIC_TITLE_NONE = 119;
	protected static final int CC_LIST_MUSIC_TITLE_JOLLY = 120;
	protected static final int CC_LIST_MUSIC_TITLE_DREAM = 121;
	protected static final int CC_LIST_MUSIC_TITLE_BRAVE = 122;
	protected static final int CC_LIST_MUSIC_TITLE_SHINE = 123;
	protected static final int CC_LIST_MUSIC_TITLE_BRISK = 124;
	protected static final int CC_LIST_MUSIC_TITLE_ANGRY = 125;
	protected static final int CC_LIST_MUSIC_TITLE_SLEEPY = 126;
	protected static final int CC_LIST_MUSIC_TITLE_ADVENTURE = 127;
	protected static final int CC_LIST_MUSIC_TITLE_CRUISE = 128;
	protected static final int CC_LIST_MUSIC_TITLE_JUNGLE = 129;
	protected static final int CC_RECORDING_BUTTON_TITLE_DONE = 130;
	protected static final int CC_RECORDING_POPUP_WARNING_CONTENT_WITHOUT_SAVING = 131;
	protected static final int CC_RECORDING_BUTTON_TITLE_RECODING = 132;
	protected static final int CC_RECORDING_BUTTON_TITLE_RERECORDING = 133;
	protected static final int CC_RECORDING_BUTTON_TITLE_SAVE = 134;
	protected static final int CC_RECORDING_POPUP_WARNING_RERECORDING = 135;
	protected static final int CC_RECORDING_POPUP_WARNING_STORY_WITHOUT_SAVING = 136;
	protected static final int CC_PLAY_PAGE_TITLE_INTRO = 137;
	protected static final int CC_PLAY_PAGE_TITLE_PAGE_N = 138;
	protected static final int CC_PLAY_PAGE_TITLE_FINAL = 139;
	protected static final int PF_SETTING_NOTICE_MORE_ADVANTAGE_LOGIN = 140;
	protected static final int PF_SETTING_BUTTON_TITLE_LOGIN = 141;
	protected static final int PF_SETTING_SOUND = 142;
	protected static final int PF_SETTING_BUTTON_TITLE_ABOUT = 143;
	protected static final int PF_SETTING_BUTTON_TITLE_HELP = 144;
	protected static final int PF_SETTING_BUTTON_TITLE_EDIT = 145;
	protected static final int PF_SETTING_BUTTON_TITLE_USAGE_COIN = 146;
	protected static final int PF_SETTING_BUTTON_TITLE_DELETE_ACCOUNT = 147;
	protected static final int PF_EDITACCOUNT_TEXTFIELD_CURRENT_PASSWORD = 148;
	protected static final int PF_EDITACCOUNT_TEXTFIELD_PASSWORD = 149;
	protected static final int PF_EDITACCOUNT_TEXTFIELD_PASSWORD_CONDITION = 150;
	protected static final int PF_EDITACCOUNT_TEXTFIELD_CONFIRM_PASSWORD = 151;
	protected static final int PF_EDITACCOUNT_BUTTON_TITLE_SAVE = 152;
	protected static final int PF_USAGECOIN_NOTICE = 153;
	protected static final int PF_USAGECOIN_LISTFIELD_DETAILS = 154;
	protected static final int PF_USAGECOIN_LISTFIELD_DATE = 155;
	protected static final int PF_USAGECOIN_BUTTON_TITLE_HELP = 156;
	protected static final int PF_USAGECOIN_NOTICE_NOHISTORY = 157;
	protected static final int PF_USAGECOIN_UNLIMITED = 158;
	protected static final int PF_USAGECOIN_LIMITED_PERIOD = 159;
	protected static final int PF_DELETEACCOUNT_NOTICE = 160;
	protected static final int PF_DELETEACCOUNT_CHECKBOX_CONFIRM = 161;
	protected static final int PF_DELETEACCOUNT_TEXTFIELD_CONFIRM_PASSWORD = 162;
	protected static final int PF_DELETEACCOUNT_BUTTON_TITLE_DELETE = 163;
	protected static final int PF_DELETEACCOUNT_TABLE_TITLEE_DOLECOIN = 164;
	protected static final int PF_DELETEACCOUNT_TABLE_TITLEE_BOBBY = 165;
	protected static final int PF_DELETEACCOUNT_TABLE_SUBTITLE_DOLECOIN = 166;
	protected static final int PF_DELETEACCOUNT_TABLE_SUBTITLE_BOBBY = 167;
	protected static final int PF_DELETEACCOUNT_TABLE_SUBTITLE_BOBBY_ADD = 168;
	protected static final int PF_ABOUT_APPTITLE = 169;
	protected static final int PF_ABOUT_SERVICE_INTRODUCE = 170;
	protected static final int PF_ABOUT_BUTTON_TITLE_TERMS = 171;
	protected static final int PF_ABOUT_TITLE_SERVICE = 172;
	protected static final int PF_ABOUT_TITLE_PRIVACY = 173;
	protected static final int PF_HELP_BUTTON_TITLE_FAQ = 174;
	protected static final int PF_HELP_TEXTFIELD_EMAIL = 175;
	protected static final int PF_HELP_TEXTFIELD_FEEDBACK = 176;
	protected static final int PF_HELP_DEVICE = 177;
	protected static final int PF_HELP_WARNING_PRIVACYINFO = 178;
	protected static final int PF_HELP_BUTTON_TITLE_SEND = 179;
	protected static final int CC_GUIDE_HOME = 180;
	protected static final int CC_GUIDE_JOURNEYMAP_NEWPAGE = 181;
	protected static final int CC_GUIDE_JOURNEYMAP_ALLPLAY = 182;
	protected static final int CC_GUIDE_SETPOSITION_SELECT_ITEM = 183;
	protected static final int CC_GUIDE_SETPOSITION_SELECT_BACKGROUND = 184;
	protected static final int CC_GUIDE_SETPOSITION_SELECT_MUSIC = 185;
	protected static final int CC_GUIDE_SETPOSITION_DELETE_ITEM = 186;
	protected static final int CC_GUIDE_SETPOSITION_RECORDING = 187;
	protected static final int PF_GUIDE_EDIT_PICTURE = 188;
	protected static final int PF_GUIDE_LOGIN_SIGNUP = 189;
	protected static final int PF_GUIDE_LOGIN_EARNCOIN = 190;
	protected static final int PF_GUIDE_LOGIN_GETITEM = 191;
	protected static final int PF_GUIDE_LOGIN_SHARE = 192;
	protected static final int PF_LOGIN_POPUP_ERROR_NOTAVAILABLE_EMAIL = 193;
	protected static final int CC_HOME_POPUP_UNLOCK_ITEM_MESSAGE = 194;
	protected static final int CC_JOURNEYMAP__DOWNLOAD_DEVICE_COMPLETE = 195;
	protected static final int CC_JOURNEYMAP__SHARE_COMPLETE = 196;
	protected static final int CC_SETPOSITION_POPUP_WARNING_ITEM_FULL = 197;
	protected static final int PF_EDITACCOUNT_POPUP_INPUT_PASSWORD = 198;
	protected static final int PF_EDITACCOUNT_POPUP_SAVE_COMPLETE = 199;
	protected static final int PF_HELP_POPUP_SEND_COMPLETE = 200;
	protected static final int PF_DELETEACCOUNT_DELETE_COMPLETE = 201;
	protected static final int PF_PROMOTION_CHECKBOX = 202;
	protected static final int PF_DOLECOIN_ABOUTCOIN_A = 203;
	protected static final int PF_DOLECOIN_HOWEARN_A = 204;
	protected static final int CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_DAYS = 205;
	protected static final int CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_HOURS = 206;
	protected static final int CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_HOUR = 207;
	protected static final int CC_JOURNEYMAP_SHARE_PROGRESS_NOTICE = 208;
	protected static final int CC_RECORDING_BUTTON_TITLE_RECOD = 209;
	protected static final int CC_EXPANDEDITEM_NEED_UNLOCK_COIN_CHA = 210;
	protected static final int CC_POPUP_CANCEL_SHARE = 211;
	protected static final int PF_SIGNUP_POPUP_BUTTON_TITLE_OK_SIGN_UP = 212;
	protected static final int CC_PRLOAD_BOOKTITLE_1 = 213;
	protected static final int CC_PRLOAD_BOOKTITLE_2 = 214;
	protected static final int CM_POPUP_BUTTON_TITLE_YES = 215;
	protected static final int CM_POPUP_BUTTON_TITLE_NO = 216;
	protected static final int CF_NET_ERROR_EMAIL_NOT_AVAILABLE = 217;
}