package com.dole.bobbysstoryworld;

import java.util.Locale;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.dole.bobbysstoryworld.ui.CustomDialog;
import com.dole.bobbysstoryworld.ui.CustomDialog.CustomDialogButtonClickListener;

public class DoleCoinView extends FrameLayout {
	private static final String TAG = DoleCoinView.class.getName();
//	private RequestAuthSessionStatusCallback mSessionCallback = new RequestAuthSessionStatusCallback();
	
//	private static final List<String> mFriendsPermission = new ArrayList<String>() {
//        {
//            add("user_friends");
//            add("public_profile");
//        }
//    };

    public DoleCoinView(Context context) {
        super(context);
    }

    public DoleCoinView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    
    public DoleCoinView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();

		TextView coinNum = (TextView)findViewById(R.id.dole_coin_number);
		ImageView coinBefor = (ImageView)findViewById(R.id.dole_coin_before_login);
//		Button facebookInviteButton = (Button)findViewById(R.id.dole_coin_invite_facebook);
		Button qrCodeButton = (Button)findViewById(R.id.dole_coin_scan_qrcode);
		TextView howearn = (TextView)findViewById(R.id.coin_earn_a);
		
		Locale locale = getResources().getConfiguration().locale;
		if(locale.getLanguage().equals(Locale.KOREAN.getLanguage()) || locale.getLanguage().equals(Locale.JAPANESE.getLanguage())) {
			howearn.setGravity(Gravity.CENTER);
			howearn.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.dole_coin_howearn_text_size));
		} else {
			howearn.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
			howearn.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.dole_coin_howearn_text_size_en));
		}
		
		int userNo = BobbysStoryWorldPreference.getInt(Constants.PREF_USER_NO, 0);
		if(userNo > 0) {
			int doleCoin = ((PortraitActivity)getContext()).nativeDoleCoinGetBalance();
			BobbysStoryWorldPreference.putInt(Constants.PREF_LAST_DOLE_COIN, doleCoin); 
			coinBefor.setVisibility(View.GONE);
			String coinText = String.valueOf(doleCoin);
			android.util.Log.e(TAG, "coinText:"+coinText);
			int len = coinText.length();
			int textSize;
			android.util.Log.e(TAG, "len:"+len);
			switch(len) {
			case 1:
			case 2:
				textSize = getResources().getDimensionPixelSize(R.dimen.dole_coin_size_length_1_2);
				break;
			case 3:
				textSize = getResources().getDimensionPixelSize(R.dimen.dole_coin_size_length_3);
				break;
			case 4:
				textSize = getResources().getDimensionPixelSize(R.dimen.dole_coin_size_length_4);
				break;
			case 5:
			default:
				textSize = getResources().getDimensionPixelSize(R.dimen.dole_coin_size_length_5);
				break;
			}
			android.util.Log.e(TAG, "textSize:"+textSize);
			coinNum.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
			coinNum.setText(coinText);
		} else {
			coinNum.setVisibility(View.GONE);
		}
		
//		facebookInviteButton.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View view) {
//				((PortraitActivity)getContext()).showMenu(PortraitActivity.ENTRY_MENU_DOLE_COIN_USAGE);
//			}
//		});

		qrCodeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if(isLoggedIn()) {
				((PortraitActivity)getContext()).showMenu(PortraitActivity.ENTRY_MENU_DOLE_COIN_QRCODE);
				} else {
					Bundle args = new Bundle();
					args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
					args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.pf_dolecoin_need_login);
					args.putInt(Constants.CUSTOM_DIALOG_LEFT_BUTTON_RES, R.drawable.popup_cancel_btn);
					args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
					args.putInt(Constants.CUSTOM_DIALOG_LEFT_BUTTON_TEXT_RES, R.string.popup_back);
					args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_TEXT_RES, R.string.popup_login);
					args.putBoolean(Constants.CUSTOM_DIALOG_ALLOW_CANCEL, true);

					CustomDialog dialog = new CustomDialog(getContext(), args, new CustomDialogButtonClickListener() {
						@Override
						public void onClickOK(int requestCode) {
							((PortraitActivity)getContext()).showMenu(PortraitActivity.ENTRY_MENU_DOLE_COIN_QRCODE);
						}

						@Override
						public void onClickCancel(int requestCode) {
							// do nothing
						}
					});
					dialog.show();
				}
			}
		});
	}
	
	private boolean isLoggedIn() {
		if(BobbysStoryWorldPreference.getString(Constants.PREF_AUTH_KEY, null) != null)
			return true;
		else
			return false;
	}

//	private boolean ensureOpenSession() {
//		android.util.Log.e(TAG, "ensureOpenSession:");
//        if (Session.getActiveSession() == null ||
//                !Session.getActiveSession().isOpened()) {
//            Session.openActiveSession(
//            		(PortraitActivity)getContext(),
//                    true, 
//                    Arrays.asList("user_friends"),
//                    mSessionCallback);
//            return false;
//        }
//        return true;
//    }
//	
//	private class RequestAuthSessionStatusCallback implements Session.StatusCallback {
//        @Override
//        public void call(Session session, SessionState state, Exception exception) {
//        	android.util.Log.d(TAG, " - session state = " + state);
//        	
//        	onSessionStateChanged(session, state, exception);
//        	
//        	if(state.isOpened())
//        		session.removeCallback(this);
//        }
//    }
//	
//	private boolean sessionHasNecessaryPerms(Session session) {
//        if (session != null && session.getPermissions() != null) {
//            for (String requestedPerm : mFriendsPermission) {
//                if (!session.getPermissions().contains(requestedPerm)) {
//                    return false;
//                }
//            }
//            return true;
//        }
//        return false;
//    }
//	
//	private List<String> getMissingPermissions(Session session) {
//        List<String> missingPerms = new ArrayList<String>(mFriendsPermission);
//        if (session != null && session.getPermissions() != null) {
//            for (String requestedPerm : mFriendsPermission) {
//                if (session.getPermissions().contains(requestedPerm)) {
//                    missingPerms.remove(requestedPerm);
//                }
//            }
//        }
//        return missingPerms;
//    }
//	
//	private void onSessionStateChanged(Session session, SessionState state, Exception exception) {
//		 if (state.isOpened() && !sessionHasNecessaryPerms(session)) {
//			 session.requestNewReadPermissions(
//                     new NewPermissionsRequest(
//                    		 (PortraitActivity)getContext(), 
//                             getMissingPermissions(session)));
//		 } else if(state.isOpened()){
//			 requestFriendsList(session);
//		 }
//	}
//	
//	private void requestFriendsList(final Session session) {
//		((PortraitActivity)getContext()).showMenu(PortraitActivity.ENTRY_MENU_INVITE_FACEBOOK);
//	}
	
//	@Override
//	public void onActivityResult(int requestCode, int resultCode, Intent data) {
//		super.onActivityResult(requestCode, resultCode, data);
//
//		android.util.Log.d(TAG, " requestCode = " + requestCode + " resultCode = " + resultCode + " data = " + data);
//		switch (requestCode) {
//		case Session.DEFAULT_AUTHORIZE_ACTIVITY_CODE:
//			android.util.Log.d(TAG, " Return from facebook login activity");
//            Session.getActiveSession().onActivityResult(getContext(), requestCode, resultCode, data);
//            break;
//		default:
//			break;
//		}
//	}
}