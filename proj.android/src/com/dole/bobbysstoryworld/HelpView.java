package com.dole.bobbysstoryworld;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.dole.bobbysstoryworld.server.RegisterCS;
import com.dole.bobbysstoryworld.server.ServerTask;
import com.dole.bobbysstoryworld.server.ServerTask.IServerResponseCallback;
import com.dole.bobbysstoryworld.ui.FontEditText;
import com.dole.bobbysstoryworld.ui.FontTextView;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

public class HelpView extends RelativeLayout implements IServerResponseCallback {
	
	private RegisterCS mRegisterCS;
	private boolean mIsEmailFill = false;
	private boolean mIsQuestionFill = false;
	private ProgressDialog mProgressDialog;
	
	public HelpView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public HelpView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public HelpView(Context context) {
		this(context, null);
	}
	
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		
		TextView device = (TextView)findViewById(R.id.help_device);
		TextView topic = (TextView)findViewById(R.id.help_topic);
		final Button send = (Button)findViewById(R.id.help_send);
		final FontEditText email = (FontEditText)findViewById(R.id.help_email);
		final TextView question = (TextView)findViewById(R.id.help_question);
		
		String deviceText = device.getText().toString() + " : Android OS / " + Build.MODEL;
		device.setText(deviceText);
		send.setEnabled(false);
		send.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				sendCS();
			}
		});
		
		email.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				FontTextView errorView = (FontTextView)findViewById(R.id.help_email_error);
				errorView.setVisibility(View.GONE);
				String st = s.toString();
				if(st.trim().equals(""))
					mIsEmailFill = false;
				else
					mIsEmailFill = true;
				
				if(mIsEmailFill && mIsQuestionFill)
					send.setEnabled(true);
				else
					send.setEnabled(false);
			}
		});
		
		email.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				FontTextView errorView = (FontTextView)findViewById(R.id.help_email_error);
				errorView.setVisibility(View.GONE);
				return false;
			}
		});
		
		email.setOnImeHideListener(new FontEditText.ImeHideListener() {
			@Override
			public void onImeHide() {
				String emailAdd = email.getText().toString();
				if(!Util.checkEmail(emailAdd)) {
					FontTextView errorView = (FontTextView)findViewById(R.id.help_email_error);
					errorView.setText(R.string.pf_net_error_email_enter_full_address);
					errorView.setVisibility(View.VISIBLE);
				}
			}
		});
		
		email.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(!hasFocus) {
					String emailAdd = email.getText().toString();
					if(!Util.checkEmail(emailAdd)) {
						FontTextView errorView = (FontTextView)findViewById(R.id.help_email_error);
						errorView.setText(R.string.pf_net_error_email_enter_full_address);
						errorView.setVisibility(View.VISIBLE);
					}
				}
			}
		});
		
		email.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId != EditorInfo.IME_NULL || (actionId == EditorInfo.IME_NULL && event != null && event.getAction() == KeyEvent.ACTION_DOWN)) {
					String emailAdd = email.getText().toString();
					if(!Util.checkEmail(emailAdd)) {
						FontTextView errorView = (FontTextView)findViewById(R.id.help_email_error);
						errorView.setText(R.string.pf_net_error_email_enter_full_address);
						errorView.setVisibility(View.VISIBLE);
						return true;
					}
				}
				return false;
			}
		});
		
		question.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				String st = s.toString();
				if(st.trim().equals(""))
					mIsQuestionFill = false;
				else
					mIsQuestionFill = true;
				
				if(mIsEmailFill && mIsQuestionFill)
					send.setEnabled(true);
				else
					send.setEnabled(false);
			}
		});
		
		topic.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent faq = new Intent();
				faq.setAction(Intent.ACTION_VIEW);
				faq.setData(Uri.parse(Constants.FAQ_URL));
				getContext().startActivity(faq);
				
				MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext() , Constants.MIXPANEL_TOKEN);
		        mixpanel.track("Opened FAQ", null);
			}
		});

		String userId = BobbysStoryWorldPreference.getString(Constants.PREF_USER_ID, "");
		if(!userId.equals(""))
			email.setText(userId);
		
		setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent e) {
				Util.hideSoftInput(getContext(), v);
				email.clearFocus();
				question.clearFocus();
				return false;
			}
		});
	}
	
	private void sendCS() {
		TextView email = (TextView)findViewById(R.id.help_email);
		TextView question = (TextView)findViewById(R.id.help_question);
		TextView device = (TextView)findViewById(R.id.help_device);
		Date date = new Date();
		
		String emailAdd = email.getText().toString();
		if(!Util.checkEmail(emailAdd)) {
			FontTextView errorView = (FontTextView)findViewById(R.id.help_email_error);
			errorView.setText(R.string.pf_net_error_email_enter_full_address);
			errorView.setVisibility(View.VISIBLE);
			return;
		}
		
		HashMap<String, String> request = new HashMap<String, String>();
		request.put(Constants.TAG_USER_NO, String.valueOf(BobbysStoryWorldPreference.getInt(Constants.PREF_USER_NO, 0)));
		request.put(Constants.TAG_USER_ID, BobbysStoryWorldPreference.getString(Constants.PREF_USER_ID, emailAdd));
		request.put(Constants.TAG_EMAIL, emailAdd);
		request.put(Constants.TAG_IS_RECEIVE_EMAIL, String.valueOf(true));
		request.put(Constants.TAG_CATEGORY_NO, String.valueOf(41));
		request.put(Constants.TAG_SUB_CATEGORY_NO, String.valueOf(167));
		request.put(Constants.TAG_INQUIRY_TITLE, "Bobbys story world - android");
		request.put(Constants.TAG_INQUIRY_CONTENT, question.getText().toString());
		request.put(Constants.TAG_SERVICE_CODE, Constants.SERVICE_CODE);
		request.put(Constants.TAG_OCCURRENCE_DATE_TIME, date.toString());
		request.put(Constants.TAG_DESCRIPTION, device.getText().toString());
		request.put(Constants.TAG_CLIENT_IP, Util.getIp(getContext()));
		
		mRegisterCS = new RegisterCS(getContext(), request);
		mRegisterCS.setCallback(this);
		Model.runOnWorkerThread(mRegisterCS);
		
		mProgressDialog = Util.openProgressDialog(getContext());
	}

	@Override
	public void onSuccess(ServerTask parser, Map<String, String> result) {
		Toast.makeText(getContext(), getResources().getString(R.string.pf_help_popup_send_complete), Toast.LENGTH_SHORT).show();
		((PortraitActivity)getContext()).onBackPressed();
		mProgressDialog.dismiss();
		
		//MixpanelAPI SUBMITTED FEEDBACK
		MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(), Constants.MIXPANEL_TOKEN);
		mixpanel.track(Constants.TAG_SUBMITTED_FEEDBACK, null);
		mixpanel.flush();
	}

	@Override
	public void onFailed(ServerTask parser, Map<String, String> result, int returnCode) {
		Toast.makeText(getContext(), Util.switchErrorCode(returnCode, getResources()), Toast.LENGTH_SHORT).show();
		mProgressDialog.dismiss();
	}
}
