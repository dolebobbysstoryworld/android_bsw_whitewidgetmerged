package com.dole.bobbysstoryworld;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.dole.bobbysstoryworld.GuideDialog.GuideMode;
import com.dole.bobbysstoryworld.server.GetBalance;
import com.dole.bobbysstoryworld.server.GetMobileProfile;
import com.dole.bobbysstoryworld.server.RequestAuthKey;
import com.dole.bobbysstoryworld.server.RequestSNSLogin;
import com.dole.bobbysstoryworld.server.ServerTask;
import com.dole.bobbysstoryworld.server.ServerTask.IServerResponseCallback;
import com.dole.bobbysstoryworld.ui.FontEditText;
import com.dole.bobbysstoryworld.ui.FontTextView;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

public class LoginView extends RelativeLayout implements IServerResponseCallback {
	private static final String TAG = "LoginView";
	
	public static final String FORGOT_EMAIL_ID = "FORGOT_EMAIL_ID";
	
	private GetMobileProfile mGetMobileProfile;
	private RequestAuthKey mRequestAuthKey;
	private String mTempUserId;
	private String mTempUserNo;
	private String mTempAuthKey;
	private String mTempCountry;
	private String mTempCity;
	private String mTempLanguage;
	private int mTempGender;
	private String mTempBirth;
	private String mTempName;
	private String mTempFacebookId;
	private boolean mIsFirst;
	private boolean mIsEmailFill = false;
	private boolean mIsPasswordFill = false;
	
	private boolean mIsFacebookLogin = false;
	private boolean mPendingSaveAuthData = false;
	
	private String mForgottenPassEmail = null;
	private ProgressDialog mProgressDialog;
	
	private List<String> facebookPermissions = Arrays.asList(
			"email",
			"user_birthday",
			"user_location"
			);
	
	private LoginSessionStatusCallback mFacebookLoginStatusCallback = new LoginSessionStatusCallback();
	
	public LoginView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public LoginView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public LoginView(Context context) {
		this(context, null);
	}

	@Override
	protected void onDetachedFromWindow() {
		if(mFacebookLoginStatusCallback != null)
			mFacebookLoginStatusCallback.mIsAlive = false;
		super.onDetachedFromWindow();
	}
	
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		
		int userNo = BobbysStoryWorldPreference.getInt(Constants.PREF_USER_NO, -1);
		if(userNo > 0)
			((PortraitActivity)getContext()).onBackPressed();
		
		final View linkLoginForgot = findViewById(R.id.login_link_forgot_password);
		final View linkSignUp = findViewById(R.id.login_link_sign_up);
		final View linkFacebookLogin = findViewById(R.id.login_link_login_facebook);
		final View loginSkip = findViewById(R.id.login_skip);
		final View loginBtn = findViewById(R.id.login_send_btn);
		final View backBtn = findViewById(R.id.common_btn_cancel);
		final FontEditText emailView = (FontEditText)findViewById(R.id.login_edit_email);
		final FontEditText pwdView = (FontEditText)findViewById(R.id.login_edit_password);
		
		Bundle data = ((PortraitActivity)getContext()).getSavedDataBundle();
		if(data != null) {
			String email = data.getString(FORGOT_EMAIL_ID, "");
			data.remove(FORGOT_EMAIL_ID);
			emailView.setText(email);
			if(email.trim().equals(""))
				mIsEmailFill = false;
			else
				mIsEmailFill = true;
		}
		
		linkLoginForgot.setOnClickListener(mViewOnclickListener);
		linkSignUp.setOnClickListener(mViewOnclickListener);
		linkFacebookLogin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Session session = Session.getActiveSession();
				if(session != null && !session.isOpened()) {
					session.closeAndClearTokenInformation();
					Session.setActiveSession(null);
					post(new Runnable() {
						@Override
						public void run() {
							signInWithFacebook();							
						}
					});
					return;
				}
				signInWithFacebook();
				// MixpanelAPI Login calls
				
				MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(),
						Constants.MIXPANEL_TOKEN);
				mixpanel.track(Constants.TAG_OPENED_SIGNUP, null);
				//mixpanel.timeEvent(Constants.TAG_REGISTRATION_TIME_FACEBOOK);
				mixpanel.flush();
			}
		});
		
		mIsFirst = BobbysStoryWorldPreference.getBoolean(Constants.PREF_IS_FIRST_APP_LAUNCH, true);
		if(mIsFirst) {
			loginSkip.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// No more login after first launch
					BobbysStoryWorldPreference.putBoolean(Constants.PREF_IS_FIRST_APP_LAUNCH, false);
					((PortraitActivity)getContext()).onBackPressed();
				}
			});
			backBtn.setVisibility(View.GONE);
		} else {
			loginSkip.setVisibility(View.GONE);
		}
		
		boolean isGuideShown = BobbysStoryWorldPreference.getBoolean(GuideMode.GUIDE_LOGIN.name(), false);
		if(!isGuideShown) {
			getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
				@Override
				public boolean onPreDraw() {

					final int IDX_CNT = 3; 
					final int[] posX = new int[IDX_CNT];
					final int[] posY = new int[IDX_CNT];
					final int[] loc = new int[2];
					linkSignUp.getLocationOnScreen(loc);
					posX[0] = loc[0];
					posY[0] = loc[1];
					
					posX[2] = -1;
					posY[2] = loc[1] - linkSignUp.getHeight() / 2;

					linkFacebookLogin.getLocationOnScreen(loc);
					posX[1] = loc[0];
					posY[1] = loc[1];
					
					final Bundle guideArgs = new Bundle();
					guideArgs.putString(GuideDialog.MODE_GUIDE, GuideMode.GUIDE_LOGIN.name());
					guideArgs.putInt(GuideDialog.ITEM_COUNT, IDX_CNT);
					guideArgs.putIntArray(GuideDialog.ITEM_IDS, new int[]{R.id.guide_text4, R.id.guide_text5, R.id.guide_image_frame}); 
					guideArgs.putIntArray(GuideDialog.ITEM_LEFTS, posX);
					guideArgs.putIntArray(GuideDialog.ITEM_TOPS, posY);

					new GuideDialog(getContext(), guideArgs).show();
					
					getViewTreeObserver().removeOnPreDrawListener(this);
					BobbysStoryWorldPreference.putBoolean(GuideMode.GUIDE_LOGIN.name(), true);
					return true;
				}
			});
		}
		
		loginBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
			    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
				requestLogin();
			}
		});
		
		loginBtn.setEnabled(false);
		emailView.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				FontTextView errorView = (FontTextView)findViewById(R.id.login_edit_email_error);
				errorView.setVisibility(View.GONE);
				String st = s.toString();
				if(st.trim().equals(""))
					mIsEmailFill = false;
				else
					mIsEmailFill = true;
				
				if(mIsEmailFill && mIsPasswordFill)
					loginBtn.setEnabled(true);
				else
					loginBtn.setEnabled(false);
			}
		});
		
		emailView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				FontTextView errorView = (FontTextView)findViewById(R.id.login_edit_email_error);
				errorView.setVisibility(View.GONE);
				return false;
			}
		});
		
		emailView.setOnImeHideListener(new FontEditText.ImeHideListener() {
			@Override
			public void onImeHide() {
				String emailAdd = emailView.getText().toString();
				if(emailAdd.length() > 0 && !Util.checkEmail(emailAdd)) {
					FontTextView errorView = (FontTextView)findViewById(R.id.login_edit_email_error);
					errorView.setText(R.string.pf_net_error_email_enter_full_address);
					errorView.setVisibility(View.VISIBLE);
				}
			}
		});
		
		emailView.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(!hasFocus) {
					String emailAdd = emailView.getText().toString();
					if(emailAdd.length() > 0 && !Util.checkEmail(emailAdd)) {
						FontTextView errorView = (FontTextView)findViewById(R.id.login_edit_email_error);
						errorView.setText(R.string.pf_net_error_email_enter_full_address);
						errorView.setVisibility(View.VISIBLE);
					}
				}
			}
		});
		
		emailView.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId != EditorInfo.IME_NULL || (actionId == EditorInfo.IME_NULL && event != null && event.getAction() == KeyEvent.ACTION_DOWN)) {
					String emailAdd = emailView.getText().toString();
					if(emailAdd.length() > 0 && !Util.checkEmail(emailAdd)) {
						FontTextView errorView = (FontTextView)findViewById(R.id.login_edit_email_error);
						errorView.setText(R.string.pf_net_error_email_enter_full_address);
						errorView.setVisibility(View.VISIBLE);
						return true;
					}
				}
				return false;
			}
		});
		
		pwdView.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				FontTextView errorView = (FontTextView)findViewById(R.id.login_edit_password_error);
				errorView.setVisibility(View.GONE);
				String st = s.toString();
				if(st.trim().equals(""))
					mIsPasswordFill = false;
				else
					mIsPasswordFill = true;
				
				if(mIsEmailFill && mIsPasswordFill)
					loginBtn.setEnabled(true);
				else
					loginBtn.setEnabled(false);
			}
		});
		
		pwdView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				FontTextView errorView = (FontTextView)findViewById(R.id.login_edit_password_error);
				errorView.setVisibility(View.GONE);
				return false;
			}
		});
		
		pwdView.setOnImeHideListener(new FontEditText.ImeHideListener() {
			@Override
			public void onImeHide() {
				String password = pwdView.getText().toString();
				final int len = password.length();
				if(len > 0 && len < Constants.PASSWORD_MIN_LENGTH) {
					FontTextView errorView = (FontTextView)findViewById(R.id.login_edit_password_error);
					errorView.setText(R.string.pf_net_error_password_short);
					errorView.setVisibility(View.VISIBLE);
				}
			}
		});
		
		pwdView.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(!hasFocus) {
					String password = pwdView.getText().toString();
					final int len = password.length();
					if(len > 0 && len < Constants.PASSWORD_MIN_LENGTH) {
						FontTextView errorView = (FontTextView)findViewById(R.id.login_edit_password_error);
						errorView.setText(R.string.pf_net_error_password_short);
						errorView.setVisibility(View.VISIBLE);
					}
				}
			}
		});
		
		pwdView.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId != EditorInfo.IME_NULL || (actionId == EditorInfo.IME_NULL && event != null && event.getAction() == KeyEvent.ACTION_DOWN)) {
					String password = pwdView.getText().toString();
					final int len = password.length();
					if(len > 0 && len < Constants.PASSWORD_MIN_LENGTH) {
						FontTextView errorView = (FontTextView)findViewById(R.id.login_edit_password_error);
						errorView.setText(R.string.pf_net_error_password_short);
						errorView.setVisibility(View.VISIBLE);
						return true;
					}
				}
				return false;
			}
		});
		
		setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent e) {
				Util.hideSoftInput(getContext(), v);
				emailView.clearFocus();
				pwdView.clearFocus();
				return false;
			}
		});
		
		
	}
	
//	@Override
//	public void onResume() {
//		super.onResume();
//		final EditText emailView = (EditText) getView().findViewById(R.id.login_edit_email);
//		if(mForgottenPassEmail != null && !"".equals(mForgottenPassEmail)) {
//			emailView.setText(mForgottenPassEmail);		
//		}
//	}
	
	private OnClickListener mViewOnclickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			final int id = view.getId();
			switch (id) {
			case R.id.login_link_forgot_password:
				((PortraitActivity)getContext()).showMenu(PortraitActivity.ENTRY_MENU_FIND_PASSWORD);
				break;
			case R.id.login_link_sign_up:
				Bundle data = ((PortraitActivity)getContext()).getSavedDataBundle();
		    	if(data == null)
		    		data = new Bundle();
		    	data.putBoolean(PortraitActivity.FIRST_LAUNCH, mIsFirst);
				((PortraitActivity)getContext()).showMenu(PortraitActivity.ENTRY_MENU_SIGNUP);
				break;
			default:
				break;
			}
		}
	};
	
	private void requestLogin() {
		final TextView emailView = (TextView)findViewById(R.id.login_edit_email);
		final TextView passView = (TextView)findViewById(R.id.login_edit_password);
		
		String emailAdd = emailView.getText().toString();
		String password = passView.getText().toString();
		final int len = password.length();
		if(emailAdd.length() > 0 && !Util.checkEmail(emailAdd)) {
			FontTextView errorView = (FontTextView)findViewById(R.id.login_edit_email_error);
			errorView.setText(R.string.pf_net_error_email_enter_full_address);
			errorView.setVisibility(View.VISIBLE);
			return;
		}
		if(len > 0 && len < Constants.PASSWORD_MIN_LENGTH) {
			FontTextView errorView = (FontTextView)findViewById(R.id.login_edit_password_error);
			errorView.setText(R.string.pf_net_error_password_short);
			errorView.setVisibility(View.VISIBLE);
			return;
		}
		
		mTempUserId = emailView.getText().toString();
		
		final HashMap<String, String> request = new HashMap<String, String>();
		request.put(Constants.TAG_USER_ID, mTempUserId);
		request.put(Constants.TAG_PASSWORD, passView.getText().toString());
		request.put(Constants.TAG_SERVICE_CODE, Constants.SERVICE_CODE);
		request.put(Constants.TAG_MOBILE_OS, Constants.OS_STRING);

		String registrationId = BobbysStoryWorldPreference.getString(BobbysStoryWorldActivity.PROPERTY_REG_ID, "");

		request.put(Constants.TAG_DEVICE_TOKEN, registrationId);
		request.put(Constants.TAG_UUID, Util.getUUID(getContext()));
		request.put(Constants.TAG_CLIENT_IP, Util.getIp(getContext()));
		
		mRequestAuthKey = new RequestAuthKey(getContext(), request);
		mRequestAuthKey.setCallback(this);
		Model.runOnWorkerThread(mRequestAuthKey);

		mProgressDialog = Util.openProgressDialog(getContext());
	}
	
	private void onLoginSessionStateChange(Session session, SessionState state, Exception exception) {
	    if (state.isOpened()) {
	        Request.newMeRequest(session, new Request.GraphUserCallback() {
	        	@Override
	        	public void onCompleted(GraphUser user, Response response) {
 	        		if (user != null) {
	        			final Locale current = Locale.getDefault(); //getResources().getConfiguration().locale;
	        			
	        			mTempUserId = (String) user.getProperty("email");
 	        			mTempCountry = Util.getCountryIsoForFacebook(getContext(), user);
 	        			mTempLanguage = current.getLanguage();
	        			String gender = user.asMap().get("gender").toString(); 
 	        			mTempGender = Constants.GenderType.MALE.name().equalsIgnoreCase(gender)?
    							Constants.GenderType.MALE.type : Constants.GenderType.FEMALE.type;
 	        			try {
	 	   	        		JSONObject object = new JSONObject(response.getRawResponse());
//	 	   	        		if(object.has("id"))
// 	   	        			mTempFacebookId = mTempUserId;
	 	   	        		mTempFacebookId = (String)user.getProperty("id");


 	 	        			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
 	 	        			final String birthday = user.getBirthday();
 	 	        			if(birthday != null) {
	 	 	        			Date birth = format.parse(birthday);
	 	 	        			Calendar cal = Calendar.getInstance();
	 	 	        			cal.setTime(birth);
	 	 	        			mTempBirth = String.valueOf(cal.get(Calendar.YEAR));
	 	        			}
 	        			} catch(ParseException e) {
 	        				e.printStackTrace();
 	        			} catch(JSONException e) {
 	        				e.printStackTrace();
 	        			}
 	        			mTempName = user.getName();
 	        			mTempCity = Util.getCityForFacebook(user);
	        			
 	        			android.util.Log.d(TAG, 
	        					"after onLoginSessionStateChange\n name = " + user.getName() +
	        					"\n -- email = " + mTempUserId +
	        					"\n -- gender = " + gender + 
	        					"\n -- birth = " + mTempBirth +
	        					"\n -- gender = " + Constants.GenderType.MALE.name().toLowerCase() + " | real gender = " + gender +
	        					"\n -- city = " + mTempCity +
	        					"\n -- country = " + mTempCountry +
	        					"\n -- facebookId = " + mTempFacebookId
	        					);
	        			requestSNSLogin();
	        		}
	        	}
	        }).executeAsync();
	    } else if (state.isClosed()) {
	    	android.util.Log.d(TAG, "onLoginSessionStateChange out...");
	    } else {
	    	android.util.Log.d(TAG, "somethin else??? stat = " + state.toString());
	    }
	}
	
	private void signInWithFacebook() {
		Session session = Session.getActiveSession();
		if (session == null) {
			session = new Session(getContext());
			Session.setActiveSession(session);
		}

	    if (!session.isOpened() && !session.isClosed()) {
	        Session.OpenRequest openRequest = null;
	        openRequest = new Session.OpenRequest((Activity)getContext());

	        if (openRequest != null) {
	            //openRequest.setDefaultAudience(SessionDefaultAudience.FRIENDS);
	        	mFacebookLoginStatusCallback.mIsAlive = true;
	            openRequest.setPermissions(facebookPermissions);
	            openRequest.setLoginBehavior(SessionLoginBehavior.SSO_WITH_FALLBACK);
	            openRequest.setCallback(mFacebookLoginStatusCallback);
	            session.openForRead(openRequest);
	        }
	    } else {
	    	onLoginSessionStateChange(session, session.getState(), null);
	    }
	}

//    private void onClickFacebookLogout() {
//        Session session = Session.getActiveSession();
//        if (!session.isClosed()) {
//            session.closeAndClearTokenInformation();
//        }
//    }

    private class LoginSessionStatusCallback implements Session.StatusCallback {
    	public boolean mIsAlive;
    	
        @Override
        public void call(Session session, SessionState state, Exception exception) {
        	if(mIsAlive)
        		onLoginSessionStateChange(session, state, exception);
        }
    }
	
	private void requestSNSLogin() {
		mIsFacebookLogin = true;
		final HashMap<String, String> request = new HashMap<String, String>();
		request.put(Constants.TAG_UUID, Util.getUUID(getContext()));
		request.put(Constants.TAG_SNS_TYPE, "SNS001");
		request.put(Constants.TAG_EMAIL, mTempUserId);
		request.put(Constants.TAG_SNSID, mTempFacebookId);

		String registrationId = BobbysStoryWorldPreference.getString(BobbysStoryWorldActivity.PROPERTY_REG_ID, "");

		request.put(Constants.TAG_DEVICE_TOKEN, registrationId);
		request.put(Constants.TAG_CLIENT_IP, Util.getIp(getContext()));
		request.put(Constants.TAG_MOBILE_OS, Constants.OS_STRING);
		request.put(Constants.TAG_SERVICE_CODE, Constants.SERVICE_CODE);
		
		RequestSNSLogin snsLogin = new RequestSNSLogin(getContext(), request);
		snsLogin.setCallback(this);
		Model.runOnWorkerThread(snsLogin);
		mProgressDialog = Util.openProgressDialog(getContext());
	}
	
	private void requestTerms() {
    	Bundle data = ((PortraitActivity)getContext()).getSavedDataBundle();
//    	data.putBoolean(AgreePersonalInfoView.FROM_FACEBOOK_LOGIN, true);
    	data.putString(Constants.TAG_USER_ID, mTempUserId);
    	data.putString(Constants.TAG_EMAIL, mTempUserId);
    	data.putString(Constants.TAG_STANDARD_COUNTRY_CODE, mTempCountry);
    	data.putString(Constants.TAG_ADDRESS_MAIN, mTempCity);
    	data.putString(Constants.TAG_BIRTHDAY, mTempBirth);
    	data.putString(Constants.TAG_GENDER, String.valueOf(mTempGender));
    	data.putString(Constants.TAG_LANGUAGE, mTempLanguage);
    	data.putString(Constants.TAG_SNS_ID, mTempFacebookId);
    	data.putString(Constants.TAG_SNS_USER_NAME, mTempName);
//    	((PortraitActivity)getContext()).showMenu(PortraitActivity.ENTRY_MENU_SIGNUP_TERMS);

		data.putInt(UserDetailView.EXTRA_USER_DETAIL_MODE_NAME, UserDetailView.SIGN_UP_FACEBOOK);
		((PortraitActivity)getContext()).showMenu(PortraitActivity.ENTRY_MENU_SIGNUP);
//		((PortraitActivity)getContext()).removeMenuStack(PortraitActivity.ENTRY_MENU_SIGNUP_TERMS);
	}
	
	@Override
	public void onSuccess(ServerTask parser, Map<String, String> result) {
		if(parser instanceof RequestAuthKey || parser instanceof RequestSNSLogin) {
			// No more login after first launch
			BobbysStoryWorldPreference.putBoolean(Constants.PREF_IS_FIRST_APP_LAUNCH, false);

			mTempUserNo = result.get(Constants.TAG_USER_NO);
			mTempAuthKey = result.get(Constants.TAG_AUTHKEY);
			
			final HashMap<String, String> request = new HashMap<String, String>();
			request.put(Constants.TAG_USER_NO, mTempUserNo);
			request.put(Constants.TAG_AUTHKEY, mTempAuthKey);
			request.put(Constants.TAG_CLIENT_IP, Util.getIp(getContext()));
			
			mGetMobileProfile = new GetMobileProfile(getContext(), request);
			mGetMobileProfile.setCallback(this);
			Model.runOnWorkerThread(mGetMobileProfile);
		} else if(parser instanceof GetMobileProfile) {
			BobbysStoryWorldPreference.putString(Constants.PREF_USER_ID, mTempUserId);
			BobbysStoryWorldPreference.putInt(Constants.PREF_USER_NO, Integer.valueOf(mTempUserNo));
			BobbysStoryWorldPreference.putString(Constants.PREF_AUTH_KEY, mTempAuthKey);
			BobbysStoryWorldPreference.putBoolean(Constants.PREF_EMAIL_VALIFICATION, Boolean.valueOf(result.get(Constants.TAG_EMAIL_VALIFICATION)));
			BobbysStoryWorldPreference.putString(Constants.PREF_EMAIL_ACTIVATE_DATE_TIME, result.get(Constants.TAG_EMAIL_ACTIVATE_DATE_TIME));
			BobbysStoryWorldPreference.putString(Constants.PREF_CHANGE_PASSWROD_DATE_TIME, result.get(Constants.TAG_CHANGE_PASSWROD_DATE_TIME));
			BobbysStoryWorldPreference.putString(Constants.PREF_REGISTER_DATE_TIME, result.get(Constants.TAG_REGISTER_DATE_TIME));
			BobbysStoryWorldPreference.putString(Constants.PREF_COUNTRY, result.get(Constants.TAG_COUNTRY));
			BobbysStoryWorldPreference.putString(Constants.PREF_LANGUAGE, result.get(Constants.TAG_LANGUAGE));
			BobbysStoryWorldPreference.putInt(Constants.PREF_GENDER, Integer.valueOf(result.get(Constants.TAG_GENDER)));
			BobbysStoryWorldPreference.putString(Constants.PREF_BIRTHDAY, result.get(Constants.TAG_BIRTHDAY));
			BobbysStoryWorldPreference.putString(Constants.PREF_ADDRESS_MAIN, result.get(Constants.TAG_ADDRESS_MAIN));
			BobbysStoryWorldPreference.putInt(Constants.PREF_LOGIN_TYPE, mIsFacebookLogin?Constants.LOGIN_TYPE_FACEBOOK:Constants.LOGIN_TYPE_EMAIL);
			
			mPendingSaveAuthData = true;
			
			//MixpanelAPI Create User Profile
			MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(), Constants.MIXPANEL_TOKEN);
			mixpanel.alias(mTempUserId, null);
			//mixpanel.identify(mTempUserNo);
			mixpanel.getPeople().identify(mixpanel.getDistinctId());
			mixpanel.getPeople().initPushHandling(Constants.GCM_SENDER_ID);
			mixpanel.getPeople().set("$email", mTempUserId);
			mixpanel.getPeople().set("$created", result.get(Constants.TAG_REGISTER_DATE_TIME));
			mixpanel.getPeople().set("$first_name","");
			mixpanel.getPeople().set("$last_name","");
			mixpanel.getPeople().set("$name","");
			mixpanel.getPeople().set("$ip",Util.getIp(getContext()));
			mixpanel.getPeople().set("city", result.get(Constants.TAG_ADDRESS_MAIN));
			mixpanel.getPeople().set("gender", mTempGender == -1 ? 99 : mTempGender);
			if (mTempBirth!=null) {
				String birthyear = mTempBirth.length() > 4? mTempBirth.substring(0,4) : "2015";
				mixpanel.getPeople().set("birthday", birthyear);
			}
			else {
				mixpanel.getPeople().set("birthday", "2015");
			}
			
			
			// JP added
			JSONObject props = new JSONObject();
			try {
				props.put("User Type", "Registered");
				props.put("Email", mTempUserId);
				mixpanel.registerSuperProperties(props);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();	
			}
			
			//MixpanelAPI track Login
			props = new JSONObject();
			try {
				props.put("Authentication", "Email");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			mixpanel.track(Constants.TAG_DIRECT_LOGIN, props);
			mixpanel.flush();

			HashMap<String, String> request = new HashMap<String, String>();
			request.put(Constants.TAG_USER_NO, mTempUserNo);
			request.put(Constants.TAG_CLIENT_IP, Util.getIp(getContext()));

			GetBalance getBalance = new GetBalance(getContext(), request);
			getBalance.setCallback(this);
			Model.runOnWorkerThread(getBalance);
		} else if(parser instanceof GetBalance) {
			String balance = result.get(Constants.TAG_EVENT_BALANCE);
			BobbysStoryWorldPreference.putInt(Constants.PREF_LAST_DOLE_COIN, Integer.valueOf(balance));
			((PortraitActivity)getContext()).saveDoleCoinBalance();
			if(mPendingSaveAuthData) {
				mPendingSaveAuthData = false;
				((PortraitActivity)getContext()).saveAuthData();
			}

			mProgressDialog.dismiss();
			((PortraitActivity)getContext()).loginFinished(false);
		}
	}
	
	@Override
	public void onFailed(ServerTask parser, Map<String, String> result, int returnCode) {
		android.util.Log.e(TAG, " onFailed parser = " + parser + " | ret = " + returnCode);

		if(mPendingSaveAuthData) {
			mPendingSaveAuthData = false;
			((PortraitActivity)getContext()).saveAuthData();
		}

		if(parser instanceof RequestSNSLogin && returnCode == Constants.ERROR_CODE_UNREGIST_SNS) {
			mProgressDialog.dismiss();
			requestTerms();
		} else {
			if(Util.isEmailError(returnCode)) {
				FontTextView errorView = (FontTextView)findViewById(R.id.login_edit_email_error);
				errorView.setText(Util.switchErrorCode(returnCode, getResources()));
				errorView.setVisibility(View.VISIBLE);
			} else if(Util.isPasswordError(returnCode)) {
				FontTextView errorView = (FontTextView)findViewById(R.id.login_edit_password_error);
				errorView.setText(Util.switchErrorCode(returnCode, getResources()));
				errorView.setVisibility(View.VISIBLE);
			} else {
			Toast.makeText(getContext(), Util.switchErrorCode(returnCode, getResources()), Toast.LENGTH_SHORT).show();
			}
			mProgressDialog.dismiss();
		}
	}

//	@Override
//	public void onActivityResult(int requestCode, int resultCode, Intent data) {
//		super.onActivityResult(requestCode, resultCode, data);
//
//		DLog.e(TAG, " requestCode = " + requestCode + " resultCode = " + resultCode + " data = " + data);
//		switch (requestCode) {
//		case Session.DEFAULT_AUTHORIZE_ACTIVITY_CODE:
//			DLog.d(TAG, " Return from facebook login activity");
//            Session.getActiveSession().onActivityResult(getActivity(), requestCode, resultCode, data);
//            break;
//            
//		case Constants.REQUEST_FORGOTTEN_PASS_EMAIL:
//			mForgottenPassEmail = data.getStringExtra(Constants.EMAIL);
//			break;
//		default:
//			break;
//		}
//	}
}
