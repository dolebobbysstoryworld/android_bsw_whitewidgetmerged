package com.dole.bobbysstoryworld.ui;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class LoadingImageView extends ImageView {
	private int mWidth;
	private int mHeight;
	private long mProgress;
	private long mMax;
	private int mRadius;
	private float mRatio;
	private Paint mPaint = new Paint();
	private Paint mPaintPorterDuff = new Paint();
	private RectF mRect;
	private int mInfoPadding;
	private int mImageSize;
	private Drawable mImageForeground;
	private Drawable mImageBackground;
	private Bitmap mWindow;
	
	public LoadingImageView(Context context) {
		super(context);
		init();
	}
	
	public LoadingImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public LoadingImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}
	
	public void startLoading() {
		mProgress = 0;
	}
	
	public void setLoadingImage(int imageBg, int imageFg, int size) {
		Resources r = getContext().getResources();
		mImageSize = r.getDimensionPixelSize(size);
		mImageForeground = r.getDrawable(imageFg);
		mImageBackground = r.getDrawable(imageBg);
		mImageForeground.setBounds(0, 0, mImageSize, mImageSize);
		mImageBackground.setBounds(0, 0, mImageSize, mImageSize);
	}
	
	public void setMax(int duration) {
		mMax = duration;
		mRatio = (float)360 / (float)mMax;
	}
	
	public void progress(int progress) {
		mProgress = progress;

		final int biggerSize = (int)(mImageSize * 1.2f);
		if(mRect == null)
			mRect = new RectF();
		mRect.set(0, 0, biggerSize, biggerSize);

		if(mWindow != null)
			mWindow.recycle();

		Bitmap pie = Bitmap.createBitmap(biggerSize, biggerSize, Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(pie);
		c.drawArc(mRect, -90.f, mProgress * mRatio, true, mPaint);

		mWindow = Bitmap.createBitmap(mImageSize, mImageSize, Bitmap.Config.ARGB_8888);
		Canvas windowCanvas = new Canvas(mWindow);
		mImageForeground.draw(windowCanvas);
		windowCanvas.drawBitmap(pie, (mImageSize - biggerSize) / 2, (mImageSize - biggerSize) / 2, mPaintPorterDuff);

		((Activity)getContext()).runOnUiThread(new Runnable() {
			public void run() {
				invalidate();
			}
		});
	}
	
	private void init() {
		mPaint.setAntiAlias(true);
		mPaint.setColor(Color.BLACK);

		mPaintPorterDuff.setAntiAlias(true);
		mPaintPorterDuff.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));

		setScaleType(ScaleType.CENTER);
	}
	
//	@Override
//	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
//		super.onSizeChanged(w, h, oldw, oldh);
//		if(w > 0 && h > 0 && oldw == 0 && oldh == 0) {
//			mWidth = w;
//			mHeight = h;
//		}
//	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		if(mImageBackground != null || mWindow != null) {
			canvas.save();
			canvas.translate((canvas.getWidth() - mImageSize) / 2, 0);
			if(mImageBackground != null)
				mImageBackground.draw(canvas);
			if(mWindow != null)
			canvas.drawBitmap(mWindow, 0, 0, mPaint);
			canvas.restore();
		}
	}
}
