package com.dole.bobbysstoryworld.ui;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.dole.bobbysstoryworld.R;

public class CityDialog extends Dialog {
	
	private String[] mCities;
	private CityDialogClickListener mDialogClickListener;
	
	public interface CityDialogClickListener {
		public void onClickOK(String city);
		public void onClickCancel();
	}

	public CityDialog(Context context, JSONArray cities, CityDialogClickListener listener) {
		super(context);
		mCities = new String[cities.length()];
		for(int i = 0; i < cities.length(); i++) {
			try {
				mCities[i] = cities.getString(i);
			} catch (JSONException e) {
				mCities[i] = "";
				e.printStackTrace();
			}
		}
		mDialogClickListener = listener;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.city_chooser);
		
		TextView dialogTitle = (TextView)findViewById(R.id.dialog_title);
		if(dialogTitle != null)
			dialogTitle.setText(R.string.pf_signup_popup_title_select_city);

		int titleHeight = getContext().getResources().getDimensionPixelSize(R.dimen.dialog_title_height);
		int buttonHeight = getContext().getResources().getDimensionPixelSize(R.dimen.dialog_button_height);
		int itemHeight = getContext().getResources().getDimensionPixelSize(R.dimen.dialog_item_height);
		
		final int totalHeight = titleHeight + buttonHeight + (itemHeight * Math.min(3, mCities.length));
		
		getWindow().setBackgroundDrawable(new ColorDrawable(0));
		
		int divierId = getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = findViewById(divierId);
		divider.setVisibility(View.GONE);
		
		int titleId = getContext().getResources().getIdentifier("android:id/title", null, null);
		View title = findViewById(titleId);
		title.setVisibility(View.GONE);
		
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
	    lp.copyFrom(getWindow().getAttributes());
	    lp.height = totalHeight;
	    getWindow().setAttributes(lp);
		
	    final ListView listView = (ListView)findViewById(R.id.dialog_content);
		View button = findViewById(R.id.dialog_button);
		
		CityAdapter adapter = new CityAdapter();
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(mOnItemClickListener);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(mDialogClickListener != null)
					mDialogClickListener.onClickCancel();
				dismiss();
			}
		});
	}

	private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			dismiss();
			Object tag = view.getTag();
			if(tag instanceof String) {
				final String city = (String)tag;
				if(mDialogClickListener != null)
					mDialogClickListener.onClickOK(city);
//				final Intent data = new Intent();
//				data.putExtra(Constants.CITY, city);
//				getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, data);
			}
		}
	};

	class CityAdapter extends BaseAdapter {
		LayoutInflater inflater;

		public CityAdapter() {
			inflater = LayoutInflater.from(getContext());
		}

		@Override
		public int getCount() {
			if(mCities == null)
				return 0;
			return mCities.length;
		}
		
		@Override
		public Object getItem(int position) {
			if(mCities == null)
				return -1;
			if(position >= mCities.length)
				return -1;
			return mCities[position];
		}

		@Override
		public long getItemId(int position) {
			if(mCities == null)
				return -1;
			return position;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null) {
				convertView = inflater.inflate(R.layout.city_list_item, null);
			}
			
			final TextView textView = (TextView)convertView;
			String city = mCities[position];
			textView.setText(city);
			convertView.setTag(city);
			
			return convertView;
		}
		
	}
}
