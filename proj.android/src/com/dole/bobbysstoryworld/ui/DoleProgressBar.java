package com.dole.bobbysstoryworld.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.dole.bobbysstoryworld.R;

public class DoleProgressBar extends RelativeLayout {
	private int mWidth = 0;
	private int mIntervalWidth = 0;
	private int mIntervalNum = 0;
	private int mMinWidth = 0;
	private int mCurSceneId = 0;
	private int mTotalScene = 0;
	private VideoView mVideoView;
	private int mSceneInterval = 0;
	private int mStartInterval = 0;
	private AnimationDrawable mLeftDrawable;
	private AnimationDrawable mCenterDrawable;
	private AnimationDrawable mRightDrawable;
	
	public DoleProgressBar(Context context) {
		super(context);
	}
	
	public DoleProgressBar(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public DoleProgressBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	public void setCurSceneId(int sceneId) {
		if(sceneId % 5 == 0)
			sceneId = 5;
		mCurSceneId = sceneId;
	}
	
	public void setTotalScene(int sceneCount) {
		mTotalScene = sceneCount;
	}
	
	public void setVideoView(VideoView videoView) {
		mVideoView = videoView;
	}
	
	private void init() {
		View bg = findViewById(R.id.video_stop_progress_bg);
		View left_bar = findViewById(R.id.video_stop_progress_bar_left);
		View center_bar = findViewById(R.id.video_stop_progress_bar_center);
		View right_bar = findViewById(R.id.video_stop_progress_bar_right);
		
		mLeftDrawable = (AnimationDrawable)left_bar.getBackground();
		mCenterDrawable = (AnimationDrawable)center_bar.getBackground();
		mRightDrawable = (AnimationDrawable)right_bar.getBackground();
		mLeftDrawable.start();
		mCenterDrawable.start();
		mRightDrawable.start();
		
		mIntervalWidth = getResources().getDrawable(R.drawable.share_progress_center_01).getIntrinsicWidth();
		mMinWidth = getResources().getDrawable(R.drawable.share_progress_left_01).getIntrinsicWidth() + 
				getResources().getDrawable(R.drawable.share_progress_right_01).getIntrinsicWidth();
		mIntervalNum = (mWidth - mMinWidth)/mIntervalWidth; 
		
		int padding = (mWidth - mIntervalNum*mIntervalWidth)/2;
		bg.setPadding(padding, 0, padding, 0);
		left_bar.setPadding(padding, 0, 0, 0);
	}
	
	public void startProgress() {
		if(mWidth == 0) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					startProgress();
				}
			}, 300);
			return;
		}
		mSceneInterval = mIntervalNum / mTotalScene;
		if(mIntervalNum % mTotalScene <= mCurSceneId)
			mSceneInterval++;

		View center_bar = findViewById(R.id.video_stop_progress_bar_center);
		LayoutParams param = (LayoutParams)center_bar.getLayoutParams();
		
		if(mTotalScene > 1) {
			mStartInterval = (mIntervalNum / mTotalScene) * (mCurSceneId-1);
			if(mIntervalNum % mTotalScene <= mCurSceneId)
				mStartInterval += 1+mCurSceneId-(mIntervalNum % mTotalScene);
		}
		else
			mStartInterval = 0;
		
		param.width = mIntervalWidth * mStartInterval;
		requestLayout();
		new Handler().postDelayed(mRunnable, 1000);
	}
	
	Runnable mRunnable = new Runnable() {
		@Override
		public void run() {
			((Activity)getContext()).runOnUiThread(new Runnable() {
				@Override
				public void run() {
					requestLayout();
				}
			});
			View center_bar = findViewById(R.id.video_stop_progress_bar_center);
			LayoutParams param = (LayoutParams)center_bar.getLayoutParams();
			
			int interval;
			if(mVideoView.getDuration()/mSceneInterval <= 0)
				interval = mSceneInterval;
			else
				interval = mVideoView.getCurrentPosition() / (mVideoView.getDuration()/mSceneInterval);
			

			param.width = mIntervalWidth * (mStartInterval + interval);
			new Handler().postDelayed(mRunnable, 1000);
		}
	};
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		if(oldw == 0 && oldh == 0 && w > 0 && h > 0) {
			mWidth = w;
			init();
		}
		super.onSizeChanged(w, h, oldw, oldh);
	}
	
}
