package com.dole.bobbysstoryworld.ui;

import java.io.File;
import java.util.Locale;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.widget.TextView;

import com.android.vending.expansion.zipfile.APKExpansionSupport;
import com.dole.bobbysstoryworld.Constants;
import com.dole.bobbysstoryworld.FontCache;

public class FontTextHelper {
	
	public static String sEnglishFontPath = "";
	public static String sEnglishBoldFontPath = "";
	public static String sKorFontPath = "";
	public static String sGuideDefaultFontPath = "";
	
	public static final String sLangKr = Locale.KOREAN.getLanguage();
	public static final String sLangJa = Locale.JAPANESE.getLanguage();
	
	public static String sTypefaceFilename;

	private static boolean sInit = false;

	private static void init(Context context) {
		if(sInit)
			return;

		if(Constants.DEBUG) {
			sEnglishFontPath = "fonts/Roboto-Regular.ttf";
			sEnglishBoldFontPath = "fonts/Roboto-Bold.ttf";
			sKorFontPath = "fonts/DroidSansFallback.ttf";
			sGuideDefaultFontPath = "fonts/NanumPen.ttf";
		} else {
			final String prefix = APKExpansionSupport.getAPKExpansionPath(context);
			sEnglishFontPath = prefix + File.separator + "fonts/Roboto-Regular.ttf";
			sEnglishBoldFontPath = prefix + File.separator + "fonts/Roboto-Bold.ttf";
			sKorFontPath = prefix + File.separator + "fonts/DroidSansFallback.ttf";
			sGuideDefaultFontPath = prefix + File.separator + "fonts/NanumPen.ttf";
		}
		sInit = true;
	}

	public static void setFont(Context context, TextView textView) {
		init(context);

		final String currentLang = Locale.getDefault().getLanguage();
		if(sLangKr.equals(currentLang)) {
			sTypefaceFilename = sKorFontPath;
			//sTypefaceFilename = "NanumPen.ttf";
		} else {
			if(textView.getTypeface() != null && (textView.getTypeface().getStyle() & Typeface.BOLD) != 0) {
				//Bold
				sTypefaceFilename = sEnglishBoldFontPath;
			} else {
				sTypefaceFilename = sEnglishFontPath;
			}
		}
		FontCache.setCustomFont(context, textView, sTypefaceFilename);
	}

	public static void setFont(Context context, TextView textView, String font) {
		init(context);

		FontCache.setCustomFont(context, textView, font);
	}

	public static void setGuideFont(Context context, TextView textView) {
		init(context);

		final String currentLang = Locale.getDefault().getLanguage();
		if(!sLangJa.equals(currentLang)) {
			sTypefaceFilename = sGuideDefaultFontPath;
		} else {
			sTypefaceFilename = sKorFontPath;
		}
		FontCache.setCustomFont(context, textView, sTypefaceFilename);
	}
	
	public static void setFont(Context context, Paint textPaint) {
		init(context);

		final String currentLang = Locale.getDefault().getLanguage();
		if(sLangKr.equals(currentLang)) {
			sTypefaceFilename = sKorFontPath;
			//sTypefaceFilename = "NanumPen.ttf";
		} else {
			if(textPaint.getTypeface() != null && (textPaint.getTypeface().getStyle() & Typeface.BOLD) != 0) {
				//Bold
				sTypefaceFilename = sEnglishBoldFontPath;
			} else {
				sTypefaceFilename = sEnglishFontPath;
			}
		}
		FontCache.setCustomFont(context, textPaint, sTypefaceFilename);
	}
}
