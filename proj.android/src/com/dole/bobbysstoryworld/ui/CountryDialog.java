package com.dole.bobbysstoryworld.ui;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.dole.bobbysstoryworld.R;

public class CountryDialog extends Dialog {
	public static final int COUNTRY = 0;
	public static final int CITY = 1;

//	public static final int ETC = 0;
//	public static final int JP = 1;
//	public static final int KR = 2;
//	public static final int NZ = 3;
//	public static final int PH = 4;
//	public static final int SG = 5;
//	public static final int AU = 6;
//	public static final int AE = 7;
	
	public static final int NZ = 0;
	public static final int PH = 1;
	public static final int SG = 2;
	public static final int AE = 3;
	public static final int JP = 4;
	public static final int KR = 5;
	//public static final int ETC = 6;
	//public static final int AU = 6;
	
	private String[] mCountries;
//	private String[] mCountryName;
//	private String[] mCountryCode;
	private CountryDialogClickListener mDialogClickListener;
	private int mMode;
	private int mCountry;
	
	public interface CountryDialogClickListener {
		public void onClickOK(String city);
		public void onClickCancel();
	}

	public CountryDialog(Context context, CountryDialogClickListener listener, int mode, int country) {
		super(context);
		mDialogClickListener = listener;
		mMode = mode;
		mCountry = country;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.city_chooser);
		
		TextView dialogTitle = (TextView)findViewById(R.id.dialog_title);
		if(dialogTitle != null) {
			if(mMode == COUNTRY)
			dialogTitle.setText(R.string.pf_signup_popup_title_select_country);
			else
				dialogTitle.setText(R.string.pf_signup_popup_title_select_city);
		}

		if(mMode == COUNTRY) {
		mCountries = getContext().getResources().getStringArray(R.array.country);
		} else {
			switch(mCountry) {
				case JP:
					mCountries = getContext().getResources().getStringArray(R.array.city_jp);
					break;

				case KR:
					mCountries = getContext().getResources().getStringArray(R.array.city_kr);
					break;

				case NZ:
					mCountries = getContext().getResources().getStringArray(R.array.city_nz);
					break;

				case SG:
					mCountries = getContext().getResources().getStringArray(R.array.city_sg);
					break;

				default:
					mCountries = getContext().getResources().getStringArray(R.array.city_ph);
					break;
			}
		}

//		mCountryName = new String[mCountries.length];
//		mCountryCode = new String[mCountries.length];
//		for(int i = 0; i < mCountries.length; i++) {
//			String[] tmp = mCountries[i].split(",");
//			if(tmp.length == 2) {
//				mCountryName[i] = tmp[0];
//				mCountryCode[i] = tmp[0];
//			} else if(tmp.length == 1) {
//				mCountryName[i] = tmp[0];
//				mCountryCode[i] = getContext().getResources().getString(R.string.comm_other);
//			}
//		}
		
		int titleHeight = getContext().getResources().getDimensionPixelSize(R.dimen.dialog_title_height);
		int buttonHeight = getContext().getResources().getDimensionPixelSize(R.dimen.dialog_button_height);
		int itemHeight = getContext().getResources().getDimensionPixelSize(R.dimen.dialog_item_height);
		
		final int totalHeight = titleHeight + buttonHeight + (itemHeight * Math.min(3, mCountries.length));
		
		getWindow().setBackgroundDrawable(new ColorDrawable(0));
		
		int divierId = getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = findViewById(divierId);
		divider.setVisibility(View.GONE);
		
		int titleId = getContext().getResources().getIdentifier("android:id/title", null, null);
		View title = findViewById(titleId);
		title.setVisibility(View.GONE);
		
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
	    lp.copyFrom(getWindow().getAttributes());
	    lp.height = totalHeight;
	    getWindow().setAttributes(lp);
		
	    final ListView listView = (ListView)findViewById(R.id.dialog_content);
		View button = findViewById(R.id.dialog_button);
		
		CityAdapter adapter = new CityAdapter();
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(mOnItemClickListener);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(mDialogClickListener != null)
					mDialogClickListener.onClickCancel();
				dismiss();
			}
		});
	}

	private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			dismiss();
			Object tag = view.getTag();
			if(tag instanceof String) {
				final String code = (String)tag;
				if(mDialogClickListener != null)
					mDialogClickListener.onClickOK(code);
//				final Intent data = new Intent();
//				data.putExtra(Constants.CITY, city);
//				getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, data);
			}
		}
	};

	class CityAdapter extends BaseAdapter {
		LayoutInflater inflater;

		public CityAdapter() {
			inflater = LayoutInflater.from(getContext());
		}

		@Override
		public int getCount() {
			if(mCountries == null)
				return 0;
			return mCountries.length;
		}
		
		@Override
		public Object getItem(int position) {
			if(mCountries == null)
				return -1;
			if(position >= mCountries.length)
				return -1;
			return mCountries[position];
		}

		@Override
		public long getItemId(int position) {
			if(mCountries == null)
				return -1;
			return position;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null) {
				convertView = inflater.inflate(R.layout.city_list_item, null);
			}
			
			final TextView textView = (TextView)convertView;
			String countryName = mCountries[position];
//			String countryCode = mCountryCode[position];
			textView.setText(countryName);
			convertView.setTag(countryName);
			
			return convertView;
		}
		
	}
}
