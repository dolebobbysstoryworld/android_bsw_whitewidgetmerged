package com.dole.bobbysstoryworld.ui;

import android.os.Handler;
import android.os.SystemClock;
import android.view.animation.Interpolator;

/**
 * This class provides a simple timing engine for running animations which listen progress.
 */
public class SimpleAnimator implements Runnable {
    
	public static final long DEFAULT_DURATION = 1500;
    
    private long mDuration; // = DEFAULT_DURATION;
    private Interpolator mInterpolator;
    private Handler mHandler;
    private boolean mFinished;
    private long mStartTime;
    private float mProgress;
    private AnimatorListener mAnimatorListener;
    
    public SimpleAnimator() {
        this(null);
    }
    
    public SimpleAnimator(Interpolator interpolator) {
        mInterpolator = interpolator;
        mHandler = new Handler();
        mFinished = true;
    }
    
    public void start() {
        if (!mFinished) {
            mHandler.removeCallbacks(this);
            //mDuration = Math.min(SystemClock.uptimeMillis() - mStartTime, DEFAULT_DURATION); 
        } else {
        	mDuration = DEFAULT_DURATION;
        	
        	if (mAnimatorListener != null) 
            	mAnimatorListener.onStart();
        }
        
        mFinished = false;
        mHandler.post(this);
        mStartTime = SystemClock.uptimeMillis();
        
        mProgress = 0;
    }
    
    public void stop() {
        mFinished = true;
        mHandler.removeCallbacks(this);
    }
    
    @Override
    public void run() {
        if (mFinished) return;

        final long elapsedTime = SystemClock.uptimeMillis() - mStartTime;
        final long duration = mDuration;
        if (elapsedTime < duration) {
            // animating
            float progress = elapsedTime / (float)duration;
            if (mInterpolator != null) {
                progress = mInterpolator.getInterpolation(progress);
            }
            mProgress = progress;
            if (mAnimatorListener != null) mAnimatorListener.onAnimating(progress);
            mHandler.post(this);
        } else {
            // finished animation
            mFinished = true;
            if (mAnimatorListener != null) 
            	mAnimatorListener.onEnd();
            mProgress = 1f;
        }
    }
    
    public void setDuration(int duration) {
        mDuration = duration;
    }
    
    public void setAnimatorListener(AnimatorListener listener) {
        mAnimatorListener = listener;
    }
    
    public void setInterpolator(Interpolator interpolator) {
        mInterpolator = interpolator;
    }
    
    public boolean isFinished() {
        return mFinished;
    }
    
    public float getProgress() {
        return mProgress;
    }
    
    public static interface AnimatorListener {
        public void onStart();
        public void onAnimating(float progress);
        public void onEnd();
    }
}
