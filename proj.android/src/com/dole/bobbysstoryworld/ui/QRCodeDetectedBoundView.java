package com.dole.bobbysstoryworld.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.Camera.Size;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.OvershootInterpolator;

import com.dole.bobbysstoryworld.ui.SimpleAnimator.AnimatorListener;
import com.google.zxing.ResultPoint;

public class QRCodeDetectedBoundView extends View implements AnimatorListener {
	
private static final int DEFAULT_BOUND_EDGE_CNT = 4;
	
	private Paint mPaint;
	private Size mPreviewSize;
	
	private PointF[] mCurrentPoints = null;
	private PointF[] mStartPoints= null;
	private PointF[] mTargetPoints= null;
	private Path mLastPath = null;
	private SimpleAnimator mAnimator = new SimpleAnimator();
	private Rect mTempStartViewBound = new Rect();
	private boolean mIsCleared = false;
	
	private Rect mDebugRect;
	private Paint mDebugPaint;
	private Matrix mPreviewMatrix;
	private Matrix mPreviewConvertMatrix;
	
	public interface OnRectAnimationCallback {
		public void onRectAnimationStarted();
		public void onRectAnimationFinished();
	}
	
	OnRectAnimationCallback mCallback;

	public QRCodeDetectedBoundView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);

		init();
	}

	public QRCodeDetectedBoundView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public QRCodeDetectedBoundView(Context context) {
		this(context, null);
	}
	
	public void setPreviewConvertMatrix(Matrix previewConvertMatrix) {
		mPreviewConvertMatrix = previewConvertMatrix;
	}
	
	public void setOnRectAnimationCallback(OnRectAnimationCallback callback) {
		mCallback = callback;
	}
	
	public void removeOnRectAnimationCallback() {
		mCallback = null;
	}

	private void init() {
		mAnimator.setAnimatorListener(this);
		
		mPaint = new Paint();
		mPaint.setColor(0xFF3fc4d0);
		mPaint.setStyle(Style.STROKE);
		mPaint.setStrokeWidth(3);

		// Initialize current bound
		mCurrentPoints = new PointF[DEFAULT_BOUND_EDGE_CNT];
		for(int i = 0; i < DEFAULT_BOUND_EDGE_CNT; i++) {
			mCurrentPoints[i] = new PointF();
		}
	}

	public void setResultPoint(View startView, RectF startOffset,/* ResultPoint[] points,*/ Size previewSize) { // FOM 20150214 remove 2 params
		mPreviewSize = previewSize;
		// FOM 20150214 put comment
//		if(points == null || points.length < 4)	return;

		if(mAnimator.isFinished()) {
			//startView.getGlobalVisibleRect(mTempStartViewBound);
			mTempStartViewBound.set((int)startOffset.left, (int)startOffset.top, (int)startOffset.right, (int)startOffset.bottom);

			// Initialize start bound to Path
			if(mStartPoints == null)
				mStartPoints = new PointF[DEFAULT_BOUND_EDGE_CNT];
			for(int i = 0; i < DEFAULT_BOUND_EDGE_CNT; i++)
				mStartPoints[i] = new PointF();
			
			final float scaleX = (float)getWidth() / (float)mPreviewSize.height;
			final float scaleY = (float)getHeight() / (float)mPreviewSize.width;
			
			mStartPoints[0].x = mTempStartViewBound.left / scaleX; 
			mStartPoints[0].y = mTempStartViewBound.top / scaleY;
			mStartPoints[1].x = mTempStartViewBound.left / scaleX; 
			mStartPoints[1].y = mTempStartViewBound.bottom / scaleY;
			mStartPoints[2].x = mTempStartViewBound.right / scaleX; 
			mStartPoints[2].y = mTempStartViewBound.bottom / scaleY;
			mStartPoints[3].x = mTempStartViewBound.right / scaleX; 
			mStartPoints[3].y = mTempStartViewBound.top / scaleY;
			
			if(mLastPath == null)
				mLastPath = new Path();
		} else {
			mStartPoints = mTargetPoints;
		}

		// Initialize target bound
		mTargetPoints = new PointF[DEFAULT_BOUND_EDGE_CNT];
		for(int i = 0; i < DEFAULT_BOUND_EDGE_CNT; i++) {
			mTargetPoints[i] = new PointF();
			mTargetPoints[i].x = /*points[i].getX()*/ + startOffset.left; // FOM 20150214 put comment
			mTargetPoints[i].y = /*points[i].getY()*/ + startOffset.top; // FOM 20150214 put comment
		}
		mAnimator.setInterpolator(new OvershootInterpolator());
		mAnimator.start();
		invalidate();
	}
	
	public void setResultPoint(RectF startRect, RectF targetRect) {
		
		if(mAnimator.isFinished()) {

			// Initialize start bound to Path
			if(mStartPoints == null)
				mStartPoints = new PointF[DEFAULT_BOUND_EDGE_CNT];
			for(int i = 0; i < DEFAULT_BOUND_EDGE_CNT; i++)
				mStartPoints[i] = new PointF();
			
			mStartPoints[0].x = startRect.left;
			mStartPoints[0].y = startRect.top;
			mStartPoints[1].x = startRect.left;
			mStartPoints[1].y = startRect.bottom;
			mStartPoints[2].x = startRect.right;
			mStartPoints[2].y = startRect.bottom;
			mStartPoints[3].x = startRect.right;
			mStartPoints[3].y = startRect.top;

			if(mLastPath == null)
				mLastPath = new Path();
		} else {
			mStartPoints = mTargetPoints;
		}

		// Initialize target bound
		mTargetPoints = new PointF[DEFAULT_BOUND_EDGE_CNT];
		for(int i = 0; i < DEFAULT_BOUND_EDGE_CNT; i++)
			mTargetPoints[i] = new PointF();
		mTargetPoints[0].x = targetRect.left;
		mTargetPoints[0].y = targetRect.top;
		mTargetPoints[1].x = targetRect.left;
		mTargetPoints[1].y = targetRect.bottom;
		mTargetPoints[2].x = targetRect.right;
		mTargetPoints[2].y = targetRect.bottom;
		mTargetPoints[3].x = targetRect.right;
		mTargetPoints[3].y = targetRect.top;
		
		mAnimator.setInterpolator(new OvershootInterpolator());
		mAnimator.start();
		invalidate();
	}
	
	public void setCleared(boolean clear) {
		mIsCleared = clear;
	}
	
	public boolean isCleared() {
		return mIsCleared;
	}
	

	@Override
	protected void onDraw(Canvas canvas) {
		if(mStartPoints != null && mTargetPoints != null) {
			if(!mIsCleared && mLastPath != null) {
				final float scaleX = (mPreviewSize != null)?(float)getWidth() / (float)mPreviewSize.height:1.0f;
				final float scaleY = (mPreviewSize != null)?(float)getHeight() / (float)mPreviewSize.width:1.0f;
				canvas.save();
				if(mPreviewMatrix == null) {
					mPreviewMatrix = new Matrix();
					mPreviewConvertMatrix.invert(mPreviewMatrix);
				}
				canvas.setMatrix(mPreviewMatrix);

				canvas.scale(scaleX, scaleY);
				canvas.drawPath(mLastPath, mPaint);
				canvas.restore();
				mLastPath.reset();
			}
		}
		
		if(mDebugRect != null) {
			
			canvas.drawRect(mDebugRect, mDebugPaint);
		}
	}

	@Override
	public void onStart() {
		if(mCallback != null)
			mCallback.onRectAnimationStarted();
	}

	@Override
	public void onAnimating(float progress) {
		final PointF[] srcPoints = mStartPoints;
		final PointF[] destPoints = mTargetPoints;
		final PointF[] points = mCurrentPoints;

		if (srcPoints.length != destPoints.length) return;

		final int count = srcPoints.length;
		for (int idx = 0; idx < srcPoints.length; idx++) {
			points[idx].x = srcPoints[idx].x + (int) ((destPoints[idx].x - srcPoints[idx].x) * progress);
			points[idx].y = srcPoints[idx].y + (int) ((destPoints[idx].y - srcPoints[idx].y) * progress);
		}
		
		float initX = -1;
		float initY = -1;
		for (int i = 0; i <= points.length; i++) {
			int idx = Math.min(i, count - 1);
			if(i == 0) {
				initX = points[idx].x; initY = points[idx].y;
				mLastPath.moveTo(initX, initY);
			} else if(i == count) {
				mLastPath.lineTo(initX, initY);
			} else {
				mLastPath.lineTo(points[idx].x, points[idx].y);
			}
		}
		
		postInvalidate();
	}

	@Override
	public void onEnd() {
		if(mCallback != null)
			mCallback.onRectAnimationFinished();
	}
	
	public void setDebugRect(RectF rect) {
		if(mDebugRect == null)
			mDebugRect = new Rect();
		mDebugRect.set((int) rect.left, (int) rect.top, (int) rect.right, (int) rect.bottom);
		
		if(mDebugPaint == null) {
			mDebugPaint = new Paint();
			mDebugPaint.setColor(0xFFFF0000);
			mDebugPaint.setStyle(Style.STROKE);
			mDebugPaint.setStrokeWidth(3);
		}
	}
}
