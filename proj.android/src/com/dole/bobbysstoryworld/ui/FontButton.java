package com.dole.bobbysstoryworld.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class FontButton extends Button {
	
	public FontButton(Context context) {
		super(context);
		FontTextHelper.setFont(getContext(), this);
	}
	
	public FontButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		FontTextHelper.setFont(getContext(), this);
	}

	public FontButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		FontTextHelper.setFont(getContext(), this);
	}
}
