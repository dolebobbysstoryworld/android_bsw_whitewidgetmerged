package com.dole.bobbysstoryworld.ui;

import android.view.animation.Interpolator;


public class ScrollHelper {
//    private static final String TAG = "ScrollHelper";
//    private static final boolean DEBUG = false;
    
    public static final float BASELINE_FLING_VELOCITY = 2500f;
    public static final float FLING_VELOCITY_INFLUENCE = 0.4f;
    public static final int MINIMUM_SNAP_VELOCITY = 2200;
    public static final int SNAP_VELOCITY = 600;
    public static final int SNAP_ANIMATION_DURATION = 350;
    public static final int SNAP_ANIMATION_MAX_DURATION = 500;
    
    public static final int OVERSCROLL_STYLE_LINEAR = 0;
    public static final int OVERSCROLL_STYLE_VISCOUSFLUID = 1;
    public static final int OVERSCROLL_STYLE_INFLUENCECURVE = 2;
    
    private int mScrollExtent;
    
    static float VISCOUS_FLUID_SCALE = 8.0f;
    static float VISCOUS_FLUID_NO_APPLY_FACTOR = 0.02f;
    
    // This controls the viscous fluid effect (how much of it)
    float mViscousFluidScale;
    // must be set to 1.0 (used in viscousFluid())
    float mViscousFluidNormalize;
    
    static float OVERSCROLL_FACTOR = 0.26f;
    
    public ScrollHelper() {
        this(0);
    }
    
    public ScrollHelper(int scrollExtent) {
        mScrollExtent = scrollExtent;
        initViscousFluid(VISCOUS_FLUID_SCALE);
    }
    
//    public void setScrollExtent(int extent) {
//        mScrollExtent = extent;
//    }
//    
//    public int getScrollExtent() {
//        return mScrollExtent;
//    }
    
//    public int getMaxOverScrollAmount() {
//        int retValue = mScrollExtent;
//        
//        switch (mOverScrollStyle) {
//            case OVERSCROLL_STYLE_LINEAR:
//                retValue = retValue/2;
//                break;
//            case OVERSCROLL_STYLE_VISCOUSFLUID:
//                break;
//            case OVERSCROLL_STYLE_INFLUENCECURVE:
//                float f = 1.0f;
//                f = f / (Math.abs(f)) * (influenceCurve(Math.abs(f)));
//                float max = OVERSCROLL_FACTOR * f;
//                retValue = (int)(mScrollExtent * max);
//                break;
//        }
//        
//        return retValue;
//    }
    
//    public int computeOverScrollAmount(int scrollValue) {
//        if (mScrollExtent <= 0) return 0;
//        
//        boolean isNegative = false;
//        if (scrollValue < 0) {
//            isNegative = true;
//            scrollValue = Math.abs(scrollValue);
//        }
//        int retValue = 0;
//        switch (mOverScrollStyle) {
//            case OVERSCROLL_STYLE_LINEAR:
//                retValue = scrollValue;
//                break;
//            case OVERSCROLL_STYLE_VISCOUSFLUID:
//                retValue = computeViscousFluid(scrollValue);
//                break;
//            case OVERSCROLL_STYLE_INFLUENCECURVE:
//                retValue = computeInfluenceCurve(scrollValue);
//                break;
//        }
//        
//        int maxOverScrollAmount = getMaxOverScrollAmount();
//        if (retValue > maxOverScrollAmount) {
//            retValue = maxOverScrollAmount;
//        }
//        
//        if (Launcher.LOGD && DEBUG) {
//            Log.d(TAG, "computeOverScrollAmount amount=" + retValue + " param=" + scrollValue + " max=" + maxOverScrollAmount);
//        }
//        
//        if (isNegative) retValue = -retValue;
//        return retValue;
//    }
    
    private int computeViscousFluid(int scrollValue) {
        int linear = (int)(VISCOUS_FLUID_NO_APPLY_FACTOR * mScrollExtent);
        if (Math.abs(scrollValue) < linear) return scrollValue;
        
        float value = viscousFluid(scrollValue/4000f); // duration=4000ms
        return linear + Math.round(value * (mScrollExtent-linear)); // distance=mScrollExtent
    }
    
    private int computeInfluenceCurve(int scrollValue) {
        float f = ((float)scrollValue / mScrollExtent);

        if (f == 0) return scrollValue;
        f = f / (Math.abs(f)) * (influenceCurve(Math.abs(f)));

        // Clamp this factor, f, to -1 < f < 1
        if (Math.abs(f) >= 1) {
            f /= Math.abs(f);
        }
        
        return (int) Math.round(OVERSCROLL_FACTOR * f * mScrollExtent);
    }
    
    public void initViscousFluid(float viscousFluidScale) {
        mViscousFluidScale = viscousFluidScale;
        mViscousFluidNormalize = 1.0f;
        mViscousFluidNormalize = 1.0f / viscousFluid(1.0f);
    }
    
    public float viscousFluid(float x) {
        x *= mViscousFluidScale;
        if (x < 1.0f) {
            x -= (1.0f - (float)Math.exp(-x));
        } else {
            float start = 0.36787944117f;   // 1/e == exp(-1)
            x = 1.0f - (float)Math.exp(1.0f - x);
            x = start + x * (1.0f - start);
        }
        x *= mViscousFluidNormalize;
        return x;
    }
    
    private float influenceCurve(float f) {
        f -= 1.0f;
        return f * f * f + 1.0f;
    }
    
    static float distanceInfluenceForSnapDuration(float f) {
        f -= 0.5f; // center the values about 0.
        f *= 0.3f * Math.PI / 2.0f;
        return (float) Math.sin(f);
    }
    
    static class LauncherOvershootInterpolator implements Interpolator {
        private static final float DEFAULT_TENSION = 1.3f;
        private float mTension;

        public LauncherOvershootInterpolator() {
            mTension = DEFAULT_TENSION;
        }
        
        public void setDistance(int distance) {
            mTension = distance > 0 ? DEFAULT_TENSION / distance : DEFAULT_TENSION;
        }

        public void disableSettle() {
            mTension = 0.f;
        }

        public float getInterpolation(float t) {
            // _o(t) = t * t * ((tension + 1) * t + tension)
            // o(t) = _o(t - 1) + 1
            t -= 1.0f;
            return t * t * ((mTension + 1) * t + mTension) + 1.0f;
        }
    }
    
    static class ScrollInterpolator implements Interpolator {
        float mViscousFluidScale;
        float mViscousFluidNormalize;
        
        public ScrollInterpolator() {
            initViscousFluid(4f);
        }
        
        public void setDistance(int distance) {
        }

        public void disableSettle() {
        }

        public float getInterpolation(float t) {
            return viscousFluid(t);
        }
        
        public void initViscousFluid(float viscousFluidScale) {
            mViscousFluidScale = viscousFluidScale;
            mViscousFluidNormalize = 1.0f;
            mViscousFluidNormalize = 1.0f / viscousFluid(1.0f);
        }
        
        public float viscousFluid(float x) {
            x += 0.15f;
            x *= mViscousFluidScale;
//            if (x < 1.0f) {
//                x -= (1.0f - (float)Math.exp(-x));
//            } else {
//                float start = 0.36787944117f;   // 1/e == exp(-1)
//                x = 1.0f - (float)Math.exp(1.0f - x);
//                x = start + x * (1.0f - start);
//            }
            float start = 0.36787944117f;   // 1/e == exp(-1)
            x = 1.0f - (float)Math.exp(1.0f - x);
            x = start + x * (1.0f - start);
            x *= mViscousFluidNormalize;
            return x;
        }
    }
}
