package com.dole.bobbysstoryworld.ui;

import java.util.Calendar;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.DatePicker;
import android.widget.TextView;

import com.dole.bobbysstoryworld.R;

public class BirthDayPickerDialog extends Dialog {
	
	public static final String EXTRA_DAY = "day";
	public static final String EXTRA_MONTH = "month";
	public static final String EXTRA_YEAR = "year";

	public static final String EXTRA_BIRTH_DATE_ONLY_YEAR = "extra_birth_date";
	
	private boolean mIsUseYearOnly = false;
	
	private static final int UNAVAILBLE_VALUE = -1;
	private Bundle mArg;
	private BirthDayPickerDialogClickListener mDialogClickListener;

	public interface BirthDayPickerDialogClickListener {
		public void onClickOK(Bundle args);
		public void onClickCancel();
	}

	public BirthDayPickerDialog(Context context, Bundle arg, BirthDayPickerDialogClickListener listener) {
		super(context);
		mArg = arg;
		mDialogClickListener = listener;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.birthdate_dialog);
		
		getWindow().setBackgroundDrawable(new ColorDrawable(0));
		int divierId = getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = findViewById(divierId);
		divider.setVisibility(View.GONE);
		
		int titleId = getContext().getResources().getIdentifier("android:id/title", null, null);
		View title = findViewById(titleId);
		title.setVisibility(View.GONE);
		
		DatePicker datePicker = (DatePicker) findViewById(R.id.dialog_date_picker);
		Calendar calendar = Calendar.getInstance();
		
		int year = UNAVAILBLE_VALUE;
		int month = UNAVAILBLE_VALUE;
		int day = UNAVAILBLE_VALUE;
		if(mArg != null) {
			mIsUseYearOnly = mArg.getBoolean(EXTRA_BIRTH_DATE_ONLY_YEAR);
			
			year = mArg.getInt(EXTRA_YEAR, UNAVAILBLE_VALUE);
			month = mArg.getInt(EXTRA_MONTH, UNAVAILBLE_VALUE);
			day = mArg.getInt(EXTRA_DAY, UNAVAILBLE_VALUE);
		}

		if(year == UNAVAILBLE_VALUE)
			year = calendar.get(Calendar.YEAR);
		
		if(month == UNAVAILBLE_VALUE)
			month = calendar.get(Calendar.MONTH) + 1;
		
		if(day == UNAVAILBLE_VALUE)
			day = calendar.get(Calendar.DAY_OF_MONTH);
		
		datePicker.init(year, month - 1, day, null);
		datePicker.setMaxDate(Calendar.getInstance().getTimeInMillis());
		
		if(mIsUseYearOnly) {
			TextView tv = (TextView)findViewById(R.id.dialog_title);
			tv.setText(R.string.pf_signup_popup_title_birthyear);
			hideDayAndMonth();
		}
		
		//TODO Needs null check
		findViewById(R.id.dialog_button_ok).setOnClickListener(mBottomBtnClickListener);
		findViewById(R.id.dialog_button_cancel).setOnClickListener(mBottomBtnClickListener);
	}
	
	private View.OnClickListener mBottomBtnClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			dismiss();
			switch (v.getId()) {
			case R.id.dialog_button_ok:
				DatePicker datePicker = (DatePicker)findViewById(R.id.dialog_date_picker);
				datePicker.clearFocus();
				
				Bundle args = new Bundle();
				if(!mIsUseYearOnly) {
					args.putInt(EXTRA_DAY, datePicker.getDayOfMonth());
					args.putInt(EXTRA_MONTH, datePicker.getMonth()+1);
				}
				args.putInt(EXTRA_YEAR, datePicker.getYear());
				
				if(mDialogClickListener != null)
					mDialogClickListener.onClickOK(args);
				
				break;
			case R.id.dialog_button_cancel:
				if(mDialogClickListener != null)
					mDialogClickListener.onClickCancel();
				break;
			default:
				break;
			}
		}
	};
	
	@Override
	public void show() {
		if(mIsUseYearOnly)
			hideDayAndMonth();
		super.show();
	}
	
	private void hideDayAndMonth() {
		Resources r = getContext().getResources();
		int dayId = r.getIdentifier("android:id/day", null, null);
		int monthId = r.getIdentifier("android:id/month", null, null);
		int yearId = r.getIdentifier("android:id/year", null, null);
		int pickerId = r.getIdentifier("android:id/pickers", null, null);
		int numberPickerId = r.getIdentifier("android:id/numberpicker_input", null, null);

		final int pickerHeight = r.getDimensionPixelSize(R.dimen.birthday_picker_height);
		final int hPadding = r.getDimensionPixelSize(R.dimen.birthday_picker_horizontal_padding);
		MarginLayoutParams lp = null;

		View dayPicker = findViewById(dayId);
		if(dayPicker != null)
			dayPicker.setVisibility(View.GONE);
		
		View monthPicker = findViewById(monthId);
		if(monthPicker != null)
			monthPicker.setVisibility(View.GONE);

		View picker = findViewById(pickerId);
		View pickerContainer = (View)picker.getParent();
		if(pickerContainer != null) {
			lp = (MarginLayoutParams)pickerContainer.getLayoutParams();
			lp.width = LayoutParams.MATCH_PARENT;
			pickerContainer.setLayoutParams(lp);
		}

		if(picker != null) {
			lp = (MarginLayoutParams)picker.getLayoutParams();
			lp.width = LayoutParams.MATCH_PARENT;
			lp.height = pickerHeight;
			picker.setLayoutParams(lp);
		}

		View yearPicker = findViewById(yearId);
		if(yearPicker != null) {
			lp = (MarginLayoutParams)yearPicker.getLayoutParams();
			lp.leftMargin = lp.rightMargin = hPadding;
			lp.topMargin = lp.bottomMargin = 0;
			lp.width = LayoutParams.MATCH_PARENT;
			lp.height = pickerHeight;
			yearPicker.setLayoutParams(lp);

			View numberPicker = yearPicker.findViewById(numberPickerId);
			if(numberPicker != null) {
				lp = (MarginLayoutParams)numberPicker.getLayoutParams();
				lp.height = r.getDimensionPixelSize(R.dimen.birthday_picker_item_height);
				numberPicker.setLayoutParams(lp);
			}
		}
	}
}
