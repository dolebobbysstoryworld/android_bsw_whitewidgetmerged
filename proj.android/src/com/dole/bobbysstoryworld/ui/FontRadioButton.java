package com.dole.bobbysstoryworld.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioButton;

public class FontRadioButton extends RadioButton {
	public FontRadioButton(Context context) {
		super(context);
		FontTextHelper.setFont(getContext(), this);
	}
	
	public FontRadioButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		FontTextHelper.setFont(getContext(), this);
	}

	public FontRadioButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		FontTextHelper.setFont(getContext(), this);
	}
}
