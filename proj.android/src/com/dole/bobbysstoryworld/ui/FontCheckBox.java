package com.dole.bobbysstoryworld.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;

public class FontCheckBox extends CheckBox {
	
	public FontCheckBox(Context context) {
		super(context);
		FontTextHelper.setFont(getContext(), this);
	}
	
	public FontCheckBox(Context context, AttributeSet attrs) {
		super(context, attrs);
		FontTextHelper.setFont(getContext(), this);
	}

	public FontCheckBox(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		FontTextHelper.setFont(getContext(), this);
	}
}
