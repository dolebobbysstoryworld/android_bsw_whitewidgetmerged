package com.dole.bobbysstoryworld.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dole.bobbysstoryworld.Constants;
import com.dole.bobbysstoryworld.R;

public class CustomDialog extends Dialog {
	
	private Bundle mArg;
	private CustomDialogButtonClickListener mDialogClickListener;
	
	public interface CustomDialogButtonClickListener {
		public void onClickOK(int requestCode);
		public void onClickCancel(int requestCode);
	}

	public CustomDialog(Context context, Bundle arg, CustomDialogButtonClickListener listener) {
		super(context);
		mArg = arg;
		mDialogClickListener = listener;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.custom_dialog);
		
		getWindow().setBackgroundDrawable(new ColorDrawable(0));
		
		int divierId = getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = findViewById(divierId);
		if(divider != null)
		divider.setVisibility(View.GONE);
		
		int titleId = getContext().getResources().getIdentifier("android:id/title", null, null);
		View title = findViewById(titleId);
		if(title != null)
		title.setVisibility(View.GONE);
		
		if(mArg == null)
			return;
		
		boolean isTitle = mArg.getBoolean(Constants.CUSTOM_DIALOG_IS_TITLE, false);
		int titleRes = mArg.getInt(Constants.CUSTOM_DIALOG_TITLE_RES, 0);
		int imgRes = mArg.getInt(Constants.CUSTOM_DIALOG_IMG_RES, 0);
		int textRes = mArg.getInt(Constants.CUSTOM_DIALOG_TEXT_RES, 0);
		int textResParam1 = mArg.getInt(Constants.CUSTOM_DIALOG_TEXT_RES_PARAM1, -1);
		String textString = mArg.getString(Constants.CUSTOM_DIALOG_TEXT_STRING, "");
		int leftRes = mArg.getInt(Constants.CUSTOM_DIALOG_LEFT_BUTTON_RES, 0);
		int rightRes = mArg.getInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, 0);
		boolean isYesNo = mArg.getBoolean(Constants.CUSTOM_DIALOG_CHANGE_TEXT_YES_NO, false);
		final boolean allowCancel = mArg.getBoolean(Constants.CUSTOM_DIALOG_ALLOW_CANCEL, false);
		
		int leftTextRes = mArg.getInt(Constants.CUSTOM_DIALOG_LEFT_BUTTON_TEXT_RES, 0);
		int rightTextRes = mArg.getInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_TEXT_RES, 0);

		TextView titleView = (TextView)findViewById(R.id.dialog_title);
		View noTitleHeader = findViewById(R.id.dialog_no_title_header);
		ImageView titleImage = (ImageView)findViewById(R.id.custom_dialog_image);
		TextView content = (TextView)findViewById(R.id.custom_dialog_content);
		TextView detail = (TextView)findViewById(R.id.custom_dialog_detail);
		Button leftBtn = (Button)findViewById(R.id.custom_dialog_button_left);
		Button rightBtn = (Button)findViewById(R.id.custom_dialog_button_right);
		
		if(isYesNo) {
			leftBtn.setText(R.string.pf_signup_popup_button_title_cancel);
			rightBtn.setText(R.string.pf_signup_popup_button_title_ok);
		}
		
		if(isTitle) {
			titleView.setText(titleRes);
			noTitleHeader.setVisibility(View.GONE);
		}
		else {
			titleImage.setImageResource(imgRes);
			titleView.setVisibility(View.GONE);
		}
		
		if(textRes > 0 && textResParam1 > 0)
			content.setText(getContext().getResources().getString(textRes, textResParam1));
		else if(textRes > 0)
			content.setText(textRes);

		if("".equals(textString))
			detail.setVisibility(View.GONE);
		else
			detail.setText(textString);
		
		if(leftRes > 0)
			leftBtn.setBackgroundResource(leftRes);
		else 
			leftBtn.setVisibility(View.GONE);
		
		if(rightRes > 0)
			rightBtn.setBackgroundResource(rightRes);
		else 
			rightBtn.setVisibility(View.GONE);
		
		if(leftTextRes > 0) 
			leftBtn.setText(leftTextRes);
		
		if(rightTextRes > 0)
			rightBtn.setText(rightTextRes);
		
		if(mOnClickListener != null) {
			leftBtn.setOnClickListener(mOnClickListener);
			rightBtn.setOnClickListener(mOnClickListener);
		}
		
		setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				if ((keyCode ==  android.view.KeyEvent.KEYCODE_BACK)) {
					if(allowCancel) {
						if(mDialogClickListener != null) {
							int requestCode = mArg.getInt(Constants.CUSTOM_DIALOG_REQUEST_CODE, 0);
							mDialogClickListener.onClickCancel(requestCode);
						}
						return false;
					}
					else {
						return true;
					}
				}
				return false;
			}
		});
	}
	
	private View.OnClickListener mOnClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(mDialogClickListener != null) {
				int requestCode = mArg.getInt(Constants.CUSTOM_DIALOG_REQUEST_CODE, 0);
				if(v.getId() == R.id.custom_dialog_button_left) {
					mDialogClickListener.onClickCancel(requestCode);
				}
				else {
					mDialogClickListener.onClickOK(requestCode);
				}
			}
			dismiss();
		}
	};

//	private View.OnClickListener mOnClickListener = new View.OnClickListener() {
//	
//	@Override
//	public void onClick(View v) {
//		dismiss();
//		
//		final int resultCode = (v.getId() == R.id.custom_dialog_button_left) ?
//				Activity.RESULT_CANCELED:
//					Activity.RESULT_OK;
////			getOwnerActivity()
//	}
//};
}
