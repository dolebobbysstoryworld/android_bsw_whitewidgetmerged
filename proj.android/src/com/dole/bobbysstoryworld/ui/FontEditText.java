package com.dole.bobbysstoryworld.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class FontEditText extends EditText {
	
	private ImeHideListener mOnImeHideListener;
	
	public interface ImeHideListener {
		public void onImeHide();
	}
	
	public FontEditText(Context context) {
		super(context);
		FontTextHelper.setFont(getContext(), this);
	}
	
	public FontEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		FontTextHelper.setFont(getContext(), this);
	}

	public FontEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		FontTextHelper.setFont(getContext(), this);
	}
	
	public void setOnImeHideListener(ImeHideListener listener) {
		mOnImeHideListener = listener;
	}
	
	@Override
	public boolean onKeyPreIme(int keyCode, KeyEvent event) {
		InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		if(keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP && imm.isActive(this)) {
			if(mOnImeHideListener != null)
				mOnImeHideListener.onImeHide();
		}
		return super.onKeyPreIme(keyCode, event);
	}
}
