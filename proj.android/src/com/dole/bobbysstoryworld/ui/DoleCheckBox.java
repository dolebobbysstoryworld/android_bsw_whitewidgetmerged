package com.dole.bobbysstoryworld.ui;

import android.content.Context;
import android.util.AttributeSet;

public class DoleCheckBox extends FontCheckBox {
	private boolean mIsNext = false;
	
	public DoleCheckBox(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public DoleCheckBox(Context context) {
		this(context, null);
	}
	
	@Override
	public void setChecked(boolean checked) {
		super.setChecked(mIsNext);
	}
	
	public void setNext(boolean next) {
		mIsNext = next;
		setChecked(mIsNext);
	}
}
