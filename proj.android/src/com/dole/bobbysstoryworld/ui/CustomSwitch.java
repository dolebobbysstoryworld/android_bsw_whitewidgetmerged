package com.dole.bobbysstoryworld.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Switch;

import com.dole.bobbysstoryworld.R;

public class CustomSwitch extends Switch {
	private Paint mPaint;
	private int mTextHeight;
	private int mTextPadding;
	private static final String ON = "On";
	private static final String OFF = "Off"; 
	
	public CustomSwitch(Context context) {
		super(context);
		init();
	}
	
	public CustomSwitch(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public CustomSwitch(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}
	
	private void init() {
		FontTextHelper.setFont(getContext(), this);
		mTextPadding = getResources().getDimensionPixelSize(R.dimen.toggle_button_margin);
		mPaint = new Paint();
		mPaint.setTextSize(getResources().getDimensionPixelSize(R.dimen.toggle_text_size));
		mPaint.setColor(getResources().getColor(android.R.color.white));
		mPaint.setTypeface(Typeface.DEFAULT_BOLD);
		Rect bounds = new Rect();
		mPaint.getTextBounds("a", 0, 1, bounds);
		mPaint.setDither(true);
		mPaint.setAntiAlias(true);
		mTextHeight = bounds.height();
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		float textWidth;
		if(isChecked()) {
			textWidth = mPaint.measureText(ON);
			canvas.drawText(ON, mTextPadding+((canvas.getWidth()-mTextPadding)/4)-(textWidth/2), 
					(canvas.getHeight()+mTextHeight)/2, mPaint);
		}
		else {
			textWidth = mPaint.measureText(OFF);
			canvas.drawText(OFF, (((canvas.getWidth()-mTextPadding)/4)*3)-(textWidth/2), 
					(canvas.getHeight()+mTextHeight)/2, mPaint);
		}
	}
}
