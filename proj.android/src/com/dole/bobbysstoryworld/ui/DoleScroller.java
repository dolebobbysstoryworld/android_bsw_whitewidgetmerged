/**
Copyright (C) 2009 The Tiffany Framework Software, NemusTech, Inc. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
   in the documentation and/or other materials provided with the distribution.

3. The name "Tiffany" must not be used to endorse or promote products derived from this software
   without prior written permission.  For written permission, please contact mbiz@nemustech.com.

4. Products derived from this software may not be called "Tiffany",
   nor may "Tiffany" appear in their name, without prior written permission from the NemusTech, Inc.
*/

/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dole.bobbysstoryworld.ui;

import android.content.Context;
import android.hardware.SensorManager;
import android.util.FloatMath;
import android.util.Log;
import android.view.ViewConfiguration;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;

/**
 *  
 * Functional extention of {@link android.widget.OverScroller}
 * 
 * I couldn't make what i wanted by subclassing {@link android.widget.OverScroller},
 * and i copied and pasted that class and modified.
 *
 * @author rhoon@nemustech.com
 *  
 * this is the extension of the OverScroller in 2.3 Android  
 * the functions of this is added "viscous fling" and "excessive scrolling"
 * 
 * @author woosom@nemustech.com
 */

public class DoleScroller {
    private int mMode;
    private int mFlingMode = FLING_MODE_ANDROID;

    private MagneticOverScroller mScrollerX;
    private MagneticOverScroller mScrollerY;

    private final Interpolator mInterpolator;

    private static final int DEFAULT_DURATION = 250;
    private static final int SCROLL_MODE = 0;
    private static final int FLING_MODE = 1;
    
    public static final int FLING_MODE_ANDROID = 0;
	public static final int FLING_MODE_VISCOUS_FLUID = 1;
	
	private static final boolean DEBUG = false;
	private static final String TAG = "DoleScroller";

    private static float cViscousFluidScale;
    private static float cViscousFluidNormalize;

    /**
     * Creates an OverScroller with a viscous fluid scroll interpolator.
     * @param context
     */
    public DoleScroller(Context context) {
        this(context, null);
    }

    /**
     * Creates an OverScroller with default edge bounce coefficients.
     * @param context The context of this application.
     * @param interpolator The scroll interpolator. If null, a default (viscous) interpolator will
     * be used.
     */
    public DoleScroller(Context context, Interpolator interpolator) {
        this(context, interpolator, MagneticOverScroller.DEFAULT_BOUNCE_COEFFICIENT,
                MagneticOverScroller.DEFAULT_BOUNCE_COEFFICIENT, 1.0f);
    }
    
    public DoleScroller(Context context, Interpolator interpolator, float viscousDecelerateFactor) {
    	this(context, interpolator, MagneticOverScroller.DEFAULT_BOUNCE_COEFFICIENT,
                MagneticOverScroller.DEFAULT_BOUNCE_COEFFICIENT, viscousDecelerateFactor);
    }

    /**
     * Creates an OverScroller.
     * @param context The context of this application.
     * @param interpolator The scroll interpolator. If null, a default (viscous) interpolator will
     * be used.
     * @param bounceCoefficientX A value between 0 and 1 that will determine the proportion of the
     * velocity which is preserved in the bounce when the horizontal edge is reached. A null value
     * means no bounce.
     * @param bounceCoefficientY Same as bounceCoefficientX but for the vertical direction.
     * @param viscousDecelerateFactor It decelerates an interporator slow.
     */
    public DoleScroller(Context context, Interpolator interpolator,
            float bounceCoefficientX, float bounceCoefficientY, float viscousDecelerateFactor) {
    	
    	initViscousFluid();
    	
        mInterpolator = interpolator;
        mScrollerX = new MagneticOverScroller();
        mScrollerY = new MagneticOverScroller();
        MagneticOverScroller.initializeFromContext(context, viscousDecelerateFactor);

        mScrollerX.setBounceCoefficient(bounceCoefficientX);
        mScrollerY.setBounceCoefficient(bounceCoefficientY);   
    }
    
    private float viscousFluid(float x)
    {
        x *= cViscousFluidScale;
        if (x < 1.0f) {
            x -= (1.0f - (float)Math.exp(-x));
        } else {
            float start = 0.36787944117f;   // 1/e == exp(-1)
            x = 1.0f - (float)Math.exp(1.0f - x);
            x = start + x * (1.0f - start);
        }
        x *= cViscousFluidNormalize;
        return x;
    }
    
    private void initViscousFluid() {
        // This controls the viscous fluid effect (how much of it)
        cViscousFluidScale = 8.0f;
        // must be set to 1.0 (used in viscousFluid())
        cViscousFluidNormalize = 1.0f;
        cViscousFluidNormalize = 1.0f / viscousFluid(1.0f);
    }

    /**
     *
     * Returns whether the scroller has finished scrolling.
     *
     * @return True if the scroller has finished scrolling, false otherwise.
     */
    public final boolean isFinished() {
        return mScrollerX.mFinished && mScrollerY.mFinished;
    }

    /**
     * Force the finished field to a particular value. Contrary to
     * {@link #abortAnimation()}, forcing the animation to finished
     * does NOT cause the scroller to move to the final x and y
     * position.
     *
     * @param finished The new finished value.
     */
    public final void forceFinished(boolean finished) {
        mScrollerX.mFinished = mScrollerY.mFinished = finished;
    }

    /**
     * Returns the current X offset in the scroll.
     *
     * @return The new X offset as an absolute distance from the origin.
     */
    public final int getCurrX() {
        return mScrollerX.mCurrentPosition;
    }

    /**
     * Returns the current Y offset in the scroll.
     *
     * @return The new Y offset as an absolute distance from the origin.
     */
    public final int getCurrY() {
        return mScrollerY.mCurrentPosition;
    }

    /**
     * @hide
     * Returns the current velocity.
     *
     * @return The original velocity less the deceleration, norm of the X and Y velocity vector.
     */
    public float getCurrVelocity() {
        float squaredNorm = mScrollerX.mCurrVelocity * mScrollerX.mCurrVelocity;
        squaredNorm += mScrollerY.mCurrVelocity * mScrollerY.mCurrVelocity;
        return FloatMath.sqrt(squaredNorm);
    }

    /**
     * Returns the start X offset in the scroll.
     *
     * @return The start X offset as an absolute distance from the origin.
     */
    public final int getStartX() {
        return mScrollerX.mStart;
    }

    /**
     * Returns the start Y offset in the scroll.
     *
     * @return The start Y offset as an absolute distance from the origin.
     */
    public final int getStartY() {
        return mScrollerY.mStart;
    }

    /**
     * Returns where the scroll will end. Valid only for "fling" scrolls.
     *
     * @return The final X offset as an absolute distance from the origin.
     */
    public final int getFinalX() {
        return mScrollerX.mFinal;
    }

    /**
     * Returns where the scroll will end. Valid only for "fling" scrolls.
     *
     * @return The final Y offset as an absolute distance from the origin.
     */
    public final int getFinalY() {
        return mScrollerY.mFinal;
    }

    /**
     * Returns how long the scroll event will take, in milliseconds.
     *
     * @return The duration of the scroll in milliseconds.
     *
     * @hide Pending removal once nothing depends on it
     * @deprecated OverScrollers don't necessarily have a fixed duration.
     *             This function will lie to the best of its ability.
     */
    public final int getDuration() {
        return Math.max(mScrollerX.mDuration, mScrollerY.mDuration);
    }

    /**
     * Extend the scroll animation. This allows a running animation to scroll
     * further and longer, when used with {@link #setFinalX(int)} or {@link #setFinalY(int)}.
     *
     * @param extend Additional time to scroll in milliseconds.
     * @see #setFinalX(int)
     * @see #setFinalY(int)
     *
     * @hide Pending removal once nothing depends on it
     * @deprecated OverScrollers don't necessarily have a fixed duration.
     *             Instead of setting a new final position and extending
     *             the duration of an existing scroll, use startScroll
     *             to begin a new animation.
     */
    public void extendDuration(int extend) {
        mScrollerX.extendDuration(extend);
        mScrollerY.extendDuration(extend);
    }

    /**
     * Sets the final position (X) for this scroller.
     *
     * @param newX The new X offset as an absolute distance from the origin.
     * @see #extendDuration(int)
     * @see #setFinalY(int)
     *
     * @hide Pending removal once nothing depends on it
     * @deprecated OverScroller's final position may change during an animation.
     *             Instead of setting a new final position and extending
     *             the duration of an existing scroll, use startScroll
     *             to begin a new animation.
     */
    public void setFinalX(int newX) {
        mScrollerX.setFinalPosition(newX);
    }

    /**
     * Sets the final position (Y) for this scroller.
     *
     * @param newY The new Y offset as an absolute distance from the origin.
     * @see #extendDuration(int)
     * @see #setFinalX(int)
     *
     * @hide Pending removal once nothing depends on it
     * @deprecated OverScroller's final position may change during an animation.
     *             Instead of setting a new final position and extending
     *             the duration of an existing scroll, use startScroll
     *             to begin a new animation.
     */
    public void setFinalY(int newY) {
        mScrollerY.setFinalPosition(newY);
    }
    
    /**
    *
    * @param flingMode {@link #FLING_MODE_VISCOUS_FLUID} (default), {@link #FLING_MODE_ANDROID}
    */
   public void setFlingMode( int flingMode) {
       switch( flingMode) {
           case FLING_MODE_ANDROID:
               mFlingMode = flingMode;
               mScrollerX.setMode(FLING_MODE_ANDROID);
               mScrollerY.setMode(FLING_MODE_ANDROID);
               if( DEBUG) { Log.d( TAG, "setFlingMode(FLING_MODE_ANDROID)");}
               break;
           case FLING_MODE_VISCOUS_FLUID:
               mFlingMode = flingMode;
               mScrollerX.setMode(FLING_MODE_VISCOUS_FLUID);
               mScrollerY.setMode(FLING_MODE_VISCOUS_FLUID);
               if( DEBUG) { Log.d( TAG, "setFlingMode(FLING_MODE_VISCOUS_FLUID)");}
               break;
           default:
               Log.e( TAG, "setFlingMode(" + flingMode + " ) ; DISCARD Invalid Argument. ");
               break;
       }
   }
   
   public int getFlingMode() {
	   return mFlingMode;
   }

    /**
     * Call this when you want to know the new location. If it returns true, the
     * animation is not yet finished.
     */
    public boolean computeScrollOffset() {
        if (isFinished()) {
            return false;
        }

        switch (mMode) {
            case SCROLL_MODE:
            	if(DEBUG) {Log.d(TAG, "Scroll_MODE");}
                long time = AnimationUtils.currentAnimationTimeMillis();
                // Any scroller can be used for time, since they were started
                // together in scroll mode. We use X here.
                final long elapsedTime = time - mScrollerX.mStartTime;

                final int duration = mScrollerX.mDuration;
                if (elapsedTime < duration) {
                    float q = (float) (elapsedTime) / duration;

                    if (mInterpolator == null)
                        q = viscousFluid(q);
                    else
                        q = mInterpolator.getInterpolation(q);

                    mScrollerX.updateScroll(q);
                    mScrollerY.updateScroll(q);
                } else {
                    abortAnimation();
                }
                break;

            case FLING_MODE:
                if (!mScrollerX.mFinished) {                	
                    if (!mScrollerX.update()) {
                        if (!mScrollerX.continueWhenFinished()) {
                            mScrollerX.finish();
                        }
                    }
                }

                if (!mScrollerY.mFinished) {
                    if (!mScrollerY.update()) {
                        if (!mScrollerY.continueWhenFinished()) {
                            mScrollerY.finish();
                        }
                    }
                }

                break;
        }

        return true;
    }

    /**
     * Start scrolling by providing a starting point and the distance to travel.
     * The scroll will use the default value of 250 milliseconds for the
     * duration.
     *
     * @param startX Starting horizontal scroll offset in pixels. Positive
     *        numbers will scroll the content to the left.
     * @param startY Starting vertical scroll offset in pixels. Positive numbers
     *        will scroll the content up.
     * @param dx Horizontal distance to travel. Positive numbers will scroll the
     *        content to the left.
     * @param dy Vertical distance to travel. Positive numbers will scroll the
     *        content up.
     */
    public void startScroll(int startX, int startY, int dx, int dy) {
        startScroll(startX, startY, dx, dy, DEFAULT_DURATION);
    }

    /**
     * Start scrolling by providing a starting point and the distance to travel.
     *
     * @param startX Starting horizontal scroll offset in pixels. Positive
     *        numbers will scroll the content to the left.
     * @param startY Starting vertical scroll offset in pixels. Positive numbers
     *        will scroll the content up.
     * @param dx Horizontal distance to travel. Positive numbers will scroll the
     *        content to the left.
     * @param dy Vertical distance to travel. Positive numbers will scroll the
     *        content up.
     * @param duration Duration of the scroll in milliseconds.
     */
    public void startScroll(int startX, int startY, int dx, int dy, int duration) {
        mMode = SCROLL_MODE;
        mScrollerX.startScroll(startX, dx, duration);
        mScrollerY.startScroll(startY, dy, duration);
    }

    /**
     * Call this when you want to 'spring back' into a valid coordinate range.
     *
     * @param startX Starting X coordinate
     * @param startY Starting Y coordinate
     * @param minX Minimum valid X value
     * @param maxX Maximum valid X value
     * @param minY Minimum valid Y value
     * @param maxY Minimum valid Y value
     * @return true if a springback was initiated, false if startX and startY were
     *          already within the valid range.
     */
    public boolean springBack(int startX, int startY, int minX, int maxX, int minY, int maxY) {
        mMode = FLING_MODE;
        // Make sure both methods are called.
        final boolean spingbackX = mScrollerX.springback(startX, minX, maxX);
        final boolean spingbackY = mScrollerY.springback(startY, minY, maxY);
        return spingbackX || spingbackY;
    }

    public void fling(int startX, int startY, int velocityX, int velocityY,
            int minX, int maxX, int minY, int maxY) {
        fling(startX, startY, velocityX, velocityY, minX, maxX, minY, maxY, 0, 0);
    }

    /**
     * Start scrolling based on a fling gesture. The distance traveled will
     * depend on the initial velocity of the fling.
     *
     * @param startX Starting point of the scroll (X)
     * @param startY Starting point of the scroll (Y)
     * @param velocityX Initial velocity of the fling (X) measured in pixels per
     *            second.
     * @param velocityY Initial velocity of the fling (Y) measured in pixels per
     *            second
     * @param minX Minimum X value. The scroller will not scroll past this point
     *            unless overX > 0. If overfling is allowed, it will use minX as
     *            a springback boundary.
     * @param maxX Maximum X value. The scroller will not scroll past this point
     *            unless overX > 0. If overfling is allowed, it will use maxX as
     *            a springback boundary.
     * @param minY Minimum Y value. The scroller will not scroll past this point
     *            unless overY > 0. If overfling is allowed, it will use minY as
     *            a springback boundary.
     * @param maxY Maximum Y value. The scroller will not scroll past this point
     *            unless overY > 0. If overfling is allowed, it will use maxY as
     *            a springback boundary.
     * @param overX Overfling range. If > 0, horizontal overfling in either
     *            direction will be possible.
     * @param overY Overfling range. If > 0, vertical overfling in either
     *            direction will be possible.
     */
    public void fling(int startX, int startY, int velocityX, int velocityY,
            int minX, int maxX, int minY, int maxY, int overX, int overY) {
    	mMode = FLING_MODE;
        if( mFlingMode == FLING_MODE_ANDROID ) {
        	mScrollerX.flingAndroid(startX, velocityX, minX, maxX, overX);
            mScrollerY.flingAndroid(startY, velocityY, minY, maxY, overY);
        } else if( mFlingMode == FLING_MODE_VISCOUS_FLUID ) {
        	if(DEBUG) {Log.d(TAG, "fling mVelocity: "+startX+" "+startY+" ");}
        	mScrollerX.flingViscous(startX, velocityX, minX, maxX, overX);
        	mScrollerY.flingViscous(startY, velocityY, minY, maxY, overY);
        }
    	
    }
    
    /**
	 * Start scrolling based on a fling gesture. The distance traveled will
	 * depend on the initial velocity of the fling. Final position will be
	 * a multiple of specified unitX, unitY number.
     *
     * VISCOUS_FLING_MODE not supported for this fling.
	 *
	 * @param startX
	 *            Starting point of the scroll (X)
	 * @param startY
	 *            Starting point of the scroll (Y)
	 * @param velocityX
	 *            Initial velocity of the fling (X) measured in pixels per
	 *            second. One of velocityY or velocityY should be ZERO.
	 * @param velocityY
	 *            Initial velocity of the fling (Y) measured in pixels per
	 *            second. One of velocityY or velocityY should be ZERO.
	 * @param minX
	 *            Minimum X value. The scroller will not scroll past this point.
	 * @param maxX
	 *            Maximum X value. The scroller will not scroll past this point.
	 * @param minY
	 *            Minimum Y value. The scroller will not scroll past this point.
	 * @param maxY
	 *            Maximum Y value. The scroller will not scroll past this point.
	 * @param unitX
	 *            X scroll unit. Final x position will be unitPivotX + N * unitX.
	 *            If this value < 2 or velocityX is ZERO, it's ignored.
	 * @param unitY
	 *            Y scroll unit. Final y position will be unitPivotY + N * unitY.
	 *            If this value < 2 or velocityY is ZERO, it's ignored.
	 * @param unitPivotX
	 *            X base position to make final x position to be multiple of unitX
	 * @param unitPivotY
	 *            Y base position to make final y position to be multiple of unitY
	 */
	public void fling(int startX, int startY, int velocityX, int velocityY,
			int minX, int maxX, int minY, int maxY, int unitX, int unitY,
			int unitPivotX, int unitPivotY) {

		mMode = FLING_MODE;
        
        mScrollerX.flingAndroid(startX, velocityX, minX, maxX, unitX, unitPivotX);
        mScrollerY.flingAndroid(startY, velocityY, minY, maxY, unitY, unitPivotY);
        
	}
    

    /**
     * Notify the scroller that we've reached a horizontal boundary.
     * Normally the information to handle this will already be known
     * when the animation is started, such as in a call to one of the
     * fling functions. However there are cases where this cannot be known
     * in advance. This function will transition the current motion and
     * animate from startX to finalX as appropriate.
     *
     * @param startX Starting/current X position
     * @param finalX Desired final X position
     * @param overX Magnitude of overscroll allowed. This should be the maximum
     *              desired distance from finalX. Absolute value - must be positive.
     */
    public void notifyHorizontalEdgeReached(int startX, int finalX, int overX) {
        mScrollerX.notifyEdgeReached(startX, finalX, overX);
    }

    /**
     * Notify the scroller that we've reached a vertical boundary.
     * Normally the information to handle this will already be known
     * when the animation is started, such as in a call to one of the
     * fling functions. However there are cases where this cannot be known
     * in advance. This function will animate a parabolic motion from
     * startY to finalY.
     *
     * @param startY Starting/current Y position
     * @param finalY Desired final Y position
     * @param overY Magnitude of overscroll allowed. This should be the maximum
     *              desired distance from finalY.
     */
    public void notifyVerticalEdgeReached(int startY, int finalY, int overY) {
        mScrollerY.notifyEdgeReached(startY, finalY, overY);
    }

    /**
     * Returns whether the current Scroller is currently returning to a valid position.
     * Valid bounds were provided by the
     * {@link #fling(int, int, int, int, int, int, int, int, int, int)} method.
     *
     * One should check this value before calling
     * {@link #startScroll(int, int, int, int)} as the interpolation currently in progress
     * to restore a valid position will then be stopped. The caller has to take into account
     * the fact that the started scroll will start from an overscrolled position.
     *
     * @return true when the current position is overscrolled and in the process of
     *         interpolating back to a valid value.
     */
    public boolean isOverScrolled() {
        return ((!mScrollerX.mFinished &&
                mScrollerX.mState != MagneticOverScroller.TO_EDGE) ||
                (!mScrollerY.mFinished &&
                        mScrollerY.mState != MagneticOverScroller.TO_EDGE));
    }

    /**
     * Stops the animation. Contrary to {@link #forceFinished(boolean)},
     * aborting the animating causes the scroller to move to the final x and y
     * positions.
     *
     * @see #forceFinished(boolean)
     */
    public void abortAnimation() {
        mScrollerX.finish();
        mScrollerY.finish();
    }

    /**
     * Returns the time elapsed since the beginning of the scrolling.
     *
     * @return The elapsed time in milliseconds.
     *
     * @hide
     */
    public int timePassed() {
        final long time = AnimationUtils.currentAnimationTimeMillis();
        final long startTime = Math.min(mScrollerX.mStartTime, mScrollerY.mStartTime);
        return (int) (time - startTime);
    }
    
    static class MagneticOverScroller {
        // Initial position
        int mStart;

        // Current position
        int mCurrentPosition;

        // Final position
        int mFinal;

        // Initial velocity
        int mVelocity;

        // Current velocity
        float mCurrVelocity;

        // Constant current deceleration
        float mDeceleration;

        // Animation starting time, in system milliseconds
        long mStartTime;

        // Animation duration, in milliseconds
        int mDuration;

        // Whether the animation is currently in progress
        boolean mFinished;

        // Constant gravity value, used to scale deceleration
        static float GRAVITY;

        static void initializeFromContext(Context context, float viscousDecelerateFactor) {
            final float ppi = context.getResources().getDisplayMetrics().density * 160.0f;
            float vdf = viscousDecelerateFactor > 0.0f ? viscousDecelerateFactor : 1.0f;
            GRAVITY = SensorManager.GRAVITY_EARTH // g (m/s^2)
                    * 39.37f // inch/meter
                    * ppi // pixels per inch
                    * ViewConfiguration.getScrollFriction() * vdf;
        }

        private static final int TO_EDGE = 0;
        private static final int TO_BOUNDARY = 1;
        private static final int TO_BOUNCE = 2;
        
        private static final int FLING_MODE_ANDROID = 0;
        private static final int FLING_MODE_VISCOUS_FLUID = 1;

        private int mState = TO_EDGE;
        private int mMode = FLING_MODE_VISCOUS_FLUID;

        // The allowed overshot distance before boundary is reached.
        private int mOver;

        // Duration in milliseconds to go back from edge to edge. Springback is half of it.
        private static final int OVERSCROLL_SPRINGBACK_DURATION = 200;

        // Oscillation period
        private static final float TIME_COEF =
            1000.0f * (float) Math.PI / OVERSCROLL_SPRINGBACK_DURATION;

        // If the velocity is smaller than this value, no bounce is triggered
        // when the edge limits are reached (would result in a zero pixels
        // displacement anyway).
        private static final float MINIMUM_VELOCITY_FOR_BOUNCE = Float.MAX_VALUE;//140.0f;

        // Proportion of the velocity that is preserved when the edge is reached.
        private static final float DEFAULT_BOUNCE_COEFFICIENT = 0.16f;

		private static final boolean DEBUG = false;
		private static final String TAG = "TFMagneticOverScroller";

        private float mBounceCoefficient = DEFAULT_BOUNCE_COEFFICIENT;

        // Added for TF fling
		private float mViscousFluidScale;
		private float mVelocityViscousScaled;

		private final static float VISCOUS_FLING_DURATION = 4.0f;           // seconds for calculation
		private final static float VISCOUS_FLING_DURATION_COEFF = 0.75f;    // 99.9% of fling ends in 3 secs
		
		private float mDurationReciprocal;

		private int mMin;
		private int mMax;
		
		private int mExccessiveStart;

		// Added for fling using the "unit" and the "unitPivot"
		private float mCorrectionDeceleration;
		
        MagneticOverScroller() {
            mFinished = true;
        }

        void updateScroll(float q) {
        	
            mCurrentPosition = mStart + Math.round(q * (mFinal - mStart));
            	
            if(DEBUG) {Log.d(TAG, "updateScroll " + mCurrentPosition);}
        }

        /*
         * Get a signed deceleration that will reduce the velocity.
         */
        static float getDeceleration(int velocity) {
            return velocity > 0 ? -GRAVITY : GRAVITY;
        }

        /*
         * Returns the time (in milliseconds) it will take to go from start to end.
         */
        static int computeDuration(int start, int end, float initialVelocity, float deceleration) {
            final int distance = start - end;
            final float discriminant = initialVelocity * initialVelocity - 2.0f * deceleration
                    * distance;
            if (discriminant >= 0.0f) {
                float delta = (float) Math.sqrt(discriminant);
                if (deceleration < 0.0f) {
                    delta = -delta;
                }
                return (int) (1000.0f * (-initialVelocity - delta) / deceleration);
            }

            // End position can not be reached
            return 0;
        }

        void startScroll(int start, int distance, int duration) {
        	
            mFinished = false;

            mStart = start;
            mFinal = start + distance;

            mStartTime = AnimationUtils.currentAnimationTimeMillis();
            mDuration = duration;

            // Unused
            mDeceleration = 0.0f;
            mVelocity = 0;
            
            // Added for TF fling
            mViscousFluidScale = 8.0f;
            mDurationReciprocal = 1.0f / (float) mDuration;
            
            if(DEBUG) Log.d(TAG, "startScroll " + start + " " + distance + " " + duration);
        }

        void fling(int start, int velocity, int min, int max) {
            mFinished = false;

            mStart = start;
            mStartTime = AnimationUtils.currentAnimationTimeMillis();

            mVelocity = velocity;

            mDeceleration = getDeceleration(velocity);

            // A start from an invalid position immediately brings back to a valid position
            if (mStart < min) {
                mDuration = 0;
                mFinal = min;
                return;
            }

            if (mStart > max) {
                mDuration = 0;
                mFinal = max;
                return;
            }

            // Duration are expressed in milliseconds
            mDuration = (int) (-1000.0f * velocity / mDeceleration);

            mFinal = start - Math.round((velocity * velocity) / (2.0f * mDeceleration));

            // Clamp to a valid final position
            if (mFinal < min) {
                mFinal = min;
                mDuration = computeDuration(mStart, min, mVelocity, mDeceleration);
            }

            if (mFinal > max) {
                mFinal = max;
                mDuration = computeDuration(mStart, max, mVelocity, mDeceleration);
            }
        }

        void finish() {
            mCurrentPosition = mFinal;
            // Not reset since WebView relies on this value for fast fling.
            // mCurrVelocity = 0.0f;
            mFinished = true;
        }

        void setFinalPosition(int position) {
            mFinal = position;
            mFinished = false;
        }

        void extendDuration(int extend) {
            final long time = AnimationUtils.currentAnimationTimeMillis();
            final int elapsedTime = (int) (time - mStartTime);
            mDuration = elapsedTime + extend;
            mFinished = false;
        }

        void setBounceCoefficient(float coefficient) {
            mBounceCoefficient = coefficient;
        }

        boolean springback(int start, int min, int max) {
            mFinished = true;

            mStart = start;
            mVelocity = 0;

            mStartTime = AnimationUtils.currentAnimationTimeMillis();
            mDuration = 0;

            if (start < min) {
                startSpringback(start, min, false);
            } else if (start > max) {
                startSpringback(start, max, true);
            } else {
            	mCurrentPosition = start;
            }

            return !mFinished;
        }

        /* 
         * TF startSpringback
         * originally, "startSpringback" does'nt have mExccessiveStart value.  
         * mExccessiveStart means the size from the edge to the overscroll end  
         *          
         */
        
        private void startSpringback(int start, int end, boolean positive) {
            mFinished = false;
            mState = TO_BOUNCE;
            mStart = mFinal = end;
            mDuration = OVERSCROLL_SPRINGBACK_DURATION;
            mStartTime -= OVERSCROLL_SPRINGBACK_DURATION / 2;
            mVelocity = (int) (Math.abs(end - start) * TIME_COEF * (positive ? 1.0 : -1.0f));
            
            
            mExccessiveStart = start - end;
        }
        
        void flingAndroid(int start, int velocity, int min, int max, int unit, int unitPivot) {
        	
        	mState = TO_EDGE;
            mMode = FLING_MODE_ANDROID;
       	
    		// First, call fling() without unitX, unitY, unitPivotX, unitPivotY
    		flingAndroid(start, velocity, Integer.MIN_VALUE, Integer.MAX_VALUE, 0);

    		int _final = mFinal;

    		mMin = min;
    		mMax = max;
    		
    		if(DEBUG) {
    			Log.d(TAG, "fling(int * 12) 1, final = "+ _final + " " + mStart + " " + min + " " + max + " " + mDuration);
    		}
    		
			if (min < max && unit > 1 && velocity != 0) {

    			if (_final < min) {
    				_final = min;
    			}
    			if (_final > max) {
    				_final = max;
    			}

    			if (velocity < 0) {
    				while (_final > min) {
    					if ((_final - unitPivot) % unit == 0)
    						break;
    					--_final;
    				}
    			} else {
    				while (_final < max) {
    					if ((_final - unitPivot) % unit == 0)
    						break;
    					++_final;
    				}
    			}

    			if (_final != mFinal ) {
    				mCorrectionDeceleration = Math.abs(mFinal - start) / (float) Math.abs(_final - start);
    				if( DEBUG ) {
    					Log.d(TAG, "fling(), startX = "+start+", mFinal = " + mFinal
    							+ ", final = " + _final + ", mCorrectionDeceleration = "
    							+ mCorrectionDeceleration);
    				}
    				mFinal = _final;
    				if(velocity < 0) {
    					mMin = mFinal;
    				} else {
    					mMax = mFinal;
    				}
    				if( DEBUG ) {
    					Log.d(TAG, "startX = "+start+", mFinal = " + mFinal
    							+ ", mMin = " + mMin + ", mMax = "+mMax);
    				}
    			}
    		}

			if(DEBUG) {
    			Log.d(TAG, "fling(int * 12) 2, final = "+ _final + " " + mStart + " " + min + " " + max);
    		}

    		// adjust duration value
    		if( mCorrectionDeceleration != 1.0f ) {
    			if( mCorrectionDeceleration != 1.0f) {
    				// there is different with the previous scroller before 2.3 
    				// mDuration have to be "+"   
    				mDuration = (int) Math.abs((1000 * Math.abs(velocity) / (mDeceleration*mCorrectionDeceleration)));
    			}
    			mDurationReciprocal = 1.0f / (float) mDuration;
    		}
        }

        /* 
         * original "fling" method in Android
         */
        void flingAndroid(int start, int velocity, int min, int max, int over) {
            mState = TO_EDGE;
            mMode = FLING_MODE_ANDROID;
            mOver = over;

            mFinished = false;

            mStart = start;
            mStartTime = AnimationUtils.currentAnimationTimeMillis();

            mVelocity = velocity;

            mDeceleration = getDeceleration(velocity);

            // Duration are expressed in milliseconds
            mDuration = (int) (-1000.0f * velocity / mDeceleration);
            
            int distance = Math.round((velocity * velocity) / (2.0f * mDeceleration));
            mFinal = start - distance;

            if(DEBUG) {Log.d(TAG, "flingAndroid mVelocity:"+mVelocity+" "+mFinal+ " " +distance);}
            
            // Clamp to a valid final position
            if (mFinal < min) {
                mFinal = min;
                mDuration = computeDuration(mStart, min, mVelocity, mDeceleration);
            }

            if (mFinal > max) {
                mFinal = max;
                mDuration = computeDuration(mStart, max, mVelocity, mDeceleration);
            }

            if (start > max) {
                if (start >= max + over) {
                	if(DEBUG) Log.d(TAG, "flingAndroid 1");
                    springback(max + over, min, max);
                } else {
                    if (velocity <= 0) {
                    	if(DEBUG) Log.d(TAG, "flingAndroid 2");
                        springback(start, min, max);
                    } else {
                    	if(DEBUG) Log.d(TAG, "flingAndroid 3");
                        long time = AnimationUtils.currentAnimationTimeMillis();
                        final double durationSinceEdge =
                            Math.atan((start-max) * TIME_COEF / velocity) / TIME_COEF;
                        mStartTime = (int) (time - 1000.0f * durationSinceEdge);

                        // Simulate a bounce that started from edge
                        mStart = max;

                        mVelocity = (int) (velocity / Math.cos(durationSinceEdge * TIME_COEF));

                        onEdgeReached();
                    }
                }
            } else {
                if (start < min) {
                    if (start <= min - over) {
                    	if(DEBUG) Log.d(TAG, "flingAndroid 4");
                        springback(min - over, min, max);
                    } else {
                        if (velocity >= 0) {
                        	if(DEBUG) Log.d(TAG, "flingAndroid 5");
                            springback(start, min, max);
                        } else {
                        	if(DEBUG) Log.d(TAG, "flingAndroid 6");
                            long time = AnimationUtils.currentAnimationTimeMillis();
                            final double durationSinceEdge =
                                Math.atan((start-min) * TIME_COEF / velocity) / TIME_COEF;
                            mStartTime = (int) (time - 1000.0f * durationSinceEdge);

                            // Simulate a bounce that started from edge
                            mStart = min;

                            mVelocity = (int) (velocity / Math.cos(durationSinceEdge * TIME_COEF));

                            onEdgeReached();
                        }

                    }
                }
            }
            
            mCorrectionDeceleration = 1.0f;
        }
        
        /* 
         * TF flingViscous
         * this is the implementation of viscous fling  
         * almost the same with flingAndroid but assigned appropriate values for flingViscous   
         *          
         */
        void flingViscous(int start, int velocity, int min, int max, int over) {
			mState = TO_EDGE;
			mMode = FLING_MODE_VISCOUS_FLUID;
			mFinished = false;
			
			// scaled for viscousFluidFling
			// you can see more information there 
			mVelocity = velocity;
			mViscousFluidScale = 10.0f;
			mVelocityViscousScaled = velocity / mViscousFluidScale;
			
			mStartTime = AnimationUtils.currentAnimationTimeMillis();
			mStart = start;
			
			// you can get the total distance when you give the 1.0 viscousFluidfling
			// you can see more information there
			int totalDistance = (int) (viscousFluidFling(1.0f) * VISCOUS_FLING_DURATION);
			
			mDuration = (int)(VISCOUS_FLING_DURATION * 1000); // Duration is in milliseconds
			mDurationReciprocal = 1.0f / (float) mDuration;
			
			{ // TRICKY  for faster stop;
				if( totalDistance > 10) { totalDistance -= 2;}
				mDuration *= VISCOUS_FLING_DURATION_COEFF;      // most fling finished in time
			}
			
			mMin = min;
			mMax = max;
			
			mFinal = start + Math.round(totalDistance);
			if(DEBUG) {Log.d(TAG, "flingViscous mVelocity:"+mVelocity+" "+ start +" "+mMax+" "+mMin+" "+totalDistance);}
			if(DEBUG) {Log.d(TAG, "flingViscous mfinal: "+mFinal);}
			// Pin to mMinX <= mFinalX <= mMaxX
			mFinal = Math.min(mFinal, mMax);
			mFinal = Math.max(mFinal, mMin);

			
			// for preventing this scroller from abnormal actions when you scroll at the edge of the view,
			// this checks whether the "start" is over the "min" or the "max"
			// flingAndroid does more checks there, but this doesn't 
			
			// springback's the "start" parameter changed,
			// because the view is shot to the max of the over scrolling if does android fling.
			// this version is shot to the distance calculated from velocity, so did it. 
			if (start > max) {
				springback(start, min, max);
            } else {
    			if (start < min) {
                	springback(min + start, min, max);
                }
            } 
			
			if(DEBUG) {Log.d(TAG, "flingViscous mVelocity:"+mVelocity+" "+mStart+" "+mFinal+" "+totalDistance);}
		}

        void notifyEdgeReached(int start, int end, int over) {
            mDeceleration = getDeceleration(mVelocity);

            // Local time, used to compute edge crossing time.
            float timeCurrent = mCurrVelocity / mDeceleration;
            final int distance = end - start;
            float timeEdge = -(float) Math.sqrt((2.0f * distance / mDeceleration)
                    + (timeCurrent * timeCurrent));

            mVelocity = (int) (mDeceleration * timeEdge);

            // Simulate a symmetric bounce that started from edge
            mStart = end;

            mOver = over;

            long time = AnimationUtils.currentAnimationTimeMillis();
            mStartTime = (int) (time - 1000.0f * (timeCurrent - timeEdge));

            onEdgeReached();
        }

        private void onEdgeReached() {
        	
        	// original from android
        	if( mMode == FLING_MODE_ANDROID ) {
        		// mStart, mVelocity and mStartTime were adjusted to their values when edge was reached.
                final float distance = mVelocity / TIME_COEF;
                
                if (Math.abs(distance) < mOver) {
                	if(DEBUG) {Log.d(TAG, "onEdgeReached FLING_MODE_ANDROID distance < mOver");}	
                    // Spring force will bring us back to final position
                    mState = TO_BOUNCE;
                    mFinal = mStart;
                    mDuration = OVERSCROLL_SPRINGBACK_DURATION;
                } else {
                	if(DEBUG) {Log.d(TAG, "onEdgeReached FLING_MODE_ANDROID velocity too high");}
                    // Velocity is too high, we will hit the boundary limit
                    mState = TO_BOUNDARY;
                    int over = mVelocity > 0 ? mOver : -mOver;
                    mFinal = mStart + over;
                    mDuration = (int) (1000.0f * Math.asin(over / distance) / TIME_COEF);
                }
        	} else if( mMode == FLING_MODE_VISCOUS_FLUID ) {
        		// for TF fling and excessive scrolling 
        		
        		// actually, TF fling and excessvie scrolling is doing differently with android fling
        		// "android fling" does a job by cases
        		// 1. Velocity is not high, we will not hit the boundary limit
        		// 2. Velocity is too high, we will hit the boundary limit
        		//
        		// in TF scrolling, just start "TO_BOUNDARY" state which does excessvie scrolling
        		mState = TO_BOUNDARY;
        	}
            
        }

        boolean continueWhenFinished() {
        	int duration = (int) (-1000.0f * mVelocity / mDeceleration);

        	// original from android
        	if( mMode == FLING_MODE_ANDROID ) {
        		switch (mState) {
                case TO_EDGE:
                    // Duration from start to null velocity
                    
                    if (mDuration < duration) {
                        // If the animation was clamped, we reached the edge
                        mStart = mFinal;
                        // Speed when edge was reached
                        mVelocity = (int) (mVelocity + mDeceleration * mDuration / 1000.0f);
                        mStartTime += mDuration;
                        onEdgeReached();
                    } else {
                        // Normal stop, no need to continue
                        return false;
                    }
                    break;
                case TO_BOUNDARY:
                    mStartTime += mDuration;
                    startSpringback(mFinal, mFinal - (mVelocity > 0 ? mOver:-mOver), mVelocity > 0);
                    break;
                case TO_BOUNCE:
                    //mVelocity = (int) (mVelocity * BOUNCE_COEFFICIENT);
                    mVelocity = (int) (mVelocity * mBounceCoefficient);
                    if (Math.abs(mVelocity) < MINIMUM_VELOCITY_FOR_BOUNCE) {
                        return false;
                    }
                    mStartTime += mDuration;
                    break;
 
        		}
        	} else if( mMode == FLING_MODE_VISCOUS_FLUID ) {
        		// for TF fling and exccessive scrolling 
        		switch (mState) {
        			case TO_EDGE:

        			// check a passed time
        			// this is completely differnt with android since android time is hard to understand
        			// so, here just think commonly, timepassed is passed time
                	int timePassed = (int)(AnimationUtils.currentAnimationTimeMillis() - mStartTime);
                	// if the time remains, doing the job which have to do over the edge
                	// or just end
                	if( timePassed < mDuration ) {
                		
                		// velocity of last 30 milliseconds
                		// you can get that velocity anywhere 
                		// since viscousFluidFling is a constant graph during velocity is changed
                		int timeLastCheckpoint = (timePassed > 30) ? timePassed - 30 : 0;
                        float curOffset = viscousFluidFling( (float) timePassed * mDurationReciprocal);
                        float lastCheckpoint = viscousFluidFling( (float) timeLastCheckpoint * mDurationReciprocal);
                        curOffset = Math.round( curOffset * VISCOUS_FLING_DURATION);
                        lastCheckpoint = Math.round( lastCheckpoint * VISCOUS_FLING_DURATION);
                        float pixelPerSec = (float) (curOffset - lastCheckpoint) / (float) (timePassed - timeLastCheckpoint) * 1000.0f;
                		
                        if( pixelPerSec == 0 ) {
                        	mFinished = true;
                        	mFinal = mStart;
                        	return false;
                        }
                        
                		// If the animation was clamped, we reached the edge
                        mStart = mFinal;
                        // Speed when edge was reached
                        mVelocity = (int)pixelPerSec;
                        mVelocityViscousScaled = mVelocity/mViscousFluidScale;
                        
                        long start = mStartTime;
                        mStartTime += timePassed;
                        
                        if(DEBUG) Log.d(TAG, "continueWhenFinished TO_EDGE timePassed < mDuration 1:"+timePassed+" "+mDuration+" "+duration);
                        if(DEBUG) Log.d(TAG, "continueWhenFinished TO_EDGE timePassed < mDuration 2:"+start+" "+mStart+" "+mVelocity+" "+mStartTime);
                        
                        onEdgeReached();
                        
                	} else {                		
                		if(DEBUG) Log.d(TAG, "continueWhenFinished TO_EDGE normalstop:"+timePassed+" "+mDuration+" "+duration);
                		return false;
                	}
                	break;
                	// this is called when TO_BOUNDARY, TO_BOUNCE is reached at the edge
                	// so just end.
        			case TO_BOUNDARY:
        			case TO_BOUNCE:
        				return false;
        		}
        	}
      
            update();
            return true;
        }

        /*
         * Update the current position and velocity for current time. Returns
         * true if update has been done and false if animation duration has been
         * reached.
         */
        boolean update() {
        	
        	final long time = AnimationUtils.currentAnimationTimeMillis();
            final long duration = time - mStartTime;
            double distance = 0.0f;
            final float t = duration / 1000.0f;
        	
            
        	// original from android
        	if( mMode == FLING_MODE_ANDROID ) {

        		if(DEBUG) Log.d(TAG, "update FLING_MODE_ANDROID " + duration + " " + mDuration + " " + mCurrentPosition );
                
        		if (duration > mDuration) {
                    return false;
                }

                if (mState == TO_EDGE) {
                	
                    mCurrVelocity = mVelocity + mDeceleration *  t;
                    distance = mVelocity * t + mDeceleration * mCorrectionDeceleration * t * t / 2.0f;
                    if(DEBUG) Log.d(TAG, "update TO_EDGE " + mCurrentPosition + " " + distance);
                    
                } else {
                	if(DEBUG) Log.d(TAG, "update else");
                    final float d = t * TIME_COEF;
                    mCurrVelocity = mVelocity * (float)Math.cos(d);
                    distance = mVelocity / TIME_COEF * Math.sin(d);
                
                }
                
                mCurrentPosition = mStart + (int) distance;
                
                return true;
                
        	} else if( mMode == FLING_MODE_VISCOUS_FLUID ) {
        	// update for TF fling and exccessive scrolling
        	/*        	  
        	 * actually, TF fling has 3 states TO_EDGE, TO_BOUNCE and TO_BOUNDARY like android fling.
        	 * but each states are not the same with android
        	 * TO_EDGE : from the inside of the child to the edge (same with android)
        	 * TO_BOUNCE : from the over scrolling max to the edge (like springBack case)
        	 * TO_BOUNDARY : from the edge to the edge (when the scroll is over the edge slightly)   
        	 * 
        	 */
        		
        		if( mState == TO_EDGE ) {
        			
                	float x = (float) duration * mDurationReciprocal;                	
                    float viscousDistance = viscousFluidFling(x);
                    float curOffset = (int)Math.round(viscousDistance * VISCOUS_FLING_DURATION);
                    distance =  curOffset * (int)Math.signum(mFinal-mStart);
                    
                    /*
                     * Update Current velocity 
                     */
                    int timeLastCheckpoint = (duration > 30) ? (int)duration - 30 : 0;
        			float lastCheckpoint = viscousFluidFling( (float)timeLastCheckpoint * mDurationReciprocal);
        			lastCheckpoint = Math.round( lastCheckpoint * VISCOUS_FLING_DURATION);
                    
                    float pixelPerSec = (float) (curOffset - lastCheckpoint) / (float) (duration - timeLastCheckpoint) * 1000.0f;
                    mCurrVelocity = pixelPerSec;
       
                    if( mVelocity < 0 )
                    	distance = - distance;
                    
                    if(DEBUG) {Log.d(TAG, "update TO_EDGE: mVelocity:"+mVelocity+" mCurrentPosition:"+mCurrentPosition+" " +
                    		" mStart:"+mStart+" mFinal:"+mFinal+ " mViscousDistance:"+viscousDistance+" distance:"+distance+" duration:"+duration+ " x:" +x);}
          
                    // different exit condition with android fling that does for "duration"
                	// 1. check duration 
                    if (duration > mDuration || mMin == mMax) {
                        return false;
                    }

                    // different exit condition with android fling that does for "duration"
                	// 2. reached at the edge 
                    int nextPosition = mStart + (int)distance;
                    if( nextPosition < mMin || mMax < nextPosition ) {
                		return false;
                    }
                    
                    // exit condition
                    // 3. we dont want to scroll than "mFinal".
                    if( (mVelocity > 0 && nextPosition > mFinal)
                            || (mVelocity < 0 && nextPosition < mFinal)) {
                        distance = mFinal - mStart;
                    }
        		} else if( mState == TO_BOUNCE ){ 

        			distance = (int)criticalDamping(mExccessiveStart, 0, duration);
        			
        			/*
                     * Update Current velocity 
                     */
                    int timeLastCheckpoint = (duration > 30) ? (int)duration - 30 : 0;
                    double lastCheckpoint = (int)criticalDamping( mExccessiveStart, 0, (float)timeLastCheckpoint);
                    float pixelPerSec;
        			float diffTime = (float)(duration - timeLastCheckpoint);
        			if( diffTime == 0.0f ) 
        				pixelPerSec = mCurrVelocity;
        			else 
        				pixelPerSec = (float) (distance - lastCheckpoint) / diffTime * 1000.0f;
        			
        			mCurrVelocity = pixelPerSec;
        			
                    if(DEBUG) Log.d(TAG, "update TO_BOUNCE " + distance + " " + mExccessiveStart + " " + mFinal + " " + t + " " + t);
                    
                    if(DEBUG) Log.d(TAG, "update TO_BOUNCE " + mCurrVelocity);
                    
                    // different exit condition with android fling that does for "duration"
                	// do again until reached at the edge
                    if( distance == 0.0f ) 
                		return false;
                    
                } else if( mState == TO_BOUNDARY) {
                	
        			distance = (int)criticalDamping( 0, mVelocity, duration);
                	if(DEBUG) Log.d(TAG, "update RECOVER_FROM_DECELERATE " + mCurrentPosition+ " " + mStart +" "+ distance + " " + mVelocity + " " + t);
                	
                	/*
                     * Update Current velocity 
                     */
                    long timeLastCheckpoint = (duration > 30) ? (int)duration - 30 : 0;
        			double lastCheckpoint = (int)criticalDamping( 0, mVelocity, timeLastCheckpoint);
        			float pixelPerSec;
        			float diffTime = (float)(duration - timeLastCheckpoint);
        			if( diffTime == 0.0f ) 
        				pixelPerSec = mCurrVelocity;
        			else 
        				pixelPerSec = (float) (distance - lastCheckpoint) / diffTime * 1000.0f;
        			
        			
        			mCurrVelocity = pixelPerSec;
        			
                	// different exit condition with android fling that does for "duration"
                	// do again until reached at the edge
                	if( distance == 0.0f )
                		return false;
                	
                }
        		
        		mCurrentPosition = mStart + (int)distance;
        		
        		return true;
        	}
        	
        	return false;
        }
        
        /*
        *
        * Logical background of this implementation is the "Critical damping" case of 
        * "A damped spring mass system" (http://en.wikipedia.org/wiki/Damping)
        *
        * 2010.11.01
        * swhan 
        * rhoon
        *
        * this modified that x0 is provided by scroller in CK_STATIC case
        * that's why this is not a runnable but just a method
        * if you want to understand this method, you should draw the graph of this function
        * it makes the graph of the critical damping
        * 2011.02.09
        * kdy
        */
        
        private float criticalDamping( float x0, float flingSpeed, float t ) {
        	
        	final float CK_STATIC = 200.0f;
            final float CK_DYNAMIC = 100.0f;
        	
        	float Ck = 100f;            // spring constant            
            float Cm = 1.0f;            // mass
            float CMAXt = 0.0f;
            
            final int EXCESS_SCROLL_RECOVER_DURATION = 1000;
            final int EXCESS_SCROLL_RECOVER_FROM_DECELERATION_DURATION = 1000;
            
        	if(flingSpeed == 0.0f) {
        	    CMAXt = (float)EXCESS_SCROLL_RECOVER_DURATION;
                Ck = CK_STATIC;
                //x0 = mExcessScroll;
//                if(DEBUG) Log.d(TAG, "getCal flingSpeed == 0 " + x0);
            } else {                
                CMAXt = (float)EXCESS_SCROLL_RECOVER_FROM_DECELERATION_DURATION;
                Ck = CK_DYNAMIC;
                x0 = 0.0f;
//                if(DEBUG) Log.d(TAG, "getCal flingSpeed != 0");
            }

        	float normalizedT = t/CMAXt;
        	float w0 = (float) Math.sqrt(Ck / Cm);
            float xdot0 = 2.0f * flingSpeed / Cm;
        	
            float res = (x0 + (xdot0 + w0 * x0) * normalizedT) * (float)Math.exp( -1.f * w0 * normalizedT);
   
            return res;
            
        }
        
        
        /*
         * Mass-Spring-Damper model (http://en.wikipedia.org/wiki/Damping) 에서
         * Spring(k) 무시할수 있는 작은 값으로 설정하고
         * 위치0에서 초기속도 x10(x dot zero)으로 입력하여 설정한다
         *
         * @param x : -oo ~ 0 ~ 1 : 0~1만 사용할 것을 권장
         * @return  -oo ~ 0 ~ mVelocity/10 : 0~mVelocity/10만 사용할 것을 권장
         * 
         * 이 함수는 그래프를 그려보면 명확하다
         * x가 0~1일때 0부터 (initialVelocity로 지정한 값/10)의 값으로 수렴한다(critical damping)
         * x가 1 이후엔 거의 (initialVelocity로 지정한 값/10)의 값으로 수렴하므로 의미가 없다고 볼 수 있다
         * commented by kdy
         * --
         * 
         * for using this,
         * from Mass-Spring-Damper model (http://en.wikipedia.org/wiki/Damping)
         * set Spring(k) a little value which can be ignored,
         * and set initial velocity * 10 (x dot zero) where location 0
         *
         * @param x : -oo ~ 0 ~ 1 : recommend to just use between 0~1
         * @return  -oo ~ 0 ~ mVelocity/10 : recommend to just use between 0~mVelocity/10
         * 
         * you are clear to understand this function when you draw the graph
         * when x is 0~1, this value is convergence from 0 to initialVelocity/10
         * when x is after 1, this value is convergence to initialVelocity/10, so not meaningful 
         * --
         */
        private float viscousFluidFling(float x) {
            // ==================== ORIGINAL CALC ====================
            //        final float k = 0.001f;
            //        final float m = 1f;
            //        final float w0 = (float)Math.sqrt( k/m);
            //        final float x0 = 0f;   // initial position
            //        final float x10 = mVelocity / 10.0f; // initial velocity ; 10 as base; x10 = 10 -> y = 1;
            //        final float cc = 10f;
            //        final float zeta = cc / (2f * (float)Math.sqrt( k * m));
            //        final float a = 1f;
            //        final float b = 2f* zeta*w0;
            //        final float c = w0*w0;
            //        final float _rc0 = (float)Math.sqrt(b*b-4*a*c);
            //        final float r1 = (-1f*b + _rc0) / 2*a;
            //        final float r2 = (-1f*b - _rc0) / 2*a;
            //        final float _ab1 = (r1 * x0 - x10) / (r2 - r1);
            //        final float a1 = x0 + _ab1;
            //        final float b1 = -1f * _ab1;
            //        float y = a1 * (float)Math.exp(r1*x)  + b1 * (float)Math.exp(r2*x);
            //        y *= mViscousFluidScale;
            //        Log.d("CCCCCCCCCC", " w0 = "+w0 + " zeta = " + zeta + " a=" + a + " b = " + b + " c = " + c);
            //        Log.d("CCCCCCCCCC", " rc0 = "+_rc0 + " r1 = " + r1 + " r2= " + r2 + " ab1 = " + _ab1 + " a1= " + a1 + " b1 = "+ b1 + " r2-r1 =" + (r2-r1));

            //        11-27 23:01:44.118: DEBUG/CCCCCCCCCC(23427):  w0 = 0.03162278 zeta = 158.11388 a=1.0 b = 10.0 c = 0.0010000002
            //        11-27 23:01:44.118: DEBUG/CCCCCCCCCC(23427):  rc0 = 9.9998 r1 = -1.001358E-4 r2= -9.9999 ab1 = 35.230705 a1= 35.230705 b1 = -35.230705 r2-r1 =-9.9998


            // ==================== GO AS FAR AS CALC ====================
            //        final float k = 0.001f;
            //        final float m = 1f;
            //        final float w0 = 0.03162278f; // (float)Math.sqrt( k/m);
            //        final float x0 = 0f;   // initial position
            //        final float x10 = mVelocityViscousScaled; // mVelocity / 10.0f; // initial velocity ; 10 as base; x10 = 10 -> y = 1;
            //        final float cc = 10.0f;
            //        final float zeta = 158.11388f; // cc / (2.f * (float)Math.sqrt( k * m));
            //        final float a = 1.0f;
            //        final float b = 10.0f; // 2.f* zeta*w0;
            //        final float c = 0.0001f;// w0*w0;
            //        final float _rc0 = 9.9998f;// sqrt(99.9996)// (float)Math.sqrt(b*b-4*a*c);
            //        final float r1 = -1.001358E-4f; // (-1.f*b + _rc0) / 2*a;
            //        final float r2 = -9.9999f; // (-1.f*b - _rc0) / 2*a;
            //        final float _ab1 =  (-1.f * x10) / (-9.9998f);// (r1 * x0 - x10) / (r2 - r1);
            //        final float a1 = _ab1; // x0 + _ab1;
            //        final float b1 = -1.f * _ab1;
            //        float y = a1 * (float)Math.exp(r1*x)  + b1 * (float)Math.exp(r2*x);
            //        float y = _ab1 * (float)Math.exp(r1*x)  - _ab1 * (float)Math.exp(r2*x);
            //        y *= mViscousFluidScale;

            // ==================== FINAL ====================
            
        	final float initialVelocity = mVelocityViscousScaled;
            final float _r1 = -1.001358E-4f;
            final float _r2 = -9.9999f;
            final float _ab1 =  (-1.f * initialVelocity) / (-9.9998f);
            float y = _ab1 * (float)Math.exp(_r1*x)  - _ab1 * (float)Math.exp(_r2*x);
            y *= mViscousFluidScale;
            return y;
        }
        
        void setMode( int mode ) {
        	mMode = mode;
        }
    }
}
