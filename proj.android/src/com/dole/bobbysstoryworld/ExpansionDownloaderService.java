/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dole.bobbysstoryworld;

import com.google.android.vending.expansion.downloader.impl.DownloaderService;

/**
 * This class demonstrates the minimal client implementation of the
 * DownloaderService from the Downloader library.
 */
public class ExpansionDownloaderService extends DownloaderService {
    // stuff for LVL -- MODIFY FOR YOUR APPLICATION!
    private static final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnwcpIugRvwy32Jt/vEBpJ3D1+jztNYRkBPicgv3fBgTTr8ZKsmSODOEV8mB//ROoSdspjnDuvlMzzb8RfIXEqAfbknhNrLjmGS2kPGM/HctDgYdHiDeqCufEF7JnbMsXsgdsAZy+IarRJ5PW2B7J9E8NGv+QXhLOqhOO1cFpjLgBESJ2ncgj9Q7j+p9kYG4q9HebOLbHw5XSEe/r3Mao6thKNzM9G8b6xZbZiVOWC25YvFrJuAz9UTGW+3tF3jrhFlfR+KbunZq1QHRWPYqkCD56hnZ0wql8N80KwEwhBR0pTYjYMmOMlqDm1HKjOCDP831P6DeGPev9qOJyaIoDnQIDAQAB";
    // used by the preference obfuscater
    private static final byte[] SALT = new byte[] {
        -46, 65, 30, -128, -103, -57, 74, -64, 51, 88, -95, -45, 77, -117, -36, -113, -11, 32, -64,
        89
    };

    /**
     * This public key comes from your Android Market publisher account, and it
     * used by the LVL to validate responses from Market on your behalf.
     */
    @Override
    public String getPublicKey() {
        return BASE64_PUBLIC_KEY;
    }

    /**
     * This is used by the preference obfuscater to make sure that your
     * obfuscated preferences are different than the ones used by other
     * applications.
     */
    @Override
    public byte[] getSALT() {
        return SALT;
    }

    /**
     * Fill this in with the class name for your alarm receiver. We do this
     * because receivers must be unique across all of Android (it's a good idea
     * to make sure that your receiver is in your unique package)
     */
    @Override
    public String getAlarmReceiverClassName() {
        return ExpansionAlarmReceiver.class.getName();
    }

}
