/****************************************************************************
Copyright (c) 2008-2010 Ricardo Quesada
Copyright (c) 2010-2012 cocos2d-x.org
Copyright (c) 2011      Zynga Inc.
Copyright (c) 2013-2014 Chukong Technologies Inc.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 ****************************************************************************/
package com.dole.bobbysstoryworld;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxEditBoxDialog;
import org.cocos2dx.lib.Cocos2dxHelper;
import org.cocos2dx.lib.Cocos2dxScreenRecorder;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.graphics.SurfaceTexture;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaCodec.BufferInfo;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.media.MediaMuxer.OutputFormat;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.MediaScannerConnection;
import android.media.SoundPool;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.vending.expansion.zipfile.APKExpansionSupport;
import com.dole.bobbysstoryworld.GuideDialog.GuideMode;
import com.dole.bobbysstoryworld.ui.DoleProgressBar;
import com.dole.bobbysstoryworld.ui.FontButton;
import com.dole.bobbysstoryworld.ui.FontTextHelper;
import com.dole.bobbysstoryworld.ui.ImportImageView;
import com.dole.bobbysstoryworld.ui.VideoView;
import com.facebook.Request;
import com.facebook.RequestBatch;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.internal.Utility;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.internal.be;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

public class BobbysStoryWorldActivity extends Cocos2dxActivity implements
		TextureView.SurfaceTextureListener, SensorEventListener,
		MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener,
		Camera.AutoFocusCallback {
	private final float SHAKE_THRESHOLD = 3f;
	private final float BACKGROUND_SCALE_FACTOR = 0.5f;

	private final int FLAG_ENTRY = 0xF000;
	private final int FLAG_CAMERA_PREVIEW = 0x0F00;

	private final int ENTRY_MAIN = 0;
	// private final int ENTRY_QRCODE = 0x0100;
	private final int ENTRY_TAKE_PICTURE = 0x1100;
	private final int ENTRY_MENU = 0x2000;
	private final int ENTRY_PLAY_VIDEO = 0x4000;
	private final int ENTRY_PLAY_VIDEO_LIST = 0x4001;
	private final int ENTRY_CREATE_CHARACTER = 0x8100;

	// public static final int ENTRY_MENU_SETTING = 0x2001;
	// public static final int ENTRY_MENU_LOGIN = 0x2002;
	// public static final int ENTRY_MENU_FIND_PASSWORD = 0x2003;
	// public static final int ENTRY_MENU_SIGNUP = 0x2004;
	// public static final int ENTRY_MENU_SIGNUP_TERMS = 0x2014;
	// public static final int ENTRY_MENU_LEAVE = 0x2005;
	// public static final int ENTRY_MENU_DOLE_COIN = 0x2006;
	// public static final int ENTRY_MENU_DOLE_COIN_QRCODE = 0x2106;
	// public static final int ENTRY_MENU_INVITE_FACEBOOK = 0x2007;
	// public static final int ENTRY_MENU_ABOUT = 0x2008;
	// public static final int ENTRY_MENU_HELP = 0x2009;
	// public static final int ENTRY_MENU_ABOUT_TERMS = 0x200A;

	private final int TAKE_PICTURE_CAMERA = 0;
	private final int TAKE_PICTURE_IMPORT_IMAGE = 1;
	private final int TAKE_PICTURE_SHOW_SNAPSHOT = 2;

	protected final static int SCAN_QRCODE = 0x800;
	protected final static int PLAY_VIDEO = 0x801;
	protected final static int TAKE_PICTURE = 0x802;
	protected final static int LOGIN = 0x804;
	protected final static int DOLE_COIN = 0x808;
	protected final static int CREATE_CHARACTER = 0x810;
	protected final static int SETTING = 0x811;
	protected final static int SHARE = 0x812;
	protected final static int SHARE_FB = 0x814;
	protected final static int DOWNLOAD_TO_DEVICE = 0x818;
	protected final static int UNLOCK_ITEM_WITH_LOGIN = 0x820;
	protected final static int PENDING_PURCHASE = 0x821;

	private final int RECORD_START = 0x8100;
	private final int RECORD_STOP = 0x8101;
	private final int RECORD_PAUSE = 0x8102;
	private final int RECORD_RESUME = 0x8103;
	private final int RECORD_SAVE = 0x8104;
	private final int RECORD_DELETE = 0x8105;

	private final int PLAY_START = 0x8800;
	private final int PLAY_STOP = 0x8801;
	private final int PLAY_PAUSE = 0x8802;
	private final int PLAY_RESUME = 0x8803;

	private final int REQUEST_PICK_IMAGE = 0x8000;

	private final int STATE_ERROR = -1;
	private final int STATE_IDLE = 0;
	private final int STATE_PREPARING = 1;
	private final int STATE_PREPARED = 2;
	private final int STATE_PLAYING = 3;
	private final int STATE_PAUSED = 4;
	private final int STATE_PLAYBACK_COMPLETED = 5;
	private final int STATE_WAIT_PLAY = 6;

	private final String SAVED_ORIENTATION = "ORIENTATION_BEFORE";
	private final String SAVED_ENTRY_POINT = "ENTRY_POINT";
	private final String SAVED_MAX_TEXTURE_SIZE = "MAX_TEXTURE_SIZE";
	private final String SAVED_TAKE_PICTURE_MODE = "TAKE_PICTURE_MODE";
	private final String SAVED_CHARACTER_SELECT_INDEX = "CHARACTER_SELECT_INDEX";
	private final String SAVED_CHARACTER_INDEX_ARRAY = "CHARACTER_INDEX_ARRAY";
	private final String SAVED_IMPORT_IMAGE_URI = "IMPORT_IMAGE_URI";
	private final String SAVED_HASH_CODE = "HASH_CODE";

	private final String PREF_SAMPLE_QRCODE_USED = "sample_qrcode_used";

	protected static final String RECORD_PATH = "journey";

	private final String AUDIO_MIX_POSTFIX = "_mix";
	// GCM
	public final static String PROPERTY_REG_ID = "property_reg_id";
	private final String PROPERTY_APP_VERSION = "property_app_version";
	private final int PLAY_SERVICES_RESOLUTION_REQUEST = 0xF000;
	private String SENDER_ID = "1002416322639";

	private String YOUTUBE_REDIRECT_URL = "http://localhost/oauth2callback";
	private String YOUTUBE_CREATE_CHANNEL_REDIRECT_URL = "https://m.youtube.com/channel_creation_done";

	private TextureView mTextureView;
	private SurfaceTexture mSurface;
	private ViewGroup mTakePictureView;
	private ViewGroup mCreateCharacterView;
	private VideoView mVideoView;
	private ImportImageView mImportedImageView;
	private ViewGroup mVideoStopView;
	private DoleProgressBar mDoleProgress;

	private Camera mCamera;
	private int mCameraFacing = Camera.CameraInfo.CAMERA_FACING_BACK;

	private SensorManager mSensorManager;
	private Sensor mAccelerometer;

	private GoogleCloudMessaging mGcm;
	private String regid;

	private MediaRecorder mRecorder;
	private MediaPlayer mBGMPlayer;
	private MediaPlayer mVoicePlayer;
	private SoundPool mSoundPool;
	private SoundLoadListener mSoundLoadListener = new SoundLoadListener();
	private HashMap<String, Integer> mSoundPoolMap = new HashMap<String, Integer>();
	private HashMap<String, Integer> mSoundPoolStreamIdMap = new HashMap<String, Integer>();
	private SparseArray<String> mSoundPoolWaitPlay = new SparseArray<String>();
	private String mRecordingFileName;
	private int mRecordingFileIndex;
	private boolean mOnRecordVoice = false;
	// private boolean mOnPlayVoice = false;
	private boolean mOnPlayBGM = false;
	private int mCurrentState = STATE_IDLE;
	private int mTargetState = STATE_IDLE;

	private boolean mPendingCameraOpen;
	private boolean mIsFocusing = false;
	private boolean mIsFocused = false;
	private boolean mIsScanning = false;
	private boolean mBlockPreviewCallback = false;
	private boolean mBlockAutoFocus = false;
	private DisplayMetrics mDisplayMetrics = new DisplayMetrics();
	private int mPendingAction;
	private boolean mVideoPaused = false;

	private int mOrientationBackup = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED;
	private int mEntryPoint = ENTRY_MAIN;
	private int mTakePictureMode = TAKE_PICTURE_CAMERA;
	private int mTempDataLen = 0;
	private int mMaxTextureSize = -1;
	private int mSurfaceWidth;
	private int mSurfaceHeight;
	private int mPreviewWidth;
	private int mPreviewHeight;
	private int mPictureWidth;
	private int mPictureHeight;
	private int mCustomCharacterSelectIndex = 0;
	private Uri mImportImageUri;
	private byte[] mTempBuf = null;
	private int[] mNativeEditTextSize;
	private int[] mNativeEditTextColor;
	private int[] mNativeEditTextFontSize;
	private int[] mNativeEditTextShadow;
	private String[] mNativeEditTextContents;
	private int[] mCustomCharacterCropSize;
	private int[] mCustomCharacterThumbnailSize;
	private int[] mCustomBackgroundSize;
	private int[] mCustomBackgroundCoverSize;
	private int[] mCustomBackgroundThumbnailSize;
	private int[] mCustomCharacterFrame;
	private ArrayList<CustomCharacterData> mAvailableCharacters = new ArrayList<CustomCharacterData>();
	private ArrayList<String> mPreloadVideoList = new ArrayList<String>();
	private boolean mOnPlayVideoList = false;
	private int mVideoAudioReadyCount = 0;
	private int mPreloadVideoScene = -1;

	private int currentVideo = -1;
	private int videoCounter = 0;

	private int[] mHomeCharacterCustom = new int[] {
			R.drawable.home_character_custom_01,
			R.drawable.home_character_custom_02,
			R.drawable.home_character_custom_03,
			R.drawable.home_character_custom_04,
			R.drawable.home_character_custom_05,
			R.drawable.home_character_custom_06,
			R.drawable.home_character_custom_07,
			R.drawable.home_character_custom_08,
			R.drawable.home_character_custom_09,
			R.drawable.home_character_custom_10,
			R.drawable.home_character_custom_11,
			R.drawable.home_character_custom_12,
			R.drawable.home_character_custom_13,
			R.drawable.home_character_custom_14 };

	private class CustomCharacterData {
		int characterIndex;
		int imageIndex;
	}

	private RelativeLayout mPromotionViewContainer;
	private WebView mPromotionView;
	private WebView mWebView;
	private boolean mYoutubeShareCanceled;
	private long mJniHandle; // Do not modify!

	// private UiLifecycleHelper mUIHelper;
	private static final String FACEBOOK_PUBLISH_PERMISSION = "publish_actions";
	private static final List<String> FACEBOOK_PERMISSIONS = Arrays
			.asList(FACEBOOK_PUBLISH_PERMISSION);

	private ImageView mLogo;
	/**
	 * Accelerometers
	 */
	private long mLastUpdateTime = 0;
	private float mLastZ = 0f;
	private float mAccumulatedZDiff = 0f;

	private String mQrcodeValueT = null;
	private String mQrStringValueCp = null;
	private String mQrPromotionCode = null;

	private Bitmap mTakenPicture;
	// private PreviewCallback mPreviewCallback = new QRCodePreviewCallback();
	private TakePictureCallback mTakePictureCallback = new TakePictureCallback();
	private Runnable mRecordSaveRunnable;
	// private AutoFocusCallback mQRCodeAutoFocusCallback = new
	// AutoFocusCallback() {
	// @Override
	// public void onAutoFocus(boolean success, Camera camera) {
	// if(mFocusView != null)
	// mFocusView.setSelected(success);
	//
	// //mIsFocused = success;
	//
	// if(!success) {
	// camera.autoFocus(this);
	// if(mFocusView != null)
	// mFocusView.setSelected(false);
	// return;
	// }
	//
	// mSensorManager.registerListener(BobbysStoryWorldActivity.this,
	// mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
	// if(mPreviewCallback != null) {
	// camera.setOneShotPreviewCallback(mPreviewCallback);
	// }
	//
	// resumeQrcodeAutoFocus();
	// }
	// };
	//
	//
	// private final class QRCodePreviewCallback implements PreviewCallback {
	//
	// @Override
	// public void onPreviewFrame(final byte[] data, final Camera camera) {
	// if(mEntryPoint != ENTRY_QRCODE)
	// return;
	//
	// //DLog.d("", " qr preview : isScanning " + mIsScanning +
	// " | isReleased = " + mCameraManager.isCameraReleased() + " isFocused = "
	// + mCameraManager.isFocused());
	// if(!mIsScanning && mCamera != null && mIsFocused) {
	// mIsScanning = true;
	// scanQRCode(data);
	// }
	// }
	// }

	private MediaPlayer.OnPreparedListener mPreparedListener = new MediaPlayer.OnPreparedListener() {
		public void onPrepared(MediaPlayer mp) {
			mCurrentState = STATE_PREPARED;
			if (mOnPlayVideoList) {
				mVideoAudioReadyCount++;
				if (mVideoAudioReadyCount >= 2)
					playNextListItem();
			} else {
				if (mTargetState == STATE_PLAYING) {
					startBGMPlay();
				}
			}
		}
	};

	private MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener() {
		public void onCompletion(MediaPlayer mp) {
			mCurrentState = STATE_PLAYBACK_COMPLETED;
			mTargetState = STATE_PLAYBACK_COMPLETED;
		}
	};

	private MediaPlayer.OnErrorListener mErrorListener = new MediaPlayer.OnErrorListener() {
		public boolean onError(MediaPlayer mp, int what, int extra) {
			mCurrentState = STATE_ERROR;
			mTargetState = STATE_ERROR;
			return true;
		}
	};

	private View.OnTouchListener mVideoListPlayTouchListener = new View.OnTouchListener() {
		@Override
		public boolean onTouch(View v, final MotionEvent event) {
			if (mVideoView != null && !mVideoView.isFocused()) {
				nativeHandleTouchEvent(event);
				return true;
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				if (mVideoStopView != null) {
					if (mVideoStopView.getVisibility() == View.VISIBLE) {
						mVideoStopView.setVisibility(View.GONE);
					} else {
						TextView scene = (TextView) mVideoStopView
								.findViewById(R.id.video_stop_scene);
						String title;
						if (mPreloadVideoScene > 0)
							title = getResources().getString(
									R.string.cc_play_page_title_page_n,
									mPreloadVideoScene);
						else if (mPreloadVideoScene == 0)
							title = getResources().getString(
									R.string.cc_play_page_title_page_n, 5);
						else
							title = "";
						scene.setText(title);
						mVideoStopView.setVisibility(View.VISIBLE);
					}
				}
				return true;
			}
			return false;
		}
	};

	private View.OnClickListener mVideoStopClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if (mVideoView.isPlaying()) {
				mVideoView.pause();
				mBGMPlayer.pause();
				v.setSelected(true);
			} else {
				mVideoView.start();
				mBGMPlayer.start();
				v.setSelected(false);
				if (mVideoStopView != null) {
					mVideoStopView.setVisibility(View.GONE);
				}
			}
		}
	};

	private class SoundLoadListener implements SoundPool.OnLoadCompleteListener {
		@Override
		public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
			// String filename = mSoundPoolWaitPlay.get(sampleId);
			// if(filename != null) {
			// int streamId = mSoundPool.play(Integer.valueOf(sampleId), 1.f,
			// 1.f, 0, 0, 1.f);
			// mSoundPoolStreamIdMap.put(filename, Integer.valueOf(streamId));
			// }
			mSoundPool.play(sampleId,
					Constants.DEFAULT_VOLUME_BACKGROUND_SOUND,
					Constants.DEFAULT_VOLUME_BACKGROUND_SOUND, 1, 0, 1.f);
		}
	}

	private class PictureSizeComparator implements java.util.Comparator<Size> {
		@Override
		public int compare(Size lhs, Size rhs) {
			if (lhs.height > rhs.height)
				return -1;
			else
				return 1;
		}
	}

	private Session.StatusCallback mStatusCallback = new Session.StatusCallback() {
		@Override
		public void call(final Session session, SessionState state,
				Exception exception) {
			if (exception != null) {
				if (exception instanceof com.facebook.FacebookAuthorizationException) {
					session.closeAndClearTokenInformation();
					Session.setActiveSession(null);
					checkFacebookPermission();
				}
				exception.printStackTrace();
				return;
			}

			if (state.equals(SessionState.OPENED_TOKEN_UPDATED)) {
				Cocos2dxHelper.runOnGLThread(new Runnable() {
					@Override
					public void run() {
						if (session.getPermissions().contains(
								FACEBOOK_PUBLISH_PERMISSION)) {
							nativeFacebookActionResult(0, 1);
						} else {
							nativeFacebookActionResult(-1, 1);
						}
					}
				});
			} else if (state.equals(SessionState.OPENED)) {
				checkPublishPermission();
				// Cocos2dxHelper.runOnGLThread(new Runnable() {
				// @Override
				// public void run() {
				// nativeFacebookActionResult(0, 1);
				// }
				// });
			}
		}
	};

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
	};

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setVolumeControlStream(AudioManager.STREAM_MUSIC);

		BobbysStoryWorldPreference.initSharedPreferences(this);

		if (checkPlayServices()) {
			mGcm = GoogleCloudMessaging.getInstance(this);
			regid = getRegistrationId();
			if (regid.isEmpty()) {
				android.util.Log.e("",
						"Will try registerInBackground.");
				registerInBackground();
			} else {
				android.util.Log.e("",
						"No valid Google Play Services APK found.");
			}
		}

		initJNIBridge();

		Resources r = getResources();
		mNativeEditTextSize = new int[] {
				r.getDimensionPixelSize(R.dimen.native_edit_text_title_minwidth),
				r.getDimensionPixelSize(R.dimen.native_edit_text_title_maxwidth),
				r.getDimensionPixelSize(R.dimen.native_edit_text_author_maxwidth), };
		mNativeEditTextColor = new int[] {
				r.getColor(R.color.native_edit_text_title_color),
				r.getColor(R.color.native_edit_text_author_color), };
		mNativeEditTextFontSize = new int[] {
				r.getDimensionPixelSize(R.dimen.native_edit_text_title_font_size),
				r.getDimensionPixelSize(R.dimen.native_edit_text_author_font_size), };
		mNativeEditTextShadow = new int[] {
				r.getDimensionPixelSize(R.dimen.native_edit_text_shadow_radious),
				r.getDimensionPixelSize(R.dimen.native_edit_text_shadow_dx),
				r.getDimensionPixelSize(R.dimen.native_edit_text_shadow_dy),
				r.getColor(R.color.native_edit_text_shadow_color), };
		mCustomBackgroundSize = new int[] {
				// multiply 2 to padding because of scale factor (1/2 size)
				r.getDimensionPixelSize(R.dimen.custom_background_width)
						- (int) (2 * r
								.getDimensionPixelSize(R.dimen.custom_background_horizontal_padding) / BACKGROUND_SCALE_FACTOR),
				r.getDimensionPixelSize(R.dimen.custom_background_height)
						- (int) (2 * r
								.getDimensionPixelSize(R.dimen.custom_background_vertical_padding) / BACKGROUND_SCALE_FACTOR), };

		mCustomBackgroundCoverSize = new int[] {
				r.getDimensionPixelSize(R.dimen.custom_background_cover_width),
				r.getDimensionPixelSize(R.dimen.custom_background_cover_height), };

		TypedArray cropSize = r
				.obtainTypedArray(R.array.custom_character_crop_size);
		final int cropSizeCount = cropSize.length();
		mCustomCharacterCropSize = new int[cropSizeCount];
		for (int i = 0; i < cropSizeCount; i++) {
			mCustomCharacterCropSize[i] = cropSize.getDimensionPixelSize(i, 0);
		}
		cropSize.recycle();

		TypedArray customCharacter = r
				.obtainTypedArray(R.array.custom_character_thumbnail_size);
		final int charCount = customCharacter.length();
		mCustomCharacterThumbnailSize = new int[charCount];
		for (int i = 0; i < charCount; i++) {
			mCustomCharacterThumbnailSize[i] = customCharacter
					.getDimensionPixelSize(i, 0);
		}
		customCharacter.recycle();

		TypedArray customFrame = r
				.obtainTypedArray(R.array.custom_character_frame);
		final int frameCount = customFrame.length();
		mCustomCharacterFrame = new int[frameCount];
		for (int i = 0; i < frameCount; i++) {
			mCustomCharacterFrame[i] = customFrame.getResourceId(i, 0);
		}
		customFrame.recycle();

		TypedArray customBackground = r
				.obtainTypedArray(R.array.custom_background_thumbnail_size);
		final int bgCount = customBackground.length();
		mCustomBackgroundThumbnailSize = new int[bgCount];
		for (int i = 0; i < bgCount; i++) {
			mCustomBackgroundThumbnailSize[i] = customBackground
					.getDimensionPixelSize(i, 0);
		}
		customBackground.recycle();

		setEditTextBackgroundImages(R.drawable.map_title_bg_left,
				R.drawable.map_title_bg_right);
		setEditTextTopPadding(r
				.getDimensionPixelSize(R.dimen.native_edit_text_padding_top));
		setEditTextSize(
				r.getDimensionPixelSize(R.dimen.native_edit_text_width),
				r.getDimensionPixelSize(R.dimen.native_edit_text_height));
		setDialogLayoutId(R.layout.input_comment_dialog,
				R.layout.input_password_dialog);

		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mAccelerometer = mSensorManager
				.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

		mSoundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 44100);
		mSoundPool.setOnLoadCompleteListener(mSoundLoadListener);

		// mUIHelper = new UiLifecycleHelper(this, mStatusCallback);

		final int userNo = BobbysStoryWorldPreference.getInt(
				Constants.PREF_USER_NO, 0);
		final String authKey = BobbysStoryWorldPreference.getString(
				Constants.PREF_AUTH_KEY, "");

		if (userNo > 0 && !"".equals(authKey)) {
			Cocos2dxHelper.runOnGLThread(new Runnable() {
				@Override
				public void run() {
					if (userNo >= 0 && authKey != null)
						nativeSaveAuthData(authKey, userNo);

//					MixpanelAPI mixpanel = MixpanelAPI.getInstance(
//							getApplicationContext(), Constants.MIXPANEL_TOKEN);
//					
//					mixpanel.identify(String.valueOf(userNo));

				}
			});
		}
		
		MixpanelAPI mixpanel = MixpanelAPI.getInstance(
				getContext(), Constants.MIXPANEL_TOKEN);
		
		// Create Mixpanel distinct ID if not existent
		String distinctID = mixpanel.getPeople().getDistinctId();
		if (distinctID == null) {
			String email = BobbysStoryWorldPreference.getString(Constants.PREF_USER_ID, null);

			if (email == null){ // No email registered during launch

				JSONObject props = new JSONObject();
				try {
					props.put("User Type", "Anonymous");
					props.put("Email", "");
					mixpanel.registerSuperProperties(props);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();					
				}

				String uuid = UUID.randomUUID().toString();
				mixpanel.identify(uuid);
				mixpanel.getPeople().identify(mixpanel.getDistinctId());
			} else {
				JSONObject props = new JSONObject();
				try {
					props.put("User Type", "Registered");
					props.put("Email", email);
					mixpanel.registerSuperProperties(props);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();					
				}
				mixpanel.identify(email);
				mixpanel.getPeople().identify(mixpanel.getDistinctId());
				mixpanel.getPeople().set("$email", email);
			}
		} else {
			// Already has distinctID, now check if registered or not-registered
			String email = BobbysStoryWorldPreference.getString(Constants.PREF_USER_ID, null);
			JSONObject props = new JSONObject();
			try {
				if (email == null) {
					props.put("User Type", "Anonymous");
					props.put("Email", "");
				} else {
					props.put("User Type", "Registered");
					props.put("Email", email);
					// No need to mixpanel.identify(email) since we already have a distinctId
					mixpanel.getPeople().identify(mixpanel.getDistinctId());
					mixpanel.getPeople().set("$email", email);
				}
				mixpanel.registerSuperProperties(props);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();					
			}
		}
		mixpanel.getPeople().initPushHandling(Constants.GCM_SENDER_ID);
		mixpanel.track("App Launched", null);
		mixpanel.flush();
	}

	@Override
	protected void onStart() {
		super.onStart();
		MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(), Constants.MIXPANEL_TOKEN);
		mixpanel.track("Landing", null);
		mixpanel.flush();
		EasyTracker.getInstance().activityStart(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		EasyTracker.getInstance().activityStop(this);
	}

	@Override
	protected void onPostCreate() {
		final boolean playBGM = BobbysStoryWorldPreference.getBoolean(
				Constants.PREF_IS_BGM_ON, true);
		nativeChangeBGMPlayStatus(!playBGM ? 1 : 0);
		// if(playBGM) {
		// Cocos2dxHelper.runOnGLThread(new Runnable() {
		// @Override
		// public void run() {
		// nativeStopBGM();
		// nativePlayBGM();
		// }
		// });
		// }
	}

	@Override
	protected void onPause() {
		super.onPause();

		saveInstanceState();

		if ((mEntryPoint & FLAG_CAMERA_PREVIEW) != 0)
			closeCamera();

		if (mVideoView.isPlaying()) {
			mVideoView.pause();
			mBGMPlayer.pause();
			mVideoPaused = true;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		restoreInstanceState();

		if (mVideoView.isPaused() && mVideoPaused) {
			mVideoView.start();
			mBGMPlayer.start();
			mVideoPaused = false;
		}

		if (mEntryPoint == ENTRY_CREATE_CHARACTER)
			updateCharacterFrame();

		if ((mEntryPoint & FLAG_CAMERA_PREVIEW) != 0) {
			switch (mTakePictureMode) {
			case TAKE_PICTURE_CAMERA:
				startCameraPreview(mEntryPoint);
				break;

			case TAKE_PICTURE_IMPORT_IMAGE:
				showCameraPreviewFrameLayout(mEntryPoint);
				handleImportImage();
				break;
			}
		}
	}

	@Override
	public void postInitViews(ViewGroup container) {
		getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);

		LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View textureView = container.findViewById(R.id.camera_preview);

		if (textureView == null) {
			mTextureView = new TextureView(this);
			mTextureView.setId(R.id.camera_preview);
			container.addView(mTextureView);

			SurfaceTexture surfaceTexture = ((TextureView) mTextureView)
					.getSurfaceTexture();
			if (surfaceTexture == null) {
				mTextureView.setSurfaceTextureListener(this);
				mTextureView
						.addOnLayoutChangeListener(new OnLayoutChangeListener() {
							@Override
							public void onLayoutChange(View v, int left,
									int top, int right, int bottom,
									int oldLeft, int oldTop, int oldRight,
									int oldBottom) {
								int width = right - left;
								int height = bottom - top;
								if (mPreviewWidth != width
										|| mPreviewHeight != height) {
									mPreviewWidth = width;
									mPreviewHeight = height;
								}
							}
						});
			}
		} else {
			mTextureView = (TextureView) textureView;
		}

		View takePictureContainer = container
				.findViewById(R.id.take_picture_container);
		if (takePictureContainer == null) {
			if (li != null) {
				mTakePictureView = (ViewGroup) li.inflate(
						R.layout.camera_take_picture, null);
				if (mTakePictureView != null) {
					mImportedImageView = (ImportImageView) mTakePictureView
							.findViewById(R.id.imported_image);
					container.addView(mTakePictureView);
				}
			}
		} else {
			mTakePictureView = (ViewGroup) takePictureContainer;
		}

		View createCharacterContainer = container
				.findViewById(R.id.create_character_container);
		if (createCharacterContainer == null) {
			if (li != null) {
				mCreateCharacterView = (ViewGroup) li.inflate(
						R.layout.create_character, null);
				if (mCreateCharacterView != null)
					container.addView(mCreateCharacterView);
			}
		} else {
			mCreateCharacterView = (ViewGroup) createCharacterContainer;
		}

		View videoView = container.findViewById(R.id.video_container);
		if (videoView == null) {
			if (li != null) {
				mVideoView = (VideoView) li
						.inflate(R.layout.video_player, null);
				if (mVideoView != null) {
					container.addView(mVideoView);
				}
			}
		} else {
			mVideoView = (VideoView) videoView;
		}
		mVideoView.setOnTouchListener(mVideoListPlayTouchListener);
		mVideoPaused = false;

		View videoStopView = container.findViewById(R.id.video_stop_container);
		if (videoView == null) {
			if (li != null) {
				mVideoStopView = (ViewGroup) li.inflate(
						R.layout.video_player_stop, null);
				if (mVideoStopView != null) {
					container.addView(mVideoStopView);
				}
			}
		} else {
			mVideoStopView = (ViewGroup) videoStopView;
		}
		View back = mVideoStopView.findViewById(R.id.video_stop_back);
		View pp = mVideoStopView.findViewById(R.id.video_stop_play_pause);
		mDoleProgress = (DoleProgressBar) mVideoStopView
				.findViewById(R.id.video_stop_progress);
		back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
		pp.setOnClickListener(mVideoStopClickListener);
		pp.setSelected(false);

		if (mTextureView != null) {
			mTextureView.setSurfaceTextureListener(this);
			mTextureView.setVisibility(View.GONE);
		}

		if (mTakePictureView != null) {
			setTakePictureTileBackground(false);
			mTakePictureView.setBackground(null);
			mTakePictureView.setVisibility(View.GONE);
		}

		if (mCreateCharacterView != null) {
			prepareCreateCharacterView();
			mCreateCharacterView.setVisibility(View.GONE);
		}

		if (mVideoView != null) {
			mVideoView.setOnCompletionListener(this);
			mVideoView.setOnPreparedListener(this);
			mVideoView.setVisibility(View.GONE);
		}

		if (mVideoStopView != null) {
			mVideoStopView.setVisibility(View.GONE);
		}

		ApplicationImpl app = (ApplicationImpl) getApplication();
		if (app != null) {
			if (!app.isLogoShown()) {
				mLogo = new ImageView(this);
				FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
						FrameLayout.LayoutParams.MATCH_PARENT,
						FrameLayout.LayoutParams.MATCH_PARENT);
				lp.gravity = Gravity.CENTER;
				mLogo.setLayoutParams(lp);
				mLogo.setBackgroundResource(R.drawable.splash_logo);
				container.addView(mLogo);

				app.setLogoShown();
			}
		}
	}

	@Override
	public void onEditActionFinish(boolean isDone) {
		if (isDone) {
			if (isShareEditBox()) {
				final String comment = getShareComment();
				final int type = getShareType();
				runOnGLThread(new Runnable() {
					@Override
					public void run() {
						nativeFinishShareCommentEditAction(type, comment);
						clearEditData();
					}
				});
			} else {
				final String[] contents = getEditTextContents();
				runOnGLThread(new Runnable() {
					@Override
					public void run() {
						nativeFinishEditAction(contents[0], contents[1]);
						clearEditData();
					}
				});
			}
		} else {
			nativeCancelEditAction();
			clearEditData();
		}
	}

	@Override
	public int[] getEditTextWidths() {
		// title min, title max, author
		return mNativeEditTextSize;
	}

	@Override
	public int[] getEditTextColor() {
		return mNativeEditTextColor;
	}

	@Override
	public int[] getEditTextFontSize() {
		return mNativeEditTextFontSize;
	}

	@Override
	public int[] getEditTextShadow() {
		return mNativeEditTextShadow;
	}

	@Override
	public String[] getEditTextContents() {
		return mNativeEditTextContents;
	}

	@Override
	public Typeface[] getEditTextTypefaces() {
		return new Typeface[] { FontCache.get(this, "fonts/Roboto-Bold.ttf"),
				FontCache.get(this, "fonts/Roboto-Regular.ttf") };
	}

	@Override
	protected String getDeviceToken() {
		return BobbysStoryWorldPreference.getString(PROPERTY_REG_ID, "");
	}

	@Override
	protected String getPlatformString(int id) {
		return Util.getString(this, id);
	}

	@Override
	public String getSharePopupComment(int type, String[] content) {
		if (content.length < 2)
			return null;
		switch (type) {
		case Cocos2dxEditBoxDialog.SHARE_POPUP_FACEBOOK:
			return getResources().getString(
					R.string.pf_sharecomment_popup_defaultcomment_facebook,
					content[1], content[0]);

		case Cocos2dxEditBoxDialog.SHARE_POPUP_YOUTUBE:
			return getResources().getString(
					R.string.pf_sharecomment_popup_defaultcomment_youtube,
					content[1], content[0]);

		default:
			return "";
		}
	}

	@Override
	public String getHashTag(int type) {
		switch (type) {
		case Cocos2dxEditBoxDialog.SHARE_POPUP_FACEBOOK:
			return getResources().getString(
					R.string.pf_sharecomment_popup_hashtag_facebook);

		case Cocos2dxEditBoxDialog.SHARE_POPUP_YOUTUBE:
			return getResources().getString(
					R.string.pf_sharecomment_popup_hashtag_youtube);

		default:
			return "";
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			long curTime = System.currentTimeMillis();
			if ((curTime - mLastUpdateTime) > 100) {
				long diffTime = (curTime - mLastUpdateTime);
				mLastUpdateTime = curTime;

				float[] values = event.values;
				float z = values[2];

				float speed = (z - mLastZ) / diffTime * 1000;
				mAccumulatedZDiff += speed;
				mLastZ = z;

				if (Math.abs(mAccumulatedZDiff) > SHAKE_THRESHOLD) {
					mAccumulatedZDiff = 0;
					if (mCamera != null) {
						mIsFocusing = true;
						// mCamera.autoFocus(mQRCodeManager);
						// if(mFocusView != null)
						// mFocusView.setSelected(false);
					}
					mSensorManager.unregisterListener(this);
				}
			}
		}
	}

	@Override
	public void onSurfaceTextureAvailable(SurfaceTexture surface, int width,
			int height) {
		int[] texSize = new int[1];
		android.opengl.GLES20.glGetIntegerv(
				android.opengl.GLES20.GL_MAX_TEXTURE_SIZE, texSize, 0);
		mMaxTextureSize = texSize[0];

		mSurface = surface;
		mSurfaceWidth = Math.max(width, height);
		mSurfaceHeight = Math.min(width, height);
		if (mPendingCameraOpen) {
			mPendingCameraOpen = false;
			openCamera();
		}
	}

	@Override
	public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width,
			int height) {
		mSurfaceWidth = width;
		mSurfaceHeight = height;
	}

	@Override
	public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
		mSurface = null;
		return true;
	}

	@Override
	public void onSurfaceTextureUpdated(SurfaceTexture surface) {
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		if (mEntryPoint == ENTRY_PLAY_VIDEO_LIST) {
			if (mPreloadVideoList.size() > 0) {
				try {
					mVideoStopView.setVisibility(View.GONE);
					String filename = mPreloadVideoList.remove(0);
					String bgmname = null;
					if (mPreloadVideoList.size() % 2 == 1) {
						// transition
						bgmname = "common/preload_sounds/scene_transition.wav";
						mPreloadVideoScene = -1;
					} else {
						// movie
						String language = "en";
						if (Locale.getDefault().equals(Locale.JAPAN))
							language = "ja";
						else if (Locale.getDefault().equals(Locale.KOREA))
							language = "kor";
						bgmname = "common/preload_sounds/"
								+ language
								+ File.separator
								+ filename.substring(0,
										filename.lastIndexOf(".") + 1) + "wav";
						mPreloadVideoScene = Integer.parseInt(filename
								.substring(filename.lastIndexOf("_") + 1,
										filename.lastIndexOf("."))) % 5;
						mDoleProgress.setCurSceneId(mPreloadVideoScene);
					}

					mVideoView.stopPlayback();
					if (mBGMPlayer != null) {
						mBGMPlayer.stop();
						mBGMPlayer.release();

						mBGMPlayer = new MediaPlayer();
						mBGMPlayer.setOnPreparedListener(mPreparedListener);
						mBGMPlayer.setOnCompletionListener(mCompletionListener);
						mBGMPlayer.setOnErrorListener(mErrorListener);
						mBGMPlayer
								.setAudioStreamType(AudioManager.STREAM_MUSIC);
					}

					if (Constants.DEBUG) {
						AssetManager manager = getAssets();
						AssetFileDescriptor fd = manager.openFd("common"
								+ File.separator + filename);
						mVideoView.setVideoFileDescriptor(
								fd.getFileDescriptor(), fd.getStartOffset(),
								fd.getLength());
						AssetFileDescriptor bgm = manager.openFd(bgmname);
						mBGMPlayer.setDataSource(bgm.getFileDescriptor(),
								bgm.getStartOffset(), bgm.getLength());
					} else {
						String prefix = APKExpansionSupport
								.getAPKExpansionPath(BobbysStoryWorldActivity.this);
						mVideoView.setVideoPath(prefix + File.separator
								+ "common" + File.separator + filename);
						mBGMPlayer.setDataSource(prefix + File.separator
								+ bgmname);
					}
					mBGMPlayer.prepareAsync();
					mCurrentState = STATE_PREPARING;
					mTargetState = STATE_WAIT_PLAY;
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				stopBGMPlay();
				mVideoView.stopPlayback();
				nativeShow();

				Cocos2dxHelper.runOnGLThread(new Runnable() {
					@Override
					public void run() {
						nativeOnFinishVideoListPlay();
						mEntryPoint = ENTRY_MAIN;

						BobbysStoryWorldActivity.this
								.runOnUiThread(new Runnable() {
									@Override
									public void run() {
										if (mVideoView != null
												&& mVideoStopView != null) {
											mVideoView.setVisibility(View.GONE);
											mVideoStopView
													.setVisibility(View.GONE);
										}
									}
								});
					}
				});
			}
			return;
		}

		backPress(null);
	}

	@Override
	public void onPrepared(MediaPlayer mp) {
		if (mOnPlayVideoList) {
			mVideoAudioReadyCount++;
			if (mVideoAudioReadyCount >= 2)
				playNextListItem();
		}
		mDoleProgress.setVideoView(mVideoView);
		mDoleProgress.startProgress();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			if (mPromotionViewContainer != null) {
				if (mPromotionView.canGoBack()) {
					mPromotionView.goBack();
				} else {
					hidePromotionScreen();
				}
				return true;
			} else if (mWebView != null) {
				if (!mYoutubeShareCanceled) {
					mYoutubeShareCanceled = true;
					nativeCancelYoutubeShare();
				}
				return true;
			}
			break;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onBackPressed() {
		nativeBackPress();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case REQUEST_PICK_IMAGE:
			if (resultCode == RESULT_OK) {
				mImportImageUri = data.getData();
				mTakePictureMode = TAKE_PICTURE_IMPORT_IMAGE;
				BobbysStoryWorldPreference.putInt(SAVED_TAKE_PICTURE_MODE,
						mTakePictureMode);
			}
			break;

		case Session.DEFAULT_AUTHORIZE_ACTIVITY_CODE:
			Session.getActiveSession().onActivityResult(
					BobbysStoryWorldActivity.this, requestCode, resultCode,
					data);
			break;
		}
	}

	public void removeLogo() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (mLogo != null) {
					((ViewGroup) mLogo.getParent()).removeView(mLogo);
					mLogo = null;
				}
			}
		});
	}

	public void backPress(View view) {
		if (!mSoundPoolMap.containsKey("tap.wav"))
			mSoundPoolMap.put("tap.wav",
					Integer.valueOf(mSoundPool.load(this, R.raw.tap, 1)));
		else
			mSoundPool.play(mSoundPoolMap.get("tap.wav").intValue(),
					Constants.DEFAULT_VOLUME_BACKGROUND_SOUND,
					Constants.DEFAULT_VOLUME_BACKGROUND_SOUND, 1, 0, 1.f);

		nativeBackPress();
	}

	public boolean hideView() {
		return handleBackPress();
	}

	public void importImageFrom(View view) {
		Intent intent = new Intent(Intent.ACTION_PICK);
		intent.setType("image/*");
		startActivityForResult(
				Intent.createChooser(intent, "Import image from..."),
				REQUEST_PICK_IMAGE);
	}

	public void takeSnapshot(View view) {
		if (mCamera != null) {
			mBlockAutoFocus = true;
			mCamera.takePicture(null, null, null, mTakePictureCallback);
		}
	}

	public void checkFacebookPermission() {
		Session s = Session.getActiveSession();
		if (s == null) {
			s = new Session(this);
			Session.setActiveSession(s);
		}

		if (s != null && s.isOpened()) {
			checkPublishPermission();
		} else {
			Session.openActiveSession(this, true, mStatusCallback);
			// Cocos2dxHelper.runOnGLThread(new Runnable() {
			// @Override
			// public void run() {
			// nativeFacebookActionResult(-1, 1);
			// }
			// });
		}
	}

	private void checkPublishPermission() {
		Session s = Session.getActiveSession();
		if (!s.getPermissions().contains(FACEBOOK_PUBLISH_PERMISSION)) {
			Session.NewPermissionsRequest request = new Session.NewPermissionsRequest(
					this, FACEBOOK_PERMISSIONS);
			s.addCallback(mStatusCallback);
			s.requestNewPublishPermissions(request);
		} else {
			Cocos2dxHelper.runOnGLThread(new Runnable() {
				@Override
				public void run() {
					nativeFacebookActionResult(0, 1);
				}
			});
		}
	}

	public void uploadVideoToFacebook(final String title,
			final String description, final String path) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Session session = Session.getActiveSession();
				if (session != null) {
					try {
						Request uploadVideo = Request.newUploadVideoRequest(
								session, new File(path),
								new Request.Callback() {
									@Override
									public void onCompleted(Response response) {
										nativeOnFacebookShareComplete();
									}
								});

						String applicationId = Utility
								.getMetadataApplicationId(BobbysStoryWorldActivity.this);

						Bundle parameters = uploadVideo.getParameters();
						parameters.putString("description", description);
						parameters.putString("app_id", applicationId);
						RequestBatch batch = new RequestBatch(
								Arrays.asList(uploadVideo));
						batch.addCallback(new RequestBatch.OnProgressCallback() {
							@Override
							public void onBatchCompleted(RequestBatch batch) {
							}

							@Override
							public void onBatchProgress(RequestBatch batch,
									long current, long max) {
								nativeOnProgressUpdate(current);
							}
						});
						Request.executeBatchAsync(batch);
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	public boolean showPromotionPopup() {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("gmt"));
		long currentTime = calendar.getTimeInMillis();
		long showTime = BobbysStoryWorldPreference.getLong(
				Constants.PREF_PROMOTION_DATE, 0L);
		if (showTime < currentTime && mEntryPoint == ENTRY_MAIN)
			return true;
		return false;
	}

	public void savePicture(View view) {
		if (mTakenPicture == null)
			return;
		// TODO savePicture
		Resources r = getResources();
		OutputStream os = null;
		OutputStream ost = null;
		OutputStream osc = null;

		if (mEntryPoint == ENTRY_TAKE_PICTURE) {
			Matrix transform = mImportedImageView.getTransformMatrix();
			Bitmap thumbnail = null;
			Bitmap cover = null;
			if (mTakenPicture != null) {
				// loading time

				int bitmapWidth = mTakenPicture.getWidth();
				int bitmapHeight = mTakenPicture.getHeight();
				int maxCustomWidth = (int) (mCustomBackgroundSize[0] * BACKGROUND_SCALE_FACTOR);
				int maxCustomHeight = (int) (mCustomBackgroundSize[1] * BACKGROUND_SCALE_FACTOR);
				int maxCustomCoverWidth = mCustomBackgroundCoverSize[0];
				int maxCustomCoverHeight = mCustomBackgroundCoverSize[1];
				int maxThumbnailWidth = mCustomBackgroundThumbnailSize[0];
				int maxThumbnailHeight = mCustomBackgroundThumbnailSize[1];

				float widthRatio = (float) maxCustomWidth / (float) bitmapWidth;
				float heightRatio = (float) maxCustomHeight
						/ (float) bitmapHeight;
				float ratio = widthRatio > heightRatio ? heightRatio
						: widthRatio;

				if (android.os.Build.MODEL.toUpperCase().contains("SM-N910")) {
					bitmapWidth = mTakenPicture.getWidth();
					bitmapHeight = mTakenPicture.getHeight();
					maxCustomWidth = 797;
					maxCustomHeight = 448;
					maxCustomCoverWidth = 1173;
					maxCustomCoverHeight = 882;
					maxThumbnailWidth = 230;
					maxThumbnailHeight = 130;

					maxCustomWidth = 532;
					maxCustomHeight = 300;

					widthRatio = (float) maxCustomWidth / (float) bitmapWidth;
					heightRatio = (float) maxCustomHeight
							/ (float) bitmapHeight;
					ratio = widthRatio > heightRatio ? heightRatio : widthRatio;

				}

				// if(bitmapWidth > maxCustomWidth || bitmapHeight >
				// maxCustomHeight) {
				// mTakenPicture = Bitmap.createScaledBitmap(mTakenPicture,
				// (int)Math.ceil(bitmapWidth*ratio),
				// (int)Math.ceil(bitmapHeight*ratio), false);
				// }

				transform.postScale(ratio, ratio);
				Bitmap bitmap = Bitmap.createBitmap(maxCustomWidth,
						maxCustomHeight, Bitmap.Config.ARGB_8888);
				Canvas c = new Canvas(bitmap);
				Paint p = new Paint();
				c.drawBitmap(mTakenPicture, transform, p);

				widthRatio = (float) maxThumbnailWidth / (float) maxCustomWidth;
				heightRatio = (float) maxThumbnailHeight
						/ (float) maxCustomHeight;
				ratio = widthRatio > heightRatio ? heightRatio : widthRatio;

				thumbnail = Bitmap.createScaledBitmap(bitmap,
						maxThumbnailWidth, maxThumbnailHeight, false);

				cover = Bitmap.createBitmap(maxCustomCoverWidth,
						maxCustomCoverHeight, Bitmap.Config.ARGB_8888);
				Canvas coverCanvs = new Canvas(cover);
				coverCanvs.drawBitmap(mTakenPicture,
						(maxCustomCoverWidth - bitmapWidth) / 2,
						(maxCustomCoverHeight - bitmapHeight) / 2, p);

				Bitmap delete = mTakenPicture;
				mTakenPicture = bitmap;
				delete.recycle();
			}

			File saveBackgroundPath = new File(getCacheDir()
					+ "/custom_background");
			if (!saveBackgroundPath.exists())
				saveBackgroundPath.mkdir();

			final File saveBackground = new File(getCacheDir()
					+ "/custom_background/custom_"
					+ Long.toString(System.currentTimeMillis()));
			final File saveBackgroundThumbnail = new File(saveBackground
					+ "_thumb");
			final File saveBackgroundCover = new File(saveBackground + "_cover");

			try {
				if (!saveBackground.exists())
					saveBackground.createNewFile();
				if (!saveBackgroundThumbnail.exists())
					saveBackgroundThumbnail.createNewFile();
				if (!saveBackgroundCover.exists())
					saveBackgroundCover.createNewFile();

				os = new BufferedOutputStream(new FileOutputStream(
						saveBackground));
				if (os != null) {
					mTakenPicture.compress(Bitmap.CompressFormat.PNG, 100, os);
					os.flush();
					os.close();
				}
				ost = new BufferedOutputStream(new FileOutputStream(
						saveBackgroundThumbnail));
				if (ost != null) {
					thumbnail.compress(Bitmap.CompressFormat.PNG, 100, ost);
					ost.flush();
					ost.close();
				}
				osc = new BufferedOutputStream(new FileOutputStream(
						saveBackgroundCover));
				if (osc != null) {
					cover.compress(Bitmap.CompressFormat.PNG, 100, osc);
					osc.flush();
					osc.close();
				}
				runOnGLThread(new Runnable() {
					@Override
					public void run() {
						nativeCustomBackgroundCallback(saveBackground
								.toString());
					}
				});
				nativeBackPress();

				// MixpanelAPI Created Custom Background
				MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(),
						Constants.MIXPANEL_TOKEN);
				mixpanel.track("Created Custom Background", null);
				mixpanel.flush();

			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (mEntryPoint == ENTRY_CREATE_CHARACTER) {
			final ImportImageView image = (ImportImageView) mCreateCharacterView
					.findViewById(R.id.imported_character);
			final Matrix transform = image.getTransformMatrix();
			final ImageView character = (ImageView) mCreateCharacterView
					.findViewById(R.id.character_frame);
			final int frameWidth = character.getWidth();
			final int frameHeight = character.getHeight();
			final int size = mCustomCharacterCropSize[mAvailableCharacters
					.get(mCustomCharacterSelectIndex).imageIndex - 1];

			Bitmap saveImage = Bitmap.createBitmap(size, size,
					Bitmap.Config.ARGB_8888);
			Canvas c = new Canvas(saveImage);
			Paint p = new Paint();
			p.setAntiAlias(true);
			p.setColor(Color.BLACK);
			c.drawCircle(size / 2, size / 2, size / 2, p);
			if (android.os.Build.MODEL.toUpperCase().contains("SM-N910")) {
				int tmpSize = size - 30;
				c.drawCircle(tmpSize / 2, tmpSize / 2, tmpSize / 2, p);
			}

			p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
			c.translate(-(mTakenPicture.getWidth() - size) / 2,
					-(frameHeight - size) / 2);
			c.drawBitmap(mTakenPicture, transform, p);

			Bitmap thumbnail = null;
			if (saveImage != null) {
				int thumbnailSize = mCustomCharacterThumbnailSize[mAvailableCharacters
						.get(mCustomCharacterSelectIndex).imageIndex - 1];
				if (android.os.Build.MODEL.toUpperCase().contains("SM-N910")) {
					thumbnailSize = 60;
				}
				thumbnail = Bitmap.createScaledBitmap(saveImage, thumbnailSize,
						thumbnailSize, false);
			}

			File saveCharacterPath = new File(getCacheDir()
					+ "/custom_character");
			if (!saveCharacterPath.exists())
				saveCharacterPath.mkdir();

			final File saveCharacter = new File(getCacheDir()
					+ "/custom_character/custom_"
					+ Long.toString(System.currentTimeMillis()));
			final File saveCharacterThumbnail = new File(saveCharacter
					+ "_thumb");

			try {
				if (!saveCharacter.exists())
					saveCharacter.createNewFile();
				if (!saveCharacterThumbnail.exists())
					saveCharacterThumbnail.createNewFile();

				os = new BufferedOutputStream(new FileOutputStream(
						saveCharacter));
				if (os != null) {
					saveImage.compress(Bitmap.CompressFormat.PNG, 100, os);
					os.flush();
					os.close();
				}
				ost = new BufferedOutputStream(new FileOutputStream(
						saveCharacterThumbnail));
				if (ost != null) {
					thumbnail.compress(Bitmap.CompressFormat.PNG, 100, ost);
					ost.flush();
					ost.close();
				}

				final int selectIndex = mAvailableCharacters
						.get(mCustomCharacterSelectIndex).characterIndex + 1;
				runOnGLThread(new Runnable() {
					@Override
					public void run() {
						nativeCustomCharacaterCallback(selectIndex,
								saveCharacter.toString());
					}
				});
				nativeBackPress();

				// MixpanelAPI Created Custom Character
				MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(),
						Constants.MIXPANEL_TOKEN);
				mixpanel.track("Created Custom Character", null);
				mixpanel.flush();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void cancelTakePicture(View view) {
		mBlockAutoFocus = false;
		retakePicture(view);
	}

	public void invertCamera(View view) {
		if (mTakePictureMode == TAKE_PICTURE_CAMERA) {
			if (mCamera != null) {
				int cameraCount = Camera.getNumberOfCameras();
				if (cameraCount <= 1)
					return;

				closeCamera();

				if (mCameraFacing == Camera.CameraInfo.CAMERA_FACING_BACK)
					mCameraFacing = Camera.CameraInfo.CAMERA_FACING_FRONT;
				else
					mCameraFacing = Camera.CameraInfo.CAMERA_FACING_BACK;

				int openId = -1;
				Camera.CameraInfo camInfo = new Camera.CameraInfo();
				for (int i = 0; i < cameraCount; i++) {
					Camera.getCameraInfo(i, camInfo);
					if (camInfo.facing == mCameraFacing) {
						openId = i;
						break;
					}
				}

				openCamera(openId);
			}
		}
	}

	public void onHomeCharacterCustomClick(View v) {
		final int position = v.getId() - R.id.character_images_01;
		mCustomCharacterSelectIndex = position;
		updateFrameBackgroundImage();
		updateCharacterFrame();
	}

	public void retakePicture(View view) {
		mTakePictureMode = TAKE_PICTURE_CAMERA;

		if (mEntryPoint == ENTRY_TAKE_PICTURE) {
			if (mImportedImageView != null) {
				mImportedImageView.setVisibility(View.GONE);
				mImportedImageView.setImageBitmap(null);
			}
			mTakePictureView.setBackground(null);
		} else if (mEntryPoint == ENTRY_CREATE_CHARACTER) {
			ImportImageView image = (ImportImageView) mCreateCharacterView
					.findViewById(R.id.imported_character);
			if (image != null) {
				ApplicationImpl app = (ApplicationImpl) getApplication();
				ImageCache cache = app.getImageCache();
				image.clearProperty();
				image.setZoomEnabled(false);
				image.setImageBitmap(null);
				image.setBackgroundColor(Color.TRANSPARENT);
			}
			mCreateCharacterView.setBackground(null);
		}

		changeTakePictureButtonLayer();
		if (mTakenPicture != null) {
			mTakenPicture.recycle();
			mTakenPicture = null;
		}

		if (mEntryPoint == ENTRY_TAKE_PICTURE) {
			startTakePicture();
		} else if (mEntryPoint == ENTRY_CREATE_CHARACTER) {
			startCreateCharacter();
		}
	}

	public void startVideoRecord(int width, int height) {
		startRecord(width, height);
	}

	public void stopVideoRecord() {
		stopRecord();

		// TODO mv file to External
		// moveResourceAndMediaScan(getCacheDir() + "/video.mp4");
	}

	public void changeVideoRecordStatus(int pause) {
		changeRecordStatus(pause == 0 ? false : true);
	}

	public void startQRCodeScan() {
		// mOrientationBackup = getRequestedOrientation();
		// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		// startCameraPreview(ENTRY_QRCODE);
	}

	public void stopQRCodeScan() {
		// stopCameraPreview(ENTRY_QRCODE);
		// setRequestedOrientation(mOrientationBackup);
		// mOrientationBackup = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED;
	}

	public void showLogin() {
		showMenu(PortraitActivity.ENTRY_MENU_LOGIN);
	}

	public void showDoleCoin() {
		showMenu(PortraitActivity.ENTRY_MENU_DOLE_COIN);
	}

	public void showSetting() {
		showMenu(PortraitActivity.ENTRY_MENU_SETTING);
	}

	public void setPendingAction(int action) {
		mPendingAction = action;
	}

	@Override
	public void setEditTextContents(String[] contents) {
		MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(), Constants.MIXPANEL_TOKEN);
		try {
			String newTitle = contents[0];
			String newAuthor = contents[1];
			if (mNativeEditTextContents != null) {
				if (newTitle != null && !newTitle.equals(mNativeEditTextContents[0])) {
					mixpanel.track("Changed Custom Storybook Title", null);
				}
				if (newAuthor !=null && !newAuthor.equals(mNativeEditTextContents[1])) {
					mixpanel.track("Changed Custom Storybook Author", null);
				}
				mixpanel.flush();
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		mNativeEditTextContents = contents;
	}

	public void feedAudioFileName(String[] voice) {
		final int voiceLength = voice.length;
		final String path = getCacheDir().toString();
		final String expPath = APKExpansionSupport.getAPKExpansionPath(this);
		// ArrayList<String> voiceList = new ArrayList<String>();
		ArrayList<String> mixedList = new ArrayList<String>();

		for (int i = 0; i < voice.length; i++) {
			String vItem = voice[i];
			if (vItem != null && vItem.length() > 0) {
				String voicePath = new String(path + File.separator
						+ RECORD_PATH + File.separator + vItem);
				// voiceList.add(voicePath);
				// mSoundPoolMap.put(voicePath,
				// Integer.valueOf(mSoundPool.load(voicePath, 1)));

				String mixedPath = new String(voicePath.substring(0,
						voicePath.lastIndexOf("."))
						+ AUDIO_MIX_POSTFIX
						+ voicePath.substring(voicePath.lastIndexOf(".")));
				File mixedFile = new File(mixedPath);
				if (mixedFile.exists())
					mixedList.add(mixedPath);
				else
					mixedList.add(voicePath);

				mixedList.add(expPath
						+ "/common/preload_sounds/scene_transition.aac");
			}
		}

		mixedList.remove(mixedList.size() - 1);
		mixedList.add(0, expPath + "/common/preload_sounds/scene_opening.aac");
		String language = Locale.getDefault().getLanguage();
		if (Locale.JAPANESE.getLanguage().equals(language)) {
			mixedList.add(expPath
					+ "/common/preload_sounds/ja/scene_ending.aac");
		} else if (Locale.KOREAN.getLanguage().equals(language)) {
			mixedList.add(expPath
					+ "/common/preload_sounds/kor/scene_ending.aac");
		} else {
			mixedList.add(expPath
					+ "/common/preload_sounds/en/scene_ending.aac");
		}
		setAudioFileList(mixedList);
	}

	public void changeVoiceRecordStatus(final String filename, int status) {
		switch (status) {
		case RECORD_START:
			if (!mOnRecordVoice) {
				mOnRecordVoice = true;
				mRecordingFileIndex = 0;
				File path = new File(getCacheDir() + File.separator
						+ RECORD_PATH);
				if (!path.exists()) {
					path.mkdir();
				}
				mRecordingFileName = path + File.separator + filename;
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						startVoiceRecord(mRecordingFileName);
					}
				});
			}
			break;

		case RECORD_STOP:
			if (mOnRecordVoice) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						stopVoiceRecord();
						mOnRecordVoice = false;
					}
				});
			}
			break;

		case RECORD_PAUSE:
			if (mOnRecordVoice) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						stopVoiceRecord();
						mOnRecordVoice = false;
					}
				});
			}
			break;

		case RECORD_RESUME:
			if (!mOnRecordVoice) {
				mOnRecordVoice = true;
				mRecordingFileIndex++;

				if (mRecordingFileName == null) {
					File path = new File(getCacheDir() + File.separator
							+ RECORD_PATH);
					if (!path.exists()) {
						path.mkdir();
					}
					mRecordingFileName = path + File.separator + filename;
				}

				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						startVoiceRecord(mRecordingFileName);
					}
				});
			}
			break;

		case RECORD_DELETE:
			String prefix = filename.substring(0, filename.lastIndexOf("."));
			String cachePath = getCacheDir() + File.separator + RECORD_PATH;
			File tempPath = new File(cachePath);
			String[] fileList = tempPath.list();
			if (fileList != null) {
				for (String file : fileList) {
					if (file.startsWith(prefix)) {
						File toDelete = new File(cachePath + File.separator
								+ file);
						if (toDelete.exists()) {
							toDelete.delete();
							int deleteId = -1;
							if (mSoundPoolMap != null) {
								if (mSoundPoolMap.containsKey(toDelete
										.toString()))
									deleteId = mSoundPoolMap.remove(
											toDelete.toString()).intValue();
							}
							if (mSoundPool != null && deleteId >= 0) {
								mSoundPool.unload(deleteId);
							}
						}
					}
				}
			}
			break;

		case RECORD_SAVE:
			if (mRecordSaveRunnable != null)
				return;

			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					final ProgressDialog dialog = Util
							.openProgressDialog(BobbysStoryWorldActivity.this);
					dialog.show();
					mRecordSaveRunnable = new Runnable() {
						@Override
						public void run() {
							try {
								MediaMuxer muxer = new MediaMuxer(
										mRecordingFileName,
										OutputFormat.MUXER_OUTPUT_MPEG_4);
								MediaExtractor extractor = new MediaExtractor();
								ByteBuffer inputBuffer = ByteBuffer
										.allocate(256 * 1024);
								boolean finished = false;
								int index = mRecordingFileName.lastIndexOf(".");
								int startFileIndex = 0;
								int offset = 0;
								int fileOffset = 0;
								long presentTimeOffset = 0L;
								BufferInfo bufferInfo = new BufferInfo();
								String currentVoiceFile = null;
								File temp = null;
								int trackCount = 0;
								boolean muxerStarted = false;
								do {
									temp = null;
									trackCount = 0;
									currentVoiceFile = mRecordingFileName
											.substring(0, index)
											+ "_"
											+ startFileIndex
											+ mRecordingFileName
													.substring(index);

									if (currentVoiceFile != null)
										temp = new File(currentVoiceFile);

									if (!temp.exists() || temp.length() <= 0) {
										temp = null;
										startFileIndex++;
									}

									if (temp != null) {
										extractor
												.setDataSource(currentVoiceFile);
										trackCount = extractor.getTrackCount();
										if (trackCount <= 0) {
											temp = null;
											startFileIndex++;
										}
									}
								} while (startFileIndex < mRecordingFileIndex
										&& temp == null);

								int audioTrack = -1;
								if (temp == null) {
									finished = true;
								} else {
									extractor.selectTrack(0);
									MediaFormat autioFormat = extractor
											.getTrackFormat(0);
									audioTrack = muxer.addTrack(autioFormat);

									muxer.start();
									muxerStarted = true;
								}

								while (!finished) {
									bufferInfo.offset = offset;
									bufferInfo.size = extractor.readSampleData(
											inputBuffer, offset);
									if (bufferInfo.size < 0) {
										// EOS
										if (startFileIndex < mRecordingFileIndex) {
											fileOffset += offset;
											presentTimeOffset += bufferInfo.presentationTimeUs;
											startFileIndex++;
											do {
												temp = null;
												currentVoiceFile = mRecordingFileName
														.substring(0, index)
														+ "_"
														+ startFileIndex
														+ mRecordingFileName
																.substring(index);
												if (currentVoiceFile != null)
													temp = new File(
															currentVoiceFile);
												if (!temp.exists()
														|| temp.length() <= 0) {
													temp = null;
													startFileIndex++;
												}
											} while (startFileIndex < mRecordingFileIndex
													&& temp == null);
											if (extractor != null) {
												extractor.release();
												extractor = new MediaExtractor();
											}
											extractor
													.setDataSource(currentVoiceFile);
											extractor.selectTrack(0);
										} else {
											finished = true;
										}
									} else {
										if (audioTrack >= 0) {
											bufferInfo.presentationTimeUs = presentTimeOffset
													+ extractor.getSampleTime();
											bufferInfo.flags = extractor
													.getSampleFlags();
											muxer.writeSampleData(audioTrack,
													inputBuffer, bufferInfo);
											extractor.advance();
										}
									}
								}

								if (extractor != null)
									extractor.release();
								extractor = null;

								if (muxerStarted)
									muxer.stop();
								muxer.release();

								for (int i = 0; i <= mRecordingFileIndex; i++) {
									String deleteName = mRecordingFileName
											.substring(0, index)
											+ "_"
											+ i
											+ mRecordingFileName
													.substring(index);
									File deleteFile = new File(deleteName);
									if (deleteFile.exists())
										deleteFile.delete();
								}

								if (filename != null) {
									String bgm = APKExpansionSupport
											.getAPKExpansionPath(BobbysStoryWorldActivity.this)
											+ File.separator
											+ "common"
											+ File.separator + filename;
									final int extIndex = mRecordingFileName
											.lastIndexOf(".");
									String outfile = mRecordingFileName
											.substring(0, extIndex)
											+ AUDIO_MIX_POSTFIX
											+ mRecordingFileName
													.substring(extIndex);

									nativeOnFinishVoiceRecording(
											mRecordingFileName, bgm, outfile);
								}

								mRecordingFileName = null;
								mRecordingFileIndex = 0;
							} catch (IOException e) {
								e.printStackTrace();
							} finally {
								dialog.dismiss();
								mRecordSaveRunnable = null;
							}
						}
					};

					new Handler().post(mRecordSaveRunnable);
				}
			});
			break;
		}
	}

	public void changeVoicePlayStatus(final String filename, int status) {
		switch (status) {
		case PLAY_START:
			// mOnPlayVoice = true;
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					prepareVoicePlay(filename);
				}
			});
			break;

		case PLAY_STOP:
			// if(mOnPlayVoice) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					stopVoicePlay(filename);
					// mOnPlayVoice = false;
				}
			});
			// }
			break;

		case PLAY_PAUSE:
			// if(mOnPlayVoice) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					pauseVoicePlay(filename);
				}
			});
			// }
			break;

		case PLAY_RESUME:
			// if(!mOnPlayVoice) {
			// mOnPlayVoice = true;
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					resumeVoicePlay(filename);
				}
			});
			// }
			break;
		}
	}

	public void changeBGMPlayStatus(final String filename, int status) {
		switch (status) {
		case PLAY_START:
			mOnPlayBGM = true;
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					prepareBGMPlay(filename);
				}
			});
			break;

		case PLAY_STOP:
			if (mOnPlayBGM) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						stopBGMPlay();
						mOnPlayBGM = false;
					}
				});
			}
			break;

		case PLAY_PAUSE:
			if (mOnPlayBGM) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						pauseBGMPlay();
					}
				});
			}
			break;

		case PLAY_RESUME:
			if (!mOnPlayBGM) {
				mOnPlayBGM = true;
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						startBGMPlay();
					}
				});
			}
			break;
		}
	}

	public void showMenu(int entry) {
		Intent i = new Intent(this, PortraitActivity.class);
		i.putExtra(PortraitActivity.MENU_MODE, entry);
		switch (mPendingAction) {
		case DOLE_COIN:
			i.putExtra(PortraitActivity.MENU_PENDING,
					PortraitActivity.ENTRY_MENU_DOLE_COIN);
			break;

		case SHARE:
		case DOWNLOAD_TO_DEVICE:
		case PENDING_PURCHASE:
			i.putExtra(PortraitActivity.MENU_PENDING, mPendingAction);
			break;
		}
		startActivity(i);
	}

	public void mixpanelTrack(String eventName, String dictString) 
	{
		Log.i("Mixpanel JNI", "Mixpanel event: " + eventName);
		Log.i("Mixpanel JNI", "Mixpanel dictString: " + dictString);
		try {
			MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(),
					Constants.MIXPANEL_TOKEN);
			if (dictString != null) {
				mixpanel.track(eventName, new JSONObject(dictString));
			} else {
				mixpanel.track(eventName, null);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void sharedVideoFacebook() {
		MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(),
				Constants.MIXPANEL_TOKEN);
		mixpanel.track("Shared Video Via Facebook", null);
		mixpanel.flush();
	}

	public void sharedVideoYoutube() {
		MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(),
				Constants.MIXPANEL_TOKEN);
		mixpanel.track("Shared Video Via Youtube", null);
		mixpanel.flush();
	}

	public void purchasedCharacter(String purchaseKey) {
		MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(),
				Constants.MIXPANEL_TOKEN);
		JSONObject props = new JSONObject();
		try {
			props.put("Character", purchaseKey);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mixpanel.track("Purchased Character", props);
		mixpanel.flush();
	}

	public void purchasedBackground(String purchaseKey) {
		MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(),
				Constants.MIXPANEL_TOKEN);
		JSONObject props = new JSONObject();
		try {
			props.put("Background", purchaseKey);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mixpanel.track("Purchased Background", props);
		mixpanel.flush();
	}

	public void createStoryStart(int tag, int finalCounter) {
		currentVideo = tag;
		videoCounter = finalCounter;

		MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(),
				Constants.MIXPANEL_TOKEN);
		JSONObject props = new JSONObject();
		try {
			props.put("Storybook", "Custom");
			String email = BobbysStoryWorldPreference.getString(
					Constants.PREF_USER_ID, "");
			props.put("CreatedBy", email);
			props.put("Video", "Video " + tag);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mixpanel.track("Started Creating Story in Custom Storybook", props);
		mixpanel.flush();
	}

	public void createStorySuccess() {
		MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(),
				Constants.MIXPANEL_TOKEN);
		JSONObject props = new JSONObject();
		try {
			props.put("Storybook", "Custom");
			String email = BobbysStoryWorldPreference.getString(
					Constants.PREF_USER_ID, "");
			props.put("CreatedBy", email);
			props.put("Video", "Video " + currentVideo);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mixpanel.track("Successfully Created Story in Custom Storybook", props);

		/* JP Moved to CharacterRecordingScene.cpp 
		// check if user completed all stories
		if (videoCounter >= 4) {
			JSONObject props2 = new JSONObject();
			try {
				props2.put("Storybook", "Custom");
				String email = BobbysStoryWorldPreference.getString(
						Constants.PREF_USER_ID, "");
				props2.put("CreatedBy", email);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mixpanel.track("Successfully Completed Custom Storybook", props2);
		}*/
		mixpanel.flush(); 
	}

	public void didOpenBook(String bookKey) {
		
		/* 
		Log.i("", "bookkey: " + bookKey);

		MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(),
				Constants.MIXPANEL_TOKEN);
		JSONObject props = new JSONObject();
		try {
			if (bookKey.equalsIgnoreCase("book1")) {
				props.put("Storybook", "Sample A");
				mixpanel.track("Opened Book Sample", props);
			} else if (bookKey.equalsIgnoreCase("book2")) {
				props.put("Storybook", "Sample B");
				mixpanel.track("Opened Book Sample", props);
			} else {//if (bookKey.contains("my_story")) {
				props.put("Storybook", "Custom");
				String email = BobbysStoryWorldPreference.getString(
						Constants.PREF_USER_ID, "");
				props.put("CreatedBy", email);
				mixpanel.track("Started Creating Custom Storybook", props);
				//mixpanel.track("Opened Custom Storybook", props);
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mixpanel.flush(); */
	}

	public void didOpenBookA() {
		/*
		MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(),
				Constants.MIXPANEL_TOKEN);
		JSONObject props = new JSONObject();
		try {
			props.put("Storybook", "Sample A");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mixpanel.track("Opened Book Sample", props);
		mixpanel.flush();
		*/
	}

	public void didOpenBookB() {
		/*
		MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(),
				Constants.MIXPANEL_TOKEN);
		JSONObject props = new JSONObject();
		try {
			props.put("Storybook", "Sample B");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mixpanel.track("Opened Book Sample", props);
		mixpanel.flush();
		*/
	}

	public void didCreateCustomBook() {
		/*
		MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(),
				Constants.MIXPANEL_TOKEN);
		JSONObject props = new JSONObject();
		try {
			props.put("Storybook", "Custom");
			String email = BobbysStoryWorldPreference.getString(
					Constants.PREF_USER_ID, "");
			props.put("CreatedBy", email);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mixpanel.track("Successfully Created Story in Custom Storybook", props);
		mixpanel.flush(); */
	}

	public void startPlayVideo(final String filename) {
		mEntryPoint = ENTRY_PLAY_VIDEO;
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (mVideoView != null) {
					try {
						String language = "en";
						if (Locale.getDefault().equals(Locale.JAPAN))
							language = "ja";
						else if (Locale.getDefault().equals(Locale.KOREA))
							language = "kor";

						mBGMPlayer = new MediaPlayer();
						mBGMPlayer.setOnPreparedListener(mPreparedListener);
						mBGMPlayer.setOnCompletionListener(mCompletionListener);
						mBGMPlayer.setOnErrorListener(mErrorListener);
						mBGMPlayer
								.setAudioStreamType(AudioManager.STREAM_MUSIC);
						mPreloadVideoScene = Integer.parseInt(filename
								.substring(filename.lastIndexOf("_") + 1,
										filename.lastIndexOf("."))) % 5;
						if (Constants.DEBUG) {
							AssetManager manager = getAssets();
							AssetFileDescriptor fd = manager.openFd("common"
									+ File.separator + filename);
							mVideoView.setVideoFileDescriptor(
									fd.getFileDescriptor(),
									fd.getStartOffset(), fd.getLength());
							AssetFileDescriptor bgm = manager
									.openFd("common/preload_sounds/"
											+ language
											+ File.separator
											+ filename.substring(
													0,
													filename.lastIndexOf(".") + 1)
											+ "wav");
							mBGMPlayer.setDataSource(bgm.getFileDescriptor(),
									bgm.getStartOffset(), bgm.getLength());
						} else {
							String prefix = APKExpansionSupport
									.getAPKExpansionPath(BobbysStoryWorldActivity.this);
							mVideoView.setVideoPath(prefix + File.separator
									+ "common" + File.separator + filename);
							mBGMPlayer.setDataSource(prefix
									+ File.separator
									+ "common/preload_sounds/"
									+ language
									+ File.separator
									+ filename.substring(0,
											filename.lastIndexOf(".") + 1)
									+ "wav");
						}
						mBGMPlayer.prepareAsync();
						mCurrentState = STATE_PREPARING;
						mTargetState = STATE_PLAYING;
						mDoleProgress.setTotalScene(1);
						mDoleProgress.setCurSceneId(1);

						mVideoView.setVisibility(View.VISIBLE);
						mVideoView.requestFocus();
						mVideoView.start();
						mVideoStopView.findViewById(R.id.video_stop_play_pause)
								.setSelected(false);
						nativeHide();

						Cocos2dxHelper.runOnGLThread(new Runnable() {
							@Override
							public void run() {
								nativeStopBGM();
							}
						});

						// report video played in mixpanel
						/*
						MixpanelAPI mixpanel = MixpanelAPI.getInstance(
								getContext(), Constants.MIXPANEL_TOKEN);
						JSONObject props = new JSONObject();
						try {
							if (filename.equalsIgnoreCase("scene_01.mp4")) {
								props.put("Storybook", "Sample A");
								props.put("Video", "Video 1");
								mixpanel.track("Played Video", props);
							} else if (filename
									.equalsIgnoreCase("scene_02.mp4")) {
								props.put("Storybook", "Sample A");
								props.put("Video", "Video 2");
								mixpanel.track("Played Video", props);
							} else if (filename
									.equalsIgnoreCase("scene_03.mp4")) {
								props.put("Storybook", "Sample A");
								props.put("Video", "Video 3");
								mixpanel.track("Played Video", props);
							} else if (filename
									.equalsIgnoreCase("scene_04.mp4")) {
								props.put("Storybook", "Sample A");
								props.put("Video", "Video 4");
								mixpanel.track("Played Video", props);
							} else if (filename
									.equalsIgnoreCase("scene_05.mp4")) {
								props.put("Storybook", "Sample A");
								props.put("Video", "Video 5");
								mixpanel.track("Played Video", props);
							} else if (filename
									.equalsIgnoreCase("scene_06.mp4")) {
								props.put("Storybook", "Sample B");
								props.put("Video", "Video 1");
								mixpanel.track("Played Video", props);
							} else if (filename
									.equalsIgnoreCase("scene_07.mp4")) {
								props.put("Storybook", "Sample B");
								props.put("Video", "Video 2");
								mixpanel.track("Played Video", props);
							} else if (filename
									.equalsIgnoreCase("scene_08.mp4")) {
								props.put("Storybook", "Sample B");
								props.put("Video", "Video 3");
								mixpanel.track("Played Video", props);
							} else if (filename
									.equalsIgnoreCase("scene_09.mp4")) {
								props.put("Storybook", "Sample B");
								props.put("Video", "Video 4");
								mixpanel.track("Played Video", props);
							} else if (filename
									.equalsIgnoreCase("scene_10.mp4")) {
								props.put("Storybook", "Sample B");
								props.put("Video", "Video 5");
								mixpanel.track("Played Video", props);
							} else {
								props.put("Storybook", "Custom");
								mixpanel.track("Played Video", props);
							}
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						//mixpanel.track("Played Video", props);
						mixpanel.flush();
						*/
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	public void startPlayVideoList(final String[] videoList) {
		mEntryPoint = ENTRY_PLAY_VIDEO_LIST;
		mPreloadVideoList.clear();
		mVideoAudioReadyCount = 0;
		mOnPlayVideoList = false;

		if (videoList.length <= 0)
			return;

		for (String item : videoList) {
			mPreloadVideoList.add(item);
		}

		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (mVideoView != null) {
					String filename = mPreloadVideoList.remove(0);

					/*
					MixpanelAPI mixpanel = MixpanelAPI.getInstance(
							getContext(), Constants.MIXPANEL_TOKEN);
					JSONObject props = new JSONObject();
					try {
						if (filename.equalsIgnoreCase("scene_01.mp4"))
							props.put("Storybook", "Sample A");
						if (filename.equalsIgnoreCase("scene_06.mp4"))
							props.put("Storybook", "Sample B");
						props.put("Video", "All");
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					mixpanel.track("Played Video", props);
					mixpanel.flush();
					 */
					try {
						String language = "en";
						if (Locale.getDefault().equals(Locale.JAPAN))
							language = "ja";
						else if (Locale.getDefault().equals(Locale.KOREA))
							language = "kor";

						mBGMPlayer = new MediaPlayer();
						mBGMPlayer.setOnPreparedListener(mPreparedListener);
						mBGMPlayer.setOnCompletionListener(mCompletionListener);
						mBGMPlayer.setOnErrorListener(mErrorListener);
						mBGMPlayer
								.setAudioStreamType(AudioManager.STREAM_MUSIC);
						mPreloadVideoScene = Integer.parseInt(filename
								.substring(filename.lastIndexOf("_") + 1,
										filename.lastIndexOf("."))) % 5;
						if (Constants.DEBUG) {
							AssetManager manager = getAssets();
							AssetFileDescriptor fd = manager.openFd("common"
									+ File.separator + filename);
							mVideoView.setVideoFileDescriptor(
									fd.getFileDescriptor(),
									fd.getStartOffset(), fd.getLength());
							AssetFileDescriptor bgm = manager
									.openFd("common/preload_sounds/"
											+ language
											+ File.separator
											+ filename.substring(
													0,
													filename.lastIndexOf(".") + 1)
											+ "wav");
							mBGMPlayer.setDataSource(bgm.getFileDescriptor(),
									bgm.getStartOffset(), bgm.getLength());
						} else {
							String prefix = APKExpansionSupport
									.getAPKExpansionPath(BobbysStoryWorldActivity.this);
							mVideoView.setVideoPath(prefix + File.separator
									+ "common" + File.separator + filename);
							mBGMPlayer.setDataSource(prefix
									+ File.separator
									+ "common/preload_sounds/"
									+ language
									+ File.separator
									+ filename.substring(0,
											filename.lastIndexOf(".") + 1)
									+ "wav");
						}
						mBGMPlayer.prepareAsync();
						mCurrentState = STATE_PREPARING;
						mTargetState = STATE_WAIT_PLAY;
						mDoleProgress.setTotalScene(5);
						mDoleProgress.setCurSceneId(mPreloadVideoScene);
						mVideoView.setVisibility(View.VISIBLE);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	public void playVideoList(int play) {
		if (mEntryPoint != ENTRY_PLAY_VIDEO_LIST)
			return;

		if (play == 1) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (mCurrentState != STATE_PREPARED
							|| mTargetState != STATE_WAIT_PLAY)
						return;

					startBGMPlay();
					mVideoView.requestFocus();
					mVideoView.start();
					mVideoStopView.findViewById(R.id.video_stop_play_pause)
							.setSelected(false);
					nativeHide();
					mOnPlayVideoList = true;

					Cocos2dxHelper.runOnGLThread(new Runnable() {
						@Override
						public void run() {
							nativeOnStartVideoListPlay();
						}
					});
				}
			});
		} else {
			mPreloadVideoList.clear();
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					stopBGMPlay();
					mVideoView.stopPlayback();
					nativeShow();

					Cocos2dxHelper.runOnGLThread(new Runnable() {
						@Override
						public void run() {
							if (mEntryPoint == ENTRY_PLAY_VIDEO_LIST
									&& mOnPlayVideoList)
								nativeOnStopVideoListPlay();
							mEntryPoint = ENTRY_MAIN;
							mOnPlayVideoList = false;

							BobbysStoryWorldActivity.this
									.runOnUiThread(new Runnable() {
										@Override
										public void run() {
											if (mVideoView != null
													&& mVideoStopView != null) {
												mVideoView
														.setVisibility(View.GONE);
												mVideoStopView
														.setVisibility(View.GONE);
											}
										}
									});
						}
					});
				}
			});
		}
	}

	public void stopPlayVideo() {
		mEntryPoint = ENTRY_MAIN;
		if (mVideoView != null && mVideoStopView != null) {
			nativeShow();

			stopBGMPlay();
			mVideoView.stopPlayback();
			mVideoView.setVisibility(View.GONE);
			mVideoStopView.setVisibility(View.GONE);
		}
	}

	public void startTakePicture() {
		startCameraPreview(ENTRY_TAKE_PICTURE);
	}

	public void stopTakePicture() {
		stopCameraPreview(mEntryPoint);
	}

	public void startCreateCharacter() {
		startCameraPreview(ENTRY_CREATE_CHARACTER);
	}

	public void startCreateCharacter(String[] characterKey, String[] image,
			String[] locked) {
		mAvailableCharacters.clear();

		for (int i = 0; i < characterKey.length; i++) {
			if (locked[i].equals("0")) {
				String current = characterKey[i];
				String img = image[i];
				int postFixPosition = 0;
				if (img != null && img.length() > 0)
					postFixPosition = img.lastIndexOf("_normal");

				int index = Integer.parseInt(current.substring(1)) - 1;
				int imgIndex = 0;
				if (postFixPosition > 0)
					imgIndex = Integer.parseInt(img.substring(
							postFixPosition - 2, postFixPosition));
				if (index >= 0 && mCustomCharacterFrame[index] > 0
						&& imgIndex > 0) {
					CustomCharacterData data = new CustomCharacterData();
					data.characterIndex = index;
					data.imageIndex = imgIndex;
					mAvailableCharacters.add(data);
				}
			}
		}

		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				updateFrameBackgroundImage();
				startCameraPreview(ENTRY_CREATE_CHARACTER);
			}
		});
	}

	public void stopCreateCharacter() {
		stopCameraPreview(mEntryPoint);
	}

	public int getPendingDoleCoin() {
		final int balance = BobbysStoryWorldPreference.getInt(
				Constants.PREF_PENDING_DOLE_COIN, -1);
		if (balance >= 0)
			BobbysStoryWorldPreference.remove(Constants.PREF_PENDING_DOLE_COIN);
		return balance;
	}

	public boolean getPendingGuideHome() {
		final boolean show = BobbysStoryWorldPreference.getBoolean(
				Constants.PREF_PENDING_GUIDE_HOME, false);
		if (show)
			BobbysStoryWorldPreference
					.remove(Constants.PREF_PENDING_GUIDE_HOME);
		return show;
	}

	public void startCameraPreview(final int entryPoint) {
		Cocos2dxHelper.runOnGLThread(new Runnable() {
			@Override
			public void run() {
				nativeStopBGM();
			}
		});

		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				mEntryPoint = entryPoint;
				mImportImageUri = null;

				showCameraPreviewFrameLayout(entryPoint);

				if (mTakePictureMode == TAKE_PICTURE_CAMERA) {
					if (mTextureView != null) {
						mTextureView.setVisibility(View.VISIBLE);
						if (mSurface == null) {
							mPendingCameraOpen = true;
						} else {
							openCamera();
						}
					}
				}
			}
		});
	}

	public void stopCameraPreview(final int entryPoint) {
		Cocos2dxHelper.runOnGLThread(new Runnable() {
			@Override
			public void run() {
				nativePlayBGM();
			}
		});

		mSensorManager.unregisterListener(this);

		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				nativeShow();

				if (entryPoint <= 0) {
					return;
				}
				mEntryPoint = ENTRY_MAIN;

				switch (entryPoint) {
				case ENTRY_TAKE_PICTURE:
					if (mImportedImageView != null) {
						mImportedImageView.setVisibility(View.GONE);
						mImportedImageView.setImageBitmap(null);
					}

					if (mTakePictureView != null) {
						mTakePictureMode = TAKE_PICTURE_CAMERA;
						changeTakePictureButtonLayer();
						mTakePictureView.setVisibility(View.GONE);
					}
					break;

				case ENTRY_CREATE_CHARACTER:
					ImportImageView image = (ImportImageView) mCreateCharacterView
							.findViewById(R.id.imported_character);
					if (image != null) {
						ApplicationImpl app = (ApplicationImpl) getApplication();
						ImageCache cache = app.getImageCache();
						image.clearProperty();
						image.setZoomEnabled(false);
						image.setImageBitmap(null);
						image.setBackgroundColor(Color.TRANSPARENT);
						mCreateCharacterView.setBackground(null);
					}

					if (mCreateCharacterView != null) {
						mTakePictureMode = TAKE_PICTURE_CAMERA;
						changeTakePictureButtonLayer();
						mCreateCharacterView.setVisibility(View.GONE);
					}

					mCustomCharacterSelectIndex = 0;
					HorizontalScrollView scroll = (HorizontalScrollView) mCreateCharacterView
							.findViewById(R.id.create_character_images)
							.getParent();
					if (scroll != null)
						scroll.scrollTo(0, 0);
					break;
				}

				if (mTextureView != null) {
					mTextureView.setVisibility(View.GONE);
					if ((entryPoint & FLAG_CAMERA_PREVIEW) != 0)
						closeCamera();
				}
				mCameraFacing = Camera.CameraInfo.CAMERA_FACING_BACK;
				mImportImageUri = null;
			}
		});
	}

	private void saveInstanceState() {
		ApplicationImpl app = (ApplicationImpl) getApplication();
		BobbysStoryWorldPreference.putInt(SAVED_HASH_CODE, app.hashCode());
		BobbysStoryWorldPreference
				.putInt(SAVED_ORIENTATION, mOrientationBackup);
		BobbysStoryWorldPreference.putInt(SAVED_ENTRY_POINT, mEntryPoint);
		BobbysStoryWorldPreference.putInt(SAVED_MAX_TEXTURE_SIZE,
				mMaxTextureSize);
		BobbysStoryWorldPreference.putInt(SAVED_TAKE_PICTURE_MODE,
				mTakePictureMode);
		BobbysStoryWorldPreference.putInt(SAVED_CHARACTER_SELECT_INDEX,
				mCustomCharacterSelectIndex);
		final int indexSize = mAvailableCharacters.size();
		if (indexSize > 0) {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < indexSize; i++) {
				CustomCharacterData data = mAvailableCharacters.get(i);
				sb.append(data.characterIndex);
				sb.append(":");
				sb.append(data.imageIndex);
				if (i < indexSize - 1)
					sb.append(",");
			}
			BobbysStoryWorldPreference.putString(SAVED_CHARACTER_INDEX_ARRAY,
					sb.toString());
		}
		if (mImportImageUri != null)
			BobbysStoryWorldPreference.putString(SAVED_IMPORT_IMAGE_URI,
					mImportImageUri.toString());
	}

	private void restoreInstanceState() {
		ApplicationImpl app = (ApplicationImpl) getApplication();
		int hashCode = BobbysStoryWorldPreference.getInt(SAVED_HASH_CODE, 0);
		if (hashCode == app.hashCode()) {
			mOrientationBackup = BobbysStoryWorldPreference.getInt(
					SAVED_ORIENTATION, mOrientationBackup);
			mEntryPoint = BobbysStoryWorldPreference.getInt(SAVED_ENTRY_POINT,
					mEntryPoint);
			mMaxTextureSize = BobbysStoryWorldPreference.getInt(
					SAVED_MAX_TEXTURE_SIZE, mMaxTextureSize);
			mTakePictureMode = BobbysStoryWorldPreference.getInt(
					SAVED_TAKE_PICTURE_MODE, mTakePictureMode);
			mCustomCharacterSelectIndex = BobbysStoryWorldPreference.getInt(
					SAVED_CHARACTER_SELECT_INDEX, mCustomCharacterSelectIndex);
			String uri = BobbysStoryWorldPreference.getString(
					SAVED_IMPORT_IMAGE_URI, (mImportImageUri == null ? null
							: mImportImageUri.toString()));
			if (uri != null)
				mImportImageUri = Uri.parse(uri);

			String indexString = BobbysStoryWorldPreference.getString(
					SAVED_CHARACTER_INDEX_ARRAY, null);
			if (indexString != null && indexString.length() > 0) {
				String[] indexList = indexString.split(",");
				mAvailableCharacters.clear();
				for (int i = 0; i < indexList.length; i++) {
					CustomCharacterData data = new CustomCharacterData();
					String[] indexes = indexList[i].split(":");
					data.characterIndex = Integer.parseInt(indexes[0]);
					data.imageIndex = Integer.parseInt(indexes[1]);
					mAvailableCharacters.add(data);
				}
			}
		}

		BobbysStoryWorldPreference.remove(SAVED_ORIENTATION);
		BobbysStoryWorldPreference.remove(SAVED_ENTRY_POINT);
		BobbysStoryWorldPreference.remove(SAVED_MAX_TEXTURE_SIZE);
		BobbysStoryWorldPreference.remove(SAVED_TAKE_PICTURE_MODE);
		BobbysStoryWorldPreference.remove(SAVED_CHARACTER_SELECT_INDEX);
		BobbysStoryWorldPreference.remove(SAVED_IMPORT_IMAGE_URI);
		BobbysStoryWorldPreference.remove(SAVED_CHARACTER_INDEX_ARRAY);
	}

	private void playNextListItem() {
		mVideoAudioReadyCount = 0;
		startBGMPlay();
		mVideoView.start();
	}

	private void handleImportImage() {
		try {
			Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext()
					.getContentResolver(), mImportImageUri);
			if (bitmap != null) {
				final int bitmapWidth = bitmap.getWidth();
				final int bitmapHeight = bitmap.getHeight();
				if (bitmapWidth > mMaxTextureSize) {
					bitmap = Bitmap
							.createScaledBitmap(
									bitmap,
									mMaxTextureSize,
									(int) (bitmapHeight * mMaxTextureSize / (float) bitmapWidth),
									false);
				} else if (bitmapHeight > mMaxTextureSize) {
					bitmap = Bitmap
							.createScaledBitmap(bitmap, (int) (bitmapWidth
									* mMaxTextureSize / (float) bitmapHeight),
									mMaxTextureSize, false);
				}

				if (mEntryPoint == ENTRY_TAKE_PICTURE) {
					if (mImportedImageView != null) {
						mImportedImageView.setVisibility(View.VISIBLE);
						mImportedImageView.setImageBitmap(bitmap);
					}
					setTakePictureTileBackground(true);
				} else if (mEntryPoint == ENTRY_CREATE_CHARACTER) {
					ImportImageView image = (ImportImageView) mCreateCharacterView
							.findViewById(R.id.imported_character);
					if (image != null) {
						image.setZoomEnabled(true);
						image.setImageBitmap(bitmap);
						image.setBackgroundResource(R.drawable.camera_import_bg);
					}
				}

				mTakenPicture = bitmap;
				mTextureView.setVisibility(View.GONE);
				closeCamera();
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
	}

	private void showCameraPreviewFrameLayout(int entryPoint) {
		switch (entryPoint) {
		case ENTRY_TAKE_PICTURE:
			if (mTakePictureView != null)
				mTakePictureView.setVisibility(View.VISIBLE);
			changeTakePictureButtonLayer();
			if (mTakePictureMode == TAKE_PICTURE_CAMERA)
				setTakePictureTileBackground(false);

			if (mTakePictureMode != TAKE_PICTURE_CAMERA)
				return;
			break;

		case ENTRY_CREATE_CHARACTER:
			if (mCreateCharacterView != null)
				mCreateCharacterView.setVisibility(View.VISIBLE);

			if (mAvailableCharacters.size() > 0) {
				LinearLayout scroll = (LinearLayout) mCreateCharacterView
						.findViewById(R.id.create_character_images);
				if (scroll != null) {
					scroll.removeAllViews();
					LayoutInflater li = (LayoutInflater) BobbysStoryWorldActivity.this
							.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					for (int i = 0; i < mAvailableCharacters.size(); i++) {
						CustomCharacterData data = mAvailableCharacters.get(i);
						ImageView customItem = (ImageView) li.inflate(
								R.layout.home_character_custom_item, scroll,
								false);
						customItem.setId(R.id.character_images_01 + i);
						customItem
								.setBackgroundResource(mHomeCharacterCustom[data.imageIndex - 1]);
						scroll.addView(customItem);
					}
				}
			}

			updateCharacterFrame();
			changeTakePictureButtonLayer();
			setTakePictureTileBackground(false);

			if (mTakePictureMode != TAKE_PICTURE_CAMERA)
				return;
			break;
		}
	}

	private boolean handleBackPress() {
		switch (mEntryPoint) {
		case ENTRY_PLAY_VIDEO:
			stopPlayVideo();
			break;
		}

		if ((mEntryPoint & FLAG_CAMERA_PREVIEW) != 0) {
			stopCameraPreview(mEntryPoint);
		}

		if (mTakenPicture != null) {
			mTakenPicture.recycle();
			mTakenPicture = null;
		}

		mEntryPoint = ENTRY_MAIN;

		return true;
	}

	private void openCamera() {
		openCamera(-1);
	}

	private void openCamera(int id) {
		mBlockAutoFocus = false;
		if (id < 0)
			mCamera = Camera.open();
		else
			mCamera = Camera.open(id);

		try {
			Camera.Parameters parameters = mCamera.getParameters();
			if (parameters != null) {
				setupPreviewAndPictureSize(parameters);
				mCamera.setParameters(parameters);
			}
			mCamera.setPreviewTexture(mSurface);
			mCamera.startPreview();
			mCamera.autoFocus(this);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

		setupCameraParameters(id);
	}

	private void closeCamera() {
		if (mCamera != null) {
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
		}
		mPendingCameraOpen = false;
	}

	private void setupCameraParameters(int id) {
		if (mCamera != null) {
			int displayOrientation = 0;
			if (id < 0)
				displayOrientation = getCameraDisplayOrientation(Camera.CameraInfo.CAMERA_FACING_BACK);
			else
				displayOrientation = getCameraDisplayOrientation(id);
			mCamera.setDisplayOrientation(displayOrientation);
		}
	}

	private void setupPreviewAndPictureSize(Parameters parameters) {
		mPreviewWidth = Integer.MAX_VALUE;
		mPreviewHeight = Integer.MAX_VALUE;
		boolean match = false;
		ArrayList<Size> tempList = new ArrayList<Size>();
		List<Size> previewList = parameters.getSupportedPreviewSizes();
		for (Size previewSize : previewList) {
			if (previewSize.width == mSurfaceWidth
					&& previewSize.height == mSurfaceHeight) {
				match = true;
				break;
			}
		}
		if (match) {
			mPreviewWidth = mSurfaceWidth;
			mPreviewHeight = mSurfaceHeight;
		} else {
			final float ratio = mSurfaceHeight / (float) mSurfaceWidth;
			for (Size previewSize : previewList) {
				if (previewSize.height / (float) previewSize.width == ratio)
					tempList.add(previewSize);
			}

			for (Size item : tempList) {
				if (item.width >= mSurfaceWidth
						&& item.height >= mSurfaceHeight) {
					if (item.width <= mPreviewWidth
							&& item.height <= mPreviewHeight) {
						mPreviewWidth = item.width;
						mPreviewHeight = item.height;
					}
				}
			}

			if (mPreviewWidth == Integer.MAX_VALUE
					|| mPreviewHeight == Integer.MAX_VALUE) {
				for (Size previewSize : previewList) {
					if (previewSize.width >= mSurfaceWidth
							&& previewSize.height >= mSurfaceHeight) {
						if (previewSize.width <= mPreviewWidth
								&& previewSize.height <= mPreviewHeight) {
							mPreviewWidth = previewSize.width;
							mPreviewHeight = previewSize.height;
						}
					}
				}
			}
		}
		Camera.Size bestSize = null;

		List<Camera.Size> sizeList = mCamera.getParameters()
				.getSupportedPreviewSizes();
		bestSize = sizeList.get(0);

		for (int i = 1; i < sizeList.size(); i++) {
			if ((sizeList.get(i).width * sizeList.get(i).height) > (bestSize.width * bestSize.height)) {
				bestSize = sizeList.get(i);
			}
		}

		parameters.setPreviewSize(bestSize.width, bestSize.height);
		// parameters.setPreviewSize(mPreviewWidth, mPreviewHeight);

		tempList.clear();

		match = false;
		List<Size> pictureList = parameters.getSupportedPictureSizes();
		for (Size pictureSize : pictureList) {
			if (pictureSize.width == mPreviewWidth
					&& pictureSize.height == mPreviewHeight) {
				match = true;
				break;
			}
		}
		mPictureWidth = Integer.MAX_VALUE;
		mPictureHeight = Integer.MAX_VALUE;
		if (match) {
			mPictureWidth = mPreviewWidth;
			mPictureHeight = mPreviewHeight;
		} else {
			final float ratio = mPreviewHeight / (float) mPreviewWidth;
			for (Size pictureSize : pictureList) {
				if (pictureSize.height / (float) pictureSize.width == ratio)
					tempList.add(pictureSize);
			}

			for (Size item : tempList) {
				if (item.width >= mPreviewWidth
						&& item.height >= mPreviewHeight) {
					if (item.width <= mPictureWidth
							&& item.height <= mPictureHeight) {
						mPictureWidth = item.width;
						mPictureHeight = item.height;
					}
				}
			}

			if (mPictureWidth == Integer.MAX_VALUE
					|| mPictureHeight == Integer.MAX_VALUE) {
				for (Size pictureSize : pictureList) {
					if (pictureSize.width >= mPreviewWidth
							&& pictureSize.height >= mPreviewHeight) {
						if (pictureSize.width <= mPictureWidth
								&& pictureSize.height <= mPictureHeight) {
							mPictureWidth = pictureSize.width;
							mPictureHeight = pictureSize.height;
						}
					}
				}
			}
		}

		/*
		 * List<Size> pictureList = parameters.getSupportedPictureSizes();
		 * mPictureWidth = Integer.MAX_VALUE; mPictureHeight =
		 * Integer.MAX_VALUE; final float ratio = mPreviewHeight /
		 * (float)mPreviewWidth; for(Size pictureSize: pictureList) {
		 * if(pictureSize.width <= mSurfaceWidth && pictureSize.height <=
		 * mSurfaceHeight) tempList.add(pictureSize); }
		 * 
		 * if(tempList.size() > 0) { Collections.sort(tempList, new
		 * PictureSizeComparator()); Size determinedSize = tempList.get(0);
		 * mPictureWidth = determinedSize.width; mPictureHeight =
		 * determinedSize.height; }
		 */

		// parameters.setPictureSize(mPictureWidth, mPictureHeight);
	}

	private int getCameraDisplayOrientation(int cameraId) {
		Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
		Camera.getCameraInfo(cameraId, info);
		int rotation = getWindowManager().getDefaultDisplay().getRotation();
		int degrees = 0;
		switch (rotation) {
		case Surface.ROTATION_0:
			degrees = 0;
			break;
		case Surface.ROTATION_90:
			degrees = 90;
			break;
		case Surface.ROTATION_180:
			degrees = 180;
			break;
		case Surface.ROTATION_270:
			degrees = 270;
			break;
		}

		int result;
		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			result = (info.orientation + degrees) % 360;
			result = (360 - result) % 360; // compensate the mirror
		} else { // back-facing
			result = (info.orientation - degrees + 360) % 360;
		}
		return result;
	}

	private void requestQrCodeValidation() {
		// Request qr code validity
		// Map<String, String> request = new HashMap<String, String>();
		// request.put(Constants.TAG_USER_NO,
		// String.valueOf(HeightChartPreference.getInt(Constants.TAG_USER_NO,
		// 0)));
		// request.put(Constants.TAG_SERVICE_CODE, Constants.SERVICE_CODE);
		// request.put(Constants.TAG_SERIAL, mQrStringValueCp);
		// request.put(Constants.TAG_PROMOTION_INFO, "1");
		// request.put(Constants.TAG_CLIENT_IP, Util.getIp(getActivity()));
		//
		// ServerTask task = new RequestCheckQRCode(getActivity(), request);
		// task.setCallback(this);
		// Model.runOnWorkerThread(task);
	}

	private void setTakePictureTileBackground(boolean enable) {
		if (mTakePictureView == null)
			return;

		if (enable) {
			Resources r = getResources();
			ApplicationImpl app = (ApplicationImpl) getApplication();
			ImageCache cache = app.getImageCache();
			Bitmap window = cache
					.getImage(Constants.IMAGE_TAKE_PICTURE_BACKGROUND);
			if (window == null) {
				Bitmap frame = BitmapFactory.decodeResource(getContext()
						.getResources(), R.drawable.camera_frame);
				Paint frameP = new Paint();
				frameP.setShader(new BitmapShader(frame,
						Shader.TileMode.REPEAT, Shader.TileMode.REPEAT));

				window = Bitmap.createBitmap(mDisplayMetrics.widthPixels,
						mDisplayMetrics.heightPixels, Bitmap.Config.ARGB_8888);
				Canvas c = new Canvas(window);
				c.drawPaint(frameP);

				cache.setImage(Constants.IMAGE_TAKE_PICTURE_BACKGROUND, window);
			}

			BitmapDrawable tileDrawable = new BitmapDrawable(r, window);
			mTakePictureView.setBackground(tileDrawable);
		} else {
			mTakePictureView.setBackground(null);
		}
	}

	private void updateFrameBackgroundImage() {
		ApplicationImpl app = (ApplicationImpl) getApplication();
		ImageCache cache = app.getImageCache();
		Bitmap frameBg = cache
				.getImage(Constants.IMAGE_CREATE_CHARACTER_FRAME_BACKGROUND);
		View importShadow = mCreateCharacterView
				.findViewById(R.id.imported_character_shadow);
		if (importShadow != null) {
			Bitmap newBg = null;
			Resources r = getResources();
			if (mAvailableCharacters.size() > 0) {
				newBg = Bitmap.createBitmap(mDisplayMetrics.widthPixels,
						mDisplayMetrics.heightPixels, Bitmap.Config.ARGB_8888);
				Canvas c = new Canvas(newBg);
				Paint p = new Paint();
				p.setAntiAlias(true);
				p.setColor(Color.BLACK);

				int size = mCustomCharacterCropSize[mAvailableCharacters
						.get(mCustomCharacterSelectIndex).imageIndex - 1];
				int frameHeight = r
						.getDimensionPixelSize(R.dimen.create_character_frame_height);
				c.drawCircle(mDisplayMetrics.widthPixels / 2, frameHeight / 2,
						size / 2.f, p);
				c.drawColor(r.getColor(R.color.create_character_bg_color),
						PorterDuff.Mode.SRC_OUT);

				cache.setImage(
						Constants.IMAGE_CREATE_CHARACTER_FRAME_BACKGROUND,
						newBg);
			}

			if (newBg != null) {
				importShadow.setBackground(new BitmapDrawable(getResources(),
						newBg));
				if (frameBg != null)
					frameBg.recycle();
			}
		}
	}

	private void prepareCreateCharacterView() {
		if (mCreateCharacterView == null)
			return;

		ImportImageView importBg = (ImportImageView) mCreateCharacterView
				.findViewById(R.id.imported_character);
		if (importBg != null) {
			updateFrameBackgroundImage();
			importBg.setZoomEnabled(false);
		}

		View view = (View) mCreateCharacterView
				.findViewById(R.id.create_character_scroll_area);
		if (view != null) {
			Drawable bg = getResources().getDrawable(
					R.drawable.character_frame_bg);
			if (bg != null) {
				bg.setAlpha((int) (256 * 0.6f));
				view.setBackground(bg);
			}
		}
	}

	private void changeTakePictureButtonLayer() {
		boolean needGuide = true;
		View takePicture = null;
		View retake = null;
		View importImage = null;

		if (mEntryPoint == ENTRY_TAKE_PICTURE) {
			takePicture = findViewById(R.id.take_picture_buttons);
			retake = findViewById(R.id.take_picture_retake_buttons);
			importImage = findViewById(R.id.take_picture_import_buttons);
		} else if (mEntryPoint == ENTRY_CREATE_CHARACTER) {
			takePicture = findViewById(R.id.create_character_buttons);
			retake = findViewById(R.id.create_character_retake_buttons);
			importImage = findViewById(R.id.create_character_import_buttons);
		}

		switch (mTakePictureMode) {
		case TAKE_PICTURE_CAMERA:
			needGuide = false;
			if (takePicture != null)
				takePicture.setVisibility(View.VISIBLE);
			if (retake != null)
				retake.setVisibility(View.GONE);
			if (importImage != null)
				importImage.setVisibility(View.GONE);
			break;

		case TAKE_PICTURE_SHOW_SNAPSHOT:
			if (takePicture != null)
				takePicture.setVisibility(View.GONE);
			if (retake != null)
				retake.setVisibility(View.VISIBLE);
			if (importImage != null)
				importImage.setVisibility(View.GONE);
			break;

		case TAKE_PICTURE_IMPORT_IMAGE:
			if (takePicture != null)
				takePicture.setVisibility(View.GONE);
			if (retake != null)
				retake.setVisibility(View.GONE);
			if (importImage != null)
				importImage.setVisibility(View.VISIBLE);
			break;
		}

		if (needGuide) {
			GuideMode mode = null;
			if (mTakePictureMode == TAKE_PICTURE_SHOW_SNAPSHOT)
				mode = GuideMode.GUIDE_CREATE_BG;
			else
				mode = GuideMode.GUIDE_IMPORT_BG;

			final String modeName = mode.name();
			boolean isPictureGuideShown = BobbysStoryWorldPreference
					.getBoolean(modeName, false);
			if (!isPictureGuideShown) {
				mTakePictureView.getViewTreeObserver().addOnPreDrawListener(
						new OnPreDrawListener() {
							@Override
							public boolean onPreDraw() {
								final Bundle guideArgs = new Bundle();
								guideArgs.putString(GuideDialog.MODE_GUIDE,
										modeName);
								new GuideDialog(getContext(), guideArgs).show();
								mTakePictureView.getViewTreeObserver()
										.removeOnPreDrawListener(this);
								BobbysStoryWorldPreference.putBoolean(modeName,
										true);
								return true;
							}
						});
			}
		}
	}

	private void updateCharacterFrame() {
		ImageView character = (ImageView) mCreateCharacterView
				.findViewById(R.id.character_frame);
		if (character != null) {
			character
					.setImageResource(mCustomCharacterFrame[mAvailableCharacters
							.get(mCustomCharacterSelectIndex).characterIndex]);
		}

		ViewGroup view = (ViewGroup) mCreateCharacterView
				.findViewById(R.id.create_character_images);
		if (view != null) {
			for (int i = 0; i < view.getChildCount(); i++) {
				ImageView item = (ImageView) view.getChildAt(i);
				if (item != null) {
					boolean isSelected = (i == mCustomCharacterSelectIndex);
					item.setSelected(isSelected);
					if (isSelected)
						item.setImageResource(R.drawable.background_frame_check);
					else
						item.setImageDrawable(null);
				}
			}
		}

		view.getChildAt(mCustomCharacterSelectIndex).setSelected(true);
	}

	private void startVoiceRecord(String filename) {
		if (mOnRecordVoice && mRecorder != null)
			return;

		int index = filename.lastIndexOf(".");
		File f = new File(filename.substring(0, index) + "_"
				+ mRecordingFileIndex + filename.substring(index));
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		mRecorder = new MediaRecorder();
		mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
		mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
		mRecorder.setAudioChannels(2);
		mRecorder.setAudioSamplingRate(44100);
		mRecorder.setAudioEncodingBitRate(96000);
		mRecorder.setOutputFile(f.toString());

		try {
			mRecorder.prepare();
			if (mOnRecordVoice) {
				mRecorder.start();
			} else {
				if (mRecorder != null) {
					mRecorder.release();
					mRecorder = null;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void stopVoiceRecord() {
		if (mRecorder != null) {
			try {
				mRecorder.stop();
			} catch (RuntimeException re) {
				int index = mRecordingFileName.lastIndexOf(".");
				File f = new File(mRecordingFileName.substring(0, index) + "_"
						+ mRecordingFileIndex
						+ mRecordingFileName.substring(index));
				if (f.exists()) {
					f.delete();
				}
				mRecordingFileIndex--;
			}
			mRecorder.release();
			mRecorder = null;
		}
	}

	private void prepareVoicePlay(String filename) {
		String fullName = getCacheDir() + File.separator + RECORD_PATH
				+ File.separator + filename;

		// if(mSoundPoolMap.containsKey(fullName)) {
		// Integer id = mSoundPoolMap.get(fullName);
		// int streamId = mSoundPool.play(id.intValue(), 1.f, 1.f, 0, 0, 1.f);
		// mSoundPoolStreamIdMap.put(fullName, Integer.valueOf(streamId));
		// } else {
		// final int id = mSoundPool.load(fullName, 1);
		// mSoundPoolMap.put(fullName, Integer.valueOf(id));
		// mSoundPoolWaitPlay.put(id, fullName);
		// }

		stopVoicePlay(null);

		try {
			mVoicePlayer = new MediaPlayer();
			mVoicePlayer
					.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
						public void onPrepared(MediaPlayer mp) {
							if (mVoicePlayer != null)
								mVoicePlayer.start();
						}
					});
			mVoicePlayer
					.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
						public void onCompletion(MediaPlayer mp) {
							stopVoicePlay(null);
						}
					});
			mVoicePlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mVoicePlayer.setDataSource(fullName);
			mVoicePlayer.prepareAsync();
		} catch (IOException e) {
			stopVoicePlay(null);
		}
	}

	public void resumeVoicePlay(String filename) {
		// String fullName = getCacheDir() + File.separator + RECORD_PATH +
		// File.separator + filename;
		// if(mSoundPoolStreamIdMap.containsKey(fullName)) {
		// Integer id = mSoundPoolStreamIdMap.get(fullName);
		// mSoundPool.resume(id.intValue());
		// }

		if (mVoicePlayer != null) {
			mVoicePlayer.start();
		}
	}

	public void pauseVoicePlay(String filename) {
		// String fullName = getCacheDir() + File.separator + RECORD_PATH +
		// File.separator + filename;
		// if(mSoundPoolStreamIdMap.containsKey(fullName)) {
		// Integer id = mSoundPoolStreamIdMap.get(fullName);
		// mSoundPool.pause(id.intValue());
		// }

		if (mVoicePlayer != null) {
			mVoicePlayer.pause();
		}
	}

	private void stopVoicePlay(String filename) {
		// String fullName = getCacheDir() + File.separator + RECORD_PATH +
		// File.separator + filename;
		// if(mSoundPoolStreamIdMap.containsKey(fullName)) {
		// Integer id = mSoundPoolStreamIdMap.remove(fullName);
		// mSoundPool.stop(id.intValue());
		// }

		if (mVoicePlayer != null) {
			mVoicePlayer.stop();
			mVoicePlayer.release();
			mVoicePlayer = null;
		}
	}

	private void prepareBGMPlay(String filename) {
		try {
			AssetFileDescriptor fd = getAssets().openFd("common/" + filename);
			mBGMPlayer = new MediaPlayer();
			mBGMPlayer.setOnPreparedListener(mPreparedListener);
			mBGMPlayer.setOnCompletionListener(mCompletionListener);
			mBGMPlayer.setOnErrorListener(mErrorListener);
			mBGMPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mBGMPlayer.setDataSource(fd.getFileDescriptor(),
					fd.getStartOffset(), fd.getLength());
			mBGMPlayer.prepareAsync();

			mCurrentState = STATE_PREPARING;
			mTargetState = STATE_PLAYING;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void startBGMPlay() {
		if (isBGMInPlaybackState()) {
			mBGMPlayer.start();
			mCurrentState = STATE_PLAYING;
		}
		mTargetState = STATE_PLAYING;
	}

	public void pauseBGMPlay() {
		if (isBGMInPlaybackState()) {
			if (mBGMPlayer.isPlaying()) {
				mBGMPlayer.pause();
				mCurrentState = STATE_PAUSED;
			}
		}
		mTargetState = STATE_PAUSED;
	}

	private void stopBGMPlay() {
		if (mBGMPlayer != null) {
			mBGMPlayer.stop();
			mBGMPlayer.release();
			mBGMPlayer = null;
			mCurrentState = STATE_IDLE;
			mTargetState = STATE_IDLE;
		}
	}

	private boolean isBGMInPlaybackState() {
		return (mBGMPlayer != null && mCurrentState != STATE_ERROR
				&& mCurrentState != STATE_IDLE && mCurrentState != STATE_PREPARING);
	}

	/**
	 * Check the device to make sure it has the Google Play Services APK. If it
	 * doesn't, display a dialog that allows users to download the APK from the
	 * Google Play Store or enable it in the device's system settings.
	 */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				android.util.Log.e("", "This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}

	/**
	 * Gets the current registration ID for application on GCM service.
	 * <p>
	 * If result is empty, the app needs to register.
	 * 
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	private String getRegistrationId() {
		String registrationId = BobbysStoryWorldPreference.getString(
				PROPERTY_REG_ID, "");
		if (registrationId.isEmpty()) {
			android.util.Log.e("", "Registration not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = BobbysStoryWorldPreference.getInt(
				PROPERTY_APP_VERSION, Integer.MIN_VALUE);
		int currentVersion = getAppVersion();
		if (registeredVersion != currentVersion) {
			android.util.Log.e("", "App version changed.");
			return "";
		}
		return registrationId;
	}

	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private int getAppVersion() {
		try {
			PackageInfo packageInfo = getPackageManager().getPackageInfo(
					getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p>
	 * Stores the registration ID and app versionCode in the application's
	 * shared preferences.
	 */
	private void registerInBackground() {
		new AsyncTask<Void, Object, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				try {
					if (mGcm == null) {
						mGcm = GoogleCloudMessaging
								.getInstance(BobbysStoryWorldActivity.this);
					}
					regid = mGcm.register(SENDER_ID);
					msg = "Device registered, registration ID=" + regid;

					// You should send the registration ID to your server over
					// HTTP,
					// so it can use GCM/HTTP or CCS to send messages to your
					// app.
					// The request to your server should be authenticated if
					// your app
					// is using accounts.
					sendRegistrationIdToBackend();

					// For this demo: we don't need to send it because the
					// device
					// will send upstream messages to a server that echo back
					// the
					// message using the 'from' address in the message.

					// Persist the regID - no need to register again.
					storeRegistrationId(regid);
				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
					// If there is an error, don't just keep trying to register.
					// Require the user to click a button again, or perform
					// exponential back-off.
				}
				return msg;
			}
		}.execute(null, null, null);
	}

	/**
	 * Sends the registration ID to your server over HTTP, so it can use
	 * GCM/HTTP or CCS to send messages to your app. Not needed for this demo
	 * since the device sends upstream messages to a server that echoes back the
	 * message using the 'from' address in the message.
	 */
	private void sendRegistrationIdToBackend() {
		// Your implementation here.
	}

	/**
	 * Stores the registration ID and app versionCode in the application's
	 * {@code SharedPreferences}.
	 * 
	 * @param context
	 *            application's context.
	 * @param regId
	 *            registration ID
	 */
	private void storeRegistrationId(String regId) {
		int appVersion = getAppVersion();
		android.util.Log.e("", "Saving regId on app version " + appVersion);
		BobbysStoryWorldPreference.putString(PROPERTY_REG_ID, regId);
		BobbysStoryWorldPreference.putInt(PROPERTY_APP_VERSION, appVersion);
	}

	public static String getRecordingFilename() {
		return Cocos2dxScreenRecorder.getInstance().getVideoName();
	}

	public void saveVideoToDevice(String sourceFile, String title, String author) {
		File destPath = Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);

		// TODO Generate file name
		File outFile = new File(destPath, new String(title + "_" + author
				+ ".mp4"));

		FileInputStream in = null;
		FileOutputStream out = null;
		try {
			if (!destPath.exists()) {
				destPath.mkdirs();
			}

			in = new FileInputStream(sourceFile);
			out = new FileOutputStream(outFile);

			byte[] buffer = new byte[1024];
			int read;
			while ((read = in.read(buffer)) != -1) {
				out.write(buffer, 0, read);
			}
			in.close();
			in = null;

			out.flush();
			out.close();
			out = null;

			new File(sourceFile).delete();

			MediaScannerConnection.scanFile(this,
					new String[] { outFile.toString() }, null,
					new MediaScannerConnection.OnScanCompletedListener() {
						public void onScanCompleted(String path, Uri uri) {
							android.util.Log.i("hsw_dbg", "Video Scanned "
									+ path + ":");
							android.util.Log.i("hsw_dbg", "-> uri=" + uri);
							
							MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext() , Constants.MIXPANEL_TOKEN);
					        mixpanel.track("Downloaded Story to Device", null);
						}
					});

		} catch (IOException e) {
			android.util.Log.w("hsw_dbg", "Error writing " + outFile, e);
		}
	}

	public void onRecordSceneChange() {
		changeScene();
	}

	public void showPromotionScreen(final String url) {
		runOnUiThread(new Runnable() {
			public void run() {
				nativeHide();

				if (mPromotionViewContainer != null)
					return;

				LayoutInflater li = (LayoutInflater) BobbysStoryWorldActivity.this
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				mPromotionViewContainer = (RelativeLayout) li.inflate(
						R.layout.promotion_screen, null);

				ViewGroup rootView = (ViewGroup) findViewById(android.R.id.content);

				mPromotionView = (WebView) mPromotionViewContainer
						.findViewById(R.id.promotion_web);

				FontButton close = (FontButton) mPromotionViewContainer
						.findViewById(R.id.promotion_close);
				FontTextHelper.setFont(BobbysStoryWorldActivity.this, close,
						FontTextHelper.sEnglishBoldFontPath);

				mPromotionView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
				mPromotionView.getSettings().setJavaScriptEnabled(true);
				mPromotionView.setBackgroundColor(Color.WHITE); // TODO Not
																// working
				mPromotionView.reload();

				mPromotionViewContainer.findViewById(R.id.promotion_close)
						.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								hidePromotionScreen();
							}
						});

				rootView.addView(mPromotionViewContainer);

				mPromotionView.setBackgroundColor(0);
				mPromotionView.getSettings().setCacheMode(
						WebSettings.LOAD_NO_CACHE);
				mPromotionView.getSettings().setAppCacheEnabled(false);
				mPromotionView.setWebViewClient(new RedirectionWebViewClient());

				mPromotionView.loadUrl(url);
			}
		});
	}

	public void hidePromotionScreen() {
		runOnUiThread(new Runnable() {
			public void run() {
				nativeShow();

				if (mPromotionViewContainer == null)
					return;

				if (((CheckBox) mPromotionViewContainer
						.findViewById(R.id.promotion_checkbox)).isChecked()) {
					Calendar calendar = Calendar.getInstance(TimeZone
							.getTimeZone("gmt"));
					final long offset = TimeZone.getDefault().getOffset(
							calendar.getTimeInMillis());
					calendar.add(Calendar.DAY_OF_YEAR, 1);
					calendar.set(Calendar.HOUR_OF_DAY, 0);
					calendar.set(Calendar.MINUTE, 0);
					calendar.set(Calendar.SECOND, 0);
					calendar.set(Calendar.MILLISECOND, 0);
					final long nextShow = calendar.getTimeInMillis() - offset;
					BobbysStoryWorldPreference.putLong(
							Constants.PREF_PROMOTION_DATE, nextShow);
				}

				ViewGroup rootView = (ViewGroup) findViewById(android.R.id.content);
				rootView.removeView(mPromotionViewContainer);
				mPromotionView.destroy();
				mPromotionViewContainer = null;
				mPromotionView = null;
			}
		});
	}

	private class RedirectionWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			BobbysStoryWorldActivity.this.startActivity(i);
			return true;
		}
	}

	// JNI methods
	private native void initJNIBridge();

	private native void nativeBackPress();

	private native void nativeFinishEditAction(String title, String author);

	private native void nativeFinishShareCommentEditAction(int shareType,
			String comment);

	private native void nativeCancelEditAction();

	private native void nativeCustomCharacaterCallback(int index, String path);

	private native void nativeCustomBackgroundCallback(String path);

	private native void retrieveAuthKeyFromRedirection(long jniHandle,
			String redirectUrlWithCode);

	private native void startClientNextAction(long jsniHandle);

	private native void nativeOnFinishVoiceRecording(String voice, String bgm,
			String outfile);

	private native void nativeFacebookActionResult(int result, int type);

	private native void nativeOnFinishVideoListPlay();

	private native void nativeOnStartVideoListPlay();

	private native void nativeOnStopVideoListPlay();

	private native void nativeOnFacebookShareComplete();

	private native void nativeOnProgressUpdate(long progress);

	private native void nativeCancelYoutubeShare();

	private native void nativeSaveAuthData(String auth, int userNo);

	private native void nativeChangeBGMPlayStatus(int status);

	private native void nativePlayBGM();

	private native void nativeStopBGM();

	private final class TakePictureCallback implements PictureCallback {
		@Override
		public void onPictureTaken(final byte[] data, final Camera camera) {
			Model.runOnWorkerThread(new Runnable() {
				@Override
				public void run() {
					if (data != null && data.length > 0) {
						BitmapFactory.Options options = new BitmapFactory.Options();
						options.inDither = false;
						options.inPreferredConfig = Bitmap.Config.ARGB_8888;
						Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0,
								data.length, options);

						if (bitmap == null)
							return;

						if (mCameraFacing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
							Matrix m = new Matrix();
							m.preScale(-1, 1);
							bitmap = Bitmap.createBitmap(bitmap, 0, 0,
									bitmap.getWidth(), bitmap.getHeight(), m,
									false);
						}
						// FOM 20150217
						// TODO TakePictureCallback
						if (android.os.Build.MODEL.toUpperCase().contains(
								"SM-N910")) {
							if ((String.valueOf(mPreviewHeight).length() > 4 || String
									.valueOf(mPreviewWidth).length() > 4)
									|| (bitmap.getWidth() <= 0 || bitmap
											.getHeight() <= 0)) {
								List<Camera.Size> sizeList = mCamera
										.getParameters()
										.getSupportedPreviewSizes();
								Camera.Size bestSize = sizeList.get(0);

								for (int i = 1; i < sizeList.size(); i++) {
									if ((sizeList.get(i).width * sizeList
											.get(i).height) > (bestSize.width * bestSize.height)) {
										bestSize = sizeList.get(i);
									}
								}
								mPreviewWidth = bestSize.width;
								mPreviewHeight = bestSize.height;
								// return;
							}
						}
						bitmap = Bitmap.createScaledBitmap(bitmap,
								mPreviewWidth, mPreviewHeight, false);

						if (mTakenPicture != null) {
							mTakenPicture.recycle();
							mTakenPicture = null;
						}

						final int bitmapWidth = bitmap.getWidth();
						final int bitmapHeight = bitmap.getHeight();
						if (bitmapWidth > mMaxTextureSize) {
							bitmap = Bitmap
									.createScaledBitmap(
											bitmap,
											mMaxTextureSize,
											(int) (bitmapHeight
													* mMaxTextureSize / (float) bitmapWidth),
											false);
						} else if (bitmapHeight > mMaxTextureSize) {
							bitmap = Bitmap
									.createScaledBitmap(
											bitmap,
											(int) (bitmapWidth
													* mMaxTextureSize / (float) bitmapHeight),
											mMaxTextureSize, false);
						}
						mTakenPicture = bitmap;

						if (mTakenPicture != null) {
							Runnable r = null;
							if (mEntryPoint == ENTRY_TAKE_PICTURE) {
								// if(mImportedImageView != null) {
								r = new Runnable() {
									@Override
									public void run() {
										mTextureView.setVisibility(View.GONE);
										closeCamera();

										mTakePictureMode = TAKE_PICTURE_SHOW_SNAPSHOT;
										mImportedImageView
												.setVisibility(View.VISIBLE);
										mImportedImageView
												.setImageBitmap(mTakenPicture);
										mImportedImageView.setZoomEnabled(true);
										// mTakePictureView.setBackground(
										// new
										// BitmapDrawable(BobbysStoryWorldActivity.this.getResources(),
										// mTakenPicture));
										changeTakePictureButtonLayer();
									}
								};
								// }
							} else if (mEntryPoint == ENTRY_CREATE_CHARACTER) {
								final ImportImageView image = (ImportImageView) mCreateCharacterView
										.findViewById(R.id.imported_character);
								if (image != null) {
									r = new Runnable() {
										@Override
										public void run() {
											mTextureView
													.setVisibility(View.GONE);
											closeCamera();

											mTakePictureMode = TAKE_PICTURE_SHOW_SNAPSHOT;
											image.setZoomEnabled(true);
											image.setImageBitmap(mTakenPicture);
											image.setBackgroundResource(R.drawable.camera_import_bg);
											changeTakePictureButtonLayer();
										}
									};
								}
							}

							if (r != null)
								BobbysStoryWorldActivity.this.runOnUiThread(r);
						}
					}
				}
			});
		}
	}

	public void displayWebView(final long jniHandle, final int shareType,
			final String apiUrl, final String redirectUrl, final int x,
			final int y, final int width, final int height) {
		mJniHandle = jniHandle;
		runOnUiThread(new Runnable() {
			public void run() {
				nativeHide();
				ViewGroup rootView = (ViewGroup) findViewById(android.R.id.content);
				mWebView = new WebView(BobbysStoryWorldActivity.this);
				mWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
				mWebView.getSettings().setJavaScriptEnabled(true);
				mWebView.setBackgroundColor(Color.WHITE); // TODO Not working
				mWebView.reload();
				rootView.addView(mWebView);

				FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mWebView
						.getLayoutParams();
				layoutParams.leftMargin = x;
				layoutParams.topMargin = y;
				layoutParams.width = width;
				layoutParams.height = height;
				mWebView.setLayoutParams(layoutParams);

				mWebView.setBackgroundColor(0);
				mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
				mWebView.getSettings().setAppCacheEnabled(false);
				mWebView.setWebViewClient(new AuthRedirectionWebViewClient(
						shareType, redirectUrl));

				mWebView.loadUrl(apiUrl);
			}
		});
	}

	class AuthRedirectionWebViewClient extends WebViewClient {
		private int mShareType;
		private String mRedirectUrl;

		AuthRedirectionWebViewClient(int shareType, String redirectUrl) {
			mShareType = shareType;
			mRedirectUrl = redirectUrl;
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			if (url != null && url.startsWith(mRedirectUrl)) {
				if (url.startsWith(YOUTUBE_REDIRECT_URL)) {
					retrieveAuthKeyFromRedirection(mJniHandle, url);
				} else if (url.startsWith(YOUTUBE_CREATE_CHANNEL_REDIRECT_URL)) {
					startClientNextAction(mJniHandle);
				}
				removeWebView();
				return;
			}
		}
	}

	public void removeWebView() {
		runOnUiThread(new Runnable() {
			public void run() {
				nativeShow();
				if (mWebView != null) {
					ViewGroup rootView = (ViewGroup) findViewById(android.R.id.content);
					rootView.removeView(mWebView);
					mWebView.destroy();
					mWebView = null;
					mYoutubeShareCanceled = false;
				}
			}
		});
	}

	@Override
	public void onAutoFocus(boolean success, Camera camera) {
		if (!success) {
			if (mCamera != null && !mBlockAutoFocus)
				camera.autoFocus(this);
			return;
		}
	}

	public void updateAuthKey(String authKey) {
		BobbysStoryWorldPreference.putString(Constants.PREF_AUTH_KEY, authKey);
	}
}