package com.dole.bobbysstoryworld;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.cocos2dx.lib.Cocos2dxUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.Area;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.TextureView;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.widget.TextView;
import android.widget.Toast;

import com.dole.bobbysstoryworld.server.GetBalance;
import com.dole.bobbysstoryworld.server.RequestCheckQRCode;
import com.dole.bobbysstoryworld.server.RequestNewAuthKey;
import com.dole.bobbysstoryworld.server.RequestUseQRCode;
import com.dole.bobbysstoryworld.server.ServerTask;
import com.dole.bobbysstoryworld.server.ServerTask.IServerResponseCallback;
import com.dole.bobbysstoryworld.ui.CustomDialog;
import com.dole.bobbysstoryworld.ui.CustomDialog.CustomDialogButtonClickListener;
import com.dole.bobbysstoryworld.ui.FontEditText;
import com.dole.bobbysstoryworld.ui.QRCodeDetectedBoundView;
import com.dole.bobbysstoryworld.ui.QRCodeDetectedBoundView.OnRectAnimationCallback;
import com.dole.bobbysstoryworld.ui.SimpleAnimator;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

public class QRCodeManager implements IServerResponseCallback, OnRectAnimationCallback, AutoFocusCallback, CustomDialogButtonClickListener{

	private static final String TAG = "QRCodeManager";
	private static final String QR_TAG_T = "T";
	private static final String QR_TAG_CP = "CP";
	private static final int QRCODE_LENGTH = 16;
	private static final String PREFIX_SCHEME = "http";
	
    private final int RESUME_QRCODE_FOCUS_CALLBACK = 3000;
	
	private HandlerThread mWorkerThread;
    private Handler mWorker;
	
	//private QRCodeReader mCodeReader;
	private Camera mCamera;
	private PreviewCallback mPreviewCallback;
	
	private String mQrcodeValueT = null;
	private String mQrStringValueCp = null;
	private int mQrProductValue = 500;
	private boolean mIsUsedCodeAndWithin24Hour = false;
	
	private int mTmpInputTextCount = 0;
	private boolean mIsScanning = false;
	private long mDeleteHeightIdForCodeReuse = Constants.UNAVAILABLE_ID;

	private ProgressDialog mProgressDialog;
	private RequestCheckQRCode mCheckQRCodeTask;
	
	private Bitmap mDebugImage;
	
	private PortraitActivity mActivity;
	private View mRootView;
	
	/** QRCode */
	private Matrix mPreviewMatrix;
	private FutureTask<String> mCurrentTask;
//	private HandlerThread mQRCodeWorkerThread;
//    private Handler mQRCodeWorker;
    private boolean mIsQRCodePreviewing = false;
    
    private int mPreviewWidth = 0;
    private int mPreviewHeight = 0;
    //RectF previewRect = new RectF();
	
	private Matrix mConvertMatrix = null;
    private float mAspectRatio = 16f / 9f;
    private float mSurfaceTextureUncroppedWidth;
    private float mSurfaceTextureUncroppedHeight;

    private TextureView mTextureView;
    private View mFocusView;
    private RectF mFocusRect;
    
    private SurfaceTexture mSurfaceTexture;
    private int mDisplayOrientation;

	private boolean mIsMeteringAreasSupported = false;
	
	private boolean mIsPaused = false;
	private boolean mIsKeyboardInputQR = false;
	
	private int iValidateFromApi = -1; // FOM 20150301
	
    // FOM (s) 20150214 for zbar
    ImageScanner				scanner;
    static {
        System.loadLibrary( "iconv" );
    }
    // FOM (e) 20150214 for zbar
        
    public QRCodeManager(PortraitActivity activity, View view, TextureView textureView) {
		mActivity = activity;
		mRootView = view;
		mTextureView = textureView;

		//mCodeReader = new QRCodeReader();
		
		final FontEditText directInputCode = (FontEditText) view.findViewById(R.id.qrcode_direct_input);
		final TextView directInputCodeError = (TextView) view.findViewById(R.id.qrcode_direct_input_error);
		
		directInputCode.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				mTmpInputTextCount = s.length();
				if(mTmpInputTextCount  == QRCODE_LENGTH) {
					mQrStringValueCp = s.toString();
					mIsKeyboardInputQR = true;
					
					requestQrCodeValidation();
				}
			}
		});
		
		directInputCode.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				directInputCodeError.setVisibility(View.GONE);
				return false;
			}
		});
		
		directInputCode.setOnImeHideListener(new FontEditText.ImeHideListener() {
			@Override
			public void onImeHide() {
				if(mTmpInputTextCount != QRCODE_LENGTH) {
					directInputCodeError.setText(mActivity.getResources().getString(R.string.pf_net_error_qrcode_input, QRCODE_LENGTH));
					directInputCodeError.setVisibility(View.VISIBLE);
				}
			}
		});
		
		directInputCode.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				switch (keyCode) {
				case KeyEvent.KEYCODE_ENTER:
				case KeyEvent.KEYCODE_BACK:

					if(mTmpInputTextCount != QRCODE_LENGTH) {
						directInputCodeError.setText(mActivity.getResources().getString(R.string.pf_net_error_qrcode_input, QRCODE_LENGTH));
						directInputCodeError.setVisibility(View.VISIBLE);
					}
				default:
					break;
				}

				return false;
			}
		});

		final View focusView = view.findViewById(R.id.qrcode_center_focus);
		mPreviewCallback = new QRCodePreviewCallback();
		
		final QRCodeDetectedBoundView qrCodeBound = (QRCodeDetectedBoundView) view.findViewById(R.id.qrcode_code_bound);
		qrCodeBound.setOnRectAnimationCallback(this);
        
        // FOM 20150214
        /************************************/
        /* Instance barcode scanner */
        scanner = new ImageScanner();
        scanner.setConfig( 0, Config.X_DENSITY, 3 );
        scanner.setConfig( 0, Config.Y_DENSITY, 3 );
	}
	
	public void setCamera(Camera camera) {
		mCamera = camera;

		Parameters param = camera.getParameters();
		param.setExposureCompensation(0);
		mDisplayOrientation = Util.getCameraDisplayOrientation((Activity) mActivity, Camera.CameraInfo.CAMERA_FACING_BACK, mCamera);
		camera.setDisplayOrientation(mDisplayOrientation);
		param.setAntibanding(Parameters.ANTIBANDING_OFF);

		//Set param to camera
		camera.setParameters(param);
	}
	
	@Override
	public void onAutoFocus(boolean success, Camera camera) {
		if(mFocusView != null)
			mFocusView.setSelected(success);
		
		if(!success) {
			camera.autoFocus(this);
			if(mFocusView != null)
				mFocusView.setSelected(false);
			return;
		}
		
		//TODO mSensorManager.registerListener(BobbysStoryWorldActivity.this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
		if(mPreviewCallback != null) {
			startOneShotPreviewCallbackLoop();
		}
		
		resumeQrcodeAutoFocus();
	}
	
	public void initFocus(View focusView) {
		mFocusView = focusView;

		if(null != mCamera) {
			mCamera.cancelAutoFocus();

			String focusMode = null;
			final Parameters parameters = mCamera.getParameters();
			final List<String> focusModes = parameters.getSupportedFocusModes();

			if(focusModes.contains(Parameters.FOCUS_MODE_MACRO))
				focusMode = Parameters.FOCUS_MODE_MACRO;
			else if(focusModes.contains(Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
				focusMode = Parameters.FOCUS_MODE_CONTINUOUS_PICTURE;
			} else {
				focusMode = Parameters.FOCUS_MODE_AUTO;
			}
			
			int[] location = new int[2]; 
			mFocusView.getLocationInWindow(location);

			Rect previewRect = new Rect(0, 0, mPreviewWidth, mPreviewHeight);
			Matrix matrix = new Matrix();
			Util.prepareMatrix(matrix, mDisplayOrientation, previewRect);
			RectF rect = new RectF(
					(int)location[0], (int)location[1], 
					(int)(location[0] + mFocusView.getWidth()), (int)(location[1] + mFocusView.getHeight()));
			
			mConvertMatrix = new Matrix();
			mConvertMatrix.preRotate(-90, 0, 0);
			mConvertMatrix.postConcat(mPreviewMatrix);
			mConvertMatrix.postTranslate(0, mPreviewWidth);

			final QRCodeDetectedBoundView qrCodeBound = (QRCodeDetectedBoundView) mRootView.findViewById(R.id.qrcode_code_bound);
			qrCodeBound.setPreviewConvertMatrix(mConvertMatrix);
			
			mFocusRect = new RectF(rect); 
			mConvertMatrix.mapRect(mFocusRect);

			matrix.mapRect(rect);
			
			final List<Area> areas = new ArrayList<Area>();
			areas.add(new Camera.Area(new Rect((int)rect.left, (int)rect.top, (int)rect.right, (int)rect.bottom), 100));
			parameters.setFocusAreas(areas);
			//parameters.setFocusMode(focusMode);

			if (mIsMeteringAreasSupported) {
				parameters.setMeteringAreas(areas);
			}

			mCamera.setParameters(parameters);
			mCamera.autoFocus(this);
		}
	}

	public void onResume() {
		mIsPaused = false;
		if(mWorkerThread == null)
			mWorkerThread = new HandlerThread("qrcode-scan");

		mWorkerThread.start();
		if(mWorker == null)
			mWorker = new Handler(mWorkerThread.getLooper());
		
		if(mRootView != null) {
			resumeQrcodeAutoFocus();
			startOneShotPreviewCallbackLoop();
		}
	}

	public void onPause() {
		mIsPaused = true;
		mIsScanning = false;
		
		mWorkerThread.quit();
		mWorkerThread = null;
		mWorker = null;
		
		if(mRootView != null) {
			cancelQRCodeScan();
			mRootView.getHandler().removeCallbacks(mQRCodefocusRunnable);
		}
	}

	private final class QRCodePreviewCallback implements PreviewCallback {
		@Override
		public void onPreviewFrame(final byte[] data, final Camera camera) {
			if(!mIsScanning && !mIsPaused) {
				mIsScanning = true;
				scanQRCode(data);
			}
		}
    }
	
	private class QRCodeScanTask implements Callable<String> {
		private byte[] mData;
		
		public QRCodeScanTask(byte[] data) {
			mData = data;
		}
		
		@Override
		public String call() throws Exception {
			
			if(mCamera == null) {
				mIsScanning = false;
				return null;
			}
			
			final Parameters parameters = mCamera.getParameters();
	    	final Camera.Size previewSize = parameters.getPreviewSize();
	    	
	    	int width = previewSize.width;
	    	int height = previewSize.height;
	    	// FOM (s) 20150214 put comment
	    	final RectF focusRectInPreview = mFocusRect;
//	    	PlanarYUVLuminanceSource source = null;
//	    	try {
//		    	source = 
//		    			new PlanarYUVLuminanceSource(mData,
//		    					width, height,
//		    					(int) focusRectInPreview.left, (int) focusRectInPreview.top, 
//		    					(int) focusRectInPreview.width(), (int) focusRectInPreview.height(), 
//		    					false);
//		    } catch (Exception e) {
//				android.util.Log.d(TAG, " decode error!", e);
//		    }
//
//	    	Result result = null;
//	    	if (source != null) {
//	    		BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
//	    		try {
//	    			result = mCodeReader.decode(bitmap);
//	    		} catch (ReaderException re) {
////	    			android.util.Log.e(TAG, "QRCode not decoded", re);
//	    		} finally {
//	    			mCodeReader.reset();
//	    		}
//	    	}
//	    	
//	    	if(result == null) {
//	    		mIsScanning = false;  		
//	    		return null;
//	    	}
//	    	
//	    	final ResultPoint[] points = result.getEnclosingPoints();
            // FOM (e) 20150214 put comment
            
            // FOM (s) 20140214 decoding of qr
            Image barcode = new Image( width, height, "Y800" );
            barcode.setData( mData );
            String resultStr = ""; // FOM 20150214 move here
            int resultImage = scanner.scanImage( barcode );
            
            if ( resultImage != 0 ) {
                SymbolSet syms = scanner.getResults();
                for ( Symbol sym : syms ) {
                    resultStr = sym.getData();
                }
            }
            // FOM (e) 20140214
           
	    	List<NameValuePair> params = null;
			try {
				//String resultStr =  result.toString(); // FOM 20150214 change position
				if(resultStr == null) {
					mIsScanning = false;				
					return null;
				}
				
				int urlPos = resultStr.toString().indexOf(PREFIX_SCHEME);
				if(urlPos < 0) {
					mIsScanning = false;
					return null;
				}
				
				params = URLEncodedUtils.parse(new URI(resultStr.substring(urlPos)), "UTF-8");
				for (NameValuePair param : params) {
					final String key = param.getName();
					final String value = param.getValue();
					if(QR_TAG_T.equals(key)) {
						mQrcodeValueT = value;
					} else if(QR_TAG_CP.equals(key)) {
						mQrStringValueCp = value;
					}
				}
				android.util.Log.d(TAG, " decoded code = " + mQrStringValueCp);
				
				//successfully decoded
				if(mQrStringValueCp != null && mQrcodeValueT !=null){
					final View rootView = mRootView;
			    	mActivity.runOnUiThread(new Runnable() {
						public void run() {
		                    if (rootView == null) return; // FOM 20150214
		                    
							final View focusView = mRootView.findViewById(R.id.qrcode_center_focus);
							final QRCodeDetectedBoundView qrCodeBound = (QRCodeDetectedBoundView) rootView.findViewById(R.id.qrcode_code_bound);
							qrCodeBound.setResultPoint(focusView, focusRectInPreview,/*, points,*/ previewSize);
							qrCodeBound.setCleared(false);
					    	qrCodeBound.invalidate();
						}
					});
				}
				
				return mQrStringValueCp;
			} catch (URISyntaxException e) {
				android.util.Log.d(TAG, "URISyntax error");
			} finally {
				mIsScanning = false;		
			}

			return null;
		}
	}
	
	private void startOneShotPreviewCallbackLoop() {
		if(mCamera != null)
			mCamera.setOneShotPreviewCallback(mPreviewCallback);
	}
	
	private boolean scanQRCode(byte[] data) {
		if(mCamera == null) 
			return false;
		
		QRCodeScanTask task = new QRCodeScanTask(data);
		mCurrentTask = new FutureTask<String>(task);
		mWorker.post(mCurrentTask);
		
		Model.runOnWorkerThread(new Runnable() {
			@Override
			public void run() {
				try {
					mCurrentTask.get();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				} finally {
					if(mActivity.isPreviewing()) {
						startOneShotPreviewCallbackLoop();
					}
				}
			}
		});
		
		return (mQrStringValueCp != null);
	}
	
	private void cancelQRCodeScan() {
		if(mWorker != null)
			mWorker.removeCallbacks(mCurrentTask);
	}
	
	private void setPendingFocusCallback() {
		
		mRootView.postDelayed(new Runnable() {

			@Override
			public void run() {
				pausePreview(false);
			}
		}, 3000);
	}
	
	@Override
	public void onSuccess(ServerTask parser, Map<String, String> result) {
		if(parser instanceof RequestNewAuthKey) {
			BobbysStoryWorldPreference.putString(Constants.PREF_AUTH_KEY, result.get(Constants.TAG_AUTHKEY));
			Util.requestUseQrCode(mActivity, this, mQrStringValueCp);
		} else if(parser instanceof RequestCheckQRCode) {
			boolean canUse = Boolean.valueOf(result.get(Constants.TAG_CAN_USE));
			if(canUse) {
				android.util.Log.d(TAG, " result = " + result);
				JSONObject jsonObj = new JSONObject();
				try {
					JSONArray array = new JSONArray(result.get("ProductInfos"));
					if (array.length()==0) {
						mQrProductValue = 500;
					}else{
						jsonObj = array.getJSONObject(0);
						if(jsonObj.isNull("Value")||jsonObj.length()<1)
							mQrProductValue = 500;
						else
							mQrProductValue = Integer.valueOf(jsonObj.getString("Value"));
					}					
					android.util.Log.d(TAG, " value = " + mQrProductValue);
					Util.requestUseQrCode(mActivity, this, mQrStringValueCp);
					iValidateFromApi = 1;
				} catch (JSONException e) {
					e.printStackTrace();
					if(mProgressDialog != null) {
						mProgressDialog.dismiss();
						mProgressDialog = null;
					}
					iValidateFromApi = 0;
					showErrorPopup();
				} catch (NumberFormatException e) {
					mQrProductValue = 500;
				}
			}else{
				iValidateFromApi = 0;
			}

			if(mCheckQRCodeTask != null)
				mCheckQRCodeTask = null;
			
		} else if(parser instanceof RequestUseQRCode) {
			android.util.Log.d(TAG, " use qr code result = " + result);
			
			if(result.get("Return").equals("true")) {
				
				MixpanelAPI mixpanel = MixpanelAPI.getInstance(mActivity.getApplicationContext(), Constants.MIXPANEL_TOKEN);
				if (mIsKeyboardInputQR == true)
					mixpanel.track("Inputted Alphanumeric Code", null);
				else
					mixpanel.track("Scanned QR Code", null);
				mixpanel.flush();
				
				HashMap<String, String> request = new HashMap<String, String>();
				request.put(Constants.TAG_USER_NO, String.valueOf(BobbysStoryWorldPreference.getInt(Constants.PREF_USER_NO, 0)));
				request.put(Constants.TAG_CLIENT_IP, Util.getIp(mActivity));

				GetBalance getBalance = new GetBalance(mActivity, request);
				getBalance.setCallback(this);
				Model.runOnWorkerThread(getBalance);
			} else {
				showErrorPopup();
			}
		} else if(parser instanceof GetBalance) {
			android.util.Log.d(TAG, " get balance result = " + result);
			
			QRCodeDetectedBoundView qrCodeBound = (QRCodeDetectedBoundView) mRootView.findViewById(R.id.qrcode_code_bound);
			qrCodeBound.removeOnRectAnimationCallback();
			
			if(mProgressDialog != null) {
				mProgressDialog.dismiss();
				mProgressDialog = null;
			}
			
			String balance = result.get(Constants.TAG_EVENT_BALANCE);
			BobbysStoryWorldPreference.putInt(Constants.PREF_LAST_DOLE_COIN, Integer.valueOf(balance));
			mActivity.onQRCodeDetected(Integer.valueOf(balance), mQrProductValue);
		}
	}

	@Override
	public void onFailed(ServerTask parser, Map<String, String> result, int returnCode) {
		if(parser instanceof RequestUseQRCode && returnCode == Constants.ERROR_CODE_AUTHKEY_EXPIRED) {
			Util.requestNewAuthKey(mActivity, this);
			//Toast.makeText(mActivity, "onFail RequestUseQRCode", Toast.LENGTH_SHORT).show();
			
		} else if(parser instanceof RequestNewAuthKey) {
			Toast.makeText(mActivity, R.string.error_code_default, Toast.LENGTH_SHORT).show();
			Util.clearUserInfo();
//			mActivity.finishApplication();
			mActivity.nativeClearAuthData();
			mActivity.onBackPressed();
		} else {
		if(mProgressDialog != null) {
			mProgressDialog.dismiss();
			mProgressDialog = null;
		}
		if(parser instanceof RequestCheckQRCode) {
			//Toast.makeText(mActivity, "onFail RequestCheckQRCode", Toast.LENGTH_SHORT).show();
				if(returnCode != 1) {
//					Toast.makeText(mActivity, Util.switchErrorCode(returnCode, mActivity.getResources()), Toast.LENGTH_SHORT).show();
					switch(returnCode) {
						case ServerTask.RESULT_ERROR_NETWORK:
							showNetworkErrorPopup();
							break;

						default:
							showErrorPopup();
							break;
					}
					if(mCheckQRCodeTask != null){
						mCheckQRCodeTask = null;
					}
					mQrStringValueCp = null;
					mQrcodeValueT = null;
					iValidateFromApi = -1;
				return;
			}
				
			boolean canUse = Boolean.valueOf(result.get(Constants.TAG_CAN_USE));
			final PortraitActivity activity = mActivity;
			if(mActivity == null)
				return;
			
			if(!canUse) {
//				int warningId = R.string.unavailable_code; 
//				int requestCode = Constants.REQUEST_USER_NOTICE_FAILURE;
//				if(usedInfo != null) {
//					long timeDiff = new Date().getTime() - usedInfo.getInputDate().getTime();
//					long diffHours = timeDiff / (60 * 60 * 1000);
//					android.util.Log.d(TAG, " diffHours = " + diffHours);
//
//					if(diffHours <= 24) {
//						android.util.Log.d(TAG, " cannot! = " + result + " ! but reuse!!");
//						warningId = R.string.used_code;
//						requestCode = Constants.REQUEST_USER_NOTICE_REUSE_USED_CODE;
//					}
//				}
//				
//				android.util.Log.d(TAG, " cannot! = " + result);
//				final Bundle dialogArgs = new Bundle();
//				dialogArgs.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
//				dialogArgs.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, warningId);
//				dialogArgs.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
//				if(requestCode == Constants.REQUEST_USER_NOTICE_REUSE_USED_CODE)
//					dialogArgs.putBoolean(Constants.CUSTOM_DIALOG_ALLOW_CANCEL, true);
//
//				Util.showDialogFragment(
//						getFragmentManager(), 
//						new CustomDialog(), 
//						Constants.TAG_DIALOG_CUSTOM, 
//						dialogArgs, 
//						QRCodeManager.this, 
//						requestCode);
			}
			
			
			if(mCheckQRCodeTask != null)
				mCheckQRCodeTask = null;
				Toast.makeText(mActivity, Util.switchErrorCode(returnCode, mActivity.getResources()), Toast.LENGTH_SHORT).show();
			} else if(parser instanceof RequestUseQRCode) {
			android.util.Log.d(TAG, " use qr code result = " + result);
			Toast.makeText(mActivity, Util.switchErrorCode(returnCode, mActivity.getResources()), Toast.LENGTH_SHORT).show();
		}
	}
		//Toast.makeText(mActivity, "onFail on qr manager", Toast.LENGTH_SHORT).show();
	}
	
	private void showErrorPopup() {
		Bundle args = new Bundle();
		args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
		args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.pf_qrcode_error_used);
		args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
		args.putBoolean(Constants.CUSTOM_DIALOG_ALLOW_CANCEL, true);
		
		CustomDialog dialog = new CustomDialog(mActivity, args, null);
		dialog.show();
	}

	private void showNetworkErrorPopup() {
		Bundle args = new Bundle();
		args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
		args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.pf_qrcode_error_network_not_connect);
		args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
		args.putBoolean(Constants.CUSTOM_DIALOG_ALLOW_CANCEL, true);
		
		CustomDialog dialog = new CustomDialog(mActivity, args, null);
		dialog.show();
	}
	
	private void requestQrCodeValidation() {
		if(mCheckQRCodeTask != null)
			return;
		
		//Request qr code validity
		Map<String, String> request = new HashMap<String, String>();
		request.put(Constants.TAG_USER_NO, String.valueOf(BobbysStoryWorldPreference.getInt(Constants.TAG_USER_NO, 0)));
		request.put(Constants.TAG_SERVICE_CODE, Constants.SERVICE_CODE);
		request.put(Constants.TAG_SERIAL, mQrStringValueCp);
		request.put(Constants.TAG_PROMOTION_INFO, "1");
		request.put(Constants.TAG_CLIENT_IP, Util.getIp(mActivity));

		mCheckQRCodeTask = new RequestCheckQRCode(mActivity, request);
		mCheckQRCodeTask.setCallback(this);
		Model.runOnWorkerThread(mCheckQRCodeTask);
		mProgressDialog = Util.openProgressDialog(mActivity);
	}

	@Override
	public void onRectAnimationStarted() {
		final View rootView = mRootView;
        
        if( rootView == null) return; // FOM 20150214
		
        rootView.postDelayed(new Runnable() {
			@Override
			public void run() {
				
				if(Cocos2dxUtil.getNetworkStatus() != Cocos2dxUtil.NETWORK_STATUS_AVAILABLE){
					showNetworkErrorPopup();
					return;
				}
					
				if ( mQrStringValueCp != null && mQrcodeValueT != null) {
					if(iValidateFromApi == 0){
						showErrorPopup();
						mQrStringValueCp = null;
						mQrcodeValueT = null;
						iValidateFromApi = -1;
						return;
					}
				}
		    		
				if ( mQrStringValueCp != null && mQrcodeValueT != null ) {
					mIsKeyboardInputQR = false;
					requestQrCodeValidation();
				}
			}
		}, SimpleAnimator.DEFAULT_DURATION);
	}

	@Override
	public void onRectAnimationFinished() {
	}
    
    public boolean isQRCodePreviewing() {
		return mIsQRCodePreviewing;
	}
    
    public void pausePreview(boolean pausePreviewCallback) {
		//mIsPausePreviewCallback = pausePreviewCallback;
		mIsQRCodePreviewing = !pausePreviewCallback;
		
		if(mIsQRCodePreviewing) 
			resumeQrcodeAutoFocus();
	}
	
	public void resumeQrcodeAutoFocus() {
		if(mRootView != null && mRootView.getHandler() != null) {
			mRootView.getHandler().removeCallbacks(mQRCodefocusRunnable);
			mRootView.postDelayed(mQRCodefocusRunnable, RESUME_QRCODE_FOCUS_CALLBACK);
		}
	}
	
	private Runnable mQRCodefocusRunnable = new Runnable() {
		@Override
		public void run() {
			if(mCamera != null && !mIsPaused) {
//				mIsFocused = false;
				mCamera.autoFocus(QRCodeManager.this);
				if(mFocusView != null)
					mFocusView.setSelected(false);
			}
		}
	};
	
	public void setTransformMatrix(int width, int height) {
		mPreviewWidth = width;
		mPreviewHeight = height;
		if(mTextureView == null)
			return;
		
		mPreviewMatrix = mTextureView.getTransform(mPreviewMatrix);
        float scaleX = 1f, scaleY = 1f;
        float scaledTextureWidth, scaledTextureHeight;
        
        float aspectRatio = (float) mPreviewHeight / (float) mPreviewWidth; //(float) height / (float) width;
        
        if (width > height) {
            scaledTextureWidth = Math.max(width, (int) (height * aspectRatio));
            scaledTextureHeight = Math.max(height, (int)(width / aspectRatio));
        } else {
            scaledTextureWidth = Math.max(width, (int) (height / aspectRatio));
            scaledTextureHeight = Math.max(height, (int) (width * aspectRatio));
        }
        
        if (mSurfaceTextureUncroppedWidth != scaledTextureWidth ||
                mSurfaceTextureUncroppedHeight != scaledTextureHeight) {
            mSurfaceTextureUncroppedWidth = scaledTextureWidth;
            mSurfaceTextureUncroppedHeight = scaledTextureHeight;
        }
        scaleX = scaledTextureWidth / width;
        scaleY = scaledTextureHeight / height;
        
        mPreviewMatrix.setScale(scaleX , scaleY, (float) width / 2, (float) height / 2);
        
        if(mTextureView == null)
			return;
        mTextureView.setTransform(mPreviewMatrix);
    }

	@Override
	public void onClickOK(int requestCode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClickCancel(int requestCode) {
		// Do nothing
	}
}
