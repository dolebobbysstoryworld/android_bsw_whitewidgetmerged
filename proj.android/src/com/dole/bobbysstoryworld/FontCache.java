package com.dole.bobbysstoryworld;

import java.util.Hashtable;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.widget.TextView;

public class FontCache {

    private static Hashtable<String, Typeface> fontCache = new Hashtable<String, Typeface>();

    public static Typeface get(Context context, String name) {
        Typeface tf = fontCache.get(name);
        if(tf == null) {
            try {
            	if(Constants.DEBUG) {
                tf = Typeface.createFromAsset(context.getAssets(), name);
            	} else {
            		tf = Typeface.createFromFile(name);
            	}
            }
            catch (Exception e) {
                return null;
            }
            fontCache.put(name, tf);
        }
        return tf;
    }

    public static void setCustomFont(Context context, TextView textview, String font) {
        if(font == null) {
            return;
        }
        Typeface tf = FontCache.get(context, font);
        if(tf != null) {
            textview.setTypeface(tf);
        }
    }

    public static void setCustomFont(Context context, Paint textPaint, String font) {
        if(font == null) {
            return;
        }
        Typeface tf = FontCache.get(context, font);
        if(tf != null) {
        	textPaint.setTypeface(tf);
        }
    }
}
