package com.dole.bobbysstoryworld;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.Camera;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.format.Formatter;
import android.view.Surface;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.dole.bobbysstoryworld.server.RequestChargeCoin;
import com.dole.bobbysstoryworld.server.RequestNewAuthKey;
import com.dole.bobbysstoryworld.server.RequestUseQRCode;
import com.dole.bobbysstoryworld.server.ServerTask;
import com.dole.bobbysstoryworld.server.ServerTask.IServerResponseCallback;
import com.facebook.model.GraphUser;

public class Util {
	private static final String TAG = "Util";
	
    public static void hideSoftInput(Context context, View v) {
    	InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
	    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
	
	public static String getIp(Context context) {
		WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		return Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
	}
    
    public static ProgressDialog openProgressDialog(Context context) {
    	ProgressDialog dialog = new ProgressDialog(context);
    	dialog.setCancelable(false);
    	dialog.setIndeterminate(true);
    	dialog.show();
    	dialog.setContentView(R.layout.progress_layout);
    	return dialog;
    }
    
    public static boolean checkEmail(String src) {
    	String a = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
    	Pattern p = Pattern.compile(a);
    	Matcher m = p.matcher(src);
    	return m.find();
    }
    
    public static String switchErrorCode(int code, Resources res) {
    	switch(code) {
    	case ServerTask.RESULT_ERROR_NETWORK:
    		return res.getString(R.string.pf_net_popup_warning_network_unstable);
    	case Constants.ERROR_CODE_NO_RECORD:
    		return res.getString(R.string.pf_net_error_email_address);
    	case Constants.ERROR_CODE_PARAM:
    		return res.getString(R.string.error_code_param);
    	case Constants.ERROR_CODE_EXIST_ID:
    		return res.getString(R.string.pf_net_error_email_already_use);
    	case Constants.ERROR_CODE_EXIST_EMAIL:
    		return res.getString(R.string.pf_net_error_email_already_use);
    	case Constants.ERROR_CODE_NO_DATA:
    		return res.getString(R.string.pf_net_error_email_address);
    	case Constants.ERROR_CODE_CHECK_PWD:
    		return res.getString(R.string.pf_net_error_password_incorrect);
    	case Constants.ERROR_CODE_UNREGIST_SNS:
    		return res.getString(R.string.error_code_default);
    	case Constants.ERROR_CODE_SNS_REG:
    		return res.getString(R.string.pf_login_popup_error_fblogin);
    	case Constants.ERROR_CODE_SNS_ID:
    		return res.getString(R.string.pf_net_error_email_for_sns);
    	case Constants.ERROR_CODE_FACEBOOK_ID:
    		return res.getString(R.string.pf_net_error_email_for_sns);
    	case Constants.ERROR_CODE_LOGIN_LIMIT:
    		return res.getString(R.string.error_code_default);
    	case Constants.ERROR_CODE_USELESS_AUTH_KEY:
    		return res.getString(R.string.error_code_default);
    	case Constants.ERROR_CODE_EMAIL_UNAVAILABLE:
    		return res.getString(R.string.pf_net_error_email_not_available);
    	default:
    		return res.getString(R.string.error_code_default);
    	}
    }
	
    public static boolean isEmailError(int code) {
    	switch(code) {
    	case Constants.ERROR_CODE_NO_RECORD:
    	case Constants.ERROR_CODE_EXIST_ID:
    	case Constants.ERROR_CODE_SNS_ID:
    	case Constants.ERROR_CODE_EXIST_EMAIL:
    	case Constants.ERROR_CODE_FACEBOOK_ID:
    	case Constants.ERROR_CODE_NO_DATA:
    		return true;
    	}
    	return false;
    }
    
    public static boolean isPasswordError(int code) {
    	switch(code) {
    	case Constants.ERROR_CODE_CHECK_PWD:
    		return true;
    	}
    	return false;
    }
	
	public static String getUUID(Context context) {
		String uuid = null;
		final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		uuid = tm.getDeviceId();
		
		if(uuid == null) {
			uuid = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
		}
		
		if(uuid == null)
			return null;
		
		try {
			uuid = makeSHA1Hash(uuid);
		} catch (NoSuchAlgorithmException e) {
			android.util.Log.e(TAG, "Exception occurred while encrypting uuid", e);
			uuid = null;
		}
		
		return uuid;
	}
	
	private static String makeSHA1Hash(String input) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA1");
		md.reset();
		
		byte[] buffer = input.getBytes();
		md.update(buffer);
		
		byte[] digest = md.digest();
		
		String hexStr = "";
		for (int i = 0; i < digest.length; i++) {
			hexStr +=  Integer.toString( ( digest[i] & 0xff ) + 0x100, 16).substring( 1 );
		}
		return hexStr;
	}

    public static String createOrderId() {
    	String userNo = String.valueOf(BobbysStoryWorldPreference.getInt(Constants.TAG_USER_NO, 0));
    	String timeStamp = String.valueOf(System.currentTimeMillis());
    	return userNo + "_" + timeStamp;
    }

	/**
	 * Use qrcode
	 */
    public static void requestUseQrCode(Context context, IServerResponseCallback callback, String serial) {
    	final String authKey = BobbysStoryWorldPreference.getString(Constants.TAG_AUTHKEY, "");
    	final String userNo = String.valueOf(BobbysStoryWorldPreference.getInt(Constants.TAG_USER_NO, 0));
    	android.util.Log.e(TAG, "authKey:"+authKey);
    	android.util.Log.e(TAG, "userNo:"+userNo);
    	if(authKey == null || userNo == null)
    		return;
    	
    	final Map<String, String> request = new HashMap<String, String>();
    	request.put(Constants.TAG_USER_NO, userNo);
    	request.put(Constants.TAG_AUTHKEY, authKey);
    	request.put(Constants.TAG_ORDER_ID, createOrderId());
    	request.put(Constants.TAG_SERIAL, serial);
    	request.put(Constants.TAG_CLIENT_IP, Util.getIp(context));

    	ServerTask task = new RequestUseQRCode(context, request);
    	task.setCallback(callback);
    	Model.runOnWorkerThread(task);
    }

	/**
	 * Charge Dole coin
	 */
    public static void requestChargeCoin(Context context, IServerResponseCallback callback, int chargeAmount) {
    	final String authKey = BobbysStoryWorldPreference.getString(Constants.TAG_AUTHKEY, null);
    	final String userNo = String.valueOf(BobbysStoryWorldPreference.getInt(Constants.TAG_USER_NO, 0));
    	if(authKey == null || userNo == null)
    		return;
    	
    	final Map<String, String> request = new HashMap<String, String>();
    	request.put(Constants.TAG_SERVICE_CODE, Constants.SERVICE_CODE);
    	request.put(Constants.TAG_USER_NO, userNo);
    	request.put(Constants.TAG_AUTHKEY, authKey);
    	request.put(Constants.TAG_ORDER_ID, createOrderId());
    	
    	request.put(Constants.TAG_PAYMENT_METHOD_CODE, Constants.PAYMENT_METHOD_CODE);
    	request.put(Constants.TAG_PAYMENT_AMOUNT, "0");
    	request.put(Constants.TAG_CHARGE_AMOUNT, String.valueOf(chargeAmount));
    	request.put(Constants.TAG_VALIDITY_PERIOD, Constants.PAYMENT_VALIDITY_PERIOD);
    	request.put(Constants.TAG_DESCRIPTION, "");
    	request.put(Constants.TAG_UUID, Util.getUUID(context));    	
    	request.put(Constants.TAG_CLIENT_IP, Util.getIp(context));

    	ServerTask task = new RequestChargeCoin(context, request);
    	task.setCallback(callback);
    	Model.runOnWorkerThread(task);
    }

	/**
	 * refresh authkey
	 */
    public static void requestNewAuthKey(Context context, IServerResponseCallback callback) {
    	final String authKey = BobbysStoryWorldPreference.getString(Constants.PREF_AUTH_KEY, null);
    	final String userNo = String.valueOf(BobbysStoryWorldPreference.getInt(Constants.PREF_USER_NO, 0));
    	if(authKey == null || userNo == null)
    		return;
    	String registrationId = BobbysStoryWorldPreference.getString(BobbysStoryWorldActivity.PROPERTY_REG_ID, "");

    	final Map<String, String> request = new HashMap<String, String>();
    	request.put(Constants.TAG_USER_NO, userNo);
    	request.put(Constants.TAG_AUTHKEY, authKey);
    	request.put(Constants.TAG_SERVICE_CODE, Constants.SERVICE_CODE);
    	request.put(Constants.TAG_MOBILE_OS, Constants.OS_STRING);
		request.put(Constants.TAG_DEVICE_TOKEN, registrationId);
		request.put(Constants.TAG_DEVICE_TOKEN, "");
    	request.put(Constants.TAG_UUID, Util.getUUID(context));    	
    	request.put(Constants.TAG_CLIENT_IP, Util.getIp(context));

    	ServerTask task = new RequestNewAuthKey(context, request);
    	task.setCallback(callback);
    	Model.runOnWorkerThread(task);
    }

	/**
	 * clear userinfo for logout
	 */
    public static void clearUserInfo() {
    	BobbysStoryWorldPreference.remove(Constants.PREF_USER_ID);
    	BobbysStoryWorldPreference.remove(Constants.PREF_USER_NO);
    	BobbysStoryWorldPreference.remove(Constants.PREF_AUTH_KEY);
    	BobbysStoryWorldPreference.remove(Constants.PREF_EMAIL_VALIFICATION);
    	BobbysStoryWorldPreference.remove(Constants.PREF_EMAIL_ACTIVATE_DATE_TIME);
    	BobbysStoryWorldPreference.remove(Constants.PREF_CHANGE_PASSWROD_DATE_TIME);
    	BobbysStoryWorldPreference.remove(Constants.PREF_REGISTER_DATE_TIME);
    	BobbysStoryWorldPreference.remove(Constants.PREF_COUNTRY);
		BobbysStoryWorldPreference.remove(Constants.PREF_LANGUAGE);
		BobbysStoryWorldPreference.remove(Constants.PREF_GENDER);
		BobbysStoryWorldPreference.remove(Constants.PREF_BIRTHDAY);
		BobbysStoryWorldPreference.remove(Constants.PREF_ADDRESS_MAIN);
		BobbysStoryWorldPreference.remove(Constants.PREF_LOGIN_TYPE);
		BobbysStoryWorldPreference.remove(Constants.PREF_LAST_DOLE_COIN);
		BobbysStoryWorldPreference.remove(Constants.PREF_COIN_TO_ADDED);
		BobbysStoryWorldPreference.remove(Constants.PREF_PENDING_DOLE_COIN);
    }

    public static String getCountryIsoFromTelephonyManager(Context context) {
    	try {
            final TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
                return simCountry.toLowerCase(Locale.US);
            }
            else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                    return networkCountry.toLowerCase(Locale.US);
                }
            }
        }
        catch (Exception e) { }
        return null;
    }
    
    public static String getCountryIsoForFacebook(Context context, GraphUser user) {
    	String countryIso = null;
    	if( user.getLocation() != null) {
    		countryIso = user.getLocation().getCountry();
    	}
    	
//    	if(countryIso == null) {
//    		 countryIso = getCountryIsoFromTelephonyManager(context);
//    	} else {
//    		return countryIso;
//    	}
    	
    	return countryIso;
    }
    
    public static String getCityForFacebook(GraphUser user) {
    	String city = null;
    	if(user.getLocation() != null) {
    		city = user.getLocation().getCity();

    		if(city == null) {
    			String cityAndCountry = (String) user.getLocation().getProperty("name");
    			if(cityAndCountry != null) {
    				city = cityAndCountry.substring(0, cityAndCountry.indexOf(","));
    			}
    		}
    	}
    	if(city == null)
    		return "";
    	return city;
    }

    public static int getCameraDisplayOrientation(Activity activity,
            int cameraId, android.hardware.Camera camera) {
        android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
        case Surface.ROTATION_0: degrees = 0; break;
        case Surface.ROTATION_90: degrees = 90; break;
        case Surface.ROTATION_180: degrees = 180; break;
        case Surface.ROTATION_270: degrees = 270; break;
        }
        
        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        return result;
        //camera.setDisplayOrientation(result);
    }
    
    public static void rectFToRect(RectF rectF, Rect rect) {
        rect.left = Math.round(rectF.left);
        rect.top = Math.round(rectF.top);
        rect.right = Math.round(rectF.right);
        rect.bottom = Math.round(rectF.bottom);
    }

    public static Rect rectFToRect(RectF rectF) {
        Rect rect = new Rect();
        rectFToRect(rectF, rect);
        return rect;
    }

    public static RectF rectToRectF(Rect r) {
        return new RectF(r.left, r.top, r.right, r.bottom);
    }

    public static void prepareMatrix(Matrix matrix, int displayOrientation,
            int viewWidth, int viewHeight) {
        matrix.setScale(1, 1);
        matrix.postRotate(displayOrientation);
        matrix.postScale(viewWidth / 2000f, viewHeight / 2000f);
        matrix.postTranslate(viewWidth / 2f, viewHeight / 2f);
    }

    public static void prepareMatrix(Matrix matrix, int displayOrientation,
                                     Rect previewRect) {
        matrix.setScale(1, 1);
        matrix.postRotate(displayOrientation);
        matrix.setRectToRect(rectToRectF(previewRect), new RectF(-1000, -1000, 1000, 1000),
                Matrix.ScaleToFit.FILL);
    }
    
    public static boolean isCriticalVersionUpdateRequired(String appVersion, String serverVserion) {
    	String[] appParts = appVersion.split("\\.");
    	String[] serverParts = serverVserion.split("\\.");
    	int length = Math.max(appParts.length, serverParts.length);

    	for(int i = 0; i < length; i++) {

    		int appPart = i < appParts.length ? Integer.parseInt(appParts[i]) : 0;
    		int serverPart = i < serverParts.length ? Integer.parseInt(serverParts[i]) : 0;

    		if(appPart < serverPart)
    			return i < 2 ? true:false; // MAJOR(0), MINOR(1), FIX(2)

    		if(appPart > serverPart)
    			return false;
    	}
    	return false;
    }
    
    public static boolean isInternetAvailable(Context context) {
    	final ConnectivityManager cm =
    	        (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
    	 
    	final NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    	return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
    
    public static boolean isWifiAvailable(Context context) {
    	final ConnectivityManager cm =
    	        (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
    	 
    	final NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    	if(activeNetwork == null)
    		return false;
    	int networkType = activeNetwork.getType();
    	if(networkType == ConnectivityManager.TYPE_WIFI)
    		return true;
    	else
    		return false;
    }

	public static String getString(Context context, int id) {
		Resources r = context.getResources();
		switch(id) {
			case Constants.CC_APP_UPDATE_NOTICE: return r.getString(R.string.cc_app_update_notice);
			case Constants.CC_APPUPDATE_BUTTON_TITLE_OK: return r.getString(R.string.cc_appupdate_button_title_ok);
			case Constants.PF_NET_WARNING_3G: return r.getString(R.string.pf_net_warning_3g);
			case Constants.CM_POPUP_BUTTON_TITLE_CANCEL: return r.getString(R.string.cm_popup_button_title_cancel);
			case Constants.CM_POPUP_BUTTON_TITLE_OK: return r.getString(R.string.cm_popup_button_title_ok);
			case Constants.PF_NET_POPUP_WARNING_NETWORK_UNSTABLE: return r.getString(R.string.pf_net_popup_warning_network_unstable);
			case Constants.PF_NET_POPUP_NOTICCE_UPDATE_AND_RESTART: return r.getString(R.string.pf_net_popup_notice_update_and_restart);
			case Constants.PF_NET_POPUP_NOTICCE_DOWNLOAD_RESOURCCE: return r.getString(R.string.pf_net_popup_notice_download_resource);
			case Constants.CM_NET_WARNING_NETWORK_UNSTABLE: return r.getString(R.string.cm_net_warning_network_unstable);
			case Constants.PF_NET_ERROR_EMAIL_ADDRESS: return r.getString(R.string.pf_net_error_email_address);
			case Constants.PF_NET_ERROR_EMAIL_FOR_SNS: return r.getString(R.string.pf_net_error_email_for_sns);
			case Constants.PF_NET_ERROR__PASSWORD_INCORRECT: return r.getString(R.string.pf_net_error_password_incorrect);
			case Constants.PF_NET_ERROR_EMAIL_ENTER_FULL_ADDRESS: return r.getString(R.string.pf_net_error_email_enter_full_address);
			case Constants.PF_NET_ERROR_EMAIL_ALREADY_USE: return r.getString(R.string.pf_net_error_email_already_use);
			case Constants.PF_NET_ERROR__PASSWORD_SHORT: return r.getString(R.string.pf_net_error_password_short);
			case Constants.PF_NET_ERROR__QRCODE_INPUT: return r.getString(R.string.pf_net_error_qrcode_input);
			case Constants.CC_EVENTNOTICCE_DISPLAY_A_DAY: return r.getString(R.string.cc_eventnotice_display_a_day);
			case Constants.CM_POPUP_BUTTON_TITLE_CLOSE: return r.getString(R.string.cm_popup_button_title_close);
			case Constants.CC_HOME_BOOKTITLE_NEW: return r.getString(R.string.cc_home_booktitle_new);
			case Constants.CC_HOME_MENUTITLE_SETTING: return r.getString(R.string.cc_home_menutitle_setting);
			case Constants.CC_HOME_POPUP_DELETE_STORY: return r.getString(R.string.cc_home_popup_delete_story);
			case Constants.CC_HOME_POPUP_NEED_LOGIN: return r.getString(R.string.cc_home_popup_need_login);
			case Constants.CC_HOME_POPUP_NEED_LOGIN_BUTTON_TITLE_LOGIN: return r.getString(R.string.cc_home_popup_need_login_button_title_login);
			case Constants.PF_DOLECOIN_ABOUTCOIN_Q: return r.getString(R.string.pf_dolecoin_aboutcoin_q);
			case Constants.PF_DOLECOIN_ABOUTCOIN_A: return r.getString(R.string.pf_dolecoin_aboutcoin_a);
			case Constants.PF_DOLECOIN_HOWEARN_Q: return r.getString(R.string.pf_dolecoin_howearn_q);
			case Constants.PF_DOLECOIN_HOWEARN_A: return r.getString(R.string.pf_dolecoin_howearn_a);
			case Constants.PF_DOLECOIN_BUTTONTITLE_SCAN: return r.getString(R.string.pf_dolecoin_buttontitle_scan);
			case Constants.PF_DOLECOIN_NEED_LOGIN: return r.getString(R.string.pf_dolecoin_need_login);
			case Constants.PF_POPUP_RECEIVED_DOLECOIN: return r.getString(R.string.pf_popup_received_dolecoin);
			case Constants.PF_QRCODE_INPUT_CODE: return r.getString(R.string.pf_qrcode_input_code);
			case Constants.PF_QRCODE_SCAN_CODE: return r.getString(R.string.pf_qrcode_scan_code);
			case Constants.PF_QRCODE_ERROR_USED: return r.getString(R.string.pf_qrcode_error_used);
			case Constants.PF_QRCODE_ERROR_NETWORK_NOT_CONNCET: return r.getString(R.string.pf_qrcode_error_network_not_connect);
			case Constants.CC_LIST_CHARACTER_MAKE: return r.getString(R.string.cc_list_character_make);
			case Constants.CC_LIST_NEED_UNLOCK_COIN: return r.getString(R.string.cc_list_need_unlock_coin);
			case Constants.CC_LIST_CHARACTER_WARNING_DELETE: return r.getString(R.string.cc_list_character_warning_delete);
			case Constants.CC_EXPANDEDITEM_NEED_UNLOCK_COIN: return r.getString(R.string.cc_expandeditem_need_unlock_coin);
			case Constants.CC_EXPANDEDITEM_NEED_UNLOCK_COIN_1: return r.getString(R.string.cc_expandeditem_need_unlock_coin_1);
			case Constants.CC_EXPANDEDITEM_NEED_UNLOCK_COIN_2: return r.getString(R.string.cc_expandeditem_need_unlock_coin_2);
			case Constants.CC_EXPANDEDITEM_NEED_UNLOCK_COIN_LIMITED_PERIOD: return r.getString(R.string.cc_expandeditem_need_unlock_coin_limited_period);
			case Constants.CC_EXPANDEDITEM_NEED_UNLOCK_COIN_LIMITED_NUMBER: return r.getString(R.string.cc_expandeditem_need_unlock_coin_limited_number);
			case Constants.CC_EXPANDEDITEM_POPUP_WARNING_USE_UNLOCK_COIN: return r.getString(R.string.cc_expandeditem_popup_warning_use_unlock_coin);
			case Constants.CC_EXPANDEDITEM_POPUP_COIN_BUTTON_TITLE_UNLOCK: return r.getString(R.string.cc_expandeditem_popup_coin_button_title_unlock);
			case Constants.CC_EXPANDEDITEM_POPUP_NOT_ENOUGH_COIN: return r.getString(R.string.cc_expandeditem_popup_not_enough_coin);
			case Constants.CC_EXPANDEDITEM_POPUP_BUTTON_TITLE_EARN_COIN: return r.getString(R.string.cc_expandeditem_popup_button_title_earn_coin);
			case Constants.CC_EXPANDEDITEM_POPUP_NEED_LOGIN: return r.getString(R.string.cc_expandeditem_popup_need_login);
			case Constants.CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD: return r.getString(R.string.cc_expandeditem_notice_limited_period);
			case Constants.CC_EXPANDEDITEM_NOTICE_LIMITED_NUMBER: return r.getString(R.string.cc_expandeditem_notice_limited_number);
			case Constants.PF_CREATECHAR_BUTTON_TITLE_RETAKE: return r.getString(R.string.pf_createchar_button_title_retake);
			case Constants.PF_CREATECHAR_BUTTON_TITLE_SAVE: return r.getString(R.string.pf_createchar_button_title_save);
			case Constants.PF_CREATECHAR_BUTTON_TITLE_CANCEL: return r.getString(R.string.pf_createchar_button_title_cancel);
			case Constants.CC_LIST_BACKGROUND_MAKE: return r.getString(R.string.cc_list_background_make);
			case Constants.CC_LIST_BACKGROUND_WARNING_DELETE: return r.getString(R.string.cc_list_background_warning_delete);
			case Constants.CC_EXPANDEDBG_NEED_UNLOCK_COIN: return r.getString(R.string.cc_expandedbg_need_unlock_coin);
			case Constants.CC_EXPANDEDBG_NEED_UNLOCK_COIN_1: return r.getString(R.string.cc_expandeddbg_need_unlock_coin_1);
			case Constants.CC_EXPANDEDBG_NEED_UNLOCK_COIN_2: return r.getString(R.string.cc_expandeddbg_need_unlock_coin_2);
			case Constants.CC_EXPANDEDBG_NEED_UNLOCK_COIN_LIMITED_PERIOD: return r.getString(R.string.cc_expandedbg_need_unlock_coin_limited_period);
			case Constants.CC_EXPANDEDBG_NEED_UNLOCK_COIN_NUMBER: return r.getString(R.string.cc_expandedbg_need_unlock_coin_number);
			case Constants.PF_LOGIN_TEXTFIELD_EMAIL: return r.getString(R.string.pf_login_textfield_email);
			case Constants.PF_LOGIN_TEXTFIELD_PASSWORD: return r.getString(R.string.pf_login_textfield_password);
			case Constants.PF_LOGIN_BUTTON_TITLE_LOGIN: return r.getString(R.string.pf_login_button_title_login);
			case Constants.PF_LOGIN_BUTTON_TITLE_FORGOT: return r.getString(R.string.pf_login_button_title_forgot);
			case Constants.PF_LOGIN_BUTTON_TITLE_SIGNUP: return r.getString(R.string.pf_login_button_title_signup);
			case Constants.PF_LGOIN_BUTTON_FBLOGIN: return r.getString(R.string.pf_login_button_fblogin);
			case Constants.PF_LOGIN_POPUP_ERROR_FBLOGIN: return r.getString(R.string.pf_login_popup_error_fblogin);
			case Constants.PF_LOGIN_BUTTON_TITLE_SKIP: return r.getString(R.string.pf_login_button_title_skip);
			case Constants.PF_FORGOT_NOTICE_FORGOT_PASSWORD: return r.getString(R.string.pf_forgot_notice_forgot_password);
			case Constants.PF_FORGOT_TEXTFIELD_EMAIL: return r.getString(R.string.pf_forgot_textfield_email);
			case Constants.PF_FORGOT_BUTTON_TITLE_SEND: return r.getString(R.string.pf_forgot_button_title_send);
			case Constants.PF_FORGOT_POPUP_CONFIRM_PASSWORD: return r.getString(R.string.pf_forgot_popup_confirm_password);
			case Constants.PF_SIGNUP_TEXTFIELD_EMAIL: return r.getString(R.string.pf_signup_textfield_email);
			case Constants.PF_SIGNUP_TEXTFIELD_PASSWORD: return r.getString(R.string.pf_signup_textfield_password);
			case Constants.PF_SIGNUP_TEXTFIELD_PASSWORD_CONDITION: return r.getString(R.string.pf_signup_textfield_password_condition);
			case Constants.PF_SIGNUP_TEXTFIELD_PASSWORD_CONFIRM: return r.getString(R.string.pf_signup_textfield_password_confirm);
			case Constants.PF_SIGNUP_TEXTFIELD_BIRTHYEAR: return r.getString(R.string.pf_signup_textfield_birthyear);
			case Constants.PF_SIGNUP_TEXTFIELD_CITY: return r.getString(R.string.pf_signup_textfield_city);
			case Constants.PF_SIGNUP_RADIOTITLE_MALE: return r.getString(R.string.pf_signup_radiotitle_male);
			case Constants.PF_SIGNUP_RADIOTITLE_FEMALE: return r.getString(R.string.pf_signup_radiotitle_female);
			case Constants.PF_SIGNUP_CHECKBOX_TITLE_AGREE: return r.getString(R.string.pf_signup_checkbox_title_agree);
			case Constants.PF_SIGNUP_BUTTON_TITLE_SIGNUP: return r.getString(R.string.pf_signup_button_title_signup);
			case Constants.PF_SIGNUP_POPUP_CANCEL_SIGNUP: return r.getString(R.string.pf_signup_popup_cancel_signup);
			case Constants.PF_SIGNUP_POPUP_BUTTON_TITLE_OK: return r.getString(R.string.pf_signup_popup_button_title_ok);
			case Constants.PF_SIGNUP_POPUP_BUTTON_TITLE_CANCEL: return r.getString(R.string.pf_signup_popup_button_title_cancel);
			case Constants.PF_SIGNUP_POPUP_TITLE_BIRTHYEAR: return r.getString(R.string.pf_signup_popup_title_birthyear);
			case Constants.PF_SIGNUP_BUTTON_TITLE_SET: return r.getString(R.string.pf_signup_button_title_set);
			case Constants.PF_SIGNUP_PUPUP_TITLE_SELECT_COUNTRY: return r.getString(R.string.pf_signup_popup_title_select_country);
			case Constants.PF_SIGNUP_COUNTRY_LIST_OTHER: return r.getString(R.string.pf_signup_country_list_other);
			case Constants.PF_SIGNUP_PUPUP_TITLE_SELECT_CITY: return r.getString(R.string.pf_signup_popup_title_select_city);
			case Constants.PF_SIGNUP_CHECKBOX_TITLE_AGREE_TERMS: return r.getString(R.string.pf_signup_checkbox_title_agree_terms);
			case Constants.PF_SIGNUP_CHECKBOX_TITLE_AGREE_PRIVACY: return r.getString(R.string.pf_signup_checkbox_title_agree_privacy);
			case Constants.PF_SIGNUP_BUTTON_TITLE_NEXT: return r.getString(R.string.pf_signup_button_title_next);
			case Constants.CC_JOURNEYMAP_BUTTON_INTRO: return r.getString(R.string.cc_journeymap_button_intro);
			case Constants.CC_JOURNEYMAP_BUTTON_FINAL: return r.getString(R.string.cc_journeymap_button_final);
			case Constants.CC_JOURNEYMAP_BOOKTITLE_DEFAULT: return r.getString(R.string.cc_journeymap_booktitle_default);
			case Constants.CC_JOURNEYMAP_SEPARATOR_BOOKNAME: return r.getString(R.string.cc_journeymap_separator_bookname);
			case Constants.CC_JOURNEYMAP_BOOKAUTHOR_DEFAULT: return r.getString(R.string.cc_journeymap_bookauthor_default);
			case Constants.CC_JOURNEYMAP_BUTTON_TITLE_PLAY: return r.getString(R.string.cc_journeymap_button_title_play);
			case Constants.CC_JOURNEYMAP_MENU_TITLE_DOWNLOADDEVICE: return r.getString(R.string.cc_journeymap_menu_title_downloaddevice);
			case Constants.CC_JOURNEYMAP_MENU_TITLE_SHARE: return r.getString(R.string.cc_journeymap_menu_title_share);
			case Constants.CC_JOURNEYMAP_POPUP_NOTICCE_DOWNLOADDEVICE: return r.getString(R.string.cc_journeymap_popup_notice_downloaddevice);
			case Constants.CC_JOURNEYMAP_POPUP_NOTICE_SHARE: return r.getString(R.string.cc_journeymap_popup_notice_share);
			case Constants.CC_JOURNEYMAP_POPUP_TITLE_SHARE_LIST: return r.getString(R.string.cc_journeymap_popup_title_share_list);
			case Constants.CC_JOURNEYMAP_POPUP_SHARELIST_FACEBOOK: return r.getString(R.string.cc_journeymap_popup_sharelist_facebook);
			case Constants.CC_JOURNEYMAP_POPUP_SHARELIST_YOUTUBE: return r.getString(R.string.cc_journeymap_popup_sharelist_youtube);
			case Constants.CC_JOURNEYMAP_POPUP_ERROR_SHARE: return r.getString(R.string.cc_journeymap_popup_error_share);
			case Constants.CC_JOURNEYMAP_POPUP_WARNING_DELETE_PAGE: return r.getString(R.string.cc_journeymap_popup_warning_delete_page);
			case Constants.CC_JOURNEYMAP_BUTTON_TITLE_CANCEL: return r.getString(R.string.cc_journeymap_button_title_cancel);
			case Constants.CC_JOURNEYMAP_POPUP_WARNING_CANCEL_CONVERT: return r.getString(R.string.cc_journeymap_popup_warning_cancel_convert);
			case Constants.CC_JOURNEYMAP_POPUP_WARNING_NOTENOUGH_STORAGE: return r.getString(R.string.cc_journeymap_popup_warning_notenough_storage);
			case Constants.PF_SHARECOMMENT_POPUP_TITLE: return r.getString(R.string.pf_sharecomment_popup_title);
			case Constants.PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_FACEBOOK: return r.getString(R.string.pf_sharecomment_popup_defaultcomment_facebook);
			case Constants.PF_SHARECOMMNET_POPUP_HASHTAG_FACEBOOK: return r.getString(R.string.pf_sharecomment_popup_hashtag_facebook);
			case Constants.PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_YOUTUBE: return r.getString(R.string.pf_sharecomment_popup_defaultcomment_youtube);
			case Constants.PF_SHARECOMMNET_POPUP_HASHTAG_YOUTUBE: return r.getString(R.string.pf_sharecomment_popup_hashtag_youtube);
			case Constants.CC_SETPOSITION_BUTTON_TITLE_RECORD: return r.getString(R.string.cc_setposition_button_title_record);
			case Constants.CC_SETPOSITION_POPUP_NOTICE_AUTOSELECT_LASTPAGE: return r.getString(R.string.cc_setposition_popup_notice_autoselect_lastpage);
			case Constants.CC_SETPOSITION_POPUP_WARNING_LIMITED_REDUCED: return r.getString(R.string.cc_setposition_popup_warning_limited_reduced);
			case Constants.CC_SETPOSITION_POPUP_WARNING_ITEM_REUNLOCK: return r.getString(R.string.cc_setposition_popup_warning_item_reunlock);
			case Constants.CC_SETPOSTION_POPUP_WARNING_DELETE_CHARACTER: return r.getString(R.string.cc_setposition_popup_warning_delete_character);
			case Constants.CC_SETPOSTION_POPUP_WARNING_DELETE_BACKGROUND: return r.getString(R.string.cc_setposition_popup_warning_delete_background);
			case Constants.CC_LIST_MUSIC_TITLE_NONE: return r.getString(R.string.cc_list_music_title_none);
			case Constants.CC_LIST_MUSIC_TITLE_JOLLY: return r.getString(R.string.cc_list_music_title_jolly);
			case Constants.CC_LIST_MUSIC_TITLE_DREAM: return r.getString(R.string.cc_list_music_title_dream);
			case Constants.CC_LIST_MUSIC_TITLE_BRAVE: return r.getString(R.string.cc_list_music_title_brave);
			case Constants.CC_LIST_MUSIC_TITLE_SHINE: return r.getString(R.string.cc_list_music_title_shine);
			case Constants.CC_LIST_MUSIC_TITLE_BRISK: return r.getString(R.string.cc_list_music_title_brisk);
			case Constants.CC_LIST_MUSIC_TITLE_ANGRY: return r.getString(R.string.cc_list_music_title_angry);
			case Constants.CC_LIST_MUSIC_TITLE_SLEEPY: return r.getString(R.string.cc_list_music_title_sleepy);
			case Constants.CC_LIST_MUSIC_TITLE_ADVENTURE: return r.getString(R.string.cc_list_music_title_adventure);
			case Constants.CC_LIST_MUSIC_TITLE_CRUISE: return r.getString(R.string.cc_list_music_title_cruise);
			case Constants.CC_LIST_MUSIC_TITLE_JUNGLE: return r.getString(R.string.cc_list_music_title_jungle);
			case Constants.CC_RECORDING_BUTTON_TITLE_DONE: return r.getString(R.string.cc_recording_button_title_done);
			case Constants.CC_RECORDING_POPUP_WARNING_CONTENT_WITHOUT_SAVING: return r.getString(R.string.cc_recording_popup_warning_content_without_saving);
			case Constants.CC_RECORDING_BUTTON_TITLE_RECODING: return r.getString(R.string.cc_recording_button_title_recording);
			case Constants.CC_RECORDING_BUTTON_TITLE_RERECORDING: return r.getString(R.string.cc_recording_button_title_rerecording);
			case Constants.CC_RECORDING_BUTTON_TITLE_SAVE: return r.getString(R.string.cc_recording_button_title_save);
			case Constants.CC_RECORDING_POPUP_WARNING_RERECORDING: return r.getString(R.string.cc_recording_popup_warning_rerecording);
			case Constants.CC_RECORDING_POPUP_WARNING_STORY_WITHOUT_SAVING: return r.getString(R.string.cc_recording_popup_warning_story_without_saving);
			case Constants.CC_PLAY_PAGE_TITLE_INTRO: return r.getString(R.string.cc_play_page_title_intro);
			case Constants.CC_PLAY_PAGE_TITLE_PAGE_N: return r.getString(R.string.cc_play_page_title_page_n);
			case Constants.CC_PLAY_PAGE_TITLE_FINAL: return r.getString(R.string.cc_play_page_title_final);
			case Constants.PF_SETTING_NOTICE_MORE_ADVANTAGE_LOGIN: return r.getString(R.string.pf_setting_notice_more_advantage_login);
			case Constants.PF_SETTING_BUTTON_TITLE_LOGIN: return r.getString(R.string.pf_setting_button_title_login);
			case Constants.PF_SETTING_SOUND: return r.getString(R.string.pf_setting_sound);
			case Constants.PF_SETTING_BUTTON_TITLE_ABOUT: return r.getString(R.string.pf_setting_button_title_about);
			case Constants.PF_SETTING_BUTTON_TITLE_HELP: return r.getString(R.string.pf_setting_button_title_help);
			case Constants.PF_SETTING_BUTTON_TITLE_EDIT: return r.getString(R.string.pf_setting_button_title_edit);
			case Constants.PF_SETTING_BUTTON_TITLE_USAGE_COIN: return r.getString(R.string.pf_setting_button_title_usage_coin);
			case Constants.PF_SETTING_BUTTON_TITLE_DELETE_ACCOUNT: return r.getString(R.string.pf_setting_button_title_delete_account);
			case Constants.PF_EDITACCOUNT_TEXTFIELD_CURRENT_PASSWORD: return r.getString(R.string.pf_editaccount_textfield_current_password);
			case Constants.PF_EDITACCOUNT_TEXTFIELD_PASSWORD: return r.getString(R.string.pf_editaccount_textfield_password);
			case Constants.PF_EDITACCOUNT_TEXTFIELD_PASSWORD_CONDITION: return r.getString(R.string.pf_editaccount_textfield_password_condition);
			case Constants.PF_EDITACCOUNT_TEXTFIELD_CONFIRM_PASSWORD: return r.getString(R.string.pf_editaccount_textfield_confirm_password);
			case Constants.PF_EDITACCOUNT_BUTTON_TITLE_SAVE: return r.getString(R.string.pf_editaccount_button_title_save);
			case Constants.PF_USAGECOIN_NOTICE: return r.getString(R.string.pf_usagecoin_notice);
			case Constants.PF_USAGECOIN_LISTFIELD_DETAILS: return r.getString(R.string.pf_usagecoin_listfield_details);
			case Constants.PF_USAGECOIN_LISTFIELD_DATE: return r.getString(R.string.pf_usagecoin_listfield_date);
			case Constants.PF_USAGECOIN_BUTTON_TITLE_HELP: return r.getString(R.string.pf_usagecoin_button_title_help);
			case Constants.PF_USAGECOIN_NOTICE_NOHISTORY: return r.getString(R.string.pf_usagecoin_notice_nohistory);
			case Constants.PF_USAGECOIN_UNLIMITED: return r.getString(R.string.pf_usagecoin_unlimited);
			case Constants.PF_USAGECOIN_LIMITED_PERIOD: return r.getString(R.string.pf_usagecoin_limited_period);
			case Constants.PF_DELETEACCOUNT_NOTICE: return r.getString(R.string.pf_deleteaccount_notice);
			case Constants.PF_DELETEACCOUNT_CHECKBOX_CONFIRM: return r.getString(R.string.pf_deleteaccount_checkbox_confirm);
			case Constants.PF_DELETEACCOUNT_TEXTFIELD_CONFIRM_PASSWORD: return r.getString(R.string.pf_deleteaccount_textfield_confirm_password);
			case Constants.PF_DELETEACCOUNT_BUTTON_TITLE_DELETE: return r.getString(R.string.pf_deleteaccount_button_title_delete);
			case Constants.PF_DELETEACCOUNT_TABLE_TITLEE_DOLECOIN: return r.getString(R.string.pf_deleteaccount_table_title_dolecoin);
			case Constants.PF_DELETEACCOUNT_TABLE_TITLEE_BOBBY: return r.getString(R.string.pf_deleteaccount_table_title_bobby);
			case Constants.PF_DELETEACCOUNT_TABLE_SUBTITLE_DOLECOIN: return r.getString(R.string.pf_deleteaccount_table_subtitle_dolecoin);
			case Constants.PF_DELETEACCOUNT_TABLE_SUBTITLE_BOBBY: return r.getString(R.string.pf_deleteaccount_table_subtitle_bobby);
			case Constants.PF_DELETEACCOUNT_TABLE_SUBTITLE_BOBBY_ADD: return r.getString(R.string.pf_deleteaccount_table_subtitle_bobby_add);
			case Constants.PF_ABOUT_APPTITLE: return r.getString(R.string.pf_about_apptitle);
			case Constants.PF_ABOUT_SERVICE_INTRODUCE: return r.getString(R.string.pf_about_service_introduce);
			case Constants.PF_ABOUT_BUTTON_TITLE_TERMS: return r.getString(R.string.pf_about_button_title_terms);
			case Constants.PF_ABOUT_TITLE_SERVICE: return r.getString(R.string.pf_about_title_service);
			case Constants.PF_ABOUT_TITLE_PRIVACY: return r.getString(R.string.pf_about_title_privacy);
			case Constants.PF_HELP_BUTTON_TITLE_FAQ: return r.getString(R.string.pf_help_button_title_faq);
			case Constants.PF_HELP_TEXTFIELD_EMAIL: return r.getString(R.string.pf_help_textfield_email);
			case Constants.PF_HELP_TEXTFIELD_FEEDBACK: return r.getString(R.string.pf_help_textfield_feedback);
			case Constants.PF_HELP_DEVICE: return r.getString(R.string.pf_help_device);
			case Constants.PF_HELP_WARNING_PRIVACYINFO: return r.getString(R.string.pf_help_warning_privacyinfo);
			case Constants.PF_HELP_BUTTON_TITLE_SEND: return r.getString(R.string.pf_help_button_title_send);
			case Constants.CC_GUIDE_HOME: return r.getString(R.string.cc_guide_home);
			case Constants.CC_GUIDE_JOURNEYMAP_NEWPAGE: return r.getString(R.string.cc_guide_journeymap_newpage);
			case Constants.CC_GUIDE_JOURNEYMAP_ALLPLAY: return r.getString(R.string.cc_guide_journeymap_allplay);
			case Constants.CC_GUIDE_SETPOSITION_SELECT_ITEM: return r.getString(R.string.cc_guide_setposition_select_item);
			case Constants.CC_GUIDE_SETPOSITION_SELECT_BACKGROUND: return r.getString(R.string.cc_guide_setposition_select_background);
			case Constants.CC_GUIDE_SETPOSITION_SELECT_MUSIC: return r.getString(R.string.cc_guide_setposition_select_music);
			case Constants.CC_GUIDE_SETPOSITION_DELETE_ITEM: return r.getString(R.string.cc_guide_setposition_delete_item);
			case Constants.CC_GUIDE_SETPOSITION_RECORDING: return r.getString(R.string.cc_guide_setposition_recording);
			case Constants.PF_GUIDE_EDIT_PICTURE: return r.getString(R.string.pf_guide_edit_picture);
			case Constants.PF_GUIDE_LOGIN_SIGNUP: return r.getString(R.string.pf_guide_login_signup);
			case Constants.PF_GUIDE_LOGIN_EARNCOIN: return r.getString(R.string.pf_guide_login_earncoin);
			case Constants.PF_GUIDE_LOGIN_GETITEM: return r.getString(R.string.pf_guide_login_getitem);
			case Constants.PF_GUIDE_LOGIN_SHARE: return r.getString(R.string.pf_guide_login_share);
			case Constants.PF_LOGIN_POPUP_ERROR_NOTAVAILABLE_EMAIL: return r.getString(R.string.pf_login_popup_error_notavailable_email);
			case Constants.CC_HOME_POPUP_UNLOCK_ITEM_MESSAGE: return r.getString(R.string.cc_home_popup_unlock_item_message);
			case Constants.CC_JOURNEYMAP__DOWNLOAD_DEVICE_COMPLETE: return r.getString(R.string.cc_journeymap_download_device_complete);
			case Constants.CC_JOURNEYMAP__SHARE_COMPLETE: return r.getString(R.string.cc_journeymap_share_complete);
			case Constants.CC_SETPOSITION_POPUP_WARNING_ITEM_FULL: return r.getString(R.string.cc_setposition_popup_warning_item_full);
			case Constants.PF_EDITACCOUNT_POPUP_INPUT_PASSWORD: return r.getString(R.string.pf_editaccount_popup_input_password);
			case Constants.PF_EDITACCOUNT_POPUP_SAVE_COMPLETE: return r.getString(R.string.pf_editaccount_popup_save_complete);
			case Constants.PF_HELP_POPUP_SEND_COMPLETE: return r.getString(R.string.pf_help_popup_send_complete);
			case Constants.PF_DELETEACCOUNT_DELETE_COMPLETE: return r.getString(R.string.pf_deleteaccount_delete_complete);
			case Constants.PF_PROMOTION_CHECKBOX: return r.getString(R.string.pf_promotion_checkbox);
			case Constants.CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_DAYS: return r.getString(R.string.cc_expandeditem_notice_limited_period_days);
			case Constants.CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_HOURS: return r.getString(R.string.cc_expandeditem_notice_limited_period_hours);
			case Constants.CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_HOUR: return r.getString(R.string.cc_expandeditem_notice_limited_period_hour);
			case Constants.CC_JOURNEYMAP_SHARE_PROGRESS_NOTICE: return r.getString(R.string.cc_journeymap_share_progress_notice);
			case Constants.CC_RECORDING_BUTTON_TITLE_RECOD: return r.getString(R.string.cc_recording_button_title_record);
			case Constants.CC_EXPANDEDITEM_NEED_UNLOCK_COIN_CHA: return r.getString(R.string.cc_expandeditem_need_unlock_coin_cha);
			case Constants.CC_POPUP_CANCEL_SHARE: return r.getString(R.string.cc_popup_cancel_share);
			case Constants.CC_PRLOAD_BOOKTITLE_1: return r.getString(R.string.cc_preload_booktitle_1);
			case Constants.CC_PRLOAD_BOOKTITLE_2: return r.getString(R.string.cc_preload_booktitle_2);
			case Constants.CM_POPUP_BUTTON_TITLE_YES: return r.getString(R.string.cm_popup_button_title_yes);
			case Constants.CM_POPUP_BUTTON_TITLE_NO: return r.getString(R.string.cm_popup_button_title_no);
			case Constants.CF_NET_ERROR_EMAIL_NOT_AVAILABLE: return r.getString(R.string.pf_net_error_email_not_available);
		}
		return "";
	}
}