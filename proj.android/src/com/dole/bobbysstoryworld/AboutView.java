package com.dole.bobbysstoryworld;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AboutView extends RelativeLayout {
	
	boolean mIsInitialSelection = true;	
	
	public AboutView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public AboutView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public AboutView(Context context) {
		this(context, null);
	}

	@Override
	public void onFinishInflate() {
		super.onFinishInflate();
		
		TextView version = (TextView)findViewById(R.id.about_version);
		View service = findViewById(R.id.about_service_terms);
		try {
			PackageInfo pInfo = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
			version.setText(getContext().getResources().getString(R.string.about_version, pInfo.versionName));
		} catch (Exception e) {
			e.printStackTrace();
		}
		service.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Bundle data = ((PortraitActivity)getContext()).getSavedDataBundle();
				if(data == null)
					data = new Bundle();
				data.putBoolean(AgreePersonalInfoView.HIDE_CHECKBOX, true);
				data.putString(Constants.TAG_STANDARD_COUNTRY_CODE, BobbysStoryWorldPreference.getString(Constants.PREF_COUNTRY, ""));
				((PortraitActivity)getContext()).showMenu(PortraitActivity.ENTRY_MENU_ABOUT_TERMS);
			}
		});
	}
}
