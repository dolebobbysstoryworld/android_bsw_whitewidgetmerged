package com.dole.bobbysstoryworld;

import android.graphics.Bitmap;
import android.util.LruCache;

public class ImageCache {
	private static final int MAX_COUNT = 10;
	private LruCache<Integer, Bitmap> mImageCache;

	private class BitmapCache extends LruCache<Integer, Bitmap> {
        public BitmapCache(int maxSize) {
            super(maxSize);
        }
        
        @Override
        protected void entryRemoved(boolean evicted, Integer key, Bitmap oldValue, Bitmap newValue) {
            super.entryRemoved(evicted, key, oldValue, newValue);
            if (oldValue.isMutable()) {
                oldValue.recycle();
            }
        }
	}

	public ImageCache() {
		mImageCache = new BitmapCache(MAX_COUNT);
	}
	
	public Bitmap getImage(int id) { 
		return mImageCache.get(id);
	}

	public void setImage(int id, Bitmap image) { 
		mImageCache.put(id, image);
	}

	public void clearCache() {
		mImageCache.evictAll();
	}
}