package com.dole.bobbysstoryworld;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;

public class BobbysStoryWorldPreference {
	
	private static SharedPreferences sPreference;
    private static final String SHARED_PREFERENCES_NAME = "bobbys_journey_preferences";
    
    private static final Map<String, Boolean> sTmpGuideState = new HashMap<String, Boolean>();
    
    public static void initSharedPreferences(Context context) {
    	sPreference = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }
    
    public static void putString(String key, String value) {
    	sPreference.edit().putString(key, value).commit();
    }
    
    public static void putBoolean(String key, boolean value) {
    	
//    	if(Constants.DEBUG_KEEP_APP_FIRST_LAUNCH && key != null && 
//    			(key.startsWith("GUIDE_") || Constants.PREF_IS_FIRST_APP_LAUNCH.equals(key)
//    					|| Constants.PREF_SAMPLE_QRCODE_USED.equals(key))) {
//    		sTmpGuideState.put(key, value);
//    		return;
//    	}
    	
    	sPreference.edit().putBoolean(key, value).commit();
    }

    public static void putInt(String key, int value) {
    	sPreference.edit().putInt(key, value).commit();
    }

    public static void putLong(String key, long value) {
    	sPreference.edit().putLong(key, value).commit();
    }

    public static String getString(String key, String defaultValue) {
    	return sPreference.getString(key, defaultValue);
    }
    
    public static boolean getBoolean(String key, boolean defaultValue) {
//    	if(Constants.DEBUG_KEEP_APP_FIRST_LAUNCH && key != null && 
//    			(key.startsWith("GUIDE_") || Constants.PREF_IS_FIRST_APP_LAUNCH.equals(key)
//    					|| Constants.PREF_SAMPLE_QRCODE_USED.equals(key))) {
//    		Boolean val = sTmpGuideState.get(key);
//    		if(val == null)
//    			return defaultValue;
//    		return val;
//    	}
    	return sPreference.getBoolean(key, defaultValue);
    }
    
    public static int getInt(String key, int defaultValue) {
    	return sPreference.getInt(key, defaultValue);
    }
    
    public static long getLong(String key, long defaultValue) {
    	return sPreference.getLong(key, defaultValue);
    }
    
    public static boolean remove(String key) {
    	return sPreference.edit().remove(key).commit();
    }
}