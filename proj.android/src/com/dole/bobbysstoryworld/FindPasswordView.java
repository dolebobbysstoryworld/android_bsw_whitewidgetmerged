package com.dole.bobbysstoryworld;

import java.util.HashMap;
import java.util.Map;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.dole.bobbysstoryworld.server.FindPassword;
import com.dole.bobbysstoryworld.server.ServerTask;
import com.dole.bobbysstoryworld.server.ServerTask.IServerResponseCallback;
import com.dole.bobbysstoryworld.ui.CustomDialog;
import com.dole.bobbysstoryworld.ui.FontTextView;

public class FindPasswordView extends RelativeLayout implements IServerResponseCallback {

	private FindPassword mFindPassword;
	private String mEmailId;
	private ProgressDialog mProgressDialog;

	public FindPasswordView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public FindPasswordView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public FindPasswordView(Context context) {
		this(context, null);
	}

	@Override
	public void onFinishInflate() {
		super.onFinishInflate();
		
		final View sendBtn = findViewById(R.id.find_password_send_btn);
		final TextView email = (TextView)findViewById(R.id.find_password_edit_email);
		
		String userId = BobbysStoryWorldPreference.getString(Constants.PREF_USER_ID, "");
		if(!userId.equals("")) {
			email.setText(userId);
		}
		
		sendBtn.setEnabled(false);
		sendBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Util.hideSoftInput(getContext(), v);
				requestPassword();
			}
		});

		email.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				FontTextView errorView = (FontTextView)findViewById(R.id.find_password_edit_email_error);
				errorView.setVisibility(View.GONE);
				String st = s.toString();
				if(!Util.checkEmail(st))
					sendBtn.setEnabled(false);
				else
					sendBtn.setEnabled(true);
			}
		});
		
		email.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				FontTextView errorView = (FontTextView)findViewById(R.id.find_password_edit_email_error);
				errorView.setVisibility(View.GONE);
				return false;
			}
		});
		
		email.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId != EditorInfo.IME_NULL || (actionId == EditorInfo.IME_NULL && event != null && event.getAction() == KeyEvent.ACTION_DOWN)) {
					String emailAdd = email.getText().toString();
					if(!Util.checkEmail(emailAdd)) {
						FontTextView errorView = (FontTextView)findViewById(R.id.find_password_edit_email_error);
						errorView.setText(R.string.pf_net_error_email_enter_full_address);
						errorView.setVisibility(View.VISIBLE);
						return true;
					}
				}
				return false;
			}
		});
		
		setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent e) {
				Util.hideSoftInput(getContext(), v);
			    email.clearFocus();
				return false;
			}
		});
	}

	private void requestPassword() {
		final TextView emailView = (TextView)findViewById(R.id.find_password_edit_email);
		
		String emailAdd = emailView.getText().toString();
		
		if(!Util.checkEmail(emailAdd)) {
			FontTextView errorView = (FontTextView)findViewById(R.id.find_password_edit_email_error);
			errorView.setText(R.string.pf_net_error_email_enter_full_address);
			errorView.setVisibility(View.VISIBLE);
			return;
		}
	    
		mEmailId = emailView.getText().toString();
		HashMap<String, String> request = new HashMap<String, String>();
		request.put(Constants.TAG_USER_ID, mEmailId);
		request.put(Constants.TAG_EMAIL, mEmailId);
		request.put(Constants.TAG_CLIENT_IP, Util.getIp(getContext()));
		
		mFindPassword = new FindPassword(getContext(), request);
		mFindPassword.setCallback(FindPasswordView.this);
		Model.runOnWorkerThread(mFindPassword);

		mProgressDialog = Util.openProgressDialog(getContext());
	}

	@Override
	public void onSuccess(ServerTask parser, Map<String, String> result) {
		Bundle data = ((PortraitActivity)getContext()).getSavedDataBundle();
		if(data == null)
    		data = new Bundle();
		data.putString(LoginView.FORGOT_EMAIL_ID, mEmailId);
		
		mProgressDialog.dismiss();
		((PortraitActivity)getContext()).onBackPressed();
		
		Bundle args = new Bundle();
		args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
		args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.pf_forgot_popup_confirm_password);
		args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
		args.putBoolean(Constants.CUSTOM_DIALOG_ALLOW_CANCEL, true);
		
		CustomDialog dialog = new CustomDialog(getContext(), args, null);
		dialog.show();
		
	}

	@Override
	public void onFailed(ServerTask parser, Map<String, String> result, int returnCode) {
		if(Util.isEmailError(returnCode)) {
			FontTextView errorView = (FontTextView)findViewById(R.id.find_password_edit_email_error);
			errorView.setText(Util.switchErrorCode(returnCode, getResources()));
			errorView.setVisibility(View.VISIBLE);
		}
		else {
		Toast.makeText(getContext(), Util.switchErrorCode(returnCode, getResources()), Toast.LENGTH_SHORT).show();
		}
		mProgressDialog.dismiss();
	}

//	@Override
//	public void onActivityResult(int requestCode, int resultCode, Intent data) {
//		super.onActivityResult(requestCode, resultCode, data);
//		
//		switch (requestCode) {
//		case Constants.REQUEST_USER_CONFIRM:
//			if(resultCode == Activity.RESULT_OK) {
//				final Fragment target = getTargetFragment();
//				if(target != null) {
//					Intent retData = new Intent();
//					retData.putExtra(Constants.EMAIL, mEmailId);
//					target.onActivityResult(Constants.REQUEST_FORGOTTEN_PASS_EMAIL, resultCode, retData);
//				}
//				getFragmentManager().popBackStack();
//			}
//			break;
//
//		default:
//			break;
//		}
//	}
}
