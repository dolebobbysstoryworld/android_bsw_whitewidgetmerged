package com.dole.bobbysstoryworld.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import com.dole.bobbysstoryworld.Constants;
import com.dole.bobbysstoryworld.R;

public abstract class ServerTask implements Runnable, Future<Map<String, String>> {
	protected static final String TAG = ServerTask.class.toString();
    public static final int RESULT_ERROR_NETWORK = -1;

    protected Context mContext;
    protected Map<String, String> mRequest;
    protected Map<String, String> mResult;
    protected HttpResponse mResponse;
    private String mUrl;
    protected String mApiDetail;
        
    public interface IServerResponseCallback {
    	public void onSuccess(ServerTask parser, Map<String, String> result);
    	public void onFailed(ServerTask parser, Map<String, String> result, int returnCode);
    }
    
    private IServerResponseCallback mCallback;
    private boolean mIsCancelled = false;
    private boolean mIsDone = false;

    private Handler mHandler;
    
    public ServerTask(Context context, Map<String, String> request) {
            mContext = context;
            mHandler = new Handler(Looper.getMainLooper());
            mRequest = request;
            mApiDetail = applyApiDetail();
            
            if(Constants.USE_STAGING_SERVER)
            	mUrl = context.getString(R.string.api_staging_main_url);
            else
            	mUrl = context.getString(R.string.api_main_url);
    }

    public Context getContext() {
            return mContext;
    }
    
    abstract protected String applyApiDetail();

    @Override
    public final void run() {
            if(mIsCancelled)
                    return;

            onPreExecute();
            //Main work
            int errorNo = 0;
            try {
            	android.util.Log.d(TAG, " before request mUrl = " + mUrl + mApiDetail + " req = " + mRequest);
            	
            	if (mRequest.get(Constants.TAG_DEVICE_TOKEN)==""||mRequest.get(Constants.TAG_DEVICE_TOKEN)==null) {
            		mRequest.put(Constants.TAG_DEVICE_TOKEN, "NO_DEVICE_TOKEN");
				}
            	
				mResponse = ServerIF.postRequestServer(mUrl + mApiDetail , mRequest);
				
				android.util.Log.d(TAG, " res = " + mResponse);
				
				final StringBuffer result = new StringBuffer();
				BufferedReader rd = new BufferedReader(new InputStreamReader(mResponse.getEntity().getContent()));
				String line = "";
				while ((line = rd.readLine()) != null) {
					result.append(line);
				}
	            mResult = parseResponse(result.toString(), mCallback);
            } catch (IllegalStateException e) {
            	android.util.Log.d(TAG, "IllegalStateException Error getting response from server occurred", e);
            	errorNo = RESULT_ERROR_NETWORK;
            } catch (ClientProtocolException e) {
            	android.util.Log.d(TAG, "ClientProtocolException Error getting response from server occurred", e);
            	errorNo = RESULT_ERROR_NETWORK;
    		} catch (IOException e) {
    			errorNo = RESULT_ERROR_NETWORK;
    			android.util.Log.d(TAG, "IOException Error getting response from server occurred", e);
            } finally {
            	finish(errorNo);
            }

            mIsDone = true;
    }

    protected void onPreExecute() {
    }

    protected void onPostExecute() {
    }
    
    public void finish(final int errorNo) {
    	mHandler.post(new Runnable() {
    		@Override
    		public void run() {
    			onPostExecute();
    			
    			boolean returnVal;
    			int returnCode;
    			try {
    				android.util.Log.d(TAG, "raw val = " + mResult.get(Constants.TAG_RETURN));
    				returnVal = Boolean.valueOf(mResult.get(Constants.TAG_RETURN));
    				returnCode = Integer.valueOf(mResult.get(Constants.TAG_RETURN_CODE));
    				android.util.Log.d(TAG, "returnVal = " + returnVal);
    				android.util.Log.d(TAG, "returnCode = " + returnCode);
    			} catch (Exception e) {
    				returnVal = false;
    				returnCode = -1;
    			}
    			
    			android.util.Log.d(TAG, "text");
    			
    			if(mCallback != null && mResponse != null) {
    				android.util.Log.d(TAG, "ret val = " + returnVal);
    				android.util.Log.d(TAG, "ret code = " + returnCode);
    				StatusLine status = mResponse.getStatusLine();
    				if(!returnVal || status.getStatusCode() != HttpStatus.SC_OK || mIsCancelled) {
//    					Toast.makeText(getContext(), "onFail " + applyApiDetail(), Toast.LENGTH_LONG).show();
    					mCallback.onFailed(ServerTask.this, mResult, returnCode);
    				} else {
//    					Toast.makeText(getContext(), "onSuccess " + applyApiDetail(), Toast.LENGTH_LONG).show();
    					mCallback.onSuccess(ServerTask.this, mResult);
    				}
    			} else {
//    				Toast.makeText(getContext(), "onFail " + applyApiDetail(), Toast.LENGTH_LONG).show();
    				mCallback.onFailed(ServerTask.this, mResult, errorNo);
    			}
    		}
    	});
    }
    
    @Override
    public boolean isCancelled() {
            return mIsCancelled;
    }

    @Override
    public boolean isDone() {
            return mIsDone;
    }
    
    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
            mIsCancelled = true;
            return mIsCancelled;
    }

    @Override
    public Map<String, String> get() throws InterruptedException, ExecutionException {
    	return mResult;
    }

    @Override
    public Map<String, String> get(long timeout, TimeUnit unit) throws InterruptedException,
                    ExecutionException, TimeoutException {
    	return mResult;
    }

    
    abstract protected Map<String, String> parseResponse(String jsonResult,
            IServerResponseCallback callback);

    public void setCallback(IServerResponseCallback callback) {
    	mCallback = callback;
    }
}