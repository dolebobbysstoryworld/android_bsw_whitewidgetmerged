package com.dole.bobbysstoryworld.server;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.content.Context;
import android.os.Looper;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.dole.bobbysstoryworld.Constants;

public class ServerIF {
	private static final String TAG = "ServerIF";
	private static String sUserAgent = null;

	public static HttpResponse postRequestServer(String url, Map<String, String> requestParam) 
			throws ClientProtocolException, IOException, IllegalStateException {
		final HttpClient client = new DefaultHttpClient();

		HttpPost httpPost = new HttpPost(url);

		try {
			JSONObject json = new JSONObject(requestParam);
			if(Constants.DEBUG) android.util.Log.d(TAG, " json req = " + json);
			StringEntity se = new StringEntity(json.toString(), "UTF-8"); 
			//se.setContentEncoding("UTF-8");
			se.setContentType("application/json");
			//se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			httpPost.setEntity(se); 
		} catch (UnsupportedEncodingException e) {
			android.util.Log.d(TAG, " Unsupported encoding error occurred", e);
		}

		return client.execute(httpPost);
	}

	public static String getUserAgent(final Context context) {
		if(null != sUserAgent)
			return sUserAgent;

		try {
			Constructor<WebSettings> constructor = WebSettings.class.getDeclaredConstructor(Context.class, WebView.class);
			constructor.setAccessible(true);
			try {
				WebView wv = new WebView(context);
				sUserAgent = wv.getSettings().getUserAgentString();
				return sUserAgent;
			} finally {
				constructor.setAccessible(false);
			}
		} catch (Exception e) {
			if(Thread.currentThread().getName().equalsIgnoreCase("main")){
				WebView webview = new WebView(context);
				sUserAgent = webview.getSettings().getUserAgentString();
			}else{
				Thread thread = new Thread(){
					public void run(){
						Looper.prepare();
						WebView m_webview = new WebView(context);
						sUserAgent = m_webview.getSettings().getUserAgentString();
						Looper.loop();
					}
				};
				thread.start();
				return sUserAgent;
			}
			return sUserAgent;
		}
	}
}