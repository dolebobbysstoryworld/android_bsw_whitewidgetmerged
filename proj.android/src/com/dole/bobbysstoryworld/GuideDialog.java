package com.dole.bobbysstoryworld;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import org.cocos2dx.lib.Cocos2dxHelper;

import android.app.ActionBar.LayoutParams;
import android.app.Dialog;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.vending.expansion.zipfile.APKExpansionSupport;
import com.dole.bobbysstoryworld.ui.FontTextHelper;

public class GuideDialog extends Dialog {
	private static final String TAG = GuideDialog.class.getName();
	
	public static final String MODE_GUIDE = "mode_guide";
	public static final String ITEM_COUNT = "item_count";
	public static final String ITEM_IDS = "item_ids";
	public static final String ITEM_LEFTS = "item_lefts";
	public static final String ITEM_TOPS = "item_tops";
	
	private GuideMode mMode = GuideMode.GUIDE_LOGIN;
	private SoundPool mSoundPool = null;
	private Context mContexet;
	private int mSoundId = -1;
	
	enum GuideMode {
		GUIDE_CREATE_CHARACTER(R.layout.guide_character, "10_create.mp3"),
		GUIDE_CREATE_BG(R.layout.guide_character, "10_create.mp3"),
		GUIDE_IMPORT_BG(R.layout.guide_character, "10_create.mp3"),
		GUIDE_LOGIN(R.layout.guide_login, "01_login.mp3"); 
		
		int layoutId;
		String soundName;
		private GuideMode(int layoutId, String soundName) {
			this.layoutId = layoutId;
			this.soundName = soundName;
		}
	}
	
	private int mArgsCount = 0;
	private int[] mPosIDAry;
	private int[] mPosXAry;
	private int[] mPosYAry;

	private Bundle mArg;

	public GuideDialog(Context context, Bundle arg) {
		super(context);
		mContexet = context;
		mArg = arg;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		if(mArg != null) {
			mMode = GuideMode.valueOf(mArg.getString(MODE_GUIDE));
			
			BobbysStoryWorldPreference.putBoolean(mMode.name(), true);
		}
		setContentView(mMode.layoutId);

		getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        String lang = getContext().getResources().getConfiguration().locale.getLanguage();
        if(!lang.equals(Locale.KOREAN.getLanguage()) && !lang.equals(Locale.JAPANESE.getLanguage()))
        	lang = Locale.ENGLISH.getLanguage();
        final String soundPath = "common/guide_sounds/" + lang + File.separator + mMode.soundName;
		mSoundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        
		if(Constants.DEBUG) {
            AssetFileDescriptor afd;
		try {
    			afd = getContext().getAssets().openFd(soundPath);
			mSoundId = mSoundPool.load(afd, 1);
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
        } else {
        	final String prefix = APKExpansionSupport.getAPKExpansionPath(getContext());
			mSoundId = mSoundPool.load(prefix + File.separator + soundPath, 1);
        }

		mSoundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() {
			@Override
			public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
				soundPool.play(mSoundId, 1f, 1f, 1, 0, 1f);
			}
		});
        
        if(mArg != null) {
			mMode = GuideMode.valueOf(mArg.getString(MODE_GUIDE));
			
			mArgsCount = mArg.getInt(GuideDialog.ITEM_COUNT);
			mPosIDAry = mArg.getIntArray(GuideDialog.ITEM_IDS);
			mPosXAry = mArg.getIntArray(GuideDialog.ITEM_LEFTS);
			mPosYAry = mArg.getIntArray(GuideDialog.ITEM_TOPS);
			
			for(int i = 0; i< mArgsCount; i++) {
				final int id = mPosIDAry[i];
				final View child = findViewById(id);
				if(child != null) {
					final RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) child.getLayoutParams();
					if(mPosXAry[i] != -1)
						param.leftMargin = mPosXAry[i];
					if(mPosYAry[i] != -1)
						param.topMargin = mPosYAry[i];
					child.setLayoutParams(param);
					child.requestLayout();
				}
			}
		}
        
        View textView = findViewById(R.id.guide_text);
        if(textView instanceof TextView) {
			final TextView guideText = (TextView) textView;
			FontTextHelper.setGuideFont(guideText.getContext(), guideText);
		}
        
        if(mContexet instanceof PortraitActivity) {
        	Cocos2dxHelper.runOnGLThread(new Runnable() {
    			@Override
    			public void run() {
    				PortraitActivity activity = (PortraitActivity)mContexet;
    				activity.nativePauseBGM();
    			}
    		});
        }
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		dismiss();
		return true;
	}
	
	@Override
	public void dismiss() {
		if(mSoundPool != null) {
			mSoundPool.stop(mSoundId);
			mSoundPool.release();
		}
        if(mContexet instanceof PortraitActivity) {
        	Cocos2dxHelper.runOnGLThread(new Runnable() {
    			@Override
    			public void run() {
    				PortraitActivity activity = (PortraitActivity)mContexet;
    	        	activity.nativeResumeBGM();
    			}
    		});
        }
		super.dismiss();
	}
}
