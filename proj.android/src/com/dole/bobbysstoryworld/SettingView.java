package com.dole.bobbysstoryworld;

import org.cocos2dx.lib.Cocos2dxHelper;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.dole.bobbysstoryworld.ui.CustomDialog.CustomDialogButtonClickListener;
//import com.facebook.Session;
//import com.facebook.SessionState;
//import com.facebook.Session.NewPermissionsRequest;

public class SettingView extends RelativeLayout {
	private static final String TAG = SettingView.class.getName();
	
	private boolean mIsLoggedIn = false;
//	private RequestAuthSessionStatusCallback mSessionCallback = new RequestAuthSessionStatusCallback();
	
//	public static String BACKUP_DIR = "BobbysJourney";
//	public static String BACKUP_FILE_NAME = "bobbys_journey_backup_";
	
//	private static final List<String> mFriendsPermission = new ArrayList<String>() {
//        {
//            add("user_friends");
//            add("public_profile");
//        }
//    };

	public SettingView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public SettingView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public SettingView(Context context) {
		this(context, null);
	}

	@Override
	public void onFinishInflate() {
		super.onFinishInflate();
		
		mIsLoggedIn = BobbysStoryWorldPreference.getString(Constants.PREF_USER_ID, "").equals("") ? false : true;
		
		Resources res = getResources();
		final Button login = (Button)findViewById(R.id.setting_btn_login);
		final Button usage = (Button)findViewById(R.id.setting_btn_usage);
		final View leave = findViewById(R.id.setting_leave);
		View help = findViewById(R.id.setting_help);
		View about = findViewById(R.id.setting_about);
		TextView emailText = (TextView)findViewById(R.id.setting_id_text);
//		Button facebookInviteButton = (Button)findViewById(R.id.setting_invite_facebook);
//		
//		facebookInviteButton.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View view) {
//				Session session = Session.getActiveSession();
//				if (session == null) {
//					session = new Session(getContext());
//					Session.setActiveSession(session);
//				}
//				
//				if(ensureOpenSession()) {
//					requestFriendsList(session);
//				}
//			}
//		});
		
		if(mIsLoggedIn) {
			emailText.setText(BobbysStoryWorldPreference.getString(Constants.PREF_USER_ID, ""));
			if(BobbysStoryWorldPreference.getInt(Constants.PREF_LOGIN_TYPE, -1) == Constants.LOGIN_TYPE_FACEBOOK) {
				Drawable facebook = res.getDrawable(R.drawable.login_via_facebook);
				int imgSize = res.getDimensionPixelSize(R.dimen.login_info_text_drawable_size);
				facebook.setBounds(0, 0, imgSize, imgSize);
				emailText.setCompoundDrawables(facebook, null, null, null);
				emailText.setCompoundDrawablePadding(res.getDimensionPixelSize(R.dimen.setting_id_drawable_padding));
			}
			int textSize = res.getDimensionPixelSize(R.dimen.setting_id_text_size);
			emailText.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
			emailText.setTypeface(Typeface.DEFAULT_BOLD);
			leave.setOnClickListener(mViewOnclickListener);
			usage.setOnClickListener(mViewOnclickListener);
			leave.setVisibility(View.VISIBLE);
			usage.setVisibility(View.VISIBLE);
			login.setText(R.string.pf_setting_button_title_edit);
		}
		
		final boolean isBgmToggled = BobbysStoryWorldPreference.getBoolean(Constants.PREF_IS_BGM_ON, true);
		
		final Switch bgmOnOff = (Switch)findViewById(R.id.setting_toggle_switch);
		bgmOnOff.setChecked(isBgmToggled);
		bgmOnOff.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
				BobbysStoryWorldPreference.putBoolean(Constants.PREF_IS_BGM_ON, isChecked);
//				if(isChecked) {
					Cocos2dxHelper.runOnGLThread(new Runnable() {
						@Override
						public void run() {
							((PortraitActivity)getContext()).nativeChangeBGMPlayStatus(!isChecked ? 1 : 0);
							((PortraitActivity)getContext()).nativeStopBGM();
							((PortraitActivity)getContext()).nativePlayBGM();
						}
					});
//				}
			}
		});
		
		about.setOnClickListener(mViewOnclickListener);
		login.setOnClickListener(mViewOnclickListener);
		help.setOnClickListener(mViewOnclickListener);
	}
	
	private OnClickListener mViewOnclickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			
			final int id = view.getId();
			switch (id) {
			case R.id.setting_btn_login:
				if(mIsLoggedIn)	{
					Bundle data = ((PortraitActivity)getContext()).getSavedDataBundle();
					data.putInt(UserDetailView.EXTRA_USER_DETAIL_MODE_NAME,
							BobbysStoryWorldPreference.getInt(Constants.PREF_LOGIN_TYPE, Constants.LOGIN_TYPE_EMAIL));
					((PortraitActivity)getContext()).showMenu(PortraitActivity.ENTRY_MENU_SIGNUP);
				} else {
					((PortraitActivity)getContext()).showMenu(PortraitActivity.ENTRY_MENU_LOGIN);
				}
				break;
			case R.id.setting_about:
//				Bundle args = new Bundle();
//				args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
//				args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.ask_for_unregistration);
//				args.putInt(Constants.CUSTOM_DIALOG_LEFT_BUTTON_RES, R.drawable.popup_cancel_btn);
//				args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
//				args.putBoolean(Constants.CUSTOM_DIALOG_ALLOW_CANCEL, true);
//				
//				CustomDialog dialog = new CustomDialog(getContext(), args, mDialogClickListener);
//				dialog.show();
				((PortraitActivity)getContext()).showMenu(PortraitActivity.ENTRY_MENU_ABOUT);
				break;
			case R.id.setting_help:
				((PortraitActivity)getContext()).showMenu(PortraitActivity.ENTRY_MENU_HELP);
				break;
			case R.id.setting_leave:
				((PortraitActivity)getContext()).showMenu(PortraitActivity.ENTRY_MENU_LEAVE);
				break;
			case R.id.setting_btn_usage:
				((PortraitActivity)getContext()).showMenu(PortraitActivity.ENTRY_MENU_DOLE_COIN_USAGE);
				break;
			default:
				break;
			}
		}
	};
	
//	private boolean ensureOpenSession() {
//		android.util.Log.e(TAG, "ensureOpenSession:");
//        if (Session.getActiveSession() == null ||
//                !Session.getActiveSession().isOpened()) {
//            Session.openActiveSession(
//            		(PortraitActivity)getContext(),
//                    true, 
//                    Arrays.asList("user_friends"),
//                    mSessionCallback);
//            return false;
//        }
//        return true;
//    }
//	
//	private void onSessionStateChanged(Session session, SessionState state, Exception exception) {
//		 if (state.isOpened() && !sessionHasNecessaryPerms(session)) {
//			 //Session.openActiveSessionFromCache(getActivity());
//			 session.requestNewReadPermissions(
//                     new NewPermissionsRequest(
//                    		 (PortraitActivity)getContext(), 
//                             getMissingPermissions(session)));
//		 } else if(state.isOpened()){
//			 requestFriendsList(session);
//		 }
//	}
//	
//	private void requestFriendsList(final Session session) {
//		((PortraitActivity)getContext()).showMenu(PortraitActivity.ENTRY_MENU_INVITE_FACEBOOK);
//	}
//	
//	private boolean sessionHasNecessaryPerms(Session session) {
//        if (session != null && session.getPermissions() != null) {
//            for (String requestedPerm : mFriendsPermission) {
//                if (!session.getPermissions().contains(requestedPerm)) {
//                    return false;
//                }
//            }
//            return true;
//        }
//        return false;
//    }
//	
//	private List<String> getMissingPermissions(Session session) {
//        List<String> missingPerms = new ArrayList<String>(mFriendsPermission);
//        if (session != null && session.getPermissions() != null) {
//            for (String requestedPerm : mFriendsPermission) {
//                if (session.getPermissions().contains(requestedPerm)) {
//                    missingPerms.remove(requestedPerm);
//                }
//            }
//        }
//        return missingPerms;
//    }
//	
//	private class RequestAuthSessionStatusCallback implements Session.StatusCallback {
//        @Override
//        public void call(Session session, SessionState state, Exception exception) {
//        	onSessionStateChanged(session, state, exception);
//
//        	if(state.isOpened() && sessionHasNecessaryPerms(session))
//        		session.removeCallback(this);
//        }
//    }
	
	public CustomDialogButtonClickListener mDialogClickListener = new CustomDialogButtonClickListener() {
		@Override
		public void onClickOK(int requestCode) {
			Toast.makeText(getContext(), "click ok", Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onClickCancel(int requestCode) {
			Toast.makeText(getContext(), "click cancel", Toast.LENGTH_SHORT).show();
			
		}
	};
	
//	@Override
//	public void onActivityResult(int requestCode, int resultCode, Intent data) {
//		super.onActivityResult(requestCode, resultCode, data);
//
//		DLog.d(TAG, " requestCode = " + requestCode + " resultCode = " + resultCode + " data = " + data);
//		switch (requestCode) {
//		case Session.DEFAULT_AUTHORIZE_ACTIVITY_CODE:
//			DLog.d(TAG, " Return from facebook login activity");
//            Session.getActiveSession().onActivityResult(getActivity(), requestCode, resultCode, data);
//            break;
//		case Constants.REQUEST_DIALOG_BACKUP_RESTORE: {
//			if(resultCode != Activity.RESULT_OK)
//				return;
//			
//			if(data == null)
//				return;
//			
//			int select = data.getIntExtra(BackupDialog.KEY_SELECT, -1);
//			String fileExist = checkBackupFile();
//			
//			if(select == BackupDialog.SELECT_BACKUP) {
//				if(fileExist != null) {
//					Bundle args = new Bundle();
//					args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
//					args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.saved_file_exist);
//					args.putInt(Constants.CUSTOM_DIALOG_LEFT_BUTTON_RES, R.drawable.popup_cancel_btn);
//					args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
//					
//					Util.showDialogFragment(
//							getFragmentManager(), 
//							new CustomDialog(), 
//							Constants.TAG_DIALOG_CUSTOM, 
//							args, 
//							SettingView.this, 
//							Constants.REQUEST_CONFIRM_BACKUP);
//				}
//				else {
//					backup();
//				}
//			}
//			else if(select == BackupDialog.SELECT_RESTORE) {
//				if(fileExist != null) {
//					Bundle args = new Bundle();
//					args.putInt(BackupDialog.KEY_MODE, BackupDialog.MODE_CHOOSE_RESTORE);
//					args.putLong(BackupDialog.KEY_FILE, Long.valueOf(fileExist));
//					Util.showDialogFragment(
//							getFragmentManager(), 
//							new BackupDialog(), 
//							Constants.TAG_DIALOG_BACKUP, 
//							args, 
//							SettingView.this, 
//							Constants.REQUEST_DIALOG_CHOOSE_RESTORE);
//				}
//				else {
//					Bundle args = new Bundle();
//					args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
//					args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.no_saved_file);
//					args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
//					
//					Util.showDialogFragment(
//							getFragmentManager(), 
//							new CustomDialog(), 
//							Constants.TAG_DIALOG_CUSTOM, 
//							args);
//				}
//			}
//			break;
//		}
//		case Constants.REQUEST_DIALOG_CHOOSE_RESTORE: {
//			if(resultCode != Activity.RESULT_OK)
//				return;
//			
//			Bundle args = new Bundle();
//			args.putInt(Constants.CUSTOM_DIALOG_IMG_RES, R.drawable.popup_warning);
//			args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES, R.string.replase_saved_file);
//			args.putInt(Constants.CUSTOM_DIALOG_LEFT_BUTTON_RES, R.drawable.popup_cancel_btn);
//			args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES, R.drawable.popup_ok_btn);
//			
//			Util.showDialogFragment(
//					getFragmentManager(), 
//					new CustomDialog(), 
//					Constants.TAG_DIALOG_CUSTOM, 
//					args, 
//					SettingView.this, 
//					Constants.REQUEST_CONFIRM_RESTORE);
//			break;
//		}
//		case Constants.REQUEST_CONFIRM_BACKUP:
//			if(resultCode != Activity.RESULT_OK)
//				return;
//			
//			deleteFile();
//			backup();
//			break;
//		case Constants.REQUEST_CONFIRM_RESTORE:
//			if(resultCode != Activity.RESULT_OK)
//				return;
//
//			restore();
//			break;
//		case Constants.REQUEST_CONFIRM_RESTORE_COMPLETE:
//			ContentResolver resolver = getActivity().getContentResolver();
//			ContentProviderClient client = resolver.acquireContentProviderClient(HeightChartProvider.AUTHORITY);
//			HeightChartProvider provider = (HeightChartProvider)client.getLocalContentProvider();
//			provider.resetDatabase();
//			client.release();
//			
//			((ApplicationImpl)getActivity().getApplicationContext()).reloadDB();
//			
//			LocalBroadcastManager lbMan = LocalBroadcastManager.getInstance(getActivity().getApplication());
//			IntentFilter filter = new IntentFilter();
//			filter.addAction(Constants.ACTION_LOAD_FINISHED);
//			mDbLoadFinishReceiver = new DbLoadFinishReceiver();
//			lbMan.registerReceiver(mDbLoadFinishReceiver, filter);
//			break;
//		default:
//			break;
//		}
//	}
	
//	private DbLoadFinishReceiver mDbLoadFinishReceiver;
//	private class DbLoadFinishReceiver extends BroadcastReceiver {
//
//		@Override
//		public void onReceive(Context context, Intent intent) {
//			final String action = intent.getAction();
//			DLog.e(TAG, "onReceive Action : " + action);
//			if(Constants.ACTION_LOAD_FINISHED.equals(action)) {
//				Intent newActivity = new Intent(getActivity(), MainActivity.class);
//				getActivity().startActivity(newActivity);
//				getActivity().finish();
//				
//				if(mDbLoadFinishReceiver != null) {
//					LocalBroadcastManager lbMan = LocalBroadcastManager.getInstance(getActivity().getApplication());
//					lbMan.unregisterReceiver(mDbLoadFinishReceiver);
//					mDbLoadFinishReceiver = null;
//				}
//			}
//		}
//	}
//	
//	@Override
//	public void onDestroy() {
//		if(mDbLoadFinishReceiver != null) {
//			LocalBroadcastManager lbMan = LocalBroadcastManager.getInstance(getActivity().getApplication());
//			lbMan.unregisterReceiver(mDbLoadFinishReceiver);
//			mDbLoadFinishReceiver = null;
//		}
//		super.onDestroy();
//	}
}
