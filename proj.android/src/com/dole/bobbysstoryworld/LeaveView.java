package com.dole.bobbysstoryworld;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.dole.bobbysstoryworld.server.DisconnectSNS;
import com.dole.bobbysstoryworld.server.RequestNewAuthKey;
import com.dole.bobbysstoryworld.server.ServerTask;
import com.dole.bobbysstoryworld.server.ServerTask.IServerResponseCallback;
import com.dole.bobbysstoryworld.server.WithdrawUser;
import com.dole.bobbysstoryworld.ui.FontEditText;
import com.dole.bobbysstoryworld.ui.FontTextHelper;
import com.dole.bobbysstoryworld.ui.FontTextView;
import com.facebook.Session;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

public class LeaveView extends RelativeLayout implements IServerResponseCallback {
	
	private static final String TAG = LeaveView.class.getName();
	
	private WithdrawUser mWithdrawUser;
	private DisconnectSNS mDisconnectSNS;
	private boolean mIsPasswordFill;
	private ProgressDialog mProgressDialog;

	public LeaveView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public LeaveView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public LeaveView(Context context) {
		this(context, null);
	}
	
	@Override
	public void onFinishInflate() {
		super.onFinishInflate();
		
		final Button send = (Button)findViewById(R.id.delete_account_send);
		final CheckBox inquiry = (CheckBox)findViewById(R.id.delete_account_inquiry);
		ListView list = (ListView)findViewById(R.id.delete_account_list);
		final FontEditText passwordView = (FontEditText)findViewById(R.id.delete_account_pwd);
		View passwordFrame = findViewById(R.id.delete_account_pwd_frame);
		final int loginType = BobbysStoryWorldPreference.getInt(Constants.PREF_LOGIN_TYPE, Constants.LOGIN_TYPE_EMAIL);
		
		if(loginType == Constants.LOGIN_TYPE_EMAIL) {
		passwordView.setEnabled(false);
		
		passwordView.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				FontTextView errorView = (FontTextView)findViewById(R.id.delete_account_pwd_error);
				errorView.setVisibility(View.GONE);
				String st = s.toString();
				if(st.trim().equals(""))
					mIsPasswordFill = false;
				else
					mIsPasswordFill = true;

				if(mIsPasswordFill && inquiry.isChecked())
					send.setEnabled(true);
				else
					send.setEnabled(false);
			}
		});
		
		passwordView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				FontTextView errorView = (FontTextView)findViewById(R.id.delete_account_pwd_error);
				errorView.setVisibility(View.GONE);
				return false;
			}
		});
		
			passwordView.setOnImeHideListener(new FontEditText.ImeHideListener() {
				@Override
				public void onImeHide() {
					String password = passwordView.getText().toString();
					final int len = password.length();
					if(len > 0 && len < Constants.PASSWORD_MIN_LENGTH) {
						FontTextView errorView = (FontTextView)findViewById(R.id.delete_account_pwd_error);
						errorView.setText(R.string.pf_net_error_password_short);
						errorView.setVisibility(View.VISIBLE);
					}
				}
			});
			
			passwordView.setOnFocusChangeListener(new OnFocusChangeListener() {
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if(!hasFocus) {
						String password = passwordView.getText().toString();
						final int len = password.length();
						if(len > 0 && len < Constants.PASSWORD_MIN_LENGTH) {
							FontTextView errorView = (FontTextView)findViewById(R.id.delete_account_pwd_error);
							errorView.setText(R.string.pf_net_error_password_short);
							errorView.setVisibility(View.VISIBLE);
						}
					}
				}
			});
			
		passwordView.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId != EditorInfo.IME_NULL || (actionId == EditorInfo.IME_NULL && event != null && event.getAction() == KeyEvent.ACTION_DOWN)) {
					String password = passwordView.getText().toString();
					final int len = password.length();
					if(len > 0 && len < Constants.PASSWORD_MIN_LENGTH) {
						FontTextView errorView = (FontTextView)findViewById(R.id.delete_account_pwd_error);
						errorView.setText(R.string.pf_net_error_password_short);
						errorView.setVisibility(View.VISIBLE);
						return true;
					}
				}
				return false;
			}
		});
		
		setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent e) {
				Util.hideSoftInput(getContext(), v);
				passwordView.clearFocus();
				return false;
			}
		});
	}
		else if(loginType == Constants.LOGIN_TYPE_FACEBOOK) {
			passwordFrame.setVisibility(View.INVISIBLE);
		}
		
		send.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				sendDelete();
			}
		});

		send.setEnabled(false);
		inquiry.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					passwordView.setEnabled(true);
					if("".equals(passwordView.getText().toString()) && loginType == Constants.LOGIN_TYPE_EMAIL)
						send.setEnabled(false);
					else
						send.setEnabled(true);
						
				}
				else {
					send.setEnabled(false);
					passwordView.setEnabled(false);
				}
			}
		});
		
		DeleteAccountAdapter adapter = new DeleteAccountAdapter();
		list.setAdapter(adapter);
	}
	
	private void sendDelete() {
		TextView passwordView = (TextView)findViewById(R.id.delete_account_pwd);
		String password = passwordView.getText().toString();
		int loginType = BobbysStoryWorldPreference.getInt(Constants.PREF_LOGIN_TYPE, Constants.LOGIN_TYPE_EMAIL);
		final int len = password.length();
		if(len > 0 && len < Constants.PASSWORD_MIN_LENGTH) {
			FontTextView errorView = (FontTextView)findViewById(R.id.delete_account_pwd_error);
			errorView.setText(R.string.pf_net_error_password_short);
			errorView.setVisibility(View.VISIBLE);
			return;
		}
		
		HashMap<String, String> request = new HashMap<String, String>();
		if(loginType == Constants.LOGIN_TYPE_EMAIL) {
			request.put(Constants.TAG_USER_NO, String.valueOf(BobbysStoryWorldPreference.getInt(Constants.PREF_USER_NO, 0)));
			request.put(Constants.TAG_USER_ID, BobbysStoryWorldPreference.getString(Constants.PREF_USER_ID, ""));
			request.put(Constants.TAG_AUTHKEY, BobbysStoryWorldPreference.getString(Constants.PREF_AUTH_KEY, ""));
			request.put(Constants.TAG_PASSWORD, password);
			request.put(Constants.TAG_REASON, "");
			request.put(Constants.TAG_CLIENT_IP, Util.getIp(getContext()));
			
			mWithdrawUser = new WithdrawUser(getContext(), request);
			mWithdrawUser.setCallback(this);
			Model.runOnWorkerThread(mWithdrawUser);
		}
		else if(loginType == Constants.LOGIN_TYPE_FACEBOOK) {
			request.put(Constants.TAG_USER_NO, String.valueOf(BobbysStoryWorldPreference.getInt(Constants.PREF_USER_NO, 0)));
			request.put(Constants.TAG_AUTHKEY, BobbysStoryWorldPreference.getString(Constants.PREF_AUTH_KEY, ""));
			request.put(Constants.TAG_SNS_CODE, Constants.SNS_CODE);
			request.put(Constants.TAG_SNS_ID, BobbysStoryWorldPreference.getString(Constants.PREF_USER_ID, ""));
			request.put(Constants.TAG_CLIENT_IP, Util.getIp(getContext()));
			
			mDisconnectSNS = new DisconnectSNS(getContext(), request);
			mDisconnectSNS.setCallback(this);
			Model.runOnWorkerThread(mDisconnectSNS);
		}
		if(mProgressDialog == null || !mProgressDialog.isShowing())
		mProgressDialog = Util.openProgressDialog(getContext());
	}

	@Override
	public void onSuccess(ServerTask parser, Map<String, String> result) {
		if(parser instanceof RequestNewAuthKey) {
			BobbysStoryWorldPreference.putString(Constants.PREF_AUTH_KEY, result.get(Constants.TAG_AUTHKEY));
			sendDelete();
		}
		else {
			int loginType = BobbysStoryWorldPreference.getInt(Constants.PREF_LOGIN_TYPE, Constants.LOGIN_TYPE_EMAIL);
			if(loginType == Constants.LOGIN_TYPE_FACEBOOK) {
				Session session = Session.getActiveSession();
				if(session != null) {
					if (!session.isClosed()) {
			            session.closeAndClearTokenInformation();
			        }
				}
				else {
					session = new Session(getContext());
			        Session.setActiveSession(session);
			        session.closeAndClearTokenInformation();
				}
			}
			Util.clearUserInfo();
			if(mProgressDialog != null)
				mProgressDialog.dismiss();
			((PortraitActivity)getContext()).onBackPressed();
			Toast.makeText(getContext(), getResources().getString(R.string.pf_deleteaccount_delete_complete), Toast.LENGTH_LONG).show();
			
			MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(),
					Constants.MIXPANEL_TOKEN);
			mixpanel.track("Deleted Account", null);
			mixpanel.flush();
			
			// JP added post 2.0.0
			JSONObject props = new JSONObject();
			try {
				props.put("User Type", "Anonymous");
				props.put("Email", "");
				mixpanel.registerSuperProperties(props);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();					
			}			
			String uuid = UUID.randomUUID().toString();
			mixpanel.identify(uuid);
			mixpanel.getPeople().identify(mixpanel.getDistinctId());
			mixpanel.getPeople().initPushHandling(Constants.GCM_SENDER_ID);
		}
	}

	@Override
	public void onFailed(ServerTask parser, Map<String, String> result, int returnCode) {
		if((parser instanceof WithdrawUser || parser instanceof DisconnectSNS)
				&& returnCode == Constants.ERROR_CODE_AUTHKEY_EXPIRED) {
			Util.requestNewAuthKey(getContext(), this);
		}
		else if(parser instanceof RequestNewAuthKey) {
			Toast.makeText(getContext(), R.string.error_code_default, Toast.LENGTH_SHORT).show();
			Util.clearUserInfo();
//			((PortraitActivity)getContext()).finishApplication();
			((PortraitActivity)getContext()).nativeClearAuthData();
			((PortraitActivity)getContext()).onBackPressed();
		}
		else {
			if(Util.isPasswordError(returnCode)) {
				FontTextView errorView = (FontTextView)findViewById(R.id.delete_account_pwd_error);
				errorView.setText(Util.switchErrorCode(returnCode, getResources()));
				errorView.setVisibility(View.VISIBLE);
			}
			else {
		Toast.makeText(getContext(), Util.switchErrorCode(returnCode, getResources()), Toast.LENGTH_SHORT).show();
			}
			if(mProgressDialog != null)
		mProgressDialog.dismiss();
	}
	}
	
	private class DeleteAccountItem {
		int resId;
		String service;
		String info;
		boolean titleBold;
		
		public DeleteAccountItem(int r, String s, String i, boolean bold) {
			resId = r;
			service = s;
			info = i;
			titleBold = bold;
		}
	}

	
	private class DeleteAccountAdapter extends BaseAdapter {
		List<DeleteAccountItem> mData;
		
		public DeleteAccountAdapter() {
			//TODO : define at xml
			mData = new ArrayList<DeleteAccountItem>();
			mData.add(new DeleteAccountItem(
					R.drawable.delete_account_image_dole_coin, 
					getResources().getString(R.string.pf_deleteaccount_table_title_dolecoin), 
					getResources().getString(R.string.pf_deleteaccount_table_subtitle_dolecoin),
					false));
			mData.add(new DeleteAccountItem(
					R.drawable.delete_account_image_height_chart, 
					getResources().getString(R.string.height_chart), 
					getResources().getString(R.string.pf_deleteaccount_table_subtitle_bobby) + ", " + getResources().getString(R.string.pf_deleteaccount_table_subtitle_bobby_add),
					false));
			mData.add(new DeleteAccountItem(
					R.drawable.delete_account_image_bobby,
					getResources().getString(R.string.pf_deleteaccount_table_title_bobby), 
					getResources().getString(R.string.pf_deleteaccount_table_subtitle_bobby),
					Locale.getDefault().equals(Locale.JAPAN) ? true : false));
		}
		
		@Override
		public boolean isEnabled(int position) {
			return false;
		}

		@Override
		public int getCount() {
			return mData.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null) {
				LayoutInflater li = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = li.inflate(R.layout.delete_account_item, parent, false);
			}
			View v = convertView;
			ImageView img = (ImageView)v.findViewById(R.id.delete_account_item_image);
			TextView service = (TextView)v.findViewById(R.id.delete_account_item_service);
			TextView info = (TextView)v.findViewById(R.id.delete_account_item_info);

			DeleteAccountItem cur = mData.get(position);
			
			if(cur.titleBold) {
				service.setTypeface(service.getTypeface(), Typeface.BOLD);
				service.setTextSize(TypedValue.COMPLEX_UNIT_PX, getContext().getResources().getDimensionPixelSize(R.dimen.leave_list_text_size_storyworld));
			} else {
				service.setTypeface(service.getTypeface(), Typeface.NORMAL);
				service.setTextSize(TypedValue.COMPLEX_UNIT_PX, getContext().getResources().getDimensionPixelSize(R.dimen.leave_list_text_size));
			}
			FontTextHelper.setFont(getContext(), service);
			
			img.setImageResource(cur.resId);
			service.setText(cur.service);
			info.setText(cur.info);
			return v;
		}
	}
}