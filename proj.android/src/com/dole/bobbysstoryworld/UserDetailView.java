package com.dole.bobbysstoryworld;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.dole.bobbysstoryworld.Constants.GenderType;
import com.dole.bobbysstoryworld.server.GetBalance;
import com.dole.bobbysstoryworld.server.GetMobileProfile;
import com.dole.bobbysstoryworld.server.GetProvinceInfo;
import com.dole.bobbysstoryworld.server.ModifyMobileProfile;
import com.dole.bobbysstoryworld.server.RequestAuthKey;
import com.dole.bobbysstoryworld.server.RequestModifySNSUserProfile;
import com.dole.bobbysstoryworld.server.RequestSNSLogin;
import com.dole.bobbysstoryworld.server.RequestSignUp;
import com.dole.bobbysstoryworld.server.ServerTask;
import com.dole.bobbysstoryworld.server.ServerTask.IServerResponseCallback;
import com.dole.bobbysstoryworld.ui.BirthDayPickerDialog;
import com.dole.bobbysstoryworld.ui.CountryDialog;
import com.dole.bobbysstoryworld.ui.CustomDialog;
import com.dole.bobbysstoryworld.ui.DoleCheckBox;
import com.dole.bobbysstoryworld.ui.FontEditText;
import com.dole.bobbysstoryworld.ui.FontTextView;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

public class UserDetailView extends FrameLayout implements
		IServerResponseCallback, CheckCancelInterface {

	private static final String TAG = UserDetailView.class.getName();

	public static final String EXTRA_USER_DETAIL_MODE_NAME = "user_detail_mode";
	private static final String EXTRA_USER_DETAIL_MODE_ID = "user_detail_mode.id";
	private static final String EXTRA_USER_DETAIL_MODE_PWD = "user_detail_mode.pwd";
	private static final String EXTRA_USER_DETAIL_MODE_CONFIRM_PWD = "user_detail_mode.confirm_pwd";
	private static final String EXTRA_USER_DETAIL_MODE_BIRTH_YEAR = "user_detail_mode.birth_year";
	private static final String EXTRA_USER_DETAIL_MODE_COUNTRY = "user_detail_mode.country";
	private static final String EXTRA_USER_DETAIL_MODE_CITY = "user_detail_mode.city";
	private static final String EXTRA_USER_DETAIL_MODE_GENDER = "user_detail_mode.gender";
	protected static final String EXTRA_USER_DETAIL_MODE_TERMS_AGREED = "user_detail_mode.terms_agreed";

	public static final int SIGN_UP_EMAIL = -1;
	public static final int SIGN_UP_FACEBOOK = -2;

	private RequestSignUp mRequestSingUp;
	private ModifyMobileProfile mModifyMobile;
	private int mMode;
	private String mTempUserId;
	private String mTempPassword;
	private String mTempUserNo;
	private String mTempAuthKey;
	private String mTempCountry;
	private String mTempLanguage;
	private int mTempGender;
	private String mTempBirth = "";
	private String mTempAddress;
	private boolean mIsCheckedNext;
	private boolean mIsEmailFill = false;
	private boolean mIsPasswordFill = false;
	private boolean mIsBirthFill = false;
	private boolean mIsCityFill = false;
	private ProgressDialog mProgressDialog;
	private boolean mIsFacebookLogin = false;
	private boolean mNeedPasswordHintMultiline = true;
	private int mCurrentSelectedCountry;

	private CustomDialog.CustomDialogButtonClickListener mDialogClickListener = new CustomDialog.CustomDialogButtonClickListener() {
		@Override
		public void onClickOK(int requestCode) {
			((TextView) findViewById(R.id.user_detail_edit_email)).setText("");
			((TextView) findViewById(R.id.user_detail_edit_password))
					.setText("");
			((TextView) findViewById(R.id.user_detail_confirm_password))
					.setText("");
			((TextView) findViewById(R.id.user_detail_edit_birthday))
					.setText("");
			((TextView) findViewById(R.id.user_detail_edit_city)).setText("");
			((PortraitActivity) getContext()).onBackPressed();
		}

		@Override
		public void onClickCancel(int requestCode) {
			// Do nothing
		}
	};

	private BirthDayPickerDialog.BirthDayPickerDialogClickListener mBirthdayPickerListener = new BirthDayPickerDialog.BirthDayPickerDialogClickListener() {
		@Override
		public void onClickOK(Bundle args) {
			if (args != null) {
				int birthYear = args
						.getInt(BirthDayPickerDialog.EXTRA_YEAR, -1);
				if (birthYear >= 0) {
					final TextView editBirthDay = (TextView) findViewById(R.id.user_detail_edit_birthday);
					if (editBirthDay != null)
						editBirthDay.setText(Integer.toString(birthYear));
				}
			}
		}

		@Override
		public void onClickCancel() {
			// Do nothing
		}
	};

	private CountryDialog.CountryDialogClickListener mCityClickListener = new CountryDialog.CountryDialogClickListener() {
		@Override
		public void onClickOK(String city) {
			final TextView cityView = (TextView) findViewById(R.id.user_detail_edit_city);
			if (cityView != null && city != null) {
				cityView.setText(city);
			}
		}

		@Override
		public void onClickCancel() {
			// final TextView cityView =
			// (TextView)findViewById(R.id.user_detail_edit_city);
			// if(cityView != null) {
			// cityView.setText("");
			// }
		}
	};

	private CountryDialog.CountryDialogClickListener mCountryClickListener = new CountryDialog.CountryDialogClickListener() {
		@Override
		public void onClickOK(String code) {
			TextView countryView = (TextView) findViewById(R.id.user_detail_edit_country);
			countryView.setText(code);
			if (updateCurrentCountry(code) && mMode < 0) {
				mIsCheckedNext = false;
				final DoleCheckBox checkTerms = (DoleCheckBox) findViewById(R.id.user_detail_text_personal_info_term);
				checkTerms.setNext(mIsCheckedNext);
			}
		}

		@Override
		public void onClickCancel() {
			// final TextView countryView =
			// (TextView)findViewById(R.id.user_detail_edit_country);
			// if(countryView != null) {
			// countryView.setText("");
			// }
			// mCurrentSelectedCountry = 0;
		}
	};

	public UserDetailView(Context context) {
		super(context);
	}

	public UserDetailView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public UserDetailView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();

		if (Locale.getDefault().equals(Locale.KOREA)
				|| Locale.getDefault().equals(Locale.JAPAN)) {
			mNeedPasswordHintMultiline = false;
		}

		Bundle data = ((PortraitActivity) getContext()).getSavedDataBundle();
		if (data != null) {
			mMode = data.getInt(EXTRA_USER_DETAIL_MODE_NAME, SIGN_UP_EMAIL);
			data.remove(EXTRA_USER_DETAIL_MODE_NAME);
		} else {
			mMode = SIGN_UP_EMAIL;
		}

		// Change visibility and Change text
		final FontEditText emailText = (FontEditText) findViewById(R.id.user_detail_edit_email);
		final FontEditText oldPassView = (FontEditText) findViewById(R.id.user_detail_old_password);
		final FontEditText passView = (FontEditText) findViewById(R.id.user_detail_edit_password);
		final FontEditText passConfirmView = (FontEditText) findViewById(R.id.user_detail_confirm_password);
		final DoleCheckBox checkTerms = (DoleCheckBox) findViewById(R.id.user_detail_text_personal_info_term);
		final Button confirmChangeButton = (Button) findViewById(R.id.user_detail_btn_confirm_edit);
		final TextView editBirthDay = (TextView) findViewById(R.id.user_detail_edit_birthday);
		final TextView countryView = (TextView) findViewById(R.id.user_detail_edit_country);
		final FontEditText cityView = (FontEditText) findViewById(R.id.user_detail_edit_city);
		final RadioGroup genderGroup = (RadioGroup) findViewById(R.id.user_detail_select_gender);
		View back = findViewById(R.id.user_detail_btn_cancel);
		final View oldPassViewFrame = findViewById(R.id.user_detail_old_password_frame);
		final View passViewFrame = findViewById(R.id.user_detail_edit_password_frame);
		final View passConfirmViewFrame = findViewById(R.id.user_detail_confirm_password_frame);

		final RadioButton maleButton = (RadioButton) findViewById(R.id.user_detail_gender_male);
		final RadioButton femaleButton = (RadioButton) findViewById(R.id.user_detail_gender_female);

		maleButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(),
				// Constants.MIXPANEL_TOKEN);
				// mixpanel.track("Input Data on Registration Field", null);
				// mixpanel.flush();
			}
		});

		femaleButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(),
				// Constants.MIXPANEL_TOKEN);
				// mixpanel.track("Input Data on Registration Field", null);
				// mixpanel.flush();
			}
		});

		if (emailText != null) {
			String emailString = getResources().getString(
					R.string.pf_signup_textfield_email)
					+ "*";
			SpannableString spanStr = new SpannableString(emailString);
			spanStr.setSpan(
					new ForegroundColorSpan(getResources().getColor(
							R.color.esterisk)), emailString.length() - 1,
					emailString.length(), 0);
			emailText.setHint(spanStr);
		}

		if (passView != null) {
			if (mMode == SIGN_UP_EMAIL) {
				String passString = getResources().getString(
						R.string.pf_signup_textfield_password_condition);
				int start = passString.indexOf("(");
				passString = passString.replace("(", "*\u0020").substring(0,
						passString.length());

				SpannableString spanStr = new SpannableString(passString);
				spanStr.setSpan(
						new AbsoluteSizeSpan(getResources()
								.getDimensionPixelSize(
										R.dimen.setting_textsize_small)),
						start + 1, passString.length(), 0);
				spanStr.setSpan(new ForegroundColorSpan(getResources()
						.getColor(R.color.setting_textsize_small_color)),
						start + 1, passString.length(), 0);
				spanStr.setSpan(new ForegroundColorSpan(getResources()
						.getColor(R.color.esterisk)), start, start + 1, 0);
				passView.setHint(spanStr);
			} else {
				String passString = getResources().getString(
						R.string.pf_editaccount_textfield_password_condition);
				int start = passString.indexOf("(");
				passString = passString.replace("(", "*\u0020").substring(0,
						passString.length());

				FontTextView multiHint = (FontTextView) findViewById(R.id.user_detail_old_password_hint);
				if (mNeedPasswordHintMultiline) {
					passView.setHint(passString.substring(0, start));
					multiHint.setHint(passString.substring(start + 1));
				} else {
					multiHint.setVisibility(View.GONE);

					SpannableString spanStr = new SpannableString(passString);
					spanStr.setSpan(
							new AbsoluteSizeSpan(getResources()
									.getDimensionPixelSize(
											R.dimen.setting_textsize_small)),
							start + 1, passString.length(), 0);
					spanStr.setSpan(new ForegroundColorSpan(getResources()
							.getColor(R.color.setting_textsize_small_color)),
							start + 1, passString.length(), 0);
					spanStr.setSpan(new ForegroundColorSpan(getResources()
							.getColor(R.color.esterisk)), start, start + 1, 0);
					passView.setHint(spanStr);
				}
			}
		}

		if (passConfirmView != null) {
			if (mMode == SIGN_UP_EMAIL || mMode == SIGN_UP_FACEBOOK) {
				String passConfirmString = getResources().getString(
						R.string.pf_signup_textfield_password_confirm)
						+ "*";
				SpannableString spanStr = new SpannableString(passConfirmString);
				spanStr.setSpan(new ForegroundColorSpan(getResources()
						.getColor(R.color.esterisk)), passConfirmString
						.length() - 1, passConfirmString.length(), 0);
				passConfirmView.setHint(spanStr);
			} else {
				passConfirmView.setHint(getResources().getString(
						R.string.pf_signup_textfield_password_confirm));
			}
		}

		if (countryView != null) {
			if (mMode == SIGN_UP_EMAIL || mMode == SIGN_UP_FACEBOOK) {
				String countryString = getResources().getString(
						R.string.pf_signup_textfield_country)
						+ "*";
				SpannableString spanStr = new SpannableString(countryString);
				spanStr.setSpan(new ForegroundColorSpan(getResources()
						.getColor(R.color.esterisk)),
						countryString.length() - 1, countryString.length(), 0);
				countryView.setHint(spanStr);
			} else {
				countryView.setHint(getResources().getString(
						R.string.pf_signup_textfield_country));
			}

			if (mMode == SIGN_UP_EMAIL || mMode == SIGN_UP_FACEBOOK) {
				String localeCountry = Locale.getDefault().getCountry();
				if (localeCountry.equals("SG")) {
					countryView.setText("Singapore");
					mCurrentSelectedCountry = CountryDialog.SG;
				} else if (localeCountry.equals("JP")) {
					countryView.setText("日本");
					mCurrentSelectedCountry = CountryDialog.JP;
				} else if (localeCountry.equals("NZ")) {
					countryView.setText("New Zealand");
					mCurrentSelectedCountry = CountryDialog.NZ;
				} else if (localeCountry.equals("KR")) {
					countryView.setText("한국");
					mCurrentSelectedCountry = CountryDialog.KR;
				} else if (localeCountry.equals("PH")) {
					countryView.setText("Philippines");
					mCurrentSelectedCountry = CountryDialog.PH;
//				} else if (localeCountry.equals("AU")) {
//					countryView.setText("Philippines");
//					mCurrentSelectedCountry = CountryDialog.AU;
				} else {
					countryView.setText("Philippines");
					//mCurrentSelectedCountry = CountryDialog.ETC;
					mCurrentSelectedCountry = CountryDialog.PH;
				}
			}
		}

		if (oldPassView != null) {
			oldPassView
					.setHint(R.string.pf_editaccount_textfield_current_password);
		}

		countryView.setFocusable(false);
		countryView.setClickable(true);
		countryView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				CountryDialog dialog = new CountryDialog(getContext(),
						mCountryClickListener, CountryDialog.COUNTRY, 0);
				dialog.show();

				// MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(),
				// Constants.MIXPANEL_TOKEN);
				// mixpanel.track("Input Data on Registration Field", null);
				// mixpanel.flush();
			}
		});

		cityView.setFocusable(true);
		cityView.setClickable(true);
		cityView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (mCurrentSelectedCountry > 0) {
					// CountryDialog dialog = new CountryDialog(getContext(),
					// mCityClickListener, CountryDialog.CITY,
					// mCurrentSelectedCountry);
					// dialog.show();

					// MixpanelAPI mixpanel = MixpanelAPI.getInstance(
					// getContext(), Constants.MIXPANEL_TOKEN);
					// mixpanel.track("Input Data on Registration Field", null);
					// mixpanel.flush();
				}
			}
		});

		String savedId = null;
		String savedPwd = null;
		String savedConfirmPwd = null;
		String savedBirthYear = null;
		String savedCountry = null;
		String savedCity = null;
		String countryName = null;
		int savedGender = -1;

		if (data != null) {
			switch (mMode) {
			case SIGN_UP_EMAIL:
				savedId = data.getString(EXTRA_USER_DETAIL_MODE_ID, null);
				savedPwd = data.getString(EXTRA_USER_DETAIL_MODE_PWD, null);
				savedConfirmPwd = data.getString(
						EXTRA_USER_DETAIL_MODE_CONFIRM_PWD, null);
				savedBirthYear = data.getString(
						EXTRA_USER_DETAIL_MODE_BIRTH_YEAR, null);
				savedCountry = data.getString(EXTRA_USER_DETAIL_MODE_COUNTRY,
						null);
				savedCity = data.getString(EXTRA_USER_DETAIL_MODE_CITY, null);
				savedGender = data.getInt(EXTRA_USER_DETAIL_MODE_GENDER, -1);
				mIsCheckedNext = data.getBoolean(
						EXTRA_USER_DETAIL_MODE_TERMS_AGREED, false);

				data.remove(EXTRA_USER_DETAIL_MODE_ID);
				data.remove(EXTRA_USER_DETAIL_MODE_PWD);
				data.remove(EXTRA_USER_DETAIL_MODE_CONFIRM_PWD);
				data.remove(EXTRA_USER_DETAIL_MODE_BIRTH_YEAR);
				data.remove(EXTRA_USER_DETAIL_MODE_COUNTRY);
				data.remove(EXTRA_USER_DETAIL_MODE_CITY);
				data.remove(EXTRA_USER_DETAIL_MODE_GENDER);
				data.remove(EXTRA_USER_DETAIL_MODE_TERMS_AGREED);

				if (savedCountry != null) {
					updateCurrentCountry(savedCountry);
				}
				break;

			case SIGN_UP_FACEBOOK:
				savedId = data.getString(Constants.TAG_USER_ID, "");
				savedBirthYear = data.getString(Constants.TAG_BIRTHDAY, null);
				// savedCity = data.getString(Constants.TAG_ADDRESS_MAIN, null);
				savedCountry = getCurrentCountry(data.getString(
						Constants.TAG_STANDARD_COUNTRY_CODE, null));
				savedGender = Integer.valueOf(data.getString(
						Constants.TAG_GENDER, "-1"));
				mIsCheckedNext = data.getBoolean(
						EXTRA_USER_DETAIL_MODE_TERMS_AGREED, false);
				break;

			case Constants.LOGIN_TYPE_EMAIL:
			case Constants.LOGIN_TYPE_FACEBOOK:
				final String addrMain = BobbysStoryWorldPreference.getString(
						Constants.PREF_ADDRESS_MAIN, null);
				final String addrCountry = BobbysStoryWorldPreference
						.getString(Constants.PREF_COUNTRY, null);
				final String birthday = BobbysStoryWorldPreference.getString(
						Constants.PREF_BIRTHDAY, null);
				if (birthday != null) {
					final String bYear = birthday.substring(0, 4);
					if (!bYear.equals("9999"))
						savedBirthYear = bYear;
				}
				if (addrMain != null && !addrMain.equals("invalid")) {
					savedCity = addrMain;
				}
				if (addrCountry != null) {
					savedCountry = getCurrentCountry(addrCountry);
				}
				savedGender = BobbysStoryWorldPreference.getInt(
						Constants.PREF_GENDER, 0);
				break;
			}
		}

		switch (mMode) {
		case Constants.LOGIN_TYPE_FACEBOOK:
			checkTerms.setNext(true);
			emailText.setTextColor(0xFF4f2c00);
			emailText.setBackgroundColor(Color.TRANSPARENT);
			emailText.setGravity(Gravity.CENTER);
			emailText.setText(BobbysStoryWorldPreference.getString(
					Constants.PREF_USER_ID, ""));
			emailText.setCompoundDrawables(null, null, null, null);
			emailText.setEnabled(false);

			oldPassViewFrame.setVisibility(View.GONE);
			passViewFrame.setVisibility(View.GONE);
			passConfirmViewFrame.setVisibility(View.GONE);

			// editBirthDay.setText(BobbysStoryWorldPreference.getString(Constants.PREF_BIRTHDAY,
			// "").substring(0, 4));
			countryName = BobbysStoryWorldPreference.getString(
					Constants.PREF_ADDRESS_MAIN, "");
			if (countryName.length() > 0 && !countryName.equals("invalid"))
				cityView.setText(countryName);

			// if(BobbysStoryWorldPreference.getInt(Constants.PREF_GENDER, 0) ==
			// Constants.GenderType.MALE.type)
			// genderGroup.check(R.id.user_detail_gender_male);
			// else if(BobbysStoryWorldPreference.getInt(Constants.PREF_GENDER,
			// 0) == Constants.GenderType.FEMALE.type)
			// genderGroup.check(R.id.user_detail_gender_female);
			checkTerms.setVisibility(View.GONE);
			confirmChangeButton.setText(getResources().getString(
					R.string.pf_editaccount_button_title_save));
			confirmChangeButton.setEnabled(true);
			mIsEmailFill = true;
			mIsPasswordFill = true;
			mIsCityFill = true;
			break;

		case SIGN_UP_FACEBOOK:
			checkTerms.setNext(mIsCheckedNext);
			emailText.setTextColor(0xFF4f2c00);
			emailText.setBackgroundColor(Color.TRANSPARENT);
			emailText.setGravity(Gravity.CENTER);
			emailText.setCompoundDrawables(null, null, null, null);
			emailText.setEnabled(false);

			oldPassViewFrame.setVisibility(View.GONE);
			passViewFrame.setVisibility(View.GONE);
			passConfirmViewFrame.setVisibility(View.GONE);

			// editBirthDay.setText(args.getString(Constants.TAG_BIRTHDAY,
			// "").substring(6));
			// editBirthDay.setText("");
			// cityView.setText(args.getString(Constants.TAG_ADDRESS_MAIN, ""));
			cityView.setText("");

			// if(Integer.valueOf(args.getString(Constants.TAG_GENDER, "0")) ==
			// Constants.GenderType.MALE.type)
			// if(Integer.valueOf("0") == Constants.GenderType.MALE.type)
			// genderGroup.check(R.id.user_detail_gender_male);
			// else if(Integer.valueOf(args.getString(Constants.TAG_GENDER,
			// "0")) == Constants.GenderType.FEMALE.type)
			// else if(Integer.valueOf("0") == Constants.GenderType.FEMALE.type)
			// genderGroup.check(R.id.user_detail_gender_female);
			// checkTerms.setVisibility(View.GONE);
			confirmChangeButton.setText(getResources().getString(
					R.string.pf_editaccount_button_title_save));
			if (checkTerms.isChecked())
				confirmChangeButton.setEnabled(true);
			else
				confirmChangeButton.setEnabled(false);
			back.setVisibility(View.GONE);
			mIsEmailFill = true;
			mIsPasswordFill = true;
			mIsCityFill = true;
			break;

		case Constants.LOGIN_TYPE_EMAIL:
			checkTerms.setNext(true);
			if (passView != null) {
				String passString = getResources().getString(
						R.string.pf_editaccount_textfield_password_condition);
				int start = passString.indexOf("(");
				passString = passString.replace("(", "\u0020").substring(0,
						passString.length() - 1);

				FontTextView multiHint = (FontTextView) findViewById(R.id.user_detail_old_password_hint);
				if (mNeedPasswordHintMultiline) {
					passView.setHint(passString.substring(0, start));
					multiHint.setHint(passString.substring(start + 1));
				} else {
					multiHint.setVisibility(View.GONE);

					SpannableString spanStr = new SpannableString(passString);
					spanStr.setSpan(
							new AbsoluteSizeSpan(getResources()
									.getDimensionPixelSize(
											R.dimen.setting_textsize_small)),
							start, passString.length(), 0);
					spanStr.setSpan(new ForegroundColorSpan(getResources()
							.getColor(R.color.setting_textsize_small_color)),
							start, passString.length(), 0);
					passView.setHint(spanStr);
				}
			}
			emailText.setTextColor(0xFF4f2c00);
			emailText.setBackgroundColor(Color.TRANSPARENT);
			emailText.setGravity(Gravity.CENTER);
			emailText.setText(BobbysStoryWorldPreference.getString(
					Constants.PREF_USER_ID, ""));
			emailText.setCompoundDrawables(null, null, null, null);
			emailText.setEnabled(false);
			// editBirthDay.setText(BobbysStoryWorldPreference.getString(Constants.PREF_BIRTHDAY,
			// "").substring(0, 4));
			// countryName =
			// BobbysStoryWorldPreference.getString(Constants.PREF_ADDRESS_MAIN,
			// "");
			// if(countryName.length() > 0 && !countryName.equals("invalid"))
			// countryView.setText(countryName);
			// if(BobbysStoryWorldPreference.getInt(Constants.PREF_GENDER, 0) ==
			// Constants.GenderType.MALE.type)
			// genderGroup.check(R.id.user_detail_gender_male);
			// else if(BobbysStoryWorldPreference.getInt(Constants.PREF_GENDER,
			// 0) == Constants.GenderType.FEMALE.type)
			// genderGroup.check(R.id.user_detail_gender_female);
			checkTerms.setVisibility(View.GONE);
			confirmChangeButton.setText(getResources().getString(
					R.string.pf_editaccount_button_title_save));
			confirmChangeButton.setEnabled(true);
			mIsEmailFill = true;
			mIsPasswordFill = true;
			mIsCityFill = true;
			break;

		// Show all
		case SIGN_UP_EMAIL:
			oldPassViewFrame.setVisibility(View.GONE);
			// genderGroup.check(R.id.user_detail_gender_male);
			confirmChangeButton.setEnabled(false);
			checkTerms.setNext(mIsCheckedNext);

		default:
			break;
		}

		editBirthDay.setOnTouchListener(mTransactOnclickListener);

		confirmChangeButton.setOnClickListener(mDoneEditOnClickListener);
		checkTerms.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				confirmChangeButton.setEnabled(isChecked);
			}
		});
		checkTerms.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP) {
					Bundle data = ((PortraitActivity) getContext())
							.getSavedDataBundle();
					if (data != null) {
						final TextView emailText = (TextView) findViewById(R.id.user_detail_edit_email);
						final TextView passView = (TextView) findViewById(R.id.user_detail_edit_password);
						final TextView passConfirmView = (TextView) findViewById(R.id.user_detail_confirm_password);
						final TextView editBirthDay = (TextView) findViewById(R.id.user_detail_edit_birthday);
						final TextView cityView = (TextView) findViewById(R.id.user_detail_edit_city);
						final RadioGroup genderGroup = (RadioGroup) findViewById(R.id.user_detail_select_gender);
						final int checkedId = genderGroup
								.getCheckedRadioButtonId();
						int check = -1;
						if (checkedId == R.id.user_detail_gender_male)
							check = Constants.GenderType.MALE.type;
						else if (checkedId == R.id.user_detail_gender_female)
							check = Constants.GenderType.FEMALE.type;
						data.putString(EXTRA_USER_DETAIL_MODE_ID, emailText
								.getText().toString());
						data.putString(EXTRA_USER_DETAIL_MODE_PWD, passView
								.getText().toString());
						data.putString(EXTRA_USER_DETAIL_MODE_CONFIRM_PWD,
								passConfirmView.getText().toString());
						final String birthYear = editBirthDay.getText()
								.toString();
						if (birthYear.length() > 0)
							data.putString(EXTRA_USER_DETAIL_MODE_BIRTH_YEAR,
									birthYear);
						data.putString(EXTRA_USER_DETAIL_MODE_COUNTRY,
								countryView.getText().toString());
						data.putString(EXTRA_USER_DETAIL_MODE_CITY, cityView
								.getText().toString());
						data.putInt(EXTRA_USER_DETAIL_MODE_GENDER, check);
						data.putBoolean(EXTRA_USER_DETAIL_MODE_TERMS_AGREED,
								mIsCheckedNext);
						data.putString(Constants.TAG_STANDARD_COUNTRY_CODE,
								getCurrentCountryCode());
						data.putInt(EXTRA_USER_DETAIL_MODE_NAME, mMode);
					}

					((PortraitActivity) getContext())
							.showMenu(PortraitActivity.ENTRY_MENU_SIGNUP_TERMS);

					// MixpanelAPI mixpanel = MixpanelAPI.getInstance(
					// getContext(), Constants.MIXPANEL_TOKEN);
					// mixpanel.track("Input Data on Registration Field", null);
					// mixpanel.flush();

					return true;
				}
				return false;
			}
		});
		back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!handleCancelButtonClick())
					((PortraitActivity) getContext()).onBackPressed();
			}
		});

		emailText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_edit_email_error);
				errorView.setVisibility(View.GONE);
				if (mMode != SIGN_UP_EMAIL)
					return;
				String st = s.toString();
				if (st.trim().equals(""))
					mIsEmailFill = false;
				else
					mIsEmailFill = true;

				// if(mIsEmailFill && mIsPasswordFill && mIsBirthFill &&
				// mIsCityFill && checkTerms.isChecked())
				if (mIsEmailFill && mIsPasswordFill && checkTerms.isChecked())
					confirmChangeButton.setEnabled(true);
				else
					confirmChangeButton.setEnabled(false);
			}
		});

		emailText.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_edit_email_error);
				errorView.setVisibility(View.GONE);
				return false;
			}
		});

		emailText.setOnImeHideListener(new FontEditText.ImeHideListener() {
			@Override
			public void onImeHide() {
				String emailAdd = emailText.getText().toString();
				if (emailAdd.length() > 0 && !Util.checkEmail(emailAdd)) {
					FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_edit_email_error);
					errorView
							.setText(R.string.pf_net_error_email_enter_full_address);
					errorView.setVisibility(View.VISIBLE);
				}
			}
		});

		emailText.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					String emailAdd = emailText.getText().toString();
					if (emailAdd.length() > 0 && !Util.checkEmail(emailAdd)) {
						FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_edit_email_error);
						errorView
								.setText(R.string.pf_net_error_email_enter_full_address);
						errorView.setVisibility(View.VISIBLE);
					}
				} else {
					MixpanelAPI mixpanel = MixpanelAPI.getInstance(
							getContext(), Constants.MIXPANEL_TOKEN);
					mixpanel.track("Input Data on Registration Field", null);
					mixpanel.flush();
				}
			}
		});

		emailText.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if (actionId != EditorInfo.IME_NULL
						|| (actionId == EditorInfo.IME_NULL && event != null && event
								.getAction() == KeyEvent.ACTION_DOWN)) {
					String emailAdd = emailText.getText().toString();
					if (emailAdd.length() > 0 && !Util.checkEmail(emailAdd)) {
						FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_edit_email_error);
						errorView
								.setText(R.string.pf_net_error_email_enter_full_address);
						errorView.setVisibility(View.VISIBLE);
						return true;
					}
				}
				return false;
			}
		});

		passView.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				if (mNeedPasswordHintMultiline) {
					FontTextView multiHint = (FontTextView) findViewById(R.id.user_detail_old_password_hint);
					if (after > 0) {
						multiHint.setVisibility(View.GONE);
					} else {
						if (start == 0)
							multiHint.setVisibility(View.VISIBLE);
					}
				}
			}

			@Override
			public void afterTextChanged(Editable s) {
				FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_edit_password_error);
				errorView.setVisibility(View.GONE);
				if (mMode != SIGN_UP_EMAIL)
					return;
				String st = s.toString();
				if (st.trim().equals(""))
					mIsPasswordFill = false;
				else
					mIsPasswordFill = true;

				// if(mIsEmailFill && mIsPasswordFill && mIsBirthFill &&
				// mIsCityFill && checkTerms.isChecked())
				if (mIsEmailFill && mIsPasswordFill && checkTerms.isChecked())
					confirmChangeButton.setEnabled(true);
				else
					confirmChangeButton.setEnabled(false);
			}
		});

		passView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_edit_password_error);
				errorView.setVisibility(View.GONE);
				return false;
			}
		});

		passView.setOnImeHideListener(new FontEditText.ImeHideListener() {
			@Override
			public void onImeHide() {
				String password = passView.getText().toString();
				if (mMode == SIGN_UP_EMAIL && password.length() > 0
						&& password.length() < Constants.PASSWORD_MIN_LENGTH) {
					FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_edit_password_error);
					errorView.setText(R.string.pf_net_error_password_short);
					errorView.setVisibility(View.VISIBLE);
				} else if (mMode == Constants.LOGIN_TYPE_EMAIL
						&& password.length() > 0
						&& password.length() < Constants.PASSWORD_MIN_LENGTH) {
					FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_edit_password_error);
					errorView.setText(R.string.pf_net_error_password_short);
					errorView.setVisibility(View.VISIBLE);
				}
			}
		});

		passView.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					String password = passView.getText().toString();
					if (mMode == SIGN_UP_EMAIL
							&& password.length() < Constants.PASSWORD_MIN_LENGTH) {
						FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_edit_password_error);
						errorView.setText(R.string.pf_net_error_password_short);
						errorView.setVisibility(View.VISIBLE);
					} else if (mMode == Constants.LOGIN_TYPE_EMAIL
							&& password.length() > 0
							&& password.length() < Constants.PASSWORD_MIN_LENGTH) {
						FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_edit_password_error);
						errorView.setText(R.string.pf_net_error_password_short);
						errorView.setVisibility(View.VISIBLE);
					}
				} else {
					// MixpanelAPI mixpanel = MixpanelAPI.getInstance(
					// getContext(), Constants.MIXPANEL_TOKEN);
					// mixpanel.track("Input Data on Registration Field", null);
					// mixpanel.flush();
				}
			}
		});

		passView.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if (actionId != EditorInfo.IME_NULL
						|| (actionId == EditorInfo.IME_NULL && event != null && event
								.getAction() == KeyEvent.ACTION_DOWN)) {
					String password = passView.getText().toString();
					if (mMode == SIGN_UP_EMAIL
							&& password.length() < Constants.PASSWORD_MIN_LENGTH) {
						FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_edit_password_error);
						errorView.setText(R.string.pf_net_error_password_short);
						errorView.setVisibility(View.VISIBLE);
						return true;
					} else if (mMode == Constants.LOGIN_TYPE_EMAIL
							&& password.length() > 0
							&& password.length() < Constants.PASSWORD_MIN_LENGTH) {
						FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_edit_password_error);
						errorView.setText(R.string.pf_net_error_password_short);
						errorView.setVisibility(View.VISIBLE);
						return true;
					}
				}
				return false;
			}
		});

		oldPassView.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_old_password_error);
				errorView.setVisibility(View.GONE);
			}
		});

		oldPassView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_old_password_error);
				errorView.setVisibility(View.GONE);
				return false;
			}
		});

		oldPassView.setOnImeHideListener(new FontEditText.ImeHideListener() {
			@Override
			public void onImeHide() {
				String oldPassword = oldPassView.getText().toString();
				if (oldPassword.length() > 0
						&& oldPassword.length() < Constants.PASSWORD_MIN_LENGTH) {
					FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_old_password_error);
					errorView.setText(R.string.pf_net_error_password_short);
					errorView.setVisibility(View.VISIBLE);
				}
			}
		});

		oldPassView.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					String oldPassword = oldPassView.getText().toString();
					if ("".equals(oldPassword)) {
						FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_old_password_error);
						errorView
								.setText(R.string.pf_editaccount_popup_input_password);
						errorView.setVisibility(View.VISIBLE);
					} else if (oldPassword.length() < Constants.PASSWORD_MIN_LENGTH) {
						FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_old_password_error);
						errorView.setText(R.string.pf_net_error_password_short);
						errorView.setVisibility(View.VISIBLE);
					}
				} else {
					// MixpanelAPI mixpanel = MixpanelAPI.getInstance(
					// getContext(), Constants.MIXPANEL_TOKEN);
					// mixpanel.track("Input Data on Registration Field", null);
					// mixpanel.flush();
				}
			}
		});

		oldPassView.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if (actionId != EditorInfo.IME_NULL
						|| (actionId == EditorInfo.IME_NULL && event != null && event
								.getAction() == KeyEvent.ACTION_DOWN)) {
					String oldPassword = oldPassView.getText().toString();
					if ("".equals(oldPassword)) {
						FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_old_password_error);
						errorView
								.setText(R.string.pf_editaccount_popup_input_password);
						errorView.setVisibility(View.VISIBLE);
						return true;
					} else if (oldPassword.length() < Constants.PASSWORD_MIN_LENGTH) {
						FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_old_password_error);
						errorView.setText(R.string.pf_net_error_password_short);
						errorView.setVisibility(View.VISIBLE);
						return true;
					}
				}
				return false;
			}
		});

		passConfirmView.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_confirm_password_error);
				errorView.setVisibility(View.GONE);
			}
		});

		passConfirmView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_confirm_password_error);
				errorView.setVisibility(View.GONE);
				return false;
			}
		});

		passConfirmView
				.setOnImeHideListener(new FontEditText.ImeHideListener() {
					@Override
					public void onImeHide() {
						String confirmPassword = passConfirmView.getText()
								.toString();
						String newPassword = passView.getText().toString();
						if (!newPassword.equals(confirmPassword)) {
							FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_confirm_password_error);
							errorView
									.setText(R.string.pf_net_error_password_incorrect);
							errorView.setVisibility(View.VISIBLE);
						}
					}
				});

		passConfirmView.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					String confirmPassword = passConfirmView.getText()
							.toString();
					String newPassword = passView.getText().toString();
					if (!newPassword.equals(confirmPassword)) {
						FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_confirm_password_error);
						errorView
								.setText(R.string.pf_net_error_password_incorrect);
						errorView.setVisibility(View.VISIBLE);
					}
				} else {
					// MixpanelAPI mixpanel = MixpanelAPI.getInstance(
					// getContext(), Constants.MIXPANEL_TOKEN);
					// mixpanel.track("Input Data on Registration Field", null);
					// mixpanel.flush();
				}
			}
		});

		passConfirmView.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if (actionId != EditorInfo.IME_NULL
						|| (actionId == EditorInfo.IME_NULL && event != null && event
								.getAction() == KeyEvent.ACTION_DOWN)) {
					String confirmPassword = passConfirmView.getText()
							.toString();
					String newPassword = passView.getText().toString();
					if (!newPassword.equals(confirmPassword)) {
						FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_confirm_password_error);
						errorView
								.setText(R.string.pf_net_error_password_incorrect);
						errorView.setVisibility(View.VISIBLE);
						return true;
					}
				}
				return false;
			}
		});

		editBirthDay.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				String st = s.toString();
				if (st.trim().equals(""))
					mIsBirthFill = false;
				else
					mIsBirthFill = true;

				if (mIsEmailFill && mIsPasswordFill && checkTerms.isChecked())
					confirmChangeButton.setEnabled(true);
				else
					confirmChangeButton.setEnabled(false);
			}
		});

		cityView.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				String st = s.toString();
				if (st.trim().equals(""))
					mIsCityFill = false;
				else
					mIsCityFill = true;

				// if(mIsEmailFill && mIsPasswordFill && mIsBirthFill &&
				// mIsCityFill && checkTerms.isChecked())
				if (mIsEmailFill && mIsPasswordFill && checkTerms.isChecked())
					confirmChangeButton.setEnabled(true);
				else
					confirmChangeButton.setEnabled(false);
			}
		});

		setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent e) {
				Util.hideSoftInput(getContext(), v);
				emailText.clearFocus();
				oldPassView.clearFocus();
				passView.clearFocus();
				passConfirmView.clearFocus();
				passView.clearFocus();
				cityView.clearFocus();
				return false;
			}
		});

		if (savedId != null)
			emailText.setText(savedId);
		if (savedPwd != null)
			passView.setText(savedPwd);
		if (savedConfirmPwd != null)
			passConfirmView.setText(savedConfirmPwd);
		if (savedBirthYear != null)
			editBirthDay.setText(savedBirthYear);
		if (savedCountry == null) {
			countryView.setText("Philippines");
			mCurrentSelectedCountry = CountryDialog.PH;
		} else {
			countryView.setText(savedCountry);
		}
		if (savedCity != null)
			cityView.setText(savedCity);
		if (savedGender > 0) {
			if (savedGender == Constants.GenderType.MALE.type)
				genderGroup.check(R.id.user_detail_gender_male);
			else if (savedGender == Constants.GenderType.FEMALE.type)
				genderGroup.check(R.id.user_detail_gender_female);
		}

		if (savedId == null
				&& savedPwd == null
				&& savedConfirmPwd == null
				&& BobbysStoryWorldPreference.getString(Constants.PREF_USER_ID,
						"").length() < 1) {
			// MixpanelAPI Login calls
			MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(),
					Constants.MIXPANEL_TOKEN);
			mixpanel.track(Constants.TAG_OPENED_SIGNUP, null);
			// mixpanel.timeEvent(Constants.TAG_REGISTRATION_TIME_EMAIL);
			mixpanel.timeEvent(Constants.TAG_SUCCESSFULLY_CREATED_ACCOUNT);
			mixpanel.flush();
		}
	}

	private OnClickListener mDoneEditOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (mMode) {
			case SIGN_UP_EMAIL:
				requestSignUp();
				break;

			case SIGN_UP_FACEBOOK:
				requestSignupSns();
				break;

			case Constants.LOGIN_TYPE_EMAIL:
				modifyEmail();
				break;

			case Constants.LOGIN_TYPE_FACEBOOK:
				modifySnsInfo();
				break;

			default:
				break;
			}
		}
	};

	private OnTouchListener mTransactOnclickListener = new OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			if (event.getAction() != MotionEvent.ACTION_UP)
				return false;

			final int id = v.getId();
			BirthDayPickerDialog dialog = null;
			switch (id) {
			case R.id.user_detail_edit_birthday: {
				final Bundle args = new Bundle();
				args.putBoolean(
						BirthDayPickerDialog.EXTRA_BIRTH_DATE_ONLY_YEAR, true);
				try {
					args.putInt(BirthDayPickerDialog.EXTRA_YEAR, Integer
							.valueOf(((TextView) v).getText().toString()));
				} catch (java.lang.NumberFormatException e) {
					e.printStackTrace();
				}
				dialog = new BirthDayPickerDialog(getContext(), args,
						mBirthdayPickerListener);

				// MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(),
				// Constants.MIXPANEL_TOKEN);
				// mixpanel.track("Input Data on Registration Field", null);
				// mixpanel.flush();

				break;
			}
			default:
				break;
			}

			if (dialog == null)
				return false;

			dialog.show();
			return true;
		}
	};

	public boolean handleCancelButtonClick() {
		boolean ret = false;
		InputMethodManager imm = (InputMethodManager) getContext()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(getWindowToken(), 0);
		switch (mMode) {
		// Show all
		case SIGN_UP_EMAIL:
			final TextView emailText = (TextView) findViewById(R.id.user_detail_edit_email);
			final TextView passView = (TextView) findViewById(R.id.user_detail_edit_password);
			final TextView passConfirmView = (TextView) findViewById(R.id.user_detail_confirm_password);
			// final TextView editBirthDay =
			// (TextView)findViewById(R.id.user_detail_edit_birthday);
			final TextView cityView = (TextView) findViewById(R.id.user_detail_edit_city);

			String emailString = emailText.getText().toString();
			String passwordString = passView.getText().toString();
			String confirmPasswordString = passConfirmView.getText().toString();
			// String birthdayString = editBirthDay.getText().toString();
			String cityString = cityView.getText().toString();

			if (emailString.length() == 0 && passwordString.length() == 0
					&& confirmPasswordString.length() == 0
					// && birthdayString.length() == 0
					&& cityString.length() == 0) {
				// leave it
			} else {
				Bundle args = new Bundle();
				args.putInt(Constants.CUSTOM_DIALOG_IMG_RES,
						R.drawable.popup_warning);
				args.putInt(Constants.CUSTOM_DIALOG_TEXT_RES,
						R.string.pf_signup_popup_cancel_signup);
				args.putInt(Constants.CUSTOM_DIALOG_LEFT_BUTTON_RES,
						R.drawable.popup_cancel_btn);
				args.putInt(Constants.CUSTOM_DIALOG_RIGHT_BUTTON_RES,
						R.drawable.popup_ok_btn);
				args.putBoolean(Constants.CUSTOM_DIALOG_CHANGE_TEXT_YES_NO,
						true);

				CustomDialog dialog = new CustomDialog(getContext(), args,
						mDialogClickListener);
				dialog.show();
				ret = true;
			}
			break;
		}
		return ret;
	}

	private void requestSignUp() {
		final TextView emailView = (TextView) findViewById(R.id.user_detail_edit_email);
		final TextView passView = (TextView) findViewById(R.id.user_detail_edit_password);
		final TextView passConfirmView = (TextView) findViewById(R.id.user_detail_confirm_password);
		final TextView birthView = (TextView) findViewById(R.id.user_detail_edit_birthday);
		final TextView countryView = (TextView) findViewById(R.id.user_detail_edit_country);
		final TextView cityView = (TextView) findViewById(R.id.user_detail_edit_city);
		final RadioGroup genderGroup = (RadioGroup) findViewById(R.id.user_detail_select_gender);

		final int genderId = genderGroup.getCheckedRadioButtonId();
		final RadioButton genderButton = (RadioButton) genderGroup
				.findViewById(genderId);
		final GenderType gender;
		if (genderId == R.id.user_detail_gender_male) {
			gender = GenderType.MALE;
		} else if (genderId == R.id.user_detail_gender_female) {
			gender = GenderType.FEMALE;
		} else {
			gender = null;
		}

		final Locale current = getResources().getConfiguration().locale;
		final String lang = current.getLanguage();

		String emailAdd = emailView.getText().toString();
		String newPwd = passView.getText().toString();
		String newConfirmPwd = passConfirmView.getText().toString();
		String country = countryView.getText().toString();

		if (emailAdd.length() > 0 && !Util.checkEmail(emailAdd)) {
			FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_edit_email_error);
			errorView.setText(R.string.pf_net_error_email_enter_full_address);
			errorView.setVisibility(View.VISIBLE);
			return;
		}
		if (newPwd.length() > 0
				&& newPwd.length() < Constants.PASSWORD_MIN_LENGTH) {
			FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_edit_password_error);
			errorView.setText(R.string.pf_net_error_password_short);
			errorView.setVisibility(View.VISIBLE);
			return;
		}
		if (!newPwd.equals(newConfirmPwd)) {
			FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_confirm_password_error);
			errorView.setText(R.string.pf_net_error_password_incorrect);
			errorView.setVisibility(View.VISIBLE);
			return;
		}
		if (country.length() == 0) {
			FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_edit_country_error);
			errorView.setText(R.string.pf_net_error_need_country);
			errorView.setVisibility(View.VISIBLE);
			return;
		}

		mTempUserId = emailView.getText().toString();
		if (!Util.checkEmail(mTempUserId)) {
			Toast.makeText(getContext(),
					R.string.pf_net_error_email_enter_full_address,
					Toast.LENGTH_SHORT).show();
			return;
		}
		mTempPassword = passView.getText().toString();
		mTempCountry = getCurrentCountryCode();
		mTempAddress = cityView.getText().toString();
		mTempBirth = birthView.getText().toString();
		if (mTempBirth.length() > 0)
			mTempBirth += "-01-01";
		mTempGender = (gender == null ? -1 : gender.type);
		mTempLanguage = lang;
		final HashMap<String, String> request = new HashMap<String, String>();
		request.put(Constants.TAG_USER_ID, mTempUserId);
		request.put(Constants.TAG_PASSWORD, mTempPassword);
		request.put(Constants.TAG_EMAIL, mTempUserId);
		request.put(Constants.TAG_STANDARD_COUNTRY_CODE,
				mTempCountry == null ? "PH" : mTempCountry);
		request.put(Constants.TAG_UUID, Util.getUUID(getContext()));
		request.put(Constants.TAG_ADDRESS_MAIN,
				mTempAddress.length() > 0 ? mTempAddress : "invalid");
		request.put(Constants.TAG_BIRTHDAY,
				mTempBirth.length() > 0 ? mTempBirth : "9999-12-31");
		request.put(
				Constants.TAG_GENDER,
				mTempGender == -1 ? Integer.toString(99) : Integer
						.toString(mTempGender));
		request.put(Constants.TAG_LANGUAGE, mTempLanguage);
		request.put(Constants.TAG_SNS_CODE, ""); // TODO For facebook
		request.put(Constants.TAG_SNS_ID, ""); // TODO For facebook
		request.put(Constants.TAG_SNS_USER_NAME, ""); // TODO For facebook
		request.put(Constants.TAG_CLIENT_IP, Util.getIp(getContext()));

		mRequestSingUp = new RequestSignUp(getContext(), request);
		mRequestSingUp.setCallback(this);
		Model.runOnWorkerThread(mRequestSingUp);
		mProgressDialog = Util.openProgressDialog(getContext());
	}

	private void modifyEmail() {
		final TextView oldPassView = (TextView) findViewById(R.id.user_detail_old_password);
		final TextView passView = (TextView) findViewById(R.id.user_detail_edit_password);
		final TextView passConfirmView = (TextView) findViewById(R.id.user_detail_confirm_password);
		final TextView birthView = (TextView) findViewById(R.id.user_detail_edit_birthday);
		final TextView countryView = (TextView) findViewById(R.id.user_detail_edit_country);
		final TextView cityView = (TextView) findViewById(R.id.user_detail_edit_city);
		final RadioGroup genderGroup = (RadioGroup) findViewById(R.id.user_detail_select_gender);

		final int genderId = genderGroup.getCheckedRadioButtonId();
		final RadioButton genderButton = (RadioButton) genderGroup
				.findViewById(genderId);
		final GenderType gender;
		if (genderId == R.id.user_detail_gender_male) {
			gender = GenderType.MALE;
		} else if (genderId == R.id.user_detail_gender_female) {
			gender = GenderType.FEMALE;
		} else {
			gender = null;
		}

		String oldPwd = oldPassView.getText().toString();
		String newPwd = passView.getText().toString();
		String newConfirmPwd = passConfirmView.getText().toString();
		if (!"".equals(newPwd)
				&& newPwd.length() < Constants.PASSWORD_MIN_LENGTH) {
			FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_edit_password_error);
			errorView.setText(R.string.pf_net_error_password_short);
			errorView.setVisibility(View.VISIBLE);
			return;
		}
		if (!newPwd.equals(newConfirmPwd)) {
			FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_confirm_password_error);
			errorView.setText(R.string.pf_net_error_password_incorrect);
			errorView.setVisibility(View.VISIBLE);
			return;
		}
		if ("".equals(oldPwd)) {
			FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_old_password_error);
			errorView.setText(R.string.pf_editaccount_popup_input_password);
			errorView.setVisibility(View.VISIBLE);
			return;
		}

		final Locale current = getResources().getConfiguration().locale;
		final String lang = current.getLanguage();

		mTempCountry = getCurrentCountryCode();
		mTempAddress = cityView.getText().toString();
		mTempBirth = birthView.getText().toString();
		if (mTempBirth.length() > 0)
			mTempBirth += "-01-01";
		mTempGender = (gender == null ? -1 : gender.type);
		mTempLanguage = lang;
		final HashMap<String, String> request = new HashMap<String, String>();
		request.put(Constants.TAG_USER_NO, String
				.valueOf(BobbysStoryWorldPreference.getInt(
						Constants.PREF_USER_NO, 0)));
		request.put(Constants.TAG_USER_ID, BobbysStoryWorldPreference
				.getString(Constants.PREF_USER_ID, ""));
		request.put(Constants.TAG_COUNTRY, mTempCountry == null ? "PH"
				: mTempCountry);
		request.put(Constants.TAG_LANGUAGE, mTempLanguage);
		request.put(
				Constants.TAG_GENDER,
				mTempGender == -1 ? Integer.toString(99) : Integer
						.toString(mTempGender));
		request.put(Constants.TAG_BIRTHDAY,
				mTempBirth.length() > 0 ? mTempBirth : "9999-12-31");
		request.put(Constants.TAG_OLD_PASSWORD, oldPwd);
		request.put(Constants.TAG_NEW_PASSWORD, newPwd);
		request.put(Constants.TAG_ADDRESS_MAIN,
				mTempAddress.length() > 0 ? mTempAddress : "invalid");
		request.put(Constants.TAG_CLIENT_IP, Util.getIp(getContext()));

		mModifyMobile = new ModifyMobileProfile(getContext(), request);
		mModifyMobile.setCallback(this);
		Model.runOnWorkerThread(mModifyMobile);
		mProgressDialog = Util.openProgressDialog(getContext());
	}

	private void modifySnsInfo() {
		final TextView birthView = (TextView) findViewById(R.id.user_detail_edit_birthday);
		final TextView countryView = (TextView) findViewById(R.id.user_detail_edit_country);
		final TextView cityView = (TextView) findViewById(R.id.user_detail_edit_city);
		final RadioGroup genderGroup = (RadioGroup) findViewById(R.id.user_detail_select_gender);

		final int genderId = genderGroup.getCheckedRadioButtonId();
		final GenderType gender;
		if (genderId == R.id.user_detail_gender_male) {
			gender = GenderType.MALE;
		} else if (genderId == R.id.user_detail_gender_female) {
			gender = GenderType.FEMALE;
		} else {
			gender = null;
		}

		final Locale current = getResources().getConfiguration().locale;
		final String lang = current.getLanguage();

		mTempCountry = getCurrentCountryCode();
		mTempAddress = cityView.getText().toString(); // TODO
		mTempBirth = birthView.getText().toString();
		if (mTempBirth.length() > 0)
			mTempBirth += "-01-01";
		mTempGender = (gender == null ? -1 : gender.type);
		mTempLanguage = lang;
		final HashMap<String, String> request = new HashMap<String, String>();
		request.put(Constants.TAG_USER_NO, String
				.valueOf(BobbysStoryWorldPreference.getInt(
						Constants.PREF_USER_NO, 0)));
		request.put(Constants.TAG_USER_ID, BobbysStoryWorldPreference
				.getString(Constants.PREF_USER_ID, ""));
		request.put(Constants.TAG_COUNTRY, mTempCountry == null ? "PH"
				: mTempCountry);
		request.put(Constants.TAG_LANGUAGE, mTempLanguage);
		request.put(
				Constants.TAG_GENDER,
				mTempGender == -1 ? Integer.toString(99) : Integer
						.toString(mTempGender));
		request.put(Constants.TAG_BIRTHDAY,
				mTempBirth.length() > 0 ? mTempBirth : "9999-12-31");
		request.put(Constants.TAG_ADDRESS_MAIN,
				mTempAddress.length() > 0 ? mTempAddress : "invalid");
		request.put(Constants.TAG_CLIENT_IP, Util.getIp(getContext()));

		RequestModifySNSUserProfile requestTask = new RequestModifySNSUserProfile(
				getContext(), request);
		requestTask.setCallback(this);
		Model.runOnWorkerThread(requestTask);
		mProgressDialog = Util.openProgressDialog(getContext());
	}

	private void requestSignupSns() {
		if (false && mMode == SIGN_UP_FACEBOOK) {
			Bundle args = ((PortraitActivity) getContext())
					.getSavedDataBundle();
			mTempUserId = args.getString(Constants.TAG_USER_ID, "");

			BobbysStoryWorldPreference.putString(Constants.PREF_USER_ID,
					mTempUserId);
			BobbysStoryWorldPreference.putInt(Constants.PREF_USER_NO, 12341234); // temp
			BobbysStoryWorldPreference.putString(Constants.PREF_AUTH_KEY,
					"ASDFQWEFQWREF"); // temp
			BobbysStoryWorldPreference.putInt(Constants.PREF_LOGIN_TYPE,
					mIsFacebookLogin ? Constants.LOGIN_TYPE_FACEBOOK
							: Constants.LOGIN_TYPE_EMAIL);

			((PortraitActivity) getContext()).saveAuthData();
			((PortraitActivity) getContext()).loginFinished(false);

			return;
		}
		mIsFacebookLogin = true;

		final TextView birthView = (TextView) findViewById(R.id.user_detail_edit_birthday);
		final TextView cityView = (TextView) findViewById(R.id.user_detail_edit_city);
		final RadioGroup genderGroup = (RadioGroup) findViewById(R.id.user_detail_select_gender);

		final int genderId = genderGroup.getCheckedRadioButtonId();
		final GenderType gender;
		if (genderId == R.id.user_detail_gender_male) {
			gender = GenderType.MALE;
		} else if (genderId == R.id.user_detail_gender_female) {
			gender = GenderType.FEMALE;
		} else {
			gender = null;
		}

		Bundle args = ((PortraitActivity) getContext()).getSavedDataBundle();
		mTempUserId = args.getString(Constants.TAG_USER_ID, "");
		mTempCountry = args.getString(Constants.TAG_STANDARD_COUNTRY_CODE, "");
		mTempAddress = cityView.getText().toString(); // TODO
		mTempBirth = birthView.getText().toString();
		if (mTempBirth.length() > 0)
			mTempBirth += "-01-01";
		mTempGender = (gender == null ? -1 : gender.type);

		HashMap<String, String> request = new HashMap<String, String>();
		request.put(Constants.TAG_USER_ID, mTempUserId);
		request.put(Constants.TAG_PASSWORD, "");
		request.put(Constants.TAG_EMAIL,
				args.getString(Constants.TAG_EMAIL, ""));
		request.put(Constants.TAG_STANDARD_COUNTRY_CODE, mTempCountry);
		request.put(Constants.TAG_UUID, Util.getUUID(getContext()));
		request.put(Constants.TAG_ADDRESS_MAIN,
				mTempAddress.length() > 0 ? mTempAddress : "invalid");
		request.put(Constants.TAG_BIRTHDAY,
				mTempBirth.length() > 0 ? mTempBirth : "9999-12-31");
		request.put(
				Constants.TAG_GENDER,
				mTempGender == -1 ? Integer.toString(99) : Integer
						.toString(mTempGender));
		request.put(Constants.TAG_LANGUAGE,
				args.getString(Constants.TAG_LANGUAGE, ""));
		request.put(Constants.TAG_SNS_CODE, "SNS001");
		request.put(Constants.TAG_SNS_ID,
				args.getString(Constants.TAG_SNS_ID, ""));
		request.put(Constants.TAG_SNS_USER_NAME,
				args.getString(Constants.TAG_SNS_USER_NAME, ""));
		request.put(Constants.TAG_CLIENT_IP, Util.getIp(getContext()));

		args.remove(Constants.TAG_USER_ID);
		args.remove(Constants.TAG_EMAIL);
		args.remove(Constants.TAG_STANDARD_COUNTRY_CODE);
		args.remove(Constants.TAG_ADDRESS_MAIN);
		args.remove(Constants.TAG_BIRTHDAY);
		args.remove(Constants.TAG_GENDER);
		args.remove(Constants.TAG_LANGUAGE);
		args.remove(Constants.TAG_SNS_ID);
		args.remove(Constants.TAG_SNS_USER_NAME);

		RequestSignUp requestSignUp = new RequestSignUp(getContext(), request);
		requestSignUp.setCallback(this);
		Model.runOnWorkerThread(requestSignUp);
		mProgressDialog = Util.openProgressDialog(getContext());
	}

	private void requestSNSLogin() {
		final HashMap<String, String> request = new HashMap<String, String>();
		request.put(Constants.TAG_UUID, Util.getUUID(getContext()));
		request.put(Constants.TAG_SNS_TYPE, "SNS001");
		request.put(Constants.TAG_SNSID, mTempUserId);
		request.put(Constants.TAG_DEVICE_TOKEN, "");
		request.put(Constants.TAG_CLIENT_IP, Util.getIp(getContext()));
		request.put(Constants.TAG_MOBILE_OS, Constants.OS_STRING);
		request.put(Constants.TAG_SERVICE_CODE, Constants.SERVICE_CODE);

		RequestSNSLogin snsLogin = new RequestSNSLogin(getContext(), request);
		snsLogin.setCallback(this);
		Model.runOnWorkerThread(snsLogin);
	}

	@Override
	public void onSuccess(ServerTask parser, Map<String, String> result) {
		android.util.Log.d(TAG, " callback success = " + result);
		// TODO Do it here?
		if (parser instanceof RequestSignUp) {
			if (mMode == SIGN_UP_EMAIL) {
				mTempUserNo = result.get(Constants.TAG_USER_NO);

				final HashMap<String, String> request = new HashMap<String, String>();
				request.put(Constants.TAG_USER_ID, mTempUserId);
				request.put(Constants.TAG_PASSWORD, mTempPassword);
				request.put(Constants.TAG_SERVICE_CODE, Constants.SERVICE_CODE);
				request.put(Constants.TAG_DEVICE_TOKEN, "");
				request.put(Constants.TAG_UUID, Util.getUUID(getContext()));
				request.put(Constants.TAG_CLIENT_IP, Util.getIp(getContext()));

				RequestAuthKey requestAuthKey = new RequestAuthKey(
						getContext(), request);
				requestAuthKey.setCallback(this);
				Model.runOnWorkerThread(requestAuthKey);
			} else if (mMode == SIGN_UP_FACEBOOK) {
				requestSNSLogin();
			}
		} else if (parser instanceof RequestAuthKey) {
			BobbysStoryWorldPreference.putString(Constants.PREF_USER_ID,
					mTempUserId);
			BobbysStoryWorldPreference.putInt(Constants.PREF_USER_NO,
					Integer.valueOf(mTempUserNo));
			BobbysStoryWorldPreference.putString(Constants.PREF_AUTH_KEY,
					result.get(Constants.TAG_AUTHKEY));
			BobbysStoryWorldPreference.putString(Constants.PREF_COUNTRY,
					mTempCountry);
			BobbysStoryWorldPreference.putString(Constants.PREF_LANGUAGE,
					mTempLanguage);
			BobbysStoryWorldPreference.putInt(Constants.PREF_GENDER,
					mTempGender == -1 ? 99 : mTempGender);
			BobbysStoryWorldPreference.putString(Constants.PREF_BIRTHDAY,
					mTempBirth.length() > 0 ? mTempBirth : "9999-12-31");
			BobbysStoryWorldPreference.putString(Constants.PREF_ADDRESS_MAIN,
					mTempAddress);
			BobbysStoryWorldPreference.putInt(Constants.PREF_LOGIN_TYPE,
					Constants.LOGIN_TYPE_EMAIL);

			// MixpanelAPI Create User Profile
			MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(),
					Constants.MIXPANEL_TOKEN);
			mixpanel.alias(mTempUserId, null);
			// mixpanel.identify(mTempUserNo);
			mixpanel.getPeople().identify(mixpanel.getDistinctId());
			mixpanel.getPeople().initPushHandling(Constants.GCM_SENDER_ID);
			mixpanel.getPeople().set("$email", mTempUserId);
			mixpanel.getPeople().set("$created",
					result.get(Constants.TAG_REGISTER_DATE_TIME));
			mixpanel.getPeople().set("$first_name", "");
			mixpanel.getPeople().set("$last_name", "");
			mixpanel.getPeople().set("$name", "");
			mixpanel.getPeople().set("$ip", Util.getIp(getContext()));
			mixpanel.getPeople().set("city",
					result.get(Constants.TAG_ADDRESS_MAIN));
			mixpanel.getPeople().set("gender",
					mTempGender == -1 ? 99 : mTempGender);
			if (mTempBirth!=null) {
				String birthyear = mTempBirth.length() > 4? mTempBirth.substring(0,4) : "2015";
				mixpanel.getPeople().set("birthday", birthyear);
			}
			else {
				mixpanel.getPeople().set("birthday", "2015");
			}

			// MixpanelAPI Track Account Creation
			// mixpanel.track(Constants.TAG_SUCCESSFUL_EMAIL, null);
			// mixpanel.track(Constants.TAG_COMPLETED_EMAIL, null);
			// mixpanel.track(Constants.TAG_AUTHENTICATED_EMAIL, null);
			// mixpanel.track(Constants.TAG_REGISTRATION_TIME_EMAIL, null);

			// JP added post 2.0.0
			JSONObject props = new JSONObject();
			try {
				props.put("User Type", "Registered");
				props.put("Email", mTempUserId);
				mixpanel.registerSuperProperties(props);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			props = new JSONObject();
			if (mMode == SIGN_UP_EMAIL) {
				try {
					props.put("Authentication", "Email");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			mixpanel.track(Constants.TAG_SUCCESSFULLY_CREATED_ACCOUNT, props);
			mixpanel.flush();

			((PortraitActivity) getContext()).saveAuthData();

			if (mMode == SIGN_UP_EMAIL) {
				HashMap<String, String> request = new HashMap<String, String>();
				request.put(Constants.TAG_USER_NO, mTempUserNo);
				request.put(Constants.TAG_CLIENT_IP, Util.getIp(getContext()));

				GetBalance getBalance = new GetBalance(getContext(), request);
				getBalance.setCallback(this);
				Model.runOnWorkerThread(getBalance);
			} else {
				// mProgressDialog.dismiss();
				// ((PortraitActivity)getContext()).loginFinished();
			}
		} else if (parser instanceof GetBalance) {
			String balance = result.get(Constants.TAG_EVENT_BALANCE);
			BobbysStoryWorldPreference.putInt(Constants.PREF_LAST_DOLE_COIN,
					Integer.valueOf(balance));

			((PortraitActivity) getContext()).saveDoleCoinBalance();

			if (mProgressDialog != null) {
				mProgressDialog.dismiss();
			}
			mProgressDialog = null;

			Bundle data = ((PortraitActivity) getContext())
					.getSavedDataBundle();
			boolean isFirst = false;
			if (data != null) {
				isFirst = data.getBoolean(PortraitActivity.FIRST_LAUNCH, false);
				data.remove(PortraitActivity.FIRST_LAUNCH);
			}
			((PortraitActivity) getContext()).loginFinished(!isFirst);
		} else if (parser instanceof ModifyMobileProfile) {
			BobbysStoryWorldPreference.putString(Constants.PREF_COUNTRY,
					mTempCountry);
			BobbysStoryWorldPreference.putString(Constants.PREF_LANGUAGE,
					mTempLanguage);
			BobbysStoryWorldPreference.putInt(Constants.PREF_GENDER,
					mTempGender == -1 ? 99 : mTempGender);
			BobbysStoryWorldPreference.putString(Constants.PREF_BIRTHDAY,
					mTempBirth.length() > 0 ? mTempBirth : "9999-12-31");
			BobbysStoryWorldPreference.putString(Constants.PREF_ADDRESS_MAIN,
					mTempAddress);
			Toast.makeText(
					getContext(),
					getResources().getString(
							R.string.pf_editaccount_popup_save_complete),
					Toast.LENGTH_SHORT).show();
			mProgressDialog.dismiss();
			mProgressDialog = null;
			((PortraitActivity) getContext()).onBackPressed();
		} else if (parser instanceof RequestModifySNSUserProfile) {
			BobbysStoryWorldPreference.putString(Constants.PREF_COUNTRY,
					mTempCountry);
			BobbysStoryWorldPreference.putString(Constants.PREF_LANGUAGE,
					mTempLanguage);
			BobbysStoryWorldPreference.putInt(Constants.PREF_GENDER,
					mTempGender == -1 ? 99 : mTempGender);
			BobbysStoryWorldPreference.putString(Constants.PREF_BIRTHDAY,
					mTempBirth.length() > 0 ? mTempBirth : "9999-12-31");
			BobbysStoryWorldPreference.putString(Constants.PREF_ADDRESS_MAIN,
					mTempAddress);
			Toast.makeText(
					getContext(),
					getResources().getString(
							R.string.pf_editaccount_popup_save_complete),
					Toast.LENGTH_SHORT).show();
			mProgressDialog.dismiss();
			mProgressDialog = null;
			((PortraitActivity) getContext()).onBackPressed();
		} else if (parser instanceof RequestSNSLogin) {
			android.util.Log.e(TAG, "RequestAuthKey");
			mTempUserNo = result.get(Constants.TAG_USER_NO);
			mTempAuthKey = result.get(Constants.TAG_AUTHKEY);

			final HashMap<String, String> request = new HashMap<String, String>();
			request.put(Constants.TAG_USER_NO, mTempUserNo);
			request.put(Constants.TAG_AUTHKEY, mTempAuthKey);
			request.put(Constants.TAG_CLIENT_IP, Util.getIp(getContext()));

			GetMobileProfile getMobileProfile = new GetMobileProfile(
					getContext(), request);
			getMobileProfile.setCallback(this);
			Model.runOnWorkerThread(getMobileProfile);
		} else if (parser instanceof GetMobileProfile) {
			BobbysStoryWorldPreference.putString(Constants.PREF_USER_ID,
					mTempUserId);
			BobbysStoryWorldPreference.putInt(Constants.PREF_USER_NO,
					Integer.valueOf(mTempUserNo));
			BobbysStoryWorldPreference.putString(Constants.PREF_AUTH_KEY,
					mTempAuthKey);
			BobbysStoryWorldPreference.putBoolean(
					Constants.PREF_EMAIL_VALIFICATION, Boolean.valueOf(result
							.get(Constants.TAG_EMAIL_VALIFICATION)));
			BobbysStoryWorldPreference.putString(
					Constants.PREF_EMAIL_ACTIVATE_DATE_TIME,
					result.get(Constants.TAG_EMAIL_ACTIVATE_DATE_TIME));
			BobbysStoryWorldPreference.putString(
					Constants.PREF_CHANGE_PASSWROD_DATE_TIME,
					result.get(Constants.TAG_CHANGE_PASSWROD_DATE_TIME));
			BobbysStoryWorldPreference.putString(
					Constants.PREF_REGISTER_DATE_TIME,
					result.get(Constants.TAG_REGISTER_DATE_TIME));
			BobbysStoryWorldPreference.putString(Constants.PREF_COUNTRY,
					result.get(Constants.TAG_COUNTRY));
			BobbysStoryWorldPreference.putString(Constants.PREF_LANGUAGE,
					result.get(Constants.TAG_LANGUAGE));
			BobbysStoryWorldPreference.putInt(Constants.PREF_GENDER,
					Integer.valueOf(result.get(Constants.TAG_GENDER)));
			BobbysStoryWorldPreference.putString(Constants.PREF_BIRTHDAY,
					result.get(Constants.TAG_BIRTHDAY));
			BobbysStoryWorldPreference.putString(Constants.PREF_ADDRESS_MAIN,
					result.get(Constants.TAG_ADDRESS_MAIN));
			BobbysStoryWorldPreference.putInt(Constants.PREF_LOGIN_TYPE,
					mIsFacebookLogin ? Constants.LOGIN_TYPE_FACEBOOK
							: Constants.LOGIN_TYPE_EMAIL);

			// MixpanelAPI Create User Profile
			MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(),
					Constants.MIXPANEL_TOKEN);
			mixpanel.alias(mTempUserId, null);
			// mixpanel.identify(mTempUserNo);
			mixpanel.getPeople().identify(mixpanel.getDistinctId());
			mixpanel.getPeople().set("$email", mTempUserId);
			mixpanel.getPeople().set("$created",
					result.get(Constants.TAG_REGISTER_DATE_TIME));
			mixpanel.getPeople().set("$first_name", "");
			mixpanel.getPeople().set("$last_name", "");
			mixpanel.getPeople().set("$name", "");
			mixpanel.getPeople().set("$ip", Util.getIp(getContext()));
			mixpanel.getPeople().set("city",
					result.get(Constants.TAG_ADDRESS_MAIN));
			mixpanel.getPeople().set("gender",
					mTempGender == -1 ? 99 : mTempGender);
			if (mTempBirth!=null) {
				String birthyear = mTempBirth.length() > 4? mTempBirth.substring(0,4) : "2015";
				mixpanel.getPeople().set("birthday", birthyear);
			}
			else {
				mixpanel.getPeople().set("birthday", "2015");
			}

			// JP added
			JSONObject props = new JSONObject();
			try {
				props.put("User Type", "Registered");
				mixpanel.registerSuperProperties(props);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// MixpanelAPI Track Account Creation
			props = new JSONObject();
			try {
				props.put("Authentication", "Facebook");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mixpanel.track(Constants.TAG_DIRECT_LOGIN, props);
			// mixpanel.track(Constants.TAG_SUCCESSFULLY_CREATED_ACCOUNT,
			// props);
			mixpanel.flush();

			((PortraitActivity) getContext()).saveAuthData();

			if (mMode == SIGN_UP_FACEBOOK) {
				HashMap<String, String> request = new HashMap<String, String>();
				request.put(Constants.TAG_USER_NO, mTempUserNo);
				request.put(Constants.TAG_CLIENT_IP, Util.getIp(getContext()));

				GetBalance getBalance = new GetBalance(getContext(), request);
				getBalance.setCallback(this);
				Model.runOnWorkerThread(getBalance);
			} else {
				// mProgressDialog.dismiss();
				// ((PortraitActivity)getContext()).loginFinished();
			}
		}
		// else if(parser instanceof GetProvinceInfo) {
		// if(mProgressDialog != null) {
		// mProgressDialog.dismiss();
		// mProgressDialog = null;
		// }
		//
		// final String cities = result.get(Constants.TAG_PROVINCES);
		// try {
		// if(cities != null) {
		// JSONArray array = new JSONArray(cities);
		// CityDialog dialog = new CityDialog(getContext(), array,
		// mCityClickListener);
		// dialog.show();
		// }
		// } catch(JSONException e) {
		// e.printStackTrace();
		// }
		// }
	}

	@Override
	public void onFailed(ServerTask parser, Map<String, String> result,
			int returnCode) {
		android.util.Log.d(TAG, " callback fail = " + result);
		if (parser instanceof GetProvinceInfo) {
			if (mProgressDialog != null) {
				mProgressDialog.dismiss();
				mProgressDialog = null;
			}

			TextView cityView = (TextView) findViewById(R.id.user_detail_edit_city);
			cityView.setText(R.string.comm_other);
		} else if (Util.isEmailError(returnCode)) {
			FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_edit_email_error);
			errorView.setText(Util.switchErrorCode(returnCode, getResources()));
			errorView.setVisibility(View.VISIBLE);
		} else if (Util.isPasswordError(returnCode)) {
			FontTextView errorView = (FontTextView) findViewById(R.id.user_detail_old_password_error);
			errorView.setText(Util.switchErrorCode(returnCode, getResources()));
			errorView.setVisibility(View.VISIBLE);
		} else {
			Toast.makeText(getContext(),
					Util.switchErrorCode(returnCode, getResources()),
					Toast.LENGTH_SHORT).show();
		}
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
			mProgressDialog = null;
		}
	}

	@Override
	public boolean onCheckCancel() {
		return handleCancelButtonClick();
	}

	private String getCurrentCountry(String country) {
		if (country == null) {
			mCurrentSelectedCountry = 0;
			return null;
		} else if (country.equals("SG")) {
			mCurrentSelectedCountry = CountryDialog.SG;
			return "Singapore";
		} else if (country.equals("JP")) {
			mCurrentSelectedCountry = CountryDialog.JP;
			return "日本";
		} else if (country.equals("NZ")) {
			mCurrentSelectedCountry = CountryDialog.NZ;
			return "New Zealand";
		} else if (country.equals("KR")) {
			mCurrentSelectedCountry = CountryDialog.KR;
			return "한국";
		} else if (country.equals("PH")) {
			mCurrentSelectedCountry = CountryDialog.PH;
			return "Philippines";
		} else if (country.equals("AE")) {
			mCurrentSelectedCountry = CountryDialog.AE;
			return "United Arab Emirates";
//		} else if (country.equals("AU")) {
//			mCurrentSelectedCountry = CountryDialog.PH;
//			return "Australia";
		} else {
			mCurrentSelectedCountry = 0;
			return null;
		}
	}

	private boolean updateCurrentCountry(String country) {
		int code = mCurrentSelectedCountry;
		if (country.equals("Singapore")) {
			mCurrentSelectedCountry = CountryDialog.SG;
		} else if (country.equals("日本")) {
			mCurrentSelectedCountry = CountryDialog.JP;
		} else if (country.equals("New Zealand")) {
			mCurrentSelectedCountry = CountryDialog.NZ;
		} else if (country.equals("한국")) {
			mCurrentSelectedCountry = CountryDialog.KR;
		} else if (country.equals("Philippines")) {
			mCurrentSelectedCountry = CountryDialog.PH;
		} else if (country.equals("United Arab Emirates")) {
			mCurrentSelectedCountry = CountryDialog.AE;
//		} else if (country.equals("Australia")) {
//			mCurrentSelectedCountry = CountryDialog.AU;
		} else {
			mCurrentSelectedCountry = 0;
		}
		if (code != mCurrentSelectedCountry)
			return true;
		else
			return false;
	}

	private String getCurrentCountryCode() {
		switch (mCurrentSelectedCountry) {
		case CountryDialog.SG:
			return "SG";

		case CountryDialog.JP:
			return "JP";

		case CountryDialog.NZ:
			return "NZ";

		case CountryDialog.KR:
			return "KR";

		case CountryDialog.PH:
			return "PH";
			
		case CountryDialog.AE:
			return "AE";

//		case CountryDialog.AU:
//			return "AU";

		//case CountryDialog.ETC:
		default:
			return null;
		}
	}

	// @Override
	// public void onActivityResult(int requestCode, int resultCode, Intent
	// data) {
	// super.onActivityResult(requestCode, resultCode, data);
	//
	// if(resultCode != Activity.RESULT_OK)
	// return;
	//
	// switch (requestCode) {
	// case Constants.REQUEST_CODE_DATE_DIALOG:
	// TextView birthText =
	// (TextView)findViewById(R.id.user_detail_edit_birthday);
	// final int year = data.getIntExtra(BirthDayPickerDialog.EXTRA_YEAR, 0);
	// birthText.setText(Integer.toString(year));
	// break;
	//
	// case Constants.REQUEST_CODE_AGREE_PERSONAL:
	// boolean isNext = data.getBooleanExtra(Constants.CLICK_NEXT, false);
	// mIsCheckedNext = isNext;
	// break;
	// case Constants.REQUEST_USER_CONFIRM:
	// // if(resultCode == Activity.RESULT_OK)
	// // getFragmentManager().popBackStackImmediate();
	// break;
	// case Constants.REQUEST_CITY:
	// if(resultCode == Activity.RESULT_OK) {
	// String city = data.getStringExtra(Constants.CITY);
	// TextView cityText = (TextView)findViewById(R.id.user_detail_edit_city);
	// cityText.setText(city);
	// }
	// default:
	// break;
	// }
	// }
}
