#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# pos-converter, v1.0
#
# Jaemok Jeong, 2014/04/15

import csv
import re
import os

headerClassRe = re.compile(r'(\s*// POS_H_CLASS_START.*?\n)(.*)\n(\s*// POS_H_CLASS_END.*?\n)',re.M|re.S)
headerDefineRe = re.compile(r'(\s*// POS_H_DEFINE_START.*?\n)(.*)\n(\s*// POS_H_DEFINE_END.*?\n)',re.M|re.S)
cppFuncRe = re.compile(r'(\s*// POS_C_FUNC_START.*?\n)(.*)\n(\s*// POS_C_FUNC_END.*?\n)',re.M|re.S)

def process(input,outputh,outputc):

    headerClassStr = u""
    headerDefineStr = u""
    cppFuncStr = u""

    input = os.path.abspath(input)
    outputh = os.path.abspath(outputh)
    outputc = os.path.abspath(outputc)
    
    reader = csv.DictReader(open(input), delimiter=',')
    for row in reader:
        name = row['name']
        androidfullhd = row['androidfullhd'].split('|')
        androidhd = row['androidhd'].split('|')
        iphone35 = row['iphone35'].split('|')
        iphone4 = row['iphone4'].split('|')
        ipadhd = row['ipadhd'].split('|')

        if name.startswith('s_'):
            name = name.strip('s_')

            headerClassStr += "    cocos2d::Size %s();\n" % name
            headerDefineStr += "#define %s\t\tResourceManager::getInstance()->%s()\n" % (name.upper(), name)
            cppFuncStr += """
cocos2d::Size ResourceManager::%s() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(%s,%s);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(%s,%s);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(%s,%s);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((%s)/2,(%s)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(%s,%s);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(%s,%s);
        }
    }
    return cocos2d::Size::ZERO;
}
""" % (name,androidfullhd[0],androidfullhd[1],androidhd[0],androidhd[1],ipadhd[0],ipadhd[1],ipadhd[0],ipadhd[1],iphone4[0],iphone4[1],iphone35[0],iphone35[1])
        elif name.startswith('!'):
            name = name.strip('!')

            headerClassStr += "    float %s();\n" % name
            headerDefineStr += "#define %s\t\tResourceManager::getInstance()->%s()\n" % (name.upper(), name)
            if len(androidfullhd)==1:
                cppFuncStr += """
float ResourceManager::%s() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (%s);
        }
        case ResolutionType::androidhd: {
            return (%s);
        }
        case ResolutionType::ipadhd: {
            return (%s);
        }
        case ResolutionType::ipad: {
            return (%s)/2;
        }
        case ResolutionType::iphone4: {
            return (%s);
        }
        case ResolutionType::iphone35: {
            return (%s);
        }
    }
    return 0.0f;
}
""" % (name,androidfullhd[0],androidhd[0],ipadhd[0],ipadhd[0],iphone4[0],iphone35[0])
            else:
                cppFuncStr += """
float ResourceManager::%s() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (%s);
                } break;
                case LanguageType::KOREAN: {
                    return (%s);
                } break;
                case LanguageType::JAPANESE: {
                    return (%s);
                } break;
            }
        }
        case ResolutionType::androidhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (%s);
                } break;
                case LanguageType::KOREAN: {
                    return (%s);
                } break;
                case LanguageType::JAPANESE: {
                    return (%s);
                } break;
            }
        }
        case ResolutionType::ipadhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (%s);
                } break;
                case LanguageType::KOREAN: {
                    return (%s);
                } break;
                case LanguageType::JAPANESE: {
                    return (%s);
                } break;
            }
        }
        case ResolutionType::ipad: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (%s)/2;
                } break;
                case LanguageType::KOREAN: {
                    return (%s)/2;
                } break;
                case LanguageType::JAPANESE: {
                    return (%s)/2;
                } break;
            }
        }
        case ResolutionType::iphone4: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (%s);
                } break;
                case LanguageType::KOREAN: {
                    return (%s);
                } break;
                case LanguageType::JAPANESE: {
                    return (%s);
                } break;
            }
        }
        case ResolutionType::iphone35: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (%s);
                } break;
                case LanguageType::KOREAN: {
                    return (%s);
                } break;
                case LanguageType::JAPANESE: {
                    return (%s);
                } break;
            }
        }
    }
    return 0.0f;
}
"""%(name,androidfullhd[0],androidfullhd[1],androidfullhd[2],androidhd[0],androidhd[1],androidhd[2],ipadhd[0],ipadhd[1],ipadhd[2],ipadhd[0],ipadhd[1],ipadhd[2],iphone4[0],iphone4[1],iphone4[2],iphone35[0],iphone35[1],iphone35[2])
        else:
            headerClassStr += "    cocos2d::Point %s();\n" % name
            headerDefineStr += "#define %s\t\tResourceManager::getInstance()->%s()\n" % (name.upper(), name)
            cppFuncStr += """
cocos2d::Point ResourceManager::%s() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(%s,%s);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(%s,%s);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(%s,%s);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((%s)/2,(%s)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(%s,%s);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(%s,%s);
        }
    }
    return cocos2d::Point::ZERO;
}
""" % (name,androidfullhd[0],androidfullhd[1],androidhd[0],androidhd[1],ipadhd[0],ipadhd[1],ipadhd[0],ipadhd[1],iphone4[0],iphone4[1],iphone35[0],iphone35[1])

    hfile = open(outputh).read()
    hfile_new =  headerClassRe.sub("\\1"+headerClassStr+"\\3", hfile)
    hfile_new = headerDefineRe.sub("\\1"+headerDefineStr+"\\3", hfile_new)

    with open(outputh, 'w+') as f:
        f.write(hfile_new)

    print "Write " + outputh

    cfile = open(outputc).read()
    cfile_new = cppFuncRe.sub("\\1"+cppFuncStr+"\\3",cfile)

    with open(outputc, 'w+') as f:
        f.write(cfile_new)

    print "Write " + outputc

if __name__ == '__main__':
    process("../pos.csv", "../Classes/ResourceManager.h", "../Classes/ResourceManager.cpp")
