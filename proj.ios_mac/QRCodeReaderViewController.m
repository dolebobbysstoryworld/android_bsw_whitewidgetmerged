//
//  QRCodeReaderViewController.m
//  bobby
//
//  Created by oasis on 2014. 5. 21..
//
//

#ifndef MACOS

#import "QRCodeReaderViewController.h"
#import "ZBarCameraSimulator.h"
#import "UIImage+PathExtention.h"
#import "TSDolesNet.h"
#import "TSMessageView.h"
#import "TSProxyHelper.h"
#import "TSDoleCoinViewController.h"
#import "TSToastView.h"
#import "TSProxyHelper.h"
#import "AppController.h"
#include "Define.h"

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#define TAG_NET_CHECKMOBILECOUPON           987001
#define TAG_NET_USECOINCOUPON               987002
#define TAG_POPUP_FAIL_CHECKCOUPON          987301
#define TAG_POPUP_FAIL_COUPONUNUSED         987302
#define TAG_POPUP_FAIL_USEDOLECOIN          987303
#define TAG_POPUP_RECEIVED_USEDOLECOIN      987304
#define TAG_VIEW_ACTIVITYINDICATOR          987901

#define CODE_LENGTH 16

@interface QRCodeReaderViewController () < TSNetDelegate, TSPopupViewDelegate >
@property (nonatomic, strong) ZBarReaderView *readerView;
@property (nonatomic, strong) UITextField *codeField;
@property (nonatomic, strong) ZBarCameraSimulator *cameraSim;
@property (nonatomic, strong) UIImageView *frameImageView; 
@property (nonatomic, strong) UIImageView *errorImageView;
@property (nonatomic, strong) UILabel *codeLabel;

@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) TSNet *net;
@property (nonatomic, assign) NSInteger receivedCoin;

@end

@implementation QRCodeReaderViewController


- (id)init
{
    self = [super init];
    if (self) {
        CGRect screenFrame = [[UIScreen mainScreen] bounds];
        NSLog(@"%@", NSStringFromCGRect(screenFrame));
#ifdef __IPHONE_8_0
        if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
            self.view.frame = CGRectMake(screenFrame.origin.x,screenFrame.origin.y, screenFrame.size.height, screenFrame.size.width);
        } else {
//            self.view.frame = CGRectMake(screenFrame.origin.x,screenFrame.origin.y, screenFrame.size.width, screenFrame.size.height);
        }
#else
        self.view.frame = CGRectMake(screenFrame.origin.x,screenFrame.origin.y, screenFrame.size.height, screenFrame.size.width);
#endif
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    NSLog(@"%@", NSStringFromCGRect(self.view.frame));
#ifdef __IPHONE_8_0
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
    } else {
//        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.height, self.view.frame.size.width);
    }
#endif
    self.view.autoresizingMask = UIViewAutoresizingNone;
    CGRect padView;
    if (IS_IOS8) {
      padView = self.view.bounds;//CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width);
    } else {
        padView = CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width);
    }
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.readerView = [ZBarReaderView new];
    self.readerView.readerDelegate = self;
//    [self.readerView.scanner setSymbology:0 config:ZBAR_CFG_ENABLE to:0];
//    [self.readerView.scanner setSymbology:ZBAR_QRCODE config:ZBAR_CFG_ENABLE to:1];
    self.readerView.tracksSymbols = YES;
//    self.readerView.frame = CGRectMake(40, self.codeField.frame.origin.y + self.codeField.frame.size.height + 40, rect.size.width - (40*2), 240);
    if (IS_IPAD) {
        self.readerView.frame = padView;
    } else {
        self.readerView.frame = self.view.bounds;
    }
    self.readerView.zoom = 0.0f;
    [self.readerView.session setSessionPreset:AVCaptureSessionPresetHigh];
    
    self.readerView.trackingColor = [UIColor colorWithRed:63.0f/255.0f green:196.0f/255.0f blue:208.0f/255.0f alpha:255.0f/255.0f];
    self.readerView.torchMode = AVCaptureTorchModeOff;
    self.readerView.autoresizingMask = UIViewAutoresizingNone;
    self.readerView.previewTransform = CGAffineTransformIdentity;
    
    [self.readerView start];
    [self.view addSubview:self.readerView];
    
    AVCaptureDevice *device = [[self readerView] device];
    [device addObserver:self forKeyPath:@"adjustingExposure" options:NSKeyValueObservingOptionNew context:nil];
    [device addObserver:self forKeyPath:@"adjustingFocus" options:NSKeyValueObservingOptionNew context:nil];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.readerView.previewTransform = CGAffineTransformMakeRotation([self interfaceOrientationForPad:[UIApplication sharedApplication].statusBarOrientation]);
    }
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.readerView addGestureRecognizer:singleTap];
    
    CGRect cancelButtonRect;
    UIImage *cancelImage = [UIImage imageNamedEx:@"btn_cancel_normal.png" ignore:YES];
    UIImage *cancelPressImage = [UIImage imageNamedEx:@"btn_cancel_press.png" ignore:YES];
    if (IS_IPAD) {
//        cancelImage = [UIImage imageNamed:@"iosnative/btn_cancel_normal_pad.png"];
//        cancelPressImage = [UIImage imageNamed:@"iosnative/btn_cancel_press_pad.png"];
        cancelButtonRect = CGRectMake(padView.size.width-56/2-cancelImage.size.width, 59/2, cancelImage.size.width, cancelImage.size.height);
    } else {
//        cancelImage = [UIImage imageNamed:@"iosnative/btn_cancel_normal.png"];
//        cancelPressImage = [UIImage imageNamed:@"iosnative/btn_cancel_press_pad.png"];
        cancelButtonRect = CGRectMake(self.view.frame.size.width-18/2-cancelImage.size.width, 18/2, cancelImage.size.width, cancelImage.size.height);
    }
    
    self.cancelButton = [[UIButton alloc] initWithFrame:cancelButtonRect];
    [self.cancelButton setImage:cancelImage forState:UIControlStateNormal];
    [self.cancelButton setImage:cancelPressImage forState:UIControlStateHighlighted];
    [self.cancelButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.cancelButton];
    
    CGRect codeFieldBgRect;
    CGRect fieldRect;
    int fieldFontSize = 0;
    UIImage *tempImg = [UIImage imageNamedEx:@"qr_code_input.png" ignore:YES];
    UIImage *errorTempImg = [UIImage imageNamedEx:@"qr_code_input_error.png" ignore:YES];
    UIImage *fieldbgImage = nil;
    UIImage *fieldErrorImage = nil;
//    CGRect codeLabelRect;
    CGRect codeErrorRect;
    if (IS_IPAD) {
        codeFieldBgRect = CGRectMake(padView.size.width/2 - (808/2)/2, 62/2, 808/2, 120/2);
        fieldRect = CGRectMake(codeFieldBgRect.origin.x + 32/2, codeFieldBgRect.origin.y+codeFieldBgRect.size.height/2 - (56/2)/2, (808-32-32)/2, 56/2);
        fieldFontSize = 50/2;
//        UIImage *tempImg = [UIImage imageNamed:@"iosnative/qr_code_input_pad.png"];
//        UIImage *errorTempImg = [UIImage imageNamed:@"iosnative/qr_code_input_error_pad.png"];
        fieldbgImage = [tempImg resizableImageWithCapInsets:UIEdgeInsetsMake((tempImg.size.height-56/2)/2, (50+20)/2, (tempImg.size.height-56/2)/2, (50+20)/2) resizingMode:UIImageResizingModeStretch];
        fieldErrorImage = [errorTempImg resizableImageWithCapInsets:UIEdgeInsetsMake((tempImg.size.height-56/2)/2, (50+20)/2, (tempImg.size.height-56/2)/2, (50+20)/2) resizingMode:UIImageResizingModeStretch];
//        codeLabelRect = CGRectMake(0, fieldRect.size.height/2 -(56/2)/2, fieldRect.size.width, 56/2);
        codeErrorRect = CGRectMake(32/2, ((120-48)/2)/2, fieldRect.size.width, 48/2);
    } else {
        codeFieldBgRect = CGRectMake(44/2 , 20/2, 478/2, 70/2);
        fieldRect = CGRectMake(codeFieldBgRect.origin.x + 26/2, codeFieldBgRect.origin.y + codeFieldBgRect.size.height/2 - (34/2)/2, (478-26-26)/2, 34/2);
        fieldFontSize = 30/2;
        fieldbgImage = [tempImg resizableImageWithCapInsets:UIEdgeInsetsMake((100-34)/2, (26+20)/2, (100-34)/2, (26+20)/2) resizingMode:UIImageResizingModeStretch]; //[UIImage imageNamed:@"iosnative/qr_code_input.png"]
        fieldErrorImage = [errorTempImg resizableImageWithCapInsets:UIEdgeInsetsMake((100-34)/2, (26+20)/2, (100-34)/2, (26+20)/2) resizingMode:UIImageResizingModeStretch]; //[UIImage imageNamed:@"iosnative/qr_code_input_error.png"]
//        codeLabelRect = CGRectMake(0, fieldRect.size.height/2 -(34/2)/2, fieldRect.size.width, 34/2);
        codeErrorRect = CGRectMake(26/2, ((70-34)/2)/2, fieldRect.size.width, 34/2);
    }
    
    UIImageView *fieldbgImageView = [[UIImageView alloc] initWithFrame:codeFieldBgRect];
    fieldbgImageView.image = fieldbgImage;
    fieldbgImageView.frame = codeFieldBgRect;
    fieldbgImageView.contentMode = UIViewContentModeRedraw;
    [self.view addSubview:fieldbgImageView];
    
    self.codeField = [[UITextField alloc] initWithFrame:fieldRect];
    self.codeField.delegate = self;
    self.codeField.placeholder = NSLocalizedString(@"PF_QRCODE_INPUT_CODE", @"Enter the alphanumeric code");
    self.codeField.returnKeyType = UIReturnKeyDone;
    self.codeField.font = [UIFont fontWithName:@"Helvetica" size:fieldFontSize];
    self.codeField.backgroundColor = [UIColor clearColor];
    self.codeField.textColor = [UIColor colorWithRed:159.0f/255.0f green:161.0f/255.0f blue:164.0f/255.0f alpha:255.0/255.0f];
    [self.view addSubview:self.codeField];
    
//    self.codeLabel = [[UILabel alloc] initWithFrame:codeLabelRect];
//    self.codeLabel.text = @"Enter the alphanumeric code";
//    self.codeLabel.textColor = [UIColor colorWithRed:0x9f/255.0f green:0xa1/255.0f blue:0xa4/255.0f alpha:255.0f/255.0f];
//    self.codeLabel.textAlignment = NSTextAlignmentLeft;
//    self.codeLabel.font = [UIFont fontWithName:@"Helvetica" size:(IS_IPAD)?50/2:30/2];
//    [self.codeField addSubview:self.codeLabel];
//    
    self.errorImageView = [[UIImageView alloc] initWithFrame:codeFieldBgRect];
    self.errorImageView.image = fieldErrorImage;
    self.errorImageView.frame = codeFieldBgRect;
    self.errorImageView.contentMode = UIViewContentModeRedraw;
    [self.view addSubview:self.errorImageView];
    
    UILabel *codeErrorLabel = [[UILabel alloc] initWithFrame:codeErrorRect];
    codeErrorLabel.text = [NSString stringWithFormat:NSLocalizedString(@"PF_NET_ERROR__QRCODE_INPUT", @"Enter the %i characters of the code"), CODE_LENGTH];
    codeErrorLabel.textColor = [UIColor colorWithRed:0xfe/255.0f green:0xc3/255.0f blue:0x46/255.0f alpha:255.0f/255.0f];
    codeErrorLabel.textAlignment = NSTextAlignmentCenter;
    codeErrorLabel.font = [UIFont fontWithName:@"Helvetica" size:(IS_IPAD)?44/2:26/2];
    [self.errorImageView addSubview:codeErrorLabel];

    [self validCode:true];
    
    if(TARGET_IPHONE_SIMULATOR) {
        self.cameraSim = [[ZBarCameraSimulator alloc]
                          initWithViewController: self];
        self.cameraSim.readerView = self.readerView;
    }


    UIImage *frameNorImg = [UIImage imageNamedEx:@"qr_code_frame_normal.png" ignore:YES];
    CGRect frameRect;
    if (IS_IPAD) {
//        frameNorImg = [UIImage imageNamed:@"iosnative/qr_code_frame_normal_pad.png"];
        frameRect = CGRectMake(padView.size.width/2 - frameNorImg.size.width/2, (padView.size.height - 320/2 - 182/2)/2 - frameNorImg.size.height/2+182/2, frameNorImg.size.width, frameNorImg.size.height);
    } else {
//        frameNorImg = [UIImage imageNamed:@"iosnative/qr_code_frame_normal.png"];
        frameRect = CGRectMake(self.view.frame.size.width/2 - frameNorImg.size.width/2, (self.view.frame.size.height - 196/2 - 90/2)/2 - frameNorImg.size.height/2 + 90/2, frameNorImg.size.width, frameNorImg.size.height);
    }
    
    self.frameImageView = [[UIImageView alloc] initWithImage:frameNorImg];
    self.frameImageView.frame = frameRect;
    [self.view addSubview:self.frameImageView];
    
    UIImage *bgImage = [UIImage imageNamedEx:@"qr_code_gradient_bg.png" ignore:YES];
    CGRect bgImageRect;
    if (IS_IPAD) {
//        bgImage = [UIImage imageNamed:@"iosnative/qr_code_gradient_bg_pad.png"];
        bgImageRect = CGRectMake(0, padView.size.height-bgImage.size.height, padView.size.width, bgImage.size.height);
    } else {
//        bgImage = [UIImage imageNamed:@"iosnative/qr_code_gradient_bg.png"];
        bgImageRect = CGRectMake(0, self.view.frame.size.height-bgImage.size.height, self.view.frame.size.width, bgImage.size.height);
    }
    
    UIImageView *bgImageView = [[UIImageView alloc] initWithImage:bgImage];
    bgImageView.frame = bgImageRect;
    bgImageView.contentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:bgImageView];
    
    UIImage *guideImage = [UIImage imageNamedEx:@"qrcode_manual.png" ignore:YES];
    CGRect guideRect;
    CGRect guideLabelRect;
    int guideFontSize = 0;
    if (IS_IPAD) {
//        guideImage = [UIImage imageNamed:@"iosnative/qrcode_manual_pad.png"];
        guideRect = CGRectMake(padView.size.width/2 - guideImage.size.width/2, padView.size.height - guideImage.size.height - (64/2), guideImage.size.width, guideImage.size.height);
        guideLabelRect = CGRectMake(padView.size.width/2 - guideImage.size.width/2, padView.size.height - 64/2-254/2-54/2-48/2, 932/2, 48/2);
        guideFontSize = 43/2;
    } else {
//        guideImage = [UIImage imageNamed:@"iosnative/qrcode_manual.png"];
        guideRect = CGRectMake(self.view.frame.size.width/2 - guideImage.size.width/2, self.view.frame.size.height - guideImage.size.height - (44/2), guideImage.size.width, guideImage.size.height);
        guideLabelRect = CGRectMake(self.view.frame.size.width/2 - guideImage.size.width/2, self.view.frame.size.height - 44/2 - 152/2 - 32/2 - 30/2, 552/2, 30/2);
        guideFontSize = 26/2;
    }

    UIImageView *guideImageView = [[UIImageView alloc] initWithImage:guideImage];
    guideImageView.frame = guideRect;
    [self.view addSubview:guideImageView];
    
    UIColor *color = [UIColor colorWithRed:255.0f/255.0f green:181.0f/255.0f blue:40.0f/255.0f alpha:255.0f/255.0f];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"PF_DOLECOIN_BUTTONTITLE_SCAN", @"Scan QR code")];
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0]; //@"ja",@"ko",@"en"
    if ([language isEqualToString:@"ja"]) {//QRコードをスキャン 6,4 0,6
        [str addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(6,4)];
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0,6)];
    } else if ([language isEqualToString:@"ko"]) {//QR 코드 스캔하기
        [str addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(6,2)];
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0,6)];
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(8,2)];
    } else {
        [str addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0,4)];
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(5,7)];
    }
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica" size:guideFontSize] range:NSMakeRange(0,str.length)];
    
    UILabel *guideLabel = [[UILabel alloc] initWithFrame:guideLabelRect];
    guideLabel.textAlignment = NSTextAlignmentCenter;
    guideLabel.attributedText = str;
    [self.view addSubview:guideLabel];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChanged:) name:UITextFieldTextDidChangeNotification object:self.codeField];
    
    //hans added
    [self testUserCoupon];
}

- (void) testUserCoupon
{
    //hans added
    self.receivedCoin = 500;
    self.net = [[TSNetUseCoinCoupon alloc] initWithSerial:@"124577788QWWYEUE"];
    self.net.tag = TAG_NET_USECOINCOUPON;
    self.net.delegate = self;
    [self.net start];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    if (!self.codeField.isFirstResponder || self.codeField.enabled == NO) return;
    
    [self.view endEditing:YES];
//    [self codeLabelhiddenControl];
    
    if (self.codeField.text.length > 0 && self.codeField.text.length <= CODE_LENGTH-1) {
        [self validCode:false];
    }
    
    [self codeCheck];
}

- (void)codeCheck
{
    /*-----old code------
    if (self.codeField.text.length >= CODE_LENGTH) {
        [self validCode:YES];
        [self.readerView stop];
        self.net = [[TSNetCheckCoupon alloc] initWithSerial:self.codeField.text];
        self.net.tag = TAG_NET_CHECKMOBILECOUPON;
        self.net.delegate = self;
        [self.net start];
    } else {
        [self validCode:false];
    }
     */
    
    //hans added
        [self validCode:YES];
        [self.readerView stop];
        self.net = [[TSNetCheckCoupon alloc] initWithSerial:@"124577788QWWYEUE"];//assumed I passed scanning successfully
        self.net.tag = TAG_NET_CHECKMOBILECOUPON;
        self.net.delegate = self;
        [self.net start];
   
    
}

- (void)validCode:(BOOL)isCodeValid
{
    self.errorImageView.hidden = isCodeValid;
}

#pragma UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.codeLabel.hidden = YES;
    
    [self validCode:YES];
//    self.codeField.text = @"";
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    [self codeCheck];
    
//    if (textField.text.length >= CODE_LENGTH) {
//        [self validCode:YES];
//        [self.readerView stop];
//        self.net = [[TSNetCheckCoupon alloc] initWithSerial:self.codeField.text];
//        self.net.tag = TAG_NET_CHECKMOBILECOUPON;
//        self.net.delegate = self;
//        [self.net start];
//    } else {
//        [self validCode:false];
//    }

    return true;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@""]) return true;
    
    if (textField.text.length > CODE_LENGTH-1) return false;
    
    return true;
}

- (void)textFieldDidChanged:(NSNotification*)notification
{
//    NSLog(@"textFieldDidChanged");
//    [self codeLabelhiddenControl];
}

//- (void)codeLabelhiddenControl
//{
//    if (self.codeField.text.length <= 0) {
//        self.codeLabel.hidden = NO;
//    } else {
//        self.codeLabel.hidden = YES;
//    }
//}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    if (IS_IPAD) {
        return UIInterfaceOrientationMaskLandscape;
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
//    [self.readerView willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.readerView.previewTransform = CGAffineTransformMakeRotation([self interfaceOrientationForPad:toInterfaceOrientation]);
    }
}

- (CGFloat) interfaceOrientationForPad:(UIInterfaceOrientation)orientation
{
    CGFloat angle = 0;
    switch(orientation)
    {
        case UIInterfaceOrientationLandscapeLeft:
        {
            angle = M_PI_2;
            break;
        }
        case UIInterfaceOrientationPortraitUpsideDown:
        {
            angle = M_PI;
            break;
        }
        case UIInterfaceOrientationLandscapeRight:
        {
            angle = 3 * M_PI_2;
            break;
        }
        case UIInterfaceOrientationPortrait:
        {
            angle = (2 * M_PI);
            break;
        }
    }
    return angle;
}

- (void)viewWillDisappear: (BOOL) animated
{
    AVCaptureDevice *device = [[self readerView] device];
    [device removeObserver:self forKeyPath:@"adjustingExposure"];
    [device removeObserver:self forKeyPath:@"adjustingFocus"];
    
    [self.readerView stop];
    [self.readerView flushCache];
    [self.readerView removeFromSuperview];
    self.readerView = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)backAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) readerView:(ZBarReaderView *)readerView didReadSymbols:(ZBarSymbolSet *)symbols fromImage:(UIImage *)image
{
    [self validCode:true];
    UIImage *frameNorImg = [UIImage imageNamedEx:@"qr_code_frame_selected.png" ignore:YES];
//    if (IS_IPAD) {
//        frameNorImg = [UIImage imageNamed:@"iosnative/qr_code_frame_selected_pad.png"];
//    } else {
//        frameNorImg = [UIImage imageNamed:@"iosnative/qr_code_frame_selected.png"];
//    }
    for(ZBarSymbol *symbol in symbols) {
        NSLog(@"Scan Code : %@", symbol.data);
        NSString *symbolData = symbol.data;
        NSString *qrCode = [symbolData substringWithRange:NSMakeRange([symbolData length]-16, 16)];
        NSLog(@"Parsed Code : %@", qrCode);
//        NSString *validateUrlStr = [symbolData substringWithRange:NSMakeRange(0, 7)];
//        NSString *qrCode = symbol.data;
//        if ([validateUrlStr isEqualToString:@"http://"]) {
//            qrCode = [qrCode substringWithRange:NSMakeRange(qrCode.length-CODE_LENGTH, CODE_LENGTH)];
//        }
        self.frameImageView.image = frameNorImg;
        self.codeField.text = qrCode;
        
        [self.readerView stop];
        self.net = [[TSNetCheckCoupon alloc] initWithSerial:self.codeField.text];
        self.net.tag = TAG_NET_CHECKMOBILECOUPON;
        self.net.delegate = self;
        [self.net start];
        break;
    }
}

#pragma mark - TSNetDelegate
- (void)net:(TSNet*)netObject didStartProcess:(TSNetResultType)result {
    switch (netObject.tag) {
        case TAG_NET_CHECKMOBILECOUPON: {
            self.codeField.enabled = NO;
            self.cancelButton.enabled = NO;

            UIActivityIndicatorView *indicatorView =
            [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            indicatorView.center = CGPointMake(self.codeField.frame.size.width/2, self.codeField.frame.size.height/2);
            indicatorView.tag = TAG_VIEW_ACTIVITYINDICATOR;
            [self.codeField addSubview:indicatorView];
            [indicatorView startAnimating];
        } break;
        case TAG_NET_USECOINCOUPON: {

        } break;
        default: { } break;
    }
}

- (void)net:(TSNet*)netObject didEndProcess:(TSNetResultType)result {
    switch (netObject.tag) {
        case TAG_NET_CHECKMOBILECOUPON: {
            if (!result) {
                if (((TSNetCheckCoupon*)self.net).canUsable == YES)
                {//사용 가능
                    self.receivedCoin = ((TSNetCheckCoupon*)self.net).coinValue;
                    self.net = [[TSNetUseCoinCoupon alloc] initWithSerial:self.codeField.text];
                    self.net.tag = TAG_NET_USECOINCOUPON;
                    self.net.delegate = self;
                    [self.net start];
                }
                else
                {//사용 불가능
//                    [self validCode:false];
                    TSMessageView *messageView =
                    [[TSMessageView alloc] initWithMessage:NSLocalizedString(@"PF_QRCODE_ERROR_USED", @"This code cannot be used.") icon:TSPopupIconTypeWarning];
                    messageView.buttonLayout = TSPopupButtonLayoutOK;
                    messageView.tag = TAG_POPUP_FAIL_COUPONUNUSED;
                    messageView.delegate = self;
                    [messageView showInViewForLandscape:self.view];
                }
            }
            else
            {//tuilise_todo : 오류 결과다. 맞는 처리 필요
//                [self validCode:false];
                TSMessageView *messageView =
                [[TSMessageView alloc] initWithMessage:NSLocalizedString(@"PF_QRCODE_ERROR_USED", @"This code cannot be used.") icon:TSPopupIconTypeWarning];
                messageView.buttonLayout = TSPopupButtonLayoutOK;
                messageView.tag = TAG_POPUP_FAIL_COUPONUNUSED;
                messageView.delegate = self;
                [messageView showInViewForLandscape:self.view];
//                TSMessageView *messageView =
//                [[TSMessageView alloc] initWithMessage:[NSString stringWithFormat:@"QR Scan Fail\n%@", [netObject serverErrorString]] icon:TSPopupIconTypeWarning];
//                messageView.buttonLayout = TSPopupButtonLayoutOK;
//                messageView.tag = TAG_POPUP_FAIL_CHECKCOUPON;
//                messageView.delegate = self;
//                [messageView showInView:self.view];
            }
        } break;
        case TAG_NET_USECOINCOUPON: {
            if (!result) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kTSNotifivationDoleCoinUpdate object:nil
                                                                  userInfo:@{@"originlayer": [NSNumber numberWithInteger:self.originLayer]}];//충전 결과를 업데이트 하자.
                UIActivityIndicatorView *indicatorView =
                indicatorView = (UIActivityIndicatorView*)[self.codeField viewWithTag:TAG_VIEW_ACTIVITYINDICATOR];
                if (indicatorView) {
                    [indicatorView stopAnimating];
                    [indicatorView removeFromSuperview];
                }

                TSMessageView *messageView =
                [[TSMessageView alloc] initWithMessage:[NSString stringWithFormat:
                                                        NSLocalizedString(@"PF_POPUP_RECEIVED_DOLECOIN", @"Congratulations!\nYou received %d Dole Coins."),
                                                        self.receivedCoin] icon:TSPopupIconTypeCoin];
                messageView.buttonLayout = TSPopupButtonLayoutOK;
                messageView.tag = TAG_POPUP_RECEIVED_USEDOLECOIN;
                messageView.delegate = self;
                [messageView showInViewForLandscape:self.view];
                
            } else {//tuilise_todo : 오류 결과다. 맞는 처리 필요
                TSMessageView *messageView =
                [[TSMessageView alloc] initWithMessage:[NSString stringWithFormat:@"Server Error\n%@", [netObject serverErrorString]] icon:TSPopupIconTypeWarning];
                messageView.buttonLayout = TSPopupButtonLayoutOK;
                messageView.tag = TAG_POPUP_FAIL_USEDOLECOIN;
                messageView.delegate = self;
                [messageView showInViewForLandscape:self.view];
            }
        } break;
        default: { } break;
    }
}

- (void)net:(TSNet*)netObject didFailWithError:(TSNetResultType)result {
    switch (result) {
        case TSNetErrorNetworkDisable:
        case TSNetErrorTimeout: {
//            TSToastView *toast = [[TSToastView alloc] initWithString:NSLocalizedString(@"CM_NET_WARNING_NETWORK_UNSTABLE", @"The network is unstable.\nPlease try again")];
//            [toast showInViewWithAnimation:self.view];
            
            TSMessageView *messageView =
            [[TSMessageView alloc] initWithMessage:
             NSLocalizedString(@"PF_QRCODE_ERROR_NETWORK_NOT_CONNCET", @"The code cannot be recognized\nbecause ther is no network\nconnection.")
                                              icon:TSPopupIconTypeWarning];
            messageView.buttonLayout = TSPopupButtonLayoutOK;
            messageView.tag = TAG_POPUP_FAIL_USEDOLECOIN;
            messageView.delegate = self;
            [messageView showInViewForLandscape:self.view];
            
        } break;
        default: {
            TSMessageView *messageView =
            [[TSMessageView alloc] initWithMessage:@"server communication error!!!" icon:TSPopupIconTypeWarning];
            messageView.buttonLayout = TSPopupButtonLayoutOK;
            //    messageView.tag = TAG_POPUP_FAIL_USEDOLECOIN;
            messageView.delegate = self;
            [messageView showInViewForLandscape:self.view];
        } break;
    }

    switch (netObject.tag) {
        case TAG_NET_CHECKMOBILECOUPON: {

        } break;
        case TAG_NET_USECOINCOUPON: {

        } break;
        default: { } break;
    }
}

- (void)popview:(TSPopupView *)view didClickedButton:(TSPopupButtonType)type {
    switch (view.tag) {
        case TAG_POPUP_FAIL_CHECKCOUPON: {
        } break;
        case TAG_POPUP_FAIL_COUPONUNUSED: {
        } break;
        case TAG_POPUP_FAIL_USEDOLECOIN: {
#ifdef UI_TYPE_IOS8
            [self dismissViewControllerAnimated:YES completion:nil];
#else
            [self dismissViewControllerAnimated:NO completion:nil];
            UIViewController *viewController = nil;
            if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
            } else {
                viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
            }
            NSLog(@"viewController screen frame : %@",NSStringFromCGRect(viewController.view.frame));
            TSDoleCoinViewController *doleCoinViewController = [[TSDoleCoinViewController alloc] init];
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                doleCoinViewController.modalPresentationStyle = UIModalPresentationFormSheet;
            }
            doleCoinViewController.originLayer = self.originLayer;
            if(IS_IOS8 && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                doleCoinViewController.preferredContentSize = CGSizeMake(1024, 768);
            }
            [viewController presentViewController:doleCoinViewController animated:NO completion:nil];
#endif
        } break;
        case TAG_POPUP_RECEIVED_USEDOLECOIN: {
#ifdef UI_TYPE_IOS8
            [self dismissViewControllerAnimated:YES completion:nil];
#else
            [self dismissViewControllerAnimated:NO completion:nil];
            UIViewController *viewController = nil;
            if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
            } else {
                viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
            }
            NSLog(@"viewController screen frame : %@",NSStringFromCGRect(viewController.view.frame));
            TSDoleCoinViewController *doleCoinViewController = [[TSDoleCoinViewController alloc] init];
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                doleCoinViewController.modalPresentationStyle = UIModalPresentationFormSheet;
            }
            doleCoinViewController.originLayer = self.originLayer;
            if(IS_IOS8 && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                doleCoinViewController.preferredContentSize = CGSizeMake(1024, 768);
            }
            [viewController presentViewController:doleCoinViewController animated:NO completion:nil];
#endif
        } break;
        default:
            break;
    }
    UIActivityIndicatorView *indicatorView =
    indicatorView = (UIActivityIndicatorView*)[self.codeField viewWithTag:TAG_VIEW_ACTIVITYINDICATOR];
    if (indicatorView) {
        [indicatorView stopAnimating];
        [indicatorView removeFromSuperview];
    }
    self.codeField.enabled = YES;
    self.cancelButton.enabled = YES;
    self.codeField.text = @"";
    [self.readerView start];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    [self autoFocus];
//    if( [keyPath isEqualToString:@"adjustingExposure"] ){
//        [self autoFocus];
//    }
}

- (void)autoFocus
{
    AVCaptureDevice *device = [[self readerView] device];
    if ([device isFocusPointOfInterestSupported] && [device isFocusModeSupported:AVCaptureFocusModeAutoFocus]) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            [device setFocusMode:AVCaptureFocusModeAutoFocus];
            [device unlockForConfiguration];
        } else {
            [device lockForConfiguration:&error];
        }
    }
    
//    AVCaptureDevice *device = [[self readerView] device];
//    NSError *error;
//    if ([device position] == AVCaptureDevicePositionBack) {
//        [device lockForConfiguration:&error];
//        if ([device isFocusModeSupported:AVCaptureFocusModeAutoFocus]) {
//            device.focusMode = AVCaptureFocusModeAutoFocus;
//        }
//        [device unlockForConfiguration];
//    }
}

@end

#endif