//
//  DKImageCell.m
//
//  Created by dkhamsing on 3/20/14.
//
//
#ifndef MACOS
#import "DKImageCell.h"
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

@implementation DKImageCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        CGRect screenRect = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width);
        NSLog(@"screenRect : %@", NSStringFromCGRect(screenRect));
        CGRect main;
        CGRect thumb;
        if (IS_IPAD) {
            main = CGRectMake(screenRect.size.width/2 - (1140/2)/2, 0, 1140/2, 1140/2);
            thumb = CGRectMake((358/2)/2 - (250/2)/2, (408/2)/2 - (326/2)/2, 250/2, 326/2);
        } else {
            main = CGRectMake(screenRect.size.width/2 - (410/2)/2, 0, 410/2, 438/2);
            thumb = CGRectMake((172/2)/2- (122/2)/2, (188/2)/2 - (158/2)/2, 122/2, 158/2);
        }
//        self.contentView.layer.borderColor = [UIColor greenColor].CGColor;
//        self.contentView.layer.borderWidth = 1;
        self.DKImageView = [[UIImageView alloc] initWithFrame:main];
        self.DKImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.DKImageView];
        
        self.thumbView = [[UIImageView alloc] initWithFrame:thumb];
        self.thumbView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.thumbView];
        
        self.testLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 120, 100, 50)];
        self.testLabel.textColor = [UIColor whiteColor];
        [self.contentView addSubview:self.testLabel];
        
        UIImage *selectMarkImage = [self getSelectedImageForDevice];
        selectMark = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.size.width/2 - selectMarkImage.size.width/2, self.bounds.size.height/2 - selectMarkImage.size.height/2, selectMarkImage.size.width, selectMarkImage.size.height)];
        selectMark.image = nil;
        [self.contentView addSubview:selectMark];
        self.selected = NO;
        
    }
    return self;
}


- (void)prepareForReuse {
    [super prepareForReuse];
    self.DKImageView.image = nil;
    self.thumbView.image = nil;
    selectMark.image = nil;
    self.testLabel.text = nil;
}

- (void)setSelectedCell:(BOOL)selected
{
    if (selected) {
        selectMark.image = [self getSelectedImageForDevice];
        self.selected = YES;
    } else {
        selectMark.image = nil;
        self.selected = NO;
    }
}

-(UIImage*)getSelectedImageForDevice
{
    UIImage *selectMarkImage = nil;
    if (IS_IPAD) {
        selectMarkImage = [UIImage imageNamed:@"iosnative/background_frame_check_ipad.png"];
    } else {
        selectMarkImage = [UIImage imageNamed:@"iosnative/background_frame_check.png"];
    }
    return selectMarkImage;
}

@end
#endif
