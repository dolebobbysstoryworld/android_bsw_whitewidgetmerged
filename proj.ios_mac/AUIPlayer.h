//
//  AUIPlayer.h
//  bobby
//
//  Created by Dongwook, Kim on 14. 7. 28..
//
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

typedef enum {
    Sound_none,
    Sound_tap,
    Sound_character_walk,
    Sound_journey_map,
    Sound_bigger,
    Sound_smaller,
} SOUND_TYPE;

@interface AUIPlayer : NSObject

+ (id)sharedPlayer;
- (void)play:(SOUND_TYPE)s_type;
- (void)stop;

@end
