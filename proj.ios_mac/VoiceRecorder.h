//
//  VoiceRecorder.h
//  bobby
//
//  Created by Dongwook, Kim on 14. 6. 26..
//
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface VoiceRecorder : NSObject<AVAudioRecorderDelegate, AVAudioPlayerDelegate>

+(id)sharedRecorder;

- (void)createRecorder:(NSString *)fileName;
- (void)startRecord:(NSString *)fileName;
- (void)pauseRecord;
- (void)resumeRecord;
- (void)stopRecord;
- (void)deleteRecord;

- (void)removeFile:(NSString *)fileName;

- (void)play:(NSString *)fileName;
- (void)pause;
- (void)resume;
- (void)stop;
- (void)clear;

@end
