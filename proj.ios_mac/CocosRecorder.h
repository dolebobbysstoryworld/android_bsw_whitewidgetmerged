//
//  CocosRecorder.h
//  bobby
//
//  Created by Dongwook, Kim on 14. 5. 9..
//
//

#import "cocos2d.h"
#import <Foundation/Foundation.h>
#import "RenderTextureRecorder.h"

@interface CocosRecorder : NSObject

@property (nonatomic, readonly) BOOL isWriting;
@property (nonatomic, readonly) BOOL isPaused;
@property (nonatomic) CVPixelBufferRef renderTarget;
@property (nonatomic, assign) RenderTextureRecorder *renderTextureRecorder;

+(id)sharedRecorder;
-(void)startRecording;
-(void)finishRecording;
-(void)cancelRecording;
-(void)deleteRecordedFile;
-(void)createWriter;
-(void)muxAudio:(NSString *)voiceFile withBgm:(NSString *)bgm duration:(double)durationInMili;
-(void)pauseRecording;
-(void)resumeRecording;
-(void)writePixel;
-(void)createFBO;

@end
