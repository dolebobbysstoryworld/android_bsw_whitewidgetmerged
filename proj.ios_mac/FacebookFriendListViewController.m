//
//  FacebookFriendListViewController.m
//  bobby
//
//  Created by Dongwook, Kim on 14. 5. 7..
//
//

#ifndef MACOS

#import <FacebookSDK/FacebookSDK.h>
#import "UIImage+PathExtention.h"
#import "FacebookFriendListViewController.h"
#import "CustomFacebookFriendListCell.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>

#define APP_ID  @"459077664237188"

#define IS_IPAD     (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_4INCH    ([[UIScreen mainScreen] bounds].size.height == 568)

@interface FacebookFriendListViewController ()

@property NSDictionary *friendList;
@property UITableView *tableView;
@property UIImageView *checkMarkView;
@property UIButton *inviteButton;
@property UIView *containerView;

@end

@implementation FacebookFriendListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    CGRect rect = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)?CGRectMake(0, 0, 540, 620):self.view.frame;
    
    CGRect containerViewRect = CGRectMake(0, 0, IS_IPAD?414:[[UIScreen mainScreen] bounds].size.width, IS_IPAD?612:[[UIScreen mainScreen] bounds].size.height);
    self.containerView = [[UIView alloc] initWithFrame:containerViewRect];
    self.containerView.backgroundColor = [UIColor clearColor];
    if (IS_IPAD) {
        self.containerView.frame = CGRectMake((rect.size.width-self.containerView.frame.size.width)/2,
                                         (rect.size.height-self.containerView.frame.size.height)/2,
                                         containerViewRect.size.width, containerViewRect.size.height);
    } else {
        self.containerView.frame = rect;
    }
    
    UIImage *backgroundImage = [UIImage imageNamedEx:@"bg_map_full.png"];
    UIImage *backgroundFactorImage = [UIImage imageNamedEx:@"bg_map_factor_02.png"];
    
    CGRect cancelButtonRect;
    UIImage *cancelImage = [UIImage imageNamedEx:@"btn_cancel_normal.png" ignore:YES];
    UIImage *cancelPressImage = [UIImage imageNamedEx:@"btn_cancel_press.png" ignore:YES];
    if (IS_IPAD) {
        cancelButtonRect = CGRectMake(self.containerView.frame.size.width-56/2-cancelImage.size.width, 59/2, cancelImage.size.width, cancelImage.size.height);
    } else {
        cancelButtonRect = CGRectMake(self.containerView.frame.size.width-18/2-cancelImage.size.width, 18/2, cancelImage.size.width, cancelImage.size.height);
    }
    
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.containerView addSubview:backgroundView];
    
    UIImageView *backgroundFactorView = [[UIImageView alloc] initWithImage:backgroundFactorImage];
    [self.containerView addSubview:backgroundFactorView];
    
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:cancelButtonRect];
    [cancelButton setImage:cancelImage forState:UIControlStateNormal];
    [cancelButton setImage:cancelPressImage forState:UIControlStateHighlighted];
    [cancelButton addTarget:self action:@selector(cancelButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:cancelButton];
    
    // logo
    if (IS_IPAD || IS_4INCH) {
        UIImage *logoImage = [UIImage imageNamedEx:@"dolecoin_logo.png" ignore:YES];
        CGRect logoRect;
        
        if (IS_IPAD) {
            logoRect = CGRectMake(54/2, 58/2, 182/2, 94/2);
        } else {
            logoRect = CGRectMake(20/2, 20/2, 174/2, 88/2);
        }
        
        UIImageView *logoImageView = [[UIImageView alloc] initWithImage:logoImage];
        logoImageView.frame = logoRect;
        [self.containerView addSubview:logoImageView];
    }
    
    float friendListHeight = IS_IPAD?603:IS_4INCH?603:506;
    float friendListStartY = IS_IPAD?387/2-containerViewRect.origin.x:IS_4INCH?169:129;
    float friendListStartX = IS_IPAD?160/2:66/2;
    
    // talk box
    float talkBoxStartY = IS_IPAD?183/2:IS_4INCH?136/2:59/2;
    CGRect labelRect = CGRectMake(friendListStartX+24/2, talkBoxStartY+22/2, 346/2, (90+12)/2);
    UILabel *descLabel = [[UILabel alloc] initWithFrame:labelRect];
    descLabel.font = [UIFont systemFontOfSize:13];
    descLabel.numberOfLines = 3;
    NSString *labelText = @"Introduce the Bobby's world\nto your friends\nand be rewarded!";
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelText length])];
    [attributedString addAttribute:NSForegroundColorAttributeName
                             value:[UIColor colorWithRed:79.0/255.0 green:44.0/255.0 blue:0 alpha:1.0]
                             range:NSMakeRange(0, [labelText length])];
    [attributedString addAttribute:NSForegroundColorAttributeName
                             value:[UIColor colorWithRed:255.0/255.0 green:84.0/255.0 blue:17.0/255.0 alpha:1.0]
                             range:NSMakeRange(14, 13)];
    descLabel.attributedText = attributedString;
    
    UIImage *talkBoxImage = [[UIImage imageNamedEx:@"invite_talk_box.png" ignore:YES] resizableImageWithCapInsets:UIEdgeInsetsMake(22/2, 24/2, 38/2, 24/2)];
    UIImageView *talkBoxImageView = [[UIImageView alloc] initWithImage:talkBoxImage];
    CGRect labelBgRect = CGRectMake(friendListStartX, talkBoxStartY, 394/2, 162/2);
    talkBoxImageView.frame = labelBgRect;
    [self.containerView addSubview:talkBoxImageView];
    [self.containerView addSubview:descLabel];

    // tableview bg
    UIImage *inviteListBgImage = [[UIImage imageNamedEx:@"invite_popup.png" ignore:YES] resizableImageWithCapInsets:UIEdgeInsetsMake(94/2+1, 21/2, 24/2, 21/2)];
    UIImageView *inviteListBgImageView = [[UIImageView alloc] initWithImage:inviteListBgImage];
    CGRect inviteListBgRect = CGRectMake(friendListStartX, friendListStartY, 508/2, friendListHeight/2);
    inviteListBgImageView.frame = inviteListBgRect;
    [self.containerView addSubview:inviteListBgImageView];
    
    // select all
    UIView *selectAllButton = [[UIView alloc] initWithFrame:CGRectMake(friendListStartX, friendListStartY, 508/2, 94/2)];
    selectAllButton.backgroundColor = [UIColor clearColor];

    UILabel *selectAllLabel = [[UILabel alloc] initWithFrame:CGRectMake(21/2, 30/2, 396/2, 34/2)];
    selectAllLabel.font = [UIFont boldSystemFontOfSize:30/2];
    selectAllLabel.text = @"Select all";
    selectAllLabel.textColor = [UIColor whiteColor];

    selectAllLabel.textAlignment = NSTextAlignmentRight;
    [selectAllButton addSubview:selectAllLabel];
    
    UIImage *checkbox = [UIImage imageNamedEx:@"checkbox_off.png" ignore:YES];
    self.checkMarkView = [[UIImageView alloc] initWithImage:checkbox];
    self.checkMarkView.frame = CGRectMake((21+396+16)/2, ((30+34+30)/2 - 56/2)/2, 54/2, 56/2);
    [selectAllButton addSubview:self.checkMarkView];
    
    [selectAllButton addGestureRecognizer: [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleAll)]];
    [self.containerView addSubview:selectAllButton];
    
    // tableview
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(friendListStartX, friendListStartY+94/2, 508/2, (friendListHeight-94)/2 -2)]; // height -2 for shadow
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.allowsMultipleSelection = YES;

    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    [self.tableView setSeparatorColor:[UIColor colorWithRed:162.0/255.0 green:132.0/255.0 blue:89.0/255.0 alpha:1.0] ];

    [self.containerView addSubview:self.tableView];
    
    // monkey
    float talkMonkeyStartY = IS_IPAD?272/2:IS_4INCH?225/2:145/2;
    float talkMonkeyStartX = IS_IPAD?502/2:408/2;
    UIImage *talkMonkeyImage = [UIImage imageNamedEx:@"invite_talk_monkey.png" ignore:YES];
    UIImageView *talkMonkeyImageView = [[UIImageView alloc] initWithImage:talkMonkeyImage];
    CGRect talkMonkeyRect = CGRectMake(talkMonkeyStartX, talkMonkeyStartY, 174/2, 122/2);
    talkMonkeyImageView.frame = talkMonkeyRect;
    [self.containerView addSubview:talkMonkeyImageView];
    
    // invite button
    UIImage *inviteNormalImage = [UIImage imageNamedEx:@"button_normal_01.png" ignore:YES];
    UIImage *invitePressImage = [UIImage imageNamedEx:@"button_press_01.png" ignore:YES];
    UIImage *inviteDisableImage = [UIImage imageNamedEx:@"button_dim_01.png" ignore:YES];
    
    self.inviteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIEdgeInsets resizeCap = UIEdgeInsetsMake(inviteNormalImage.size.height/2, inviteNormalImage.size.width/2-1,
                                               inviteNormalImage.size.height/2, inviteNormalImage.size.width/2+1);
    
    [self.inviteButton setTitle:@"Invite" forState:UIControlStateNormal];
    [self.inviteButton.titleLabel setFont:[UIFont boldSystemFontOfSize:34/2]];
    [self.inviteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.inviteButton setBackgroundImage:[inviteNormalImage resizableImageWithCapInsets:resizeCap] forState:UIControlStateNormal];
    [self.inviteButton setBackgroundImage:[invitePressImage resizableImageWithCapInsets:resizeCap] forState:UIControlStateHighlighted];
    [self.inviteButton setBackgroundImage:[inviteDisableImage resizableImageWithCapInsets:resizeCap] forState:UIControlStateDisabled];
    
    float inviteButtonFromTableView = IS_IPAD?50/2:IS_4INCH?47/2:43/2; // TODO:check ipad value
    CGRect inviteButtonRect = CGRectMake(friendListStartX, CGRectGetMaxY(self.tableView.frame)+inviteButtonFromTableView, 508/2, 74/2);
    self.inviteButton.frame = inviteButtonRect;

    [self.inviteButton addTarget:self action:@selector(inviteButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.inviteButton setEnabled:NO];
    [self.containerView addSubview:self.inviteButton];
    
    if (IS_IPAD) {
        // side characters
        UIImage *leftSideCharacter = [UIImage imageNamedEx:@"full_popup_character_01.png"];
        UIImageView *leftSideCharacterView = [[UIImageView alloc] initWithImage:leftSideCharacter];
        CGRect leftSideCharacterRect = CGRectMake(-143/2, (70+472+210)/2, 296/2, 472/2);
        leftSideCharacterView.frame = leftSideCharacterRect;
        
        UIImage *rightSideCharacter = [UIImage imageNamedEx:@"full_popup_character_02.png"];
        UIImageView *rightSideCharacterView = [[UIImageView alloc] initWithImage:rightSideCharacter];
        CGRect rightSideCharacterRect = CGRectMake(680/2, 70/2, 296/2, 472/2);
        rightSideCharacterView.frame = rightSideCharacterRect;
        
        [self.containerView addSubview:leftSideCharacterView];
        [self.containerView addSubview:rightSideCharacterView];
    }
    
    self.containerView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:self.containerView];
    self.view.backgroundColor = [UIColor clearColor];
    self.friendList = nil;
  
    [self requestFBFriendListBySDK];
//    [self requestFBFriendList];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)inviteButtonPressed
{
    NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];

    NSMutableArray *selectedIDs = [NSMutableArray array];
    for (NSIndexPath *indexPath in selectedRows){
        NSString *friendID = self.friendList[@"data"][indexPath.row][@"id"];
        [selectedIDs addObject:friendID];
    }
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObject:[selectedIDs componentsJoinedByString:@","] forKey:@"to"];
    
    NSString *message = @"Bobby's Adventure message";
    NSString *title = @"Bobby's Adventure";
    
    [FBWebDialogs presentRequestsDialogModallyWithSession:nil
                                                  message:message
                                                    title:title
                                               parameters:params handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                   if (error)
                                                   {
                                                       NSLog(@"Error sending request.");
                                                   }
                                                   else
                                                   {
                                                       if (result == FBWebDialogResultDialogNotCompleted)
                                                       {
                                                           NSLog(@"User canceled request.");
                                                       }
                                                       else
                                                       {
                                                           NSLog(@"Request Sent. %@", params);
                                                       }
                                                   }}];
    NSLog(@"invite!");
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (IS_IPAD) {
        self.view.superview.backgroundColor = [UIColor clearColor];
    }
}

- (void)requestFBFriendListBySDK
{
    if ([[FBSession activeSession] isOpen]) {
        [self loginCallback:YES];
    } else {
        NSArray *permissions = [[NSArray alloc] initWithObjects:
                                @"email", @"user_friends",// @"read_friendlists",
                                nil];
        
        // Attempt to open the session. If the session is not open, show the user the Facebook login UX
        [FBSession openActiveSessionWithReadPermissions:permissions allowLoginUI:true completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
            // Did something go wrong during login? I.e. did the user cancel?
            if (status == FBSessionStateClosedLoginFailed || status == FBSessionStateClosed || status == FBSessionStateCreatedOpening) {
                
                // If so, just send them round the loop again
                [[FBSession activeSession] closeAndClearTokenInformation];
                [FBSession setActiveSession:nil];
                [self createNewSession];
                [self loginCallback:NO];
            }
            else {
                [self loginCallback:YES];
            }
        }];
    }
}

- (void)loginCallback:(BOOL)success
{
    if (success) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSString *fbAccessToken = [[[FBSession activeSession] accessTokenData] accessToken];
            NSString *urlString = [NSString stringWithFormat:@"https://graph.facebook.com/v1.0/me/friends?fields=picture,id,name&access_token=%@",fbAccessToken];
            NSURLRequest *rawRequest = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlString] ];
            NSURLResponse *response;
            NSData *resultData = [NSURLConnection sendSynchronousRequest:rawRequest returningResponse:&response error:NULL];
            
            id json =[NSJSONSerialization JSONObjectWithData:resultData options:kNilOptions error:NULL];
            
//            NSLog(@"Dictionary contains data: %@", json );
            if([json objectForKey:@"error"]!=nil)
            {

            }
            dispatch_async(dispatch_get_main_queue(),^{
                self.friendList = json;
                [self.tableView reloadData];
            });
        });
    } else {
        // TODO:handle error
    }
}

- (void)createNewSession
{
    FBSession* session = [[FBSession alloc] init];
    [FBSession setActiveSession: session];
}

- (void)requestFBFriendList
{
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    __block ACAccount *facebookAccount = nil;
    
    ACAccountType *facebookAccountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    // Specify App ID and permissions
    NSDictionary *options = @{
                              ACFacebookAppIdKey: APP_ID,
                              ACFacebookPermissionsKey: @[@"basic_info", @"read_friendlists", @"publish_stream"],
                              ACFacebookAudienceKey: ACFacebookAudienceFriends
                              };
    
    [accountStore requestAccessToAccountsWithType:facebookAccountType
                                          options:options completion:^(BOOL granted, NSError *error)
     {
         if (granted)
         {
             NSArray *accounts = [accountStore accountsWithAccountType:facebookAccountType];
             
             facebookAccount = [accounts lastObject];
             
             //             NSString *paramName = @"picture,id,name,link,gender,last_name,first_name,username",@"fields";
             NSString *paramName = @"picture,id,name";
             
             NSDictionary *param=[NSDictionary dictionaryWithObjectsAndKeys:paramName,@"fields", nil];
             
             NSURL *feedURL = [NSURL URLWithString:@"https://graph.facebook.com/me/friends"];
             SLRequest *feedRequest = [SLRequest
                                       requestForServiceType:SLServiceTypeFacebook
                                       requestMethod:SLRequestMethodGET
                                       URL:feedURL
                                       parameters:param];
             feedRequest.account = facebookAccount;
             [feedRequest performRequestWithHandler:^(NSData *responseData,
                                                      NSHTTPURLResponse *urlResponse, NSError *error)
              {
                  if(!error)
                  {
                      NSLog(@"url %@", urlResponse.textEncodingName);
                      
                      id json =[NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
                      
                      NSLog(@"Dictionary contains data: %@", json );
                      //                      NSLog(@"data 1 : %@", json[@"data"][0][@"name"]);
                      if([json objectForKey:@"error"]!=nil)
                      {
                          //[self attemptRenewCredentials];
                      }
                      dispatch_async(dispatch_get_main_queue(),^{
                          self.friendList = json;
                          [self.tableView reloadData];
                          //nameLabel.text = [json objectForKey:@"username"];
                      });
                  }
                  else{
                      //handle error gracefully
                      NSLog(@"error from get%@",error);
                      //attempt to revalidate credentials
                  }
              }];
         }
         else {
             NSLog(@"error.localizedDescription======= %@", error.localizedDescription);
         }
     }];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)cancelButtonPressed
{
//    UIViewController *selfViewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
//    for (UIView *subView in [selfViewController.view subviews]) {
//        [subView removeFromSuperview];
//    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)updateButtonStatus
{
    NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];

    if ([selectedRows count] == [self.tableView numberOfRowsInSection:0]) {
        self.checkMarkView.image = [UIImage imageNamedEx:@"checkbox_on.png" ignore:YES];
    } else {
        self.checkMarkView.image = [UIImage imageNamedEx:@"checkbox_off.png" ignore:YES];
    }
    
    if ([selectedRows count] == 0) {
        [self.inviteButton setEnabled:NO];
    } else {
        [self.inviteButton setEnabled:YES];
    }
}

- (void)toggleAll
{
    NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
    if ([selectedRows count] == [self.tableView numberOfRowsInSection:0]) { // unselect
        for (int i = 0; i < [self.tableView numberOfRowsInSection:0]; i++) {
            [self.tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] animated:NO];
        }
    } else {
        for (int i = 0; i < [self.tableView numberOfRowsInSection:0]; i++) {
            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
    }
    
    [self updateButtonStatus];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";

    CustomFacebookFriendListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[CustomFacebookFriendListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }

    cell.tag = indexPath.row;

    cell.nameLabel.text = self.friendList[@"data"][indexPath.row][@"name"];
    cell.thumbNail.image = [UIImage imageNamedEx:@"invite_no_image.png" ignore:YES];
    
    NSNumber *isSilhouette = self.friendList[@"data"][indexPath.row][@"picture"][@"data"][@"is_silhouette"];

    if (![isSilhouette boolValue]) {
        __block NSString *url = self.friendList[@"data"][indexPath.row][@"picture"][@"data"][@"url"];
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^(void) {
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
            UIImage *image = [[UIImage alloc] initWithData:imageData];
            UIImage *resizedImage = [self resizedImage:image inRect:CGRectMake(0, 0, 94/2, 94/2)];
            if (image) {
                UIImage *mask = [UIImage imageNamedEx:@"invite_facebook_image_mask.png" ignore:YES];
                UIImage *maskedImage = [self maskImage:resizedImage withMask:mask];
                
                UIImage *bgImage = [UIImage imageNamedEx:@"invite_image_frame.png" ignore:YES];
                CGRect rect = CGRectMake((bgImage.size.width - maskedImage.size.width)/2,
                                         (bgImage.size.height - maskedImage.size.height)/2,
                                         maskedImage.size.width,  maskedImage.size.height);
                UIImage *framedImage = [self drawImage:maskedImage onImage:bgImage inRect:rect];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (cell.tag == indexPath.row) {
                        cell.thumbNail.image = framedImage;
                        [cell setNeedsLayout];
                    }
                });
            }
        });
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self updateButtonStatus];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self updateButtonStatus];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120/2+1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.friendList) {
        NSDictionary *dataDic = self.friendList[@"data"];
        NSInteger count = [dataDic count];
        return count;
    } else {
        return 0;
    }
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIImage *) maskImage:(UIImage *)image withMask:(UIImage *)maskImage {
    
    CGImageRef maskRef = maskImage.CGImage;
    
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef), NULL, false);
    
    CGImageRef maskedImageRef = CGImageCreateWithMask([image CGImage], mask);
    UIImage *maskedImage = [UIImage imageWithCGImage:maskedImageRef];
    
    CGImageRelease(mask);
    CGImageRelease(maskedImageRef);
    
    // returns new image with mask applied
    return maskedImage;
}

- (UIImage *)drawImage:(UIImage *)inputImage onImage:(UIImage *)onImage inRect:(CGRect)frame {
    UIGraphicsBeginImageContextWithOptions(onImage.size, NO, 0.0);
    [onImage drawInRect:CGRectMake(0.0, 0.0, onImage.size.width, onImage.size.height)];
    [inputImage drawInRect:frame];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(UIImage *)resizedImage:(UIImage*)inImage inRect:(CGRect)thumbRect {
    UIGraphicsBeginImageContext(thumbRect.size);
    [inImage drawInRect:thumbRect];
    return UIGraphicsGetImageFromCurrentImageContext();
}

@end

#endif
