//
//  MixpanelHelper.m
//  bobby
//
//  Created by White Widget on 2/8/15.
//
//

#import "MixpanelHelper.h"
#import "Mixpanel.h"
#import "AppController.h"

#define MIXPANEL_TOKEN @"d2a741b648a2db4f6e67543d2210dae2"

#define MIXPANEL [Mixpanel sharedInstance]


// Sign-up keys

static NSString * const MPPersonKeyEmail        = @"$email";
static NSString * const MPPersonKeyCreated      = @"$created";
static NSString * const MPPersonKeyGender       = @"gender";
static NSString * const MPPersonKeyBirthday     = @"birthday";

static NSString * const MPPersonKeyIp           = @"$ip";

static NSString * const MPPersonKeyCoins        = @"coins";
static NSString * const MPPersonKeyDeleted      = @"deleted_account"; // false by default

// Facebook login

static NSString * const MPPersonFirstName       = @"$first_name";
static NSString * const MPPersonLastName        = @"$last_name";
static NSString * const MPPersonName            = @"$name";


// Engagement ***

static NSString * const MPEngagementKeyStorybook    = @"Storybook";
static NSString * const MPEngagementKeyVideo        = @"Video";
static NSString * const MPEngagementKeyCreatedBy    = @"CreatedBy";

// Event Engagement

static NSString * const MPEngagementEventOpenedBookSample   = @"Opened Book Sample";
static NSString * const MPEngagementEventPlayedVideo        = @"Played Video";

static NSString * const MPEngagementEventCreateStorybookStart       = @"Started Creating Custom Storybook";
static NSString * const MPEngagementEventCreateStoryStart           = @"Started Creating Story in Custom Storybook";

static NSString * const MPEngagementEventCreateStorybookSuccess     = @"Successfully Created Custom Storybook";
static NSString * const MPEngagementEventCreateStorySuccess         = @"Successfully Created Story in Custom Storybook";

// Sample Storybook

static NSString * const MPEngagementStorybookA      = @"Sample A";
static NSString * const MPEngagementStorybookB      = @"Sample B";
static NSString * const MPEngagementStorybookCustom = @"Custom";

// Sample Video

static NSString * const MPEngagementVideo1          = @"Video 1";
static NSString * const MPEngagementVideo2          = @"Video 2";
static NSString * const MPEngagementVideo3          = @"Video 3";
static NSString * const MPEngagementVideo4          = @"Video 4";
static NSString * const MPEngagementVideo5          = @"Video 5";
static NSString * const MPEngagementVideoAll        = @"All";

typedef NS_ENUM(NSInteger, MPHStorybook)
{
    MPHStorybookSampleA,
    MPHStorybookSampleB,
    MPHStorybookSampleCustom,
};

// holder for index
static int s_currentVideo;
static int s_currentBook;
static bool s_isFinal;

static NSDate * s_startTime;


// Account Creation ***

static NSString * const MPAccountCreationEventOpenSignUp                = @"Opened Sign-Up";
static NSString * const MPAccountCreationEventDirectLogIn               = @"Direct Log-In";
static NSString * const MPAccountCreationEventDeleted                   = @"Deleted Account";
static NSString * const MPAccountCreationEventSubmittedFeedback         = @"Feedback Message Sent";
static NSString * const MPAccountCreationEventInputOnRegistrationField  = @"Input Data On Registration Field";

static NSString * const MPAccountCreationEventSuccessfulCreationEmail   = @"Successful Email Account Creation";
static NSString * const MPAccountCreationEventSuccessfulCreationFacebook   = @"Successful Facebook Account Creation";

static NSString * const MPAccountCreationEventCompletedProfileEmail     = @"Email Profile Completed";
static NSString * const MPAccountCreationEventCompletedProfileFacebook  = @"Facebook Profile Completed";

static NSString * const MPAccountCreationEventAuthenticationUsedEmail   = @"Email Authentication Used";
static NSString * const MPAccountCreationEventAuthenticationUsedFacebook   = @"Facebook Authentication Used";

static NSString * const MPAccountCreationEventRegistrationTimeEmail     = @"Email Registration Time";
static NSString * const MPAccountCreationEventRegistrationTimeFacebook  = @"Facebook Registration Time";


static NSString * const MPAccountCreationKeyStartTime       = @"Start Time";
static NSString * const MPAccountCreationKeyEndTime         = @"End Time";



@implementation MixpanelHelper

+ (void)initializeMixpanel
{
    [Mixpanel sharedInstanceWithToken:MIXPANEL_TOKEN];
}

+ (void)signUpWithUserNumber:(NSInteger)userNumber
                       email:(NSString *)email
                      gender:(NSInteger)gender
                   birthyear:(NSString *)birthyear
{
    NSString *userNumberStr = [NSString stringWithFormat: @"%ld", (long)userNumber];
    [MIXPANEL identify:userNumberStr];
    
    NSDate * created = [NSDate date];
    
    AppController *controller = (AppController*)[UIApplication sharedApplication].delegate;
    NSString * ipString = [controller clientIP];
    
    [MIXPANEL.people set:@{
                           MPPersonKeyEmail: email,
                           MPPersonKeyCreated: created,
                           MPPersonKeyGender: [NSNumber numberWithInteger:gender],
                           MPPersonKeyBirthday: birthyear,
                           MPPersonKeyIp: ipString,
                           MPPersonKeyDeleted: [NSNumber numberWithBool:FALSE],
                           }];
    
    // track successful email account creation
    [MIXPANEL track:MPAccountCreationEventSuccessfulCreationEmail];
    [MIXPANEL track:MPAccountCreationEventCompletedProfileEmail];
    [MIXPANEL track:MPAccountCreationEventAuthenticationUsedEmail];
    
    [MIXPANEL track:MPAccountCreationEventRegistrationTimeEmail
         properties:@{
                      MPAccountCreationKeyStartTime : s_startTime,
                      MPAccountCreationKeyEndTime : [NSDate date],
                      }];
}

+ (void)loginWithUserNumber:(NSInteger)userNumber
                      email:(NSString *)email
{
    NSString *userNumberStr = [NSString stringWithFormat: @"%ld", (long)userNumber];
    [MIXPANEL identify:userNumberStr];
    
    AppController *controller = (AppController*)[UIApplication sharedApplication].delegate;
    NSString * ipString = [controller clientIP];
    
    [MIXPANEL.people set:@{
                           MPPersonKeyEmail: email,
                           MPPersonKeyIp: ipString,
                           }];
    
    // track direct login
    [MIXPANEL track:MPAccountCreationEventDirectLogIn];
}

+ (void)loginWithUserNumber:(NSInteger)userNumber
{
    NSString *userNumberStr = [NSString stringWithFormat: @"%ld", (long)userNumber];
    [MIXPANEL identify:userNumberStr];
    
    AppController *controller = (AppController*)[UIApplication sharedApplication].delegate;
    NSString * ipString = [controller clientIP];
    
    [MIXPANEL.people set:@{
                           MPPersonKeyIp: ipString,
                           }];
}

+ (void)modifyUserWithUserNumber:(NSInteger)userNumber
                          gender:(NSInteger)gender
                       birthyear:(NSString *)birthyear
{
    NSString *userNumberStr = [NSString stringWithFormat: @"%ld", (long)userNumber];
    [MIXPANEL identify:userNumberStr];
    
    AppController *controller = (AppController*)[UIApplication sharedApplication].delegate;
    NSString * ipString = [controller clientIP];
    
    [MIXPANEL.people set:@{
                           MPPersonKeyGender: [NSNumber numberWithInteger:gender],
                           MPPersonKeyBirthday: birthyear,
                           MPPersonKeyIp: ipString,
                           }];
}

+ (void)updateUserWithUserNumber:(NSInteger)userNumber email:(NSString *)email gender:(NSInteger)gender birthyear:(NSString *)birthyear
{
    NSString *userNumberStr = [NSString stringWithFormat: @"%ld", (long)userNumber];
    [MIXPANEL identify:userNumberStr];
    
    AppController *controller = (AppController*)[UIApplication sharedApplication].delegate;
    NSString * ipString = [controller clientIP];
    
    [MIXPANEL.people set:@{
                           MPPersonKeyEmail: email,
                           MPPersonKeyGender: [NSNumber numberWithInteger:gender],
                           MPPersonKeyBirthday: birthyear,
                           MPPersonKeyIp: ipString,
                           }];
}


// SNS

+ (void)signUpSNSWithUserNumber:(NSInteger)userNumber
{
    NSString *userNumberStr = [NSString stringWithFormat: @"%ld", (long)userNumber];
    [MIXPANEL identify:userNumberStr];
    
    NSDate * created = [NSDate date];
    
    AppController *controller = (AppController*)[UIApplication sharedApplication].delegate;
    NSString * ipString = [controller clientIP];
    
    [MIXPANEL.people set:@{
                           MPPersonKeyCreated: created,
                           MPPersonKeyIp: ipString,
                           MPPersonKeyDeleted: [NSNumber numberWithBool:FALSE],
                           }];
    
    // track successful email account creation
    [MIXPANEL track:MPAccountCreationEventSuccessfulCreationFacebook];
    [MIXPANEL track:MPAccountCreationEventCompletedProfileFacebook];
    [MIXPANEL track:MPAccountCreationEventAuthenticationUsedFacebook];
    
    [MIXPANEL track:MPAccountCreationEventRegistrationTimeFacebook
         properties:@{
                      MPAccountCreationKeyStartTime : s_startTime,
                      MPAccountCreationKeyEndTime : [NSDate date],
                      }];
}




// tracking

// play sample storybooks

+ (void) logOpenStorybookSampleA
{
    s_currentBook = MPHStorybookSampleA;
    
    [MIXPANEL track:MPEngagementEventOpenedBookSample
         properties:@{
                      MPEngagementKeyStorybook : MPEngagementStorybookA,
                      }];
}

+ (void) logOpenStorybookSampleB
{
    s_currentBook = MPHStorybookSampleB;
    
    [MIXPANEL track:MPEngagementEventOpenedBookSample
         properties:@{
                      MPEngagementKeyStorybook : MPEngagementStorybookB,
                      }];
}

+ (void) logPlayStorybookSampleVideoWithIndex:(int)index
{
    NSArray * books = @[
                        MPEngagementStorybookA,
                        MPEngagementStorybookB,
                        MPEngagementStorybookCustom,
                        ];
    
    NSArray * videos = @[
                         MPEngagementVideo1,
                         MPEngagementVideo2,
                         MPEngagementVideo3,
                         MPEngagementVideo4,
                         MPEngagementVideo5,
                         ];
    
    [MIXPANEL track:MPEngagementEventPlayedVideo
         properties:@{
                      MPEngagementKeyStorybook : books[s_currentBook],
                      MPEngagementKeyVideo : videos[index-1],
                      }];
}

+ (void) logPlayStorybookSampleVideoAll
{
    NSArray * books = @[
                        MPEngagementStorybookA,
                        MPEngagementStorybookB,
                        MPEngagementStorybookCustom,
                        ];
    
    [MIXPANEL track:MPEngagementEventPlayedVideo
         properties:@{
                      MPEngagementKeyStorybook : books[s_currentBook],
                      MPEngagementKeyVideo : MPEngagementVideoAll,
                      }];
}

// custom

+ (void) logCreateCustomStorybookStart
{
    NSString *email = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserEmail"];
    if ([email length] <= 0)
    {
        [MIXPANEL track:MPEngagementEventCreateStorybookStart
             properties:@{
                          MPEngagementKeyStorybook : MPEngagementStorybookCustom,
                          }];
    }
    else
    {
        [MIXPANEL track:MPEngagementEventCreateStorybookStart
             properties:@{
                          MPEngagementKeyStorybook : MPEngagementStorybookCustom,
                          MPEngagementKeyCreatedBy : email,
                          }];
    }
}

+ (void) logCreateCustomStorybookSuccess
{
    NSString *email = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserEmail"];
    if ([email length] <= 0)
    {
        [MIXPANEL track:MPEngagementEventCreateStorybookSuccess
             properties:@{
                          MPEngagementKeyStorybook : MPEngagementStorybookCustom,
                          }];
    }
    else
    {
        [MIXPANEL track:MPEngagementEventCreateStorybookSuccess
             properties:@{
                          MPEngagementKeyStorybook : MPEngagementStorybookCustom,
                          MPEngagementKeyCreatedBy : email,
                          }];
    }
}

+ (void) logCreateStoryStartWithIndex:(int)index isFinal:(BOOL)isFinal
{
    NSString *email = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserEmail"];
    
    s_currentVideo = index-1;
    
    s_isFinal = isFinal;
    
    NSArray * videos = @[
                         MPEngagementVideo1,
                         MPEngagementVideo2,
                         MPEngagementVideo3,
                         MPEngagementVideo4,
                         MPEngagementVideo5,
                         ];
    
    
    if ([email length] <= 0)
    {
        [MIXPANEL track:MPEngagementEventCreateStoryStart
             properties:@{
                          MPEngagementKeyStorybook : MPEngagementStorybookCustom,
                          MPEngagementKeyVideo: videos[index-1],
                          }];
    }
    else
    {
        [MIXPANEL track:MPEngagementEventCreateStoryStart
             properties:@{
                          MPEngagementKeyStorybook : MPEngagementStorybookCustom,
                          MPEngagementKeyVideo: videos[index-1],
                          MPEngagementKeyCreatedBy : email,
                          }];
    }
}

+ (void) logCreateStorysuccess
{
    NSString *email = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserEmail"];
    
    int index = s_currentVideo;
    
    NSArray * videos = @[
                         MPEngagementVideo1,
                         MPEngagementVideo2,
                         MPEngagementVideo3,
                         MPEngagementVideo4,
                         MPEngagementVideo5,
                         ];
    
    
    if ([email length] <= 0)
    {
        [MIXPANEL track:MPEngagementEventCreateStorySuccess
             properties:@{
                          MPEngagementKeyStorybook : MPEngagementStorybookCustom,
                          MPEngagementKeyVideo: videos[index],
                          }];
    }
    else
    {
        [MIXPANEL track:MPEngagementEventCreateStorySuccess
             properties:@{
                          MPEngagementKeyStorybook : MPEngagementStorybookCustom,
                          MPEngagementKeyVideo: videos[index],
                          MPEngagementKeyCreatedBy : email,
                          }];
    }
    
    if(s_isFinal)
    {
        if ([email length] <= 0)
        {
            [MIXPANEL track:MPEngagementEventCreateStorybookSuccess
                 properties:@{
                              MPEngagementKeyStorybook : MPEngagementStorybookCustom,
                              }];
        }
        else
        {
            [MIXPANEL track:MPEngagementEventCreateStorybookSuccess
                 properties:@{
                              MPEngagementKeyStorybook : MPEngagementStorybookCustom,
                              MPEngagementKeyCreatedBy : email,
                              }];
        }
    }
}



// account creation tracking ***

+ (void)logOpenedSignUp
{
    s_startTime = [NSDate date];
    
    [MIXPANEL track:MPAccountCreationEventOpenSignUp];
}

+ (void)markStartSNSLogin
{
    s_startTime = [NSDate date];
}


+ (void)logDeleteAccount
{
    [MIXPANEL track:MPAccountCreationEventDeleted];
    
    [MIXPANEL.people set:@{
                           MPPersonKeyDeleted: [NSNumber numberWithBool:TRUE],
                           }];
}

+ (void)logSubmittedFeedback
{
    [MIXPANEL track:MPAccountCreationEventSubmittedFeedback];
}

+ (void)logInputOnRegistrationField
{
    [MIXPANEL track:MPAccountCreationEventInputOnRegistrationField];
}



@end
