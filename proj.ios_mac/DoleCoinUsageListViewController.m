//
//  DoleCoinUsageListViewController.m
//  bobby
//
//  Created by oasis on 2014. 7. 28..
//
//
#ifndef MACOS

#import "DoleCoinUsageListViewController.h"
#import <Accounts/Accounts.h>
#import "UIImage+PathExtention.h"
#import "DoleCoinUsageTableViewCell.h"
#import "ResolutionManager.h"
#import "TSDolesNet.h"
#import "TSToastView.h"
#import "TSSettingViewController.h"
#import "TSHelpFAQView.h"
#import "AppController.h"

#define IS_IPAD     (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//#define IS_4INCH    (self.view.frame.size.height == 568)

@interface DoleCoinUsageListViewController ()< TSNetDelegate >
@property (nonatomic,strong) NSArray *usageList;
@property (nonatomic,strong) NSArray *limitedPeriodItemNameList;
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) UIView *containerView;
@property (nonatomic,strong) UIButton *helpButton;
@property (nonatomic,strong) TSNetGetPurchaseList *net;
@property (nonatomic,strong) UIView *insideView;
@property (nonatomic,strong) UIButton *cancelButton;
@property (nonatomic,strong) UIButton *helpCancelButton;
@property (nonatomic,strong) UIView *currentDisplayView;
@property (nonatomic,strong) UIView *helpDisplayView;
@property (nonatomic) BOOL is4inch;
@end

@implementation DoleCoinUsageListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
//#ifndef XCODE_6
//        if (!SYSTEM_VERSION_LESS_THAN(@"8.0")) {
//            self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.height, self.view.frame.size.width);
//        }
//        self.is4inch = ([[UIScreen mainScreen] bounds].size.height == 568);
//#endif
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //        NSLog(@"self.view.frame : %@", NSStringFromCGRect(self.view.frame));
    NSInteger userNo = [[[NSUserDefaults standardUserDefaults] valueForKey:UD_INTEGER_USERNO] integerValue];
    NSLog(@"userNo : %i", userNo);
    if (userNo > 0) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd hh:mm"];
        [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
        NSString *todayGMTStr = [formatter stringFromDate:[NSDate date]];
        NSDate *todayGMT = [formatter dateFromString:todayGMTStr];
        NSLog(@"todayGMT : %@", todayGMT);
        
        //            NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        
        NSDateComponents *korDateComp = [[NSDateComponents alloc] init];
        korDateComp.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        [korDateComp setHour:9];
        NSDate *endDate = [[calendar dateByAddingComponents:korDateComp toDate:todayGMT options:0] dateByAddingTimeInterval:60*60*24*1];
        NSString *todayStr = [formatter stringFromDate:endDate];
        NSDate *today = [formatter dateFromString:todayStr];
        NSLog(@"today : %@", today);
        
        NSDateComponents *beginDateComponents = [[NSDateComponents alloc] init];
        beginDateComponents.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        [beginDateComponents setMonth:-6];
        NSDate *startdate = [[calendar dateByAddingComponents:beginDateComponents toDate:today options:0] dateByAddingTimeInterval:60*60*24*(-1)];
        NSString *beginStr = [formatter stringFromDate:startdate];
        NSDate *begin = [formatter dateFromString:beginStr];
        NSLog(@"begin : %@", begin);
        
        self.net = [[TSNetGetPurchaseList alloc] initWithUserNo:userNo begin:begin end:today];
        self.net.delegate = self;
        [self.net start];
    }
    
   
}

- (void)viewWillLayoutSubviews {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.view.superview.layer.masksToBounds = YES;
        
        if(!IS_IOS8) {
            self.view.superview.bounds = getRectValue(TSViewIndexShare, TSShareSuperViewControllerRect);
            self.view.frame = getRectValue(TSViewIndexShare, TSShareSuperViewControllerRect);
        }
        
        self.view.superview.backgroundColor = [UIColor clearColor];
        self.view.backgroundColor = [UIColor clearColor];
    }
    [super viewWillLayoutSubviews];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //서버에서 아이템이름을 넘김. limited period item을 구분하기 위한 array, limited item이 변하거나 localized string 적용시 코드 변환 필요.
    self.limitedPeriodItemNameList = @[@"Rilla", @"Lion", @"Crocodile", @"Cave", @"The dock"];
    
//    self.usageList = @{@"coin":@"120",@"name":@"test",@"sub":@"test",@"date":@"2014.05.05",
//                       @"coin":@"220",@"name":@"test1",@"sub":@"test1",@"date":@"2014.05.05",
//                       @"coin":@"320",@"name":@"test2",@"sub":@"test2",@"date":@"2014.05.05",
//                       @"coin":@"230",@"name":@"test3",@"sub":@"test3",@"date":@"2014.05.05"};
    
//    self.usageList = @[@"1",@"2", @"4"];
    // Do any additional setup after loading the view.
    
    CGRect rect = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)?CGRectMake(0, 0, 540+50, 620):self.view.frame;
#ifdef __IPHONE_8_0
    if (!SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        rect = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.height, self.view.frame.size.width);
    }
    self.is4inch = (SYSTEM_VERSION_LESS_THAN(@"8.0"))?(self.view.frame.size.height == 568):(self.view.frame.size.width == 568);
#else
    if (!SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.height, self.view.frame.size.width);
    }
    self.is4inch = ([[UIScreen mainScreen] bounds].size.height == 568);
#endif
    
    CGRect containerViewRect = CGRectMake(0, 0, IS_IPAD?414:rect.size.width, IS_IPAD?612:rect.size.height);
    self.containerView = [[UIView alloc] initWithFrame:containerViewRect];
    self.containerView.backgroundColor = [UIColor clearColor];
    
    self.currentDisplayView = [[UIView alloc] initWithFrame:containerViewRect];
    self.currentDisplayView.backgroundColor = [UIColor clearColor];
    if (IS_IPAD) {
//        self.containerView.frame = CGRectMake((rect.size.width-self.containerView.frame.size.width)/2-50/2,
//                                              (rect.size.height-self.containerView.frame.size.height)/2,
//                                              containerViewRect.size.width, containerViewRect.size.height);
        self.containerView.frame = CGRectMake(self.view.frame.size.height/2 - containerViewRect.size.width/2, self.view.frame.size.width/2-containerViewRect.size.height/2, containerViewRect.size.width, containerViewRect.size.height);
    } else {
        self.containerView.frame = rect;
    }
    
    UIImage *backgroundImage = [UIImage imageNamedEx:@"bg_map_full.png" ignore:YES];
    UIImage *backgroundFactorImage = [UIImage imageNamedEx:@"bg_map_factor_02.png" ignore:YES];
    
    CGRect cancelButtonRect;
    UIImage *cancelImage = [UIImage imageNamedEx:@"btn_cancel_normal.png" ignore:YES];
    UIImage *cancelPressImage = [UIImage imageNamedEx:@"btn_cancel_press.png" ignore:YES];
    if (IS_IPAD) {
        cancelButtonRect = CGRectMake(self.containerView.frame.size.width-56/2-cancelImage.size.width, 59/2, cancelImage.size.width, cancelImage.size.height);
    } else {
        cancelButtonRect = CGRectMake(self.containerView.frame.size.width-18/2-cancelImage.size.width, 18/2, cancelImage.size.width, cancelImage.size.height);
    }
    
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:backgroundImage];
    [self.containerView addSubview:backgroundView];
    
    UIImageView *backgroundFactorView = [[UIImageView alloc] initWithImage:backgroundFactorImage];
    [self.currentDisplayView addSubview:backgroundFactorView];
    
    self.cancelButton = [[UIButton alloc] initWithFrame:cancelButtonRect];
    [self.cancelButton setImage:cancelImage forState:UIControlStateNormal];
    [self.cancelButton setImage:cancelPressImage forState:UIControlStateHighlighted];
    [self.cancelButton addTarget:self action:@selector(cancelButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.currentDisplayView addSubview:self.cancelButton];
    
    // logo
    if (IS_IPAD || self.is4inch) {
        UIImage *logoImage = [UIImage imageNamedEx:@"dolecoin_logo.png" ignore:YES];
        CGRect logoRect;
        
        if (IS_IPAD) {
            logoRect = CGRectMake(54/2, 58/2, 182/2, 94/2);
        } else {
            logoRect = CGRectMake(20/2, 20/2, 174/2, 88/2);
        }
        
        UIImageView *logoImageView = [[UIImageView alloc] initWithImage:logoImage];
        logoImageView.frame = logoRect;
        [self.currentDisplayView addSubview:logoImageView];
    }
    
    float friendListHeight = IS_IPAD?603:self.is4inch?603:506;
    float friendListStartY = IS_IPAD?(251+136)/2-containerViewRect.origin.x:self.is4inch?(204+136)/2:129;
    float friendListStartX = IS_IPAD?160/2:66/2;
    
    // talk box
    float talkBoxStartY = IS_IPAD?183/2:self.is4inch?136/2:59/2;
    CGRect labelRect = CGRectMake(friendListStartX, talkBoxStartY+22/2, (394)/2, (90+12)/2);
    UILabel *descLabel = [[UILabel alloc] initWithFrame:labelRect];
    descLabel.textAlignment = NSTextAlignmentCenter;
    descLabel.font = [UIFont systemFontOfSize:13];
    descLabel.numberOfLines = 3;
    NSString *labelText = NSLocalizedString(@"PF_USAGECOIN_NOTICE", @"Track your Dole Coins here!\nClick \"Help\" for more\ninformation");
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelText length])];
    [attributedString addAttribute:NSForegroundColorAttributeName
                             value:[UIColor colorWithRed:79.0/255.0 green:44.0/255.0 blue:0 alpha:1.0]
                             range:NSMakeRange(0, [labelText length])];
    
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSRange orangeColor;
    if ([language isEqualToString:@"ja"]) {
        orangeColor = NSMakeRange(0,10);
    } else if ([language isEqualToString:@"ko"]) {
        orangeColor = NSMakeRange(0,8);
    } else {
        orangeColor = NSMakeRange(0, 0);
    }
    
    [attributedString addAttribute:NSForegroundColorAttributeName
                             value:[UIColor colorWithRed:255.0/255.0 green:84.0/255.0 blue:17.0/255.0 alpha:1.0]
                             range:orangeColor];
    descLabel.attributedText = attributedString;
    
    UIImage *talkBoxImage = [[UIImage imageNamedEx:@"invite_talk_box.png" ignore:YES] resizableImageWithCapInsets:UIEdgeInsetsMake(22/2, 24/2, 38/2, 24/2)];
    UIImageView *talkBoxImageView = [[UIImageView alloc] initWithImage:talkBoxImage];
    CGRect labelBgRect = CGRectMake(friendListStartX, talkBoxStartY, (366+14+14)/2, 162/2);
    talkBoxImageView.frame = labelBgRect;
    [self.currentDisplayView addSubview:talkBoxImageView];
    [self.currentDisplayView addSubview:descLabel];
    
    // tableview bg
    UIImage *inviteListBgImage = [[UIImage imageNamedEx:@"invite_popup.png" ignore:YES] resizableImageWithCapInsets:UIEdgeInsetsMake(94/2+1, 21/2, 24/2, 21/2)];
    UIImageView *inviteListBgImageView = [[UIImageView alloc] initWithImage:inviteListBgImage];
    CGRect inviteListBgRect = CGRectMake(friendListStartX, friendListStartY, 508/2, friendListHeight/2);
    inviteListBgImageView.frame = inviteListBgRect;
    [self.currentDisplayView addSubview:inviteListBgImageView];
    
    UIView *hearderView = [[UIView alloc] initWithFrame:CGRectMake(friendListStartX, friendListStartY, 508/2, 94/2)];
    hearderView.backgroundColor = [UIColor clearColor];
    
    UIImage *coinImage = [UIImage imageNamedEx:@"dole_coin_usage_icon.png" ignore:YES];
    UIImageView *coinImageView = [[UIImageView alloc] initWithImage:coinImage];
    CGRect coinRect = CGRectMake(0, 0, 106/2, 94/2);
    coinImageView.frame = coinRect;
    [hearderView addSubview:coinImageView];
    
    UIImageView *separator1 = [[UIImageView alloc] init];
    separator1.backgroundColor = [UIColor whiteColor];
    separator1.alpha = 0.24;
    separator1.frame = CGRectMake(106/2, 21/2, 1, 52/2);
    [separator1 setContentMode:UIViewContentModeScaleToFill];
    [hearderView addSubview:separator1];
    
    UILabel *detailLabel = [[UILabel alloc] initWithFrame:CGRectMake((106+1+16)/2, 30/2, (228)/2, 34/2)];
    detailLabel.font = [UIFont boldSystemFontOfSize:30/2];
    detailLabel.text = NSLocalizedString(@"PF_USAGECOIN_LISTFIELD_DETAILS", @"Details");
    detailLabel.textColor = [UIColor whiteColor];
    detailLabel.textAlignment = NSTextAlignmentCenter;
    [hearderView addSubview:detailLabel];
    
    UIImageView *separator2 = [[UIImageView alloc] init];
    separator2.backgroundColor = [UIColor whiteColor];
    separator2.alpha = 0.24;
    separator2.frame = CGRectMake((106+1+16+228+16)/2, 21/2, 1, 52/2);
    [separator2 setContentMode:UIViewContentModeScaleToFill];
    [hearderView addSubview:separator2];
    
    UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake((106+1+16+228+16+1+16)/2, 30/2, (108)/2, 34/2)];
    dateLabel.font = [UIFont boldSystemFontOfSize:30/2];
    dateLabel.text = NSLocalizedString(@"PF_USAGECOIN_LISTFIELD_DATE", @"Date");
    dateLabel.textColor = [UIColor whiteColor];
    dateLabel.textAlignment = NSTextAlignmentCenter;
    [hearderView addSubview:dateLabel];
    [self.currentDisplayView addSubview:hearderView];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(friendListStartX, friendListStartY+94/2, 508/2, (friendListHeight-94)/2 -2)]; // height -2 for shadow
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.allowsMultipleSelection = YES;
    
    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    [self.tableView setSeparatorColor:[UIColor colorWithRed:162.0/255.0 green:132.0/255.0 blue:89.0/255.0 alpha:1.0] ];
    
    [self.currentDisplayView addSubview:self.tableView];
    CGFloat tableViewHeight = CGRectGetMaxY(self.tableView.frame);
    
    self.insideView = [[UIView alloc] initWithFrame:CGRectMake(friendListStartX, friendListStartY+94/2, 508/2, (friendListHeight-94)/2 -2)];
    float usageNoneHeight = IS_IPAD?352/2:self.is4inch?352/2:274/2;
    UIImage *usageNoneImage = [UIImage imageNamedEx:@"dole_coin_usage_no_history_image.png" ignore:YES];
    UIImageView *usageNoneImageView = [[UIImageView alloc] initWithImage:usageNoneImage];
    CGRect usageNoneRect = CGRectMake(0, 0, 508/2, usageNoneHeight);
    usageNoneImageView.frame = usageNoneRect;
    [self.insideView addSubview:usageNoneImageView];
    
    UILabel *noHistoryLabel = [[UILabel alloc] initWithFrame:CGRectMake((24)/2,usageNoneImageView.frame.size.height+(17)/2,(508-24-24)/2,(30+30+30+2+2+2)/2)];
    noHistoryLabel.textAlignment = NSTextAlignmentCenter;
    noHistoryLabel.font = [UIFont systemFontOfSize:26/2];
    noHistoryLabel.numberOfLines = 3;
    NSString *noHistoryLabelText = NSLocalizedString(@"PF_USAGECOIN_NOTICE_NOHISTORY", @"There is no history.\nEnjoy our delightful service with\nDole coins!");
    noHistoryLabel.text = noHistoryLabelText;
    noHistoryLabel.textColor = [UIColor colorWithRed:0x4f/255.0f green:0x2c/255.0f blue:0x00/255.0f alpha:255.0f/255.0f];
    [self.insideView addSubview:noHistoryLabel];
    [self.currentDisplayView addSubview:self.insideView];
    self.insideView.hidden = YES;
    
    // fox
    float talkFoxStartY = IS_IPAD?251/2:self.is4inch?(225-21)/2:(145-21)/2;
    float talkFoxStartX = IS_IPAD?(536+20)/2:(408+20)/2;
    UIImage *talkFoxImage = [UIImage imageNamedEx:@"dolecoin_talk_fox.png" ignore:YES];
    UIImageView *talkFoxImageView = [[UIImageView alloc] initWithImage:talkFoxImage];
    CGRect talkFoxRect = CGRectMake(talkFoxStartX, talkFoxStartY, 156/2, 136/2);
    talkFoxImageView.frame = talkFoxRect;
    [self.currentDisplayView addSubview:talkFoxImageView];
    
    if (IS_IPAD) {
        // side characters
        UIImage *leftSideCharacter = [UIImage imageNamedEx:@"full_popup_character_01.png" ignore:YES];
        UIImageView *leftSideCharacterView = [[UIImageView alloc] initWithImage:leftSideCharacter];
        CGRect leftSideCharacterRect = CGRectMake(-193/2, (70+472+210)/2, 346/2, 472/2);
        leftSideCharacterView.frame = leftSideCharacterRect;
        
        UIImage *rightSideCharacter = [UIImage imageNamedEx:@"full_popup_character_02.png" ignore:YES];
        UIImageView *rightSideCharacterView = [[UIImageView alloc] initWithImage:rightSideCharacter];
        CGRect rightSideCharacterRect = CGRectMake(680/2, 70/2, 296/2, 472/2);
        rightSideCharacterView.frame = rightSideCharacterRect;
        
        [self.containerView addSubview:leftSideCharacterView];
        [self.containerView addSubview:rightSideCharacterView];
    }
    
    self.containerView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:self.containerView];
    self.view.backgroundColor = [UIColor clearColor];
    
    UIImage *inviteNormalImage = [UIImage imageNamedEx:@"button_normal_01.png" ignore:YES];
    UIImage *invitePressImage = [UIImage imageNamedEx:@"button_press_01.png" ignore:YES];
    UIImage *inviteDisableImage = [UIImage imageNamedEx:@"button_dim_01.png" ignore:YES];
    
    self.helpButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIEdgeInsets resizeCap = UIEdgeInsetsMake(inviteNormalImage.size.height/2, inviteNormalImage.size.width/2-1,
                                              inviteNormalImage.size.height/2, inviteNormalImage.size.width/2+1);
    
    [self.helpButton setTitle:NSLocalizedString(@"PF_USAGECOIN_BUTTON_TITLE_HELP", @"Help") forState:UIControlStateNormal];
    [self.helpButton.titleLabel setFont:[UIFont boldSystemFontOfSize:34/2]];
    [self.helpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.helpButton setBackgroundImage:[inviteNormalImage resizableImageWithCapInsets:resizeCap] forState:UIControlStateNormal];
    [self.helpButton setBackgroundImage:[invitePressImage resizableImageWithCapInsets:resizeCap] forState:UIControlStateHighlighted];
    [self.helpButton setBackgroundImage:[inviteDisableImage resizableImageWithCapInsets:resizeCap] forState:UIControlStateDisabled];
    
    float helpButtonFromTableView = IS_IPAD?50/2:self.is4inch?47/2:43/2; // TODO:check ipad value
    CGRect helpButtonRect = CGRectMake(friendListStartX, tableViewHeight+helpButtonFromTableView, 508/2, 74/2);
    self.helpButton.frame = helpButtonRect;
    
    [self.helpButton addTarget:self action:@selector(helpButtonPressed) forControlEvents:UIControlEventTouchUpInside];
//    [self.helpButton setEnabled:NO];
    [self.currentDisplayView addSubview:self.helpButton];
    [self.containerView addSubview:self.currentDisplayView];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(noticeWillShowKeyboard:) name:UIKeyboardWillShowNotification object:nil];
    [center addObserver:self selector:@selector(noticeWillHideKeyboard:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)noticeWillShowKeyboard:(NSNotification *)notification {
    NSInteger move = 155;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        move = 110;
        NSLog(@"[UIScreen mainScreen].bounds : %@", NSStringFromCGRect([UIScreen mainScreen].bounds));
        if ([UIScreen mainScreen].bounds.size.height != 480) {
            move = 130;
        }
    }
    
    if (move != 0) {
        CGRect frame = self.view.frame;
        frame.origin.y = frame.origin.y - move;
        [UIView animateWithDuration:0.3 animations:^{
            self.view.frame = frame;
        }];
    }
}

- (void)noticeWillHideKeyboard:(NSNotification *)notification {
    NSString *viewName = NSStringFromClass([self.currentDisplayView class]);
    
    NSInteger move = 155;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        move = 110;
        if ([UIScreen mainScreen].bounds.size.height != 480) {
            move = 130;
        }
    }
    
    if (move != 0) {
        CGRect frame = self.view.frame;
        frame.origin.y = frame.origin.y + move;
        [UIView animateWithDuration:0.3 animations:^{
            self.view.frame = frame;
        }];
    }
}

- (void)helpButtonPressed
{
    if (self.currentDisplayView.hidden == false) {
        self.currentDisplayView.hidden = true;
    }
    
    if (self.helpDisplayView) {
        [self.helpDisplayView removeFromSuperview];
        self.helpDisplayView = nil;
    }
    
    TSHelpFAQView *helpFAQView = [[TSHelpFAQView alloc] initWithFrame:getRectValue(TSViewIndexShare, TSShareSubViewRect)];
    helpFAQView.delegate = self;
    [self.view addSubview:helpFAQView];
    self.helpDisplayView = helpFAQView;
    
    self.helpCancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.helpCancelButton setImage:[UIImage imageNamedEx:@"btn_cancel_normal.png" ignore:YES] forState:UIControlStateNormal];
    [self.helpCancelButton setImage:[UIImage imageNamedEx:@"btn_cancel_press.png" ignore:YES] forState:UIControlStateHighlighted];
    [self.helpCancelButton addTarget:self action:@selector(helpCancelButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    self.helpCancelButton.frame = getRectValue(TSViewIndexShare, TSShareCloseButtonRect);
    [self.view addSubview:self.helpCancelButton];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (IS_IPAD) {
        self.view.superview.backgroundColor = [UIColor clearColor];
    }
}

- (void)closeViewControllerWithView:(UIView *)view
{
    [self helpCancelButtonPressed];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)helpCancelButtonPressed
{
    if (self.currentDisplayView.hidden == true) {
        self.currentDisplayView.hidden = false;
    }
    
    if (self.helpDisplayView) {
        [self.helpDisplayView removeFromSuperview];
        self.helpDisplayView = nil;
    }
    
    if (self.helpCancelButton) {
        [self.helpCancelButton removeFromSuperview];
        self.helpCancelButton = nil;
    }
}

- (void)cancelButtonPressed
{
    [self dismissViewControllerAnimated:NO completion:nil];
    
    UIViewController *viewController = nil;
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
    } else {
        viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
    }
    NSLog(@"viewController screen frame : %@",NSStringFromCGRect(viewController.view.frame));
    TSSettingViewController *settingViewController = [[TSSettingViewController alloc] init];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        settingViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    if(IS_IOS8 && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        settingViewController.preferredContentSize = CGSizeMake(1024, 768);
    }
    [viewController presentViewController:settingViewController animated:NO completion:nil];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    DoleCoinUsageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[DoleCoinUsageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.tag = indexPath.row;
    
    cell.coinTitle.text = [self.usageList[indexPath.row][@"PurchaseAmount"] stringValue];
    NSLog(@"self.usageList[indexPath.row] : %@", self.usageList[indexPath.row]);
    cell.mainTitle.text = self.usageList[indexPath.row][@"ProductName"];
//    cell.mainTitle.text = [self.usageList[indexPath.row][@"PurchaseStatus"] stringValue];
    
    cell.subTitle.text = NSLocalizedString(@"PF_USAGECOIN_UNLIMITED", @"Unlimited use");// @"Unlimited";
    for (NSString *periodItemName in self.limitedPeriodItemNameList) {
        if ([periodItemName isEqualToString:self.usageList[indexPath.row][@"ProductName"]]) {
            cell.subTitle.text = NSLocalizedString(@"PF_USAGECOIN_LIMITED_PERIOD", @"Limited use");//@"Limited Period";
        }
    }
    
    NSString *currentLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSLog(@"current language : %@", currentLanguage);
    
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
    [formatter1 setDateFormat:@"yyyy-MM-dd hh:mm"];
    [formatter1 setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    NSString *registerDateKorStr = self.usageList[indexPath.row][@"RegisterDateTime"];
    NSDate *registerKor = [formatter1 dateFromString:registerDateKorStr];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *gmtDateComp = [[NSDateComponents alloc] init];
    gmtDateComp.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [gmtDateComp setHour:-9];
    NSDate *dateGMT = [calendar dateByAddingComponents:gmtDateComp toDate:registerKor options:0];
    NSString *gmtStr = [formatter1 stringFromDate:dateGMT];
    NSDate *registerDate = [formatter1 dateFromString:gmtStr];
    
    NSLog(@"registerDate : %@", registerDate);
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"ko"]) {
        [formatter setDateFormat:@"yyyy.MM.dd"];
    } else {
        [formatter setDateFormat:@"dd.MM.yyyy"];
    }
    NSString *localDateStr = [formatter stringFromDate:registerDate];
    NSLog(@"localDateStr : %@", localDateStr);
    
    cell.dateTitle.text = localDateStr;
    
    return cell;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120/2+1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.usageList && [self.usageList count] != 0) {
        NSInteger count = [self.usageList count];
        return (count>30)?30:count;
    } else {
        return 0;
    }
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)showActivity:(BOOL)show {
    UIActivityIndicatorView *indicatorView = nil;
    if (show) {
        indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        CGRect frame = indicatorView.frame;
//        if (IS_IPAD) {
//            frame.origin = CGPointMake(self.view.frame.size.height/2 - indicatorView.frame.size.width/2, self.view.frame.size.width/2 - indicatorView.frame.size.height/2);
//        } else {
//            frame.origin = CGPointMake(self.view.frame.size.width/2 - indicatorView.frame.size.width/2, self.view.frame.size.height/2 - indicatorView.frame.size.height/2);
//        }
        frame.origin = CGPointMake(self.containerView.frame.size.width/2 - indicatorView.frame.size.width/2, self.containerView.frame.size.height/2 - indicatorView.frame.size.height/2);
        indicatorView.tag = 999777;
        indicatorView.frame = frame;
        [self.containerView addSubview:indicatorView];
        [indicatorView startAnimating];
    } else {
        indicatorView = (UIActivityIndicatorView*)[self.view viewWithTag:999777];
        if (indicatorView) {
            [indicatorView stopAnimating];
            [indicatorView removeFromSuperview];
        }
    }
}

#pragma mark - TSNetDelegate
- (void)net:(TSNet*)netObject didStartProcess:(TSNetResultType)result {
    [self showActivity:YES];
}

- (void)net:(TSNet*)netObject didEndProcess:(TSNetResultType)result {
    [self showActivity:NO];
    self.usageList = self.net.purchaseList;
    NSLog(@"usageList count : %i", self.usageList.count);
    [self.tableView reloadData];
    if (self.usageList.count <= 0) {
        self.insideView.hidden = NO;
        self.tableView.hidden = YES;
    } else {
        self.insideView.hidden = YES;
        self.tableView.hidden = NO;
    }
}

- (void)net:(TSNet*)netObject didFailWithError:(TSNetResultType)result {
    switch (result) {
        case TSNetErrorNetworkDisable:
        case TSNetErrorTimeout: {
            TSToastView *toast = [[TSToastView alloc] initWithString:NSLocalizedString(@"CM_NET_WARNING_NETWORK_UNSTABLE", @"The network is unstable.\nPlease try again")];
            [toast showInViewWithAnimation:self.view];
            break;
        }
        default: {
            break;
        }
    }
}

@end
#endif
