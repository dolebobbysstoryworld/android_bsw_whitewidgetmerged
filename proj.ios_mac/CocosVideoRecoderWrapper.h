//
//  CocosVideoRecoderWrapper.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 4. 17..
//
//

#ifndef _COCOS_VIDEO_RECODER_WRAPPER_H
#define _COCOS_VIDEO_RECODER_WRAPPER_H

//#include <stddef.h>
#include "Export.h"
//#include <typeinfo>
//#include <ctype.h>
//#include <string.h>
#include "cocos2d.h"

#if defined(__GNUC__) && ((__GNUC__ >= 4) || ((__GNUC__ == 3) && (__GNUC_MINOR__ >= 1)))
#define CC_DEPRECATED_ATTRIBUTE __attribute__((deprecated))
#elif _MSC_VER >= 1400 //vs 2005 or higher
#define CC_DEPRECATED_ATTRIBUTE __declspec(deprecated)
#else
#define CC_DEPRECATED_ATTRIBUTE
#endif

class EXPORT_DLL CocosVideoRecoderWrapper : public cocos2d::Ref
{
public:
    static CocosVideoRecoderWrapper* getInstance(float scaleFactor);
    static void release();
    float contentScaleFactor;
    
protected:
    CocosVideoRecoderWrapper();
    virtual ~CocosVideoRecoderWrapper();
    
    bool recoding;
    
public:
    void createFrameBuffer(float dt);
    void saveFrameImage();
    void startRecoding();
    void pauseRecoding();
    void stopRecoding();
    bool isRecoding() { return recoding; }
};

#endif