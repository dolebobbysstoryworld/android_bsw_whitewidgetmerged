//
//  AVCamPreviewView.h
//  CameraTest
//
//  Created by oasis on 2014. 4. 14..
//
//

#ifndef MACOS

#import <UIKit/UIKit.h>

@class AVCaptureSession;

@interface AVCamPreviewView : UIView
@property(nonatomic) AVCaptureSession *session;
@end

#endif
