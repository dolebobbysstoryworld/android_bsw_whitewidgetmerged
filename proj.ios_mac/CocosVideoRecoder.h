//
//  CocosVideoRecoder.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 4. 17..
//
//

#import <Foundation/Foundation.h>

@interface CocosVideoRecoder : NSObject

+ (CocosVideoRecoder *) sharedRecoder;
- (void)initialized:(CGFloat)scaleFactor;
- (void)createFrameBuffer;
- (void)saveFrameImage;
- (void)startRecoding;
- (void)pauseRecoding;
- (void)stopRecoding;

@end
