//
//  CustomFacebookFriendListCell.m
//  bobby
//
//  Created by Dongwook, Kim on 14. 6. 3..
//
//

#import "CustomFacebookFriendListCell.h"
#import "UIImage+PathExtention.h"

@implementation CustomFacebookFriendListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        UIImage *image = [UIImage imageNamedEx:@"invite_no_image.png" ignore:true];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        imageView.frame = CGRectMake(18/2, (120/2+1-94/2)/2, 94/2, 94/2);
        self.thumbNail = imageView;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake((18+94+21)/2, 41/2, 284/2, 38/2)];
        label.font = [UIFont systemFontOfSize:34/2];
        label.textColor = [UIColor colorWithRed:74.0/255.0 green:44.0/255.0 blue:0 alpha:1.0];
        self.nameLabel = label;
        
        UIImage *checkBoxImage = [UIImage imageNamedEx:@"checkbox_off" ignore:true];
        UIImageView *checkBoxImageView = [[UIImageView alloc] initWithImage:checkBoxImage];
        checkBoxImageView.frame = CGRectMake((18+94+21+284+16)/2, (120/2+1-56/2)/2, 54/2, 54/2);
        self.checkBox = checkBoxImageView;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        
        [self addSubview:self.thumbNail];
        [self addSubview:self.nameLabel];
        [self addSubview:self.checkBox];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    if (selected) {
        self.checkBox.image = [UIImage imageNamedEx:@"checkbox_on" ignore:true];
    } else {
        self.checkBox.image = [UIImage imageNamedEx:@"checkbox_off" ignore:true];
    }
}

@end
