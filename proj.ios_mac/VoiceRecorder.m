//
//  VoiceRecorder.m
//  bobby
//
//  Created by Dongwook, Kim on 14. 6. 26..
//
//

#import "VoiceRecorder.h"
#import <AudioToolbox/AudioServices.h>

@interface VoiceRecorder()

@property (strong, nonatomic) AVAudioRecorder *audioRecorder;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;

@end

@implementation VoiceRecorder

static VoiceRecorder *sharedRecorder = nil;

+ (VoiceRecorder *) sharedRecorder {
    @synchronized(self)     {
        if (!sharedRecorder) {
            sharedRecorder = [[VoiceRecorder alloc] init];
        }
    }
    return sharedRecorder;
}

- (void)createRecorder:(NSString *)fileName
{
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
    AudioSessionSetProperty (kAudioSessionProperty_OverrideAudioRoute,sizeof (audioRouteOverride),&audioRouteOverride);
    
    [audioSession setActive:YES error:nil];
    
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *soundFilePath = [documentsPath stringByAppendingPathComponent:fileName];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    NSDictionary *recordSettings = @{AVEncoderAudioQualityKey: [NSNumber numberWithInt:AVAudioQualityMin],
                                     AVFormatIDKey : [NSNumber numberWithInt:kAudioFormatMPEG4AAC],
//                                     AVEncoderBitRateKey : @16,
                                     AVNumberOfChannelsKey : @2,
                                     AVSampleRateKey : @44100.0
                                     };
    NSError *error = nil;
    
    _audioRecorder = [[AVAudioRecorder alloc]
                      initWithURL:soundFileURL
                      settings:recordSettings
                      error:&error];

    if (error) {
        NSLog(@"error: %@", [error localizedDescription]);
    } else {
        [_audioRecorder setDelegate:self];
        [_audioRecorder prepareToRecord];
    }
//    _audioRecorder.meteringEnabled = true;
}

- (void)createPlayer:(NSString *)fileName
{
    
    NSArray *dirPaths =  NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = dirPaths[0];
    
    NSString *soundFilePath = [docsDir stringByAppendingPathComponent:fileName];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    [_audioPlayer setDelegate:self];
}


- (void)startRecord:(NSString *)fileName
{
    if (_audioRecorder == nil) {
        [self createRecorder:fileName];
    }
    
    if (!_audioRecorder.recording) {
        NSLog(@"Trying to start Recording : %@", fileName);
        [_audioRecorder record];
        NSLog(@"Recording started");
    }
}

- (void)pauseRecord
{
    if (_audioRecorder.recording) {
        [_audioRecorder pause];
//        NSLog(@"pauseRecord");
    }
}

- (void)resumeRecord
{
    if (!_audioRecorder.recording) {
        [_audioRecorder record];
    }
}

- (void)stopRecord
{
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];
    [_audioRecorder stop];
//    NSLog(@"stopRecord");
}

- (void)deleteRecord
{
    if (_audioRecorder) {
        [_audioRecorder stop];
        BOOL result = [_audioRecorder deleteRecording];
        NSLog(@"delete success? : %d", result);
        _audioRecorder = nil;
        _audioPlayer = nil;
    }
}

- (void)play:(NSString *)fileName
{
    if (_audioPlayer == nil) {
        [self createPlayer:fileName];
    }
    
    if (!_audioPlayer.isPlaying) {
        NSLog(@"Trying to start playing : %@", fileName);
        [_audioPlayer setCurrentTime:0];
        [_audioPlayer play];
    }
}

- (void)pause
{
    if (_audioPlayer.isPlaying) {
        [_audioPlayer pause];
    }
}

- (void)resume
{
    if (!_audioPlayer.isPlaying) {
        [_audioPlayer play];
    }
}

- (void)stop
{
    if (_audioPlayer.isPlaying) {
        [_audioPlayer stop];
        [_audioPlayer prepareToPlay];
        NSLog(@"Stop Player");
    }
}

- (void)clear
{
    _audioRecorder = nil;
    _audioPlayer = nil;
}

- (void)removeFile:(NSString *)fileName
{
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *soundFilePath = [documentsPath stringByAppendingPathComponent:fileName];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:soundFilePath error:&error];
    if (!success) {
        NSLog(@"Could not delete file - %@ : %@ ",fileName, [error localizedDescription]);
    }
}

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    NSLog(@"audioPlayerDidFinishPlaying : %f", [_audioPlayer duration]);
}

-(void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
    NSLog(@"Decode Error occurred");
}

-(void)audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder error:(NSError *)error
{
    NSLog(@"Encode Error occurred");
}

- (void)audioRecorderBeginInterruption:(AVAudioRecorder *)recorder
{
    NSLog(@"audioRecorderBeginInterruption");
}

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag
{
    NSLog(@"audioRecorderDidFinishRecording");
}

@end
