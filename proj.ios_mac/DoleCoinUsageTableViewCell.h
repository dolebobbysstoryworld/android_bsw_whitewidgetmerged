//
//  DoleCoinUsageTableViewCell.h
//  bobby
//
//  Created by oasis on 2014. 7. 28..
//
//
#ifndef MACOS

#import <UIKit/UIKit.h>

@interface DoleCoinUsageTableViewCell : UITableViewCell
@property (nonatomic, strong) UILabel *coinTitle;
@property (nonatomic, strong) UILabel *mainTitle;
@property (nonatomic, strong) UILabel *subTitle;
@property (nonatomic, strong) UILabel *dateTitle;
@end
#endif