//
//  CharacterImageCell.m
//  bobby
//
//  Created by oasis on 2014. 7. 1..
//
//
#ifndef MACOS
#import "CharacterImageCell.h"
#import "UIImage+PathExtention.h"

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

@implementation CharacterImageCell
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        CGRect thumb;
        if (IS_IPAD) {
            thumb = CGRectMake((358/2)/2 - (250/2)/2, (408/2)/2 - (326/2)/2, 250/2, 326/2);
        } else {
            thumb = CGRectMake((172/2)/2- (122/2)/2, (188/2)/2 - (158/2)/2, 122/2, 158/2);
        }
        //        self.contentView.layer.borderColor = [UIColor greenColor].CGColor;
        //        self.contentView.layer.borderWidth = 1;
        
        self.thumbView = [[UIImageView alloc] initWithFrame:thumb];
        self.thumbView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.thumbView];
        
        UIImage *selectMarkImage = [self getSelectedImageForDevice];
        selectMark = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.size.width/2 - selectMarkImage.size.width/2, self.bounds.size.height/2 - selectMarkImage.size.height/2, selectMarkImage.size.width, selectMarkImage.size.height)];
        selectMark.image = nil;
        [self.contentView addSubview:selectMark];
        self.selected = NO;
        
    }
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.thumbView.image = nil;
    selectMark.image = nil;
}

- (void)setSelectedCell:(BOOL)selected
{
    if (selected) {
        selectMark.image = [self getSelectedImageForDevice];
        self.selected = YES;
    } else {
        selectMark.image = nil;
        self.selected = NO;
    }
}

-(UIImage*)getSelectedImageForDevice
{
    UIImage *selectMarkImage = [UIImage imageNamedEx:@"background_frame_check.png" ignore:YES];
//    if (IS_IPAD) {
//        selectMarkImage = [UIImage imageNamed:@"iosnative/background_frame_check_ipad.png"];
//    } else {
//        selectMarkImage = [UIImage imageNamed:@"iosnative/background_frame_check.png"];
//    }
    return selectMarkImage;
}

@end
#endif
