//
//  ImagePickerLandscapeController.m
//  bobby
//
//  Created by oasis on 2014. 4. 15..
//
//
#ifndef MACOS

#import "ImagePickerLandscapeController.h"

@interface ImagePickerLandscapeController ()

@end

@implementation ImagePickerLandscapeController

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

@end

#endif
