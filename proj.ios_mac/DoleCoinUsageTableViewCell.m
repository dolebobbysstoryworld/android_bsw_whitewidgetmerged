//
//  DoleCoinUsageTableViewCell.m
//  bobby
//
//  Created by oasis on 2014. 7. 28..
//
//
#ifndef MACOS
#import "DoleCoinUsageTableViewCell.h"

@implementation DoleCoinUsageTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        UILabel *coin = [[UILabel alloc] initWithFrame:CGRectMake((14)/2, 41/2, 78/2, (120-41-41)/2)];//CGRectMake((14)/2, 41/2, 78/2, (120-41-41)/2)
        coin.textAlignment = NSTextAlignmentCenter;
        coin.font = [UIFont fontWithName:@"Helvetica-bold" size:34/2];
        coin.textColor = [UIColor colorWithRed:255.0/255.0 green:77.0/255.0 blue:7.0/255.0 alpha:1.0];
//        coin.backgroundColor = [UIColor yellowColor];
        self.coinTitle = coin;
        [self addSubview:self.coinTitle];
        
        UILabel *mainLabel = [[UILabel alloc] initWithFrame:CGRectMake((14+78+14+14)/2, 28/2, 234/2, (34)/2)];
        mainLabel.font = [UIFont systemFontOfSize:30/2];
        mainLabel.textColor = [UIColor colorWithRed:79.0/255.0 green:44.0/255.0 blue:0/255.0 alpha:1.0];
//        mainLabel.backgroundColor = [UIColor yellowColor];
        self.mainTitle = mainLabel;
        [self addSubview:self.mainTitle];
        
        UILabel *subLabel = [[UILabel alloc] initWithFrame:CGRectMake((14+78+14+14)/2, (28+34+8)/2, 234/2, (26)/2)];
        subLabel.font = [UIFont systemFontOfSize:22/2];
        subLabel.textColor = [UIColor colorWithRed:135.0/255.0 green:83.0/255.0 blue:17.0/255.0 alpha:1.0];
//        subLabel.backgroundColor = [UIColor yellowColor];
        self.subTitle = subLabel;
        [self addSubview:self.subTitle];
        
        UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake((14+78+14+14+234+14+14)/2, (47)/2, 112/2, (26)/2)];
        dateLabel.font = [UIFont systemFontOfSize:20/2]; // gui : 22/2
        dateLabel.textColor = [UIColor colorWithRed:79.0/255.0 green:44.0/255.0 blue:0/255.0 alpha:1.0];
//        dateLabel.backgroundColor = [UIColor yellowColor];
        self.dateTitle = dateLabel;
        [self addSubview:self.dateTitle];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
#endif
