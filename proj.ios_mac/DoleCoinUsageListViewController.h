//
//  DoleCoinUsageListViewController.h
//  bobby
//
//  Created by oasis on 2014. 7. 28..
//
//
#ifndef MACOS
#import <UIKit/UIKit.h>
#import "TSAccountDelegateProtocol.h"

@interface DoleCoinUsageListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, TSAccountDelegate>

@end
#endif
