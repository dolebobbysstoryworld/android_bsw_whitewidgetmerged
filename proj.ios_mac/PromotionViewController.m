//
//  PromotionViewController.m
//  bobby
//
//  Created by oasis on 2014. 8. 18..
//
//
#ifndef MACOS

#import "PromotionViewController.h"
#import "TSCheckBox.h"
#import "UIImage+PathExtention.h"

#include "Define.h"

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface PromotionViewController () <TSCheckBoxDelegate, UIWebViewDelegate>
@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, strong) TSCheckBox *checkBox;
@property (nonatomic, strong) NSURL *promotionURL;
@end

@implementation PromotionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithURL:(NSURL*)url
{
    self = [super init];
    if (self) {
        self.promotionURL = url;
#ifdef __IPHONE_8_0
        if (!SYSTEM_VERSION_LESS_THAN(@"8.0")) {
            self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.height, self.view.frame.size.width);
        }
#endif
        NSLog(@"PromotionViewController frame : %@", NSStringFromCGRect(self.view.frame));
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

#ifndef __IPHONE_8_0
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.height, self.view.frame.size.width);
#else
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.height, self.view.frame.size.width);
    }
#endif
    
    CGRect bottomRect;
    CGRect checkBoxRect;
    CGRect doNotShowRect;
    CGRect closeButtonRect;
    
    if (IS_IPAD) {
        bottomRect = CGRectMake(0, self.view.frame.size.height - 180/2, self.view.frame.size.width, 180/2);
        checkBoxRect = CGRectMake(44/2, (180-(86+32))/2, 86/2, 86/2);
        doNotShowRect = CGRectMake((44+86+32)/2, (180-(50+49))/2, 496/2, 49/2);
        closeButtonRect = CGRectMake(self.view.frame.size.width-44/2-(313)/2, (180-(99+25))/2, 313/2, 99/2);
    } else {
        bottomRect = CGRectMake(0, self.view.frame.size.height - 110/2, self.view.frame.size.width, 110/2);

        
        NSLog(@"bottomRect : %@", NSStringFromCGRect(bottomRect));
        checkBoxRect = CGRectMake(28/2, (110-(51+19))/2, 51/2, 51/2);
        doNotShowRect = CGRectMake((28+51+18)/2, (110-(30+30))/2, 286/2, 30/2);
        closeButtonRect = CGRectMake(self.view.frame.size.width-28/2-(186)/2, (110-(59+14))/2, 186/2, 59/2);

        
    }
    
    float bottomAlphaSpace = (IS_IPAD)?30/2:22/2;
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-bottomRect.size.height+bottomAlphaSpace)];
    NSLog(@"WebView frame : %@", NSStringFromCGRect(self.webView.frame));
    self.webView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    self.webView.delegate = self;
    NSURLRequest *requestURL = [NSURLRequest requestWithURL:self.promotionURL];
    [self.webView loadRequest:requestURL];
    [self.view addSubview:self.webView];
    
    UIView *bottom = [[UIView alloc] initWithFrame:bottomRect];
    bottom.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamedEx:@"banner_bottom.png" ignore:YES]];
    [self.view addSubview:bottom];
    
    self.checkBox = [[TSCheckBox alloc] initWithFrame:checkBoxRect];
    self.checkBox.delegate = self;
    [self.checkBox setImage:[UIImage imageNamedEx:@"banner_check_box_normal.png" ignore:YES] forState:UIControlStateNormal];
    [self.checkBox setImage:[UIImage imageNamedEx:@"banner_check_box_press.png" ignore:YES] forState:UIControlStateSelected];
    [bottom addSubview:self.checkBox];
    
    UILabel *doNotShowLabel = [[UILabel alloc] initWithFrame:doNotShowRect];
    doNotShowLabel.font = [UIFont fontWithName:@"helvetica" size:(IS_IPAD)?45/2:25/2];
    doNotShowLabel.textColor = [UIColor colorWithRed:96.0f/255.0f green:96.0f/255.0f blue:96.0f/255.0f alpha:255.0f/255.0f];
    doNotShowLabel.adjustsFontSizeToFitWidth = YES;
    doNotShowLabel.text = NSLocalizedString(@"PF_PROMOTION_CHECKBOX", @"Don't show during 1days");
    [bottom addSubview:doNotShowLabel];
    
    UIButton *closeButton = [[UIButton alloc] initWithFrame:closeButtonRect];
    [closeButton setBackgroundImage:[UIImage imageNamedEx:@"banner_close_btn.png" ignore:YES] forState:UIControlStateNormal];
    [closeButton setTitle:NSLocalizedString(@"CM_POPUP_BUTTON_TITLE_CLOSE", "Close") forState:UIControlStateNormal];
    closeButton.titleLabel.textColor = [UIColor whiteColor];
    closeButton.titleLabel.font = [UIFont fontWithName:@"helvetica-bold" size:(IS_IPAD)?50/2:30/2];
    [closeButton addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
    [bottom addSubview:closeButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}

- (void)closeAction:(id)sender
{
    if (self.checkBox.selected) {
        NSDate *date = [NSDate date];
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [[NSDateComponents alloc] init];
        components.day = 1;
//        components.timeZone = [NSTimeZone localTimeZone];
        NSDate *endDate = [calendar dateByAddingComponents:components toDate:date options:0];
        
//        [calendar rangeOfUnit:NSDayCalendarUnit startDate:&endDate interval:NULL forDate:endDate];
//        
//        NSInteger difference = [[NSTimeZone localTimeZone] secondsFromGMTForDate:endDate];
//        NSDate *localEndDate = [NSDate dateWithTimeInterval:difference sinceDate:endDate];
//        NSLog(@"local endDate : %@", localEndDate);
        
        NSLog(@"endDate : %@", endDate);
        [[NSUserDefaults standardUserDefaults] setValue:endDate forKey:KEY_PROMOTION_CAN_SHOW_DATE];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (void)checkBox:(TSCheckBox *)checkBox didChangeSelectedStatus:(BOOL)selected {
    
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

@end
#endif
