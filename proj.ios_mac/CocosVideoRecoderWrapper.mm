//
//  CocosVideoRecoderWrapper.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 4. 17..
//
//

#include "CocosVideoRecoderWrapper.h"
#include "CocosVideoRecoder.h"
#include "cocos2d.h"

USING_NS_CC;
using namespace std;

#ifndef MACOS
#include "CCEAGLView.h"

static void static_initialized(float scalefactor) {
    [[CocosVideoRecoder sharedRecoder] initialized:scalefactor];
}

static void static_createFrameBuffer() {
    [[CocosVideoRecoder sharedRecoder] createFrameBuffer];
}

static void static_saveFrameImage() {
    [[CocosVideoRecoder sharedRecoder] saveFrameImage];
}

static void static_startRecoding() {
    [[CocosVideoRecoder sharedRecoder] startRecoding];
}

static void static_pauseRecoding() {
    [[CocosVideoRecoder sharedRecoder] pauseRecoding];
}

static void static_stopRecoding() {
    [[CocosVideoRecoder sharedRecoder] stopRecoding];
}

#else

static void static_initialized(float scalefactor) {
    // log("CocosVideoRecoder not supported");
}

static void static_createFrameBuffer() {
    // log("CocosVideoRecoder not supported");
}

static void static_saveFrameImage() {
    // log("CocosVideoRecoder not supported");
}

static void static_startRecoding() {
    // log("CocosVideoRecoder not supported");
}

static void static_pauseRecoding() {
    log("CocosVideoRecoder not supported");
}

static void static_stopRecoding() {
    log("CocosVideoRecoder not supported");
}

#endif

static CocosVideoRecoderWrapper *s_pRecoderWrapper;

CocosVideoRecoderWrapper::CocosVideoRecoderWrapper() {
    this->recoding = false;
}

CocosVideoRecoderWrapper::~CocosVideoRecoderWrapper() {
    
}

CocosVideoRecoderWrapper* CocosVideoRecoderWrapper::getInstance(float scaleFactor) {
    if (! s_pRecoderWrapper) {
        s_pRecoderWrapper = new CocosVideoRecoderWrapper();
        s_pRecoderWrapper->contentScaleFactor = scaleFactor;
        static_initialized(scaleFactor);
    }
    
    return s_pRecoderWrapper;
}

void CocosVideoRecoderWrapper::release() {
    
}

static int imageIndex = 0;
void CocosVideoRecoderWrapper::createFrameBuffer(float dt) {
    static_createFrameBuffer();
}

void CocosVideoRecoderWrapper::saveFrameImage() {
    static_saveFrameImage();
}

void CocosVideoRecoderWrapper::startRecoding() {
    auto scheduler = Director::getInstance()->getScheduler();
    scheduler->schedule(schedule_selector(CocosVideoRecoderWrapper::createFrameBuffer), this, 0.0f, false);
    this->recoding = true;
    static_startRecoding();
}

void CocosVideoRecoderWrapper::pauseRecoding() {
    static_pauseRecoding();
}

void CocosVideoRecoderWrapper::stopRecoding() {
    this->recoding = false;
    auto scheduler = Director::getInstance()->getScheduler();
    scheduler->unschedule(schedule_selector(CocosVideoRecoderWrapper::createFrameBuffer), this );
    static_stopRecoding();
}

/* void CocosVideoRecoderWrapper::createFrameBuffer(float dt) {
    Director::getInstance()->setNextDeltaTimeZero(true);
    cocos2d::Size screen = Director::getInstance()->getWinSize();
    
    RenderTexture *tex = RenderTexture::create(int(screen.width), int(screen.height));
    
    cocos2d::Point anchor = tex->getAnchorPoint();
    auto& childrens = Director::getInstance()->getRunningScene()->getChildren();
    Node *node = childrens.at(0);
    tex->begin();
    node->visit();
    tex->end();
    cocos2d::Image *pImage = tex->newImage();
    auto fullPath = FileUtils::getInstance()->getWritablePath() + __String::createWithFormat("screenshot_%03d.png", imageIndex)->getCString();
    imageIndex++;
    pImage->saveToFile(fullPath);
} */