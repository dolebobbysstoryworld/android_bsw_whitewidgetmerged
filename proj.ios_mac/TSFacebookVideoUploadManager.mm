//
//  TSFacebookVideoUploadManager.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 8. 6..
//
//

#import "TSFacebookVideoUploadManager.h"
#import <FacebookSDK/FacebookSDK.h>
#import <Social/SLRequest.h>
#import "Define.h"
#include "BookMapScene.h"
#include "TSDoleNetProgressLayer.h"

#define FB_APP_ID  @"459077664237188"

@interface TSFacebookVideoUploadManager () < FBRequestConnectionDelegate > {
    TSDoleNetProgressLayer *progressLayer;
    NSInteger totalDatalength;
}

@end

TSFacebookVideoUploadManager *__sharedFacebookUploadManager;

@implementation TSFacebookVideoUploadManager

+ (TSFacebookVideoUploadManager *)sharedObject {
	if (nil == __sharedFacebookUploadManager) {
        __sharedFacebookUploadManager = [[TSFacebookVideoUploadManager alloc] init];
	}
	return __sharedFacebookUploadManager;
}

+ (void)releaseSharedObject {
	__sharedFacebookUploadManager = nil;
}

- (void)prepareShareOnFaceBook:(void *)map {
    BookMap* bookmap = (BookMap*)map;

    if (FBSession.activeSession.isOpen) {
        BOOL canPublish = NO;
        NSString *permission = nil;
        for( permission in FBSession.activeSession.permissions) {
            if ([permission isEqualToString:@"publish_actions"]) {
                canPublish = YES;
                break;
            }
        }
        if (canPublish) {
            Share::KeyJSONValueMap resultMap;
            bookmap->onSuccessCallback(NULL, 0, resultMap);
        } else {
            Share::KeyJSONValueMap resultMap;
            bookmap->onFailedCallback(NULL, -1, resultMap);
        }
    } else {
        NSArray *permissions = [[NSArray alloc] initWithObjects:
                                @"email", @"public_profile", @"publish_actions", //@"user_hometown", // @"read_friendlists", @"uid", @"gender",
                                nil];
        [FBSession openActiveSessionWithPublishPermissions:permissions defaultAudience:FBSessionDefaultAudienceEveryone  allowLoginUI:YES
                                         completionHandler:^(FBSession *session,
                                                             FBSessionState status,
                                                             NSError *error) {
                                             NSLog(@"permissions : %@", session.permissions);
                                             if (error) {
                                                 NSLog(@"Login fail :%@",error);
                                                 Share::KeyJSONValueMap resultMap;
                                                 bookmap->onFailedCallback(NULL, -1, resultMap);
                                             } else if (FB_ISSESSIONOPENWITHSTATE(status)) {
                                                 NSString *permission = nil;
                                                 BOOL canPublish = NO;
                                                 for( permission in session.permissions) {
                                                     if ([permission isEqualToString:@"publish_actions"]) {
                                                         canPublish = YES;
                                                         break;
                                                     }
                                                 }
                                                 if (canPublish) {
                                                     Share::KeyJSONValueMap resultMap;
                                                     bookmap->onSuccessCallback(NULL, 0, resultMap);
//                                                     [self uploadFacebookVideo];
                                                 } else {
                                                     Share::KeyJSONValueMap resultMap;
                                                     bookmap->onFailedCallback(NULL, -1, resultMap);
//                                                     [self throwAlertWithTitle:@"Denied" message:@"Permission Error\npublish_actions"];
                                                 }
                                             }
                                         }];
    }
    //////Set Progress Callback sample source....
    ///http://stackoverflow.com/questions/7938430/how-can-i-check-the-progress-of-my-facebook-ios-upload
}

- (NSMutableDictionary *)videoUploadParameter:(NSString *)path title:(NSString*)string comment:(NSString*)message {
#if TARGET_IPHONE_SIMULATOR
    time_t timestamp = time(NULL);
    int value = timestamp%3 + 1;
    NSString *movfile = [NSString stringWithFormat:@"%d", value];
    NSString *pathString = [[NSBundle mainBundle] pathForResource:movfile ofType:@"MOV"];
    NSData *videoData = [NSData dataWithContentsOfFile:pathString];
    NSLog(@"Path for simulator Video : %@", pathString);
#else
    NSData *videoData = [NSData dataWithContentsOfFile:path];
    NSLog(@"Path for device Video : %@", path);
#endif
    totalDatalength = videoData.length;
    NSLog(@"video data length : %ld", totalDatalength);
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   videoData, @"StoryVideo.MOV",
//                                   @"video/mp4", @"contentType",
//                                   string, @"title",
                                   message, @"description",
                                   nil];
    return params;
}

- (NSData*)videoDataWith:(NSString*)path {
#if TARGET_IPHONE_SIMULATOR
    time_t timestamp = time(NULL);
    int value = timestamp%3 + 1;
    NSString *movfile = [NSString stringWithFormat:@"%d", value];
    NSString *pathString = [[NSBundle mainBundle] pathForResource:movfile ofType:@"MOV"];
    NSData *videoData = [NSData dataWithContentsOfFile:pathString];
    NSLog(@"Path for simulator Video : %@", pathString);
#else
    NSData *videoData = [NSData dataWithContentsOfFile:path];
    NSLog(@"Path for device Video : %@", path);
#endif
    totalDatalength = videoData.length;
    NSLog(@"video data length : %ld", totalDatalength);
    return videoData;
}

- (NSInteger)videolength {
    return totalDatalength;
}

- (void)uploadFacebookVideo:(NSString *)path title:(NSString*)string comment:(NSString*)message targetUI:(void *)progress {
    TSDoleNetProgressLayer *layer = (TSDoleNetProgressLayer*)progress;
    self->progressLayer = layer;
    NSData *videoData = [self videoDataWith:path];
    [FBSession.activeSession refreshPermissionsWithCompletionHandler:
     ^(FBSession *session,NSError *error) {
         NSDictionary *parameters = @{ @"StoryVideo.MOV": videoData, @"description": message };
         FBRequestConnection *fbRequrstConnection =
         [FBRequestConnection startWithGraphPath:@"me/videos"
                                      parameters:parameters
                                      HTTPMethod:@"POST"
                               completionHandler:
          ^(FBRequestConnection *connection, id result, NSError *error) {
              if(!error) {
                  Share::KeyJSONValueMap resultMap;
                  layer->onSuccessCallback(NULL, 0, resultMap);
                  NSLog(@"RESULT: %@", result);
              } else {
                  NSLog(@"error: %@", error);
                  Share::KeyJSONValueMap resultMap;
                  layer->onFailedCallback(NULL, -1, resultMap);
              }
              
          }];
         fbRequrstConnection.delegate = self;
     }];
}

- (void)requestConnectionDidFinishLoading:(FBRequestConnection *)connection
                                fromCache:(BOOL)isCached {
    NSLog(@"requestConnectionDidFinishLoading");
}

- (void)requestConnection:(FBRequestConnection *)connection
         didFailWithError:(NSError *)error {
    NSLog(@"requestConnectionWillBeginLoading / didFailWithError");
}

- (void)requestConnection:(FBRequestConnection *)connection
          didSendBodyData:(NSInteger)bytesWritten
        totalBytesWritten:(NSInteger)totalBytesWritten
totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite {
    NSLog(@"requestConnectionWillBeginLoading bytesWritten : %ld / totalBytesWritten : %ld / totalBytesExpectedToWrite : %ld", (long)bytesWritten, (long)totalBytesWritten, (long)totalBytesExpectedToWrite);
    if (self->progressLayer) {
        self->progressLayer->showProgress(totalBytesWritten);
    }
}

//
//- (void)throwAlertWithTitle:(NSString *)title message:(NSString *)msg {
//    NSLog(@"title : %@ message : %@", title, msg);
//    cocos2d::Scene *bookmapScene = cocos2d::Director::getInstance()->getRunningScene();
//    if (!bookmapScene) {
//        NSAssert(NO, @"runningScene is empty");
//        return;
//    }
//    BookMap *bookMapLayer = (BookMap *)bookmapScene->getChildByTag(TAG_BOOKMAP);//(Home*)self.homeLayer;
//    if (!bookMapLayer) {
//        NSAssert(NO, @"runningScene is not bookmapScene");
//        return;
//    }
//    
//    TSDoleNetProgressLayer *progressLayer = (TSDoleNetProgressLayer *)bookMapLayer->getChildByTag(TAG_NET_PROGRESS);
//    NSString *message = [title stringByAppendingFormat:@"\n%@", msg];
//    if (progressLayer) {
//        progressLayer->showMessagePopup([message UTF8String]);
//    } else {
//        PopupLayer *popup = PopupLayer::create(POPUP_TYPE_WARNING, [message UTF8String], "OK");
//        bookMapLayer->addChild(popup, 1000);
//    }
//    //    [[[UIAlertView alloc]initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
//}

- (void)createNewSession {
    FBSession* session = [[FBSession alloc] init];
    [FBSession setActiveSession: session];
}


- (void)shareOnFacebook:(NSString *)path title:(NSString*)string comment:(NSString*)message targetUI:(void *)progress {
    TSDoleNetProgressLayer *layer = (TSDoleNetProgressLayer*)progress;
//#if TARGET_IPHONE_SIMULATOR
//    NSString *filePathOfVideo = [[NSBundle mainBundle] pathForResource:@"IMG_1171" ofType:@"mp4"];
//    NSData *videoData = [NSData dataWithContentsOfFile:pathString];
//    NSLog(@"Path for simulator Video : %@", pathString);
//#else
    NSData *videoData = [NSData dataWithContentsOfFile:path];
    NSLog(@"Path for device Video : %@", path);
//#endif
    NSLog(@"videoData description : %@", [videoData description]);

//    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                   videoData, @"video.mov",
//                                   @"video/quicktime", @"contentType",
//                                   @"Video name ", @"name",
//                                   @"description of Video", @"description",
//                                   nil];

    time_t timestamp = time(NULL);
    NSString* videoName = [NSString stringWithFormat:@"%ld.mp4", timestamp];
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    [params setObject:[string stringByAppendingString:[NSString stringWithFormat:@"%ld", timestamp]] forKey:@"title"];
    [params setObject:message forKey:@"description"];
    [params setObject:videoData forKey:videoName];
    
    if (FBSession.activeSession.isOpen)
    {
        [FBRequestConnection startWithGraphPath:@"me/videos"
                                     parameters:params
                                     HTTPMethod:@"POST"
                              completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                  if(!error)
                                  {
                                      NSLog(@"RESULT: %@", result);
                                      Share::KeyJSONValueMap resultMap;
                                      layer->onSuccessCallback(NULL, 0, resultMap);
                                  }
                                  else
                                  {
                                      NSLog(@"ERROR: %@", error);
                                      Share::KeyJSONValueMap resultMap;
                                      layer->onFailedCallback(NULL, -1, resultMap);
                                  }
                              }];
    }
    else
    {
        NSArray *permissions = [[NSArray alloc] initWithObjects:
                                @"publish_actions",
                                nil];
        // OPEN Session!
        [FBSession openActiveSessionWithPublishPermissions:permissions defaultAudience:FBSessionDefaultAudienceEveryone  allowLoginUI:YES
                                         completionHandler:^(FBSession *session,
                                                             FBSessionState status,
                                                             NSError *error) {
                                             if (error)
                                             {
                                                 NSLog(@"Login fail :%@",error);
                                                 Share::KeyJSONValueMap resultMap;
                                                 layer->onFailedCallback(NULL, -1, resultMap);
                                             }
                                             else if (FB_ISSESSIONOPENWITHSTATE(status))
                                             {
                                                 [FBSession setActiveSession:session];
                                                 [FBRequestConnection startWithGraphPath:@"me/videos"
                                                                              parameters:params
                                                                              HTTPMethod:@"POST"
                                                                       completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                                                           if(!error)
                                                                           {
                                                                               NSLog(@"RESULT: %@", result);
                                                                               Share::KeyJSONValueMap resultMap;
                                                                               layer->onSuccessCallback(NULL, 0, resultMap);
                                                                           }
                                                                           else
                                                                           {
                                                                               NSLog(@"ERROR: %@", error);
                                                                               Share::KeyJSONValueMap resultMap;
                                                                               layer->onFailedCallback(NULL, -1, resultMap);
                                                                           }
                                                                           
                                                                       }];
                                             }
                                         }];
    }
}













//- (void)loginForFacebook {
//    if ([[FBSession activeSession] isOpen]) {
//        [self loginCallback:YES];
//    } else {
//        NSArray *permissions = [[NSArray alloc] initWithObjects:
//                                @"email", @"public_profile", @"user_hometown", // @"read_friendlists", @"uid", @"gender",
//                                nil];
//        
//        // Attempt to open the session. If the session is not open, show the user the Facebook login UX
//        [FBSession openActiveSessionWithReadPermissions:permissions allowLoginUI:true completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
//            // Did something go wrong during login? I.e. did the user cancel?
//            //            NSLog(@"error : %@", [error localizedDescription]);
//            if (status == FBSessionStateClosedLoginFailed || status == FBSessionStateClosed || status == FBSessionStateCreatedOpening) {
//                
//                // If so, just send them round the loop again
//                [[FBSession activeSession] closeAndClearTokenInformation];
//                [FBSession setActiveSession:nil];
//                [self createNewSession];
//                [self loginCallback:NO];
//            }
//            else {
//                [self loginCallback:YES];
//            }
//        }];
//    }
//    
//}
//
//- (void)loginCallback:(BOOL)success {
//    if (success) {
//        [self shareOnFB];
//    } else {
//        NSLog(@"facebook login Fail!!!");
//    }
//}
//
//-(void)shareOnFB {
//    __block ACAccount * facebookAccount;
//    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
//    NSDictionary *emailReadPermisson = [[NSDictionary alloc] initWithObjectsAndKeys:
//                                        FB_APP_ID,ACFacebookAppIdKey,
//                                        @[@"email"],ACFacebookPermissionsKey,
//                                        ACFacebookAudienceFriends,ACFacebookAudienceKey,
//                                        nil];
//    
//    NSDictionary *publishWritePermisson = [[NSDictionary alloc] initWithObjectsAndKeys:
//                                           FB_APP_ID,ACFacebookAppIdKey,
//                                           @[@"publish_stream"],ACFacebookPermissionsKey,
//                                           ACFacebookAudienceFriends,ACFacebookAudienceKey,
//                                           nil];
//    
//    ACAccountType *facebookAccountType = [accountStore
//                                          accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
//    //Request for Read permission
//    
//    [accountStore requestAccessToAccountsWithType:facebookAccountType options:emailReadPermisson completion:^(BOOL granted, NSError *error) {
//        
//        if (granted)
//        {
//            //Request for write permission
//            [accountStore requestAccessToAccountsWithType:facebookAccountType options:publishWritePermisson completion:^(BOOL granted, NSError *error) {
//                
//                if (granted)
//                {
//                    NSArray *accounts = [accountStore
//                                         accountsWithAccountType:facebookAccountType];
//                    facebookAccount = [accounts lastObject];
//                    NSLog(@"access to facebook account ok %@", facebookAccount.username);
//                    [self uploadWithFBAccount:facebookAccount];
//                }
//                else
//                {
//                    NSLog(@"access to facebook is not granted");
//                    // extra handling here if necesary
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        // Fail gracefully...
//                        NSLog(@"%@",error.description);
//                        [self errorMethodFromFB:error];
//                    });
//                }
//            }];
//        }
//        else
//        {
//            [self errorMethodFromFB:error];
//        }
//    }];
//}
//
//-(void)errorMethodFromFB:(NSError *)error
//{
//    
//    NSLog(@"access to facebook is not granted");
//    // extra handling here if necesary
//    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        
//        // Fail gracefully...
//        NSLog(@"%@",error.description);
//        
//        if([error code]== ACErrorAccountNotFound)
//            [self throwAlertWithTitle:@"Error" message:@"Account not found. Please setup your account in settings app."];
//        if ([error code] == ACErrorAccessInfoInvalid)
//            [self throwAlertWithTitle:@"Error" message:@"The client's access info dictionary has incorrect or missing values."];
//        if ([error code] ==  ACErrorPermissionDenied)
//            [self throwAlertWithTitle:@"Error" message:@"The operation didn't complete because the user denied permission."];
//        else
//            [self throwAlertWithTitle:@"Error" message:@"Account access denied."];
//    });
//}
//
//-(void)uploadWithFBAccount:(ACAccount *)facebookAccount
//{
//    ACAccountCredential *fbCredential = [facebookAccount credential];
//    NSString *accessToken = [fbCredential oauthToken];
//    
//    NSURL *videourl = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/me/videos?access_token=%@",accessToken]];
//    
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    
//    //    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    //    NSString *documentsDirectory = [paths objectAtIndex:0];
//    //    NSString* foofile = [documentsDirectory stringByAppendingPathComponent:@"me.mov"];
//    BOOL fileExists = [fileManager fileExistsAtPath:self.videoFilePath];
//    if (fileExists)
//    {
//        NSLog(@"file saved");
//    }
//    NSString *filePath = self.videoFilePath;
//    NSURL *pathURL = [[NSURL alloc]initFileURLWithPath:filePath isDirectory:NO];
//    NSData *videoData = [NSData dataWithContentsOfFile:filePath];
//    NSDictionary *params = @{
//                             @"title": self.title,//@"Me  silly"
//                             @"description": self.comment//@"Me testing the video upload to Facebook with the new Social Framework."
//                             };
//    
//    SLRequest *uploadRequest = [SLRequest requestForServiceType:SLServiceTypeFacebook
//                                                  requestMethod:SLRequestMethodPOST
//                                                            URL:videourl
//                                                     parameters:params];
//    [uploadRequest addMultipartData:videoData
//                           withName:@"source"
//                               type:@"video/quicktime"
//                           filename:[pathURL absoluteString]];
//    
//    uploadRequest.account = facebookAccount;
//    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
//        [uploadRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
//            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&error];
//            NSLog(@"responseDictionary : %@", responseDictionary);
//            NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
//            
//            if(error)
//            {
//                NSLog(@"Error %@", error.localizedDescription);
//            }
//            else
//            {
//                [[[UIAlertView alloc]initWithTitle:@"Congratulations!" message:@"Your video is suucessfully posted to your FB newsfeed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]show];
//                NSLog(@"%@", responseString);
//            }
//        }];
//    });
//}


@end
