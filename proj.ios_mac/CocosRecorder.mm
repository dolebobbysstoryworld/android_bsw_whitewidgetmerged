//
//  CocosRecorder.m
//  bobby
//
//  Created by Dongwook, Kim on 14. 5. 9..
//
//

#import "CocosRecorder.h"
#import "PlayAndRecordScene.h"

#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

#import "CCEAGLView.h"
#import "ios/AppController.h"
#import "ios/RootViewController.h"

#define USER_MOVIE_FILENAME     @"UserMovie.mp4"
#define DOCUMENTS_FOLDER [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]

@implementation CocosRecorder
{
    AVAssetWriter *_assetWriter;
    AVAssetWriterInput *assetWriterVideoInput;
    
    AVAssetReader *audioAssetReader;
    AVAssetReaderAudioMixOutput *audioAssetReaderOutput;
    AVAssetWriterInput *audioAssetWriterAudioInput;

    AVAssetWriterInputPixelBufferAdaptor *_assetWriterPixelBufferInput;

    CVOpenGLESTextureCacheRef rawDataTextureCache;

    CVOpenGLESTextureRef renderTexture;

    GLuint movieFramebuffer;
    GLint oldFramebuffer;
    GLuint outputTexture;
    
    CMTime currentTime;
    CFAbsoluteTime startedTime;
    CFAbsoluteTime accPausedTime;
    CFAbsoluteTime pausedStartTime;

    double prevMuxStartTime;
    float prevAudioDuration;


    int frameTime;
    
    EAGLContext *myContext;
    
    dispatch_queue_t movieWritingQueue;
    
    int inputWidth;
    int inputHeight;
    float scaleFactor;
    
    float nextDelay;
    
    BOOL isRecording;
    BOOL isRecordingAudio;
}

static CocosRecorder *sharedRecorder = nil;

+ (CocosRecorder *) sharedRecorder {
    @synchronized(self)     {
        if (!sharedRecorder) {
            AppController *controller = (AppController*)[UIApplication sharedApplication].delegate;
            CCEAGLView *glView = (CCEAGLView*)controller.viewController.view;
            sharedRecorder = [[CocosRecorder alloc] initWithContext:[glView context]];
        }
    }
    return sharedRecorder;
}

- (id)initWithContext:(EAGLContext *)context
{
    if (self = [super init]) {
        myContext = context;
        cocos2d::Size visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
        
        inputWidth = visibleSize.width;
        inputHeight = visibleSize.height;
        scaleFactor = cocos2d::CC_CONTENT_SCALE_FACTOR();
        
        self.renderTextureRecorder = RenderTextureRecorder::create();
        self.renderTextureRecorder->retain();
        self.renderTextureRecorder->drawCallback = &drawCallBack;

        movieWritingQueue = dispatch_queue_create("com.nemus.bobby.movieWritingQueue", NULL);
        isRecording = NO;
        isRecordingAudio = NO;
        
        audioAssetReader = nil;
        audioAssetReaderOutput = nil;
        audioAssetWriterAudioInput = nil;
     }
    
    return self;
}

- (void)dealloc;
{
    [self destroyDataFBO];
    
#if ( (__IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_6_0) || (!defined(__IPHONE_6_0)) )
    if (movieWritingQueue != NULL)
    {
        dispatch_release(movieWritingQueue);
    }
#endif
}

-(NSURL *)movieURL {
    NSString *pathToMovie = [DOCUMENTS_FOLDER stringByAppendingPathComponent:USER_MOVIE_FILENAME];
    NSURL *movieURL = [NSURL fileURLWithPath:pathToMovie];
    return movieURL;
}

-(void)checkForAndDeleteFile {
    NSString *pathToMovie = [DOCUMENTS_FOLDER stringByAppendingPathComponent:USER_MOVIE_FILENAME];
    int result = unlink([pathToMovie UTF8String]); // If a file already exists, AVAssetWriter won't let you record new frames, so delete the old movie
    NSLog(@"old file removed : %d", result);
}

-(void)createWriter{
    [self checkForAndDeleteFile];
    
    NSError *error;
    _assetWriter = [[AVAssetWriter alloc] initWithURL:[self movieURL] fileType:AVFileTypeMPEG4 error:&error];
//    _assetWriter.movieFragmentInterval = CMTimeMakeWithSeconds(1.0, 60);
    
    if (error) {
        NSLog(@"Couldn't create writer, %@", error.localizedDescription);
        return;
    }
    
    //    // video output resolution - 720p
    //    NSNumber *vw = @1280;
    //    NSNumber *vh = @720;

    // video output = device width and height
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    float scale = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)?1.0f:2.0f;
    
    if (screenHeight > screenWidth) {
        CGFloat temp = screenWidth;
        screenWidth = screenHeight;
        screenHeight = temp;
    }
    NSNumber *vw = [NSNumber numberWithFloat:screenWidth * scale];
    NSNumber *vh = [NSNumber numberWithFloat:screenHeight * scale];
    
    NSMutableDictionary *outputSettings = [NSMutableDictionary dictionary];
    NSDictionary *outputSetting = @{
                                     AVVideoCodecKey : AVVideoCodecH264,
                                     AVVideoWidthKey : vw,
                                     AVVideoHeightKey : vh
                                     };
    [outputSettings setValuesForKeysWithDictionary:outputSetting];
    
//    NSDictionary *videoCleanApertureSettings = [NSDictionary dictionaryWithObjectsAndKeys:
//                                                vw, AVVideoCleanApertureWidthKey,
//                                                vh, AVVideoCleanApertureHeightKey,
//                                                [NSNumber numberWithInt:0], AVVideoCleanApertureHorizontalOffsetKey,
//                                                [NSNumber numberWithInt:0], AVVideoCleanApertureVerticalOffsetKey,
//                                                nil];
    
//    NSDictionary *videoAspectRatioSettings = [NSDictionary dictionaryWithObjectsAndKeys:
//                                              [NSNumber numberWithInt:3], AVVideoPixelAspectRatioHorizontalSpacingKey,
//                                              [NSNumber numberWithInt:3], AVVideoPixelAspectRatioVerticalSpacingKey,
//                                              nil];
    
//    NSMutableDictionary * compressionProperties = [[NSMutableDictionary alloc] init];
//    [compressionProperties setObject:videoCleanApertureSettings forKey:AVVideoCleanApertureKey];
//    [compressionProperties setObject:videoAspectRatioSettings forKey:AVVideoPixelAspectRatioKey];
//    [compressionProperties setObject:[NSNumber numberWithInt: 2000000] forKey:AVVideoAverageBitRateKey];
//    [compressionProperties setObject:[NSNumber numberWithInt: 30] forKey:AVVideoMaxKeyFrameIntervalKey];
//    [compressionProperties setObject:AVVideoProfileLevelH264Main41 forKey:AVVideoProfileLevelKey];
    
//    [outputSettings setObject:compressionProperties forKey:AVVideoCompressionPropertiesKey];
    
    assetWriterVideoInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:outputSettings];
    assetWriterVideoInput.expectsMediaDataInRealTime = YES;
    
    NSNumber *viw = [NSNumber numberWithInt:inputWidth * scaleFactor];
    NSNumber *vih = [NSNumber numberWithInt:inputHeight * scaleFactor];
    
    NSDictionary *sourcePixelBufferAttributesDictionary = @{(id)kCVPixelBufferPixelFormatTypeKey: @(kCVPixelFormatType_32BGRA),
                                                            (id)kCVPixelBufferWidthKey: viw,
                                                            (id)kCVPixelBufferHeightKey: vih};
    
    _assetWriterPixelBufferInput = [AVAssetWriterInputPixelBufferAdaptor assetWriterInputPixelBufferAdaptorWithAssetWriterInput:assetWriterVideoInput sourcePixelBufferAttributes:sourcePixelBufferAttributesDictionary];
    
//    assetWriterVideoInput.transform = CGAffineTransformMakeScale(1, inverse?-1:1);
    
    if ([_assetWriter canAddInput:assetWriterVideoInput]) {
        [_assetWriter addInput:assetWriterVideoInput];
    } else {
        NSLog(@"can't add video writer input %@", assetWriterVideoInput);
    }
    
    AudioChannelLayout stereoChannelLayout = {
        .mChannelLayoutTag = kAudioChannelLayoutTag_Stereo,
        .mChannelBitmap = 0,
        .mNumberChannelDescriptions = 0
    };
    
    // Convert the channel layout object to an NSData object.
    NSData *channelLayoutAsData = [NSData dataWithBytes:&stereoChannelLayout length:offsetof(AudioChannelLayout, mChannelDescriptions)];

    // Get the compression settings for 128 kbps AAC.
    NSDictionary *compressionAudioSettings = @{
                                               AVFormatIDKey         : [NSNumber numberWithUnsignedInt:kAudioFormatMPEG4AAC],
                                               AVEncoderBitRateKey   : [NSNumber numberWithInteger:128000],
                                               AVSampleRateKey       : [NSNumber numberWithInteger:48000],
                                               AVChannelLayoutKey    : channelLayoutAsData,
                                               AVNumberOfChannelsKey : [NSNumber numberWithUnsignedInteger:2]
                                               };
    
    // Create the asset writer input with the compression settings and specify the media type as audio.
    audioAssetWriterAudioInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeAudio outputSettings:compressionAudioSettings];

    // Add the input to the writer if possible.
    if ([_assetWriter canAddInput:audioAssetWriterAudioInput]){
        [_assetWriter addInput:audioAssetWriterAudioInput];
    }
 }

-(void)muxAudio:(NSString *)voiceFile withBgm:(NSString *)bgm duration:(double)durationInMili{
    isRecordingAudio = NO;

//    bool isFirst = audioAssetReaderOutput == nil;
    
    if (audioAssetReaderOutput != nil) {
        audioAssetReaderOutput = nil;
    }
    
    if (audioAssetReader != nil) {
        [audioAssetReader cancelReading];
        audioAssetReader = nil;
    }
    
    NSError *outError;

    NSDictionary *decompressionAudioSettings = @{ AVFormatIDKey : [NSNumber numberWithUnsignedInt:kAudioFormatLinearPCM] };
    
    AVMutableComposition *composition = [AVMutableComposition composition];
    
    AVMutableCompositionTrack *bgmTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    AVMutableCompositionTrack *voiceTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    
    NSError *error;
    
    // for audio sync
    double startDelay = 0;
    if (prevMuxStartTime >= 0) {
        float timeDiff = currentTime.value - prevMuxStartTime;
        startDelay = timeDiff - prevAudioDuration;
    }
    NSLog(@"start delay = %f", startDelay);

    prevMuxStartTime = currentTime.value;
    prevAudioDuration = durationInMili;
    
    startDelay = MAX(startDelay, 0);

    if (durationInMili == 0) {
        return;
    } else {
        if (voiceFile != nil) {
            NSString *voiceFilePath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@", voiceFile]];
            AVAsset *voiceAsset = [AVAsset assetWithURL:[NSURL fileURLWithPath:voiceFilePath]];
            if(voiceAsset && [voiceAsset.tracks count] > 0) {
                float resourceDurationInMili = (voiceAsset.duration.value / (float)voiceAsset.duration.timescale) * 1000;
                [voiceTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, CMTimeMake(durationInMili, 1000))
                                    ofTrack:[[voiceAsset tracksWithMediaType:AVMediaTypeAudio]objectAtIndex:0]
                                     atTime:CMTimeMake(startDelay, 1000) error:&error];
                
                NSLog(@"voiceAsset duration : %f", resourceDurationInMili);
                nextDelay = durationInMili - (resourceDurationInMili);
                NSLog(@"next delay = %f", nextDelay);
            }
        }
        
        // TODO: bgm track volume down
        if (bgm != nil) {
            NSString *bgmFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"common/%@", bgm]];
            AVAsset *bgmAsset = [AVAsset assetWithURL:[NSURL fileURLWithPath:bgmFilePath]];
            if (bgmAsset && [bgmAsset.tracks count] > 0){
                float resourceDurationInMili = (bgmAsset.duration.value / (float)bgmAsset.duration.timescale) * 1000;
                NSLog(@"bgm duration : %f",resourceDurationInMili);
                
                AVAssetTrack *bgmAssetTrack = [[bgmAsset tracksWithMediaType:AVMediaTypeAudio]objectAtIndex:0];
                [bgmTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, CMTimeMake(durationInMili, 1000))
                                  ofTrack:bgmAssetTrack
                                   atTime:CMTimeMake(startDelay, 1000) error:&error];
            }
        }


    }
    
    audioAssetReaderOutput = [AVAssetReaderAudioMixOutput assetReaderAudioMixOutputWithAudioTracks:composition.tracks
                                                                                     audioSettings:decompressionAudioSettings];
    audioAssetReader = [AVAssetReader assetReaderWithAsset:composition error:&outError];
    BOOL success = (audioAssetReader != nil);
    if (!success) {
        NSLog(@"fail to create asset reader");
    }
    
    if ([audioAssetReader canAddOutput:audioAssetReaderOutput]){
        [audioAssetReader addOutput:audioAssetReaderOutput];
    }

    [audioAssetReader startReading];
    isRecordingAudio = YES;
    
    NSLog(@"mux start : %@, %@, %f", voiceFile, bgm, durationInMili);
}

-(void)createFBO
{
    if (!movieFramebuffer) {
        [self createDataFBO];
    }
}

-(void)startRecording {
//    [self createWriter];
//    [self checkForAndDeleteFile];
//    [self createFBO];
 
    [_assetWriter startWriting];
    NSLog(@"Start recording");
    
    startedTime = 0;
    frameTime = 0;
    accPausedTime = 0;
    pausedStartTime = 0;
    
    prevMuxStartTime = -1;
    prevAudioDuration = 0;
    
    currentTime = CMTimeMake(0, 1000);
    
    isRecording = YES;
    _isWriting = YES;
    _isPaused = NO;
}

-(void)finishRecording {
    _isWriting = NO;
    _isPaused = NO;
    isRecording = NO;
    isRecordingAudio = NO;
    
    prevMuxStartTime = -1;
    prevAudioDuration = 0;
    
    audioAssetReaderOutput = nil;
    NSLog(@"Finish recording");
    
    dispatch_async(movieWritingQueue, ^{
        [assetWriterVideoInput markAsFinished];
        [_assetWriter endSessionAtSourceTime:currentTime];
        [_assetWriter finishWritingWithCompletionHandler:^{
            cocos2d::Scene *runningScene = cocos2d::Director::getInstance()->getRunningScene();
            if (!runningScene) {
                return;
            }
            PlayAndRecord *playAndRecordLayer = (PlayAndRecord *)runningScene->getChildByTag(TAG_PLAY_AND_RECORDING);
            
            if (!playAndRecordLayer) {
                NSLog(@"not playAndRecordLayer layer");
                return;
            }
            
            PlayAndRecordType type = playAndRecordLayer->getType();
            if (type == PlayAndRecordType::RecordForShare) {
                NSString *movieFile = [[self movieURL] absoluteString];
                std::string fileName = std::string([movieFile UTF8String]);
                playAndRecordLayer->playAndRecordFinished(true, fileName);
            } else if (type == RecordForDownload){
                [self saveMovieToCameraRoll];
            }
        }];

        [audioAssetReader cancelReading];
    });
}

-(void)cancelRecording {
    _isWriting = NO;
    isRecording = NO;
    isRecordingAudio = NO;
    _isPaused = NO;
    audioAssetReaderOutput = nil;

    dispatch_async(movieWritingQueue, ^{
        [_assetWriter cancelWriting];
        [audioAssetReader cancelReading];
        [self checkForAndDeleteFile];
    });
}

-(void)pauseRecording
{
    _isWriting = NO;
    isRecording = NO;
    isRecordingAudio = NO;
    _isPaused = YES;

    pausedStartTime = CFAbsoluteTimeGetCurrent();
}

-(void)resumeRecording
{
    auto now = CFAbsoluteTimeGetCurrent();
    auto pausedTime = now - pausedStartTime;
    accPausedTime += pausedTime;
    
    _isWriting = YES;
    isRecording = YES;
    isRecordingAudio = YES;
    _isPaused = NO;
}

-(void)deleteRecordedFile
{
    [self checkForAndDeleteFile];
}

- (void)saveMovieToCameraRoll
{
	ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
	[library writeVideoAtPathToSavedPhotosAlbum:[self movieURL]
								completionBlock:^(NSURL *assetURL, NSError *error) {
                                    
                                    cocos2d::Scene *runningScene = cocos2d::Director::getInstance()->getRunningScene();
                                    if (!runningScene) {
                                        return;
                                    }
                                    PlayAndRecord *playAndRecordLayer = (PlayAndRecord *)runningScene->getChildByTag(TAG_PLAY_AND_RECORDING);
                                    
                                    if (!playAndRecordLayer) {
                                        NSLog(@"not playAndRecordLayer layer");
                                        return;
                                    }
                                    
									if (error) {
                                        NSLog(@"Error %@", [error localizedDescription]);
                                        playAndRecordLayer->playAndRecordFinished(false, "");
                                    } else {
                                        [self checkForAndDeleteFile];
                                        NSLog(@"finished saving");
                                        playAndRecordLayer->playAndRecordFinished(true, "");
                                    }
								}];
}

// method for pixel push
- (void)writePixel
{
    if (!self.isWriting) {
        return;
    }
    
    CFAbsoluteTime now = CFAbsoluteTimeGetCurrent();
    int oldFrameTime = frameTime;
    
    if (startedTime == 0) {
        [_assetWriter startSessionAtSourceTime:CMTimeMake(0, 1000)];
        startedTime = now;
    }
    frameTime = (int)((now - startedTime - accPausedTime) * 1000);
    currentTime = CMTimeMake(frameTime, 1000);
    
//    NSLog(@"now : %f, started : %f", now, startedTime);
//    NSLog(@"interval : %d", frameTime);
    
    if (frameTime != 0 && oldFrameTime == frameTime) {
        NSLog(@"Same index");
        return;
    }
    
    CVPixelBufferLockBaseAddress(self.renderTarget, 0);
    
    if (_assetWriterPixelBufferInput.assetWriterInput.readyForMoreMediaData) {
        BOOL success = [_assetWriterPixelBufferInput appendPixelBuffer:self.renderTarget withPresentationTime:currentTime];
        
//        if (!success) {
//            NSLog(@"Pixel Buffer not appended");
//        } else {
////            NSLog(@"write at %d", frameTime);
//        }
    } else {
//        NSLog(@"drop frame by status");
    }
    CVPixelBufferUnlockBaseAddress(self.renderTarget, 0);
    
//    NSLog(@"audioAssetWriterAudioInput status : %d", audioAssetWriterAudioInput.readyForMoreMediaData);
    
    // Copy the next sample buffer from the reader output.
    if (audioAssetWriterAudioInput.readyForMoreMediaData && isRecordingAudio) {
        CMSampleBufferRef sampleBuffer = [audioAssetReaderOutput copyNextSampleBuffer];
        if (sampleBuffer)
        {
            // Do something with sampleBuffer here.
            BOOL success = [audioAssetWriterAudioInput appendSampleBuffer:sampleBuffer];
//            if (!success) {
//                NSLog(@"Voice sampleBuffer not appended");
//            } else {
//                NSLog(@"Voice sampleBuffer is appended");
//            }
            
            CFRelease(sampleBuffer);
            sampleBuffer = NULL;
        }
        else
        {
            // Find out why the asset reader output couldn't copy another sample buffer.
            if (audioAssetReader.status == AVAssetReaderStatusFailed)
            {
                NSError *failureError = audioAssetReader.error;
                // Handle the error here.
                NSLog(@"voice append error = %@", failureError);
            }
            else
            {
                // The asset reader output has read all of its samples.
                //                    [audioAssetReader cancelReading];
                //                    [audioAssetWriterAudioInput markAsFinished];
                isRecordingAudio = NO;
            }
        }
    }
}

- (void)createDataFBO
{
    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &oldFramebuffer);
    glGenFramebuffers(1, &movieFramebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, movieFramebuffer);
    
    // Code originally sourced from http://allmybrain.com/2011/12/08/rendering-to-a-texture-with-ios-5-texture-cache-api/
    
    CVReturn err = CVOpenGLESTextureCacheCreate(kCFAllocatorDefault, NULL, myContext, NULL, &rawDataTextureCache);
    
    if (err)
    {
        NSAssert(NO, @"Error at CVOpenGLESTextureCacheCreate %d", err);
    }
    
    GLint oldRBO;
    glGetIntegerv(GL_RENDERBUFFER_BINDING, &oldRBO);

    CFDictionaryRef empty; // empty value for attr value.
    CFMutableDictionaryRef attrs;
    empty = CFDictionaryCreate(kCFAllocatorDefault, // our empty IOSurface properties dictionary
                               NULL,
                               NULL,
                               0,
                               &kCFTypeDictionaryKeyCallBacks,
                               &kCFTypeDictionaryValueCallBacks);
    attrs = CFDictionaryCreateMutable(kCFAllocatorDefault,
                                      1,
                                      &kCFTypeDictionaryKeyCallBacks,
                                      &kCFTypeDictionaryValueCallBacks);
    
    CFDictionarySetValue(attrs,
                         kCVPixelBufferIOSurfacePropertiesKey,
                         empty);

    int textureWidth = inputWidth * scaleFactor;
    int textureHeight = inputHeight * scaleFactor;
    
    
    CVPixelBufferCreate(kCFAllocatorDefault,
                        textureWidth,
                        textureHeight,
                        kCVPixelFormatType_32BGRA,
                        attrs,
                        &_renderTarget);
    
    CVBufferSetAttachment(self.renderTarget, kCVImageBufferColorPrimariesKey, kCVImageBufferColorPrimaries_ITU_R_709_2, kCVAttachmentMode_ShouldPropagate);
    CVBufferSetAttachment(self.renderTarget, kCVImageBufferYCbCrMatrixKey, kCVImageBufferYCbCrMatrix_ITU_R_601_4, kCVAttachmentMode_ShouldPropagate);
    CVBufferSetAttachment(self.renderTarget, kCVImageBufferTransferFunctionKey, kCVImageBufferTransferFunction_ITU_R_709_2, kCVAttachmentMode_ShouldPropagate);

    CVOpenGLESTextureCacheCreateTextureFromImage (kCFAllocatorDefault,
                                                  rawDataTextureCache, self.renderTarget,
                                                  NULL, // texture attributes
                                                  GL_TEXTURE_2D,
                                                  GL_RGBA, // opengl format
                                                  textureWidth,
                                                  textureHeight,
                                                  GL_BGRA, // native iOS format
                                                  GL_UNSIGNED_BYTE,
                                                  0,
                                                  &renderTexture);
    CFRelease(attrs);
    CFRelease(empty);

    glBindTexture(CVOpenGLESTextureGetTarget(renderTexture), CVOpenGLESTextureGetName(renderTexture));
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, CVOpenGLESTextureGetName(renderTexture), 0);
  
    self.renderTextureRecorder->initWithWidthAndHeight(inputWidth, inputHeight, cocos2d::Texture2D::PixelFormat::RGBA8888, 0, CVPixelBufferGetBaseAddress(self.renderTarget), CVOpenGLESTextureGetName(renderTexture), oldFramebuffer, movieFramebuffer, oldRBO);
}

- (void)destroyDataFBO
{
    if (movieFramebuffer)
    {
        glDeleteFramebuffers(1, &movieFramebuffer);
        movieFramebuffer = 0;
    }
    
    if (renderTexture)
    {
        CFRelease(renderTexture);
    }
    if (self.renderTarget)
    {
        CVPixelBufferRelease(self.renderTarget);
    }
    
    if (self.renderTextureRecorder) {
        self.renderTextureRecorder->release();
    }
}

void drawCallBack()
{
    if (sharedRecorder) {
        [sharedRecorder writePixel];
    }
}

@end
