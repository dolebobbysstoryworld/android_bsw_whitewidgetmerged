//
//  AUIPlayer.m
//  bobby
//
//  Created by Dongwook, Kim on 14. 7. 28..
//
//

#import "AUIPlayer.h"
#import <AudioToolbox/AudioServices.h>

@interface AUIPlayer()

@property (strong, nonatomic) AVAudioPlayer *currentPlayer;
@property (nonatomic) SOUND_TYPE currentType;

@end

@implementation AUIPlayer

static AUIPlayer *sharedPlayer = nil;


+(id)sharedPlayer
{
    @synchronized(self)     {
        if (!sharedPlayer) {
            sharedPlayer = [[AUIPlayer alloc] init];
            sharedPlayer.currentType = Sound_none;
            
            AVAudioSession *audioSession = [AVAudioSession sharedInstance];
            [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
            [audioSession setActive:YES error:nil];
        }
    }
    return sharedPlayer;

}

-(void)play:(SOUND_TYPE)s_type
{
    BOOL changed = false;
    if (_currentType != s_type) {
        _currentType = s_type;
        changed = true;
    }

    if (_currentPlayer == nil || changed) {
        
        if (_currentPlayer != nil) {
            _currentPlayer = nil;
        }
        
        NSString *fileName = [self getFileName:s_type];
        if (fileName == nil) {
            NSLog(@"no file name for type: %d", s_type);
            return;
        }
        NSError *error;

        NSString *filePath = [NSString stringWithFormat:@"common/%@",fileName];
        NSString *soundFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:filePath];
        NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
        
        _currentPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:&error];
        [_currentPlayer setVolume:1.0f];
        
        if (_currentType == Sound_character_walk) {
            _currentPlayer.numberOfLoops = -1;
        }
        
        if (error) {
            NSLog(@"error for load effect sound: %@", [error localizedDescription]);
        }
    }
    
    if (![_currentPlayer isPlaying]) {
        [_currentPlayer play];
    }
}

- (void)stop
{
    if (_currentPlayer != nil) {
        [_currentPlayer stop];
    }
}

- (NSString *)getFileName:(SOUND_TYPE)s_type
{
    switch (s_type) {
        case Sound_tap:
            return @"Tap.mp3";
        case Sound_character_walk:
            return @"Move.mp3";
        case Sound_bigger:
            return @"Bigger.mp3";
        case Sound_smaller:
            return @"Smaller.mp3";
        case Sound_journey_map:
            return @"Journey_map.mp3";
        case Sound_none:
        default:
            return nil;
            break;
    }
}


@end
