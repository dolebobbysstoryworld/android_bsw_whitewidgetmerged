//
//  MixpanelHelper.h
//  bobby
//
//  Created by White Widget on 2/8/15.
//
//

#import <Foundation/Foundation.h>

@interface MixpanelHelper : NSObject

+ (void)initializeMixpanel;

+ (void)signUpWithUserNumber:(NSInteger)userNumber
                       email:(NSString *)email
                      gender:(NSInteger)gender
                   birthyear:(NSString *)birthyear;

+ (void)loginWithUserNumber:(NSInteger)userNumber
                      email:(NSString *)email;

+ (void)loginWithUserNumber:(NSInteger)userNumber;

+ (void)modifyUserWithUserNumber:(NSInteger)userNumber
                          gender:(NSInteger)gender
                       birthyear:(NSString *)birthyear;

+ (void)signUpSNSWithUserNumber:(NSInteger)userNumber;

+ (void)updateUserWithUserNumber:(NSInteger)userNumber
                           email:(NSString *)email
                          gender:(NSInteger)gender
                       birthyear:(NSString *)birthyear;

// facebook login?

// event tracking ***

// sample storybook
+ (void)logOpenStorybookSampleA;
+ (void)logOpenStorybookSampleB;

+ (void)logPlayStorybookSampleVideoWithIndex:(int)index;
+ (void)logPlayStorybookSampleVideoAll;

// custom
+ (void)logCreateCustomStorybookStart;
+ (void)logCreateStoryStartWithIndex:(int)index isFinal:(BOOL)isFinal;
+ (void)logCreateCustomStorybookSuccess;
+ (void)logCreateStorysuccess;

// account creation tracking ***

+ (void)logOpenedSignUp;
+ (void)markStartSNSLogin;

+ (void)logDeleteAccount;

+ (void)logSubmittedFeedback;

+ (void)logInputOnRegistrationField;


@end
