//
//  CamUIViewController.h
//  bobby
//
//  Created by oasis on 2014. 4. 14..
//
//
#ifndef MACOS

#import <UIKit/UIKit.h>
#import "CamViewController.h"

@interface CamUIViewController : UIViewController <CamViewControllerDelegte, UIImagePickerControllerDelegate,UINavigationControllerDelegate, UIGestureRecognizerDelegate>

@end
#endif

