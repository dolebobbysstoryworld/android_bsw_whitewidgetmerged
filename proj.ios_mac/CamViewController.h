//
//  CamViewController.h
//  bobby
//
//  Created by oasis on 2014. 4. 14..
//
//

#ifndef MACOS

#import <UIKit/UIKit.h>
@protocol CamViewControllerDelegte <NSObject>
- (void)setCameraButtonEnabled:(NSString*)enabled;
- (void)setStillButtonEnabled:(NSString*)enabled;
- (void)setImageView:(UIImage*)image;
@end

@interface CamViewController : UIViewController
- (void)changeCamera:(id)sender;
- (void)snapStillImage:(id)sender;
- (void)rotateCameraLayer;
- (void)stopRunningAction;
@property (nonatomic, strong) NSObject<CamViewControllerDelegte> *camDelegate;
@end

#endif