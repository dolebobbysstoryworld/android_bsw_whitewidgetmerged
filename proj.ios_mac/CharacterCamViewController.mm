//
//  CharacterCamViewController.m
//  bobby
//
//  Created by oasis on 2014. 6. 25..
//
//
#ifndef MACOS
#import "CharacterCamViewController.h"
#include "CustomCameraScene.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "ImagePickerLandscapeController.h"
//#import "DKImageBrowser.h"
#import "CharacterImageCell.h"
#include "Define.h"
#import "AppController.h"

#import "UIImage+PathExtention.h"
#import "TSGuideforCreateCharacterView.h"
#import "TSGuideForCamera.h"
#import "TSToastView.h"

#include "external/json/document.h"

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_4INCH ([UIScreen mainScreen].bounds.size.height == 568)
#define DOCUMENTS_FOLDER [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]
#define CHARACTER_ITEM_FOLDER_NAME @"character"
#define FOLDER_MAIN @"main"
#define FOLDER_THUMB @"thumb"
#define TAG_FILL_LAYER 0
#define CHAR_PHOTO_NUM_KEY "char_photo_num_key"
#define DURATION_SHOW_GUIDE 1.8f

#define ZPOSITION_DEFAULT 10


static const NSTimeInterval kAnimationIntervalTransform = 0.2;

//typedef enum scrollType{
//    TYPE_MAIN = 0,
//    TYPE_THUMB = 1,
//}scrollType;

typedef enum buttons_type{
    BUTTONS_TYPE_DEFAULT = 0,
    BUTTONS_TYPE_RETAKE = 1,
    BUTTONS_TYPE_CANCEL = 2,
}buttons_type;

typedef enum imageType{
    TYPE_MAIN = 0,
    TYPE_NORMAL = 1,
    TYPE_SELECT = 2,
}imageType;

typedef struct {
    CGPoint tl,tr,bl,br;
} Rectangle;

@interface CharacterCamViewController ()<UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
//@property(nonatomic, strong) UIButton *recordButton;
@property(nonatomic, strong) UIButton *cameraButton;
@property(nonatomic, strong) CamViewController *camViewController;
@property(nonatomic, strong) UIButton *stillButton;
@property(nonatomic, strong) UIButton *libraryButton;
@property(nonatomic, strong) UIButton *retakeButton;
@property(nonatomic, strong) UIButton *cancelButton;
@property(nonatomic, strong) UIButton *saveButton;
@property(nonatomic, strong) UIButton *backButton;

@property(nonatomic, strong) UIView *editView;
@property(nonatomic, strong) UIImageView *editImageView;

@property (nonatomic, strong) UIPopoverController *popover;
@property(nonatomic, strong) ImagePickerLandscapeController *imagePicker;
@property(nonatomic) CGFloat lastScale;

@property (nonatomic,strong) NSMutableArray *thumbnailDataSource;
@property (nonatomic,strong) UICollectionView *thumbnailCollectionView;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;
@property (nonatomic) NSInteger currentPage;
@property (nonatomic, strong) UIImageView *characterFaceView;
@property (nonatomic,strong) UILabel *testLabel;

@property(nonatomic) CGFloat outputWidth;
@property(nonatomic) CGRect cropRect;
@property(nonatomic) CGRect initialImageFrame;
@property(nonatomic) CGAffineTransform validTransform;
@property(nonatomic) CGFloat scale;

@property(nonatomic) CGPoint touchCenter;
@property(nonatomic) CGPoint rotationCenter;
@property(nonatomic) CGPoint scaleCenter;
@property(nonatomic) CGFloat minimumScale;
@property(nonatomic) CGFloat maximumScale;
@property(nonatomic) NSUInteger gestureCount;
@property(nonatomic) BOOL checkBounds;

@property(nonatomic) UIPanGestureRecognizer *panGesuture;
@property(nonatomic, strong) NSTimer *timer;

@property(nonatomic, strong) CAShapeLayer *fillLayer;
@end

@implementation CharacterCamViewController

- (id)init
{
    self = [super init];
    if (self) {
        [self setViewFrame];
    }
    return self;
}

- (void)orientationChanged
{
	[self.camViewController rotateCameraLayer];
}

- (void)dealloc
{
    self.camViewController = nil;
    if (self.timer != nil) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

NSString *const cellIdentifer = @"CellIdentifer";

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // Do any additional setup after loading the view, typically from a nib.
#ifdef __IPHONE_8_0
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
    } else {
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.height, self.view.frame.size.width);
    }

#endif
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orientationChanged)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
    
    self.camViewController = [[CamViewController alloc] init];
    self.camViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width);
    self.camViewController.camDelegate = self;
    [self.view addSubview:self.camViewController.view];

    float radius = (IS_IPAD)?579/2:250/2;
    
    float characterListHeight = 0;
    if (IS_IPAD) {
        characterListHeight = 396/2;
    } else {
        characterListHeight = 202/2;
    }
    
    float thumbnailheight = (IS_IPAD)?358/2:172/2;
    float characterFaceHeight = (IS_IPAD)?1140/2:456/2;
    float radiusY = (characterFaceHeight - radius)/2;
    
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, self.view.bounds.size.height, self.view.bounds.size.width - thumbnailheight) cornerRadius:0];
    UIBezierPath *circlePath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(self.view.bounds.size.height/2 - radius/2, (characterFaceHeight - radius)/2, radius, radius) cornerRadius:radius/2];
    [path appendPath:circlePath];
    [path setUsesEvenOddFillRule:YES];
    
    self.fillLayer = [CAShapeLayer layer];
    self.fillLayer.path = path.CGPath;
    self.fillLayer.fillRule = kCAFillRuleEvenOdd;
    self.fillLayer.fillColor = [UIColor colorWithRed:0x2f/255.0f green:0x2f/255.0f blue:0x2f/255.0f alpha:255*0.75/255.0f].CGColor;
    [self.view.layer addSublayer:self.fillLayer];

    self.cropRect = CGRectMake(self.view.bounds.size.height/2 - radius/2, radiusY, radius, radius);
//    self.cropRect = CGRectMake(self.view.bounds.size.height/2 - radius/2, (self.view.bounds.size.width- characterListHeight)/2  - radius/2, radius, radius);

    
//        UIView *testView = [[UIView alloc] initWithFrame:self.cropRect];
//        testView.backgroundColor = [UIColor greenColor];
//        [self.view addSubview:testView];
    
    
    self.selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    CGRect faceImageFrame;
    if (IS_IPAD) {
        faceImageFrame = CGRectMake(self.view.bounds.size.height/2 -(1140/2)/2, 0, 1140/2, 1140/2);
    } else {
        faceImageFrame = CGRectMake(self.view.bounds.size.height/2 -(496/2)/2, 0, 496/2, 456/2);
    }
    self.characterFaceView = [[UIImageView alloc] initWithFrame:faceImageFrame];
//    self.characterFaceView.image = [UIImage imageNamedEx:[self getImageString:1 imageType:TYPE_MAIN] ignore:YES];//[UIImage imageNamed:[self getImageString:1 imageType:TYPE_MAIN]];
    self.characterFaceView.layer.zPosition = ZPOSITION_DEFAULT;
    [self.view addSubview:self.characterFaceView];
    
//    self.testLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, self.view.bounds.size.width/2-50/2, 200, 50)];
//    self.testLabel.textColor = [UIColor whiteColor];
//    self.testLabel.text = [NSString stringWithFormat:@"image num: %i",1];
//    [self.view addSubview:self.testLabel];
    
    //    [self addImageScrollView];
    
    self.backButton = [self getButton:@"btn_map_back_normal"
                               pressedName:@"btn_map_back_press"
                                  iconName:nil
                                   padRect:CGRectMake(self.view.frame.size.height-(self.view.frame.size.height-(50/2)), 40/2, 186/2, 186/2)
                                 phoneRect:CGRectMake(self.view.frame.size.height-(self.view.frame.size.height-(20/2)-(90/2)), 18/2, 90/2, 90/2)
                                buttonText:nil textColor:nil];
    [self.backButton addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
    self.backButton.layer.zPosition = ZPOSITION_DEFAULT;
    [self.view addSubview:self.backButton];

    
    self.cameraButton = [self getButton:@"camera_btn_invert_normal"
                            pressedName:@"camera_btn_invert_press"
                               iconName:nil
                                padRect:CGRectMake((50+186+24+1528+36)/2, 86/2, 188/2, 110/2)
                              phoneRect:CGRectMake(self.view.frame.size.height-(20/2), 16/2, 90/2, 52/2)
                             buttonText:nil textColor:nil];
    [self.cameraButton addTarget:self action:@selector(changeCamera:) forControlEvents:UIControlEventTouchUpInside];
    self.cameraButton.layer.zPosition = ZPOSITION_DEFAULT;
    [self.view addSubview:self.cameraButton];
    
    self.stillButton = [self getButton:@"camera_btn_camera_bg_normal"
                           pressedName:@"camera_btn_camera_bg_pressed"
                              iconName:@"camera_btn_camera_icon"
                               padRect:CGRectMake((50+186+24+1528+36)/2, 280/2, 188/2, 274/2)
                             phoneRect:CGRectMake(self.view.frame.size.height-(20/2), (16+52+14)/2, 90/2, 132/2)
                            buttonText:nil textColor:nil];
    [self.stillButton addTarget:self action:@selector(snapStillImage:) forControlEvents:UIControlEventTouchUpInside];
    self.stillButton.layer.zPosition = ZPOSITION_DEFAULT;
    [self.view addSubview:self.stillButton];
    self.stillButton.enabled = NO;
    
    self.libraryButton = [self getButton:@"camera_btn_import_bg_normal"
                             pressedName:@"camera_btn_import_bg_pressed"
                                iconName:@"camera_btn_import_icon"
                                 padRect:CGRectMake((50+186+24+1528+36)/2, (280+274+68)/2, 188/2, 274/2)
                               phoneRect:CGRectMake(self.view.frame.size.height-(20/2), (16+52+14+132+30)/2, 90/2, 132/2)
                              buttonText:nil textColor:nil];
    [self.libraryButton addTarget:self action:@selector(photoLibrary:) forControlEvents:UIControlEventTouchUpInside];
    self.libraryButton.layer.zPosition = ZPOSITION_DEFAULT;
    [self.view addSubview:self.libraryButton];
    
    self.retakeButton = [self getButton:@"camera_btn_retake_bg_normal"
                            pressedName:@"camera_btn_retake_bg_pressed"
                               iconName:@"camera_btn_retake_icon"
                                padRect:CGRectMake((50+186+24+1528+36)/2, 280/2, 188/2, 274/2)
                              phoneRect:CGRectMake(self.view.frame.size.height-(20/2), 80/2, 90/2, 132/2)
                             buttonText:NSLocalizedString(@"PF_CREATECHAR_BUTTON_TITLE_RETAKE", @"Retake")
                              textColor:[UIColor colorWithRed:0/255.0f green:96.0f/255.0f blue:97.0f/255.0f alpha:255.0f/255.0f]];
    [self.retakeButton addTarget:self action:@selector(retakeAction:) forControlEvents:UIControlEventTouchUpInside];
    self.retakeButton.layer.zPosition = ZPOSITION_DEFAULT;
    [self.view addSubview:self.retakeButton];
    
    
    self.cancelButton = [self getButton:@"camera_btn_cancel_bg_normal"
                            pressedName:@"camera_btn_cancel_bg_pressed"
                               iconName:@"camera_btn_cancel_icon"
                                padRect:CGRectMake((50+186+24+1528+36)/2, 280/2, 188/2, 274/2)
                              phoneRect:CGRectMake(self.view.frame.size.height-(20/2), 80/2, 90/2, 132/2)
                             buttonText:NSLocalizedString(@"PF_CREATECHAR_BUTTON_TITLE_CANCEL", @"Cancel")
                              textColor:[UIColor colorWithRed:178.0f/255.0f green:88.0f/255.0f blue:8.0f/255.0f alpha:255.0f/255.0f]];
    [self.cancelButton addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
    self.cancelButton.layer.zPosition = ZPOSITION_DEFAULT;
    [self.view addSubview:self.cancelButton];
    
    self.saveButton = [self getButton:@"camera_btn_save_bg_normal"
                          pressedName:@"camera_btn_save_bg_pressed"
                             iconName:@"camera_btn_save_icon"
                              padRect:CGRectMake((50+186+24+1528+36)/2, (280+274+68)/2, 188/2, 274/2)
                            phoneRect:CGRectMake(self.view.frame.size.height-(20/2), (80+132+30)/2, 90/2, 132/2)
                           buttonText:NSLocalizedString(@"PF_CREATECHAR_BUTTON_TITLE_SAVE", @"Save")
                            textColor:[UIColor colorWithRed:138.0f/255.0f green:49.0f/255.0f blue:14.0f/255.0f alpha:255.0f/255.0f]];
    [self.saveButton addTarget:self action:@selector(saveAction:) forControlEvents:UIControlEventTouchUpInside];
    self.saveButton.layer.zPosition = ZPOSITION_DEFAULT;
    [self.view addSubview:self.saveButton];
    
    [self.retakeButton setHidden:true];
    [self.cancelButton setHidden:true];
    [self.saveButton setHidden:true];
    
    UICollectionViewFlowLayout *bottomLayout = [[UICollectionViewFlowLayout alloc] init];
    bottomLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    bottomLayout.minimumInteritemSpacing = 0;
    bottomLayout.minimumLineSpacing = 0;
    
//    self.thumbnailDataSource = @[ [UIImage imageNamed:[self getImageString:1 imageType:TYPE_NORMAL]],
//                                  [UIImage imageNamed:[self getImageString:2 imageType:TYPE_NORMAL]],
//                                  [UIImage imageNamed:[self getImageString:3 imageType:TYPE_NORMAL]],
//                                  [UIImage imageNamed:[self getImageString:4 imageType:TYPE_NORMAL]],
//                                  [UIImage imageNamed:[self getImageString:5 imageType:TYPE_NORMAL]],
//                                  [UIImage imageNamed:[self getImageString:6 imageType:TYPE_NORMAL]],
//                                  [UIImage imageNamed:[self getImageString:7 imageType:TYPE_NORMAL]],
//                                  [UIImage imageNamed:[self getImageString:8 imageType:TYPE_NORMAL]],
//                                  [UIImage imageNamed:[self getImageString:9 imageType:TYPE_NORMAL]],
//                                  [UIImage imageNamed:[self getImageString:10 imageType:TYPE_NORMAL]],
//                                  [UIImage imageNamed:[self getImageString:11 imageType:TYPE_NORMAL]],
//                                  [UIImage imageNamed:[self getImageString:12 imageType:TYPE_NORMAL]]
//                                  ];
    
    auto ud = UserDefault::getInstance();
    auto orderContentStr = ud->getStringForKey(PRE_LOAD_OBJ_ORDER_KEY);
    rapidjson::Document orderDocument;
    orderDocument.Parse<0>(orderContentStr.c_str());
    rapidjson::Value& order = orderDocument["character_order"];
    
    self.thumbnailDataSource = [NSMutableArray array];
    for(int index = 0; index < order.Size(); index++){
        auto characterKey = __String::create(order[rapidjson::SizeType(index)].GetString());
        
        auto characterJsonString = ud->getStringForKey(characterKey->getCString());
        rapidjson::Document characterInfo;
        characterInfo.Parse<0>(characterJsonString.c_str());
        
        bool isLock = characterInfo["locked"].GetBool();
        if (!isLock) {
            if (characterInfo.HasMember("custom_normal"))
            {
                auto normal = (char *)characterInfo["custom_normal"].GetString();
                auto select = (char *)characterInfo["custom_select"].GetString();
                auto face = (char *)characterInfo["custom_face"].GetString();
                
                NSString *normalStr = [NSString stringWithUTF8String:normal];
                NSString *selectStr = [NSString stringWithUTF8String:select];
                NSString *faceStr = [NSString stringWithUTF8String:face];
                NSString *charKey = [NSString stringWithUTF8String:characterKey->getCString()];
                
                NSDictionary *resource = @{@"charkey":charKey,
                                           @"normal":normalStr,
                                           @"select":selectStr,
                                           @"face":faceStr};
                [self.thumbnailDataSource addObject:resource];
            }
        }
    }
    
    NSLog(@"count : %lu", (unsigned long)self.thumbnailDataSource.count);
    self.characterFaceView.image = [UIImage imageNamedEx:self.thumbnailDataSource[0][@"face"] ignore:YES];
    
//    self.thumbnailDataSource = @[ [UIImage imageNamedEx:[self getImageString:1 imageType:TYPE_NORMAL] ignore:YES],
//                                  [UIImage imageNamedEx:[self getImageString:2 imageType:TYPE_NORMAL] ignore:YES],
//                                  [UIImage imageNamedEx:[self getImageString:3 imageType:TYPE_NORMAL] ignore:YES],
//                                  [UIImage imageNamedEx:[self getImageString:4 imageType:TYPE_NORMAL] ignore:YES],
//                                  [UIImage imageNamedEx:[self getImageString:5 imageType:TYPE_NORMAL] ignore:YES],
//                                  [UIImage imageNamedEx:[self getImageString:6 imageType:TYPE_NORMAL] ignore:YES],
//                                  [UIImage imageNamedEx:[self getImageString:7 imageType:TYPE_NORMAL] ignore:YES],
//                                  [UIImage imageNamedEx:[self getImageString:8 imageType:TYPE_NORMAL] ignore:YES],
//                                  [UIImage imageNamedEx:[self getImageString:9 imageType:TYPE_NORMAL] ignore:YES],
//                                  [UIImage imageNamedEx:[self getImageString:10 imageType:TYPE_NORMAL] ignore:YES],
//                                  [UIImage imageNamedEx:[self getImageString:11 imageType:TYPE_NORMAL] ignore:YES],
//                                  [UIImage imageNamedEx:[self getImageString:12 imageType:TYPE_NORMAL] ignore:YES],
//                                  [UIImage imageNamedEx:[self getImageString:13 imageType:TYPE_NORMAL] ignore:YES],
//                                  [UIImage imageNamedEx:[self getImageString:14 imageType:TYPE_NORMAL] ignore:YES]
//                                  ];
    
    self.thumbnailCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.width-thumbnailheight, self.view.bounds.size.height, thumbnailheight) collectionViewLayout:bottomLayout];
    self.thumbnailCollectionView.showsHorizontalScrollIndicator = NO;
    self.thumbnailCollectionView.delegate = self;
    self.thumbnailCollectionView.dataSource = self;
    [self.thumbnailCollectionView registerClass:[CharacterImageCell class] forCellWithReuseIdentifier:cellIdentifer];
    [self.view addSubview:self.thumbnailCollectionView];
    
    UIImage *background = [UIImage imageNamedEx:@"camera_frame_background_view_alpha.png" ignore:YES];
//    if (IS_IPAD) {
//        background = [UIImage imageNamed:@"iosnative/camera_frame_background_view_alpha_pad.png"];
//    } else {
//        background = [UIImage imageNamed:@"iosnative/camera_frame_background_view_alpha.png"];
//    }
    UIColor *pattern = [UIColor colorWithPatternImage:background];
//    self.thumbnailCollectionView.backgroundColor = [UIColor clearColor];
    self.thumbnailCollectionView.backgroundColor = pattern;
    //    self.thumbnailCollectionView.backgroundView.layer.opacity = 0.6f;
    [self.thumbnailCollectionView reloadData];
}

- (void)setDimLayer:(float)radius
{
    float thumbnailheight = (IS_IPAD)?358/2:172/2;
    float characterFaceHeight = (IS_IPAD)?1140/2:456/2;
    
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - thumbnailheight) cornerRadius:0];
    UIBezierPath *circlePath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(self.view.bounds.size.width/2 - radius/2, (characterFaceHeight - radius)/2, radius, radius) cornerRadius:radius/2];
    [path appendPath:circlePath];
    [path setUsesEvenOddFillRule:YES];
    
    if (self.fillLayer) {
        [self.fillLayer removeFromSuperlayer];
        self.fillLayer = nil;
    }
    
    self.fillLayer = [CAShapeLayer layer];
    self.fillLayer.path = path.CGPath;
    self.fillLayer.fillRule = kCAFillRuleEvenOdd;
    self.fillLayer.fillColor = [UIColor colorWithRed:0x2f/255.0f green:0x2f/255.0f blue:0x2f/255.0f alpha:255*0.75/255.0f].CGColor;
    self.fillLayer.zPosition = self.characterFaceView.layer.zPosition - 1;
    [self.view.layer addSublayer:self.fillLayer];
}

-(UIButton*)getButton:(NSString *)normalImgName pressedName:(NSString *)pressedImgNamed  iconName:(NSString*)iconName padRect:(CGRect)padRect phoneRect:(CGRect)phoneRect buttonText:(NSString*)buttonText textColor:(UIColor*)textColor
{
    UIImage *normalImage = [UIImage imageNamedEx:[NSString stringWithFormat:@"%@.png", normalImgName] ignore:YES];
    UIImage *pressImage = [UIImage imageNamedEx:[NSString stringWithFormat:@"%@.png", pressedImgNamed] ignore:YES];
    UIImage *buttonIcon = nil;
    if (iconName != nil) {
        buttonIcon = [UIImage imageNamedEx:[NSString stringWithFormat:@"%@.png", iconName] ignore:YES];
    }
    int iconYforText = 0;
    CGRect buttonRect;
    CGRect labelRect;
    if (IS_IPAD) {
//        normalImage = [UIImage imageNamed:[NSString stringWithFormat:@"iosnative/%@_pad.png", normalImgName]];
//        pressImage = [UIImage imageNamed:[NSString stringWithFormat:@"iosnative/%@_pad.png", pressedImgNamed]];
        buttonRect = CGRectMake(padRect.origin.x, padRect.origin.y, normalImage.size.width, normalImage.size.height);
//        if (iconName != nil) {
//            buttonIcon = [UIImage imageNamed:[NSString stringWithFormat:@"iosnative/%@_pad.png", iconName]];
//        }
        if (buttonText != nil) {
            labelRect = CGRectMake(normalImage.size.width/2 - buttonIcon.size.width/2 - 8/2, (normalImage.size.height - (buttonIcon.size.height+(46/2)))/2+buttonIcon.size.height, buttonIcon.size.width + 8, 46/2);
            iconYforText = (46/2)/2;
        }
    } else {
//        normalImage = [UIImage imageNamed:[NSString stringWithFormat:@"iosnative/%@.png", normalImgName]];
//        pressImage = [UIImage imageNamed:[NSString stringWithFormat:@"iosnative/%@.png", pressedImgNamed]];
        buttonRect = CGRectMake(phoneRect.origin.x-normalImage.size.width, phoneRect.origin.y, normalImage.size.width, normalImage.size.height);
//        if (iconName != nil) {
//            buttonIcon = [UIImage imageNamed:[NSString stringWithFormat:@"iosnative/%@.png", iconName]];
//        }
        if (buttonText != nil) {
            labelRect = CGRectMake(normalImage.size.width/2 - buttonIcon.size.width/2 - 4/2, (normalImage.size.height - (buttonIcon.size.height+(24/2)))/2+buttonIcon.size.height, buttonIcon.size.width + 4, 24/2);
            iconYforText = (24/2)/2;
        }
    }
    
    UIButton *button = [[UIButton alloc] initWithFrame:buttonRect];
    [button setImage:normalImage forState:UIControlStateNormal];
    [button setImage:pressImage forState:UIControlStateHighlighted];
    
    if (iconName != nil) {
        UIImageView *iconView = [[UIImageView alloc] initWithImage:buttonIcon];
        iconView.frame = CGRectMake(button.frame.size.width/2 - buttonIcon.size.width/2, button.frame.size.height/2 - buttonIcon.size.height/2 - iconYforText, buttonIcon.size.width, buttonIcon.size.height);
        [button addSubview:iconView];
    }
    
    if (buttonText != nil) {
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:buttonText];
        [str addAttribute:NSForegroundColorAttributeName value:textColor range:NSMakeRange(0,buttonText.length)];
        [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica-bold" size:[self getLabelFontSize:buttonText]] range:NSMakeRange(0,buttonText.length)];
        //        NSShadow *shadow = [[NSShadow alloc] init];
        //        [shadow setShadowColor:[UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:255*0.33/255.0f]];
        //        [shadow setShadowOffset:CGSizeMake(0.0f, 0.5f)];
        //        shadow.shadowBlurRadius = 0.0f;
        //        [str addAttribute:NSShadowAttributeName value:shadow range:NSMakeRange(0,buttonText.length)];
        
        UILabel *buttonLabel = [[UILabel alloc] initWithFrame:labelRect];
        //        buttonLabel.text = buttonText;
        //        buttonLabel.textColor = textColor;
        //        buttonLabel.font = [UIFont fontWithName:@"Helvetica" size:(IS_IPAD)?42/2:20/2];
        buttonLabel.attributedText = str;
        [buttonLabel setTextAlignment:NSTextAlignmentCenter];
        [button addSubview:buttonLabel];
    }
    
    return button;
}

- (int)getLabelFontSize:(NSString *)buttonText
{
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    int fontSize;
    if ([language isEqualToString:@"ja"]) {
        if ([buttonText isEqualToString:NSLocalizedString(@"PF_CREATECHAR_BUTTON_TITLE_CANCEL", @"Cancel")]) {
            fontSize = (IS_IPAD)?(32-4)/2:(14)/2;
        } else {
            fontSize = (IS_IPAD)?36/2:16/2;
        }
    } else if ([language isEqualToString:@"ko"]) {
        fontSize = (IS_IPAD)?36/2:16/2;
    } else {
        fontSize = (IS_IPAD)?42/2:20/2;
    }
    return fontSize;
}

- (void)photoLibrary:(UIButton*)sender
{
    [self setViewFrame];
    if ([ImagePickerLandscapeController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]) {
        if(self.imagePicker == nil){
            self.imagePicker = [[ImagePickerLandscapeController alloc] init];
            self.imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
            self.imagePicker.delegate = self;
            self.imagePicker.allowsEditing = NO;
        }
        
        if (IS_IPAD) {
            CGRect libraryRect;
            libraryRect = CGRectMake((50+186+24+1528+36)/2 - 454, (462+274+68)/2 - 80, self.libraryButton.frame.size.width, self.libraryButton.frame.size.height);
            //            libraryRect = CGRectMake(self.view.frame.size.height-(20/2)-self.libraryButton.frame.size.width, (174+132+30)/2, self.libraryButton.frame.size.width, self.libraryButton.frame.size.height);
            
            if (!self.popover) {
                self.popover = [[UIPopoverController alloc] initWithContentViewController:self.imagePicker];
            }
            if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                [self.popover presentPopoverFromRect:libraryRect inView:self.view permittedArrowDirections:NO animated:YES];
            } else {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self.popover presentPopoverFromRect:libraryRect inView:self.view permittedArrowDirections:NO animated:YES];
                }];
            }
            
        } else {
            self.backButton.enabled = false;
            UIViewController *sceneViewController;
            if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                sceneViewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
                [sceneViewController presentViewController:self.imagePicker animated:YES completion:^{self.backButton.enabled = true; }];
            } else {
                sceneViewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [sceneViewController presentViewController:self.imagePicker animated:YES completion:^{self.backButton.enabled = true; }];
                }];
            }
            
        }
    } else {
        
        return;
    }
}

- (void)navigationController:(UINavigationController*)navigationController willShowViewController:(UIViewController*)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:true];
    if (IS_IPAD) {
        float systemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
        if (systemVersion >= 7.0) {
            viewController.preferredContentSize = CGSizeMake(1528/2 + 40, 1148/2);
        } else {
            viewController.contentSizeForViewInPopover = CGSizeMake(1528/2 + 40, 1148/2);
        }
    }
}

- (void)closeAction:(id)sender
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    
    [self resetEditView];
    
    [self.camViewController stopRunningAction];

//    [self dismissViewControllerAnimated:NO completion:nil];
    cocos2d::Scene *cameraScene = cocos2d::Director::getInstance()->getRunningScene();
    if (!cameraScene) {
        return;
    }
    CustomCamera *cameraLayer = (CustomCamera*)cameraScene->getChildByTag(TAG_CAMERA);
    if (!cameraLayer) {
        NSLog(@"not camera layer");
        return;
    }
    
    for (UIView *subView in [self.view subviews]) {
        [subView removeFromSuperview];
    }
    
    for (CAShapeLayer *sublayer in [self.view.layer sublayers])
    {
        [sublayer removeFromSuperlayer];
    }
    NSLog(@"close action : ");
    cameraLayer->btnBackCallback();
    NSLog(@"cameraLayer callback ");
}

//- (void)addImageScrollView
//{
//    NSArray *imageDataSource = @[ [UIImage imageNamed:[self getImageString:1 scrollType:TYPE_MAIN]],
//                                  [UIImage imageNamed:[self getImageString:2 scrollType:TYPE_MAIN]],
//                                  [UIImage imageNamed:[self getImageString:3 scrollType:TYPE_MAIN]],
//                                  [UIImage imageNamed:[self getImageString:4 scrollType:TYPE_MAIN]],
//                                  [UIImage imageNamed:[self getImageString:5 scrollType:TYPE_MAIN]],
//                                  [UIImage imageNamed:[self getImageString:6 scrollType:TYPE_MAIN]],
//                                  [UIImage imageNamed:[self getImageString:7 scrollType:TYPE_MAIN]],
//                                  [UIImage imageNamed:[self getImageString:8 scrollType:TYPE_MAIN]],
//                                  [UIImage imageNamed:[self getImageString:9 scrollType:TYPE_MAIN]],
//                                  [UIImage imageNamed:[self getImageString:10 scrollType:TYPE_MAIN]],
//                                  [UIImage imageNamed:[self getImageString:11 scrollType:TYPE_MAIN]],
//                                  [UIImage imageNamed:[self getImageString:12 scrollType:TYPE_MAIN]],
//                                  [UIImage imageNamed:[self getImageString:13 scrollType:TYPE_MAIN]]
//                                  ];
//
//    NSArray *thumbDataSource = @[ [UIImage imageNamed:[self getImageString:1 scrollType:TYPE_THUMB]],
//                                  [UIImage imageNamed:[self getImageString:2 scrollType:TYPE_THUMB]],
//                                  [UIImage imageNamed:[self getImageString:3 scrollType:TYPE_THUMB]],
//                                  [UIImage imageNamed:[self getImageString:4 scrollType:TYPE_THUMB]],
//                                  [UIImage imageNamed:[self getImageString:5 scrollType:TYPE_THUMB]],
//                                  [UIImage imageNamed:[self getImageString:6 scrollType:TYPE_THUMB]],
//                                  [UIImage imageNamed:[self getImageString:7 scrollType:TYPE_THUMB]],
//                                  [UIImage imageNamed:[self getImageString:8 scrollType:TYPE_THUMB]],
//                                  [UIImage imageNamed:[self getImageString:9 scrollType:TYPE_THUMB]],
//                                  [UIImage imageNamed:[self getImageString:10 scrollType:TYPE_THUMB]],
//                                  [UIImage imageNamed:[self getImageString:11 scrollType:TYPE_THUMB]],
//                                  [UIImage imageNamed:[self getImageString:12 scrollType:TYPE_THUMB]],
//                                  [UIImage imageNamed:[self getImageString:13 scrollType:TYPE_THUMB]]
//                                  ];
//
//    DKImageBrowser *imageBrowser = [[DKImageBrowser alloc] init];
//    imageBrowser.view.backgroundColor = [UIColor clearColor];
//    imageBrowser.DKImageDataSource = imageDataSource;
//    imageBrowser.thumbnailDataSource = thumbDataSource;
//    [self addChildViewController:imageBrowser];
//    [self.view addSubview:imageBrowser.view];
//}
//
//- (NSString*)getImageString:(int)num scrollType:(scrollType)type
//{
//    NSString *result = NULL;
//    if (type == TYPE_MAIN) {
//        if (IS_IPAD) {
//            result = [NSString stringWithFormat:@"iosnative/camera_character_frame_%02d_pad.png",num];
//        } else {
//            result = [NSString stringWithFormat:@"iosnative/camera_character_frame_%02d.png",num];
//        }
//    } else {
//        if (IS_IPAD) {
//            result = [NSString stringWithFormat:@"iosnative/home_character_%02d_normal_pad.png",num];
//        } else {
//            result = [NSString stringWithFormat:@"iosnative/home_character_%02d_normal.png",num];
//        }
//    }
//    NSLog(@"result str : %@",result);
//    return result;
//}

- (void)buttonControl:(buttons_type)type
{
    switch (type) {
        case BUTTONS_TYPE_DEFAULT:
        {
            [self.stillButton setHidden:false];
            [self.libraryButton setHidden:false];
            [self.retakeButton setHidden:true];
            [self.saveButton setHidden:true];
            [self.cancelButton setHidden:true];
            [self.cameraButton setHidden:false];
            self.stillButton.enabled = true;
            self.libraryButton.enabled = true;
            self.retakeButton.enabled = false;
            self.saveButton.enabled = false;
            self.cancelButton.enabled = false;
            break;
        }
        case BUTTONS_TYPE_RETAKE:
        {
            [self.stillButton setHidden:true];
            [self.libraryButton setHidden:true];
            [self.retakeButton setHidden:false];
            [self.cameraButton setHidden:true];
            
            self.retakeButton.enabled = false;
            [self.saveButton setHidden:false];
            [self.cancelButton setHidden:true];
            self.stillButton.enabled = false;
            self.libraryButton.enabled = false;
            self.saveButton.enabled = true;
            self.cancelButton.enabled = false;
            
            self.backButton.enabled = false;
            break;
        }
        case BUTTONS_TYPE_CANCEL:
        {
            [self.stillButton setHidden:true];
            [self.libraryButton setHidden:true];
            [self.retakeButton setHidden:true];
            [self.saveButton setHidden:false];
            [self.cancelButton setHidden:false];
            [self.cameraButton setHidden:true];
            
            self.stillButton.enabled = false;
            self.libraryButton.enabled = false;
            self.retakeButton.enabled = false;
            self.saveButton.enabled = true;
            self.cancelButton.enabled = true;
            break;
        }
        default:
            break;
    }
}

- (void)snapStillImage:(UIButton*)sender
{
    [self photoEditView];
    
    [self buttonControl:BUTTONS_TYPE_RETAKE];
    
    [self.camViewController snapStillImage:sender];
}

- (void)changeCamera:(UIButton*)sender
{
    [self.camViewController changeCamera:sender];
}

- (void)retakeAction:(UIButton *)sender
{
    [self buttonControl:BUTTONS_TYPE_DEFAULT];
    
    [self resetEditView];
}

- (void)cancelAction:(UIButton *)sender
{
    [self buttonControl:BUTTONS_TYPE_DEFAULT];
    
    [self resetEditView];
    
    [self.camViewController rotateCameraLayer];
}

- (void)saveAction:(UIButton *)sender
{
    if(!self.editView || !self.editImageView || self.editImageView.image == nil) return;
    
    self.backButton.enabled = false;
    float retinaScale =([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale == 2.0))?2.0:1.0;
    
//    UIImage *resultImage = [self cropImage:(604/2)*retinaScale];
    
    UIImage *resultImage = nil;
    //0,1,2  //460:11 //546:8,9 //538:10 //600:5 //700:3 //654:4 //656:6 //612:7 //children boy:594 girl :586
    float thumbDiameterPhone = 0;
    float thumbDiameterPad = 0;
    
    NSString *characterKey = self.thumbnailDataSource[self.selectedIndexPath.row][@"charkey"];
    NSLog(@"charkey : %@", characterKey);
    if ([characterKey isEqualToString:@"C005"]) {
        resultImage = [self cropImage:(700/2)*retinaScale];
        thumbDiameterPhone = 60/2;
        thumbDiameterPad = 126/2;
    } else if ([characterKey isEqualToString:@"C009"]) {
        resultImage = [self cropImage:(654/2)*retinaScale];
        thumbDiameterPhone = 57/2;
        thumbDiameterPad = 118/2;
    } else if ([characterKey isEqualToString:@"C006"]) {
        resultImage = [self cropImage:(600/2)*retinaScale];
        thumbDiameterPhone = 51/2;
        thumbDiameterPad = 108/2;
    } else if ([characterKey isEqualToString:@"C007"]) {
        resultImage = [self cropImage:(656/2)*retinaScale];
        thumbDiameterPhone = 59/2;
        thumbDiameterPad = 120/2;
    } else if ([characterKey isEqualToString:@"C008"]) {
        resultImage = [self cropImage:(612/2)*retinaScale];
        thumbDiameterPhone = 56/2;
        thumbDiameterPad = 116/2;
    } else if ([characterKey isEqualToString:@"C001"]) {
        resultImage = [self cropImage:(546/2)*retinaScale];
        thumbDiameterPhone = 49/2;
        thumbDiameterPad = 98/2;
    } else if ([characterKey isEqualToString:@"C002"]) {
        resultImage = [self cropImage:(538/2)*retinaScale];
        thumbDiameterPhone = 47/2;
        thumbDiameterPad = 99/2;
    } else if ([characterKey isEqualToString:@"C003"]) {
        resultImage = [self cropImage:(538/2)*retinaScale];
        thumbDiameterPhone = 47/2;
        thumbDiameterPad = 99/2;
    } else if ([characterKey isEqualToString:@"C004"]) {
        resultImage = [self cropImage:(460/2)*retinaScale];
        thumbDiameterPhone = 42/2;
        thumbDiameterPad = 86/2;
    } else if ([characterKey isEqualToString:@"C015"]) {
        resultImage = [self cropImage:(594/2)*retinaScale];
        thumbDiameterPhone = 47/2;
        thumbDiameterPad = 101/2;
    } else if ([characterKey isEqualToString:@"C016"]) {
        resultImage = [self cropImage:(586/2)*retinaScale];
        thumbDiameterPhone = 47/2;
        thumbDiameterPad = 98/2;
    } else if ([characterKey isEqualToString:@"C011"]) {
        resultImage = [self cropImage:(604/2)*retinaScale];
        thumbDiameterPhone = 52/2;
        thumbDiameterPad = 108/2;
    } else if ([characterKey isEqualToString:@"C012"]) {
        resultImage = [self cropImage:(604/2)*retinaScale];
        thumbDiameterPhone = 53/2;
        thumbDiameterPad = 109/2;
    } else { //@"C010_custom";
        resultImage = [self cropImage:(604/2)*retinaScale];
        thumbDiameterPhone = 52/2;
        thumbDiameterPad = 115/2;
    }
    
//    switch (self.selectedIndexPath.row) {
//        case 3: //@"C005_custom";
//        {
//            resultImage = [self cropImage:(700/2)*retinaScale];
//            thumbDiameterPhone = 64/2;
//            thumbDiameterPad = 130/2;
//            break;
//        }
//        case 4://@"C009_custom";
//        {
//            resultImage = [self cropImage:(654/2)*retinaScale];
//            thumbDiameterPhone = 60/2;
//            thumbDiameterPad = 124/2;
//            break;
//        }
//        case 5://@"C006_custom";
//        {
//            resultImage = [self cropImage:(600/2)*retinaScale];
//            thumbDiameterPhone = 58/2;
//            thumbDiameterPad = 122/2;
//            break;
//        }
//        case 6://@"C007_custom";
//        {
//            resultImage = [self cropImage:(656/2)*retinaScale];
//            thumbDiameterPhone = 60/2;
//            thumbDiameterPad = 124/2;
//            break;
//        }
//        case 7://@"C008_custom";
//        {
//            resultImage = [self cropImage:(612/2)*retinaScale];
//            thumbDiameterPhone = 62/2;
//            thumbDiameterPad = 124/2;
//            break;
//        }
//        case 8://@"C001_custom";//@"C002_custom";
//        case 9:
//        {
//            resultImage = [self cropImage:(546/2)*retinaScale];
//            thumbDiameterPhone = 52/2;
//            thumbDiameterPad = 110/2;
//            break;
//        }
//        case 10://@"C003_custom";
//        {
//            resultImage = [self cropImage:(538/2)*retinaScale];
//            thumbDiameterPhone = 52/2;
//            thumbDiameterPad = 110/2;
//            break;
//        }
//        case 11://@"C004_custom";
//        {
//            resultImage = [self cropImage:(460/2)*retinaScale];
//            thumbDiameterPhone = 48/2;
//            thumbDiameterPad = 100/2; 
//            break;
//        }
//        case 12://@"C015_custom";
//        {
//            resultImage = [self cropImage:(594/2)*retinaScale];
//            thumbDiameterPhone = 50/2;
//            thumbDiameterPad = 106/2;
//            break;
//        }
//        case 13://@"C016_custom";
//        {
//            resultImage = [self cropImage:(586/2)*retinaScale];
//            thumbDiameterPhone = 50/2;
//            thumbDiameterPad = 102/2;
//            break;
//        }
//        default://@"C010_custom"; //@"C011_custom";//@"C012_custom";
//        {
//            resultImage = [self cropImage:(604/2)*retinaScale];
//            thumbDiameterPhone = 60/2;
//            thumbDiameterPad = 124/2;
//            break;
//        }
//            
//    }
    
    NSLog(@"resultImageSize : %@", NSStringFromCGSize(resultImage.size));
    
    if (resultImage == nil) {
        float bottomPadding = (IS_IPAD)?40:20;
        TSToastView *messageView = [[TSToastView alloc] initWithString:@"Invalid file format. \n Please, use another photo."];
        messageView.layer.zPosition = ZPOSITION_DEFAULT + 1;
        messageView.frame = CGRectMake(self.view.frame.size.width/2 - messageView.frame.size.width/2 , self.view.frame.size.height - messageView.frame.size.height - bottomPadding, messageView.frame.size.width, messageView.frame.size.height);
        [messageView showInViewForLandscape:self.view];
        self.backButton.enabled = true;
        return;
    }
    
//    NSData *testPngData = UIImagePNGRepresentation(resultImage);
//    UIImage *testImage = [UIImage imageWithData:testPngData];
//    
//    [[[ALAssetsLibrary alloc] init] writeImageToSavedPhotosAlbum:[testImage CGImage] orientation:(ALAssetOrientation)[resultImage imageOrientation] completionBlock:nil];
    
    [self checkAndCreateFolder:CHARACTER_ITEM_FOLDER_NAME];
//    NSString *fileName = [NSString stringWithFormat:@"c%i.png",[self getFileCountInDirectory:[NSString stringWithFormat:@"%@/%@/%@",DOCUMENTS_FOLDER,CHARACTER_ITEM_FOLDER_NAME,FOLDER_MAIN]]];
    
    auto userDefault = UserDefault::getInstance();
    auto  lastCharNum = userDefault->getIntegerForKey(CHAR_PHOTO_NUM_KEY, -1);
    unsigned long int charPhotoNum = lastCharNum + 1;
    userDefault->setIntegerForKey(CHAR_PHOTO_NUM_KEY, (int)charPhotoNum);
    
    NSString *fileName = [NSString stringWithFormat:@"c%i.png",(int)charPhotoNum];
    
    BOOL mainPhotoSaved = [self savePhotoData:resultImage folder:FOLDER_MAIN fileName:fileName];
    BOOL thumbPhotoSaved = [self savePhotoDataWithCropSize:thumbDiameterPhone pad:thumbDiameterPad folder:FOLDER_THUMB fileName:fileName];
    
    [self buttonControl:BUTTONS_TYPE_DEFAULT];
    
    [self resetEditView];
    
    [self.camViewController rotateCameraLayer];
    self.backButton.enabled = true;
    
    if(!(mainPhotoSaved && thumbPhotoSaved)) return;
    
    cocos2d::Scene *cameraScene = cocos2d::Director::getInstance()->getRunningScene();
    if (!cameraScene) {
        return;
    }
    CustomCamera *cameraLayer = (CustomCamera*)cameraScene->getChildByTag(TAG_CAMERA);
    if (!cameraLayer) {
        NSLog(@"not camera layer");
        return;
    }
    
//    NSLog(@"selected Index Path : %i", self.selectedIndexPath.row);

//    NSString *baseCharacterKey = [self getBaseCharacterKey];
    NSString *baseCharacterKey = [NSString stringWithFormat:@"%@_custom",self.thumbnailDataSource[self.selectedIndexPath.row][@"charkey"]];
    std::string keyStr = std::string([baseCharacterKey UTF8String]);
    std::string mainFile = std::string([fileName UTF8String]);
    std::string thumbFile = std::string([fileName UTF8String]);

    cameraLayer->customCharacterPhotoSaved(keyStr, mainFile, thumbFile);
    
    [self closeAction:nil];
}

//- (NSString*)getBaseCharacterKey
//{
//    NSString *baseCharacterKey = nil;
//    switch (self.selectedIndexPath.row) {
//        case 0:
//            baseCharacterKey = @"C010_custom";
//            break;
//        case 1:
//            baseCharacterKey = @"C011_custom";
//            break;
//        case 2:
//            baseCharacterKey = @"C012_custom";
//            break;
//        case 3:
//            baseCharacterKey = @"C005_custom";
//            break;
//        case 4:
//            baseCharacterKey = @"C009_custom";
//            break;
//        case 5:
//            baseCharacterKey = @"C006_custom";
//            break;
//        case 6:
//            baseCharacterKey = @"C007_custom";
//            break;
//        case 7:
//            baseCharacterKey = @"C008_custom";
//            break;
//        case 8:
//            baseCharacterKey = @"C001_custom";
//            break;
//        case 9:
//            baseCharacterKey = @"C002_custom";
//            break;
//        case 10:
//            baseCharacterKey = @"C003_custom";
//            break;
//        case 11:
//            baseCharacterKey = @"C004_custom";
//            break;
//        case 12:
//            baseCharacterKey = @"C015_custom";
//            break;
//        case 13:
//            baseCharacterKey = @"C016_custom";
//            break;
//        default:
//            break;
//    }
//    return baseCharacterKey;
//}

- (BOOL)savePhotoData:(UIImage*)originImage folder:(NSString*)folderName fileName:(NSString*)fileName
{
    NSString *photoPath = [NSString stringWithFormat:@"%@/%@/%@/%@",DOCUMENTS_FOLDER,CHARACTER_ITEM_FOLDER_NAME,folderName,fileName];
    BOOL saveResult = false;
    if (![[NSFileManager defaultManager] fileExistsAtPath:photoPath]) {
        NSData *photoData = UIImagePNGRepresentation(originImage);
        saveResult = [[NSFileManager defaultManager] createFileAtPath:photoPath contents:photoData attributes:nil];
    } else {
        NSLog(@"photo path is already exiset");
    }
    return saveResult;
}

- (BOOL)savePhotoDataWithCropSize:(float)phoneDiameter pad:(float)padDiameter folder:(NSString*)folderName fileName:(NSString*)fileName
{
    float retinaScale =([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale == 2.0))?2.0:1.0;
    CGSize size = (IS_IPAD)?CGSizeMake(padDiameter*retinaScale,padDiameter*retinaScale):CGSizeMake(phoneDiameter*retinaScale, phoneDiameter*retinaScale);
    UIImage *thumbImage = [self cropImage:size.width];
    BOOL saveResult = [self savePhotoData:thumbImage folder:folderName fileName:fileName];
    return saveResult;
}

//- (UIImage*)getThumbImage:(UIImage*)image size:(CGSize)size
//{
//    UIGraphicsBeginImageContext(CGSizeMake(size.width, size.height));
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextTranslateCTM(context, 0.0, size.height);
//    CGContextScaleCTM(context, 1.0, -1.0);
//    
//    CGContextDrawImage(context, CGRectMake(0.0, 0.0, size.width, size.height), [image CGImage]);
//    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    
//    return scaledImage;
//}

- (void)checkAndCreateFolder:(NSString*)folderName
{
    NSString *path = [DOCUMENTS_FOLDER stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",folderName]];
    if(![[NSFileManager defaultManager] fileExistsAtPath:path]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:NO attributes:nil error:nil];
    }
    NSString *mainPath = [DOCUMENTS_FOLDER stringByAppendingString:[NSString stringWithFormat:@"/%@/%@",folderName,FOLDER_MAIN]];
    if(![[NSFileManager defaultManager] fileExistsAtPath:mainPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:mainPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    NSString *thumbPath = [DOCUMENTS_FOLDER stringByAppendingString:[NSString stringWithFormat:@"/%@/%@",folderName,FOLDER_THUMB]];
    if(![[NSFileManager defaultManager] fileExistsAtPath:thumbPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:thumbPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
}

- (int)getFileCountInDirectory:(NSString*)directory
{
    NSArray *fileList = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:directory error:nil];
    return [fileList count];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CamViewControllerDelegte

- (void)setImageView:(UIImage *)image
{
    NSLog(@"image size : %f * %f", image.size.width, image.size.height);
    CGRect cropRect;
    if (IS_IPAD) {
        cropRect = CGRectMake((image.size.width-self.view.frame.size.width*(image.size.height/self.view.frame.size.height))/2, 0, self.view.frame.size.width*(image.size.height/self.view.frame.size.height), image.size.height);
//        if ([UIScreen mainScreen].scale == 2.0f) {
//            cropRect = CGRectMake((1920-1437.491289)/2, 0, 1437.491289, 1080);
//        } else {
//            cropRect = CGRectMake((1280-958.327526)/2, 0, 958.327526, 720);
//        }
    } else {
        cropRect = CGRectMake(0, (image.size.height-self.view.frame.size.height*(image.size.width/self.view.frame.size.width))/2, image.size.width, self.view.frame.size.height*(image.size.width/self.view.frame.size.width));
//        if(IS_4INCH){
//            cropRect = CGRectMake(0, 0, 1914.14634, 1080);
//        } else{
//            cropRect = CGRectMake(0, 0, 1527.8048, 1080);
//        }
    }
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    UIImage *cropImage = [UIImage imageWithCGImage:imageRef scale:1.0 orientation:image.imageOrientation];
    CGImageRelease(imageRef);
    
//    CGRect cropRect;
//    if (IS_IPAD) {
//        cropRect = CGRectMake(0, 0, image.size.width, image.size.height);
//    } else {
//        cropRect = CGRectMake(0, (image.size.height-self.view.frame.size.height*(image.size.width/self.view.frame.size.width))/2, image.size.width, self.view.frame.size.height*(image.size.width/self.view.frame.size.width));
//    }
//    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
//    UIImage *cropImage = [UIImage imageWithCGImage:imageRef scale:1.0 orientation:image.imageOrientation];
//    CGImageRelease(imageRef);
    
    self.editImageView.image = cropImage;
    [self reset:NO];
    [self setEditViewBackground];
    self.retakeButton.enabled = true;
    self.backButton.enabled = true;
    
    if (self.timer != nil) {
        [self.timer invalidate];
        self.timer = nil;
    }
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:DURATION_SHOW_GUIDE target:self selector:@selector(showGuide:) userInfo:@"showguide_camera_char_taskPhoto" repeats:NO];
}

- (void)showGuide:(NSTimer*)timer
{
#ifdef _GUIDE_ENABLE_
#ifndef _DEBUG_GUIDE_
    NSString *guideTypeKey = (NSString*)timer.userInfo;
    BOOL show = [[[NSUserDefaults standardUserDefaults] valueForKey:guideTypeKey] boolValue];
    if (show == false) {
        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:guideTypeKey];
#endif
        //        TSGuideforCreateCharacterView *guideView =
        //        [[TSGuideforCreateCharacterView alloc] initWithFrame:getRectValue(TSViewIndexGuide, TSGuideParentViewLandscape)
        //                                            transparentFrame:getRectValue(TSViewIndexGuide, TSGuideCreateCharecterAlpha)];
        TSGuideForCamera *guideView = [[TSGuideForCamera alloc] initWithFrame:getRectValue(TSViewIndexGuide, TSGuideParentViewLandscape)];
        guideView.layer.zPosition = ZPOSITION_DEFAULT;
        [guideView showInView:self.view];
#ifndef _DEBUG_GUIDE_
    }
#endif
#endif
}

- (void)setCameraButtonEnabled:(NSString *)enabled
{
    //    if ([enabled isEqualToString:@"YES"]) {
    //        [[self cameraButton] setEnabled:YES];
    //    } else {
    //        [[self cameraButton] setEnabled:NO];
    //    }
}

- (void)setEditViewBackground
{
    UIImage *background = [UIImage imageNamedEx:@"camera_frame_background_view.png" ignore:YES];
//    if (IS_IPAD) {
//        background = [UIImage imageNamed:@"iosnative/camera_frame_background_view_pad.png"];
//    } else {
//        background = [UIImage imageNamed:@"iosnative/camera_frame_background_view.png"];
//    }
    UIColor *pattern = [UIColor colorWithPatternImage:background];
    self.editView.backgroundColor = pattern;
    self.editView.contentMode = UIViewContentModeScaleAspectFit;
}

- (void)setStillButtonEnabled:(NSString *)enabled
{
    if ([enabled isEqualToString:@"YES"]) {
        [[self stillButton] setEnabled:YES];
    } else {
        [[self stillButton] setEnabled:NO];
    }
}

- (void)setViewFrame
{
    CGRect screenFrame = [[UIScreen mainScreen] bounds];
#ifdef __IPHONE_8_0
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        self.view.frame = CGRectMake(screenFrame.origin.x,screenFrame.origin.y, screenFrame.size.height, screenFrame.size.width);
    } else {
        self.view.frame = CGRectMake(screenFrame.origin.x,screenFrame.origin.y, screenFrame.size.width, screenFrame.size.height);
    }
#else
    self.view.frame = CGRectMake(screenFrame.origin.x,screenFrame.origin.y, screenFrame.size.height, screenFrame.size.width);
#endif
   
}

- (void)resetEditView
{
    if (self.editView != nil) {
        self.editImageView.image = nil;
        self.editView.backgroundColor = [UIColor clearColor];
        self.editView = nil;
        for (UIGestureRecognizer *recognizer in self.view.gestureRecognizers) {
            [self.view removeGestureRecognizer:recognizer];
        }
    }
}

#pragma mark - UIImagePickerControllerDelegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self photoEditView];
    [self setEditViewBackground];
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    self.editImageView.image = chosenImage;
    [self reset:NO];
    
    //    NSData *photoData = UIImagePNGRepresentation(chosenImage);
    //    UIImage *image = [[UIImage alloc] initWithData:photoData];
    //    UIImage *orientationImage = [UIImage imageWithCGImage:image.CGImage scale:1.0 orientation:chosenImage.imageOrientation];
    //    self.takenPhotoView.image = chosenImage;
    
    [self buttonControl:BUTTONS_TYPE_CANCEL];
    
    if (IS_IPAD && picker.sourceType == UIImagePickerControllerSourceTypeSavedPhotosAlbum) {
        [self.popover dismissPopoverAnimated:YES];
        [self releaseImagePicker];
    } else {
        UIViewController *sceneViewController;
        if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
            sceneViewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
        } else {
            sceneViewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
        }
        [sceneViewController dismissViewControllerAnimated:YES completion:^{
            self.backButton.enabled = true;
            [self releaseImagePicker];
        }];
    }
    if (self.timer != nil) {
        [self.timer invalidate];
        self.timer = nil;
    }
    self.timer = [NSTimer scheduledTimerWithTimeInterval:DURATION_SHOW_GUIDE target:self selector:@selector(showGuide:) userInfo:@"showguide_camera_char_fromAlbum" repeats:NO];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self setViewFrame];
    if (IS_IPAD && picker.sourceType == UIImagePickerControllerSourceTypeSavedPhotosAlbum) {
        [self.popover dismissPopoverAnimated:YES];
        [self releaseImagePicker];
    } else {
        UIViewController *sceneViewController;
        if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
            sceneViewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
        } else {
            sceneViewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
        }
        [sceneViewController dismissViewControllerAnimated:YES completion:^{
            self.backButton.enabled = true;
            [self releaseImagePicker];
        }];
    }
}

-(void)releaseImagePicker
{
    if (!SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        self.imagePicker = nil;
    }
}

- (void)photoEditView
{
    self.editView = [[UIView alloc] initWithFrame:CGRectMake(0,0,self.camViewController.view.frame.size.width, self.camViewController.view.frame.size.height)];
    self.editView.clipsToBounds = YES;
    self.editView.backgroundColor = [UIColor clearColor];
    [self.camViewController.view addSubview:self.editView];
    
    self.editImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,self.camViewController.view.frame.size.width, self.camViewController.view.frame.size.height)];
    self.editImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.editImageView.userInteractionEnabled = YES;
    [self.editView addSubview:self.editImageView];
    
    UIRotationGestureRecognizer *rotationGesture = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotate:)];
    rotationGesture.delegate = self;
    [self.editView addGestureRecognizer:rotationGesture];
    
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scale:)];
    pinchGesture.delegate =self;
    [self.editImageView addGestureRecognizer:pinchGesture];
    
    self.panGesuture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
    [self.editImageView addGestureRecognizer:self.panGesuture];
}

-(void)rotate:(UIRotationGestureRecognizer *)recognizer
{
//    if ([recognizer state] == UIGestureRecognizerStateBegan || [recognizer state] == UIGestureRecognizerStateChanged) {
//        self.editImageView.transform = CGAffineTransformRotate(self.editImageView.transform, recognizer.rotation);
//        [recognizer setRotation:0];
//    }
    
    if([self handleGestureState:recognizer.state]) {
        if(recognizer.state == UIGestureRecognizerStateBegan){
            self.rotationCenter = self.touchCenter;
        }
        CGFloat deltaX = self.rotationCenter.x-self.editImageView.bounds.size.width/2;
        CGFloat deltaY = self.rotationCenter.y-self.editImageView.bounds.size.height/2;
        
        CGAffineTransform transform =  CGAffineTransformTranslate(self.editImageView.transform,deltaX,deltaY);
        transform = CGAffineTransformRotate(transform, recognizer.rotation);
        transform = CGAffineTransformTranslate(transform, -deltaX, -deltaY);
        self.editImageView.transform = transform;
        [self checkBoundsWithTransform:transform];
        
        recognizer.rotation = 0;
    }
}

- (void)scale:(UIPinchGestureRecognizer *)recognizer
{
    if([self handleGestureState:recognizer.state]) {
        if(recognizer.state == UIGestureRecognizerStateBegan){
            self.scaleCenter = self.touchCenter;
        }
        CGFloat deltaX = self.scaleCenter.x-self.editImageView.bounds.size.width/2.0;
        CGFloat deltaY = self.scaleCenter.y-self.editImageView.bounds.size.height/2.0;
        
        CGAffineTransform transform =  CGAffineTransformTranslate(self.editImageView.transform, deltaX, deltaY);
        transform = CGAffineTransformScale(transform, recognizer.scale, recognizer.scale);
        transform = CGAffineTransformTranslate(transform, -deltaX, -deltaY);
        self.scale *= recognizer.scale;
        self.editImageView.transform = transform;
        
        recognizer.scale = 1;
        
        [self checkBoundsWithTransform:transform];
    }
}

- (void)pan:(UIPanGestureRecognizer *)recognizer
{
    if([self handleGestureState:recognizer.state]) {
        CGPoint translation = [recognizer translationInView:self.editImageView];
        CGAffineTransform transform = CGAffineTransformTranslate( self.editImageView.transform, translation.x, translation.y);
        self.editImageView.transform = transform;
        [self checkBoundsWithTransform:transform];
        
        [recognizer setTranslation:CGPointMake(0, 0) inView:self.editImageView];
    }
}

- (CGFloat)boundedScale:(CGFloat)scale;
{
    NSLog(@"self.scale : %f",self.scale);
    CGFloat boundedScale = scale;
    if(self.minimumScale > 0 && scale < self.minimumScale) {
        boundedScale = self.minimumScale;
    } else if(self.maximumScale > 0 && scale > self.maximumScale) {
        boundedScale = self.maximumScale;
    }
    return boundedScale;
}


- (BOOL)handleGestureState:(UIGestureRecognizerState)state
{
    BOOL handle = YES;
    switch (state) {
        case UIGestureRecognizerStateBegan:
            self.gestureCount++;
            break;
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateEnded: {
            self.gestureCount--;
            if(self.gestureCount == 0) {
                CGFloat scale = [self boundedScale:self.scale];
                if(scale != self.scale) {
                    CGFloat deltaX = self.scaleCenter.x-self.editImageView.bounds.size.width/2.0;
                    CGFloat deltaY = self.scaleCenter.y-self.editImageView.bounds.size.height/2.0;
                    
                    CGAffineTransform transform =  CGAffineTransformTranslate(self.editImageView.transform, deltaX, deltaY);
                    transform = CGAffineTransformScale(transform, scale/self.scale , scale/self.scale);
                    transform = CGAffineTransformTranslate(transform, -deltaX, -deltaY);
                    [self checkBoundsWithTransform:transform];
                    self.view.userInteractionEnabled = NO;
                    [UIView animateWithDuration:kAnimationIntervalTransform delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                        self.editImageView.transform = self.validTransform;
                    } completion:^(BOOL finished) {
                        self.view.userInteractionEnabled = YES;
                        self.scale = scale;
                    }];
                    
                } else {
                    self.view.userInteractionEnabled = NO;
                    [UIView animateWithDuration:kAnimationIntervalTransform delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                        self.editImageView.transform = self.validTransform;
                    } completion:^(BOOL finished) {
                        self.view.userInteractionEnabled = YES;
                    }];
                    
                    self.editImageView.transform = self.validTransform;
                }
            }
        } break;
        default:
            break;
    }
    return handle;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return ![gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]];
}

- (UIImage *)cropImage:(CGFloat)outPutWidthVal {
    int radius = 0;
    if (IS_IPAD) {
        radius = 856/2; //856 cropSize
    } else {
        radius = 330/2;  //330
    }
    
    float characterListHeight = 0;
    if (IS_IPAD) {
        characterListHeight = 396/2;
    } else {
        characterListHeight = 202/2;
    }

    CGImageRef resultRef = [self newTransformedImage:self.editImageView.transform
                                         sourceImage:self.editImageView.image.CGImage
                                          sourceSize:self.editImageView.image.size
                                   sourceOrientation:self.editImageView.image.imageOrientation
                                         outputWidth:outPutWidthVal
                                            cropRect:self.cropRect
                                       imageViewSize:self.editImageView.bounds.size];
    UIImage *result = nil;
    if (resultRef == nil) {
        return nil;
    }  else {
        UIImage *transform =  [UIImage imageWithCGImage:resultRef scale:1.0 orientation:UIImageOrientationUp];
        result = [self roundWithImage:transform];
        CGImageRelease(resultRef);
    }
    
    //    self.editImageView.image = transform;
    
    return result;
}

- (UIImage *)roundWithImage:(UIImage *)image {
    UIGraphicsBeginImageContextWithOptions(image.size, NO, 1.0);
    [[UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, image.size.width, image.size.height)
                                cornerRadius:image.size.width/2] addClip];
    [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height)];
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return result;
}

- (CGImageRef)newTransformedImage:(CGAffineTransform)transform
                      sourceImage:(CGImageRef)sourceImage
                       sourceSize:(CGSize)sourceSize
                sourceOrientation:(UIImageOrientation)sourceOrientation
                      outputWidth:(CGFloat)outputWidth
                         cropRect:(CGRect)cropRect
                    imageViewSize:(CGSize)imageViewSize
{
    CGImageRef source = sourceImage;

//    float retinaScale =([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale == 2.0))?2.0:1.0;
//    if (outputWidth > (sourceSize.width/2)*retinaScale || outputWidth > (sourceSize.height/2)*retinaScale) {
//        outputWidth = ((sourceSize.width/2)*retinaScale < (sourceSize.height/2)*retinaScale)?(sourceSize.width/2)*retinaScale:(sourceSize.height/2)*retinaScale;
//    }
    
    CGAffineTransform orientationTransform;
    [self transform:&orientationTransform andSize:&imageViewSize forOrientation:sourceOrientation];
    
    CGFloat aspect = cropRect.size.height/cropRect.size.width;
    CGSize outputSize = CGSizeMake(outputWidth, outputWidth*aspect);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(NULL,
                                                 (NSInteger)outputSize.width,
                                                 (NSInteger)outputSize.height,
                                                 CGImageGetBitsPerComponent(source),
                                                 0,
                                                 colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    if (!context) {
        NSLog(@"context sourceSize: %@,, cropRect : %@", NSStringFromCGSize(sourceSize), NSStringFromCGRect(cropRect));
        return nil;
    }
    
    CGContextSetFillColorWithColor(context,  [[UIColor clearColor] CGColor]);
    CGContextFillRect(context, CGRectMake(0, 0, outputSize.width, outputSize.height));
    
    CGAffineTransform uiCoords = CGAffineTransformMakeScale(outputSize.width/cropRect.size.width,
                                                            outputSize.height/cropRect.size.height);
    uiCoords = CGAffineTransformTranslate(uiCoords, cropRect.size.width/2.0, cropRect.size.height/2.0);
    uiCoords = CGAffineTransformScale(uiCoords, 1.0, -1.0);
    CGContextConcatCTM(context, uiCoords);
    
    CGContextConcatCTM(context, transform);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextConcatCTM(context, orientationTransform);
    
    float underPhotoHeight = (IS_IPAD)?(396+96)/2:(202+34)/2;
    
    CGContextDrawImage(context, CGRectMake(-imageViewSize.width/2.0,
                                           -imageViewSize.height/2.0,
                                           imageViewSize.width,
                                           imageViewSize.height)
                       ,source);
    
    NSLog(@"imageViewRect = %@", NSStringFromCGRect(CGRectMake(-imageViewSize.width/2.0,
                                                               -((underPhotoHeight+(cropRect.size.height/2.0)*(imageViewSize.height/self.editView.frame.size.height))),//-(cropRect.origin.y*(imageViewSize.height/cropRect.size.height)/2.0),
                                                               imageViewSize.width,
                                                               imageViewSize.height)));
    CGImageRef resultRef = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    return resultRef;
}

- (void)checkBoundsWithTransform:(CGAffineTransform)transform
{
    if(!self.checkBounds) {
        self.validTransform = transform;
        return;
    }
    
    CGRect r1 = [self boundingBoxForRect:self.cropRect rotatedByRadians:[self imageRotation]];
    Rectangle r2 = [self applyTransform:transform toRect:self.initialImageFrame];
    
    CGAffineTransform t = CGAffineTransformMakeTranslation(CGRectGetMidX(self.cropRect), CGRectGetMidY(self.cropRect));
    t = CGAffineTransformRotate(t, -[self imageRotation]);
    t = CGAffineTransformTranslate(t, -CGRectGetMidX(self.cropRect), -CGRectGetMidY(self.cropRect));
    
    Rectangle r3 = [self applyTransform:t toRectangle:r2];
    
    if(CGRectContainsRect([self CGRectFromRectangle:r3],r1)) {
        self.validTransform = transform;
    }
}

- (Rectangle)RectangleFromCGRect:(CGRect)rect
{
    return (Rectangle) {
        .tl = (CGPoint){rect.origin.x, rect.origin.y},
        .tr = (CGPoint){CGRectGetMaxX(rect), rect.origin.y},
        .br = (CGPoint){CGRectGetMaxX(rect), CGRectGetMaxY(rect)},
        .bl = (CGPoint){rect.origin.x, CGRectGetMaxY(rect)}
    };
}

-(CGRect)CGRectFromRectangle:(Rectangle)rect
{
    return (CGRect) {
        .origin = rect.tl,
        .size = (CGSize){.width = rect.tr.x - rect.tl.x, .height = rect.bl.y - rect.tl.y}
    };
}

- (Rectangle)applyTransform:(CGAffineTransform)transform toRect:(CGRect)rect
{
    CGAffineTransform t = CGAffineTransformMakeTranslation(CGRectGetMidX(rect), CGRectGetMidY(rect));
    t = CGAffineTransformConcat(self.editImageView.transform, t);
    t = CGAffineTransformTranslate(t,-CGRectGetMidX(rect), -CGRectGetMidY(rect));
    
    Rectangle r = [self RectangleFromCGRect:rect];
    return (Rectangle) {
        .tl = CGPointApplyAffineTransform(r.tl, t),
        .tr = CGPointApplyAffineTransform(r.tr, t),
        .br = CGPointApplyAffineTransform(r.br, t),
        .bl = CGPointApplyAffineTransform(r.bl, t)
    };
}

- (Rectangle)applyTransform:(CGAffineTransform)t toRectangle:(Rectangle)r
{
    return (Rectangle) {
        .tl = CGPointApplyAffineTransform(r.tl, t),
        .tr = CGPointApplyAffineTransform(r.tr, t),
        .br = CGPointApplyAffineTransform(r.br, t),
        .bl = CGPointApplyAffineTransform(r.bl, t)
    };
}

-(void)reset:(BOOL)animated
{
    CGFloat w = 0.0f;
    CGFloat h = 0.0f;
    CGFloat sourceAspect = self.editImageView.image.size.height/self.editImageView.image.size.width;
    CGFloat cropAspect = self.cropRect.size.height/self.cropRect.size.width;
    
    if(sourceAspect > cropAspect) { //portrait
        h = CGRectGetHeight(self.cropRect);
        w = h / sourceAspect;
    } else {
        w = CGRectGetWidth(self.cropRect);
        h = w * sourceAspect;
    }
    self.scale = 1;
    self.checkBounds = YES;
    
    self.initialImageFrame = CGRectMake(CGRectGetMidX(self.cropRect) - w/2, CGRectGetMidY(self.cropRect) - h/2,w,h);
    self.validTransform = CGAffineTransformMakeScale(self.scale, self.scale);
    
    CGFloat editViewAspect = self.editView.frame.size.height/self.editView.frame.size.width;
    CGFloat editw = 0.0f;
    CGFloat edith = 0.0f;
    
    if(sourceAspect > editViewAspect) { //portrait
        edith = CGRectGetHeight(self.editView.frame);
        editw = edith / sourceAspect;
    } else {
        editw = CGRectGetWidth(self.editView.frame);
        edith = editw * sourceAspect;
    }

    self.scale = editw/w;
    self.validTransform = CGAffineTransformMakeScale(self.scale, self.scale);

    void (^doReset)(void) = ^{
        self.editImageView.transform = CGAffineTransformIdentity;
        self.editImageView.frame = self.initialImageFrame;
        self.editImageView.transform = self.validTransform;
        
        CGAffineTransform transform = CGAffineTransformTranslate( self.editImageView.transform, 0, self.editView.frame.origin.y - (self.editImageView.frame.origin.y)/self.scale);
        self.editImageView.transform = transform;
        
        [self.panGesuture setTranslation:CGPointMake(0, 0) inView:self.editImageView];

        if(self.checkBounds) {
            self.minimumScale = 1;
            self.maximumScale = self.scale+2;
        }
    };
    doReset();
}

- (CGFloat) imageRotation
{
    CGAffineTransform t = self.editImageView.transform;
    return atan2f(t.b, t.a);
}

- (CGRect)boundingBoxForRect:(CGRect)rect rotatedByRadians:(CGFloat)angle
{
    CGAffineTransform t = CGAffineTransformMakeTranslation(CGRectGetMidX(rect), CGRectGetMidY(rect));
    t = CGAffineTransformRotate(t,angle);
    t = CGAffineTransformTranslate(t,-CGRectGetMidX(rect), -CGRectGetMidY(rect));
    return CGRectApplyAffineTransform(rect, t);
}

- (void)transform:(CGAffineTransform*)transform andSize:(CGSize *)size forOrientation:(UIImageOrientation)orientation
{
    *transform = CGAffineTransformIdentity;
    BOOL transpose = NO;
    
    switch(orientation)
    {
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:{
        } break;
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored: {
            *transform = CGAffineTransformMakeRotation(M_PI);
        } break;
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationLeft: {
            *transform = CGAffineTransformMakeRotation(M_PI_2);
            transpose = YES;
        } break;
        case UIImageOrientationRightMirrored:
        case UIImageOrientationRight: {
            *transform = CGAffineTransformMakeRotation(-M_PI_2);
            transpose = YES;
        } break;
        default:
            break;
    }
    
    if(orientation == UIImageOrientationUpMirrored || orientation == UIImageOrientationDownMirrored ||
       orientation == UIImageOrientationLeftMirrored || orientation == UIImageOrientationRightMirrored) {
        *transform = CGAffineTransformScale(*transform, -1, 1);
    }
    
    if(transpose) {
        *size = CGSizeMake(size->height, size->width);
    }
}

- (void)handleTouches:(NSSet*)touches
{
    self.touchCenter = CGPointZero;
    if(touches.count < 2) return;
    
    [touches enumerateObjectsUsingBlock:^(id obj, BOOL *stop) {
        UITouch *touch = (UITouch*)obj;
        CGPoint touchLocation = [touch locationInView:self.editImageView];
        self.touchCenter = CGPointMake(self.touchCenter.x + touchLocation.x, self.touchCenter.y +touchLocation.y);
    }];
    self.touchCenter = CGPointMake(self.touchCenter.x/touches.count, self.touchCenter.y/touches.count);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self handleTouches:[event allTouches]];
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [self handleTouches:[event allTouches]];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self handleTouches:[event allTouches]];
}

-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [self handleTouches:[event allTouches]];
}


#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return self.thumbnailDataSource.count;
}


- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}


- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath; {
    return YES;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CharacterImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifer forIndexPath:indexPath];
    if ([indexPath isEqual:self.selectedIndexPath]) {
        [cell setSelectedCell:YES];
//        cell.thumbView.image = [UIImage imageNamedEx:[self getImageString:indexPath.row+1 imageType:TYPE_SELECT] ignore:YES];
        cell.thumbView.image = [UIImage imageNamedEx:self.thumbnailDataSource[indexPath.row][@"select"] ignore:YES];
    }else {
        [cell setSelectedCell:NO];
//        cell.thumbView.image = [UIImage imageNamedEx:[self getImageString:indexPath.row+1 imageType:TYPE_NORMAL] ignore:YES];
        cell.thumbView.image = [UIImage imageNamedEx:self.thumbnailDataSource[indexPath.row][@"normal"] ignore:YES];
    }
    
    return cell;
}

//- (NSString*)getImageString:(NSInteger)num imageType:(imageType)type
//{
//    NSString *result = NULL;
//    if (type == TYPE_MAIN) {
//        result = [NSString stringWithFormat:@"camera_character_frame_%02d.png",num];
//    } else if (type == TYPE_NORMAL) {
//        result = [NSString stringWithFormat:@"home_character_custom_%02d_normal.png",num];
//    } else {
//        result = [NSString stringWithFormat:@"home_character_custom_%02d_select.png",num];
//    }
//    return result;
//}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
    
    [collectionView deselectItemAtIndexPath:indexPath animated:NO];
    
    NSString *characterKey = self.thumbnailDataSource[indexPath.row][@"charkey"];
    float radius = [self setFaceCropRect:characterKey];
    [self setDimLayer:radius];
    
    self.currentPage = indexPath.item;
    
    CharacterImageCell *previousSelectedCell = (CharacterImageCell*)[collectionView cellForItemAtIndexPath:self.selectedIndexPath];
    [previousSelectedCell setSelectedCell:NO];
//    previousSelectedCell.thumbView.image = [UIImage imageNamedEx:[self getImageString:self.selectedIndexPath.row+1 imageType:TYPE_NORMAL] ignore:YES];
    previousSelectedCell.thumbView.image = [UIImage imageNamedEx:self.thumbnailDataSource[indexPath.row][@"normal"] ignore:YES];
    
    CharacterImageCell *cell = (CharacterImageCell*)[collectionView cellForItemAtIndexPath:indexPath];
    [cell setSelectedCell:YES];
//    cell.thumbView.image = [UIImage imageNamedEx:[self getImageString:indexPath.row+1 imageType:TYPE_SELECT] ignore:YES];
    cell.thumbView.image = [UIImage imageNamedEx:self.thumbnailDataSource[indexPath.row][@"select"] ignore:YES];
    self.selectedIndexPath = indexPath;
    
//    self.characterFaceView.image = [UIImage imageNamedEx:[self getImageString:indexPath.row+1 imageType:TYPE_MAIN] ignore:YES];
    self.characterFaceView.image = [UIImage imageNamedEx:self.thumbnailDataSource[indexPath.row][@"face"] ignore:YES];
    self.testLabel.text = [NSString stringWithFormat:@"image num: %i",indexPath.row+1];
    
    [collectionView reloadData];
}

- (float)setFaceCropRect:(NSString*)characterKey
{
    float radius = 0;
    float characterFaceHeight = (IS_IPAD)?1140/2:456/2;
    if ([characterKey isEqualToString:@"C005"]) {
        radius = (IS_IPAD)?662/2:288/2;
    } else if ([characterKey isEqualToString:@"C009"]) {
        radius = (IS_IPAD)?621/2:267/2;
    } else if ([characterKey isEqualToString:@"C006"]) {
        radius = (IS_IPAD)?614/2:254/2;
    } else if ([characterKey isEqualToString:@"C007"]) {
        radius = (IS_IPAD)?656/2:281/2;
    } else if ([characterKey isEqualToString:@"C008"]) {
        radius = (IS_IPAD)?538/2:233/2;
    } else if ([characterKey isEqualToString:@"C001"]) {
        radius = (IS_IPAD)?579/2:250/2;
    } else if ([characterKey isEqualToString:@"C002"]) {
        radius = (IS_IPAD)?568/2:238/2;
    } else if ([characterKey isEqualToString:@"C003"]) {
        radius = (IS_IPAD)?572/2:241/2;
    } else if ([characterKey isEqualToString:@"C004"]) {
        radius = (IS_IPAD)?505/2:220/2;
    } else if ([characterKey isEqualToString:@"C015"]) {
        radius = (IS_IPAD)?645/2:275/2;
    } else if ([characterKey isEqualToString:@"C016"]) {
        radius = (IS_IPAD)?596/2:260/2;
    } else if ([characterKey isEqualToString:@"C011"]) {
        radius = (IS_IPAD)?584/2:251/2;
    } else if ([characterKey isEqualToString:@"C012"]) {
        radius = (IS_IPAD)?584/2:249/2;
    } else { //@"C010_custom";
        radius = (IS_IPAD)?541/2:236/2;
    }
    self.cropRect = CGRectMake(self.view.bounds.size.width/2 - radius/2, (characterFaceHeight - radius)/2, radius, radius);
    return radius;
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((IS_IPAD)?408/2:188/2, collectionView.frame.size.height);;
}


- (UIEdgeInsets)collectionView:(UICollectionView *)cview layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

@end
#endif
