//
//  MovieViewController.m
//  bobby
//
//  Created by oasis on 2014. 4. 16..
//
//

#ifndef MACOS

#import "MovieViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "UIImage+PathExtention.h"
#include "Define.h"

#import <AVFoundation/AVFoundation.h>

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_4INCH ([UIScreen mainScreen].bounds.size.height == 568)

#define TAG_PROGRESS_LEFT 11
#define TAG_PROGRESS_RIGHT 22
#define TAG_PROGRESS_CENTER 33

#define INTERVAL_PROGRESS 0.1f
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface MovieViewController ()
@property(nonatomic, strong) MPMoviePlayerViewController *movieViewController;
@property(nonatomic, strong) AVPlayerLayer *avPlayerLayer;
@property(nonatomic, strong) AVAudioPlayer *avAudioPlayer;
@property(nonatomic, strong) NSArray *movieNames;
@property(nonatomic, strong) UIView *uiView;
@property(nonatomic, strong) UIButton *playButton;
@property(nonatomic, strong) UIButton *pauseButton;
@property(nonatomic, strong) UILabel *titleLabel;
@property(nonatomic, strong) UILabel *titleLabel2;

@property (nonatomic) int currentPlayedNum;
@property (nonatomic) BOOL paused;

@property (nonatomic) play_type playType;
//@property (nonatomic) NSTimer *timer;
@property (nonatomic) NSMutableArray *audioUrls;

@property (nonatomic) float totalDuration;
@property (nonatomic, strong) UIView *playProgressBarHolder;
@property (nonatomic, strong) UIView *progressBar;
@property (nonatomic) float playedTime;
@property (nonatomic) CGRect progressBgRect;
@property (nonatomic, strong) NSTimer *progressTimer;
@property (nonatomic) float playedMovieDuration;

@end

@implementation MovieViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithMovieNames:(NSArray*)movieNames andType:(play_type)playType
{
    self = [super init];
    if (self) {
//        NSString *movieNameStr = [NSString stringWithUTF8String:movieName.c_str()];
        self.movieNames = movieNames;
        self.currentPlayedNum = 0;
        self.playType = playType;
        
        CGRect screenFrame = [[UIScreen mainScreen] bounds];
#ifdef __IPHONE_8_0
        if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
            self.view.frame = CGRectMake(screenFrame.origin.x,screenFrame.origin.y, screenFrame.size.height, screenFrame.size.width);
        } else {
            self.view.frame = CGRectMake(screenFrame.origin.x,screenFrame.origin.y, screenFrame.size.width, screenFrame.size.height);
        }
#else
        self.view.frame = CGRectMake(screenFrame.origin.x,screenFrame.origin.y, screenFrame.size.height, screenFrame.size.width);
#endif
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
#ifdef __IPHONE_8_0
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
    } else {
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.height, self.view.frame.size.width);
    }
#endif
    
    self.playedMovieDuration = 0;
    
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0]; //@"ja",@"ko",@"en"
    NSString *audioFilePath = nil;
    if ([language isEqualToString:@"ja"]) {
        audioFilePath = @"common/preload_sounds/ja";
    } else if ([language isEqualToString:@"ko"]) {
        audioFilePath = @"common/preload_sounds/kor";
    } else {
        audioFilePath = @"common/preload_sounds/en";
    }
    
    self.audioUrls = [NSMutableArray array];
    for (int i = 0; i < self.movieNames.count; i++) {
        NSString *movieName = [self.movieNames objectAtIndex:i];
        NSString *subMovieName = [movieName substringToIndex:movieName.length-4];
        NSString *audioPath;
        if (i%2 == 0) {
            audioPath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@/%@",audioFilePath,subMovieName] ofType:@"wav"];
        } else {
            audioPath = [[NSBundle mainBundle] pathForResource:@"common/preload_sounds/scene_transition" ofType:@"wav"];
        }
        
        if (audioPath) {
            NSError *err;
            if ([[NSURL fileURLWithPath:audioPath] checkResourceIsReachableAndReturnError:&err]) {
                [self.audioUrls addObject:[NSURL fileURLWithPath:audioPath]];
            }
        }
    }
    
    NSLog(@"self.audioUrls : %lu", (unsigned long)self.audioUrls.count);
    
    // Do any additional setup after loading the view.
    NSString *movieName = [self.movieNames objectAtIndex:0];
    NSString *subMovieName = [movieName substringToIndex:movieName.length-4];
    NSString *moviePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"common/%@",subMovieName] ofType:@"mp4"];
    
    AVAsset *avAsset = [AVAsset assetWithURL:[NSURL fileURLWithPath:moviePath]];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc] initWithAsset:avAsset];
    AVPlayer *avPlayer = [[AVPlayer alloc] initWithPlayerItem:avPlayerItem];
    self.avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:avPlayer];
    [self.avPlayerLayer setFrame:CGRectMake(0,0,self.view.frame.size.height, self.view.frame.size.width)];
    [self.view.layer addSublayer:self.avPlayerLayer];
    self.avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [avPlayer seekToTime:kCMTimeZero];
    [avPlayer play];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playDidFinishChange:) name:AVPlayerItemDidPlayToEndTimeNotification object:self.avPlayerLayer.player.currentItem];
    
    if (self.audioUrls.count > 0) {
        self.avAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[self.audioUrls objectAtIndex:0] error:nil];
        [self.avAudioPlayer play];
    }

    if (self.playType == PLAY_TYPE_ONE) {
        self.totalDuration = (float)CMTimeGetSeconds(avAsset.duration);
    } else {
        self.totalDuration = OPENING_DURATION + [self getMoviesTotalDuration] + ENDING_DURATION;
        
        cocos2d::Scene *jeVideoScene = cocos2d::Director::getInstance()->getRunningScene();
        if (!jeVideoScene) {
            return;
        }
        JeVideo *videoLayer = (JeVideo*)jeVideoScene->getChildByTag(TAG_VIDEO);
        if (!videoLayer) {
            return;
        }
        
        videoLayer->totalDuration = self.totalDuration;
//        if ([movieName isEqualToString:@"scene_01.mp4"]) {
//            self.totalDuration = OPENING_DURATION + (DURATION_SCENE01 + DURATION_SCENE02 + DURATION_SCENE03 + DURATION_SCENE04 + DURATION_SCENE05 + (DURATION_TRANSITION * 4) ) + ENDING_DURATION + [UIScreen mainScreen].scale;
//        } else {
//            self.totalDuration = OPENING_DURATION + (DURATION_SCENE06 + DURATION_SCENE07 + DURATION_SCENE08 + DURATION_SCENE09 + DURATION_SCENE10 + (DURATION_TRANSITION * 4) ) + ENDING_DURATION + [UIScreen mainScreen].scale;
//        }
    }
    
    NSLog(@"totlaDuration : %f", self.totalDuration);
    
    _paused = false;
    
    _uiView = [[UIView alloc] initWithFrame:self.view.frame];

    UIImage *normalImage = [UIImage imageNamedEx:@"btn_map_back_normal.png" ignore:YES];
    UIImage *pressImage = [UIImage imageNamedEx:@"btn_map_back_press.png" ignore:YES];
    
    UIImage *normalPauseImage = [UIImage imageNamedEx:@"btn_set_record_pause_normal.png" ignore:YES];
    UIImage *pressPauseImage = [UIImage imageNamedEx:@"btn_set_record_pause_pressed.png" ignore:YES];
    UIImage *normalPlayImage = [UIImage imageNamedEx:@"btn_set_record_play_normal.png" ignore:YES];
    UIImage *pressPlayImage = [UIImage imageNamedEx:@"btn_set_record_play_pressed.png" ignore:YES];
    
    CGRect buttonRect;
    CGRect playRect;
    CGRect labelRect;
    float fontSize = 0;
    
    if (IS_IPAD) {
        buttonRect = CGRectMake(self.view.frame.size.height-(self.view.frame.size.height-(50/2)), 40/2, normalImage.size.width, normalImage.size.height);
        playRect = CGRectMake(self.view.frame.size.height/2 - normalPlayImage.size.width/2, self.view.frame.size.width/2 - normalPlayImage.size.height/2,
                              normalPlayImage.size.width, normalPlayImage.size.height);
        labelRect = CGRectMake((480/2-256/2)/2, 308/2, 256/2, 78/2);

        if ([language isEqualToString:@"ja"]) {
            fontSize = 50/2;//일본어 final 짤리는 문제로 수정
        } else if ([language isEqualToString:@"ko"]) {
            fontSize = 60/2;
        } else {
            fontSize = 74/2;
        }
        
    } else {
        buttonRect = CGRectMake(self.view.frame.size.height-(self.view.frame.size.height-(20/2)-(90/2))-normalImage.size.width, 18/2, normalImage.size.width, normalImage.size.height);
        playRect = CGRectMake(self.view.frame.size.height/2 - normalPlayImage.size.width/2, self.view.frame.size.width/2 - normalPlayImage.size.height/2,
                              normalPlayImage.size.width, normalPlayImage.size.height);
        labelRect = CGRectMake((216/2-104/2)/2, 130/2, 104/2, 36/2);
        
        if ([language isEqualToString:@"ja"]) {
            fontSize = 20/2;//일본어 final 짤리는 문제로 수정
        } else if ([language isEqualToString:@"ko"]) {
            fontSize = 26/2;
        } else {
            fontSize = 32/2;
        }
    }
    
    UIButton *closeButton = [[UIButton alloc] initWithFrame:buttonRect];
    [closeButton setImage:normalImage forState:UIControlStateNormal];
    [closeButton setImage:pressImage forState:UIControlStateHighlighted];
    [closeButton addTarget:self action:@selector(closeAction) forControlEvents:UIControlEventTouchUpInside];
    
    _playButton = [[UIButton alloc] initWithFrame:playRect];
    [_playButton setImage:normalPlayImage forState:UIControlStateNormal];
    [_playButton setImage:pressPlayImage forState:UIControlStateHighlighted];
    [_playButton addTarget:self action:@selector(playAction) forControlEvents:UIControlEventTouchUpInside];
    [_playButton setHidden:YES];
    
    _pauseButton = [[UIButton alloc] initWithFrame:playRect];
    [_pauseButton setImage:normalPauseImage forState:UIControlStateNormal];
    [_pauseButton setImage:pressPauseImage forState:UIControlStateHighlighted];
    [_pauseButton addTarget:self action:@selector(pauseAction) forControlEvents:UIControlEventTouchUpInside];

    _titleLabel = [[UILabel alloc] initWithFrame:labelRect];
    [_titleLabel setTextAlignment:NSTextAlignmentCenter];
    [_titleLabel setFont:[UIFont boldSystemFontOfSize:fontSize]];
    [_titleLabel setTextColor:[UIColor whiteColor]];
    [_playButton addSubview:_titleLabel];
    
    _titleLabel2 = [[UILabel alloc] initWithFrame:labelRect];
    [_titleLabel2 setTextAlignment:NSTextAlignmentCenter];
    [_titleLabel2 setFont:[UIFont boldSystemFontOfSize:fontSize]];
    [_titleLabel2 setTextColor:[UIColor whiteColor]];
    [_pauseButton addSubview:_titleLabel2];
    
    int chatperIndex = [self getIndexFromFileName:subMovieName];
    if (chatperIndex > 0){
        NSString *titleString = [self getTitleString:chatperIndex];
        [_titleLabel setText:titleString];
        [_titleLabel2 setText:titleString];
    }

    [_uiView addSubview:closeButton];
    [_uiView addSubview:_playButton];
    [_uiView addSubview:_pauseButton];

    [_uiView setHidden:YES];
    [self.view addSubview:_uiView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(screenTouched)];
    [self.view addGestureRecognizer:tapGesture];
    
    [self createProgressBar];
    
    if (self.playType == PLAY_TYPE_ALL) {
        self.playedTime = OPENING_DURATION;
    } else {
        self.playedTime = 0;
    }
    
    self.progressTimer = [NSTimer scheduledTimerWithTimeInterval:INTERVAL_PROGRESS target:self selector:@selector(updateProgressBar) userInfo:nil repeats:YES];
}

- (float)getMoviesTotalDuration
{
    float totalAssetsDuration = 0;
    for (NSString *movieName in self.movieNames) {
        NSLog(@"movieName : %@", movieName);
        NSString *subMovieName = [movieName substringToIndex:movieName.length-4];
        NSString *moviePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"common/%@",subMovieName] ofType:@"mp4"];
        
        AVAsset *avAsset = [AVAsset assetWithURL:[NSURL fileURLWithPath:moviePath]];
        totalAssetsDuration += (float)CMTimeGetSeconds(avAsset.duration);
    }
    return totalAssetsDuration;
}

- (void)createProgressBar
{
    self.playProgressBarHolder = [[UIView alloc] init];
    
    if (IS_IPAD) {
        self.progressBgRect = CGRectMake(50/2, self.view.frame.size.width - (26+56)/2, self.view.frame.size.height - (2048-1948)/2, 56/2);
    } else {
        if (IS_4INCH) {
            self.progressBgRect = CGRectMake(20/2, self.view.frame.size.width - (10+24)/2, self.view.frame.size.height - (1136-1096)/2, 24/2);
//            self.progressBgRect = CGRectMake(20/2, self.view.frame.size.width - (10+24)/2, 1096/2, 24/2);
        } else {
            self.progressBgRect = CGRectMake(20/2, self.view.frame.size.width - (10+24)/2, self.view.frame.size.height - (960-902)/2, 24/2);
//            self.progressBgRect = CGRectMake(20/2, self.view.frame.size.width - (10+24)/2, 902/2, 24/2);
        }
    }
    
    //bg
    UIView  *progressBarBg = [[UIView alloc] initWithFrame:self.progressBgRect];
    
    UIImage *bgLeftImage = [UIImage imageNamedEx:@"recording_play_progress_bg_left.png" ignore:YES];
    UIImage *bgRightImage = [UIImage imageNamedEx:@"recording_play_progress_bg_right.png" ignore:YES];
    UIImage *bgCenterImage = [[UIImage imageNamedEx:@"recording_play_progress_bg_center.png" ignore:YES] stretchableImageWithLeftCapWidth:1 topCapHeight:0];
    
    float bgCenterBgWidth = self.progressBgRect.size.width - bgLeftImage.size.width - bgRightImage.size.width;
    
    UIImageView *bgLeft = [[UIImageView alloc] initWithImage:bgLeftImage];
    bgLeft.frame = CGRectMake(0, 0, bgLeftImage.size.width, bgLeftImage.size.height);
    UIImageView *bgRight = [[UIImageView alloc] initWithImage:bgRightImage];
    bgRight.frame = CGRectMake(bgLeftImage.size.width+bgCenterBgWidth, 0, bgRightImage.size.width, bgRightImage.size.height);
    UIImageView *bgCenter = [[UIImageView alloc] initWithImage:bgCenterImage];
    bgCenter.frame = CGRectMake(bgLeftImage.size.width, 0, bgCenterBgWidth, bgCenterImage.size.height);
    
    [progressBarBg addSubview:bgLeft];
    [progressBarBg addSubview:bgRight];
    [progressBarBg addSubview:bgCenter];
    
    [self.playProgressBarHolder addSubview:progressBarBg];
    [_uiView addSubview:self.playProgressBarHolder];
    
    //move
    self.progressBar = [[UIView alloc] initWithFrame:self.progressBgRect];
    
    UIImage *progressLeftImage = [UIImage imageNamedEx:@"recording_play_progress_left.png" ignore:YES];
    UIImage *progressRightImage = [UIImage imageNamedEx:@"recording_play_progress_right.png" ignore:YES];
    
    UIImageView *progressLeft = [[UIImageView alloc] initWithImage:progressLeftImage];
    progressLeft.frame = CGRectMake(0, 0, progressLeftImage.size.width, progressLeftImage.size.height);
    progressLeft.tag = TAG_PROGRESS_LEFT;
    UIImageView *progressRight = [[UIImageView alloc] initWithImage:progressRightImage];
    progressRight.frame = CGRectMake(progressLeftImage.size.width, 0, progressRightImage.size.width, progressRightImage.size.height);
    progressRight.tag = TAG_PROGRESS_RIGHT;
    
    [self.progressBar addSubview:progressLeft];
    [self.progressBar addSubview:progressRight];
    
    [self.playProgressBarHolder addSubview:self.progressBar];
}

- (void)updateProgressBar
{
    if (!_paused && self.avPlayerLayer.readyForDisplay) {
        if (self.playType == PLAY_TYPE_ONE) {
            self.playedTime = (float)CMTimeGetSeconds(self.avPlayerLayer.player.currentItem.currentTime);
        } else {
            self.playedTime = OPENING_DURATION + self.playedMovieDuration + (float)CMTimeGetSeconds(self.avPlayerLayer.player.currentItem.currentTime);
//            self.playedTime += INTERVAL_PROGRESS;
        }
        
        float currentProgress = self.playedTime / self.totalDuration;
        [self setProgress:currentProgress];
    }
//    NSLog(@"playedTime : %f, progress : %f", self.playedTime, self.playedTime/self.totalDuration);
}

- (void)setProgress:(float)value
{
    if (value > 1.0f) return;
    
    self.progressBar.hidden = false;
    UIImageView *progressCenter = (UIImageView*)[self getSubviewFromView:self.progressBar withTag:TAG_PROGRESS_CENTER];
    
    UIImageView *progressLeft = (UIImageView*)[self getSubviewFromView:self.progressBar withTag:TAG_PROGRESS_LEFT];
    UIImageView *progressRight = (UIImageView*)[self getSubviewFromView:self.progressBar withTag:TAG_PROGRESS_RIGHT];
    
    float progressWidth = self.progressBgRect.size.width - progressLeft.frame.size.width - progressRight.frame.size.width;
    float progressCenterWidth = ceil(progressWidth * value);
    
    if (progressCenter) {
        [progressCenter removeFromSuperview];
    }
    
    if (progressCenterWidth > 0) {
        UIImage *newCenterImage = [[UIImage imageNamedEx:@"recording_play_progress_center.png" ignore:YES] stretchableImageWithLeftCapWidth:1 topCapHeight:0];
        
        UIImageView *newCenter = [[UIImageView alloc] initWithImage:newCenterImage];
        newCenter.frame = CGRectMake(progressLeft.frame.size.width, 0, progressCenterWidth, newCenterImage.size.height);
        newCenter.tag = TAG_PROGRESS_CENTER;
        [self.progressBar addSubview:newCenter];
    }
    progressRight.frame = CGRectMake(progressLeft.frame.size.width + progressCenterWidth, 0, progressRight.frame.size.width, progressRight.frame.size.height);
}

- (UIView*)getSubviewFromView:(UIView*)view withTag:(NSInteger)tag {
    for (UIView *subview in view.subviews) {
        if(subview.tag==tag)
            return subview;
    }
    return nil;
}

- (void)screenTouched
{
    bool hidden = [_uiView isHidden];
    if(hidden) {
        [_uiView setHidden:NO];
        [self triggerHideUI];
    } else {
        [_uiView setHidden:YES];
        [self cancelHideUI];
    }
}

- (void)playAction
{
    [self.avPlayerLayer.player play];
    [self.avAudioPlayer play];

    [_playButton setHidden:YES];
    [_pauseButton setHidden:NO];
    _paused = NO;
    [_uiView setHidden:YES];
}

- (void)pauseAction
{
    [self.avPlayerLayer.player pause];
    [self.avAudioPlayer pause];

    _paused = YES;
    [_playButton setHidden:NO];
    [_pauseButton setHidden:YES];
    [self cancelHideUI];
}

- (void)cancelHideUI
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideUI) object:nil];
}

- (void)triggerHideUI
{
    [self performSelector:@selector(hideUI) withObject:nil afterDelay:2.0f];
}

- (void)hideUI
{
    [_uiView setHidden:YES];
}

//- (void)currentTime
//{
//    NSLog(@"self.avPlayerLayer.player.currentTime : %f",CMTimeGetSeconds(self.avPlayerLayer.player.currentTime));
//    CMTime endTime = CMTimeMakeWithSeconds(10.0f, self.avPlayerLayer.player.currentTime.timescale);
//    if (CMTimeGetSeconds(self.avPlayerLayer.player.currentTime) >= CMTimeGetSeconds(endTime))
//    {
//        [self closeAction];
//    }
//}

- (void)playDidFinishChange:(NSNotification*)notification
{
    self.playedMovieDuration += (float)CMTimeGetSeconds(self.avPlayerLayer.player.currentItem.asset.duration);
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:self.avPlayerLayer.player.currentItem];
    
    self.currentPlayedNum++;
    
    cocos2d::Scene *jeVideoScene = cocos2d::Director::getInstance()->getRunningScene();
    if (!jeVideoScene) {
        return;
    }
    JeVideo *videoLayer = (JeVideo*)jeVideoScene->getChildByTag(TAG_VIDEO);
    if (!videoLayer) {
        return;
    }
    
    if (self.currentPlayedNum <= 1) {
        videoLayer->changeTextureForEnding();
    }
    
    if (self.currentPlayedNum > self.movieNames.count-1) {
        if (self.playType == PLAY_TYPE_ALL) {
            [self.avPlayerLayer.player pause];
            [self.avPlayerLayer removeFromSuperlayer];
            
            if (self.avAudioPlayer) {
                [self.avAudioPlayer stop];
                self.avAudioPlayer = nil;
            }
            
            for (UIView *subView in [self.view subviews]) {
                [subView removeFromSuperview];
            }
            
            videoLayer->playEnding();
            
            [self.progressTimer invalidate];
            self.progressTimer = nil;
        } else {
            [self closeAction];
        }
    } else {
        NSString *movieName = [self.movieNames objectAtIndex:self.currentPlayedNum];
        NSString *subMovieName = [movieName substringToIndex:movieName.length-4];
        NSString *moviePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"common/%@",subMovieName] ofType:@"mp4"];

        int index = [self getIndexFromFileName:subMovieName];
        if (index > 0) {
            [self.titleLabel setText:[self getTitleString:index]];
            [self.titleLabel2 setText:[self getTitleString:index]];
        }
        
        AVAsset *avAsset = [AVAsset assetWithURL:[NSURL fileURLWithPath:moviePath]];
        AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
        [self.avPlayerLayer.player replaceCurrentItemWithPlayerItem:avPlayerItem];
        [self.avPlayerLayer.player play];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playDidFinishChange:) name:AVPlayerItemDidPlayToEndTimeNotification object:self.avPlayerLayer.player.currentItem];
        
        if (self.avAudioPlayer) {
            [self.avAudioPlayer stop];
            self.avAudioPlayer = nil;
        }
        self.avAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[self.audioUrls objectAtIndex:self.currentPlayedNum] error:nil];
        [self.avAudioPlayer play];
        
//        if (self.audioUrls.count > 0  && self.currentPlayedNum <= self.audioUrls.count-1) {
//            if (self.currentPlayedNum%2 != 0) {
//                self.avAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[self.audioUrls objectAtIndex:self.currentPlayedNum] error:nil];
//                [self.avAudioPlayer play];
//            }
//        }
    }
}

- (void)closeAction
{    
    cocos2d::Scene *jeVideoScene = cocos2d::Director::getInstance()->getRunningScene();
    if (!jeVideoScene) {
        return;
    }
    JeVideo *videoLayer = (JeVideo*)jeVideoScene->getChildByTag(TAG_VIDEO);
    if (!videoLayer) {
        return;
    }
    [self.avPlayerLayer.player pause];
    [self.avPlayerLayer removeFromSuperlayer];
    
    if (self.avAudioPlayer) {
        [self.avAudioPlayer stop];
        self.avAudioPlayer = nil;
    }
    
    for (UIView *subView in [self.view subviews]) {
        [subView removeFromSuperview];
    }
    
    videoLayer->movieDidFinishCallback();
    
    [self.progressTimer invalidate];
    self.progressTimer = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getTitleString:(NSInteger)index{
    NSString *resultString;
    switch(index)
    {
        case 1:
        {
            resultString = NSLocalizedString(@"CC_PLAY_PAGE_TITLE_INTRO", @"Intro");
        }
            break;
        case 2:
        case 3:
        case 4:
        {
            resultString = [NSString stringWithFormat:NSLocalizedString(@"CC_PLAY_PAGE_TITLE_PAGE_N", @"Page %d"), index];
        }
            break;
        case 5:
        {
            resultString = NSLocalizedString(@"CC_PLAY_PAGE_TITLE_FINAL", @"Final");
        }
            break;
        default:
            return nil;
            break;
    }
    return resultString;
}

- (int)getIndexFromFileName:(NSString *)fileName{
    if([fileName isEqualToString:@"scene_01"]){
        return 1;
    } else if([fileName isEqualToString:@"scene_02"]) {
        return 2;
    } else if([fileName isEqualToString:@"scene_03"]) {
        return 3;
    } else if([fileName isEqualToString:@"scene_04"]) {
        return 4;
    } else if([fileName isEqualToString:@"scene_05"]) {
        return 5;
    } else if([fileName isEqualToString:@"scene_06"]) {
        return 1;
    } else if([fileName isEqualToString:@"scene_07"]) {
        return 2;
    } else if([fileName isEqualToString:@"scene_08"]) {
        return 3;
    } else if([fileName isEqualToString:@"scene_09"]) {
        return 4;
    } else if([fileName isEqualToString:@"scene_10"]) {
        return 5;
    }
    
    return -1;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

#endif
