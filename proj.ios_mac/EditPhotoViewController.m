//
//  EditPhotoViewController.m
//  bobby
//
//  Created by oasis on 2014. 6. 12..
//
//
#ifndef MACOS
#import "EditPhotoViewController.h"

@interface EditPhotoViewController ()
@property(nonatomic, strong) UIImagePickerController *picker;
@property (nonatomic, strong) UIImage *selectImage;
@property(nonatomic, strong) UIImageView *imageView;
@property(nonatomic, strong) UIScrollView *scrollView;
@property(nonatomic) CGSize originImgViewBounds;
@end

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

@implementation EditPhotoViewController

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithPicker:(UIImagePickerController*)picker andImage:(UIImage*)image
{
    self = [super init];
    if (self) {
        self.selectImage = image;
        self.picker = picker;
        self.delegate = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (IS_IPAD) {
        float systemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
        if (systemVersion >= 7.0) {
            self.preferredContentSize = CGSizeMake(1528/2 + 40, 1148/2);
        } else {
            self.contentSizeForViewInPopover = CGSizeMake(1528/2 + 40, 1148/2);
        }
    }
    
    self.view.backgroundColor = [UIColor blackColor];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectZero];
    title.font = [UIFont fontWithName:@"helvetica-bold" size:(IS_IPAD)?18:16];
    title.text = @"Choose Photo";
    title.textColor = [UIColor darkGrayColor];
    
    self.navigationItem.titleView = title;
    [title sizeToFit];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelAction)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Use" style:UIBarButtonItemStylePlain target:self action:@selector(useAction)];
    
    CGSize navigationBarSize = self.navigationController.navigationBar.frame.size;
    
    CGRect scrollViewRect;
    if (IS_IPAD) {
        scrollViewRect = CGRectMake(0, 0, self.preferredContentSize.width, self.preferredContentSize.height- navigationBarSize.height);
    } else {
        scrollViewRect = CGRectMake(0, self.navigationController.navigationBar.frame.size.height, self.view.frame.size.height, self.view.frame.size.width - (navigationBarSize.height));
    }
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:scrollViewRect];
    self.scrollView.clipsToBounds = YES;
    //    self.scrollView.zoomScale = 1.0;
    
    CGFloat maximumZoom = (IS_IPAD)?2.0:2.5;
    if (self.selectImage.size.height/(self.scrollView.frame.size.height -self.navigationController.navigationBar.frame.size.height) < 1) {
        self.scrollView.maximumZoomScale = 1.0;
    } else if(self.selectImage.size.height/(self.scrollView.frame.size.height -self.navigationController.navigationBar.frame.size.height) >= maximumZoom) {
        self.scrollView.maximumZoomScale = maximumZoom;
    } else {
        self.scrollView.maximumZoomScale = self.selectImage.size.height/(self.scrollView.frame.size.height -self.navigationController.navigationBar.frame.size.height);
    }
//    self.scrollView.maximumZoomScale = 3.0;
    self.scrollView.minimumZoomScale = 1.0;
    self.scrollView.scrollEnabled = YES;
    self.scrollView.delegate = self;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, self.navigationController.navigationBar.frame.size.height, 0);
    [self.view addSubview:self.scrollView];
    
    CGFloat lineHeight = 1;
    if ([UIScreen mainScreen].scale == 2.0f) {
        lineHeight = 0.5f;
    }

    CGRect bottomBarRect;
    if (IS_IPAD) {
        bottomBarRect = CGRectMake(0, self.preferredContentSize.height - (navigationBarSize.height*2), self.preferredContentSize.width, navigationBarSize.height);
    } else {
        bottomBarRect = CGRectMake(0, self.view.frame.size.width - navigationBarSize.height, navigationBarSize.width, navigationBarSize.height);
    }
    
    UIView *bottomBar = [[UIView alloc] initWithFrame:bottomBarRect];
    bottomBar.backgroundColor = [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:130/255.0f];
    [self.view addSubview:bottomBar];
    
    CGRect lineTopRect;
    CGRect lineBottomRect;
    if (IS_IPAD) {
        lineTopRect = CGRectMake(0, 2, self.preferredContentSize.width, lineHeight);
        lineBottomRect = CGRectMake(0, bottomBar.frame.origin.y+1, self.preferredContentSize.width,lineHeight);
    } else {
        lineTopRect = CGRectMake(0, navigationBarSize.height+2, self.view.frame.size.height, lineHeight);
        lineBottomRect = CGRectMake(0, bottomBar.frame.origin.y+1, self.view.frame.size.height, lineHeight);
    }
    
    UIView *lineTop = [[UIView alloc] initWithFrame:lineTopRect];
    lineTop.backgroundColor = [UIColor grayColor];
    [self.view addSubview:lineTop];
    
    UIView *lineBottom = [[UIView alloc] initWithFrame:lineBottomRect];
    lineBottom.backgroundColor = [UIColor grayColor];
    [self.view addSubview:lineBottom];
    
    UILabel *moveAndScale = [[UILabel alloc] initWithFrame:CGRectMake(bottomBar.frame.size.width/7,0,200,bottomBar.frame.size.height)];
    moveAndScale.font = [UIFont systemFontOfSize:(IS_IPAD)?16:14];
    moveAndScale.text = @"Move and Scale";
    moveAndScale.textColor = [UIColor whiteColor];
    [bottomBar addSubview:moveAndScale];
    
    if ([self respondsToSelector:@selector(automaticallyAdjustsScrollViewInsets)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    CGRect imageViewRect;
    if (IS_IPAD) {
        imageViewRect = CGRectMake(0, 0, self.preferredContentSize.width, self.preferredContentSize.height - (navigationBarSize.height*2));
    } else {
        imageViewRect = CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width - (navigationBarSize.height));
    }
    
    self.imageView = [[UIImageView alloc] initWithFrame:imageViewRect];
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    [self initImageViewBoundsAndCenter];
    self.imageView.image = self.selectImage;
    [self.scrollView addSubview:self.imageView];
    self.scrollView.contentSize = self.originImgViewBounds = self.imageView.bounds.size;
    
//    NSLog(@"self.imgSize : %@",NSStringFromCGSize(self.selectImage.size));
    
    // Do any additional setup after loading the view.
//    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 200, 100)];
//    [button setTintColor:[UIColor blueColor]];
//    [button setTitle:@"Test!" forState:UIControlStateNormal];
//    [button addTarget:self action:@selector(imageSave) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:button];
}

//- (void) imageSave
//{
//    UIImage *croppedImg = [self getCroppedImage:self.imageView.image zoom:self.scrollView.zoomScale];
//    NSLog(@"croppedImg size : %@ , zoom : %f", NSStringFromCGSize(croppedImg), self.scrollView.zoomScale);
//    self.imageView.image = cropedImg;
//    self.scrollView.contentSize = cropedImg.size;
//    
//    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
//    self.scrollView.zoomScale = 1.0f;
//}

- (void)initImageViewBoundsAndCenter
{
    if (IS_IPAD) {
        CGFloat ratio = (CGRectGetHeight(self.scrollView.bounds)- (self.navigationController.navigationBar.frame.size.height))/self.selectImage.size.height;
        self.imageView.bounds = CGRectMake(0, 0, self.selectImage.size.width * ratio, (CGRectGetHeight(self.scrollView.bounds)- (self.navigationController.navigationBar.frame.size.height)));
    } else {
        if (self.imageView.image.imageOrientation == UIImageOrientationUp || self.imageView.image.imageOrientation == UIImageOrientationDown || self.imageView.image.imageOrientation == UIImageOrientationUpMirrored || self.imageView.image.imageOrientation == UIImageOrientationDownMirrored) {
            CGFloat ratio = (CGRectGetHeight(self.scrollView.bounds)- (self.navigationController.navigationBar.frame.size.height))/self.selectImage.size.height;
            self.imageView.bounds = CGRectMake(0, 0, self.selectImage.size.width * ratio, (CGRectGetHeight(self.scrollView.bounds)- (self.navigationController.navigationBar.frame.size.height)));
        } else {
            CGFloat ratio = CGRectGetWidth(self.scrollView.bounds)/self.selectImage.size.width;
            self.imageView.bounds = CGRectMake(0, 0, CGRectGetWidth(self.scrollView.bounds), self.selectImage.size.height * ratio);
        }
    }
    
    self.imageView.center = CGPointMake(CGRectGetMidX(self.scrollView.bounds), CGRectGetMidY(self.scrollView.bounds) - (self.navigationController.navigationBar.frame.size.height/2));
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (UIImage *)getCroppedImage:(UIImage *)image zoom:(CGFloat)zoom {
    CGFloat zoomReciprocal = 1.0f / zoom;
    CGFloat topHeight = (self.scrollView.contentSize.height/self.originImgViewBounds.height)*(self.navigationController.navigationBar.frame.size.height);
    CGFloat cropHeight = (IS_IPAD)?(image.size.height * zoomReciprocal):(image.size.height * zoomReciprocal);
    CGFloat xOffset = image.size.width / self.scrollView.contentSize.width;
    CGFloat yOffset = image.size.height / self.scrollView.contentSize.height;
    
    //    CGFloat imgWidth = (self.scrollView.frame.size.width/self.scrollView.frame.size.height)*(image.size.height*zoomReciprocal);
    CGFloat imgWidth = ceilf((self.scrollView.frame.size.width*cropHeight)/(self.scrollView.frame.size.height));
    
    CGRect croppedRect;
    croppedRect = CGRectMake(self.scrollView.contentOffset.x * xOffset,
                             self.scrollView.contentOffset.y * yOffset,
                             imgWidth,
                             cropHeight);
    CGRect transformedRect = croppedRect;
    if(image.imageOrientation==UIImageOrientationRight) {
        transformedRect.origin.x = croppedRect.origin.y;
        transformedRect.origin.y = croppedRect.origin.x;
        transformedRect.size.width = croppedRect.size.height;
        transformedRect.size.height = croppedRect.size.width;
    } else if(image.imageOrientation == UIImageOrientationLeft) {
        transformedRect.origin.x = image.size.height - (croppedRect.origin.y+croppedRect.size.height);
        transformedRect.origin.y = image.size.width - (croppedRect.origin.x+croppedRect.size.width);
        transformedRect.size.width = croppedRect.size.height;
        transformedRect.size.height = croppedRect.size.width;
    } else if(image.imageOrientation == UIImageOrientationDown) {
        transformedRect.origin.x = image.size.width - (croppedRect.origin.x+croppedRect.size.width);
        transformedRect.origin.y = image.size.height - (croppedRect.origin.y+croppedRect.size.height);
    }
    
    //    NSLog(@"imgeOrientation %d", image.imageOrientation);
    
    CGImageRef croppedImageRef = CGImageCreateWithImageInRect([image CGImage], transformedRect);
    UIImage *croppedImage = [[UIImage alloc] initWithCGImage:croppedImageRef scale:[image scale] orientation:[image imageOrientation]];
    CGImageRelease(croppedImageRef);
//    NSLog(@"croppedImage size : %@",NSStringFromCGSize(croppedImage.size));
    return croppedImage;
}

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    UIView *subView = self.imageView;
    
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
    (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
    
    CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?
    (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
    
    if (scrollView.zoomScale == 1) {
        [self initImageViewBoundsAndCenter];
    } else {
        subView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                     scrollView.contentSize.height * 0.5 + offsetY);
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)body {
    
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

- (void) cancelAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) useAction
{
    UIImage *croppedImg = [self getCroppedImage:self.imageView.image zoom:self.scrollView.zoomScale];
    if (self.delegate && [self.delegate respondsToSelector:@selector(photoEditingDone:picker:)]) {
        [self.delegate performSelector:@selector(photoEditingDone:picker:) withObject:croppedImg withObject:self.picker];
    }
}

@end
#endif
