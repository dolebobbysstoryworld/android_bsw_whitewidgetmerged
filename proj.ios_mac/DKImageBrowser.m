//
//  DKImageBrowser.m
//
//  Created by dkhamsing on 3/20/14.
//
//

#ifndef MACOS

// Controllers
#import "DKImageBrowser.h"

// Views
#import "DKImageCell.h"
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

typedef enum imageType{
    TYPE_NORMAL = 0,
    TYPE_SELECT = 1,
}imageType;

@interface DKImageBrowser () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic,strong) UICollectionView *DKImageCollectionView;

@property (nonatomic,strong) UICollectionView *DKThumbnailCollectionView;

@property (nonatomic,strong) UIView *mainContainer;

@property (nonatomic) NSInteger currentPage;

@property (nonatomic, strong) NSIndexPath *selectedIndexPath;


@end


@implementation DKImageBrowser

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}


- (void)setDKBackgroundColor:(UIColor *)DKBackgroundColor {
    _DKBackgroundColor = DKBackgroundColor;
    self.view.backgroundColor = self.DKBackgroundColor;
}


#pragma mark - View Cycle

NSString *const DKBottomCellIdentifer = @"DKBottomCellIdentifer";

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.DKImageWidth = self.view.bounds.size.height;
    self.thumbnailWidth = (IS_IPAD)?358/2:172/2;
    self.DKImagePadding = 0;
    self.DKThumbnailStripHeight = (IS_IPAD)?358/2:172/2;
    self.DKBackgroundColor = [UIColor clearColor];
    self.DKThumbnailStripPosition = DKThumbnailStripPositionBottom;
    
    self.currentPage = 0;
    self.view.backgroundColor = self.DKBackgroundColor;
    
    self.selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    NSAssert(_thumbnailDataSource.count == _DKImageDataSource.count, @"_thumbnailDataSource.count must be the same with _DKImageDataSource.count!!!!!");
    
    CGRect frame;
    frame = self.view.bounds;
    
    
    // Image collection view
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.minimumInteritemSpacing = 0;
    layout.minimumLineSpacing = self.DKImagePadding;
    
    //TODO: account for nav bar
    CGFloat spacingY = (IS_IPAD)?(396-358)/2:(202-172)/2;
    
    frame.origin = CGPointMake(0, 0);
    frame.size.width = self.view.frame.size.height;
    frame.size.height = self.view.frame.size.width -frame.origin.y -self.DKThumbnailStripHeight - spacingY;
    _DKImageCollectionView = [[UICollectionView alloc] initWithFrame:frame collectionViewLayout:layout];
    _DKImageCollectionView.showsHorizontalScrollIndicator = NO;
    _DKImageCollectionView.delegate = self;
    _DKImageCollectionView.dataSource = self;
    _DKImageCollectionView.backgroundColor = self.DKBackgroundColor;
    _DKImageCollectionView.decelerationRate = UIScrollViewDecelerationRateFast;
    [_DKImageCollectionView registerClass:[DKImageCell class] forCellWithReuseIdentifier:DKBottomCellIdentifer];
    [self.view addSubview:_DKImageCollectionView];
    
    // Thumbnail collection view
    UICollectionViewFlowLayout *bottomLayout = [[UICollectionViewFlowLayout alloc] init];
    bottomLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    bottomLayout.minimumInteritemSpacing = 0;
    bottomLayout.minimumLineSpacing = self.DKImagePadding;
    
    frame.origin = CGPointMake(0 , _DKImageCollectionView.frame.size.height + _DKImageCollectionView.frame.origin.y + spacingY);
    frame.size.width =  self.view.frame.size.height;
    frame.size.height = self.DKThumbnailStripHeight-self.DKImagePadding;
    _DKThumbnailCollectionView = [[UICollectionView alloc] initWithFrame:frame collectionViewLayout:bottomLayout];
    _DKThumbnailCollectionView.showsHorizontalScrollIndicator = NO;
    _DKThumbnailCollectionView.allowsMultipleSelection = YES;
    _DKThumbnailCollectionView.delegate = self;
    _DKThumbnailCollectionView.dataSource = self;
    _DKThumbnailCollectionView.backgroundColor = self.DKBackgroundColor;
    [_DKThumbnailCollectionView registerClass:[DKImageCell class] forCellWithReuseIdentifier:DKBottomCellIdentifer];
    [self.view addSubview:_DKThumbnailCollectionView];
    
    // Reposition
    switch (_DKThumbnailStripPosition) {
        case DKThumbnailStripPositionTop: {
            
            CGRect frame = _DKThumbnailCollectionView.frame;
            frame.origin.y = 32+ self.DKImagePadding *2;
            _DKThumbnailCollectionView.frame = frame;
            
            frame = _DKImageCollectionView.frame;
            frame.origin.y = _DKThumbnailCollectionView.frame.size.height + _DKThumbnailCollectionView.frame.origin.y - self.DKImagePadding;
            _DKImageCollectionView.frame = frame;
        }
            break;
            
        default:
            NSLog(@"TODO: strip position for %d", _DKThumbnailStripPosition);
            break;
    }
    
    _DKImageCollectionView.backgroundColor = [UIColor clearColor];
    
    UIImage *background = nil;
    if (IS_IPAD) {
        background = [UIImage imageNamed:@"iosnative/camera_frame_background_view_pad.png"];
    } else {
        background = [UIImage imageNamed:@"iosnative/camera_frame_background_view.png"];
    }
    UIColor *pattern = [UIColor colorWithPatternImage:background];
    
    _DKThumbnailCollectionView.backgroundColor = pattern;
    
    // load collection views
    [_DKImageCollectionView reloadData];
    [_DKThumbnailCollectionView reloadData];
}


#pragma mark - Helpers

- (void)displayImageWithPath:(NSString*)imagePath imageView:(UIImageView*)imageView {
    [self getImageWithPath:imagePath ResultBlock:^(UIImage *image) {
        imageView.image = image;
    }];
}


typedef void (^ImageBlock)(UIImage *image);

- (void)getImageWithPath:(NSString*)imagePath ResultBlock:(ImageBlock)resultBlock {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *url = [NSURL URLWithString:imagePath];
        UIImage *productImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:url options:NSDataReadingMappedIfSafe error:nil]];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (productImage.size.width>0) {
                if (resultBlock) {
                    resultBlock(productImage);
                }
            }
            else {
                NSLog(@"DKImageBrowser: error getting %@",imagePath);
            }
        });
    });
}


#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {    
    return _DKImageDataSource.count;
}


- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}


- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath; {
    return YES;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)cview cellForItemAtIndexPath:(NSIndexPath *)indexPath {    
    if ((cview==_DKImageCollectionView) &&
        (indexPath.item>1)) {
        [_DKThumbnailCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:indexPath.item-1 inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        
//        [_DKThumbnailCollectionView setContentOffset:[_DKThumbnailCollectionView.collectionViewLayout layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForRow:indexPath.item-1 inSection:0]].frame.origin animated:NO];
    }
    
    DKImageCell *cell = [cview dequeueReusableCellWithReuseIdentifier:DKBottomCellIdentifer forIndexPath:indexPath];
    
    cell.testLabel.text = [NSString stringWithFormat:@"image %02d",indexPath.row];
//    NSLog(@"indexPath : %@", indexPath);
//    NSLog(@"selected indexPath : %@", self.selectedIndexPath);
    
    if (cview == _DKImageCollectionView) {
        NSObject *obj = _DKImageDataSource[indexPath.row];
        if ([obj isKindOfClass:[UIImage class]]) {
            UIImage *image = _DKImageDataSource[indexPath.row];
            cell.DKImageView.image = image;
        }
        else if ([obj isKindOfClass:[NSString class]]) {
            NSString *imagePath = _DKImageDataSource[indexPath.row];
            [self displayImageWithPath:imagePath imageView:cell.DKImageView];
        }
        else {
            NSLog(@"Error: the data source must be (String) URLs to images or images");
        }
    } else {
        NSObject *obj = _thumbnailDataSource[indexPath.row];
        if ([obj isKindOfClass:[UIImage class]]) {
            UIImage *image = _thumbnailDataSource[indexPath.row];
            cell.thumbView.image = image;
        }
        else if ([obj isKindOfClass:[NSString class]]) {
            NSString *imagePath = _thumbnailDataSource[indexPath.row];
            [self displayImageWithPath:imagePath imageView:cell.thumbView];
        }
        else {
            NSLog(@"Error: the data source must be (String) URLs to images or images");
        }
        
        [cell setSelectedCell:NO];
        if ([indexPath isEqual:self.selectedIndexPath]) {
            [cell setSelectedCell:YES];
            cell.thumbView.image = [UIImage imageNamed:[self getImageString:indexPath.row+1 imageType:TYPE_SELECT]];
        }
    }
    
    
    return cell;
}

- (NSString*)getImageString:(NSInteger)num imageType:(imageType)type
{
    NSString *result = NULL;
    if (type == TYPE_NORMAL) {
        if (IS_IPAD) {
            result = [NSString stringWithFormat:@"iosnative/home_character_%02ld_normal_pad.png",(long)num];
        } else {
            result = [NSString stringWithFormat:@"iosnative/home_character_%02ld_normal.png",(long)num];
        }
    } else {
        if (IS_IPAD) {
            result = [NSString stringWithFormat:@"iosnative/home_character_%02ld_select_pad.png",(long)num];
        } else {
            result = [NSString stringWithFormat:@"iosnative/home_character_%02ld_select.png",(long)num];
        }
    }
    return result;
}

- (void)collectionView:(UICollectionView *)cview didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
    if (cview == _DKImageCollectionView) {
        return;
    }
    
    NSLog(@"indexPath :%li",(long)indexPath.row);
    [cview deselectItemAtIndexPath:indexPath animated:NO];
    
    _currentPage = indexPath.item;
    
    DKImageCell *previousSelectedCell = (DKImageCell*)[cview cellForItemAtIndexPath:self.selectedIndexPath];
    [previousSelectedCell setSelectedCell:NO];
    previousSelectedCell.thumbView.image = [UIImage imageNamed:[self getImageString:self.selectedIndexPath.row+1 imageType:TYPE_NORMAL]];
    
    DKImageCell *cell = (DKImageCell*)[cview cellForItemAtIndexPath:indexPath];
    [cell setSelectedCell:YES];
    cell.thumbView.image = [UIImage imageNamed:[self getImageString:indexPath.row+1 imageType:TYPE_SELECT]];
    self.selectedIndexPath = indexPath;
    
//    [_DKImageCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    
    [_DKImageCollectionView setContentOffset:[_DKImageCollectionView.collectionViewLayout layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0]].frame.origin animated:NO];
    [cview reloadData];
}

#pragma mark - UICollectionViewDelegateFlowLayout


- (CGSize)collectionView:(UICollectionView *)cview layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (cview==_DKImageCollectionView) {
        return CGSizeMake(self.DKImageWidth, cview.frame.size.height);
    }
    
    CGSize size;
    size.height = cview.frame.size.height;
    size.width = self.thumbnailWidth;//size.height * _DKImageWidth / _DKImageCollectionView.frame.size.height;
    
    return size;
}


- (UIEdgeInsets)collectionView:(UICollectionView *)cview layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (cview==_DKImageCollectionView) {
        return UIEdgeInsetsMake(0, 0,
                                0, 0);
    }
    
    return UIEdgeInsetsMake(0, self.DKImagePadding,
                            0, self.DKImagePadding);
}



#pragma mark - UIScrollViewDelegate

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    if (scrollView==_DKThumbnailCollectionView) {
        return;
    }
    
    scrollView.userInteractionEnabled = NO;
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (scrollView==_DKThumbnailCollectionView) {
        return;
    }
    
    if (scrollView.isDecelerating) {
        return;
    }
    
    NSInteger item = floor((scrollView.contentOffset.x - self.DKImageWidth / 2) / self.DKImageWidth) + 1;
    
    NSUInteger lastItem = _DKImageDataSource.count - 1;
    
    if (item>_currentPage) {
        _currentPage++;
    }
    else {
        _currentPage--;
    }
    
    if (_currentPage < 0) {
        _currentPage=0;
    }
    
    if (_currentPage>lastItem) {
        _currentPage = lastItem;
    }
    
//    [_DKImageCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:_currentPage inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES ];
    
    [_DKImageCollectionView setContentOffset:[_DKImageCollectionView.collectionViewLayout layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForRow:_currentPage inSection:0]].frame.origin animated:NO];
    
    DKImageCell *previousSelectedCell = (DKImageCell*)[_DKThumbnailCollectionView cellForItemAtIndexPath:self.selectedIndexPath];
    [previousSelectedCell setSelectedCell:NO];
    previousSelectedCell.thumbView.image = [UIImage imageNamed:[self getImageString:self.selectedIndexPath.row+1 imageType:TYPE_NORMAL]];
    
    DKImageCell *cell = (DKImageCell*)[_DKThumbnailCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:_currentPage inSection:0]];
    [cell setSelectedCell:YES];
    cell.thumbView.image = [UIImage imageNamed:[self getImageString:_currentPage+1 imageType:TYPE_SELECT]];
    self.selectedIndexPath = [NSIndexPath indexPathForItem:_currentPage inSection:0];
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView==_DKThumbnailCollectionView) {
        return;
    }
    
//    [_DKImageCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:_currentPage inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES ];
    
    [_DKImageCollectionView setContentOffset:[_DKImageCollectionView.collectionViewLayout layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForRow:_currentPage inSection:0]].frame.origin animated:NO];
    
    scrollView.userInteractionEnabled = YES;
}


@end

#endif
