//
//  CharacterImageCell.h
//  bobby
//
//  Created by oasis on 2014. 7. 1..
//
//
#ifndef MACOS
#import <UIKit/UIKit.h>

@interface CharacterImageCell : UICollectionViewCell
{
    UIImageView *selectMark;
}
@property (nonatomic,strong) UIImageView *thumbView;
- (void)setSelectedCell:(BOOL)selected;
@end
#endif
