//
//  TSAccountAgreementView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 2..
//
//

#import "TSAccountAgreementView.h"
#import "ResolutionManager.h"
#import "TSTitleScrollTextView.h"
#import "UIImage+PathExtention.h"
#import "UIColor+RGB.h"
#import "UIButton+DolesUI.h"

@interface TSAccountAgreementView () < TSCheckBoxDelegate >

@property (nonatomic, strong) TSCheckBox *termsCheckBox;
@property (nonatomic, strong) TSCheckBox *privacyCheckBox;
@property (nonatomic, strong) UIButton *nextButton;

@end

@implementation TSAccountAgreementView

- (id)initWithFrame:(CGRect)frame {
    return [self initWithFrame:frame forAbout:NO];
}

- (id)initWithFrame:(CGRect)frame forAbout:(BOOL)about
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.forAboutMode = about;
        self.agreeCheckBox = nil;
        UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"bg_map_factor.png"]];
        backgroundView.frame = getRectValue(TSViewIndexShare, TSShareSubViewOrgRect);
        [self addSubview:backgroundView];
        
//        [self addTermsTextView];
        [self addTermHTMLView];
        if (self.forAboutMode == NO) {
            [self addCheckBox];
            [self addButtons];
        }
    }
    return self;
}

- (void)addTermsTextView {
    NSString *termsPath = [[NSBundle mainBundle] pathForResource:@"Terms and Conditions(LTA)" ofType:@"txt"];
    NSError *error;
    NSString *termsContent = [NSString stringWithContentsOfFile:termsPath encoding:NSUTF8StringEncoding error:&error];
    NSAssert(!error, [error localizedDescription]);
    CGRect frame = CGRectZero;
    if (self.forAboutMode == NO) {
        frame = getRectValue(TSViewIndexAgreement, TSAgreeTermsRect);
    } else {
        frame = getRectValue(TSViewIndexAgreement, TSAgreeTermsForAboutRect);
    }
    TSTitleScrollTextView *termsView =
    [[TSTitleScrollTextView alloc] initWithFrame:frame Title:NSLocalizedString(@"PF_ABOUT_TITLE_SERVICE", @"Terms of Service Contents") Text:termsContent];
    [self addSubview:termsView];
    
    NSString *privacyPath = [[NSBundle mainBundle] pathForResource:@"Privacy Policy(LTA)" ofType:@"txt"];
    error = nil;
    NSString *privacyContent = [NSString stringWithContentsOfFile:privacyPath encoding:NSUTF8StringEncoding error:&error];
    NSAssert(!error, [error localizedDescription]);
    if (self.forAboutMode == NO) {
        frame = getRectValue(TSViewIndexAgreement, TSAgreePrivacyRect);
    } else {
        frame = getRectValue(TSViewIndexAgreement, TSAgreePrivacyForAboutRect);
    }
    TSTitleScrollTextView *privacyView =
    [[TSTitleScrollTextView alloc] initWithFrame:frame Title:NSLocalizedString(@"PF_ABOUT_TITLE_PRIVACY", @"Privacy Policy Contents") Text:privacyContent];
    [self addSubview:privacyView];
}

- (void)addTermHTMLView {
    CGRect frame = CGRectZero;
    if (self.forAboutMode == NO) {
        frame = getRectValue(TSViewIndexAgreement, TSAgreeTermsRect);
    } else {
        frame = getRectValue(TSViewIndexAgreement, TSAgreeTermsForAboutRect);
    }

//    NSURL *termsURL = [[NSBundle mainBundle] URLForResource:@"terms_and_conditions" withExtension:@"html"];
    NSURL *termsURL = [[NSBundle mainBundle] URLForResource:@"terms" withExtension:@"html"];
    TSTitleScrollTextView *termsView =
    [[TSTitleScrollTextView alloc] initWithFrame:frame Title:NSLocalizedString(@"PF_ABOUT_TITLE_SERVICE", @"Terms of Service Contents")
                                             URL:termsURL];
    [self addSubview:termsView];
    
    if (self.forAboutMode == NO) {
        frame = getRectValue(TSViewIndexAgreement, TSAgreePrivacyRect);
    } else {
        frame = getRectValue(TSViewIndexAgreement, TSAgreePrivacyForAboutRect);
    }

//    NSURL *privacyURL = [[NSBundle mainBundle] URLForResource:@"privacy_policy" withExtension:@"html"];
    NSURL *privacyURL = [[NSBundle mainBundle] URLForResource:@"privacy" withExtension:@"html"];
    TSTitleScrollTextView *privacyView =
    [[TSTitleScrollTextView alloc] initWithFrame:frame Title:NSLocalizedString(@"PF_ABOUT_TITLE_PRIVACY", @"Privacy Policy Contents")
                                             URL:privacyURL];
    [self addSubview:privacyView];
}

- (void)addCheckBox {
    self.termsCheckBox = [[TSCheckBox alloc] initWithFrame:getRectValue(TSViewIndexAgreement, TSAgreeTermsCheckRect)];
    [self.termsCheckBox setImage:[UIImage imageNamedEx:@"checkbox_off.png" ignore:YES] forState:UIControlStateNormal];
    [self.termsCheckBox setImage:[UIImage imageNamedEx:@"checkbox_on.png" ignore:YES] forState:UIControlStateSelected];
    self.termsCheckBox.delegate = self;
    [self addSubview:self.termsCheckBox];
    
    UILabel *termsLabel = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexAgreement, TSAgreeTermsLabelRect)];
    termsLabel.font = [UIFont fontWithName:kDefaultFontName size:13];
    termsLabel.textColor = RGBH(@"#4f2c00");
    termsLabel.text = NSLocalizedString(@"PF_SIGNUP_CHECKBOX_TITLE_AGREE_TERMS", @"Conditions and Privacy Policy");
    [self addSubview:termsLabel];

    self.privacyCheckBox = [[TSCheckBox alloc] initWithFrame:getRectValue(TSViewIndexAgreement, TSAgreePrivacyCheckRect)];
    [self.privacyCheckBox setImage:[UIImage imageNamedEx:@"checkbox_off.png" ignore:YES] forState:UIControlStateNormal];
    [self.privacyCheckBox setImage:[UIImage imageNamedEx:@"checkbox_on.png" ignore:YES] forState:UIControlStateSelected];
    self.privacyCheckBox.delegate = self;
    [self addSubview:self.privacyCheckBox];

//    UILabel *privacyLabel = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexAgreement, TSAgreePrivacyLabelRect)];
//    privacyLabel.font = [UIFont fontWithName:kDefaultFontName size:13];
//    privacyLabel.textColor = RGBH(@"#4f2c00");
//    privacyLabel.text = NSLocalizedString(@"PF_SIGNUP_CHECKBOX_TITLE_AGREE_PRIVACY", @"Conditions and Privacy Policy");
//    [self addSubview:privacyLabel];
    
    UILabel *privacyLabel = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexAgreement, TSAgreePrivacyTwoLineLabelRect)];
    privacyLabel.font = [UIFont fontWithName:kDefaultFontName size:13];
    privacyLabel.textColor = RGBH(@"#4f2c00");
    privacyLabel.numberOfLines = 0;
    privacyLabel.text = NSLocalizedString(@"PF_SIGNUP_CHECKBOX_TITLE_AGREE_PRIVACY", @"Conditions and Privacy Policy");
    [self addSubview:privacyLabel];
}

- (void)checkBox:(TSCheckBox *)checkBox didChangeSelectedStatus:(BOOL)selected {
    if (self.termsCheckBox.selected == YES && self.privacyCheckBox.selected == YES) {
        self.nextButton.enabled = YES;
    } else {
        self.nextButton.enabled = NO;
    }
}

- (void)addButtons {
    UIButton *cancelButton = [UIButton buttonWithType:TSDolsTypeCancel Frame:getRectValue(TSViewIndexAgreement, TSAgreeCancelBtnRect)
                                                Title:NSLocalizedString(@"CM_POPUP_BUTTON_TITLE_CANCEL", @"Cancel")];
    [cancelButton addTarget:self action:@selector(selectorCencelButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cancelButton];

    self.nextButton = [UIButton buttonWithType:TSDolsTypeOK Frame:getRectValue(TSViewIndexAgreement, TSAgreeNextBtnRect)
                                         Title:NSLocalizedString(@"PF_SIGNUP_BUTTON_TITLE_NEXT", @"Next")];
    [self.nextButton addTarget:self action:@selector(selectorNextButton:) forControlEvents:UIControlEventTouchUpInside];
    self.nextButton.enabled = NO;
    [self addSubview:self.nextButton];
}

- (IBAction)selectorCencelButton:(id)sender {
    if (self.agreeCheckBox) {//normal mode
        self.agreeCheckBox.selected = NO;
        if (self.agreeCheckBox.delegate && [self.agreeCheckBox.delegate respondsToSelector:@selector(checkBox:didChangeSelectedStatus:)]) {
            [self.agreeCheckBox.delegate checkBox:nil didChangeSelectedStatus:self.agreeCheckBox.selected];
        }
    } else {//facebook mode
        if (self.delegate && [self.delegate respondsToSelector:@selector(view:Page:userData:)]) {
            [self.delegate view:self Page:TSViewIndexLogin userData:nil];
        }
    }
}

- (IBAction)selectorNextButton:(id)sender {
    if (self.agreeCheckBox) {//normal mode
        self.agreeCheckBox.selected = YES;
        if (self.agreeCheckBox.delegate && [self.agreeCheckBox.delegate respondsToSelector:@selector(checkBox:didChangeSelectedStatus:)]) {
            [self.agreeCheckBox.delegate checkBox:nil didChangeSelectedStatus:self.agreeCheckBox.selected];
        }
    } else {//facebook mode
        if (self.delegate && [self.delegate respondsToSelector:@selector(view:Page:userData:)]) {
            [self.delegate view:self Page:TSViewIndexSignupSNS userData:nil];
        }
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
