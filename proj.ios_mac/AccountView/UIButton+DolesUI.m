//
//  UIButton+DolesUI.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 30..
//
//

#import "UIButton+DolesUI.h"
#import "ResolutionManager.h"
#import "UIImage+PathExtention.h"
#import "UIColor+RGB.h"

@implementation UIButton (DolesUI)

+ (UIButton*)buttonWithType:(TSDolesType)type Frame:(CGRect)frame Title:(NSString*)title {
    return [UIButton buttonWithType:type Frame:frame Title:title Font:[UIFont fontWithName:kDefaultBoldFontName size:17] Color:RGBH(@"#ffffff")];
}

+ (UIButton*)buttonWithType:(TSDolesType)type Frame:(CGRect)frame Title:(NSString*)title Font:(UIFont*)font Color:(UIColor*)color {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    NSString *normalName, *pressName, *dimName;
    UIEdgeInsets inSet;
    switch (type) {
        case TSDolsTypeOK:
        case TSDolsTypeCancel: {
            normalName = [NSString stringWithFormat:@"button_normal_0%d.png", type];
            pressName = [NSString stringWithFormat:@"button_press_0%d.png", type];
            dimName = [NSString stringWithFormat:@"button_dim_0%d.png", type];
            inSet = UIEdgeInsetsMake(17, 17, 17, 17);
        } break;
        case TSDolsTypePopOK: {
            normalName = @"popup_ok_btn_normal.png";
            pressName = @"popup_ok_btn_press.png";
            dimName = @"popup_btn_dim.png";
            inSet = UIEdgeInsetsMake(10, 10, 10, 10);
        } break;
        case TSDolsTypePopCancel: {
            normalName = @"popup_cancel_btn_normal.png";
            pressName = @"popup_cancel_btn_press.png";
            dimName = @"popup_btn_dim.png";
            inSet = UIEdgeInsetsMake(10, 10, 10, 10);
        } break;
        case TSDolsTypeAbout: {
            normalName = @"blue_button_normal.png";
            pressName = @"blue_button_pressed.png";
            inSet = UIEdgeInsetsMake(17, 17, 17, 17);
        } break;
        default:
            break;
    }
    
    [button setBackgroundImage:[[UIImage imageNamedEx:normalName ignore:YES] resizableImageWithCapInsets:inSet]
                                forState:UIControlStateNormal];
    [button setBackgroundImage:[[UIImage imageNamedEx:pressName ignore:YES] resizableImageWithCapInsets:inSet]
                                forState:UIControlStateHighlighted];
    if (dimName) {
        [button setBackgroundImage:[[UIImage imageNamedEx:dimName ignore:YES] resizableImageWithCapInsets:inSet]
                          forState:UIControlStateDisabled];
    }
    
    [button setTitle:title forState:UIControlStateNormal];
    button.titleLabel.font = font;
    
    if (dimName) {
        CGFloat r,g,b, a;
        [color getRed:&r green:&g blue:&b alpha:&a];
        UIColor *dimColor = [UIColor colorWithRed:r green:g blue:b alpha:0.6f];
        [button setTitleColor:dimColor forState:UIControlStateDisabled];
    }
    [button setTitleColor:color forState:UIControlStateNormal];
    
    button.titleLabel.numberOfLines = 0;
    button.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    return  button;
}

+ (UIButton*)underlineButtonWithTitle:(NSString*)string Frame:(CGRect)frame {
    return [UIButton underlineButtonWithTitle:string Frame:frame Tag:-999999 textColor:RGBH(@"#0c3c60")];
}

+ (UIButton*)underlineButtonWithTitle:(NSString*)string Frame:(CGRect)frame Tag:(NSInteger)tag {
    return [UIButton underlineButtonWithTitle:string Frame:frame Tag:tag textColor:RGBH(@"#0c3c60")];
}

+ (UIButton*)underlineButtonWithTitle:(NSString*)string Frame:(CGRect)frame textColor:(UIColor*)textColor {
    return [UIButton underlineButtonWithTitle:string Frame:frame Tag:-999999 textColor:textColor];
}

+ (UIButton*)underlineButtonWithTitle:(NSString*)string Frame:(CGRect)frame Tag:(NSInteger)tag textColor:(UIColor*)textColor {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    [button setTag:tag];
    
    NSMutableAttributedString *normalTitle =
    [[NSMutableAttributedString alloc] initWithString:string
                                           attributes:@{NSForegroundColorAttributeName: textColor,
                                                        NSFontAttributeName:[UIFont fontWithName:kDefaultFontName size:14]}];
    [normalTitle addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [string length])];
    [button setAttributedTitle:normalTitle forState:UIControlStateNormal];
    
    NSMutableAttributedString *pressTitle =
    [[NSMutableAttributedString alloc] initWithString:string
                                           attributes:@{NSForegroundColorAttributeName: textColor,
                                                        NSFontAttributeName:[UIFont fontWithName:kDefaultFontName size:14]}];
    [pressTitle addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [string length])];
    [button setAttributedTitle:pressTitle forState:UIControlStateHighlighted];
    
    //    button.backgroundColor = [UIColor lightGrayColor];
    return button;
}

- (void)showActivity:(BOOL)show {
    UIActivityIndicatorView *indicatorView = nil;
    if (show) {
        indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        CGRect frame = indicatorView.frame;
        frame.origin = CGPointMake(self.frame.size.width - indicatorView.frame.size.width*2, (self.frame.size.height - indicatorView.frame.size.height)/2);
        indicatorView.tag = 999777;
        indicatorView.frame = frame;
        [self addSubview:indicatorView];
        [indicatorView startAnimating];
    } else {
        indicatorView = (UIActivityIndicatorView*)[self viewWithTag:999777];
        if (indicatorView) {
            [indicatorView stopAnimating];
            [indicatorView removeFromSuperview];
        }
    }
}

@end
