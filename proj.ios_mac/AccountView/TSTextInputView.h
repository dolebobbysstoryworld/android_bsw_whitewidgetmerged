//
//  TSTextInputView.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 23..
//
//

#import "TSPopupView.h"

@interface TSTextInputView : TSPopupView

- (id)initWithTitle:(NSString*)title placehold:(NSString*)placehold;

@end
