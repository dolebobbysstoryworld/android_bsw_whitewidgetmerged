//
//  TSShareCommentView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 31..
//
//

#import "TSShareCommentView.h"
//#import "TSPopupViewController.h"

NSString *const kTSShareUserDataKeyTitle    = @"booktitle";
NSString *const kTSShareUserDataKeyAuthor   = @"authorname";
NSString *const kTSShareUserDataKeyTarget   = @"target";
NSString *const kTSShareUserDataKeyTargetFacebook = @"facebook";
NSString *const kTSShareUserDataKeyTargetYouTube = @"youtube";


@interface TSShareCommentView () < UITextViewDelegate >

@property (nonatomic, strong) NSString *bookTitle;
@property (nonatomic, strong) NSString *authorName;
@property (nonatomic, strong) NSString *defaultComment;

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UITextView *commentView;

@end

@implementation TSShareCommentView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithBookTitle:(NSString *)title authorName:(NSString *)name target:(NSString *)target {
    self = [super init];
    if (self) {
        self.bookTitle = title;
        self.authorName = name;
        self.sharetargent = target;
        if ([self.sharetargent isEqualToString:kTSShareUserDataKeyTargetYouTube] == YES) {
//            NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
//            if ([language isEqualToString:@"ko"]) {
//                self.defaultComment =
//                [NSString stringWithFormat:NSLocalizedString(@"PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_YOUTUBE", @"[(%@)] written by (%@)!"),
//                 self.authorName, self.bookTitle];
//            } else if ([language isEqualToString:@"ja"]) {
//                self.defaultComment =
//                [NSString stringWithFormat:NSLocalizedString(@"PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_YOUTUBE", @"[(%@)] written by (%@)!"),
//                 self.authorName, self.bookTitle];
//            } else {
//                self.defaultComment =
//                [NSString stringWithFormat:NSLocalizedString(@"PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_YOUTUBE", @"[(%@)] written by (%@)!"),
//                 self.bookTitle, self.authorName];
//            }
            self.hashTag = NSLocalizedString(@"PF_SHARECOMMNET_POPUP_HASHTAG_YOUTUBE", @"I used the #DoleBobby’sJourney App!");
//            [NSString stringWithFormat:NSLocalizedString(@"PF_SHARECOMMNET_POPUP_HASHTAG_YOUTUBE", @"I used the #DoleBobby’sJourney App!")];//, self.bookTitle];
        } else {
//            NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
//            if ([language isEqualToString:@"ko"]) {
//                self.defaultComment =
//                [NSString stringWithFormat:NSLocalizedString(@"PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_FACEBOOK",
//                                                             @"[%@] written by %@! I used the #DoleBobby'sJourney App!"),
//                 self.authorName, self.bookTitle];
//            } else if ([language isEqualToString:@"ja"]) {
//                self.defaultComment =
//                [NSString stringWithFormat:NSLocalizedString(@"PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_FACEBOOK",
//                                                             @"[%@] written by %@! I used the #DoleBobby'sJourney App!"),
//                 self.authorName, self.bookTitle];
//            } else {
//                self.defaultComment =
//                [NSString stringWithFormat:NSLocalizedString(@"PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_FACEBOOK",
//                                                             @"[%@] written by %@! I used the #DoleBobby'sJourney App!"),
//                 self.bookTitle, self.authorName];
//            }
//            self.hashTag = NSLocalizedString(@"PF_SHARECOMMNET_POPUP_HASHTAG_FACEBOOK", @"I used the #DoleBobby’sJourney App!");
        }
    }
    return self;
}

- (void)adjustTopViewLayout {
    //    NSLog(@"TSPickerView::adjustTopViewLayout");
    UIImageView *top = [[UIImageView alloc] initWithImage:
                        [[UIImage imageNamedEx:@"popup_top.png" ignore:YES]
                         resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)]];
    top.frame = CGRectMake(0, 0, POPUPVIEW_WIDTH, 42);
    self.topView = top;
    [self addSubview:self.topView];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(13, 10, 246, 21)];
    self.titleLabel.font = [UIFont fontWithName:kDefaultFontName size:19];
    self.titleLabel.textColor = RGBH(@"#ffffff");
    self.titleLabel.text = NSLocalizedString(@"PF_SHARECOMMENT_POPUP_TITLE", @"Write your comment");
    [self addSubview:self.titleLabel];
}

- (void)adjustContentViewLayout {
    //    NSLog(@"TSMessageView::adjustContentViewLayout");)
    self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0, self.topView.frame.size.height, POPUPVIEW_WIDTH, (18+210+18)/2)];
    
    UIImageView *middle = [[UIImageView alloc] initWithImage:
                           [[UIImage imageNamedEx:@"popup_middle.png" ignore:YES]
                            resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)]];
    middle.frame = CGRectMake(0, 0, POPUPVIEW_WIDTH, (18+210+18)/2);
    [self.contentView addSubview:middle];
    
    UIImageView *commentLeft = [[UIImageView alloc] initWithImage:
                                [[UIImage imageNamedEx:@"popup_share_input_bg_left.png" ignore:YES]
                                 resizableImageWithCapInsets:UIEdgeInsetsMake(30, 5, 30, 9)]];
    commentLeft.frame = CGRectMake(18/2, 18/2, 10, 210/2);
    [self.contentView addSubview:commentLeft];
    UIImageView *commentMid = [[UIImageView alloc] initWithImage:
                                [[UIImage imageNamedEx:@"popup_share_input_bg_center.png" ignore:YES]
                                 resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)]];
    commentMid.frame = CGRectMake(18/2+commentLeft.frame.size.width, 18/2, (508-20-20)/2, 210/2);
    [self.contentView addSubview:commentMid];
    UIImageView *commentRight = [[UIImageView alloc] initWithImage:
                                [[UIImage imageNamedEx:@"popup_share_input_bg_right.png" ignore:YES]
                                 resizableImageWithCapInsets:UIEdgeInsetsMake(30, 1, 30, 6)]];
    commentRight.frame = CGRectMake(18/2+commentLeft.frame.size.width+commentMid.frame.size.width, 18/2, 10, 210/2);
    [self.contentView addSubview:commentRight];
    
    self.commentView = [[UITextView alloc] initWithFrame:CGRectMake(18/2, 18/2, 508/2, 210/2)];
    self.commentView.backgroundColor = [UIColor clearColor];
    self.commentView.font = [UIFont fontWithName:kDefaultFontName size:34/2];
    self.commentView.textColor = RGBH(@"#9fa0a3");
//    self.commentView.textContainer.lineFragmentPadding = 0;
//    self.commentView.textContainerInset = UIEdgeInsetsMake(14, 23, 0, 14);
    self.commentView.textAlignment = NSTextAlignmentLeft;
    self.commentView.autocorrectionType = UITextAutocorrectionTypeNo;
    self.commentView.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.commentView.spellCheckingType = UITextSpellCheckingTypeNo;
    self.commentView.text = self.defaultComment;
    self.commentView.delegate = self;

    [self.contentView addSubview:self.commentView];
    
    [self addSubview:self.contentView];
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    textView.textColor = RGBH(@"#606060");
    if ([textView.text isEqualToString:self.defaultComment] == YES) {
        textView.text = @"";
    }
    
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    textView.textColor = RGBH(@"#9fa0a3");
    if ([textView.text isEqualToString:@""]) {
        textView.text = self.defaultComment;
    }
    
    [textView resignFirstResponder];
}

- (NSString*)comment {
    return self.commentView.text;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
