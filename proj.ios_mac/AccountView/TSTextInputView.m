//
//  TSTextInputView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 23..
//
//

#import "TSTextInputView.h"

@interface TSTextInputView ()

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *placehold;

@end

@implementation TSTextInputView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithTitle:(NSString *)title placehold:(NSString *)placehold {
    self = [super init];
    if (self) {
        self.title = title;
        self.placehold = placehold;
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
