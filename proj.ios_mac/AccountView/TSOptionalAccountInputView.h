//
//  TSOptionalAccountInputView.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 11. 21..
//
//

#import <UIKit/UIKit.h>

@interface TSOptionalAccountInputView : UIView

@property (nonatomic, readonly) BOOL isEdited;
@property (nonatomic, strong) NSString *birthday;
@property (nonatomic, strong) NSString *countryCode;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, assign) NSInteger gender;

- (instancetype)initWithFrame:(CGRect)frame Separator:(BOOL)separator;

@end
