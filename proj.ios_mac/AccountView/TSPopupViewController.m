//
//  TSPopupViewController.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 23..
//
//

#import "TSPopupViewController.h"
#import "TSCheckPasswordView.h"
#import "TSProxyHelper.h"
#import "TSMessageView.h"

#define TAG_POPUP_CONFIRM           606061

@interface TSPopupViewController () < TSPopupViewDelegate >

@property (nonatomic, assign) BOOL isKeyboardHide;
@property (nonatomic, assign) TSPopupType popupType;
@property (nonatomic, strong) TSPopupView *popupView;
@property (nonatomic, strong) TSPopupView *messageView;
@property (nonatomic, strong) id object;
@property (nonatomic, strong) UIView *subView;

@end

@implementation TSPopupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithPopupMode:(TSPopupType)type userData:(id)object {
    self = [super init];
    if (self) {
        self.popupType = type;
        self.object = object;
    }
    return self;
}

- (void)viewWillLayoutSubviews {
    if (self.popupView) {
        [super viewWillLayoutSubviews];
        return;
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.view.superview.layer.masksToBounds = YES;
        
        if(!IS_IOS8) {
            self.view.superview.bounds = getRectValue(TSViewIndexShare, TSShareSuperViewControllerRect);
            self.view.frame = getRectValue(TSViewIndexShare, TSShareSuperViewControllerRect);
        }
        
        self.view.superview.backgroundColor = [UIColor clearColor];
        self.view.backgroundColor = [UIColor clearColor];
    }

    switch (self.popupType) {
        case TSPopupTypePassword: {
            TSCheckPasswordView *checkPWView = [[TSCheckPasswordView alloc] initWithCoin:[self.object intValue]];
            checkPWView.customButtonTitle = NSLocalizedString(@"CC_EXPANDEDITEM_POPUP_COIN_BUTTON_TITLE_UNLOCK", @"Unlock");
            checkPWView.buttonLayout = TSPopupButtonLayoutCancelCustom;
            checkPWView.delegate = self;
            [checkPWView showInFullView:self.view];
            self.popupView = checkPWView;
        } break;
        case TSPopupTypeShareComment: {
            NSString *title = [self.object objectForKey:kTSShareUserDataKeyTitle];
            NSString *author = [self.object objectForKey:kTSShareUserDataKeyAuthor];
            NSString *target = [self.object objectForKey:kTSShareUserDataKeyTarget];
            TSShareCommentView *commentView = [[TSShareCommentView alloc] initWithBookTitle:title authorName:author target:target];
            commentView.customButtonTitle = NSLocalizedString(@"CC_JOURNEYMAP_POPUP_TITLE_SHARE_LIST", @"Share");
            commentView.buttonLayout = TSPopupButtonLayoutCancelCustom;
            commentView.delegate = self;
            commentView.pendingCancel = YES;
            [commentView showInFullView:self.view];
            self.popupView = commentView;
        } break;
        default:
            NSLog(@"Unsupport type popup!!!");
            break;
    }

    [super viewWillLayoutSubviews];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(noticeWillShowKeyboard:) name:UIKeyboardWillShowNotification object:nil];
    [center addObserver:self selector:@selector(noticeWillHideKeyboard:) name:UIKeyboardWillHideNotification object:nil];
//    self.subView = [[UIView alloc] initWithFrame:getRectValue(TSViewIndexShare, TSShareSubViewRect)];
//    self.subView.backgroundColor = [UIColor greenColor];
//    [self.view addSubview:self.subView];
}

- (void)popview:(TSPopupView *)view didClickedButton:(TSPopupButtonType)type {
    if (view.tag == TAG_POPUP_CONFIRM) {
        if (type == TSPopupButtonTypeOK) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kTSNotifivationShare object:nil userInfo:@{@"buttontype": @"cancel"}];//cancel 결과만 전달
            [self.popupView processPendingCancel];
        } else {
            self.popupView.popupHidden = NO;
            return;
        }
    } else {
        switch (self.popupType) {
            case TSPopupTypePassword: {
                if (type == TSPopupButtonTypeCustom) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:kTSNotifivationUseCoinPassword object:nil userInfo:@{@"buttontype": @"ok"}];//충전 결과를 업데이트 하자.
                } else {
                    [[NSNotificationCenter defaultCenter] postNotificationName:kTSNotifivationUseCoinPassword object:nil userInfo:@{@"buttontype": @"cancel"}];//충전 결과를 업데이트 하자.
                }
            } break;
            case TSPopupTypeShareComment: {
                if (type == TSPopupButtonTypeCustom) {
                    TSShareCommentView *commentView = (TSShareCommentView*)view;
                    NSString *finalCommentString = @"";
                    if ([commentView.sharetargent isEqualToString:kTSShareUserDataKeyTargetYouTube] == YES) {
                        finalCommentString = [NSString stringWithFormat:@"%@ %@", commentView.comment, commentView.hashTag];
                    } else {
                        if ([commentView.comment length] > 0) {
                            finalCommentString = commentView.comment;
                        }
                    }
                    [[NSNotificationCenter defaultCenter] postNotificationName:kTSNotifivationShare object:nil
                                                                      userInfo:@{@"buttontype": @"share", @"comment": finalCommentString}];//comment 전달
                } else {
                    self.popupView.popupHidden = YES;
                    if (self.isKeyboardHide == NO) {
                        [self.view endEditing:YES];
                    }
//                      [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(timerFireMethod:) userInfo:nil repeats:NO];
                    TSMessageView *messageView =
                    [[TSMessageView alloc] initWithMessage:NSLocalizedString(@"CC_POPUP_CANCEL_SHARE",
                                                                             @"Do you want to cancel?\nIf you cancel,\nit will not be shared.")
                                                      icon:TSPopupIconTypeWarning];
                    messageView.buttonLayout = TSPopupButtonLayoutCancelOK;
                    messageView.delegate = self;
                    messageView.nodimlayer = YES;
                    messageView.tag = TAG_POPUP_CONFIRM;
                    NSLog(@"subpopup frame : %@", NSStringFromCGRect(messageView.frame));
                    [messageView showInView:self.view];
                    self.messageView = messageView;
                    return;
                }
            } break;
            default:
                NSLog(@"Unsupport type popup!!!");
                break;
        }
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)timerFireMethod:(NSTimer *)timer {
    TSMessageView *messageView =
    [[TSMessageView alloc] initWithMessage:NSLocalizedString(@"CC_POPUP_CANCEL_SHARE",
                                                             @"Do you want to cancel?\nIf you cancel,\nit will not be shared.")
                                      icon:TSPopupIconTypeWarning];
    messageView.buttonLayout = TSPopupButtonLayoutCancelOK;
    messageView.delegate = self;
    messageView.tag = TAG_POPUP_CONFIRM;
    NSLog(@"subpopup frame : %@", NSStringFromCGRect(messageView.frame));
    [messageView showInView:self.view];
    self.messageView = messageView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)noticeWillShowKeyboard:(NSNotification *)notification {
    self.isKeyboardHide = NO;
    NSInteger move = 0;
    switch (self.popupType) {
        case TSPopupTypePassword: {
            ((TSCheckPasswordView*)self.popupView).keyboardShow = YES;
            move = 50;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                move = 0;
                if ([UIScreen mainScreen].bounds.size.height == 480) {
                    move = 50;
                }
            }
        } break;
        case TSPopupTypeShareComment: {
            move = 100;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                move = 70;
                if ([UIScreen mainScreen].bounds.size.height == 480) {
                    move = 100;
                }
            }
        } break;
        default:
            NSLog(@"Unsupport type popup!!!");
            break;
    }
    CGRect frame = self.view.frame;
    frame.origin.y = frame.origin.y - move;
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = frame;
    }];
}

- (void)noticeWillHideKeyboard:(NSNotification *)notification {
    self.isKeyboardHide = YES;
    NSInteger move = 0;
    switch (self.popupType) {
        case TSPopupTypePassword: {
            ((TSCheckPasswordView*)self.popupView).keyboardShow = NO;
            move = 50;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                move = 0;
                if ([UIScreen mainScreen].bounds.size.height == 480) {
                    move = 50;
                }
            }
        } break;
        case TSPopupTypeShareComment: {
            move = 100;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                move = 70;
                if ([UIScreen mainScreen].bounds.size.height == 480) {
                    move = 100;
                }
            }
        } break;
        default:
            NSLog(@"Unsupport type popup!!!");
            break;
    }
    CGRect frame = self.view.frame;
    frame.origin.y = frame.origin.y + move;
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = frame;
    }];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (NSUInteger)application:(UIApplication*)application supportedInterfaceOrientationsForWindow:(UIWindow*)window {
    return UIInterfaceOrientationMaskPortrait;
}

//- (BOOL) shouldAutorotate {
//    return NO;
//}
//
- (BOOL)disablesAutomaticKeyboardDismissal {
    return NO;//ipad UIModalPresentationFormSheet 일때 Keyboard 안내려가는거 처리
}

@end
