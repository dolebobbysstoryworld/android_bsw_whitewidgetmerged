//
//  TSCheckBox.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 2..
//
//

#import <UIKit/UIKit.h>

@class TSCheckBox;
@protocol TSCheckBoxDelegate <NSObject>

@optional
- (BOOL)checkBox:(TSCheckBox*)checkBox willChangeSelectedStatus:(BOOL)selected;
- (void)checkBox:(TSCheckBox*)checkBox didChangeSelectedStatus:(BOOL)selected;

@end

@interface TSCheckBox : UIControl

@property (nonatomic, assign) id< TSCheckBoxDelegate > delegate;
- (void)setImage:(UIImage*)image forState:(UIControlState)state;

@end
