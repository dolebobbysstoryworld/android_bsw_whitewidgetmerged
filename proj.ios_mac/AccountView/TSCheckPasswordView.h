//
//  TSCheckPasswordView.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 23..
//
//

#import "TSPopupView.h"

@interface TSCheckPasswordView : TSPopupView

- (id)initWithCoin:(NSInteger)coin;
@property (nonatomic, readonly) NSString *password;
@property (nonatomic, assign) BOOL keyboardShow;

@end
