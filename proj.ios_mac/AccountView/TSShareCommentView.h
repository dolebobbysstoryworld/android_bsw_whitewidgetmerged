//
//  TSShareCommentView.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 31..
//
//

#import "TSPopupView.h"

FOUNDATION_EXPORT NSString *const kTSShareUserDataKeyTitle;
FOUNDATION_EXPORT NSString *const kTSShareUserDataKeyAuthor;
FOUNDATION_EXPORT NSString *const kTSShareUserDataKeyTarget;
FOUNDATION_EXPORT NSString *const kTSShareUserDataKeyTargetFacebook;
FOUNDATION_EXPORT NSString *const kTSShareUserDataKeyTargetYouTube;


@interface TSShareCommentView : TSPopupView

@property (nonatomic, readonly) NSString *comment;
@property (nonatomic, strong) NSString *hashTag;
@property (nonatomic, strong) NSString *sharetargent;

- (id)initWithBookTitle:(NSString*)title authorName:(NSString*)name target:(NSString*)target;

@end
