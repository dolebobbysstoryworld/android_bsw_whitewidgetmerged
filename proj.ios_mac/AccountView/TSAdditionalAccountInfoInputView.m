//
//  TSAdditionalAccountInfoInputView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 11. 28..
//
//

#import "TSAdditionalAccountInfoInputView.h"
#import "ResolutionManager.h"
#import "TSTextField.h"
#import "UIImage+PathExtention.h"
#import "UIColor+RGB.h"
#import "TSRadioButton.h"
#import "TSPickerView.h"
#import "TSDolesNet.h"
#import "TSMessageView.h"
#import "TSToastView.h"

#import "MixpanelHelper.h"

@interface TSAdditionalAccountInfoInputView () < TSTextFieldDelegate, TSRadioButtonDelegate, TSPopupViewDelegate, TSNetDelegate >

@property (nonatomic, strong) TSTextField *countryTextField;
@property (nonatomic, strong) NSArray *countrys;
@property (nonatomic, strong) NSArray *countryCodes;
@property (nonatomic, assign) NSInteger countryIndex;
@property (nonatomic, strong) TSNet *net;

@property (nonatomic, strong) TSTextField *cityTextFiled;
@property (nonatomic, strong) NSArray *citys;
@property (nonatomic, assign) NSInteger cityIndex;

@property (nonatomic, strong) TSTextField *birthDayTextFiled;
@property (nonatomic, strong) NSArray *birthYears;
@property (nonatomic, assign) NSInteger birthDayIndex;

@property (nonatomic, strong) TSRadioButton *maleRadioButton;
@property (nonatomic, strong) TSRadioButton *femaleRadioButton;

@end

#define LAYOUT_MAINVIEW_WIDTH           508/2

#define TAG_COUNTRY_TEXTFIELD           701001
#define TAG_CITY_TEXTFIELD              701002
#define TAG_BIRTHDAY_TEXTFIELD          701003

#define TAG_MALE_RADIOBUTTON            702003
#define TAG_FEMALE_RADIOBUTTON          702004

#define TAG_COUNTRY_POPUP               703001
#define TAG_CITY_POPUP                  703002
#define TAG_BIRTHDAY_POPUP              703003

#define TAG_NET_PROVINCE                704001

@implementation TSAdditionalAccountInfoInputView

- (instancetype)initWithFrame:(CGRect)frame {
    return [self initWithFrame:frame countryCode:nil];
}

- (instancetype)initWithFrame:(CGRect)frame countryCode:(NSString*)code {
    self = [super initWithFrame:frame];
    if (self) {
//        self.backgroundColor = [UIColor greenColor];
        CGFloat currentY = 0;
        self.countryTextField =
        [[TSTextField alloc] initWithFrame:CGRectMake(0, currentY, LAYOUT_MAINVIEW_WIDTH, 70/2)
                                      icon:@"input_city_icon.png" placeholder:nil editable:NO];
        self.countryTextField.tag = TAG_COUNTRY_TEXTFIELD;
        self.countryTextField.delegate = self;
        [self addSubview:self.countryTextField];
        
        self.countrys = [TSPickerView countrys];
        self.countryCodes = [TSPickerView countryCodes];

        self.countryIndex = 3;//Philippines default
        if (code)
        {
            self.countryCode = code;
        }
        else
        {
            NSString *countryCode = [[NSLocale currentLocale] objectForKey:NSLocaleCountryCode];
            if ([countryCode isEqualToString:@"JP"])
            {
                self.countryIndex = 0;
            }
            else if ([countryCode isEqualToString:@"KR"])
            {
                self.countryIndex = 1;
            }
            else if ([countryCode isEqualToString:@"NZ"])
            {
                self.countryIndex = 2;
            }
            else if ([countryCode isEqualToString:@"SG"])
            {
                self.countryIndex = 4;
            }
            else if ([countryCode isEqualToString:@"158"])
            {
                self.countryIndex = 5;
            }
        }
        
        //hans added
        [self loadCities];
        
        self.countryTextField.text = [self.countrys objectAtIndex:self.countryIndex];
        currentY = currentY + 70/2 + 22/2;//add title label height & margin
        
        self.cityTextFiled =
        [[TSTextField alloc] initWithFrame:CGRectMake(0, currentY, LAYOUT_MAINVIEW_WIDTH, 70/2)
                                      icon:@"input_city_2_icon.png"
                               placeholder:NSLocalizedString(@"PF_SIGNUP_TEXTFIELD_CITY", @"City") editable:YES];
        self.cityTextFiled.tag = TAG_CITY_TEXTFIELD;
//        self.cityTextFiled.enabled = NO;
        self.cityTextFiled.delegate = self;
        [self addSubview:self.cityTextFiled];
        self.cityIndex = -1;
        
        currentY = currentY + 70/2 + 22/2;//add city textfield height & margin
        
        self.birthDayTextFiled =
        [[TSTextField alloc] initWithFrame:CGRectMake(0, currentY, LAYOUT_MAINVIEW_WIDTH, 70/2)
                                      icon:@"input_birthday_icon.png"
                               placeholder:NSLocalizedString(@"PF_SIGNUP_TEXTFIELD_BIRTHYEAR", @"Birthday") editable:NO];
        self.birthDayTextFiled.tag = TAG_BIRTHDAY_TEXTFIELD;
        self.birthDayTextFiled.delegate = self;
        [self addSubview:self.birthDayTextFiled];
        self.birthYears = [TSPickerView birthYearsData];
        self.birthDayIndex = [self.birthYears count] -1;
        
        currentY = currentY + 70/2 + deviceValueA(44, 44, 22)/2;//add birthday textfield height & margin
        
        self.maleRadioButton = [TSRadioButton radioButtonWithTitle:NSLocalizedString(@"PF_SIGNUP_RADIOTITLE_MALE", @"Male")
                                                             Frame:CGRectMake(0, currentY, 58/2, 60/2)];
        self.maleRadioButton.tag = TAG_MALE_RADIOBUTTON;
        self.maleRadioButton.delegate = self;
        [self addSubview:self.maleRadioButton];
        
        self.femaleRadioButton = [TSRadioButton radioButtonWithTitle:NSLocalizedString(@"PF_SIGNUP_RADIOTITLE_FEMALE", @"Female")
                                                               Frame:CGRectMake((58+18+168+20)/2, currentY, 58/2, 60/2)];
        self.femaleRadioButton.tag = TAG_FEMALE_RADIOBUTTON;
        self.femaleRadioButton.delegate = self;
        [self addSubview:self.femaleRadioButton];
        
        CGFloat viewHeight = currentY + 60/2;
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, viewHeight);

        /*------old code--------/
        NSString *currentCode = [self.countryCodes objectAtIndex:self.countryIndex];
        self.net = [[TSNetProvince alloc] initWithCountryCode:currentCode];
        self.net.delegate = self;
        self.net.tag = TAG_NET_PROVINCE;
        [self.net start];
         */
    }
    return self;
}

//hans added
- (void) loadCities
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"cities" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSDictionary *cities = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    self.citys = [NSArray arrayWithObjects:[cities valueForKey:@"Japan"], [cities valueForKey:@"Republic of Korea"], [cities valueForKey:@"New Zealand"], [cities valueForKey:@"Philippines"], [cities valueForKey:@"Singapore"], [cities valueForKey:@"Australia"], nil];
}

- (void)tstextFieldTouched:(TSTextField *)textField
{
    TSPickerView *pickView = nil;
//    textField.enabled = NO;
    switch (textField.tag) {
        case TAG_COUNTRY_TEXTFIELD: {
            pickView = [[TSPickerView alloc] initWithDataSource:self.countrys
                                                          title:NSLocalizedString(@"PF_SIGNUP_PUPUP_TITLE_SELECT_COUNTRY", @"Enter the country")];
            pickView.tag = TAG_COUNTRY_POPUP;
            pickView.selectedIndex = self.countryIndex;
        } break;
        case TAG_CITY_TEXTFIELD: {
//            NSLog(@"~~~~~~~~~~~cities: %@", self.citys);
//            pickView = [[TSPickerView alloc] initWithDataSource:[self.citys objectAtIndex:self.countryIndex]
//                                                          title:NSLocalizedString(@"PF_SIGNUP_PUPUP_TITLE_SELECT_COUNTRY", @"Enter the country")];
//            pickView.tag = TAG_CITY_POPUP;
//            pickView.selectedIndex = self.cityIndex>=0?self.cityIndex:0;
        } break;
        case TAG_BIRTHDAY_TEXTFIELD: {
            pickView = [[TSPickerView alloc] initWithDataSource:self.birthYears
                                                          title:NSLocalizedString(@"PF_SIGNUP_POPUP_TITLE_BIRTHYEAR", @"Set birth year")];
            pickView.tag = TAG_BIRTHDAY_POPUP;
            pickView.selectedIndex = self.birthDayIndex;
        } break;
            
        default:
            break;
    }
    pickView.buttonLayout = TSPopupButtonLayoutCancelSet;
    pickView.delegate = self;
    [pickView showInView:self.superview];
}

- (void)popview:(TSPopupView *)view didClickedButton:(TSPopupButtonType)type {
    if (type == TSPopupButtonTypeSet) {
        switch (view.tag) {
            case TAG_COUNTRY_POPUP: {
                if (self.countryIndex != ((TSPickerView*)view).selectedIndex) {
                    self.countryIndex = ((TSPickerView*)view).selectedIndex;
                    self.countryTextField.text = [self.countrys objectAtIndex:self.countryIndex];
                    self.cityTextFiled.text = nil;
                    self.cityIndex = -1;
                    if (self.delegate && [self.delegate respondsToSelector:@selector(changedTargetData:dataObject:)]) {
                        [self.delegate changedTargetData:kTSAccountDataCountryCode dataObject:[NSNumber numberWithInteger:self.countryIndex]];
                    }
                    /*--------old code------/
                    NSString *countryCode = [self.countryCodes objectAtIndex:self.countryIndex];
                    if ([countryCode isEqualToString:@"Other"] == NO)
                    {//display가 아니다....
                        self.cityTextFiled.enabled = NO;
                        if (self.net)
                        {
                            self.net = nil;
                        }
                        self.net = [[TSNetProvince alloc] initWithCountryCode:countryCode];
                        self.net.delegate = self;
                        self.net.tag = TAG_NET_PROVINCE;
                        [self.net start];
                        return;
                    }
                     */
                }
            } break;
            case TAG_CITY_POPUP: {
                self.cityIndex = ((TSPickerView*)view).selectedIndex;
                //self.cityTextFiled.text = [((TSNetProvince*)self.net).provinces objectAtIndex:self.cityIndex];
                //hans added
                self.cityTextFiled.text = [[self.citys objectAtIndex:self.countryIndex] objectAtIndex:self.cityIndex];
            } break;
            case TAG_BIRTHDAY_POPUP: {
                self.birthDayIndex = ((TSPickerView*)view).selectedIndex;
                self.birthDayTextFiled.text = [self.birthYears objectAtIndex:self.birthDayIndex];
            } break;
            default:
                break;
        }
    } else {
        switch (view.tag) {
            case TAG_COUNTRY_POPUP: {
//                if ([self.cityTextFiled.text length] <= 0) {
//                    self.cityTextFiled.enabled = YES;
//                    self.cityTextFiled.text = @"";
//                    self.countryIndex = -1;
//                    self.cityIndex = -1;
//                }
            } break;
            case TAG_CITY_POPUP: {
                //                self.isCitySelected = NO;
//                self.cityTextFiled.enabled = YES;
//                self.cityTextFiled.text = @"";
//                self.countryIndex = -1;
//                self.cityIndex = -1;
            } break;
            case TAG_BIRTHDAY_POPUP: {
//                self.birthDayTextFiled.enabled = YES;
//                self.birthDayTextFiled.text = @"";
//                self.birthDayIndex = [self.birthYears count] -1;
            } break;
            default:
                break;
        }
    }
    
    [MixpanelHelper logInputOnRegistrationField];
}

#pragma mark - TSNetDelegate
- (void)net:(TSNet*)netObject didStartProcess:(TSNetResultType)result {
    switch (netObject.tag) {
        case TAG_NET_PROVINCE: {
            self.cityTextFiled.showActivity = YES;
            self.cityTextFiled.enabled = NO;
        } break;
        default: { } break;
    }
}

- (void)net:(TSNet*)netObject didEndProcess:(TSNetResultType)result {
    switch (netObject.tag) {
        case TAG_NET_PROVINCE: {
            self.cityTextFiled.showActivity = NO;
            if (!result)
            {
                self.cityTextFiled.enabled = YES;
                self.citys = ((TSNetProvince*)self.net).provinces;
                if (self.city)
                {
                    NSString *cityName;
                    NSInteger index = 0;
                    for (cityName in self.citys)
                    {
                        if ([cityName isEqualToString:self.city])
                        {
                            self.cityIndex = index;
                            return;
                        }
                        index++;
                    }
                }
            }
            else
            {//tuilise_todo : 오류 결과다. 맞는 처리 필요
                self.cityTextFiled.text = nil;
                TSMessageView *messageView =
                [[TSMessageView alloc] initWithMessage:[NSString stringWithFormat:@"Server Error\n%@.", [netObject serverErrorString]]
                                                  icon:TSPopupIconTypeWarning];
                messageView.buttonLayout = TSPopupButtonLayoutOK;
                [messageView showInView:self.superview];
            }
        } break;
        default: { } break;
    }
}

- (void)net:(TSNet*)netObject didFailWithError:(TSNetResultType)result {
    switch (netObject.tag) {
        case TAG_NET_PROVINCE: {
            self.cityTextFiled.showActivity = NO;
            self.cityTextFiled.enabled = NO;
            switch (result) {
                case TSNetErrorNetworkDisable:
                case TSNetErrorTimeout: {
                    TSToastView *toast = [[TSToastView alloc] initWithString:NSLocalizedString(@"CM_NET_WARNING_NETWORK_UNSTABLE", @"The network is unstable.\nPlease try again")];
                    [toast showInViewWithAnimation:self.superview];
                } break;
                default: {
                } break;
            }
        } break;
        default: { } break;
    }
}

- (void)radioButton:(TSRadioButton *)radioButton didChangeSelectedStatus:(BOOL)selected {
    switch (radioButton.tag) {
        case TAG_MALE_RADIOBUTTON: {
            if (selected) {
                self.femaleRadioButton.selected = NO;
            }
        } break;
        case TAG_FEMALE_RADIOBUTTON: {
            if (selected) {
                self.maleRadioButton.selected = NO;
            }
        } break;
        default: { } break;
    }
    
    [MixpanelHelper logInputOnRegistrationField];
}

- (NSString*)birthday {
    NSLog(@"birthday string : %@", self.birthDayTextFiled.text);
    if ([self.birthDayTextFiled.text length] <= 0) {
        return @"9999";
    }
    return self.birthDayTextFiled.text;
}

- (void)setBirthday:(NSString *)birthday {
    if ([birthday isEqualToString:@"9999"] == YES) {
        return;
    }
    self.birthDayTextFiled.text = birthday;
    self.birthDayIndex = [birthday integerValue] - [[self.birthYears objectAtIndex:0] integerValue];
    NSLog(@"birthDay : %@ / index : %ld", self.birthDayTextFiled.text, (long)self.birthDayIndex);
}

- (NSString*)countryCode {
    if (self.countryIndex == -1) {
        return [self.countryCodes objectAtIndex:[self.countryCodes count]-1];
    }
    return [self.countryCodes objectAtIndex:self.countryIndex];
}

- (void)setCountryCode:(NSString *)countryCode {
    NSString *code;
    NSInteger index = 0;
    for (code in self.countryCodes) {
        if ([countryCode isEqualToString:code]) {
            self.countryIndex = index;
            return;
        }
        index++;
    }
}

- (NSString*)city {
    return self.cityTextFiled.text;
}

- (void)setCity:(NSString *)city {
    self.cityTextFiled.text = city;
    /*-------old code-------
    if (self.citys) {
        NSString *cityName;
        NSInteger index = 0;
        for (cityName in self.citys) {
            if ([cityName isEqualToString:city]) {
                self.cityIndex = index;
                return;
            }
            index++;
        }
    }
    */
}

- (NSInteger)gender {
    if (self.maleRadioButton.selected == YES) {
        return 1;
    }
    
    if (self.femaleRadioButton.selected == YES) {
        return 2;
    }
    
    return 99;
}

- (void)setGender:(NSInteger)gender {
    switch (gender) {
        case 1:
            self.maleRadioButton.selected = YES;
            self.femaleRadioButton.selected = NO;
            break;
        case 2:
            self.maleRadioButton.selected = NO;
            self.femaleRadioButton.selected = YES;
            break;
        default:
            self.maleRadioButton.selected = NO;
            self.femaleRadioButton.selected = NO;
            break;
    }
}

- (BOOL)isEdited {
    if ([self.countryTextField.text length] > 0) {
        return YES;
    }
    
    if ([self.cityTextFiled.text length] > 0) {
        return YES;
    }
    
    if ([self.birthDayTextFiled.text length] > 0) {
        return YES;
    }
    
    if (self.maleRadioButton.selected == YES || self.femaleRadioButton.selected == YES) {
        return YES;
    }
    
    return NO;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
