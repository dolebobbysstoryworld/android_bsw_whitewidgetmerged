//
//  TSRadioButton.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 2..
//
//

#import "TSRadioButton.h"
#import "ResolutionManager.h"
#import "UIColor+RGB.h"
#import "UIImage+PathExtention.h"

@interface TSRadioButton ()

@property (nonatomic, strong) UIImageView *viewForNormalStatus;
@property (nonatomic, strong) UIImageView *viewForSelectedStatus;
@property (nonatomic, strong) UIImageView *viewForDisableStatus;
@property (nonatomic, strong) UILabel *titleLabel;

@end

#define DELTA_MARGIN            9

@implementation TSRadioButton

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setImage:(UIImage*)image forState:(UIControlState)state {
    switch (state) {
        case UIControlStateNormal: {
            self.viewForNormalStatus = [[UIImageView alloc] initWithImage:image];
            [self addSubview:self.viewForNormalStatus];
        } break;
        case UIControlStateSelected: {
            self.viewForSelectedStatus = [[UIImageView alloc] initWithImage:image];
        } break;
        case UIControlStateDisabled: {
            self.viewForDisableStatus = [[UIImageView alloc] initWithImage:image];
        } break;
        default: { } break;
    }
    [self adjustFrameSize];
}

- (void)adjustFrameSize {
    CGPoint origin = self.frame.origin;
    CGSize size = self.viewForNormalStatus.frame.size;
    if (self.titleLabel) {
        size.width = self.viewForNormalStatus.frame.size.width + DELTA_MARGIN + self.titleLabel.frame.size.width;
    }
    self.frame = CGRectMake(origin.x, origin.y, size.width, size.height);
}

- (void)setTitle:(NSString *)string {
    CGRect imageFrame = self.viewForNormalStatus.frame;
    CGPoint lefttop =  CGPointMake(imageFrame.origin.x + imageFrame.size.width + DELTA_MARGIN, imageFrame.origin.y);
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(lefttop.x, lefttop.y, 0, imageFrame.size.height)];
    self.titleLabel.text = string;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.titleLabel.font = [UIFont fontWithName:kDefaultFontName size:17];
    self.titleLabel.textColor = RGBH(@"#4f2c00");
    [self.titleLabel sizeToFit];
    self.titleLabel.numberOfLines = 0;
    CGRect labelFrame = self.titleLabel.frame;
    labelFrame.size.height = self.viewForNormalStatus.frame.size.height;
    self.titleLabel.frame = labelFrame;
//    self.titleLabel.backgroundColor = [UIColor redColor];
    [self adjustFrameSize];
    [self addSubview:self.titleLabel];
}

- (void)setSelected:(BOOL)selected {
    if (self.selected == selected) {
        return;
    }
    
    if (!self.enabled)
        return;
    
    super.selected = selected;
    if (self.selected) {
        [self.viewForNormalStatus removeFromSuperview];
        [self addSubview:self.viewForSelectedStatus];
    } else {
        [self.viewForSelectedStatus removeFromSuperview];
        [self addSubview:self.viewForNormalStatus];
    }
}

- (void)setEnabled:(BOOL)enabled {
    if (self.enabled == enabled) {
        return;
    }
    super.enabled = enabled;
    
    if (self.enabled) {
        [self.viewForDisableStatus removeFromSuperview];
        [self setSelected:self.selected];
    } else {
        if (self.viewForDisableStatus) {
            if (self.selected) {
                [self.viewForSelectedStatus removeFromSuperview];
            } else {
                [self.viewForNormalStatus removeFromSuperview];
            }
            [self addSubview:self.viewForDisableStatus];
        }
    }
}

#pragma mark - touches
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    if (!self.enabled)
        return;
    
    [super touchesBegan:touches withEvent:event];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    if (!self.enabled)
        return;
    
    [super touchesCancelled:touches withEvent:event];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    if (!self.enabled)
        return;
    
    self.alpha = 1.0f;
    
    if ([self superview]){
        UITouch *touch = [touches anyObject];
        CGPoint point = [touch locationInView:[self superview]];
//        CGRect validTouchArea = CGRectMake((self.frame.origin.x - 5),
//                                           (self.frame.origin.y - 10),
//                                           (self.frame.size.width + 5),
//                                           (self.frame.size.height + 10));
        if (CGRectContainsPoint(self.frame, point)){
            if (!self.selected) {
                self.selected = !self.selected;
                if (self.delegate && [self.delegate respondsToSelector:@selector(radioButton:didChangeSelectedStatus:)]) {
                    [self.delegate radioButton:self didChangeSelectedStatus:self.selected];
                }
            }
        }
    }
    
    [super touchesEnded:touches withEvent:event];
}

+ (TSRadioButton*)radioButtonWithTitle:(NSString *)string Frame:(CGRect)frame {
    TSRadioButton *radioButton = [[TSRadioButton alloc] initWithFrame:frame];
    [radioButton setImage:[UIImage imageNamedEx:@"radio_off.png" ignore:YES] forState:UIControlStateNormal];
    [radioButton setImage:[UIImage imageNamedEx:@"radio_on.png" ignore:YES] forState:UIControlStateSelected];
    [radioButton setTitle:string];
    return radioButton;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
