//
//  UILabel+DolesUI.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 24..
//
//

#import "UILabel+DolesUI.h"
#import "ResolutionManager.h"
#import "UIColor+RGB.h"
#import "UIImage+PathExtention.h"

@implementation UILabel (DolesUI)
+ (UIView*)viewForLoginIDLabel:(CGRect)frame {
    return [UILabel viewForLoginIDLabel:frame ID:[[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_LOGINID]];
}

+ (UIView*)viewForLoginIDLabel:(CGRect)frame ID:(NSString *)userID {
    UIFont *font = [UIFont fontWithName:kDefaultBoldFontName size:16];
    CGSize stringSize = [userID sizeWithAttributes:@{ NSFontAttributeName : font }];

    CGRect labelFrame = CGRectZero;
    if ( stringSize.width <= frame.size.width ) {
        labelFrame.size.width = stringSize.width;
        labelFrame.size.height = frame.size.height;
    } else {
        labelFrame.size.width = frame.size.width-(17+6);
        labelFrame.size.height = frame.size.height;
    }

    UILabel *idLabel = [[UILabel alloc] initWithFrame:labelFrame];
    idLabel.font = [UIFont fontWithName:kDefaultBoldFontName size:16];
    idLabel.textAlignment = NSTextAlignmentCenter;
    idLabel.textColor = RGBH(@"#4f2c00");
    idLabel.numberOfLines = 1;//tuilise_todo : 두줄이 되려면 가로 가이드가 2px 좀더 커야 한다.
    idLabel.text = userID;
//    idLabel.backgroundColor = [UIColor redColor];
    if ( stringSize.width <= frame.size.width ) {
        idLabel.adjustsFontSizeToFitWidth = NO;
    } else {
        idLabel.adjustsFontSizeToFitWidth = YES;
    }

    UIImageView *facebookIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"login_via_facebook.png" ignore:YES]];
    CGRect imageFrame = facebookIcon.frame;
    imageFrame.origin.y = (frame.size.height - imageFrame.size.height)/2;
    facebookIcon.frame = imageFrame;
    
    CGRect viewFrame = CGRectZero;
    viewFrame.size.width = 17+6+labelFrame.size.width;
    viewFrame.size.height = frame.size.height;
    
    labelFrame = idLabel.frame;
    labelFrame.origin.x = 17+6;
    idLabel.frame = labelFrame;
    
    UIView *view = [[UIView alloc] initWithFrame:viewFrame];
    
//    NSLog(@"viewFrame Frame before center : %@", NSStringFromCGRect(view.frame));
    CGPoint viewCenter = CGPointMake(CGRectGetMidX(frame), CGRectGetMidY(frame));
    view.center = viewCenter;
//    view.backgroundColor = [UIColor yellowColor];
//    view.alpha = 0.6f;

    [view addSubview:facebookIcon];
    [view addSubview:idLabel];
    
//    NSLog(@"Original Frame : %@", NSStringFromCGRect(frame));
//    NSLog(@"labelFrame Frame : %@", NSStringFromCGRect(idLabel.frame));
//    NSLog(@"imageFrame Frame : %@", NSStringFromCGRect(facebookIcon.frame));
//    NSLog(@"viewFrame Frame : %@", NSStringFromCGRect(view.frame));
    return view;
}

@end
