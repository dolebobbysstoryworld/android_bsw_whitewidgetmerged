//
//  TSAccountSigninView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 2..
//
//

#import "TSAccountSigninView.h"
#import "UIImage+PathExtention.h"
#import "TSTextField.h"
#import "TSRadioButton.h"
#import "TSCheckBox.h"
#import "UIColor+RGB.h"
#import "UIButton+DolesUI.h"
#import "TSDolesNet.h"
#import "TSPickerView.h"
#import "TSAccountAgreementView.h"
#import "TSMessageView.h"
#import "TSToastView.h"
#import "TSProxyHelper.h"

#import "MixpanelHelper.h"

#define TAG_BIRTHDAY_TEXTFIELD          10001
#define TAG_BIRTHDAY_POPUP              10002
#define TAG_CITY_TEXTFIELD              10003
#define TAG_CITY_POPUP                  10004
#define TAG_EMAIL_TEXTFIELD             10005
#define TAG_PASSWORD_TEXTFIELD          10006
#define TAG_CPASSWORD_TEXTFIELD         10007
#define TAG_MAIL_RADIOBUTTON            10008
#define TAG_FEMAIL_RADIOBUTTON          10009
#define TAG_NET_PROVINCE                10010
#define TAG_NET_SIGNUP                  10011
#define TAG_COUNTRY_POPUP               10012
#define TAG_SIGNUPFINISH_POPUP          10013
#define TAG_NET_LOGIN                   10014
#define TAG_POPUP_LOGIN_FAIL            10015

@interface TSAccountSigninView () < TSNetDelegate, TSTextFieldDelegate, TSPopupViewDelegate, TSRadioButtonDelegate, TSCheckBoxDelegate >

@property (nonatomic, strong) TSTextField *emailTextFiled;
@property (nonatomic, strong) TSTextField *passwordTextFiled;
@property (nonatomic, strong) TSTextField *confirmTextFiled;
@property (nonatomic, strong) TSTextField *birthDayTextFiled;
@property (nonatomic, assign) NSInteger birthDayIndex;
@property (nonatomic, strong) TSTextField *cityTextFiled;
@property (nonatomic, strong) NSArray *countrys;
@property (nonatomic, strong) NSArray *countryCodes;
@property (nonatomic, assign) NSInteger countryIndex;
@property (nonatomic, assign) NSInteger cityIndex;
@property (nonatomic, assign) BOOL isCitySelected;
@property (nonatomic, strong) TSNet *net;
@property (nonatomic, strong) NSArray *birthYears;
@property (nonatomic, assign) BOOL emailStringReady;
@property (nonatomic, assign) BOOL passwordStringReady;
@property (nonatomic, assign) BOOL conformStringReady;
@property (nonatomic, assign) BOOL birthdayStringReady;
@property (nonatomic, assign) BOOL cityStringReady;
@property (nonatomic, strong) UIButton *signupButton;
@property (nonatomic, strong) TSRadioButton *mailRadioButton;
@property (nonatomic, strong) TSRadioButton *femailRadioButton;
@property (nonatomic, strong) TSCheckBox *agreeCheckBox;
@property (nonatomic, assign) BOOL isLoginFinished;

@end

@implementation TSAccountSigninView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"bg_map_factor.png"]];
        backgroundView.frame = getRectValue(TSViewIndexShare, TSShareSubViewOrgRect);
        [self addSubview:backgroundView];
        
        [self addTextFields];
//        [self addRadioButtons];
        [self addCheckBoxs];
        [self addSignupButton];
        
        self.birthYears = [TSPickerView birthYearsData];
        self.birthDayIndex = [self.birthYears count] -1;
        self.countrys = [TSPickerView countrys];
        self.countryCodes = [TSPickerView countryCodes];
        self.countryIndex = 0;
        self.cityIndex = 0;
        self.isCitySelected = NO;
        self.isLoginFinished = NO;
        self.needPostNotification = NO;
        
        [MixpanelHelper logOpenedSignUp];
    }
    return self;
}

#pragma mark - Textfiled operation
- (void)addTextFields {
    self.emailTextFiled =
    [[TSTextField alloc] initWithFrame:getRectValue(TSViewIndexSignin, TSSigninEmailRect)
                                  icon:@"input_mail_icon.png" placeholder:NSLocalizedString(@"PF_LOGIN_TEXTFIELD_EMAIL", @"ExampleID@dole.com")];
    [self.emailTextFiled setKeyboardType:UIKeyboardTypeEmailAddress];
    self.emailTextFiled.delegate = self;
    self.emailTextFiled.tag = TAG_EMAIL_TEXTFIELD;
    [self addSubview:self.emailTextFiled];
    
    self.passwordTextFiled =
    [[TSTextField alloc] initWithFrame:getRectValue(TSViewIndexSignin, TSSigninPasswordRect)
                                  icon:@"input_password_icon.png" placeholder:NSLocalizedString(@"PF_LOGIN_TEXTFIELD_PASSWORD", @"Password")];
    [self.passwordTextFiled setPasswordType:YES];
    self.passwordTextFiled.delegate = self;
    [self.passwordTextFiled setComment:NSLocalizedString(@"PF_EDITACCOUNT_TEXTFIELD_PASSWORD_CONDITION", @"at least 6 characters")];
    self.passwordTextFiled.tag = TAG_PASSWORD_TEXTFIELD;
    [self addSubview:self.passwordTextFiled];

    self.confirmTextFiled =
    [[TSTextField alloc] initWithFrame:getRectValue(TSViewIndexSignin, TSSigninCPasswordRect)
                                  icon:@"input_password_icon.png" placeholder:NSLocalizedString(@"PF_SIGNUP_TEXTFIELD_PASSWORD_CONFIRM", @"Conform Password")];
    [self.confirmTextFiled setPasswordType:YES];
    self.confirmTextFiled.delegate = self;
    self.confirmTextFiled.tag = TAG_CPASSWORD_TEXTFIELD;
    [self addSubview:self.confirmTextFiled];

//    self.birthDayTextFiled =
//    [[TSTextField alloc] initWithFrame:getRectValue(TSViewIndexSignin, TSSigninBirthRect)
//                                  icon:@"input_birthday_icon.png" placeholder:NSLocalizedString(@"PF_SIGNUP_TEXTFIELD_BIRTHYEAR", @"Birthday") editable:NO];
//    self.birthDayTextFiled.tag = TAG_BIRTHDAY_TEXTFIELD;
//    self.birthDayTextFiled.delegate = self;
//    [self addSubview:self.birthDayTextFiled];
//
    self.cityTextFiled =
    [[TSTextField alloc] initWithFrame:getRectValue(TSViewIndexSignin, TSSigninCityRect)
                                  icon:@"input_city_icon.png"
                           placeholder:NSLocalizedString(@"PF_ACCOUNT_PLACEHOLDER_COUNTRY", @"Country") editable:NO];
    self.cityTextFiled.tag = TAG_CITY_TEXTFIELD;
    self.cityTextFiled.delegate = self;
//    self.cityTextFiled.enabled = NO;
    [self addSubview:self.cityTextFiled];
}

- (void)tstextFieldDidEndEditing:(TSTextField *)textField {
    NSInteger tag = textField.tag;
    switch (tag) {
        case TAG_EMAIL_TEXTFIELD: {
            self.emailStringReady =
            [self.emailTextFiled checkEmailStringValidationWithShowError:
             NSLocalizedString(@"PF_NET_ERROR_EMAIL_ENTER_FULL_ADDRESS", @"Please enter the full email address,\nincluding after the @ sign")];
        } break;
        case TAG_PASSWORD_TEXTFIELD:
        case TAG_CPASSWORD_TEXTFIELD: {
            self.passwordStringReady =
            [self.passwordTextFiled checkPasswordStringValidationWithShowError:
             NSLocalizedString(@"PF_NET_ERROR__PASSWORD_SHORT", @"￼Please enter a password with at least 6 characters")];
            self.conformStringReady =
            [self.confirmTextFiled equalPasswordString:self.passwordTextFiled.text withShowError:
             NSLocalizedString(@"PF_NET_ERROR__PASSWORD_INCORRECT", @"The password is incorrect")];
        } break;
        default: { } break;
    }
    [self checkEnableSigninButton];
}

- (void)tstextFieldTouched:(TSTextField *)textField {
    TSPickerView *pickView = nil;
    if (textField.tag == TAG_BIRTHDAY_TEXTFIELD) {
        pickView = [[TSPickerView alloc] initWithDataSource:self.birthYears
                                                      title:NSLocalizedString(@"PF_SIGNUP_POPUP_TITLE_BIRTHYEAR", @"Set birth year")];
        pickView.tag = TAG_BIRTHDAY_POPUP;
        pickView.selectedIndex = self.birthDayIndex;
    } else {
        if (YES) {//if ([self.cityTextFiled.text length] <= 0) {//아직 국가가 선택되지 않았다.
            pickView = [[TSPickerView alloc] initWithDataSource:self.countrys
                                                          title:NSLocalizedString(@"PF_SIGNUP_PUPUP_TITLE_SELECT_COUNTRY", @"Enter the country")];
            pickView.tag = TAG_COUNTRY_POPUP;
            pickView.selectedIndex = self.countryIndex;
            self.isCitySelected = NO;
        }/* else {//국가는 선택 되었다
            pickView = [[TSPickerView alloc] initWithDataSource:((TSNetProvince*)self.net).provinces
                                                          title:NSLocalizedString(@"PF_SIGNUP_PUPUP_TITLE_SELECT_CITY", @"Enter the city")];
            pickView.tag = TAG_CITY_POPUP;
            pickView.selectedIndex = self.cityIndex;
        }*/
    }
    pickView.buttonLayout = TSPopupButtonLayoutCancelSet;
    pickView.delegate = self;
    [pickView showInView:self];
}

- (void)popview:(TSPopupView *)view didClickedButton:(TSPopupButtonType)type {
    if (type == TSPopupButtonTypeSet) {
        switch (view.tag) {
            case TAG_COUNTRY_POPUP: {
                self.countryIndex = ((TSPickerView*)view).selectedIndex;
                self.cityTextFiled.text = [self.countrys objectAtIndex:self.countryIndex];
//                NSString *countryCode = [self.countryCodes objectAtIndex:self.countryIndex];
//                if ([countryCode isEqualToString:@"Other"] == NO) {//display가 아니다....
//                    self.cityIndex = 0;
//                    self.net = [[TSNetProvince alloc] initWithCountryCode:countryCode];
//                    self.net.delegate = self;
//                    self.net.tag = TAG_NET_PROVINCE;
//                    [self.net start];
//                    self.cityTextFiled.enabled = NO;
//                    return;
//                }
                self.cityTextFiled.enabled = YES;
                self.isCitySelected = YES;
            } break;
            case TAG_CITY_POPUP: {
                self.cityIndex = ((TSPickerView*)view).selectedIndex;
                self.cityTextFiled.text = [((TSNetProvince*)self.net).provinces objectAtIndex:self.cityIndex];
                self.cityTextFiled.enabled = YES;
                self.isCitySelected = YES;
            } break;
            case TAG_BIRTHDAY_POPUP: {
                self.birthDayIndex = ((TSPickerView*)view).selectedIndex;
                self.birthDayTextFiled.text = [self.birthYears objectAtIndex:self.birthDayIndex];
            } break;
            default:
                break;
        }
        [self checkEnableSigninButton];
    } else {
        switch (view.tag) {
            case TAG_COUNTRY_POPUP: {
                if ([self.cityTextFiled.text length] <= 0) {
                    self.isCitySelected = NO;
                    self.cityTextFiled.enabled = YES;
                    self.cityTextFiled.text = @"";
                    self.countryIndex = 0;
                    self.cityIndex = 0;
                    [self checkEnableSigninButton];
                }
            } break;
            case TAG_CITY_POPUP: {
                self.isCitySelected = NO;
                self.cityTextFiled.enabled = YES;
                self.cityTextFiled.text = @"";
                self.countryIndex = 0;
                self.cityIndex = 0;
            } break;
            case TAG_BIRTHDAY_POPUP: {
                
            } break;
            case TAG_SIGNUPFINISH_POPUP: {
                if (self.delegate && [self.delegate respondsToSelector:@selector(closeViewControllerWithView:)]) {
                    [self.delegate closeViewControllerWithView:self];
                }
            } break;
            case TAG_POPUP_LOGIN_FAIL: {
                if (self.delegate && [self.delegate respondsToSelector:@selector(view:Page:userData:)]) {
                    [self.delegate view:self Page:TSViewIndexLogin userData:nil];
                }
            } break;
            default:
                break;
        }
    }
}

#pragma mark - Radio Button operarion
- (void)addRadioButtons {
    self.mailRadioButton = [TSRadioButton radioButtonWithTitle:NSLocalizedString(@"PF_SIGNUP_RADIOTITLE_MALE", @"Male")
                                                         Frame:getRectValue(TSViewIndexSignin, TSSigninMailRadioRect)];
    self.mailRadioButton.tag = TAG_MAIL_RADIOBUTTON;
    self.mailRadioButton.selected = YES;
    self.mailRadioButton.delegate = self;
    [self addSubview:self.mailRadioButton];
    self.femailRadioButton = [TSRadioButton radioButtonWithTitle:NSLocalizedString(@"PF_SIGNUP_RADIOTITLE_FEMALE", @"Female")
                                                           Frame:getRectValue(TSViewIndexSignin, TSSigninFemailRadioRect)];
    self.femailRadioButton.tag = TAG_FEMAIL_RADIOBUTTON;
    self.femailRadioButton.delegate = self;
    [self addSubview:self.femailRadioButton];
}

- (void)radioButton:(TSRadioButton *)radioButton didChangeSelectedStatus:(BOOL)selected {
    switch (radioButton.tag) {
        case TAG_MAIL_RADIOBUTTON: {
            if (selected) {
                self.femailRadioButton.selected = NO;//tuilise_todo : 나중에 group 처리 살계해 보자.
            }
        } break;
        case TAG_FEMAIL_RADIOBUTTON: {
            if (selected) {
                self.mailRadioButton.selected = NO;
            }
        } break;
        default: { } break;
    }
    [self checkEnableSigninButton];
}

#pragma mark - checkBox operation
- (void)addCheckBoxs {
    self.agreeCheckBox = [[TSCheckBox alloc] initWithFrame:getRectValue(TSViewIndexSignin, TSSigninAgreeCheckRect)];
    self.agreeCheckBox.delegate = self;
    [self.agreeCheckBox setImage:[UIImage imageNamedEx:@"checkbox_off.png" ignore:YES] forState:UIControlStateNormal];
    [self.agreeCheckBox setImage:[UIImage imageNamedEx:@"checkbox_on.png" ignore:YES] forState:UIControlStateSelected];
    [self addSubview:self.agreeCheckBox];
    
    UILabel *agreeLabel = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexSignin, TSSigninAgreeLabelRect)];
    agreeLabel.font = [UIFont fontWithName:kDefaultFontName size:13];
    agreeLabel.textColor = RGBH(@"#4f2c00");
    agreeLabel.numberOfLines = 0;
    agreeLabel.text = NSLocalizedString(@"PF_SIGNUP_CHECKBOX_TITLE_AGREE", @"Agree to the Terms\n& Conditions and Privacy Policy");
    [self addSubview:agreeLabel];
}

- (void)checkBox:(TSCheckBox *)checkBox didChangeSelectedStatus:(BOOL)selected {
    if (checkBox) {//편법으로 처리해 보자.
        self.hidden = YES;
        TSAccountAgreementView *agreeView = [[TSAccountAgreementView alloc] initWithFrame:getRectValue(TSViewIndexShare, TSShareSubViewRect)];
        agreeView.agreeCheckBox = checkBox;
        agreeView.tag = 98989898;
        [self.superview addSubview:agreeView];
        self.closeButton.hidden = YES;
    } else {
        TSAccountAgreementView *agreeView = (TSAccountAgreementView *)[self.superview viewWithTag:98989898];
        [agreeView removeFromSuperview];
        self.hidden = NO;
        self.closeButton.hidden = NO;
    }
    [self checkEnableSigninButton];
}

#pragma mark - signup button operation
- (void)addSignupButton {
    self.signupButton = [UIButton buttonWithType:TSDolsTypeOK Frame:getRectValue(TSViewIndexSignin, TSSigninSignUpBtnRect)
                                           Title:NSLocalizedString(@"PF_SIGNUP_BUTTON_TITLE_SIGNUP", @"Sign up")];
    [self.signupButton addTarget:self action:@selector(selectorSignupButton:) forControlEvents:UIControlEventTouchUpInside];
    self.signupButton.enabled = NO;
    [self addSubview:self.signupButton];
}

- (IBAction)selectorSignupButton:(id)sender {
    NSLog(@"selectorSignupButton");
    self.net = [[TSNetSignup alloc] initWithEmail:self.emailTextFiled.text
                                         Password:self.passwordTextFiled.text
                                           Gender:99//self.mailRadioButton.selected?1:2
                                         BirthDay:@"9999"//self.birthDayTextFiled.text
                                      AddressMain:self.cityTextFiled.text
                                      countryCode:[self.countryCodes objectAtIndex:self.countryIndex]];
    self.net.delegate = self;
    self.net.tag = TAG_NET_SIGNUP;
    [self.net start];
}

- (void)checkEnableSigninButton {
    if (self.emailStringReady == YES && self.passwordStringReady == YES && self.conformStringReady == YES &&
        /*([self.birthDayTextFiled.text length] > 0) && */(self.isCitySelected == YES) &&//([self.cityTextFiled.text length] > 0) &&
        /*(self.mailRadioButton.selected == YES || self.femailRadioButton.selected ) &&*/ self.agreeCheckBox.selected == YES ) {
        self.signupButton.enabled = YES;
    } else {
        self.signupButton.enabled = NO;
    }
}

#pragma mark - TSNetDelegate
- (void)net:(TSNet*)netObject didStartProcess:(TSNetResultType)result {
    switch (netObject.tag) {
        case TAG_NET_PROVINCE: {
            self.cityTextFiled.showActivity = YES;
        } break;
        case TAG_NET_SIGNUP: {
            [self.signupButton showActivity:YES];
            self.signupButton.enabled = NO;
        } break;
        default: { } break;
    }
}

- (void)net:(TSNet*)netObject didEndProcess:(TSNetResultType)result {
    switch (netObject.tag) {
        case TAG_NET_PROVINCE: {
            self.cityTextFiled.showActivity = NO;
            if (!result) {
//                self.cityTextFiled.showActivity = NO;
//                self.cityTextFiled.enabled = YES;
//                self.cityTextFiled.text = [((TSNetProvince*)self.net).provinces objectAtIndex:0];//각 국의 도시 정보 처음값으로 초기화
                
                TSPickerView *pickView = [[TSPickerView alloc] initWithDataSource:((TSNetProvince*)self.net).provinces
                                                                            title:NSLocalizedString(@"PF_SIGNUP_PUPUP_TITLE_SELECT_CITY", @"Enter the city")];
                pickView.tag = TAG_CITY_POPUP;
                pickView.selectedIndex = self.cityIndex;
                pickView.buttonLayout = TSPopupButtonLayoutCancelSet;
                pickView.delegate = self;
                [pickView showInView:self];
            } else {//tuilise_todo : 오류 결과다. 맞는 처리 필요
                self.cityTextFiled.text = nil;
                TSMessageView *messageView =
                [[TSMessageView alloc] initWithMessage:[NSString stringWithFormat:@"Server Error\n%@.", [netObject serverErrorString]]
                                                  icon:TSPopupIconTypeWarning];
                messageView.buttonLayout = TSPopupButtonLayoutOK;
                [messageView showInView:self];
            }
        } break;
        case TAG_NET_SIGNUP: {
            if (!result) {
                self.net = [[TSNetLogin alloc] initWithID:self.emailTextFiled.text Password:self.passwordTextFiled.text];
                self.net.tag = TAG_NET_LOGIN;
                self.net.delegate = self;
                [self.net start];
//                TSMessageView *messageView =
//                [[TSMessageView alloc] initWithMessage:@"Success New account!!\nplease login." icon:TSPopupIconTypeWarning];
//                messageView.buttonLayout = TSPopupButtonLayoutOK;
//                messageView.tag = TAG_SIGNUPFINISH_POPUP;
//                messageView.delegate = self;
//                [messageView showInView:self];
            } else {//tuilise_todo : 오류 결과다. 맞는 처리 필요
                self.signupButton.enabled = YES;
                [self.signupButton showActivity:NO];
                [self.emailTextFiled setError:NSLocalizedString(@"PF_LOGIN_POPUP_ERROR_NOTAVAILABLE_EMAIL", @"This email address is already in use or not available")];
//                TSMessageView *messageView =
//                [[TSMessageView alloc] initWithMessage:[NSString stringWithFormat:@"회원 가입 실패\n%@", [netObject serverErrorString]] icon:TSPopupIconTypeWarning];
//                messageView.buttonLayout = TSPopupButtonLayoutOK;
//                [messageView showInView:self];
            }
        } break;
        case TAG_NET_LOGIN: {
            if (!result) {
                self.isLoginFinished = YES;
//                [[NSNotificationCenter defaultCenter] postNotificationName:kTSNotifivationLoginDidFinished object:nil userInfo:@{@"originlayer": [NSNumber numberWithInteger:self.originLayer]}];
                if (self.delegate && [self.delegate respondsToSelector:@selector(closeViewControllerWithView:)]) {
                    self.needPostNotification = YES;
                    [self.delegate closeViewControllerWithView:self];
                }
            } else {
//                switch (netObject.returnCode) {
//                    case 90105: {
//                        [self.emailTextFiled setError:@"The email does not exist or is incorrect"];
//                    } break;
//                    case 90113: {
//                        [self.emailTextFiled setError:@"This account is signed up with Facebook"];
//                    } break;
//                    case 90106: {
//                        [self.passwordTextFiled setError:@"The password is incorrect"];
//                    } break;
//                    default: {
//                        TSMessageView *messageView =
//                        [[TSMessageView alloc] initWithMessage:[NSString stringWithFormat:@"로그인 실패\n%@", [netObject serverErrorString]] icon:TSPopupIconTypeWarning];
//                        messageView.buttonLayout = TSPopupButtonLayoutOK;
//                        [messageView showInView:self];
//                    } break;
//                }
                TSMessageView *messageView =
                [[TSMessageView alloc] initWithMessage:[NSString stringWithFormat:@"Server Error\n%@.", [netObject serverErrorString]]
                                                  icon:TSPopupIconTypeWarning];
                messageView.buttonLayout = TSPopupButtonLayoutOK;
                [messageView showInView:self];
            }
        } break;
        default: { } break;
    }
}

- (void)net:(TSNet*)netObject didFailWithError:(TSNetResultType)result {
    switch (netObject.tag) {
        case TAG_NET_PROVINCE: {
            self.cityTextFiled.showActivity = NO;
            switch (result) {
                case TSNetErrorNetworkDisable:
                case TSNetErrorTimeout: {
                    TSToastView *toast = [[TSToastView alloc] initWithString:NSLocalizedString(@"CM_NET_WARNING_NETWORK_UNSTABLE", @"The network is unstable.\nPlease try again")];
                    [toast showInViewWithAnimation:self];
                } break;
                default: {
                } break;
            }
        } break;
        case TAG_NET_LOGIN: {
            TSMessageView *messageView =
            [[TSMessageView alloc] initWithMessage:NSLocalizedString(@"CM_NET_WARNING_NETWORK_UNSTABLE", @"The network is unstable.\nPlease try again")
                                              icon:TSPopupIconTypeWarning];
            messageView.buttonLayout = TSPopupButtonLayoutOK;
            messageView.delegate = self;
            messageView.tag = TAG_POPUP_LOGIN_FAIL;
            [messageView showInView:self];
        } break;
        case TAG_NET_SIGNUP: {
            self.signupButton.enabled = YES;
            [self.signupButton showActivity:NO];
            TSMessageView *messageView =
            [[TSMessageView alloc] initWithMessage:NSLocalizedString(@"CM_NET_WARNING_NETWORK_UNSTABLE", @"The network is unstable.\nPlease try again")
                                              icon:TSPopupIconTypeWarning];
            messageView.buttonLayout = TSPopupButtonLayoutOK;
            [messageView showInView:self];
        } break;
        default: { } break;
    }
}

- (BOOL)isEdited {
    if (self.isLoginFinished == YES) {
        return !self.isLoginFinished;
    }
    
    if ([self.emailTextFiled.text length] > 0) {
        return YES;
    }
    if ([self.passwordTextFiled.text length] > 0) {
        return YES;
    }
    if ([self.confirmTextFiled.text length] > 0) {
        return YES;
    }
    if ([self.birthDayTextFiled.text length] > 0) {
        return YES;
    }
    if ([self.cityTextFiled.text length] > 0) {
        return YES;
    }
    
    return NO;
}

- (void)postLoginNotification:(NSInteger)recallType {
    [[NSNotificationCenter defaultCenter] postNotificationName:kTSNotifivationLoginDidFinished
                                                        object:nil
                                                      userInfo:@{@"originlayer": [NSNumber numberWithInteger:self.originLayer],
                                                                 @"recalltype": [NSNumber numberWithInteger:recallType]}];
}

@end
