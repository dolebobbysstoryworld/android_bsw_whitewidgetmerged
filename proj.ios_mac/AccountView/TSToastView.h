//
//  TSToastView.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 17..
//
//

#import <UIKit/UIKit.h>

@interface TSToastView : UIView

- (id)initWithString:(NSString*)string;
- (void)showInView:(UIView*)parent;
- (void)showInViewWithAnimation:(UIView*)parent;
- (void)showInViewForLandscape:(UIView*)parent;
@end
