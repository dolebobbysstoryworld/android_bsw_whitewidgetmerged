//
//  UIColor+RGB.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 30..
//
//

#import <UIKit/UIKit.h>

#define RGB(r,g,b)              [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r,g,b,a)           [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define RGBH(string)            [UIColor colorForHex:string]
#define RGBAH(string, a)        [UIColor colorForHex:string alpha:a]

@interface UIColor (RGB)

+ (UIColor *) colorForHex:(NSString *)hexColor;//#AF7AFF 같은 형식의 문자열 입력
+ (UIColor *) colorForHex:(NSString *)hexColor alpha:(CGFloat)a;
+ (UIColor *) colorWithR:(NSInteger)r G:(NSInteger)g B:(NSInteger)b alpha:(CGFloat)a;
+ (UIColor *) colorWithR:(NSInteger)r G:(NSInteger)g B:(NSInteger)b;

@end
