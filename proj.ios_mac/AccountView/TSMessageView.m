//
//  TSMessageView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 3..
//
//

#import "TSMessageView.h"

@interface TSMessageView ()

@property (nonatomic, strong) NSString *message;
@property (nonatomic, assign) TSPopupIconType iconType;

@end

@implementation TSMessageView

- (id)initWithMessage:(NSString*)string icon:(TSPopupIconType)iconType {
    self = [super init];
    if (self) {
        self.message = string;
        self.iconType = iconType;
    }
    return self;
}

- (void)adjustContentViewLayout {
//    NSLog(@"TSMessageView::adjustContentViewLayout");
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 254, 19)];//tuilise_todo : 가변 줄 수에 따라 높이란다. 어떻게 할까?
    messageLabel.font = [UIFont fontWithName:kDefaultFontName size:17];
    messageLabel.textColor = RGBH(@"#787878");
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.numberOfLines = 0;
    messageLabel.text = self.message;
    [messageLabel sizeToFit];
    
    self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0, self.topView.frame.size.height, POPUPVIEW_WIDTH, 65+8+messageLabel.frame.size.height+12)];
    
    NSString *iconName;
    switch (self.iconType) {
        case TSPopupIconTypeWarning: {
            iconName = @"popup_warning.png";
        } break;
        case TSPopupIconTypeCoin: {
            iconName = @"popup_coin.png";
        } break;
        default: { NSAssert(NO, @"unsupport message icon image"); } break;
    }
    UIImageView *iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:iconName ignore:YES]];
    iconView.frame = CGRectMake(103, 0, 65, 65);//tuilis_todo : x pos 소수점 나온다. 우선 버렸음
    [self.contentView addSubview:iconView];
    
    CGRect frame = messageLabel.frame;
    frame.origin.x = (POPUPVIEW_WIDTH-frame.size.width)/2;
    frame.origin.y = 65+8;
    messageLabel.frame = frame;
    [self.contentView addSubview:messageLabel];
    
    UIImageView *middle = [[UIImageView alloc] initWithImage:
                           [[UIImage imageNamedEx:@"popup_middle.png" ignore:YES]
                            resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)]];
    middle.frame = self.contentView.frame;
    [self addSubview:middle];

    [self addSubview:self.contentView];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
