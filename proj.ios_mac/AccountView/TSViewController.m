//
//  TSViewController.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 11. 6..
//
//
#define GFDEBUG_ENABLE

#import "TSViewController.h"
#import "UIViewController+ChangeRootView.h"
#import "TSToastView.h"

@interface TSViewController ()

@end

@implementation TSViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.className = NSStringFromClass([self class]);
        self.bgFactorName = @"bg_map_factor.png";
    }
    return self;
}

- (void)viewWillLayoutSubviews {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.view.superview.layer.masksToBounds = YES;
        
        if(!IS_IOS8) {
            self.view.superview.bounds = getRectValue(TSViewIndexShare, TSShareSuperViewControllerRect);
            self.view.frame = getRectValue(TSViewIndexShare, TSShareSuperViewControllerRect);
        }

        self.view.superview.backgroundColor = [UIColor clearColor];
        self.view.backgroundColor = [UIColor clearColor];
    }
    GFLog(@"%@ superview.bounds : %@, view.frame : %@", self.className, NSStringFromCGRect(self.view.superview.bounds), NSStringFromCGRect(self.view.frame));
    [super viewWillLayoutSubviews];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.layoutView = self.view;
    // Do any additional setup after loading the view.
    GFLog(@"%@ frameSize : %@", self.className, NSStringFromCGRect(self.view.frame));
    self.view.backgroundColor = [UIColor clearColor];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.layoutView = [[UIView alloc] initWithFrame:getRectValue(TSViewIndexShare, TSShareSubViewRect)];
        [self.view addSubview:self.layoutView];
        NSLog(@"%@ self.view.frame : %@ / self.layoutView.frame : %@", self.className, NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.layoutView.frame));
    }
    
    if (self.ignoreBGImage == YES) {
        return;
    }
    
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"bg_map_full.png"]];
    backgroundView.frame = getRectValue(TSViewIndexShare, TSShareSubViewOrgRect);
    [self.layoutView addSubview:backgroundView];

    UIImageView *factorView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:self.bgFactorName]];
    factorView.frame = getRectValue(TSViewIndexShare, TSShareSubViewOrgRect);
    [self.layoutView addSubview:factorView];
    
    self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.closeButton setImage:[UIImage imageNamedEx:@"btn_cancel_normal.png" ignore:YES] forState:UIControlStateNormal];
    [self.closeButton setImage:[UIImage imageNamedEx:@"btn_cancel_press.png" ignore:YES] forState:UIControlStateHighlighted];
    [self.closeButton addTarget:self action:@selector(selectorCloseButton:) forControlEvents:UIControlEventTouchUpInside];
    self.closeButton.frame = getRectValue(TSViewIndexShare, TSShareCloseButtonRectInView);
    [self.layoutView addSubview:self.closeButton];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        ///PAD만 표시라 가변 위치 필요 없음
        if (!self.lion) {
            self.lion = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"full_popup_character_01.png"]];
            CGRect newFrame = CGRectMake((((2048-828)/2)-224+32)/2, (768-612)/2+(70+472+210)/2, 346/2, 472/2);//CGRectMake(3, 376, 148, 236);
            self.lion.frame = newFrame;
            [self.view addSubview:self.lion];
        } else {
            [self.view bringSubviewToFront:self.lion];
        }
        
        if (!self.racoon) {
            self.racoon = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"full_popup_character_02.png"]];
            self.racoon.frame = CGRectMake( ((2048-828)/2+(828-116)-32)/2 , (768-612)/2+70/2, 296/2, 472/2);//CGRectMake(414, 35, 148, 236);
            [self.view addSubview:self.racoon];
        } else {
            [self.view bringSubviewToFront:self.racoon];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    GFLog(@"%@ viewWillAppear", self.className);
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(noticeWillShowKeyboard:) name:UIKeyboardWillShowNotification object:nil];
    [center addObserver:self selector:@selector(noticeWillHideKeyboard:) name:UIKeyboardWillHideNotification object:nil];
    //    [center addObserver:self selector:@selector(notificationPopupShow:) name:tsNotificationPopupShow object:nil];
    //    [center addObserver:self selector:@selector(notificationPopupClose:) name:tsNotificationPopupClose object:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    GFLog(@"%@ viewDidDisappear", self.className);
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    //    [[NSNotificationCenter defaultCenter] removeObserver:self name:tsNotificationPopupShow object:nil];
    //    [[NSNotificationCenter defaultCenter] removeObserver:self name:tsNotificationPopupClose object:nil];
}

- (IBAction)selectorCloseButton:(id)sender {
    [self closeTSViewController];
}

- (void)closeTSViewController {
    [self closeTSViewControllerWithAnimation:YES];
}

- (void)closeTSViewControllerWithAnimation:(BOOL)animated {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self dismissViewControllerAnimated:animated completion:nil];
    } else {
        if (self.preRootViewController) {
            [UIViewController restoreRootViewControlller:self];
        } else {
            [self dismissViewControllerAnimated:animated completion:nil];
        }
    }
}

- (void)selectorAfterObjectProcess:(id)object {
    NSDictionary *objectsDic = (NSDictionary*)object;
    if ([objectsDic objectForKey:@"toast"] != nil) {
        TSToastView *toast = [[TSToastView alloc] initWithString:[objectsDic objectForKey:@"toast"]];
        if (!IS_IOS8 && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [toast showInViewWithAnimation:self.layoutView];
        } else {
            [toast showInViewWithAnimation:self.view];
        }
    }
}

- (BOOL)isShowActivity {
    UIActivityIndicatorView *indicatorView = nil;
    indicatorView = (UIActivityIndicatorView*)[self.layoutView viewWithTag:999777];
    if (indicatorView) {
        return YES;
    }
    return NO;
}

- (void)showActivityInView:(BOOL)show {
    UIActivityIndicatorView *indicatorView = nil;
    if (show) {
        if ([self isShowActivity]) {
            return;
        }
        indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        indicatorView.center = CGCenterPointFromRect(CGRectMake(0, 0, self.layoutView.frame.size.width, self.layoutView.frame.size.height));
        indicatorView.tag = 999777;
//        indicatorView.frame = frame;
        [self.layoutView addSubview:indicatorView];
        [indicatorView startAnimating];
    } else {
        indicatorView = (UIActivityIndicatorView*)[self.layoutView viewWithTag:999777];
        if (indicatorView) {
            [indicatorView stopAnimating];
            [indicatorView removeFromSuperview];
        }
    }
}

- (void)noticeWillShowKeyboard:(NSNotification *)notification {
    GFLog(@"%@ noticeWillShowKeyboard", self.className);
    self.isKeyboardHide = NO;
}

- (void)noticeWillHideKeyboard:(NSNotification *)notification {
    GFLog(@"%@ noticeWillHideKeyboard", self.className);
    self.isKeyboardHide = YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

- (NSUInteger)supportedInterfaceOrientations{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return UIInterfaceOrientationMaskLandscape;
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (NSUInteger)application:(UIApplication*)application supportedInterfaceOrientationsForWindow:(UIWindow*)window {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return UIInterfaceOrientationMaskLandscape;
    }
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)disablesAutomaticKeyboardDismissal {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return NO;//ipad UIModalPresentationFormSheet 일때 Keyboard 안내려가는거 처리
    }
    return YES;
}

- (NSString*)description {
    return [NSString stringWithFormat:@"%@ // %@", self.className, [super description]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
