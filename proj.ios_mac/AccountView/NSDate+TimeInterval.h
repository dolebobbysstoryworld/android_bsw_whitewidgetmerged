//
//  NSDate+TimeInterval.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 19..
//
//

#import <Foundation/Foundation.h>

@interface NSDate (TimeInterval)

+ (NSString*)stringFromTimeIntervalSinceDate:(NSDate*)fromDate;
- (NSString*)stringFromTimeIntervalNow;

@end
