//
//  TSModifyAccountView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 10..
//
//

#import "TSModifyAccountView.h"
#import "ResolutionManager.h"
#import "UIImage+PathExtention.h"
#import "UIColor+RGB.h"
#import "TSTextField.h"
#import "TSRadioButton.h"
#import "TSDolesNet.h"
#import "TSPickerView.h"
#import "TSMessageView.h"
#import "TSToastView.h"

#define TAG_BIRTHDAY_TEXTFIELD          10001
#define TAG_BIRTHDAY_POPUP              10002
#define TAG_CITY_TEXTFIELD              10003
#define TAG_CITY_POPUP                  10004
#define TAG_CURRENTPW_TEXTFIELD         10005
#define TAG_CHANGEPW_TEXTFIELD          10006
#define TAG_CONFIRMPW_TEXTFIELD         10007
#define TAG_MAIL_RADIOBUTTON            10008
#define TAG_FEMAIL_RADIOBUTTON          10009
#define TAG_PROVINCE_NET                10010
#define TAG_MODIFYACCOUNT_NET           10011
#define TAG_COUNTRY_POPUP               10012

@interface TSModifyAccountView () < TSTextFieldDelegate, TSRadioButtonDelegate, TSPopupViewDelegate, TSNetDelegate >

@property (nonatomic, strong) TSTextField *currentPWTextFiled;
@property (nonatomic, strong) TSTextField *changePWTextFiled;
@property (nonatomic, strong) TSTextField *confirmPWTextFiled;

@property (nonatomic, strong) TSTextField *birthDayTextFiled;
@property (nonatomic, assign) NSInteger birthDayIndex;
@property (nonatomic, strong) TSTextField *cityTextFiled;
@property (nonatomic, strong) NSArray *countrys;
@property (nonatomic, strong) NSArray *countryCodes;
@property (nonatomic, assign) NSInteger countryIndex;
@property (nonatomic, assign) NSInteger cityIndex;
@property (nonatomic, assign) BOOL isCitySelected;
@property (nonatomic, strong) TSNet *net;
@property (nonatomic, strong) NSArray *birthYears;

@property (nonatomic, assign) BOOL currentPWStringReady;
@property (nonatomic, assign) BOOL changePWStringReady;
@property (nonatomic, assign) BOOL confirmPWStringReady;

@property (nonatomic, strong) TSRadioButton *mailRadioButton;
@property (nonatomic, strong) TSRadioButton *femailRadioButton;

@property (nonatomic, strong) UIButton *saveButton;

@end

@implementation TSModifyAccountView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"bg_map_factor.png"]];
        backgroundView.frame = getRectValue(TSViewIndexShare, TSShareSubViewOrgRect);
        [self addSubview:backgroundView];

        UILabel *idLabel = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexModify, TSModifyIDLabelRect)];
        idLabel.font = [UIFont fontWithName:kDefaultBoldFontName size:16];
        idLabel.textAlignment = NSTextAlignmentCenter;
        idLabel.textColor = RGBH(@"#4f2c00");
        idLabel.numberOfLines = 1;//tuilise_todo : 두줄이 되려면 가로 가이드가 2px 좀더 커야 한다.
        idLabel.adjustsFontSizeToFitWidth = YES;
        idLabel.text = [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_LOGINID];
        [self addSubview:idLabel];
    
        [self adjustTextFieldLayout];
//        [self adjustRadioButtonLayout];
        [self adjustButtonLayout];
        
        self.birthYears = [TSPickerView birthYearsData];
        self.countrys = [TSPickerView countrys];
        self.countryCodes = [TSPickerView countryCodes];
        self.countryIndex = 0;
        NSInteger index = 0;
        NSString *code = nil;
        NSString *saveCode = [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_COUNTRY];
        for (index = 0; index < [self.countryCodes count]; index++) {
            code = [self.countryCodes objectAtIndex:index];
            if ([saveCode isEqualToString:code] == YES) {
                self.countryIndex = index;
                break;
            }
        }
        self.isCitySelected = YES;
    }
    return self;
}

- (void)adjustTextFieldLayout {
    self.currentPWTextFiled =
    [[TSTextField alloc] initWithFrame:getRectValue(TSViewIndexModify, TSModifyCurrentPWRect)
                                  icon:@"input_password_icon.png" placeholder:NSLocalizedString(@"PF_EDITACCOUNT_TEXTFIELD_CURRENT_PASSWORD", @"Current Password")];
    [self.currentPWTextFiled setPasswordType:YES];
    self.currentPWTextFiled.delegate = self;
    self.currentPWTextFiled.tag = TAG_CURRENTPW_TEXTFIELD;
    [self addSubview:self.currentPWTextFiled];

    self.changePWTextFiled =
    [[TSTextField alloc] initWithFrame:getRectValue(TSViewIndexModify, TSModifyChangePWRect)
                                  icon:@"input_password_icon.png" placeholder:NSLocalizedString(@"PF_EDITACCOUNT_TEXTFIELD_PASSWORD", @"Change Password")];
    [self.changePWTextFiled setPasswordType:YES];
    self.changePWTextFiled.delegate = self;
    [self.changePWTextFiled setComment:NSLocalizedString(@"PF_EDITACCOUNT_TEXTFIELD_PASSWORD_CONDITION", @"at least 6 characters")];
    self.changePWTextFiled.tag = TAG_CHANGEPW_TEXTFIELD;
    [self addSubview:self.changePWTextFiled];
    
    self.confirmPWTextFiled =
    [[TSTextField alloc] initWithFrame:getRectValue(TSViewIndexModify, TSModifyConfirmPWRect)
                                  icon:@"input_password_icon.png" placeholder:NSLocalizedString(@"PF_EDITACCOUNT_TEXTFIELD_CONFIRM_PASSWORD", @"Conform Password")];
    [self.confirmPWTextFiled setPasswordType:YES];
    self.confirmPWTextFiled.delegate = self;
    self.confirmPWTextFiled.tag = TAG_CONFIRMPW_TEXTFIELD;
    [self addSubview:self.confirmPWTextFiled];
    
//    self.birthDayTextFiled =
//    [[TSTextField alloc] initWithFrame:getRectValue(TSViewIndexModify, TSModifyBirthYearRect)
//                                  icon:@"input_birthday_icon.png" placeholder:NSLocalizedString(@"PF_SIGNUP_TEXTFIELD_BIRTHYEAR", @"Birthday") editable:NO];
//    self.birthDayTextFiled.tag = TAG_BIRTHDAY_TEXTFIELD;
//    self.birthDayTextFiled.delegate = self;
//    [self addSubview:self.birthDayTextFiled];
//    self.birthDayTextFiled.text = [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_BIRTHDAY];
    
    self.cityTextFiled =
    [[TSTextField alloc] initWithFrame:getRectValue(TSViewIndexModify, TSModifyCityRect)
                                  icon:@"input_city_icon.png"
                           placeholder:NSLocalizedString(@"PF_ACCOUNT_PLACEHOLDER_COUNTRY", @"Country") editable:NO];
    self.cityTextFiled.tag = TAG_CITY_TEXTFIELD;
    self.cityTextFiled.delegate = self;
//    self.cityTextFiled.enabled = NO;
    [self addSubview:self.cityTextFiled];
//    self.cityTextFiled.text = [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_CITY];
    NSArray *countryCodes = [TSPickerView countryCodes];
    NSString *saveCountryCode = [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_COUNTRY];
    NSString *countryCode;
    NSInteger index = 0;
    for (countryCode in countryCodes) {
        if ([countryCode isEqualToString:saveCountryCode] == YES) {
            break;
        }
        index++;
    }
    NSString *country = [[TSPickerView countrys] objectAtIndex:index];
    self.cityTextFiled.text = country;
    
//    self.net = [[TSNetProvince alloc] init];
//    self.net.tag = TAG_PROVINCE_NET;
//    self.net.delegate = self;
//    [self.net start];
}

- (void)tstextFieldDidEndEditing:(TSTextField *)textField {
    NSInteger tag = textField.tag;
    switch (tag) {
        case TAG_CURRENTPW_TEXTFIELD: {
            self.currentPWStringReady =
            [self.currentPWTextFiled checkPasswordStringValidationWithShowError:
             NSLocalizedString(@"PF_NET_ERROR__PASSWORD_SHORT", @"￼Please enter a password with at least 6 characters")];
        } break;
        case TAG_CHANGEPW_TEXTFIELD:
        case TAG_CONFIRMPW_TEXTFIELD: {
            self.changePWStringReady =
            [self.changePWTextFiled checkPasswordStringValidationWithShowError:
             NSLocalizedString(@"PF_NET_ERROR__PASSWORD_SHORT", @"￼Please enter a password with at least 6 characters")];
            self.confirmPWStringReady =
            [self.confirmPWTextFiled equalPasswordString:self.changePWTextFiled.text
                                           withShowError:NSLocalizedString(@"PF_NET_ERROR__PASSWORD_INCORRECT", @"The password is incorrect")];
        } break;
        default: { } break;
    }
    [self checkEnableSaveButton];
}

- (void)tstextFieldTouched:(TSTextField *)textField {
    TSPickerView *pickView = nil;
    if (textField.tag == TAG_BIRTHDAY_TEXTFIELD) {
        pickView = [[TSPickerView alloc] initWithDataSource:self.birthYears title:NSLocalizedString(@"PF_SIGNUP_POPUP_TITLE_BIRTHYEAR", @"Set birth year")];
        pickView.tag = TAG_BIRTHDAY_POPUP;
        pickView.selectedString = self.birthDayTextFiled.text;
    } else {
        if (YES) {//if ([self.cityTextFiled.text length] <= 0) {//아직 국가가 선택되지 않았다.
            pickView = [[TSPickerView alloc] initWithDataSource:self.countrys title:NSLocalizedString(@"PF_SIGNUP_PUPUP_TITLE_SELECT_COUNTRY", @"Enter the country")];
            pickView.tag = TAG_COUNTRY_POPUP;
            pickView.selectedIndex = self.countryIndex;
            self.isCitySelected = NO;
        }/* else {//국가는 선택 되었다
            pickView = [[TSPickerView alloc] initWithDataSource:((TSNetProvince*)self.net).provinces
                                                          title:NSLocalizedString(@"PF_SIGNUP_PUPUP_TITLE_SELECT_CITY", @"Enter the city")];
            pickView.tag = TAG_CITY_POPUP;
            pickView.selectedString = [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_CITY];
        }*/
    }
    pickView.buttonLayout = TSPopupButtonLayoutCancelSet;
    pickView.delegate = self;
    [pickView showInView:self];
}

- (void)popview:(TSPopupView *)view didClickedButton:(TSPopupButtonType)type {
    if (type == TSPopupButtonTypeSet) {
        switch (view.tag) {
            case TAG_COUNTRY_POPUP: {
                self.countryIndex = ((TSPickerView*)view).selectedIndex;
                self.cityTextFiled.text = [self.countrys objectAtIndex:self.countryIndex];
//                NSString *countryCode = [self.countryCodes objectAtIndex:self.countryIndex];
//                if ([countryCode isEqualToString:NSLocalizedString(@"PF_SIGNUP_COUNTRY_LIST_OTHER", @"Other")] == NO) {
//                    self.cityIndex = 0;
//                    self.net = [[TSNetProvince alloc] initWithCountryCode:countryCode];
//                    self.net.delegate = self;
//                    self.net.tag = TAG_PROVINCE_NET;
//                    [self.net start];
//                    self.cityTextFiled.enabled = NO;
//                    return;
//                }
                self.cityTextFiled.enabled = YES;
                self.isCitySelected = YES;
            } break;
            case TAG_CITY_POPUP: {
                self.cityIndex = ((TSPickerView*)view).selectedIndex;
                self.cityTextFiled.text = [((TSNetProvince*)self.net).provinces objectAtIndex:self.cityIndex];
                self.isCitySelected = YES;
                self.cityTextFiled.enabled = YES;
            } break;
            case TAG_BIRTHDAY_POPUP: {
                self.birthDayIndex = ((TSPickerView*)view).selectedIndex;
                self.birthDayTextFiled.text = [self.birthYears objectAtIndex:self.birthDayIndex];
            } break;
            default:
                break;
        }
    } else {
        switch (view.tag) {
            case TAG_COUNTRY_POPUP: {
                if ([self.cityTextFiled.text length] <= 0) {
                    self.isCitySelected = NO;
                }
            } break;
            case TAG_CITY_POPUP: {
                self.isCitySelected = NO;
                self.cityTextFiled.enabled = YES;
            } break;
            case TAG_BIRTHDAY_POPUP: {
                
            } break;
            default:
                break;
        }
    }
    [self checkEnableSaveButton];
}

- (void)adjustRadioButtonLayout {
    self.mailRadioButton = [TSRadioButton radioButtonWithTitle:NSLocalizedString(@"PF_SIGNUP_RADIOTITLE_MALE", @"Male") Frame:getRectValue(TSViewIndexModify, TSModifyMaleRadioRect)];
    self.mailRadioButton.tag = TAG_MAIL_RADIOBUTTON;
    self.mailRadioButton.delegate = self;
    [self addSubview:self.mailRadioButton];
    self.femailRadioButton = [TSRadioButton radioButtonWithTitle:NSLocalizedString(@"PF_SIGNUP_RADIOTITLE_FEMALE", @"Female") Frame:getRectValue(TSViewIndexModify, TSModifyFemaleRadioRect)];
    self.femailRadioButton.tag = TAG_FEMAIL_RADIOBUTTON;
    self.femailRadioButton.delegate = self;
    [self addSubview:self.femailRadioButton];
    
    NSInteger gender = [[[NSUserDefaults standardUserDefaults] valueForKey:UD_INTEGER_GENDER] integerValue];
    if (gender == 2) {
        self.femailRadioButton.selected = YES;
    } else {
        self.mailRadioButton.selected = YES;
    }
}

- (void)radioButton:(TSRadioButton *)radioButton didChangeSelectedStatus:(BOOL)selected {
    switch (radioButton.tag) {
        case TAG_MAIL_RADIOBUTTON: {
            if (selected) {
                self.femailRadioButton.selected = NO;//tuilise_todo : 나중에 group 처리 살계해 보자.
            }
        } break;
        case TAG_FEMAIL_RADIOBUTTON: {
            if (selected) {
                self.mailRadioButton.selected = NO;
            }
        } break;
        default: { } break;
    }
    [self checkEnableSaveButton];
}


- (void)adjustButtonLayout {
    self.saveButton = [UIButton buttonWithType:TSDolsTypeOK Frame:getRectValue(TSViewIndexModify, TSModifySaveButtonRect)
                                         Title:NSLocalizedString(@"PF_EDITACCOUNT_BUTTON_TITLE_SAVE", @"Save")];
    [self.saveButton addTarget:self action:@selector(selectorSaveButton:) forControlEvents:UIControlEventTouchUpInside];
    self.saveButton.enabled = YES;
    [self addSubview:self.saveButton];
}

- (IBAction)selectorSaveButton:(id)sender {
    NSLog(@"selectorSaveButton");
    if([self.currentPWTextFiled.text length] < 6) {
        TSToastView *toast = [[TSToastView alloc] initWithString:NSLocalizedString(@"PF_EDITACCOUNT_POPUP_INPUT_PASSWORD", @"Enter your current password")];
        [toast showInViewWithAnimation:self];
        return;
    }
    
    self.net =
    [[TSNetModifyProfile alloc] initWithPassword:self.currentPWTextFiled.text newPassword:self.changePWTextFiled.text
                                       birthYear:@"9999"//self.birthDayTextFiled.text
                                         address:self.cityTextFiled.text
                                          gender:99//self.mailRadioButton.selected?1:2
                                     countryCode:[self.countryCodes objectAtIndex:self.countryIndex]];
    self.net.tag = TAG_MODIFYACCOUNT_NET;
    self.net.delegate = self;
    [self.net start];
}

- (void)checkEnableSaveButton {
//    if (self.currentPWStringReady == YES && /*self.changePWStringReady == YES && self.confirmPWStringReady == YES &&*///new 패스워드 입력 안해도 된단다.
//        ([self.birthDayTextFiled.text length] > 0) && (self.isCitySelected == YES) &&//([self.cityTextFiled.text length] > 0) &&
//        (self.mailRadioButton.selected == YES || self.femailRadioButton.selected ) ) {
//        self.saveButton.enabled = YES;
//    } else {
//        self.saveButton.enabled = NO;
//    }
    self.saveButton.enabled = YES;
}

#pragma mark - TSNetDelegate
- (void)net:(TSNet*)netObject didStartProcess:(TSNetResultType)result {
    switch (netObject.tag) {
        case TAG_PROVINCE_NET: {
            self.cityTextFiled.showActivity = YES;
        } break;
        case TAG_MODIFYACCOUNT_NET: {
            [self.saveButton showActivity:YES];
            self.saveButton.enabled = NO;
        } break;
        default: {  } break;
    }
}

- (void)net:(TSNet*)netObject didEndProcess:(TSNetResultType)result {
    if (!result) {
        switch (netObject.tag) {
            case TAG_PROVINCE_NET: {
                self.cityTextFiled.showActivity = NO;
//                self.cityTextFiled.enabled = YES;
//                self.cityTextFiled.text = [((TSNetProvince*)self.net).provinces objectAtIndex:0];//각 국의 도시 정보 처음값으로 초기화

                TSPickerView *pickView = [[TSPickerView alloc] initWithDataSource:((TSNetProvince*)self.net).provinces
                                                                            title:NSLocalizedString(@"PF_SIGNUP_PUPUP_TITLE_SELECT_CITY", @"Enter the city")];
                pickView.tag = TAG_CITY_POPUP;
                pickView.selectedIndex = self.cityIndex;
                pickView.buttonLayout = TSPopupButtonLayoutCancelSet;
                pickView.delegate = self;
                [pickView showInView:self];
            } break;
            case TAG_MODIFYACCOUNT_NET: {
                [self.saveButton showActivity:NO];
//                if (self.delegate && [self.delegate respondsToSelector:@selector(closeViewControllerWithView:)]) {
//                    [self.delegate closeViewControllerWithView:self];
//                }
                NSDictionary *dic = @{@"toast": NSLocalizedString(@"PF_EDITACCOUNT_POPUP_SAVE_COMPLETE", "It has been changed")};
                if (self.delegate && [self.delegate respondsToSelector:@selector(view:Page:userData:)]) {
                    [self.delegate view:self Page:TSViewIndexSettingMain userData:dic];
                }
            } break;
            default: {  } break;
        }
    } else {//tuilise_todo : 오류 결과다. 맞는 처리 필요
        NSString *string = nil;
        switch (netObject.tag) {
            case TAG_PROVINCE_NET: {
                self.cityTextFiled.showActivity = NO;
                string = [NSString stringWithFormat:@"Server Error\n%@", [netObject serverErrorString]];
            } break;
            case TAG_MODIFYACCOUNT_NET: {
                self.saveButton.enabled = YES;
                [self.saveButton showActivity:NO];
                if (netObject.returnCode == 90106) {
                    [self.currentPWTextFiled setError:NSLocalizedString(@"PF_NET_ERROR__PASSWORD_INCORRECT", @"The password is incorrect")];
                    return;
                }
                string = [NSString stringWithFormat:@"Server Error\n%@", [netObject serverErrorString]];
            } break;
            default: {  } break;
        }
        
        TSMessageView *messageView = [[TSMessageView alloc] initWithMessage:string icon:TSPopupIconTypeWarning];
        messageView.buttonLayout = TSPopupButtonLayoutOK;
        [messageView showInView:self];
    }
}

- (void)net:(TSNet*)netObject didFailWithError:(TSNetResultType)result {
    switch (result) {
        case TSNetErrorNetworkDisable:
        case TSNetErrorTimeout: {
            TSToastView *toast = [[TSToastView alloc] initWithString:NSLocalizedString(@"CM_NET_WARNING_NETWORK_UNSTABLE", @"The network is unstable.\nPlease try again")];
            [toast showInViewWithAnimation:self];
        } break;
        default: {
        } break;
    }
    switch (netObject.tag) {
        case TAG_PROVINCE_NET: {
            self.cityTextFiled.enabled = YES;
            self.cityTextFiled.showActivity = NO;
        } break;
        case TAG_MODIFYACCOUNT_NET: {
            self.saveButton.enabled = YES;
            [self.saveButton showActivity:NO];
        } break;
        default: {  } break;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
