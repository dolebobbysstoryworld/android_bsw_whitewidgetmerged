//
//  TSPopupView.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 3..
//
//

#import <UIKit/UIKit.h>
#import "UIImage+PathExtention.h"
#import "ResolutionManager.h"
#import "UIColor+RGB.h"
#import "UIButton+DolesUI.h"

typedef NS_ENUM(NSInteger, TSPopupType) {
    TSPopupTypeMessage = 0,
    TSPopupTypePicker,
    TSPopupTypePassword,
    TSPopupTypeTextInput,
    TSPopupTypeShareComment,
};

typedef NS_ENUM(NSInteger, TSPopupButtonLayoutType) {
    TSPopupButtonLayoutCancel = 0,
    TSPopupButtonLayoutCancelOK,
    TSPopupButtonLayoutNoYes,
    TSPopupButtonLayoutOK,
    TSPopupButtonLayoutCancelSet,//Picker에만 쓰일 듯?
    TSPopupButtonLayoutCancelCustom,
};

typedef NS_ENUM(NSInteger, TSPopupButtonType) {
    TSPopupButtonTypeOK = 0,
    TSPopupButtonTypeCancel,
    TSPopupButtonTypeSet,
    TSPopupButtonTypeCustom,
};

@class TSPopupView;
@protocol TSPopupViewDelegate <NSObject>

- (void)popview:(TSPopupView*)view didClickedButton:(TSPopupButtonType)type;

@end

#define POPUPVIEW_WIDTH                 272
#define POPUPVIEW_CONTENT_HEIGHT        138
#define POPUPVIEW_ONEBUTTON_FRAME       CGRectMake(5, 5, 262, 35)
#define POPUPVIEW_LEFTBUTTON_FRAME      CGRectMake(5, 5, 129, 35)
#define POPUPVIEW_RIGHTBUTTON_FRAME     CGRectMake(138, 5, 129, 35)

@interface TSPopupView : UIView

@property (nonatomic, assign) id<TSPopupViewDelegate> delegate;
@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, assign) TSPopupButtonLayoutType buttonLayout;
@property (nonatomic, assign) TSPopupType popupType;
@property (nonatomic, strong) NSString *customButtonTitle;
@property (nonatomic, strong) NSString *customCancelTitle;
@property (nonatomic, strong) UIButton *rightButton;
@property (nonatomic, assign) BOOL pendingCancel;
@property (nonatomic, assign) BOOL popupHidden;
@property (nonatomic, assign) BOOL nodimlayer;

// Overriding method. ** 외부에서 임의 호출 금지 **
- (void)adjustTopViewLayout;
- (void)adjustContentViewLayout;
- (void)adjustBottomViewLayout;
- (BOOL)buttonWillOKProcessing;
////////////////////////////////

- (void)showInView:(UIView*)parent;
- (void)showInFullView:(UIView*)parent;
- (void)showInViewForLandscape:(UIView*)parent;

- (void)hide;
- (IBAction)selectorCancelButton:(id)sender;
- (IBAction)selectorOKButton:(id)sender;
- (void)processPendingCancel;

@end
