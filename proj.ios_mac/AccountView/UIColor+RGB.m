//
//  UIColor+RGB.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 30..
//
//

#import "UIColor+RGB.h"

@implementation UIColor (RGB)
+ (UIColor *) colorForHex:(NSString *)hexColor {
    return [UIColor colorForHex:hexColor alpha:1.0f];
}
+ (UIColor *) colorForHex:(NSString *)hexColor alpha:(CGFloat)a { //#FFFFFF
	hexColor = [[hexColor stringByTrimmingCharactersInSet:
                 [NSCharacterSet whitespaceAndNewlineCharacterSet]
                 ] uppercaseString];
	
	// String should be 6 or 7 characters if it includes '#'
	if ([hexColor length] < 6)
		return [UIColor blackColor];
	
	// strip # if it appears
	if ([hexColor hasPrefix:@"#"])
		hexColor = [hexColor substringFromIndex:1];
	
	// if the value isn't 6 characters at this point return
	// the color black
	if ([hexColor length] != 6)
		return [UIColor blackColor];
	
	// Separate into r, g, b substrings
	NSRange range;
	range.location = 0;
	range.length = 2;
	
	NSString *rString = [hexColor substringWithRange:range];
	
	range.location = 2;
	NSString *gString = [hexColor substringWithRange:range];
	
	range.location = 4;
	NSString *bString = [hexColor substringWithRange:range];
	
	// Scan values
	unsigned int r, g, b;
	[[NSScanner scannerWithString:rString] scanHexInt:&r];
	[[NSScanner scannerWithString:gString] scanHexInt:&g];
	[[NSScanner scannerWithString:bString] scanHexInt:&b];
    
	return [UIColor colorWithR:r G:g B:b alpha:a];
	/*return [UIColor colorWithRed:((float) r / 255.0f)
     green:((float) g / 255.0f)
     blue:((float) b / 255.0f)
     alpha:1.0f];*/
}

+ (UIColor *) colorWithR:(NSInteger)r G:(NSInteger)g B:(NSInteger)b alpha:(CGFloat)a {
	return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:a];
}

+ (UIColor *) colorWithR:(NSInteger)r G:(NSInteger)g B:(NSInteger)b {
	return [UIColor colorWithR:r G:g B:b alpha:1.0f];
}

@end
