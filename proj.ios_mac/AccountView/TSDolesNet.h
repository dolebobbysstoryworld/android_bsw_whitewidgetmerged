//
//  TSDolesNet.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 3..
//
//

#import <Foundation/Foundation.h>

#pragma mark - Root Class
typedef NS_ENUM(NSInteger, TSNetResultType) {
	TSNetSuccess = 0,
	TSNetErrorResultFail,
	TSNetErrorNetworkDisable,
	TSNetErrorConnection,
	TSNetErrorNotAllowed,
	TSNetErrorServiceNotFound,
	TSNetErrorWrongMessage,
	TSNetErrorTimeout,
	TSNetErrorBadURL,
};

typedef NS_ENUM(NSInteger, TSNetDataType) {
	TSNetDataUnknown = 0,
    TSNetDataDictionary,
    TSNetDataArray,
    TSNetDataBinary,
};

@class TSNet;

@protocol TSNetDelegate <NSObject>

@optional
- (void)net:(TSNet*)netObject didStartProcess:(TSNetResultType)result;
- (void)net:(TSNet*)netObject didEndProcess:(TSNetResultType)result;
- (void)net:(TSNet*)netObject didFailWithError:(TSNetResultType)result;

@end

@interface TSNet : NSObject

@property (nonatomic, strong) id<TSNetDelegate> delegate;
@property (nonatomic, readonly) NSInteger tag;
@property (nonatomic, readonly) BOOL returnResult;
@property (nonatomic, readonly) NSInteger returnCode;
@property (nonatomic, assign) BOOL ignoreServerError;
@property (nonatomic, strong) NSString *customRootKey;

- (void)setTag:(NSInteger)tag;
- (void)setRequestWitData:(NSDictionary*)postDictionary Group:(NSString*)group withAPI:(NSString*)api;
- (void)start;
- (NSString*)serverErrorString;

- (void)updateAuthKey;
- (void)didUpdatedAutkKey;

@end

#pragma mark - Profile Group
@interface TSNetSignup : TSNet

@property (nonatomic, assign) NSInteger userNumber;
- (id)initWithEmail:(NSString*)email Password:(NSString *)password Gender:(NSInteger)gender BirthDay:(NSString*)birthDay AddressMain:(NSString*)addressMain countryCode:(NSString*)code;
- (id)initWithSNSEmail:(NSString*)email snsID:(NSString*)snsid name:(NSString*)name
                Gender:(NSInteger)gender BirthDay:(NSString*)birthDay AddressMain:(NSString*)addressMain countryCode:(NSString*)code;

@end

@interface TSNetWithDraw : TSNet

- (id)initWithPassword:(NSString*)password Reason:(NSString*)reason;

@end

@interface TSNetFindPassword : TSNet

- (id)initWithEmail:(NSString*)string;

@end

@interface TSNetGetProfile : TSNet

@property (nonatomic, strong) NSString *addressMain;
@property (nonatomic, strong) NSString *birthDay;
@property (nonatomic, strong) NSString *changePasswordDateTime;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *emailActivateDateTime;
@property (nonatomic, assign) BOOL emailVarification;
@property (nonatomic, assign) NSInteger gender;
@property (nonatomic, strong) NSString *language;
@property (nonatomic, strong) NSString *nickname;
@property (nonatomic, strong) NSString *registerDateTIme;
@property (nonatomic, strong) NSString *userID;//authKey?????

@end

@interface TSNetModifyProfile : TSNet

- (id)initWithPassword:(NSString*)password newPassword:(NSString*)newPassword
             birthYear:(NSString*)birthYear address:(NSString*)address gender:(NSInteger)gender countryCode:(NSString*)code;

@end

@interface TSNetModifySNSProfile : TSNet

- (id)initWithBirthYear:(NSString*)birthYear address:(NSString*)address gender:(NSInteger)gender countryCode:(NSString*)code;

@end

@interface TSNetProvince : TSNet

@property (nonatomic, strong) NSArray *provinces;

- (id)initWithCountryCode:(NSString *)code;

@end

@interface TSNetDisconnectSNS : TSNet

-(id)initWithID:(NSString*)snsid reason:(NSString*)reason;

@end

@interface TSNetGetAppVersion : TSNet

@property (nonatomic, strong) NSString *appURL;
@property (nonatomic, strong) NSString *majorVersion;

@end

#pragma mark - Auth Group
@interface TSNetLogin : TSNet

- (id)initWithID:(NSString *)idString Password:(NSString*)password;

@end

@interface TSNetSNSLogin : TSNet

- (id)initWithSNSID:(NSString *)snsID;

@end

@interface TSNetUpdateAuthKey : TSNet

@end

#pragma mark - Coupon group
@interface TSNetCheckCoupon : TSNet

@property (nonatomic, assign) BOOL canUsable;
@property (nonatomic, assign) NSInteger coinValue;

- (id)initWithSerial:(NSString *)qrString;

@end

#pragma mark - Billing group
@interface TSNetGetBalance : TSNet

@property (nonatomic, strong) NSString *eventBalance;

@end

@interface TSNetUseCoinCoupon : TSNet

- (id)initWithSerial:(NSString *)qrString;

@end

@interface TSNetGetPurchaseList : TSNet

@property (nonatomic, strong) NSArray *purchaseList;
//Dictionary Key...
//OrderID: "test7"
//ProductName: "Bobby"
//PurchaseAmount: 9
//PurchaseNo: 2
//PurchaseQuantity: 3
//PurchaseStatus: 30
//RegisterDateTime: "2014-07-03 03:00"
//UserNo: 162

- (id)initWithUserNo:(NSInteger)userNo begin:(NSDate*)beginDate end:(NSDate*)endDate;

@end

#pragma mark - CS group
@interface TSNetRegisterCS : TSNet
- (id)initWithEmail:(NSString*)email description:(NSString*)description deviceInfo:(NSString*)device;
@end

#pragma mark - Service Define
//-----old code-------/
//#define TSNET_ALPHA_URL       @"https://dole-alpha.ithinkcore.net/%@/Api.svc/%@"
//#define TSNET_PRODUCTION_URL  @"https://dole.ithinkcore.net/%@/Api.svc/%@"

//hans added
#define TSNET_ALPHA_URL       @"http://doleserver.com:4000/api/v2/%@/Api.svc/%@"
#define TSNET_PRODUCTION_URL  @"http://doleserver.com:3000/api/v2/%@/Api.svc/%@"

#if COCOS2D_DEBUG
//#define TSNET_SERVICE_URL     TSNET_ALPHA_URL
#define TSNET_SERVICE_URL     TSNET_PRODUCTION_URL
#else
#define TSNET_SERVICE_URL     TSNET_PRODUCTION_URL
#endif

#define TSNET_SERVICE_CODE    @"SVR004"

//#define TSNET_REAL_SNSID_ENABLE

#define APIGROUP_PROFILE                    @"Profile"
#define APIPROFILE_SIGNUP                   @"MobileSignup"
#define APIPROFILE_WITHDRAW                 @"WithdrawUser"
#define APIPROFILE_FINDPW                   @"FindPassword"
#define APIPROFILE_GETPROFILE               @"GetMobileProfile"
#define APIPROFILE_MODIFYPROFILE            @"ModifyMobileProfile"
#define APIPROFILE_MODIFYSNSPROFILE         @"ModifySNSUserProfile"
#define APIPROFILE_GETPROVINCE              @"GetProvinceInfo"
#define APIPROFILE_DICCONNECTSNS            @"DisconnectSNS"
#define APIPROFILE_GETAPPVERSION            @"GetAppVersion"

#define APIGROUP_AUTH                       @"Auth"
#define APIAUTH_LOGIN                       @"GetAuthKeyMobile"
#define APIAUTH_SNSLOGIN                    @"SNSLogin"

#define APIGROUP_COUPON                     @"Coupon/Mobile"
#define APICOUPON_CHECKCOUPON               @"CheckMobileCoupon"

#define APIGROUP_BILLINGW                   @"Billing/Wallet"
#define APIBILLINGW_GETBALANCE              @"GetBalance"

#define APIGROUP_BILLINGC                   @"Billing/Charge"
#define APIBILLINGC_USECOINCOUPON           @"UseCoinCoupon"
#define APIBILLINGC_CHARGEFREECASH          @"ChargeFreeCash"

#define APIGROUP_CMS                        @"Portal/Game"
#define APICMS_GETEVENTLIST                 @"GetEventNoticeList"

#define APIGROUP_CS                         @"CS/Main"
#define APICS_REGISTERCS                    @"RegisterCS"

#define APIGROUP_SHOP                       @"Shop"
#define APISHOP_GETINVENTORYLIST            @"GetInventoryList"
