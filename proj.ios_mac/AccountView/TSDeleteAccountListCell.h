//
//  TSDeleteAccountListCell.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 11. 18..
//
//

#import <UIKit/UIKit.h>

FOUNDATION_EXPORT NSString *const kDeleteAccountIconName;
FOUNDATION_EXPORT NSString *const kDeleteAccountTitle;
FOUNDATION_EXPORT NSString *const kDeleteAccountDescription;
FOUNDATION_EXPORT NSString *const kDeleteAccountFont;
FOUNDATION_EXPORT NSString *const kDeleteAccountSubFont;
FOUNDATION_EXPORT NSString *const kDeleteAccountCustomFrame;

@interface TSDeleteAccountListCell : UITableViewCell

@property (nonatomic, strong) NSDictionary *customData;

@end