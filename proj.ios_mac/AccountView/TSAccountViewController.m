//
//  TSAccountViewController.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 29..
//
//

#import "TSAccountViewController.h"
#import "UIImage+PathExtention.h"
#import "TSAccountDelegateProtocol.h"

#import "TSAccountLoginView.h"
#import "TSAccountSigninView.h"
#import "TSAccountAgreementView.h"
#import "TSAccountSNSSignupView.h"
#import "TSGuideForLoginView.h"
#import "TSGuideForLogin.h"

#import "TSDoleCoinViewController.h"
#import "TSSettingViewController.h"
#import "TSMessageView.h"
#import "TSProxyHelper.h"
#import "QRCodeReaderViewController.h"
#import "AppController.h"

@interface TSAccountViewController () < TSAccountDelegate, TSPopupViewDelegate >

@property (nonatomic, strong) UIView *currentDisplayView;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) UIImageView *lion;
@property (nonatomic, strong) UIImageView *racoon;
@property (nonatomic, assign) BOOL isKeyboardHide;
@property (nonatomic, assign) BOOL firstRun;

@property (nonatomic, strong) NSDictionary *loginForFacebook;
@property (nonatomic, assign) BOOL needLoginPostNotification;

@end

@implementation TSAccountViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (instancetype)initWithFirstRun:(BOOL)first
{
    self = [super init];
    if (self) {
        self.firstRun = first;
        self.parentType = TSParentTypeNone;
        self.needLoginPostNotification = NO;
        self.recallType = TSRecallNone;
    }
    return self;
}

- (void)viewWillLayoutSubviews {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.view.superview.layer.masksToBounds = YES;
        
        if(!IS_IOS8) {
            self.view.superview.bounds = getRectValue(TSViewIndexShare, TSShareSuperViewControllerRect);
            self.view.frame = getRectValue(TSViewIndexShare, TSShareSuperViewControllerRect);
        }
        
        self.view.superview.backgroundColor = [UIColor clearColor];
        self.view.backgroundColor = [UIColor clearColor];
    }
    [super viewWillLayoutSubviews];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(noticeShowKeyboard:) name:UIKeyboardDidShowNotification object:nil];
    [center addObserver:self selector:@selector(noticeHideKeyboard:) name:UIKeyboardWillHideNotification object:nil];
    [center addObserver:self selector:@selector(notificationPopupShow:) name:tsNotificationPopupShow object:nil];
    [center addObserver:self selector:@selector(notificationPopupClose:) name:tsNotificationPopupClose object:nil];
    
    self.view.backgroundColor = [UIColor clearColor];
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"bg_map_full.png"]];
    backgroundView.frame = getRectValue(TSViewIndexShare, TSShareSubViewRect);
    [self.view addSubview:backgroundView];
    
    self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.closeButton setImage:[UIImage imageNamedEx:@"btn_cancel_normal.png" ignore:YES] forState:UIControlStateNormal];
    [self.closeButton setImage:[UIImage imageNamedEx:@"btn_cancel_press.png" ignore:YES] forState:UIControlStateHighlighted];
    [self.closeButton addTarget:self action:@selector(selectorCloseButton:) forControlEvents:UIControlEventTouchUpInside];
    self.closeButton.frame = getRectValue(TSViewIndexShare, TSShareCloseButtonRect);
    [self.view addSubview:self.closeButton];
    if (self.firstRun) {
        self.closeButton.hidden = YES;
    }
    
    [self view:nil Page:TSViewIndexLogin userData:nil];//loginView init;
#ifdef _GUIDE_ENABLE_
//    if (self.firstRun) {
#ifndef _DEBUG_GUIDE_
    BOOL show = [[[NSUserDefaults standardUserDefaults] valueForKey:@"showguide_login"] boolValue];
    if (show == false) {
        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:@"showguide_login"];
#endif
//        TSGuideForLoginView *guideView = [[TSGuideForLoginView alloc] initWithFrame:getRectValue(TSViewIndexShare, TSShareSuperViewControllerRect)];
//        [guideView showInView:self.view];
        TSGuideForLogin *guideView = [[TSGuideForLogin alloc] initWithFrame:getRectValue(TSViewIndexShare, TSShareSuperViewControllerRect)];
        [guideView showInView:self.view];
#ifndef _DEBUG_GUIDE_
    }
#endif
//    }
#endif
}

- (void)backgroundResourceLoad {
    if (self.closeButton) {//tuilise_todo : firstRun 일때는 생성하지 마라
        [self.view bringSubviewToFront:self.closeButton];
        NSString *viewName = NSStringFromClass([self.currentDisplayView class]);
        if (self.firstRun && [viewName isEqualToString:@"TSAccountLoginView"] == YES) {
            self.closeButton.hidden = YES;
        }
    }

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        ///PAD만 표시라 가변 위치 필요 없음
        if (!self.lion) {
            self.lion = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"full_popup_character_01.png"]];
            CGRect newFrame = CGRectMake((((2048-828)/2)-224+32)/2, (768-612)/2+(70+472+210)/2, 346/2, 472/2);//CGRectMake(3, 376, 148, 236);
            self.lion.frame = newFrame;
            [self.view addSubview:self.lion];
        } else {
            [self.view bringSubviewToFront:self.lion];
        }
        
        if (!self.racoon) {
            self.racoon = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"full_popup_character_02.png"]];
            self.racoon.frame = CGRectMake( ((2048-828)/2+(828-116)-32)/2 , (768-612)/2+70/2, 296/2, 472/2);//CGRectMake(414, 35, 148, 236);
            [self.view addSubview:self.racoon];
        } else {
            [self.view bringSubviewToFront:self.racoon];
        }
    }
}

- (void)closeViewController {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:tsNotificationPopupShow object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:tsNotificationPopupClose object:nil];
    if (self.parentType == TSParentTypeNone) {
        [self dismissViewControllerAnimated:YES completion:^{
            if (self.firstRun) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kTSNotifivationFirstRunAccountViewColsed object:nil];
            }
            if (self.needLoginPostNotification == YES) {
                [((TSAccountLoginView*)self.currentDisplayView) postLoginNotification:self.recallType];
            }
        }];
        return;
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        /*if (self.parentType == TSParentTypeSetting) {
            UIViewController *viewController = nil;
            if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
            } else {
                viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
            }
            NSLog(@"viewController screen frame : %@",NSStringFromCGRect(viewController.view.frame));
            TSSettingViewController *settingViewController = [[TSSettingViewController alloc] init];
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                settingViewController.modalPresentationStyle = UIModalPresentationFormSheet;
            }
            settingViewController.originLayer = self.originLayer;
            if(IS_IOS8 && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                settingViewController.preferredContentSize = CGSizeMake(1024, 768);
            }
            [viewController presentViewController:settingViewController animated:NO completion:nil];
        } else */if (self.parentType == TSParentTypeDoleCoin) {
            if (self.needLoginPostNotification == YES) {
                UIViewController *viewController = nil;
                if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                    viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
                } else {
                    viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
                }
                QRCodeReaderViewController *qrCodeReaderViewContoller = [[QRCodeReaderViewController alloc] init];
                qrCodeReaderViewContoller.originLayer = self.originLayer;
                [viewController presentViewController:qrCodeReaderViewContoller animated:YES completion:nil];
            } else {
                UIViewController *viewController = nil;
                if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                    viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
                } else {
                    viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
                }
                NSLog(@"viewController screen frame : %@",NSStringFromCGRect(viewController.view.frame));
                TSDoleCoinViewController *doleCoinViewController = [[TSDoleCoinViewController alloc] init];
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                    doleCoinViewController.modalPresentationStyle = UIModalPresentationFormSheet;
                }
                doleCoinViewController.originLayer = self.originLayer;
                if(IS_IOS8 && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                    doleCoinViewController.preferredContentSize = CGSizeMake(1024, 768);
                }
                [viewController presentViewController:doleCoinViewController animated:NO completion:nil];
            }
        }
        if (self.firstRun) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kTSNotifivationFirstRunAccountViewColsed object:nil];
        }
        if (self.needLoginPostNotification == YES) {
            [((TSAccountLoginView*)self.currentDisplayView) postLoginNotification:self.recallType];
        }
    }];
}

- (void)popview:(TSPopupView *)view didClickedButton:(TSPopupButtonType)type {
    if (type == TSPopupButtonTypeOK) {
//        [self closeViewController];from JIRA-121
        [self view:self.currentDisplayView Page:TSViewIndexLogin userData:nil];
    }
}

- (IBAction)selectorCloseButton:(id)sender {
    BOOL cancelClosed = NO;
    NSString *viewName = NSStringFromClass([self.currentDisplayView class]);
    if ([viewName isEqualToString:@"TSAccountSigninView"] == YES) {
        TSAccountSigninView *signInView = (TSAccountSigninView *)self.currentDisplayView;
        if (signInView.isEdited == YES) {
            cancelClosed = YES;
        } else {
            if (signInView.needPostNotification == YES) {
                self.needLoginPostNotification = YES;
            } else {
                [self view:self.currentDisplayView Page:TSViewIndexLogin userData:nil];
                return;
            }
        }
    } else if ([viewName isEqualToString:@"TSAccountSNSSignupView"] == YES) {
        TSAccountSNSSignupView *snsSignInView = (TSAccountSNSSignupView *)self.currentDisplayView;
        if (snsSignInView.isEdited == YES) {
            cancelClosed = YES;
        } else {
            if (snsSignInView.needPostNotification == YES) {
                self.needLoginPostNotification = YES;
            } else {
                [self view:self.currentDisplayView Page:TSViewIndexLogin userData:nil];
                return;
            }
        }
    } else if ([viewName isEqualToString:@"TSAccountLoginView"] == YES) {
        TSAccountLoginView *loginView = (TSAccountLoginView *)self.currentDisplayView;
        if(loginView.forgotMode == YES) {
            [loginView restoreLoginMode];
            return;
        }
        if (loginView.needPostNotification == YES) {
            self.needLoginPostNotification = YES;
        }
    }
    
    if (cancelClosed == YES) {
        TSMessageView *messageView = [[TSMessageView alloc] initWithMessage:NSLocalizedString(@"PF_SIGNUP_POPUP_CANCEL_SIGNUP", @"Do you want to cancel sign up?")
                                                                       icon:TSPopupIconTypeWarning];
//        messageView.customButtonTitle = NSLocalizedString(@"PF_SIGNUP_POPUP_BUTTON_TITLE_OK", @"YES");
        messageView.buttonLayout = TSPopupButtonLayoutNoYes;
        messageView.delegate = self;
        [messageView showInView:self.currentDisplayView];
        [self.view endEditing:YES];
        return;
    }

    [self closeTSViewController];
}

#pragma mark - TSAccountDelegate
- (void)view:(UIView *)view Page:(TSViewIndex)index userData:(id)object {
    if (view) {//뻗지는 않겠지?
        [self.currentDisplayView removeFromSuperview];
        self.currentDisplayView = nil;
    }

    switch (index) {
        case TSViewIndexLogin: {
            self.closeButton.hidden = NO;
            TSAccountLoginView *loginView = [[TSAccountLoginView alloc] initWithFrame:getRectValue(TSViewIndexShare, TSShareSubViewRect)];
            loginView.delegate = self;
            loginView.firstRun = self.firstRun;
            loginView.originLayer =self.originLayer;
            loginView.closeButton = self.closeButton;
            [self.view addSubview:loginView];
            self.currentDisplayView = loginView;
        } break;
        case TSViewIndexSignin: {
            self.closeButton.hidden = NO;
            TSAccountSigninView *signinView = [[TSAccountSigninView alloc] initWithFrame:getRectValue(TSViewIndexShare, TSShareSubViewRect)];
            signinView.delegate = self;
            signinView.originLayer = self.originLayer;
            signinView.closeButton = self.closeButton;
            [self.view addSubview:signinView];
            self.currentDisplayView = signinView;
        } break;
        case TSViewIndexAgreement: {//SNS 가입 처리시 사용 된다. 지우지 마라....
            self.closeButton.hidden = YES;
            TSAccountAgreementView *agreeView = [[TSAccountAgreementView alloc] initWithFrame:getRectValue(TSViewIndexShare, TSShareSubViewRect)];
            agreeView.delegate = self;
            self.loginForFacebook = object;
            [self.view addSubview:agreeView];
            self.currentDisplayView = agreeView;
        } break;
        case TSViewIndexSignupSNS: {
            self.closeButton.hidden = NO;
            TSAccountSNSSignupView *snsSignUpView = [[TSAccountSNSSignupView alloc] initWithFrame:getRectValue(TSViewIndexShare, TSShareSubViewRect)];
            snsSignUpView.delegate = self;
            snsSignUpView.originLayer = self.originLayer;
            snsSignUpView.loginForFacebookData = self.loginForFacebook;
            [self.view addSubview:snsSignUpView];
            self.currentDisplayView = snsSignUpView;
        } break;
        default: { } break;
    }
    
    [self backgroundResourceLoad];
}

- (void)closeViewControllerWithView:(UIView *)view {
    [self selectorCloseButton:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)notificationPopupShow:(NSNotification *)notification {
    self.closeButton.hidden = YES;
}

- (void)notificationPopupClose:(NSNotification *)notification {
    NSString *viewName = NSStringFromClass([self.currentDisplayView class]);
    if (self.firstRun == YES && [viewName isEqualToString:@"TSAccountLoginView"] == YES) {
        return;
    }
    self.closeButton.hidden = NO;
}

- (void)noticeShowKeyboard:(id)sender {
    self.isKeyboardHide = NO;
}

- (void)noticeHideKeyboard:(id)sender {
    self.isKeyboardHide = YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (NSUInteger)application:(UIApplication*)application supportedInterfaceOrientationsForWindow:(UIWindow*)window {
    return UIInterfaceOrientationMaskPortrait;
}

//- (BOOL) shouldAutorotate {
//    return NO;
//}
//
- (BOOL)disablesAutomaticKeyboardDismissal {
    return NO;//ipad UIModalPresentationFormSheet 일때 Keyboard 안내려가는거 처리
}

@end
