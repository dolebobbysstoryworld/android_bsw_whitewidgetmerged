//
//  TSAccountLoginView.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 29..
//
//

#import <UIKit/UIKit.h>
#import "TSAccountDelegateProtocol.h"

@interface TSAccountLoginView : UIView

@property (nonatomic, assign) id<TSAccountDelegate> delegate;
@property (nonatomic, assign) BOOL firstRun;
@property (nonatomic, assign) TSOriginLayer originLayer;
@property (nonatomic, assign) BOOL forgotMode;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, assign) BOOL needPostNotification;

- (void)restoreLoginMode;
- (void)postLoginNotification:(NSInteger)recallType;

@end
