//
//  TSTitleScrollTextView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 2..
//
//

#import "TSTitleScrollTextView.h"
#import "UIImage+PathExtention.h"
#import "UIColor+RGB.h"
#import "ResolutionManager.h"

@interface TSTitleScrollTextView ()

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSURL *htmlURL;

@end

@implementation TSTitleScrollTextView

- (id)initWithFrame:(CGRect)frame Title:(NSString*)title Text:(NSString*)text {
    self = [super initWithFrame:frame];
    if (self) {
        self.title = title;
        self.text = text;
        [self addBackgroundImageWithBar:YES];
        [self addTextTitle];
        [self addTextView];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame Title:(NSString*)title URL:(NSURL*)html {
    self = [super initWithFrame:frame];
    if (self) {
        self.title = title;
        self.htmlURL = html;
//        NSLog(@"Content URL : %@", self.htmlURL);
        [self addBackgroundImageWithBar:NO];
        [self addHTMLView];
    }
    return self;
}

- (void)addBackgroundImageWithBar:(BOOL)bar {
    UIImageView *top = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"terms_box_top.png" ignore:YES]];
    top.frame = CGRectMake(0, 0, 254, 10);
    [self addSubview:top];

    UIImageView *middle = [[UIImageView alloc] initWithImage:
                           [[UIImage imageNamedEx:@"terms_box_mid.png" ignore:YES] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)]];
    middle.frame = CGRectMake(0, 10, 254, self.frame.size.height-(10*2));//
    [self addSubview:middle];
    
    UIImageView *btm = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"terms_box_btm.png" ignore:YES]];
    btm.frame = CGRectMake(0, 10+middle.frame.size.height, 254, 10);//169
    [self addSubview:btm];
    
    if (bar) {
        UIView *barView = [[UIView alloc] initWithFrame:CGRectMake(10, 38, 235, 2)];
        barView.backgroundColor = RGBH(@"#a28459");
        [self addSubview:barView];
    }
}

- (void)addTextTitle {
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 12, 235, 16)];
    titleLabel.font = [UIFont fontWithName:kDefaultFontName size:14];
    titleLabel.textColor = RGBH(@"#4f2c00");
    titleLabel.text = self.title;
    [self addSubview:titleLabel];
}

- (void)addTextView {
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 235, 0)];
    textLabel.font = [UIFont fontWithName:kDefaultFontName size:12];
    textLabel.textColor = RGBH(@"#664b2a");
    textLabel.numberOfLines = 0;
    textLabel.text = self.text;
    [textLabel sizeToFit];
    
    UIScrollView *textScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(10, 49, 235, self.frame.size.height-49-4)];//129
//    textScrollView.backgroundColor = [UIColor lightGrayColor];
    textScrollView.contentSize = textLabel.frame.size;
    textScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [textScrollView addSubview:textLabel];
    [self addSubview:textScrollView];
}

- (void)addHTMLView {
//    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(10, 49, 235, self.frame.size.height-49-4)];
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(10, 0, self.frame.size.width-20, self.frame.size.height)];
    webView.backgroundColor = [UIColor clearColor];
    [webView loadRequest:[NSURLRequest requestWithURL:self.htmlURL]];
    [self addSubview:webView];
}

@end
