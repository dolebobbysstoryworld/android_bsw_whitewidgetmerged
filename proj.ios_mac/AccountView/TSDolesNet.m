//
//  TSDolesNet.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 3..
//
//
#define GFDEBUG_ENABLE

#import "TSDolesNet.h"
#import "ResolutionManager.h"
#import "NSDate+TimeInterval.h"
#import "AppController.h"

#import "MixpanelHelper.h"

//#define GFDEBUG_ENABLE
#ifdef GFDEBUG_ENABLE
#define GFLog(s, ...)	\
NSLog(@"\n%@ Method:%s {\n%@\n}", NSStringFromClass([self class]), __PRETTY_FUNCTION__, [NSString stringWithFormat:(s), ##__VA_ARGS__]);
#else
#define GFLog( s, ... )
#endif

#pragma mark - Root Class
//NSArray *array = [NSLocale ISOCountryCodes];//국가약어
//GFLog(@"%@", array);
//array = [NSLocale ISOCurrencyCodes];//통화
//GFLog(@"%@", array);
//array = [NSLocale ISOLanguageCodes];//언어 ex:ko
//GFLog(@"%@", array);

@interface TSNet () < TSNetDelegate >

@property (nonatomic, strong) NSMutableURLRequest *request;
@property (nonatomic, strong) NSURLConnection *connection;
@property (nonatomic, strong) NSMutableData *receiveData;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *language;
@property (nonatomic, strong) NSString *clientIP;
@property (nonatomic, strong) NSString *UUID;
@property (nonatomic, strong) NSString *deviceToken;

@property (nonatomic, strong) NSDate *startDate;

@property (nonatomic, assign) BOOL updateAuthKeyMode;
@property (nonatomic, strong) TSNetUpdateAuthKey *protocolUpdateAuthKey;

@end

@implementation TSNet


+ (NSString*)languageCode {
//    NSLog(@"%@", [NSLocale preferredLanguages]);
    NSString *currentlanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
    if ([currentlanguage isEqualToString:@"en"]) {
        return @"English";
//        @"French"
//        @"German"
//        @"Portuguese"
//        @"Spanish"
//        @"Polish"
//        @"Afrikaans"
//        @"Albanian"
//        @"Arabic"
//        @"Basque"
//        @"Belarusian"
//        @"Bulgarian"
//        @"Catalan"
//        @"Chinese"
//        @"Croatian"
//        @"Czech"
//        @"Danish"
//        @"Dutch"
//        @"Estonian"
//        @"Faeroese"
//        @"Farsi"
//        @"Finnish"
//        @"Gaelic"
//        @"Greek"
//        @"Hebrew"
//        @"Hindi"
//        @"Hungarian"
//        @"Icelandic"
//        @"Indonesian"
//        @"Italian"
    } else if ([currentlanguage isEqualToString:@"ja"]) {
        return @"Japanese";
    } else if ([currentlanguage isEqualToString:@"ko"]) {
        return @"Korean";
//        @"Latvian"
//        @"Lithuanian"
//        @"Macedonian"
//        @"Malaysian"
//        @"Maltese"
//        @"Norwegian"
//        @"Rhaeto-Romanic"
//        @"Romanian"
//        @"Russian"
//        @"Sami"
//        @"Serbian"
//        @"Slovak"
//        @"Slovenian"
//        @"Sorbian"
//        @"Sutu"
//        @"Swedish"
//        @"Thai"
//        @"Tsonga"
//        @"Tswana"
//        @"Turkish"
//        @"Ukrainian"
//        @"Urdu"
//        @"Venda"
//        @"Vietnamese"
//        @"Xhosa"
//        @"Yiddish"
//        @"Zulu"
    } else {
        return @"ETC";
    }
}

- (id)init {
    self = [super init];
    if (self) {
        self.ignoreServerError = NO;
        AppController *controller = (AppController*)[UIApplication sharedApplication].delegate;
        self.deviceToken = controller.deviceTokenString;
        if ([self.deviceToken length] <= 0) {
            self.deviceToken = @"aaaabbbbccccddddeeeeffffgggghhhhiiii";
        }
        self.UUID = controller.appUUID;
        if ([self.UUID length] <= 0) {
            self.UUID = @"1111-2222-3333-4444-5555";
        }
        self.clientIP = controller.clientIP;
        if ([self.clientIP length] <= 0) {
            self.clientIP = @"4.3.2.1";
        }
        self.country = [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_COUNTRY];
        self.language = [TSNet languageCode];
        
        self.receiveData = [[NSMutableData alloc] init];
        self.request = [[NSMutableURLRequest alloc] init];
        [self.request setHTTPMethod:@"POST"];
//        [self.request setCachePolicy:NSURLRequestReloadIgnoringCacheData];
        [self.request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [self.request setTimeoutInterval:15];
    }
    return self;
}

- (void)setRequestWitData:(NSDictionary*)postDictionary Group:(NSString*)group withAPI:(NSString*)api {
//    GFLog(@"postDictionary : \n%@", postDictionary);
    NSError *error = nil;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:postDictionary options:NSJSONWritingPrettyPrinted error:&error];
    GFLog(@"~~~~~~~~~~POSTJSON : %@", [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding]);
    NSAssert(!error, [error localizedDescription]);
    
    [self.request setURL:[NSURL URLWithString:[NSString stringWithFormat:TSNET_SERVICE_URL, group, api]]];
    [self.request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[postData length]] forHTTPHeaderField:@"Content-Length"];
    [self.request setHTTPBody:postData];
}

- (void)start {
    if (self.updateAuthKeyMode == YES) {
        return;
    }
    GFLog(@"connection start!!!!!!!");
    self.startDate = [NSDate date];
    AppController *controller = (AppController*)[UIApplication sharedApplication].delegate;
    if (controller.networkAvailable == NO) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(net:didFailWithError:)]) {
            [self.delegate net:self didFailWithError:TSNetErrorNetworkDisable];
        }
        return;
    }
    
    self.connection = [[NSURLConnection alloc] initWithRequest:self.request delegate:self startImmediately:YES];
    if (self.delegate && [self.delegate respondsToSelector:@selector(net:didStartProcess:)]) {
        [self.delegate net:self didStartProcess:TSNetSuccess];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    GFLog(@"Server Connection Error-----------\n Description:%@\n userInfo:%@\n FailureReason:%@",
          [error localizedDescription],
          [error userInfo],
          [error localizedFailureReason]);
    TSNetResultType type = TSNetErrorConnection;
    if (error.code == NSURLErrorTimedOut) {
        type = TSNetErrorTimeout;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(net:didFailWithError:)]) {
        [self.delegate net:self didFailWithError:type];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    GFLog(@"didReceiveResponse : \n%@\ninterval time : %@", response, [NSDate stringFromTimeIntervalSinceDate:self.startDate]);
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    GFLog(@"didReceiveData length : %lu : \n%@\ninterval time : %@", (unsigned long)[data length], data, [NSDate stringFromTimeIntervalSinceDate:self.startDate]);
    [self.receiveData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSError *error = nil;
    NSLog(@"~~~~~~~~~~~~~~~~Receive : \n%@", [[NSString alloc] initWithData:self.receiveData encoding:NSUTF8StringEncoding]);
    id responseObject = [NSJSONSerialization JSONObjectWithData:self.receiveData options:NSJSONReadingAllowFragments error:&error];
    TSNetResultType restype = TSNetErrorResultFail;
    if (!responseObject) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(net:didEndProcess:)]) {
            [self.delegate net:self didEndProcess:restype];
        }
        //NSAssert(!error, @"JSON Object Null!!!\nERROR : %@", [error localizedDescription]);
    }
    
    TSNetDataType type = TSNetDataUnknown;
    if ([responseObject isKindOfClass:[NSArray class]]) {//이게 올일은 없겠지....
        GFLog(@"%@", (NSArray*)responseObject);
        type = TSNetDataArray;
//        NSAssert(NO, @"unknown responce data!!!!");
        return;
    }
    
    GFLog(@"responseObject : \n%@", (NSDictionary*)responseObject);

    if ([self.customRootKey length] > 0) {
        responseObject = [responseObject valueForKey:self.customRootKey];
        GFLog(@"%@ customRootKey : \n%@", self.customRootKey, (NSDictionary*)responseObject);
    }
    _returnResult = [[responseObject valueForKey:@"Return"] boolValue];
    _returnCode = [[responseObject valueForKey:@"ReturnCode"] integerValue];
    if (self.returnResult || self.ignoreServerError) {
        if (self.ignoreServerError) { GFLog(@"ignoreServerError YES!!!!!!!!!!!!!!!!"); }
        BOOL result = [self processResponceData:responseObject dataType:TSNetDataDictionary];
        if (result) {
            restype = TSNetSuccess;
        }
    }
    [[NSUserDefaults standardUserDefaults] synchronize];//tuilise_todo : applicationDidEnterBackground move code at project end
    if (self.delegate && [self.delegate respondsToSelector:@selector(net:didEndProcess:)]) {
        [self.delegate net:self didEndProcess:restype];
    }
}

- (void)setTag:(NSInteger)tag {
    _tag = tag;
}

- (NSString*)serverErrorString {
    NSString *es = nil;
#ifdef COCOS2D_DEBUG
    switch (self.returnCode) {
        case 90001:{ es = @"파라미터 오류"; } break;
        case 90002:{ es = @"해당 레코드 없음"; } break;
        case 90003:{ es = @"이력 등록 오류"; } break;
        case 90101:{ es = @"ID 중복오류"; } break;
        case 90102:{ es = @"EMAIL 중복오류"; } break;
        case 90103:{ es = @"사용자 상태변경 오류"; } break;
        case 90105:{ es = @"사용자 데이터 없음"; } break;
        case 90106:{ es = @"비밀번호확인 오류"; } break;
        case 90107:{ es = @"등록되지 않는 SNS 계정"; } break;
        case 90108:{ es = @"사용자 등록 실패"; } break;
        case 90109:{ es = @"사용자 프로필 등록 실패"; } break;
        case 90110:{ es = @"사용자 등급 등록 실패"; } break;
        case 90111:{ es = @"SNS계정 등록 실패"; } break;
        case 90114:{ es = @"사용중인 SNS 계정임"; } break;
        case 90126:{ es = @"ID 길이 오류"; } break;
        case 90127:{ es = @"PASSWORD 길이 오류"; } break;
        case 90131:{ es = @"동일 PASSWORD 오류"; } break;
        case 90202:{ es = @"인증키 유효기간 만료"; } break;
        case 90211:{ es = @"로그인 실패 횟수 초과"; } break;
        case 90213:{ es = @"오류 인증키"; } break;
        case 90214:{ es = @"인증키 정보 불일치"; } break;
            
        case 1:{ es = @"성공"; } break;
        default: {   es = @"알수 없는 오류"; } break;
    }
    es = [NSString stringWithFormat:@"%@[%ld]", es, (long)self.returnCode];
#else
    es = [NSString stringWithFormat:@"ERROR CODE [%d]", self.returnCode];
#endif
    return es;
}

- (BOOL)processResponceData:(NSDictionary*)responseObject dataType:(TSNetDataType)type { return YES; }

- (void)updateAuthKey {
    self.updateAuthKeyMode = YES;
    self.protocolUpdateAuthKey = [[TSNetUpdateAuthKey alloc] init];
    self.protocolUpdateAuthKey.delegate = self;
    [self.protocolUpdateAuthKey start];
}

- (void)net:(TSNet*)netObject didStartProcess:(TSNetResultType)result {
    if (result != TSNetSuccess) {
        self.updateAuthKeyMode = NO;
        if (self.delegate && [self.delegate respondsToSelector:@selector(net:didStartProcess:)]) {
            _returnResult = netObject.returnResult;
            _returnCode = netObject.returnCode;
            [self.delegate net:self didStartProcess:result];
        }
    } else {
        if (self.delegate && [self.delegate respondsToSelector:@selector(net:didStartProcess:)]) {
            [self.delegate net:self didStartProcess:result];
        }
    }
}

- (void)net:(TSNet*)netObject didEndProcess:(TSNetResultType)result {
    if (result != TSNetSuccess) {
        self.updateAuthKeyMode = NO;
        if (self.delegate && [self.delegate respondsToSelector:@selector(net:didFailWithError:)]) {
            _returnResult = netObject.returnResult;
            _returnCode = netObject.returnCode;
            [self.delegate net:self didEndProcess:result];
        }
    } else {
        self.updateAuthKeyMode = NO;
        [self didUpdatedAutkKey];
        [self start];
    }
}

- (void)net:(TSNet*)netObject didFailWithError:(TSNetResultType)result {
    self.updateAuthKeyMode = NO;
    if (self.delegate && [self.delegate respondsToSelector:@selector(net:didFailWithError:)]) {
        _returnResult = netObject.returnResult;
        _returnCode = netObject.returnCode;
        [self.delegate net:self didFailWithError:result];
    }
}

- (void)didUpdatedAutkKey
{

}

@end

#pragma mark - Profile Group

@interface TSNetSignup ()

@property (nonatomic, assign) BOOL snsSignUp;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, assign) NSInteger gender;
@property (nonatomic, strong) NSString *birthYear;
@property (nonatomic, strong) NSString *city;

@end

@implementation TSNetSignup

- (id)initWithEmail:(NSString*)email Password:(NSString *)password Gender:(NSInteger)gender
           BirthDay:(NSString*)birthDay AddressMain:(NSString*)addressMain countryCode:(NSString*)code {//tuilise_todo : SNS 가입 처리 추가로 해야 한다.
    self = [super init];
    if (self) {
        self.snsSignUp = NO;
        self.email = email;
        self.password = password;
        self.gender = gender;
        self.birthYear = birthDay;
        if (!addressMain) {
            addressMain = @"";
        }
        self.city = addressMain;
        self.country = code;
        NSDictionary* postDictionary =
        @{ @"UserID"                : email,
           @"Email"                 : email,
           @"Password"              : password,
           @"Gender"                : [NSNumber numberWithInteger:gender],
           @"BirthDay"              : [NSString stringWithFormat:@"%@-12-31", birthDay],
               //[NSString stringWithFormat:@"%@-01-01", birthDay],//tuilise_todo : 날짜 포멧이 월,일까지 입력되지 않으면 400 오류가 난다. 확인 필요
           @"Language"              : self.language,
           @"SnsCode"               : @"",
           @"SnsID"                 : @"",
           @"SnsUserName"           : @"",
           @"AddressMain"           : addressMain,
           @"StandardCountryCode"   : self.country,
           @"UUID"                  : self.UUID,
           @"ClientIP"              : self.clientIP };
        [self setRequestWitData:postDictionary Group:APIGROUP_PROFILE withAPI:APIPROFILE_SIGNUP];
    }
    return self;
}

- (id)initWithSNSEmail:(NSString*)email snsID:(NSString*)snsid name:(NSString*)name
                Gender:(NSInteger)gender BirthDay:(NSString*)birthDay AddressMain:(NSString*)addressMain countryCode:(NSString*)code {//tuilise_todo : SNS 가입 처리 추가로 해야 한다.
    self = [super init];
    if (self) {
        self.snsSignUp = YES;
        self.email = email;
        self.gender = gender;
        self.birthYear = birthDay;
        if (!addressMain) {
            addressMain = @"";
        }
        self.city = addressMain;
        self.country = code;
        NSDictionary* postDictionary =
        @{ @"UserID"                : email,
           @"Email"                 : email,
           @"Password"              : @"",
           @"Gender"                : [NSNumber numberWithInteger:gender],
           @"BirthDay"              : [NSString stringWithFormat:@"%@-12-31", birthDay],
               //[NSString stringWithFormat:@"%@-01-01", birthDay],//tuilise_todo : 날짜 포멧이 월,일까지 입력되지 않으면 400 오류가 난다. 확인 필요
           @"Language"              : self.language,
           @"SnsCode"               : @"SNS001",
           @"SnsID"                 : snsid,
           @"SnsUserName"           : name,
           @"AddressMain"           : addressMain,
           @"StandardCountryCode"   : self.country,
           @"UUID"                  : self.UUID,
           @"ClientIP"              : self.clientIP };
        [self setRequestWitData:postDictionary Group:APIGROUP_PROFILE withAPI:APIPROFILE_SIGNUP];
    }
    return self;
}

- (BOOL)processResponceData:(NSDictionary*)responseObject dataType:(TSNetDataType)type {
    BOOL result = YES;
    if (self.snsSignUp == YES) {
        if (self.returnCode != 90102 && self.returnCode != 1) {
            result = NO;
        }
    }
    
    self.userNumber = [[responseObject valueForKey:@"UserNo"] integerValue];
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInteger:self.userNumber] forKey:UD_INTEGER_USERNO];
    [[NSUserDefaults standardUserDefaults] setValue:self.email forKey:UD_STRING_EMAIL];
    [[NSUserDefaults standardUserDefaults] setValue:self.email forKey:UD_STRING_LOGINID];
    [[NSUserDefaults standardUserDefaults] setValue:self.birthYear forKey:UD_STRING_BIRTHDAY];
    [[NSUserDefaults standardUserDefaults] setValue:self.password forKey:UD_STRING_LOGINPW];
    [[NSUserDefaults standardUserDefaults] setValue:self.city forKey:UD_STRING_CITY];
    [[NSUserDefaults standardUserDefaults] setValue:self.country forKeyPath:UD_STRING_COUNTRY];
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInteger:self.gender] forKey:UD_INTEGER_GENDER];
    
    [MixpanelHelper signUpWithUserNumber:self.userNumber
                                   email:self.email
                                  gender:self.gender
                               birthyear:self.birthYear];
    
    return result;
}

@end

@interface TSNetWithDraw ()

@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *reason;

@end

@implementation TSNetWithDraw

- (id)initWithPassword:(NSString*)password Reason:(NSString*)reason {
    self = [super init];
    
    if (self) {
        self.password = password;
        self.reason = reason;
        [super updateAuthKey];
    }
    return self;
}

- (void)didUpdatedAutkKey {
    NSDictionary* postDictionary =
    @{ @"UserNo"    : [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:UD_INTEGER_USERNO]],
       @"UserID"    : [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_LOGINID],
       @"AuthKey"   : [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_AUTHKEY],
       @"Password"  : self.password,
       @"Reason"    : self.reason,
       @"ClientIP"  : self.clientIP };
    [self setRequestWitData:postDictionary Group:APIGROUP_PROFILE withAPI:APIPROFILE_WITHDRAW];
}

@end

@implementation TSNetFindPassword

- (id)initWithEmail:(NSString*)string {
    self = [super init];
    
    if (self) {
        NSDictionary* postDictionary =
        @{ @"UserID"    : string,
           @"Email"     : string,
           @"ClientIP"  : self.clientIP };
        [self setRequestWitData:postDictionary Group:APIGROUP_PROFILE withAPI:APIPROFILE_FINDPW];
    }
    return self;
}

@end

@implementation TSNetGetProfile

- (id)init {
    self = [super init];
    if (self) {
        NSDictionary* postDictionary =
        @{ @"UserNo"    : [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:UD_INTEGER_USERNO]],
           @"AuthKey"   : [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_AUTHKEY],
           @"ClientIP"  : self.clientIP };
        [self setRequestWitData:postDictionary Group:APIGROUP_PROFILE withAPI:APIPROFILE_GETPROFILE];
    }
    return self;
}

//- (BOOL)processResponceData:(NSDictionary*)responseObject dataType:(TSNetDataType)type {
//    self.addressMain = [responseObject valueForKey:@"AddressMain"];
//    self.birthDay = [responseObject valueForKey:@"BirthDay"];
//    self.changePasswordDateTime = [responseObject valueForKey:@"ChangePasswordDateTime"];
//    self.country = [responseObject valueForKey:@"Country"];
//    self.email = [responseObject valueForKey:@"Email"];
//    self.emailActivateDateTime = [responseObject valueForKey:@"EmailActivateDateTime"];
//    self.emailVarification = [[responseObject valueForKey:@"EmailVarification"] boolValue];
//    self.gender = [[responseObject valueForKey:@"Gender"] integerValue];
//    self.language = [responseObject valueForKey:@"Language"];
//    self.nickname = [responseObject valueForKey:@"Nickname"];
//    self.registerDateTIme = [responseObject valueForKey:@"RegisterDateTime"];
//    self.userID = [responseObject valueForKey:@"UserID"];
//    
//    NSArray *array = [self.birthDay componentsSeparatedByString:@"-"];
//    NSString *birthYear = [array objectAtIndex:0];
//    [[NSUserDefaults standardUserDefaults] setValue:birthYear forKey:UD_STRING_BIRTHDAY];
//    [[NSUserDefaults standardUserDefaults] setValue:self.addressMain forKey:UD_STRING_CITY];
//    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInteger:self.gender] forKey:UD_INTEGER_GENDER];
//    [[NSUserDefaults standardUserDefaults] setValue:self.country forKeyPath:UD_STRING_COUNTRY];
//    
//    // log sns here
//    
//    return true;
//}

- (BOOL)processResponceData:(NSDictionary*)responseObject dataType:(TSNetDataType)type {
    self.addressMain = [responseObject valueForKey:@"AddressMain"];
    if (self.addressMain.class == [NSNull class])
    {
        self.addressMain = @"";
    }
    self.birthDay = [responseObject valueForKey:@"BirthDay"];
    self.changePasswordDateTime = [responseObject valueForKey:@"ChangePasswordDateTime"];
    self.country = [responseObject valueForKey:@"Country"];
    self.email = [responseObject valueForKey:@"Email"];
    self.emailActivateDateTime = [responseObject valueForKey:@"EmailActivateDateTime"];
    self.emailVarification = [[responseObject valueForKey:@"EmailVarification"] boolValue];
    self.gender = [[responseObject valueForKey:@"Gender"] integerValue];
    self.language = [responseObject valueForKey:@"Language"];
    self.nickname = [responseObject valueForKey:@"Nickname"];
    self.registerDateTIme = [responseObject valueForKey:@"RegisterDateTime"];
    self.userID = [responseObject valueForKey:@"UserID"];
    
    NSString *birthYear = nil;
    if (self.birthDay && ![self.birthDay isKindOfClass:[NSNull class]])
    {
        NSArray *array = nil;
        array = [self.birthDay componentsSeparatedByString:@"-"];
        birthYear = [array objectAtIndex:0];
    } else
    {
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
        NSInteger year = [components year];
        birthYear = [NSString stringWithFormat:@"%ld",(long)year];
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:birthYear forKey:UD_STRING_BIRTHDAY];
    [[NSUserDefaults standardUserDefaults] setValue:self.addressMain forKey:UD_STRING_CITY];
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInteger:self.gender] forKey:UD_INTEGER_GENDER];
    if (self.country.class == [NSNull class])
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"Other" forKeyPath:UD_STRING_COUNTRY];
    }
    else
        [[NSUserDefaults standardUserDefaults] setValue:self.country forKeyPath:UD_STRING_COUNTRY];
    
    [MixpanelHelper updateUserWithUserNumber:[[NSUserDefaults standardUserDefaults] valueForKey:UD_INTEGER_USERNO]
                                       email:self.email
                                      gender:self.gender
                                    birthyear:self.birthDay];
    
    return true;
}


@end

@interface TSNetModifyProfile ()

@property (nonatomic, strong) NSString *changedPassword;
@property (nonatomic, strong) NSString *birthYear;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, assign) NSInteger gender;

@end

@implementation TSNetModifyProfile

- (id)initWithPassword:(NSString*)password newPassword:(NSString*)newPassword
             birthYear:(NSString*)birthYear address:(NSString*)address gender:(NSInteger)gender countryCode:(NSString*)code {
    self = [super init];
    if (self) {
        self.changedPassword = newPassword;
        if (self.changedPassword == nil) {
            self.changedPassword = @"";
        }
        self.birthYear = birthYear;
        self.address = address;
        self.gender = gender;
        self.country = code;
        NSDictionary* postDictionary =
        @{ @"UserNo"        : [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:UD_INTEGER_USERNO]],
           @"UserID"        : [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_LOGINID],
           @"Country"       : self.country,
           @"Language"      : self.language,
           @"Gender"        : [NSString stringWithFormat:@"%ld", (long)gender],
           @"BirthDay"      : [NSString stringWithFormat:@"%@-12-31", birthYear],//[NSString stringWithFormat:@"%@-01-01", birthYear],
           @"OldPassword"   : password,
           @"NewPassword"   : self.changedPassword,
           @"AddressMain"   : address,
           @"ClientIP"      : self.clientIP };
        [self setRequestWitData:postDictionary Group:APIGROUP_PROFILE withAPI:APIPROFILE_MODIFYPROFILE];
    }
    return self;
}

- (BOOL)processResponceData:(NSDictionary*)responseObject dataType:(TSNetDataType)type {
    [[NSUserDefaults standardUserDefaults] setValue:self.changedPassword forKey:UD_STRING_LOGINPW];
    [[NSUserDefaults standardUserDefaults] setValue:self.birthYear forKey:UD_STRING_BIRTHDAY];
    [[NSUserDefaults standardUserDefaults] setValue:self.address forKey:UD_STRING_CITY];
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInteger:self.gender] forKey:UD_INTEGER_GENDER];
    [[NSUserDefaults standardUserDefaults] setValue:self.country forKeyPath:UD_STRING_COUNTRY];
    
    [MixpanelHelper modifyUserWithUserNumber:[[NSUserDefaults standardUserDefaults] valueForKey:UD_INTEGER_USERNO]
                                      gender:self.gender
                                   birthyear:self.birthYear];
    
    return true;
}

@end

@interface TSNetModifySNSProfile ()

@property (nonatomic, strong) NSString *birthYear;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, assign) NSInteger gender;

@end

@implementation TSNetModifySNSProfile

- (id)initWithBirthYear:(NSString*)birthYear address:(NSString*)address gender:(NSInteger)gender countryCode:(NSString*)code {
    self = [super init];
    if (self) {
        self.birthYear = birthYear;
        self.address = address;
        self.gender = gender;
        self.country = code;
        NSDictionary* postDictionary =
        @{ @"UserNo"        : [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:UD_INTEGER_USERNO]],
           @"UserID"        : [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_LOGINID],//tuilise_todo : ID가 이게 맞는지 확인 필요
           @"Country"       : self.country,
           @"Language"      : self.language,
           @"Gender"        : [NSString stringWithFormat:@"%ld", (long)gender],
           @"BirthDay"      : [NSString stringWithFormat:@"%@-12-31", birthYear],//[NSString stringWithFormat:@"%@-01-01", birthYear],
           @"AddressMain"   : address,
           @"ClientIP"      : self.clientIP };
        [self setRequestWitData:postDictionary Group:APIGROUP_PROFILE withAPI:APIPROFILE_MODIFYSNSPROFILE];
    }
    return self;
}

- (BOOL)processResponceData:(NSDictionary*)responseObject dataType:(TSNetDataType)type {
    [[NSUserDefaults standardUserDefaults] setValue:self.birthYear forKey:UD_STRING_BIRTHDAY];
    [[NSUserDefaults standardUserDefaults] setValue:self.address forKey:UD_STRING_CITY];
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInteger:self.gender] forKey:UD_INTEGER_GENDER];
    [[NSUserDefaults standardUserDefaults] setValue:self.country forKeyPath:UD_STRING_COUNTRY];
    return true;
}

@end

@implementation TSNetProvince

- (id)initWithCountryCode:(NSString *)code {
    self = [super init];
    if (self) {
        self.country = code;
        NSDictionary* postDictionary =
        @{ @"StandardCountryCode"   : self.country,
           @"ClientIP"              : self.clientIP };
        [self setRequestWitData:postDictionary Group:APIGROUP_PROFILE withAPI:APIPROFILE_GETPROVINCE];
    }
    return self;
}

- (BOOL)processResponceData:(NSDictionary*)responseObject dataType:(TSNetDataType)type {
    self.provinces = [responseObject valueForKey:@"Provinces"];
    return true;
}

@end

@interface TSNetDisconnectSNS ()

@property (nonatomic, strong) NSString *snsid;

@end

@implementation TSNetDisconnectSNS

-(id)initWithID:(NSString*)snsid reason:(NSString*)reason {
    self = [super init];
    if (self) {
        self.snsid = snsid;
        [super updateAuthKey];
    }
    return self;
}

- (void)didUpdatedAutkKey {
    NSDictionary* postDictionary =
    @{ @"UserNo"    : [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:UD_INTEGER_USERNO]],
       @"AuthKey"   : [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_AUTHKEY],
       @"SnsCode"   : @"SNS001",
       @"SnsID"     : self.snsid,
       @"clientIP"  : self.clientIP };
    [self setRequestWitData:postDictionary Group:APIGROUP_PROFILE withAPI:@"DisconnectSNS"];
}

- (BOOL)processResponceData:(NSDictionary*)responseObject dataType:(TSNetDataType)type {
    return true;
}

@end

@implementation TSNetGetAppVersion

- (id)init {
    self = [super init];
    if (self) {
        NSDictionary* postDictionary =
        @{ @"ServiceCode"   : @"SVR004",
           @"MobileOS"      : @"iOS",
           @"ClientIP"      : self.clientIP };
        [self setRequestWitData:postDictionary Group:APIGROUP_PROFILE withAPI:APIPROFILE_GETAPPVERSION];
    }
    return self;
}

- (BOOL)processResponceData:(NSDictionary*)responseObject dataType:(TSNetDataType)type {
    self.appURL = [responseObject valueForKey:@"AppURL"];
    self.majorVersion = [responseObject valueForKey:@"MajorVersion"];
    return true;
}

@end

#pragma mark - Auth Group
@interface TSNetLogin ()

@property (nonatomic, strong) NSString *idString;
@property (nonatomic, strong) NSString *password;

@end

@implementation TSNetLogin

- (id)initWithID:(NSString *)idString Password:(NSString*)password {
    self = [super init];
    if (self) {
        self.idString = idString;
        self.password = password;
        
        if (self.idString == nil || self.password == nil) {
            return nil;
        }
        NSDictionary* postDictionary =
        @{  @"UserID"        : self.idString,
            @"Password"      : self.password,
            @"ServiceCode"   : TSNET_SERVICE_CODE,
            @"MobileOS"      : @"iOS",
            @"DeviceToken"   : self.deviceToken,
            @"UUID"          : self.UUID,
            @"ClientIP"      : self.clientIP };
        [self setRequestWitData:postDictionary Group:APIGROUP_AUTH withAPI:APIAUTH_LOGIN];
    }
    return self;
}

- (BOOL)processResponceData:(NSDictionary*)responseObject dataType:(TSNetDataType)type {
    NSString *authKey = [responseObject valueForKey:@"AuthKey"];
    NSInteger userNumber = [[responseObject valueForKey:@"UserNo"] integerValue];
    
    [[NSUserDefaults standardUserDefaults] setValue:authKey forKey:UD_STRING_AUTHKEY];
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInteger:userNumber] forKey:UD_INTEGER_USERNO];
    [[NSUserDefaults standardUserDefaults] setValue:self.idString forKey:UD_STRING_LOGINID];
    [[NSUserDefaults standardUserDefaults] setValue:self.idString forKey:UD_STRING_EMAIL];
    [[NSUserDefaults standardUserDefaults] setValue:self.password forKey:UD_STRING_LOGINPW];
    
    [MixpanelHelper loginWithUserNumber:userNumber email:self.idString];
    
    return YES;
}

@end

@implementation TSNetSNSLogin

- (id)initWithSNSID:(NSString *)snsID {
    self = [super init];
    if (self) {
//        NSDictionary* postDictionary =
//        @{ @"UUID"          : self.UUID,
//           @"SNSType"       : @"SNS001",
//           @"SNSID"         : snsID,
//           @"DeviceToken"   : self.deviceToken,
//           @"ClientIP"      : self.clientIP };
//        [self setRequestWitData:postDictionary Group:APIGROUP_AUTH withAPI:APIAUTH_SNSLOGIN];
        NSDictionary* postDictionary =
        @{ @"UUID"          : self.UUID,
           @"SNSType"       : @"SNS001",
           @"SNSID"         : snsID,
           @"DeviceToken"   : self.deviceToken,
           @"ClientIP"      : self.clientIP,
           @"MobileOS"      : @"iOS",
           @"ServiceCode"   : TSNET_SERVICE_CODE,
           };
        [self setRequestWitData:postDictionary Group:APIGROUP_AUTH withAPI:@"SNSLoginV2"];
    }
    return self;
}

- (BOOL)processResponceData:(NSDictionary*)responseObject dataType:(TSNetDataType)type {
    NSString *authKey = [responseObject valueForKey:@"AuthKey"];
    NSInteger userNumber = [[responseObject valueForKey:@"UserNo"] integerValue];
    
    [[NSUserDefaults standardUserDefaults] setValue:authKey forKey:UD_STRING_AUTHKEY];
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInteger:userNumber] forKey:UD_INTEGER_USERNO];
    
    // sign up with sns
    [MixpanelHelper signUpSNSWithUserNumber:userNumber];
    
    return YES;
}

@end

@implementation TSNetUpdateAuthKey

- (id)init {
    self = [super init];
    if (self) {
        NSDictionary* postDictionary =
        @{ @"UserNo"        : [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:UD_INTEGER_USERNO]],
           @"AuthKey"       : [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_AUTHKEY],
           @"ServiceCode"   : TSNET_SERVICE_CODE,
           @"MobileOS"      : @"iOS",
           @"DeviceToken"   : self.deviceToken,
           @"UUID"          : self.UUID,
           @"ClientIP"      : self.clientIP };
        [self setRequestWitData:postDictionary Group:APIGROUP_AUTH withAPI:@"GetReNewAuthKey"];
    }
    
    return self;
}

- (BOOL)processResponceData:(NSDictionary*)responseObject dataType:(TSNetDataType)type {
    NSString *authKey = [responseObject valueForKey:@"AuthKey"];
    [[NSUserDefaults standardUserDefaults] setValue:authKey forKey:UD_STRING_AUTHKEY];
    return YES;
}

@end

#pragma mark - Coupon group
@implementation TSNetCheckCoupon

- (id)initWithSerial:(NSString *)qrString {
    self = [super init];
    if (self) {
        self.customRootKey = @"CheckMobileCouponResult";
        NSDictionary* postDictionary =
        @{ @"ServiceCode"   : TSNET_SERVICE_CODE,
           @"UserNo"        : [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:UD_INTEGER_USERNO]],
           @"Serial"        : qrString,
           @"UUID"          : self.UUID,
           @"PromotionInfo" : @"1",
           @"ClientIP"      : self.clientIP };
        [self setRequestWitData:postDictionary Group:APIGROUP_COUPON withAPI:APICOUPON_CHECKCOUPON];
    }
    return self;
}

- (BOOL)processResponceData:(NSDictionary*)responseObject dataType:(TSNetDataType)type
{
    NSLog(@"~~~~~~~~~~~~~~~CheckMobileCoupon response object: \n%@", responseObject);
    
    self.canUsable = [[responseObject valueForKey:@"CanUse"] boolValue];
    GFLog(@"ProductInfos:%@", [responseObject valueForKey:@"ProductInfos"]);
    NSArray *productInfos = [responseObject valueForKey:@"ProductInfos"];
    if (productInfos)
    {
        NSDictionary *productInfo = [productInfos objectAtIndex:0];
        if (productInfo)
        {
            NSNumber *value = [productInfo valueForKey:@"Value"];
            if (value)
            {
                self.coinValue =[value integerValue];
            }
            else
            {
                self.coinValue = 500;
            }
        }
        else
        {
            self.coinValue = 500;
        }
    }
    else
    {
        self.coinValue = 500;
    }
    return YES;
}

@end

#pragma mark - Billing group
@implementation TSNetGetBalance

- (id)init {
    self = [super init];
    if (self) {
        NSDictionary* postDictionary =
        @{ @"UserNo"        : [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:UD_INTEGER_USERNO]],
           @"ClientIP"      : self.clientIP };
        [self setRequestWitData:postDictionary Group:APIGROUP_BILLINGW withAPI:APIBILLINGW_GETBALANCE];
    }
    return self;
}

- (BOOL)processResponceData:(NSDictionary*)responseObject dataType:(TSNetDataType)type {
    self.eventBalance = [NSString stringWithFormat:@"%ld", (long)[[responseObject valueForKey:@"EventBalance"] integerValue]];
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInteger:self.eventBalance] forKey:UD_INTEGER_BALANCE];
    return YES;
}

@end

@interface TSNetUseCoinCoupon ()

@property (nonatomic, strong) NSString *qrString;

@end

#include <time.h>
@implementation TSNetUseCoinCoupon

- (id)initWithSerial:(NSString *)qrString {
    self = [super init];
    if (self) {
        self.qrString = qrString;
        [super updateAuthKey];
    }
    return self;
}

- (void)didUpdatedAutkKey {
    NSString *order = [NSString stringWithFormat:@"%ld_%ld", (long)[[[NSUserDefaults standardUserDefaults] valueForKey:UD_INTEGER_USERNO] integerValue], time(NULL)];
    NSDictionary* postDictionary =
    @{ @"UserNo"    : [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:UD_INTEGER_USERNO]],
       @"AuthKey"   : [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_AUTHKEY],
       @"OrderID"   : order,
       @"UUID"      : self.UUID,
       @"Serial"    : self.qrString,
       @"ClientIP"  : self.clientIP };
    [self setRequestWitData:postDictionary Group:APIGROUP_BILLINGC withAPI:APIBILLINGC_USECOINCOUPON];
}

@end

@implementation TSNetGetPurchaseList

- (id)initWithUserNo:(NSInteger)userNo begin:(NSDate*)beginDate end:(NSDate*)endDate {
    self = [super init];
    if (self) {
        NSDateFormatter *formatter;
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSString *startDay = [formatter stringFromDate:beginDate];
        NSString *endDay = [formatter stringFromDate:endDate];
        
        NSDictionary* postDictionary =
        @{ @"UserNo"    : [NSString stringWithFormat:@"%d", (int)userNo],
           @"BeginDate" : startDay,
           @"EndDate"   : endDay,
           @"PageIndex" : [NSNumber numberWithInteger:1],
           @"RowPerPage": [NSNumber numberWithInteger:30],
           @"ClientIP"  : self.clientIP };
        [self setRequestWitData:postDictionary Group:@"Billing/Purchase" withAPI:@"GetPurchaseList"];
    }
    return self;
}

- (BOOL)processResponceData:(NSDictionary*)responseObject dataType:(TSNetDataType)type {
    NSArray *arrayFromJson = [responseObject valueForKey:@"PurchaseList"];
    NSArray *array = [NSArray arrayWithArray:arrayFromJson];
    self.purchaseList = array;
    GFLog(@"purchaseList\n%@", self.purchaseList);
    return YES;
}

@end

#pragma mark - CS group
@implementation TSNetRegisterCS

- (id)initWithEmail:(NSString*)email description:(NSString*)description deviceInfo:(NSString*)device {
    self = [super init];
    if (self) {
        NSDateFormatter *formatter;
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        NSString *dateString = [formatter stringFromDate:[NSDate date]];
        NSString *userNoString = [NSString stringWithFormat:@"%d",
                                  (int)[[[NSUserDefaults standardUserDefaults] valueForKey:UD_INTEGER_USERNO] integerValue]];
        NSString *userID = [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_LOGINID];
        if (userID == nil) {
            userID = email;
        }
        NSDictionary* postDictionary =
        @{ @"UserNo"                : userNoString,
           @"UserID"                : userID,
           @"Email"                 : email,
           @"ISReceiveEmail"        : @"true",
           @"CategoryNo"            : @41,
           @"SubCategoryNo"         : @167,
           @"InquiryTitle"          : NSLocalizedString(@"PF_ABOUT_APPTITLE", @"Bobby’s Story World"),
           @"InquiryContent"        : description,//tuilise_todo : 나머지 값을 어쩌나?
           @"ServiceCode"           : TSNET_SERVICE_CODE,
           @"OccurrenceDateTime"    : dateString,
           @"Description"           : device,
           @"ClientIP"              : self.clientIP };
        [self setRequestWitData:postDictionary Group:APIGROUP_CS withAPI:APICS_REGISTERCS];
    }
    return self;
}

@end