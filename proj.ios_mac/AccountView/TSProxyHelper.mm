//
//  TSProxyHelper.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 19..
//
//

#import "TSProxyHelper.h"
#import "ResolutionManager.h"
#import "HomeScene.h"
#import "Define.h"
#import "SimpleAudioEngine.h"
#import "BookMapScene.h"
#import "TSAccountViewController.h"
#import "CharacterRecordingScene.h"
#import "BGMManager.h"

#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

NSString *const kTSNotifivationLoginDidFinished                = @"kTSNotifivationLoginDidFinished";
NSString *const kTSNotifivationRecallLoginDidFinished          = @"kTSNotifivationRecallLoginDidFinished";
NSString *const kTSNotifivationSetMute                         = @"kTSNotifivationSetMute";
NSString *const kTSNotifivationUseCoinPassword                 = @"kTSNotifivationUseCoinPassword";
NSString *const kTSNotifivationShare                           = @"kTSNotifivationShare";
NSString *const kTSNotifivationDoleCoinUpdate                  = @"kTSNotifivationDoleCoinUpdate";
NSString *const kTSNotifivationFirstRunAccountViewColsed       = @"kTSNotifivationFirstRunAccountViewColsed";

@implementation TSProxyHelper

+ (TSProxyHelper *)sharedInstance {
    static TSProxyHelper *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}

- (id)init {
    self = [super init];
    if (self) {

    }
    return self;
}

- (void)addNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotificationForLoginResult:)
                                                 name:kTSNotifivationRecallLoginDidFinished
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotificationForSetMute:)
                                                 name:kTSNotifivationSetMute
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotificationForUseCoinPassword:)
                                                 name:kTSNotifivationUseCoinPassword
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotificationForShare:)
                                                 name:kTSNotifivationShare
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotificationForUpdateDoleCoin:)
                                                 name:kTSNotifivationDoleCoinUpdate
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotificationForFirstRun:)
                                                 name:kTSNotifivationFirstRunAccountViewColsed
                                               object:nil];
}

- (void)removeNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTSNotifivationRecallLoginDidFinished object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTSNotifivationSetMute object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTSNotifivationUseCoinPassword object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTSNotifivationShare object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTSNotifivationDoleCoinUpdate object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTSNotifivationFirstRunAccountViewColsed object:nil];
}

- (void)receiveNotificationForLoginResult:(NSNotification*)notification {
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:NO] forKey:@UDKEY_BOOL_DELETEACCOUNT];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:UD_BOOL_FACEBOOKLOGIN] boolValue] == NO) {
        [tracker set:kGAIScreenName value:@"BobbyStoryWorld_IOS_Account_Login"];
    } else {
        [tracker set:kGAIScreenName value:@"BobbyStoryWorld_IOS_Account_FacebookLogin"];
    }
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    TSOriginLayer originLayer = (TSOriginLayer)[[notification.userInfo objectForKey:@"originlayer"] integerValue];
    TSRecall recallType = (TSRecall)[[notification.userInfo objectForKey:@"recalltype"] integerValue];
    Home *homeLayer = NULL;
    BookMap *bookMapLayer = NULL;
    CharacterRecording *chareclayer = NULL;
    if (originLayer == TSOriginLayerHome) {
        cocos2d::Scene *homeScene = cocos2d::Director::getInstance()->getRunningScene();
        if (!homeScene) {
            NSLog(@"runningScene is empty");
            return;
        }
        homeLayer = (Home *)homeScene->getChildByTag(TAG_HOME);//(Home*)self.homeLayer;
        if (!homeLayer) {
            NSLog(@"runningScene is not homescene");
            return;
        }
    } else if (originLayer == TSOriginLayerBookMap) {
        cocos2d::Scene *bookMapScene = cocos2d::Director::getInstance()->getRunningScene();
        if (!bookMapScene) {
            NSLog(@"runningScene is empty");
            return;
        }
        bookMapLayer = (BookMap *)bookMapScene->getChildByTag(TAG_BOOKMAP);//(Home*)self.homeLayer;
        if (!bookMapLayer) {
            NSLog(@"runningScene is not BookMapScene");
            return;
        }
        homeLayer = bookMapLayer->getHome();
        if (!homeLayer) {
            NSLog(@"homeLayer empty");
            return;
        }
    } else if (originLayer == TSOriginLayerCharacterRecording) {
        cocos2d::Scene *charecScene = cocos2d::Director::getInstance()->getRunningScene();
        if (!charecScene) {
            NSLog(@"runningScene is empty");
            return;
        }
        chareclayer = (CharacterRecording *)charecScene->getChildByTag(TAG_RECORDING);//(Home*)self.homeLayer;
        if (!chareclayer) {
            NSLog(@"runningScene is not CharacterRecordingScene");
            return;
        }
        homeLayer = chareclayer->getHome();
        if (!homeLayer) {
            NSLog(@"homeLayer empty");
            return;
        }
        if (originLayer == TSOriginLayerCharacterRecording) {
            if (recallType == TSRecallCharRECUnlockItem) {
                chareclayer->checkRecall(recallType);
                recallType = TSRecallNone;
            }
        }
    } else {
        NSLog(@"Unknown Origin Layer !!!!!!!!!!!!!!!");
    }
    
    if (homeLayer) {
        homeLayer->doleCoinGetBalanceProtocol();
//        homeLayer->doleCacheSyncVersionProtocol(); //로그인이 필요 없어진 관계로 app 실행 시 바로 호출됨. 2014.09.18
        homeLayer->doleGetInventoryListProtocol();//위의 처리시 삭제 되면서 로그인 시 업데이트 처리 해야 함. 2014.09.18
    }
    if (recallType > TSRecallNone) {
        if (originLayer == TSOriginLayerHome) {
            homeLayer->checkRecall(recallType);
        } else if (originLayer == TSOriginLayerBookMap) {
            bookMapLayer->checkRecall(recallType);
        } else if (originLayer == TSOriginLayerCharacterRecording) {
            chareclayer->checkRecall(recallType);
        }
    }
}

- (void)receiveNotificationForSetMute:(NSNotification *)notifocation {
    NSLog(@"UserInfo : %@", notifocation.userInfo);
    BOOL soundOn = [[notifocation.userInfo objectForKey:@"volume"] boolValue];
    if (soundOn == YES) {
//        float volume = [[[NSUserDefaults standardUserDefaults] valueForKey:UD_FLOAT_VOLUME] floatValue];
//        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithFloat:DEFAULT_VOLUME_BACKGROUND_SOUND] forKeyPath:UD_FLOAT_VOLUME];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(DEFAULT_VOLUME_BACKGROUND_SOUND);
//        CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(DEFAULT_VOLUME_BACKGROUND_SOUND);
        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:NO] forKeyPath:UD_BOOL_MUTEBGM];
        [[NSUserDefaults standardUserDefaults] synchronize];
        TSBGMManager::getInstance()->setMute(false);
        TSBGMManager::getInstance()->play();
    } else {
//        float volume = CocosDenshion::SimpleAudioEngine::getInstance()->getBackgroundMusicVolume();
//        float effectVolume = CocosDenshion::SimpleAudioEngine::getInstance()->getEffectsVolume();
//        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithFloat:0.0f] forKeyPath:UD_FLOAT_VOLUME];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(0.0f);
//        CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(0.0f);
        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKeyPath:UD_BOOL_MUTEBGM];
        [[NSUserDefaults standardUserDefaults] synchronize];
        TSBGMManager::getInstance()->setMute(true);
        TSBGMManager::getInstance()->stop();
    }
    return;
}

- (void)receiveNotificationForUseCoinPassword:(NSNotification *)notifocation {
    NSLog(@"UserInfo : %@", notifocation.userInfo);
    cocos2d::Scene *homeScene = cocos2d::Director::getInstance()->getRunningScene();
    if (!homeScene) {
        NSAssert(NO, @"runningScene is empty");
        return;
    }
    Home *homeLayer = (Home *)homeScene->getChildByTag(TAG_HOME);//(Home*)self.homeLayer;
    if (!homeLayer) {
        NSAssert(NO, @"runningScene is not homescene");
        return;
    }
    BOOL isOK = [[notifocation.userInfo objectForKey:@"buttontype"] isEqualToString:@"ok"];
    if (isOK) {
        homeLayer->doleExecutePurchase();
    } else {
        homeLayer->doleExecuteCancel();
    }
    return;
}

- (void)receiveNotificationForShare:(NSNotification *)notifocation {
    NSLog(@"UserInfo : %@", notifocation.userInfo);
    cocos2d::Scene *bookmapScene = cocos2d::Director::getInstance()->getRunningScene();
    if (!bookmapScene) {
        NSAssert(NO, @"runningScene is empty");
        return;
    }
    BookMap *bookMapLayer = (BookMap *)bookmapScene->getChildByTag(TAG_BOOKMAP);//(Home*)self.homeLayer;
    if (!bookMapLayer) {
        NSAssert(NO, @"runningScene is not bookmapScene");
        return;
    }
    
    BOOL isShare = [[notifocation.userInfo objectForKey:@"buttontype"] isEqualToString:@"share"];
    if (isShare) {
        NSString *comment = [notifocation.userInfo objectForKey:@"comment"];
        NSLog(@"input comment : %@", comment);
        bookMapLayer->onShareCommentResult(true, [comment UTF8String]);
        
    } else {
        NSLog(@"input comment canceled");
        bookMapLayer->onShareCommentResult(false, NULL);
    }
    return;
}

- (void)receiveNotificationForUpdateDoleCoin:(NSNotification *)notification {
    TSOriginLayer originLayer = (TSOriginLayer)[[notification.userInfo objectForKey:@"originlayer"] integerValue];
    Home *homeLayer = NULL;
    BookMap *bookMapLayer = NULL;
    CharacterRecording *chareclayer = NULL;
    if (originLayer == TSOriginLayerHome) {
        cocos2d::Scene *homeScene = cocos2d::Director::getInstance()->getRunningScene();
        if (!homeScene) {
            NSLog(@"runningScene is empty");
            return;
        }
        homeLayer = (Home *)homeScene->getChildByTag(TAG_HOME);//(Home*)self.homeLayer;
        if (!homeLayer) {
            NSLog(@"runningScene is not homescene");
            return;
        }
    } else if (originLayer == TSOriginLayerBookMap) {
        cocos2d::Scene *bookMapScene = cocos2d::Director::getInstance()->getRunningScene();
        if (!bookMapScene) {
            NSLog(@"runningScene is empty");
            return;
        }
        bookMapLayer = (BookMap *)bookMapScene->getChildByTag(TAG_BOOKMAP);//(Home*)self.homeLayer;
        if (!bookMapLayer) {
            NSLog(@"runningScene is not BookMapScene");
            return;
        }
        homeLayer = bookMapLayer->getHome();
        if (!homeLayer) {
            NSLog(@"homeLayer empty");
            return;
        }
    } else if (originLayer == TSOriginLayerCharacterRecording) {
        cocos2d::Scene *charecScene = cocos2d::Director::getInstance()->getRunningScene();
        if (!charecScene) {
            NSLog(@"runningScene is empty");
            return;
        }
        chareclayer = (CharacterRecording *)charecScene->getChildByTag(TAG_RECORDING);//(Home*)self.homeLayer;
        if (!chareclayer) {
            NSLog(@"runningScene is not CharacterRecordingScene");
            return;
        }
        homeLayer = chareclayer->getHome();
        if (!homeLayer) {
            NSLog(@"homeLayer empty");
            return;
        }
    } else {
        NSLog(@"Unknown Origin Layer !!!!!!!!!!!!!!!");
    }
    
    if (homeLayer) {
        homeLayer->doleCoinGetBalanceProtocol();
    }
    return;
}

- (void)receiveNotificationForFirstRun:(NSNotification *)notifocation {
    NSLog(@"UserInfo : %@", notifocation.userInfo);
    cocos2d::Scene *homeScene = cocos2d::Director::getInstance()->getRunningScene();
    if (!homeScene) {
        NSAssert(NO, @"runningScene is empty");
        return;
    }
    Home *homeLayer = (Home *)homeScene->getChildByTag(TAG_HOME);//(Home*)self.homeLayer;
    if (!homeLayer) {
        NSAssert(NO, @"runningScene is not homescene");
        return;
    }
    
    homeLayer->showGuide();
    return;
}


@end
