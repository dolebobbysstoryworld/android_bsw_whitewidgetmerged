//
//  TSTitleScrollTextView.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 2..
//
//

#import <UIKit/UIKit.h>

@interface TSTitleScrollTextView : UIView

- (id)initWithFrame:(CGRect)frame Title:(NSString*)title Text:(NSString*)text;//support text content
- (id)initWithFrame:(CGRect)frame Title:(NSString*)title URL:(NSURL*)html;//support html content

@end
