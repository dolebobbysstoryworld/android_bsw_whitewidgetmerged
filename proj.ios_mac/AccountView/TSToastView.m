//
//  TSToastView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 17..
//
//

#import "TSToastView.h"
#import "ResolutionManager.h"
#import "UIImage+PathExtention.h"
#import "UIColor+RGB.h"

//#define GFDEBUG_ENABLE
#ifdef GFDEBUG_ENABLE
#define GFLog(s, ...)	\
NSLog(@"\nMethod:%s {\n%@\n}", __PRETTY_FUNCTION__, [NSString stringWithFormat:(s), ##__VA_ARGS__]);
#else
#define GFLog( s, ... )
#endif

#define DEGREES_TO_RADIANS(x) (M_PI * (x) / 180.0)

//#define PAD_FRAME       CGRectMake(701, 690, 323, 62)
//#define PHONE_FRAME     CGRectMake(66, 497, 188, 36)
//#define PHONES_FRAME    CGRectMake(66, 409, 188, 36)

#define PAD_ORIGIN      CGPointMake(0, 0)//CGPointMake(701, 716)
#define PHONE_ORIGIN    CGPointMake(66, 512)
#define PHONES_ORIGIN   CGPointMake(66, 424)

//#define PAD_BOUNDS      CGRectMake(0, 0, 323, 62)
//#define PHONE_BOUNDS    CGRectMake(0, 0, 188, 36)

#define PAD_BOUNDS_NL   CGRectMake(0, 0, 323, 36)
#define PHONE_BOUNDS_NL CGRectMake(0, 0, 188, 20)

@interface TSToastView ()

@property (nonatomic, strong) NSString *message;
@property (nonatomic, assign) NSInteger lineCount;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) BOOL animated;

@end

@implementation TSToastView

- (id)initWithString:(NSString*)string {
    CGRect frame = PAD_BOUNDS_NL;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        frame = PHONE_BOUNDS_NL;
    }
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.animated = NO;
        self.backgroundColor = [UIColor clearColor];
        self.message = string;
        [self adjustLabelLayout];
    }
    return self;
}

- (UILabel*)resizinglabel:(CGFloat)width {
    CGRect frame = CGRectMake(0, 0, width, 26);
    
    NSInteger fontSize = 0;
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0]; //@"ja",@"ko",@"en"
    if ([language isEqualToString:@"ja"]) {
        fontSize = 24-8; //24
    } else {
        fontSize = 24; //24
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        if ([language isEqualToString:@"ja"]) {
            fontSize = 14-4;
        } else {
            fontSize = 14;
        }
        
        frame = CGRectMake(0, 0, width, 16);
    }
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.font = [UIFont fontWithName:kDefaultFontName size:fontSize];
    label.textColor = RGBH(@"#ffffff");
    label.numberOfLines = 0;
    label.textAlignment = NSTextAlignmentCenter;
    label.text = self.message;
    GFLog(@"label frame before sizeToFit: %@", NSStringFromCGRect(label.frame));
    [label sizeToFit];
    GFLog(@"label frame after sizeToFit: %@", NSStringFromCGRect(label.frame));
    return label;
}

- (void)adjustLabelLayout {
    CGFloat width = 285;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        width = 168;
    }
    UILabel *label = [self resizinglabel:width];
    CGSize size = [self.message sizeWithAttributes:@{ NSFontAttributeName : label.font }];
    self.lineCount = label.frame.size.height / size.height;
    if (self.lineCount >= 2) {
        width = 262;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            width = 157;
        }
        label = [self resizinglabel:width];
        size = [self.message sizeWithAttributes:@{ NSFontAttributeName : label.font }];
        self.lineCount = label.frame.size.height / size.height;
    }
    
    GFLog(@"Toast frame before height change : %@", NSStringFromCGRect(self.frame));
    CGRect frame = self.frame;
    frame.size = CGSizeMake(self.frame.size.width, self.frame.size.height + label.frame.size.height);
    self.frame = frame;
    GFLog(@"Toast frame after height change : %@", NSStringFromCGRect(self.frame));
    
    label.center = self.center;
    GFLog(@"label frame change center: %@", NSStringFromCGRect(label.frame));
    [self adjustBackgroundLayout];
    [self addSubview:label];
    
    frame = self.frame;
    GFLog(@"Toast frame before change : %@", NSStringFromCGRect(self.frame));
    frame.origin = PAD_ORIGIN;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        frame.origin = PHONE_ORIGIN;
        if ([UIScreen mainScreen].bounds.size.height == 480) {
            frame.origin = PHONES_ORIGIN;
        }
    }
    frame.origin = CGPointMake(frame.origin.x, frame.origin.y - label.frame.size.height);
    self.frame = frame;
    GFLog(@"Toast frame after change : %@", NSStringFromCGRect(self.frame));
}

- (void)adjustBackgroundLayout {
    UIImageView *background = [[UIImageView alloc] initWithImage:[[UIImage imageNamedEx:@"popup_toast.png" ignore:YES]
                                                                  resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)]];
    background.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    [self addSubview:background];
}

- (void)showInView:(UIView*)parent {
    self.animated = NO;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [parent addSubview:self];
    } else {
#ifdef UI_TYPE_IOS8
        CGFloat x = (parent.frame.size.width - self.frame.size.width)/2;
        CGFloat y = (parent.frame.size.height - self.frame.size.height-32/2);
        self.frame = CGRectMake(x, y, self.frame.size.width, self.frame.size.height);
#else
        CGFloat heigth = 612+(parent.superview.frame.size.height-612)/2;
        self.center = CGPointMake(parent.frame.size.width/2, heigth - (32/2+self.frame.size.height/2));
        [parent addSubview:self];
#endif
//        UIView *currentView = parent;
//        while (currentView) {
//            GFLog(@"Name:%@ RECT:%@",NSStringFromClass([currentView class]), NSStringFromCGRect(currentView.frame));
//            if (currentView.superview) {
//                currentView = currentView.superview;
//            } else {
//                self.center = CGPointMake(16+self.frame.size.height/2, currentView.frame.size.height/2);//pad full screen 기준으로 회전 중심축 이동
//                self.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
//                [currentView addSubview:self];
//                break;
//            }
//        }
    }
    if (self.lineCount < 2) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(timerFireMethod:) userInfo:nil repeats:NO];
    } else {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:self.lineCount target:self selector:@selector(timerFireMethod:) userInfo:nil repeats:NO];
    }
}

- (void)showInViewWithAnimation:(UIView*)parent {
    self.animated = YES;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [parent addSubview:self];
    } else {
#ifdef UI_TYPE_IOS8
        CGFloat x = (parent.frame.size.width - self.frame.size.width)/2;
        CGFloat y = (parent.frame.size.height - self.frame.size.height-32/2);
        self.frame = CGRectMake(x, y, self.frame.size.width, self.frame.size.height);
        if (!IS_IOS8) {
            CGFloat newY = (parent.frame.size.height+(768-parent.frame.size.height)/2)-(self.frame.size.height+32/2);
            self.frame = CGRectMake(self.frame.origin.x, newY, self.frame.size.width, self.frame.size.height);
        }
#else
        CGFloat heigth = 612+(parent.superview.frame.size.height-612)/2;
        self.center = CGPointMake(parent.frame.size.width/2, heigth - (32/2+self.frame.size.height/2));
#endif
        [parent addSubview:self];
//        UIView *currentView = parent;
//        while (currentView) {
//            GFLog(@"Name:%@ RECT:%@",NSStringFromClass([currentView class]), NSStringFromCGRect(currentView.frame));
//            if (currentView.superview) {
//                currentView = currentView.superview;
//            } else {
//                self.center = CGPointMake(16+self.frame.size.height/2, currentView.frame.size.height/2);//pad full screen 기준으로 회전 중심축 이동
//                self.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
//                [currentView addSubview:self];
//                break;
//            }
//        }
    }
    self.alpha = 0.3f;
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         self.alpha = 1.0f;
                     }
                     completion:^(BOOL finished){
                         if (self.lineCount < 2) {
                             self.timer = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(timerFireMethodWithAnimation:) userInfo:nil repeats:NO];
                         } else {
                             self.timer = [NSTimer scheduledTimerWithTimeInterval:self.lineCount target:self selector:@selector(timerFireMethodWithAnimation:) userInfo:nil repeats:NO];
                         }
                     }];
}

- (void)showInViewForLandscape:(UIView*)parent {
    self.animated = NO;
    [parent addSubview:self];
    if (self.lineCount < 2) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(timerFireMethod:) userInfo:nil repeats:NO];
    } else {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:self.lineCount target:self selector:@selector(timerFireMethod:) userInfo:nil repeats:NO];
    }
}

- (void)timerFireMethod:(NSTimer *)timer {
    [self removeFromSuperview];
}

- (void)timerFireMethodWithAnimation:(NSTimer *)timer {
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         self.alpha = 0.3f;
                     }
                     completion:^(BOOL finished){
                         [self removeFromSuperview];
                     }];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if ([self superview]){
        UITouch *touch = [touches anyObject];
        CGPoint point = [touch locationInView:[self superview]];
        if (CGRectContainsPoint(self.frame, point)){
            if (self.timer && [self.timer isValid] == YES) {
                [self.timer invalidate];
                self.timer = nil;
            }
            if (self.animated == NO) {
                [self timerFireMethod:nil];
            } else {
                [self timerFireMethodWithAnimation:nil];
            }
        }
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
