//
//  TSAccountSigninView.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 2..
//
//

#import <UIKit/UIKit.h>
#import "TSAccountDelegateProtocol.h"

@interface TSAccountSigninView : UIView

@property (nonatomic, assign) id<TSAccountDelegate> delegate;
@property (nonatomic, readonly) BOOL isEdited;
@property (nonatomic, assign) TSOriginLayer originLayer;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, assign) BOOL needPostNotification;

- (void)postLoginNotification:(NSInteger)recallType;

@end
