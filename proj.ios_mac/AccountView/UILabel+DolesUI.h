//
//  UILabel+DolesUI.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 24..
//
//

#import <UIKit/UIKit.h>

@interface UILabel (DolesUI)

+ (UIView*)viewForLoginIDLabel:(CGRect)frame;
+ (UIView*)viewForLoginIDLabel:(CGRect)frame ID:(NSString *)userID;

@end
