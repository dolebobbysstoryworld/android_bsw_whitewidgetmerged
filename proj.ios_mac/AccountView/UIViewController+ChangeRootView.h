//
//  UIViewController+ChangeRootView.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 11. 5..
//
//

#import <UIKit/UIKit.h>
#import "TSViewController.h"

@interface UIViewController (ChangeRootView)

+ (void)replaceRootViewControlller:(TSViewController*)newRootViewController;
+ (void)restoreRootViewControlller:(TSViewController*)currentRootViewController;

@end
