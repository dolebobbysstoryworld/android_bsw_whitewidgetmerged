//
//  TSPickerView.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 3..
//
//

#import "TSPopupView.h"

@interface TSPickerView : TSPopupView

@property (nonatomic, assign) NSInteger selectedIndex;
@property (nonatomic, assign) NSString *selectedString;

- (id)initWithDataSource:(NSArray*)dataSource title:(NSString*)string;
+ (NSArray*)birthYearsData;
+ (NSArray*)countrys;
+ (NSArray*)countryCodes;

@end
