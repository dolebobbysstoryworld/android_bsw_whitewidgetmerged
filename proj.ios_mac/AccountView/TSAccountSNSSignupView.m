//
//  TSAccountSNSSignupView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 24..
//
//

#import "TSAccountSNSSignupView.h"
#import "ResolutionManager.h"
#import "UIImage+PathExtention.h"
#import "UIColor+RGB.h"
#import "UIButton+DolesUI.h"
#import "TSTextField.h"
#import "TSRadioButton.h"
#import "TSDolesNet.h"
#import "TSPickerView.h"
#import "TSMessageView.h"
#import "TSProxyHelper.h"
#import "UILabel+DolesUI.h"
#import "TSToastView.h"

#define TAG_BIRTHDAY_TEXTFIELD          10001
#define TAG_BIRTHDAY_POPUP              10002
#define TAG_CITY_TEXTFIELD              10003
#define TAG_CITY_POPUP                  10004
#define TAG_CURRENTPW_TEXTFIELD         10005
#define TAG_MAIL_RADIOBUTTON            10008
#define TAG_FEMAIL_RADIOBUTTON          10009
#define TAG_NET_PROVINCE                10010
#define TAG_NET_SIGNUP                  10011
//#define TAG_POPUP_SIGNUP                10012
#define TAG_NET_SNSLOGIN                10013
#define TAG_COUNTRY_POPUP               10014
#define TAG_POPUP_LOGIN_FAIL            10015

@interface TSAccountSNSSignupView () < TSTextFieldDelegate, TSRadioButtonDelegate, TSPopupViewDelegate, TSNetDelegate >

@property (nonatomic, strong) TSTextField *birthDayTextFiled;
@property (nonatomic, assign) NSInteger birthDayIndex;
@property (nonatomic, strong) TSTextField *cityTextFiled;
@property (nonatomic, strong) NSArray *countrys;
@property (nonatomic, strong) NSArray *countryCodes;
@property (nonatomic, assign) NSInteger countryIndex;
@property (nonatomic, assign) NSInteger cityIndex;
@property (nonatomic, assign) BOOL isCitySelected;
@property (nonatomic, strong) TSNet *net;
@property (nonatomic, strong) NSArray *birthYears;

@property (nonatomic, strong) TSRadioButton *mailRadioButton;
@property (nonatomic, strong) TSRadioButton *femailRadioButton;

@property (nonatomic, strong) UIButton *signupButton;

@property (nonatomic, assign) BOOL isLoginFinished;

@end

@implementation TSAccountSNSSignupView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"bg_map_factor.png"]];
        backgroundView.frame = getRectValue(TSViewIndexShare, TSShareSubViewOrgRect);
        [self addSubview:backgroundView];
        
        [self adjustTextFieldLayout];
//        [self adjustRadioButtonLayout];
        [self adjustButtonLayout];
        
        self.birthYears = [TSPickerView birthYearsData];
        self.birthDayIndex = [self.birthYears count] -1;
        self.countrys = [TSPickerView countrys];
        self.countryCodes = [TSPickerView countryCodes];
        self.countryIndex = 0;
        self.cityIndex = 0;
        self.isCitySelected = NO;

        self.isLoginFinished = NO;
        self.needPostNotification = NO;
        
}
    return self;
}

- (void)adjustTextFieldLayout {
//    self.birthDayTextFiled =
//    [[TSTextField alloc] initWithFrame:getRectValue(TSViewIndexSignupSNS, TSSignupSNSBirthYearRect)
//                                  icon:@"input_birthday_icon.png" placeholder:NSLocalizedString(@"PF_SIGNUP_TEXTFIELD_BIRTHYEAR", @"Birthday") editable:NO];
//    self.birthDayTextFiled.tag = TAG_BIRTHDAY_TEXTFIELD;
//    self.birthDayTextFiled.delegate = self;
//    [self addSubview:self.birthDayTextFiled];
    
    self.cityTextFiled =
    [[TSTextField alloc] initWithFrame:getRectValue(TSViewIndexSignupSNS, TSSignupSNSCityRect)
                                  icon:@"input_city_icon.png"
                           placeholder:NSLocalizedString(@"PF_ACCOUNT_PLACEHOLDER_COUNTRY", @"Country") editable:NO];
    self.cityTextFiled.tag = TAG_CITY_TEXTFIELD;
    self.cityTextFiled.delegate = self;
//    self.cityTextFiled.enabled = NO;
    [self addSubview:self.cityTextFiled];
    
//    self.net = [[TSNetProvince alloc] init];
//    self.net.tag = TAG_NET_PROVINC;
//    self.net.delegate = self;
//    [self.net start];
}

- (void)tstextFieldTouched:(TSTextField *)textField {
    TSPickerView *pickView = nil;
    if (textField.tag == TAG_BIRTHDAY_TEXTFIELD) {
        pickView = [[TSPickerView alloc] initWithDataSource:self.birthYears
                                                      title:NSLocalizedString(@"PF_SIGNUP_POPUP_TITLE_BIRTHYEAR", @"Set birth year")];
        pickView.tag = TAG_BIRTHDAY_POPUP;
        pickView.selectedIndex = self.birthDayIndex;
    } else {
        if (YES) {//if ([self.cityTextFiled.text length] <= 0) {//아직 국가가 선택되지 않았다.
            pickView = [[TSPickerView alloc] initWithDataSource:self.countrys
                                                          title:NSLocalizedString(@"PF_SIGNUP_PUPUP_TITLE_SELECT_COUNTRY", @"Enter the country")];
            pickView.tag = TAG_COUNTRY_POPUP;
            pickView.selectedIndex = self.countryIndex;
            self.isCitySelected = NO;
        }/* else {//국가는 선택 되었다
            pickView = [[TSPickerView alloc] initWithDataSource:((TSNetProvince*)self.net).provinces
                                                          title:NSLocalizedString(@"PF_SIGNUP_PUPUP_TITLE_SELECT_CITY", @"Enter the city")];
            pickView.tag = TAG_CITY_POPUP;
            pickView.selectedIndex = self.cityIndex;
        }*/
    }
    pickView.buttonLayout = TSPopupButtonLayoutCancelSet;
    pickView.delegate = self;
    [pickView showInView:self];
}

- (void)setLoginForFacebookData:(NSDictionary *)data {
    _loginForFacebookData = data;
    [self addSubview:[UILabel viewForLoginIDLabel:getRectValue(TSViewIndexSignupSNS, TSSignupSNSIDLabelRect)
                                               ID:[self.loginForFacebookData objectForKey:UD_STRING_LOGINID]]];

    NSInteger gender = [[self.loginForFacebookData objectForKey:UD_INTEGER_GENDER] integerValue];
    if (gender == 2) {
        self.femailRadioButton.selected = YES;
    } else {
        self.mailRadioButton.selected = YES;
    }
    self.mailRadioButton.enabled = NO;
    self.femailRadioButton.enabled = NO;
}

- (void)popview:(TSPopupView *)view didClickedButton:(TSPopupButtonType)type {
    switch (view.tag) {
        case TAG_BIRTHDAY_POPUP: {
            self.birthDayIndex = ((TSPickerView*)view).selectedIndex;
            self.birthDayTextFiled.text = [self.birthYears objectAtIndex:self.birthDayIndex];
        } break;
        case TAG_COUNTRY_POPUP: {
            if (type == TSPopupButtonTypeSet) {
                self.countryIndex = ((TSPickerView*)view).selectedIndex;
                self.cityTextFiled.text = [self.countrys objectAtIndex:self.countryIndex];
//                NSString *countryCode = [self.countryCodes objectAtIndex:self.countryIndex];
//                if ([countryCode isEqualToString:NSLocalizedString(@"PF_SIGNUP_COUNTRY_LIST_OTHER", @"Other")] == NO) {
//                    self.cityIndex = 0;
//                    self.net = [[TSNetProvince alloc] initWithCountryCode:countryCode];
//                    self.net.delegate = self;
//                    self.net.tag = TAG_NET_PROVINCE;
//                    [self.net start];
//                    self.cityTextFiled.enabled = NO;
//                    return;
//                }
                self.isCitySelected = YES;
            } else {
                if ([self.cityTextFiled.text length] <= 0) {
                    self.isCitySelected = NO;
                }
            }
        } break;
        case TAG_CITY_POPUP: {
            if (type == TSPopupButtonTypeSet) {
                self.cityIndex = ((TSPickerView*)view).selectedIndex;
                self.cityTextFiled.text = [((TSNetProvince*)self.net).provinces objectAtIndex:self.cityIndex];
                self.cityTextFiled.enabled = YES;
                self.isCitySelected = YES;
            } else {
                self.isCitySelected = NO;
                self.cityTextFiled.enabled = YES;
            }
        } break;
//        case TAG_POPUP_SIGNUP: {
//            [[NSNotificationCenter defaultCenter] postNotificationName:kTSNotifivationLoginDidFinished object:nil];
//            if (self.delegate && [self.delegate respondsToSelector:@selector(closeViewControllerWithView:)]) {
//                [self.delegate closeViewControllerWithView:self];
//            }
//            return;
//        } break;
        case TAG_POPUP_LOGIN_FAIL: {
            if (self.delegate && [self.delegate respondsToSelector:@selector(view:Page:userData:)]) {
                [self.delegate view:self Page:TSViewIndexLogin userData:nil];
            }
        } break;
        default: {  } break;
    }

    [self checkEnableSaveButton];
}

- (void)adjustRadioButtonLayout {
    self.mailRadioButton = [TSRadioButton radioButtonWithTitle:NSLocalizedString(@"PF_SIGNUP_RADIOTITLE_MALE", @"Male")
                                                         Frame:getRectValue(TSViewIndexSignupSNS, TSSignupSNSMaleRadioRect)];
    self.mailRadioButton.tag = TAG_MAIL_RADIOBUTTON;
    self.mailRadioButton.delegate = self;
    [self addSubview:self.mailRadioButton];
    
    self.femailRadioButton = [TSRadioButton radioButtonWithTitle:NSLocalizedString(@"PF_SIGNUP_RADIOTITLE_FEMALE", @"Female")
                                                           Frame:getRectValue(TSViewIndexSignupSNS, TSSignupSNSFemaleRadioRect)];
    self.femailRadioButton.tag = TAG_FEMAIL_RADIOBUTTON;
    self.femailRadioButton.delegate = self;
    [self addSubview:self.femailRadioButton];
}

- (void)radioButton:(TSRadioButton *)radioButton didChangeSelectedStatus:(BOOL)selected {
    switch (radioButton.tag) {
        case TAG_MAIL_RADIOBUTTON: {
            if (selected) {
                self.femailRadioButton.selected = NO;//tuilise_todo : 나중에 group 처리 살계해 보자.
            }
        } break;
        case TAG_FEMAIL_RADIOBUTTON: {
            if (selected) {
                self.mailRadioButton.selected = NO;
            }
        } break;
        default: { } break;
    }
    [self checkEnableSaveButton];
}


- (void)adjustButtonLayout {
    self.signupButton = [UIButton buttonWithType:TSDolsTypeOK Frame:getRectValue(TSViewIndexSignupSNS, TSSignupSNSSignupButtonRect)
                                           Title:NSLocalizedString(@"PF_SIGNUP_BUTTON_TITLE_SIGNUP", @"Sign up")];
    [self.signupButton addTarget:self action:@selector(selectorSignUpButton:) forControlEvents:UIControlEventTouchUpInside];
    self.signupButton.enabled = NO;
    [self addSubview:self.signupButton];
}

- (IBAction)selectorSignUpButton:(id)sender {
    NSLog(@"selectorSignUpButton");
    self.signupButton.enabled = NO;
    NSString *email = [self.loginForFacebookData objectForKey:UD_STRING_EMAIL];
    NSString *snsID = [self.loginForFacebookData objectForKey:UD_STRING_SNSID];
    NSString *snsName = [self.loginForFacebookData objectForKey:UD_STRING_SNSNAME];
    NSInteger gender = [[self.loginForFacebookData objectForKey:UD_INTEGER_GENDER] integerValue];
    self.net =
#ifdef TSNET_REAL_SNSID_ENABLE
    [[TSNetSignup alloc] initWithSNSEmail:email snsID:snsID name:snsName Gender:gender
                                 BirthDay:self.birthDayTextFiled.text AddressMain:self.cityTextFiled.text countryCode:[self.countryCodes objectAtIndex:self.countryIndex]];
#else
    [[TSNetSignup alloc] initWithSNSEmail:email snsID:email name:snsName Gender:gender
                                 BirthDay:@"9999"//self.birthDayTextFiled.text
                              AddressMain:self.cityTextFiled.text countryCode:[self.countryCodes objectAtIndex:self.countryIndex]];
#endif
    self.net.tag = TAG_NET_SIGNUP;
    self.net.delegate = self;
//    self.net.ignoreServerError = YES;
    [self.net start];
}

- (void)checkEnableSaveButton {
    if (/*([self.birthDayTextFiled.text length] > 0) &&*/ (self.isCitySelected == YES) //&&//([self.cityTextFiled.text length] > 0) &&
        /*(self.mailRadioButton.selected == YES || self.femailRadioButton.selected )*/ ) {
        self.signupButton.enabled = YES;
    } else {
        self.signupButton.enabled = NO;
    }
}

#pragma mark - TSNetDelegate
- (void)net:(TSNet*)netObject didStartProcess:(TSNetResultType)result {
    switch (netObject.tag) {
        case TAG_NET_PROVINCE: {
            self.cityTextFiled.showActivity = YES;
        } break;
//        case TAG_NET_SNSLOGIN:
        case TAG_NET_SIGNUP: {
            [self.signupButton showActivity:YES];
            self.signupButton.enabled = NO;
        } break;
        default: {  } break;
    }
}

- (void)net:(TSNet*)netObject didEndProcess:(TSNetResultType)result {
    if (!result) {
        switch (netObject.tag) {
            case TAG_NET_PROVINCE: {
                self.cityTextFiled.showActivity = NO;
//                self.cityTextFiled.enabled = YES;
//                self.cityTextFiled.text = [((TSNetProvince*)self.net).provinces objectAtIndex:0];//각 국의 도시 정보 처음값으로 초기화
                
                TSPickerView *pickView = [[TSPickerView alloc] initWithDataSource:((TSNetProvince*)self.net).provinces
                                                                            title:NSLocalizedString(@"PF_SIGNUP_PUPUP_TITLE_SELECT_CITY", @"Enter the city")];
                pickView.tag = TAG_CITY_POPUP;
                pickView.selectedIndex = self.cityIndex;
                pickView.buttonLayout = TSPopupButtonLayoutCancelSet;
                pickView.delegate = self;
                [pickView showInView:self];
            } break;
            case TAG_NET_SIGNUP: {
                [self saveFacebookLoginInfo];
                self.net =
                [[TSNetSNSLogin alloc] initWithSNSID:[[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_SNSID]];
                self.net.delegate = self;
                self.net.tag = TAG_NET_SNSLOGIN;
                [self.net start];
            } break;
            case TAG_NET_SNSLOGIN: {
                self.signupButton.enabled = YES;
                [self.signupButton showActivity:NO];
                self.isLoginFinished = YES;
                if (self.delegate && [self.delegate respondsToSelector:@selector(closeViewControllerWithView:)]) {
                    self.needPostNotification = YES;
                    [self.delegate closeViewControllerWithView:self];
                }
            } break;
            default: {  } break;
        }
    } else {//tuilise_todo : 오류 결과다. 맞는 처리 필요
        switch (netObject.tag) {
            case TAG_NET_PROVINCE: {
                self.cityTextFiled.showActivity = NO;
                self.cityTextFiled.text = nil;
                TSMessageView *messageView = [[TSMessageView alloc] initWithMessage:[NSString stringWithFormat:@"Server Error\n%@",
                                                                                     [netObject serverErrorString]] icon:TSPopupIconTypeWarning];
                messageView.buttonLayout = TSPopupButtonLayoutOK;
                [messageView showInView:self];
            } break;
            case TAG_NET_SIGNUP:
            case TAG_NET_SNSLOGIN: {
                self.signupButton.enabled = YES;
                [self.signupButton showActivity:NO];
                NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                [ud removeObjectForKey:UD_BOOL_FACEBOOKLOGIN];
                [ud removeObjectForKey:UD_STRING_LOGINID];
                [ud removeObjectForKey:UD_STRING_EMAIL];
                [ud removeObjectForKey:UD_INTEGER_GENDER];
                [ud removeObjectForKey:UD_STRING_SNSID];
                [ud removeObjectForKey:UD_STRING_SNSNAME];
                [ud synchronize];
//                if (netObject.returnCode == 90107) {
                    TSMessageView *messageView = [[TSMessageView alloc] initWithMessage:[NSString stringWithFormat:@"Server Error\n%@",
                                                                                         [netObject serverErrorString]] icon:TSPopupIconTypeWarning];
                    messageView.buttonLayout = TSPopupButtonLayoutOK;
                    [messageView showInView:self];
//                } else {
//                    TSMessageView *messageView =
//                    [[TSMessageView alloc] initWithMessage:NSLocalizedString(@"CM_NET_WARNING_NETWORK_UNSTABLE", @"The network is unstable.\nPlease try again")
//                                                      icon:TSPopupIconTypeWarning];
//                    messageView.buttonLayout = TSPopupButtonLayoutOK;
//                    messageView.delegate = self;
//                    messageView.tag = TAG_POPUP_LOGIN_FAIL;
//                    [messageView showInView:self];
//                }
                return;
            } break;
            default: { } break;
        }
    }
}

- (void)net:(TSNet*)netObject didFailWithError:(TSNetResultType)result {
    switch (netObject.tag) {
        case TAG_NET_PROVINCE: {
            TSToastView *toast = [[TSToastView alloc] initWithString:NSLocalizedString(@"CM_NET_WARNING_NETWORK_UNSTABLE", @"The network is unstable.\nPlease try again")];
            [toast showInViewWithAnimation:self];
            self.cityTextFiled.showActivity = NO;
        } break;
        case TAG_NET_SIGNUP:
        case TAG_NET_SNSLOGIN: {
            self.signupButton.enabled = YES;
            [self.signupButton showActivity:NO];
            TSMessageView *messageView =
            [[TSMessageView alloc] initWithMessage:NSLocalizedString(@"CM_NET_WARNING_NETWORK_UNSTABLE", @"The network is unstable.\nPlease try again")
                                              icon:TSPopupIconTypeWarning];
            messageView.buttonLayout = TSPopupButtonLayoutOK;
            messageView.delegate = self;
            messageView.tag = TAG_POPUP_LOGIN_FAIL;
            [messageView showInView:self];
        } break;
        default: {  } break;
    }
}

- (BOOL)isEdited {
    if (self.isLoginFinished == YES) {
        return !self.isLoginFinished;
    }

    if ([self.birthDayTextFiled.text length] > 0) {
        return YES;
    }
    if ([self.cityTextFiled.text length] > 0) {
        return YES;
    }
    return NO;
}

- (void)saveFacebookLoginInfo {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:[NSNumber numberWithBool:YES] forKey:UD_BOOL_FACEBOOKLOGIN];
    NSString *email = [self.loginForFacebookData objectForKey:UD_STRING_LOGINID];
    NSString *userName = [self.loginForFacebookData objectForKey:UD_STRING_SNSNAME];
    [ud setObject:email forKey:UD_STRING_LOGINID];
    [ud setObject:email forKey:UD_STRING_EMAIL];
    [ud setObject:[NSNumber numberWithInteger:[[self.loginForFacebookData objectForKey:UD_INTEGER_GENDER] integerValue]] forKey:UD_INTEGER_GENDER];
    NSString *idString = [self.loginForFacebookData objectForKey:UD_STRING_SNSID];
    [ud setObject:idString forKey:UD_STRING_SNSID];
    [ud setObject:userName forKey:UD_STRING_SNSNAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)postLoginNotification:(NSInteger)recallType {
    [[NSNotificationCenter defaultCenter] postNotificationName:kTSNotifivationLoginDidFinished
                                                        object:nil
                                                      userInfo:@{@"originlayer": [NSNumber numberWithInteger:self.originLayer],
                                                                 @"recalltype": [NSNumber numberWithInteger:recallType]}];
}

@end
