//
//  TSSettingViewController.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 10..
//
//

#import "TSSettingViewController.h"
#import "ResolutionManager.h"
#import "UIImage+PathExtention.h"
#import "TSSettingMainView.h"
#import "../FacebookFriendListViewController.h"
#import "TSAccountViewController.h"
#import "TSModifyAccountView.h"
#import "TSHelpFAQView.h"
#import "TSDeleteAccountView.h"
#import "TSModifySNSAccountView.h"
#import "TSAboutView.h"
#import "TSAccountAgreementView.h"
#import "DoleCoinUsageListViewController.h"
#import "AppController.h"

@interface TSSettingViewController () < TSAccountDelegate >

@property (nonatomic, strong) UIView *currentDisplayView;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) UIImageView *lion;
@property (nonatomic, strong) UIImageView *racoon;
@property (nonatomic, assign) BOOL isKeyboardHide;

@end

@implementation TSSettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(noticeWillShowKeyboard:) name:UIKeyboardWillShowNotification object:nil];
    [center addObserver:self selector:@selector(noticeWillHideKeyboard:) name:UIKeyboardWillHideNotification object:nil];
    [center addObserver:self selector:@selector(notificationPopupShow:) name:tsNotificationPopupShow object:nil];
    [center addObserver:self selector:@selector(notificationPopupClose:) name:tsNotificationPopupClose object:nil];
    
    self.view.backgroundColor = [UIColor clearColor];
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"bg_map_full.png"]];
    backgroundView.frame = getRectValue(TSViewIndexShare, TSShareSubViewRect);
    [self.view addSubview:backgroundView];

    [self view:nil Page:TSViewIndexSettingMain userData:nil];//loginView init;
}

- (void)backgroundResourceLoad {
    if (self.closeButton) {//tuilise_todo : firstRun 일때는 생성하지 마라
        [self.view bringSubviewToFront:self.closeButton];
    } else {
            self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.closeButton setImage:[UIImage imageNamedEx:@"btn_cancel_normal.png" ignore:YES] forState:UIControlStateNormal];
            [self.closeButton setImage:[UIImage imageNamedEx:@"btn_cancel_press.png" ignore:YES] forState:UIControlStateHighlighted];
            [self.closeButton addTarget:self action:@selector(selectorCloseButton:) forControlEvents:UIControlEventTouchUpInside];
            self.closeButton.frame = getRectValue(TSViewIndexShare, TSShareCloseButtonRect);
            [self.view addSubview:self.closeButton];
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        ///PAD만 표시라 가변 위치 필요 없음
        if (!self.lion) {
            self.lion = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"full_popup_character_01.png"]];
            CGRect newFrame = CGRectMake((((2048-828)/2)-224+32)/2, (768-612)/2+(70+472+210)/2, 346/2, 472/2);//CGRectMake(3, 376, 148, 236);
            self.lion.frame = newFrame;
            [self.view addSubview:self.lion];
        } else {
            [self.view bringSubviewToFront:self.lion];
        }
        
        if (!self.racoon) {
            self.racoon = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"full_popup_character_02.png"]];
            self.racoon.frame = CGRectMake( ((2048-828)/2+(828-116)-32)/2 , (768-612)/2+70/2, 296/2, 472/2);//CGRectMake(414, 35, 148, 236);
            [self.view addSubview:self.racoon];
        } else {
            [self.view bringSubviewToFront:self.racoon];
        }
    }
}

- (IBAction)selectorCloseButton:(id)sender {
    NSString *viewName = NSStringFromClass([self.currentDisplayView class]);
    if ([viewName isEqualToString:@"TSAboutView"] == YES ||
        [viewName isEqualToString:@"TSHelpFAQView"] == YES ||
        [viewName isEqualToString:@"TSDeleteAccountView"] == YES) {
        [self view:self.currentDisplayView Page:TSViewIndexSettingMain userData:nil];//loginView init;
        return;
    } else if ( [viewName isEqualToString:@"TSAccountAgreementView"] == YES ) {
        [self view:self.currentDisplayView Page:TSViewIndexAbout userData:nil];//loginView init;
        return;
    } else if ( [viewName isEqualToString:@"TSModifyAccountView"] == YES ||
                [viewName isEqualToString:@"TSModifySNSAccountView"] == YES ) {
        [self view:self.currentDisplayView Page:TSViewIndexSettingMain userData:nil];//loginView init;
        return;
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:tsNotificationPopupShow object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:tsNotificationPopupClose object:nil];
//    [self dismissViewControllerAnimated:YES completion:nil];
    [self closeTSViewController];
}

#pragma mark - TSAccountDelegate
- (void)view:(UIView *)view Page:(TSViewIndex)index userData:(id)object {
    if (view) {//뻗지는 않겠지?
        [self.currentDisplayView removeFromSuperview];
        self.currentDisplayView = nil;
    }
    
    switch (index) {
        case TSViewIndexSettingMain: {
            TSSettingMainView *settingMainView = [[TSSettingMainView alloc] initWithFrame:getRectValue(TSViewIndexShare, TSShareSubViewRect)];
            settingMainView.delegate = self;
            if (object) {
                settingMainView.objects = object;
            }
            [self.view addSubview:settingMainView];
            self.currentDisplayView = settingMainView;
        } break;
        case TSViewIndexModify: {
            TSModifyAccountView *modifyView = [[TSModifyAccountView alloc] initWithFrame:getRectValue(TSViewIndexShare, TSShareSubViewRect)];
            modifyView.delegate = self;
            [self.view addSubview:modifyView];
            self.currentDisplayView = modifyView;
        } break;
        case TSViewIndexHelp: {
            TSHelpFAQView *helpFAQView = [[TSHelpFAQView alloc] initWithFrame:getRectValue(TSViewIndexShare, TSShareSubViewRect)];
            helpFAQView.delegate = self;
            [self.view addSubview:helpFAQView];
            self.currentDisplayView = helpFAQView;
        } break;
        case TSViewIndexDeleteAccount: {
            TSDeleteAccountView *deleteAccountView = [[TSDeleteAccountView alloc] initWithFrame:getRectValue(TSViewIndexShare, TSShareSubViewRect)];
            deleteAccountView.delegate = self;
            deleteAccountView.originLayer = self.originLayer;
            [self.view addSubview:deleteAccountView];
            self.currentDisplayView = deleteAccountView;
        } break;
        case TSViewIndexModifySNS: {
            TSModifySNSAccountView *modifySNSView = [[TSModifySNSAccountView alloc] initWithFrame:getRectValue(TSViewIndexShare, TSShareSubViewRect)];
            modifySNSView.delegate = self;
            [self.view addSubview:modifySNSView];
            self.currentDisplayView = modifySNSView;
        } break;
        case TSViewIndexAbout: {
            TSAboutView *aboutView = [[TSAboutView alloc] initWithFrame:getRectValue(TSViewIndexShare, TSShareSubViewRect)];
            aboutView.delegate = self;
            aboutView.originLayer = self.originLayer;
            [self.view addSubview:aboutView];
            self.currentDisplayView = aboutView;
        } break;
        case TSViewIndexAgreement: {
            TSAccountAgreementView *agreeView = [[TSAccountAgreementView alloc] initWithFrame:getRectValue(TSViewIndexShare, TSShareSubViewRect) forAbout:YES];
            agreeView.delegate = self;
            [self.view addSubview:agreeView];
            self.currentDisplayView = agreeView;
        } break;
        case TSViewIndexUsage: {
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:tsNotificationPopupShow object:nil];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:tsNotificationPopupClose object:nil];
            [self dismissViewControllerAnimated:NO completion:nil];
            UIViewController *selfViewController = nil;
            if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                selfViewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
            } else {
                selfViewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
            }
            DoleCoinUsageListViewController *usageViewController = [[DoleCoinUsageListViewController alloc] init];
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                usageViewController.modalPresentationStyle = UIModalPresentationFormSheet;
            }
            if(IS_IOS8 && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                usageViewController.preferredContentSize = CGSizeMake(1024, 768);
            }
            [selfViewController presentViewController:usageViewController animated:NO completion:nil];
        } break;
        default: { } break;
    }
    
    [self backgroundResourceLoad];
}

- (void)facebookViewControllerWithView:(UIView *)view {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:tsNotificationPopupShow object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:tsNotificationPopupClose object:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
    UIViewController *selfViewController = nil;
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        selfViewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
    } else {
        selfViewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
    }
    FacebookFriendListViewController *facebookFriendListViewController = [[FacebookFriendListViewController alloc] init];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        facebookFriendListViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    //    [selfViewController presentModalViewController:facebookFriendListViewController animated:YES];
    [selfViewController presentViewController:facebookFriendListViewController animated:YES completion:nil];
}

- (void)loginViewControllerWithView:(UIView*)view {
/*    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:tsNotificationPopupShow object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:tsNotificationPopupClose object:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
    UIViewController *viewController = nil;
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
    } else {
        viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
    }
//    NSLog(@"viewController screen frame : %@",NSStringFromCGRect(viewController.view.frame));
    TSAccountViewController *accountViewController = [[TSAccountViewController alloc] initWithFirstRun:NO];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        accountViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    accountViewController.parentType = TSParentTypeSetting;
    accountViewController.originLayer = self.originLayer;
    if(IS_IOS8 && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        accountViewController.preferredContentSize = CGSizeMake(1024, 768);
    }
    [viewController presentViewController:accountViewController animated:NO completion:nil];
*/
    TSAccountViewController *accountViewController = [[TSAccountViewController alloc] initWithFirstRun:NO];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        accountViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    accountViewController.parentType = TSParentTypeSetting;
    accountViewController.originLayer = self.originLayer;
    if(IS_IOS8 && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        accountViewController.preferredContentSize = CGSizeMake(1024, 768);
    }
    [self presentViewController:accountViewController animated:YES completion:nil];
    
}

- (void)closeViewControllerWithView:(UIView *)view {
    [self selectorCloseButton:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)notificationPopupShow:(NSNotification *)notification {
    self.closeButton.hidden = YES;
}

- (void)notificationPopupClose:(NSNotification *)notification {
    self.closeButton.hidden = NO;
}

- (void)noticeWillShowKeyboard:(NSNotification *)notification {
    self.isKeyboardHide = NO;
    NSString *viewName = NSStringFromClass([self.currentDisplayView class]);

    NSInteger move = 0;
    if ([viewName isEqualToString:@"TSDeleteAccountView"] == YES) {
        move = 150;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            move = 100;
            if ([UIScreen mainScreen].bounds.size.height == 480) {
                move = 140;
            }
        }
    }

    if ([viewName isEqualToString:@"TSHelpFAQView"] == YES) {
        move = 155;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            move = 110;
            if ([UIScreen mainScreen].bounds.size.height != 480) {
                move = 130;
            }
        }
    }

    if (move != 0) {
        CGRect frame = self.view.frame;
        frame.origin.y = frame.origin.y - move;
        [UIView animateWithDuration:0.3 animations:^{
            self.view.frame = frame;
        }];
    }
}

- (void)noticeWillHideKeyboard:(NSNotification *)notification {
    self.isKeyboardHide = YES;
    NSString *viewName = NSStringFromClass([self.currentDisplayView class]);

    NSInteger move = 0;
    if ([viewName isEqualToString:@"TSDeleteAccountView"] == YES) {
        move = 150;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            move = 100;
            if ([UIScreen mainScreen].bounds.size.height == 480) {
                move = 140;
            }
        }
        
    }

    if ([viewName isEqualToString:@"TSHelpFAQView"] == YES) {
        move = 155;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            move = 110;
            if ([UIScreen mainScreen].bounds.size.height != 480) {
                move = 130;
            }
        }
        
    }

    if (move != 0) {
        CGRect frame = self.view.frame;
        frame.origin.y = frame.origin.y + move;
        [UIView animateWithDuration:0.3 animations:^{
            self.view.frame = frame;
        }];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

//- (NSUInteger)supportedInterfaceOrientations{
//    return UIInterfaceOrientationMaskPortrait;
//}
//
//- (NSUInteger)application:(UIApplication*)application supportedInterfaceOrientationsForWindow:(UIWindow*)window {
//    return UIInterfaceOrientationMaskPortrait;
//}

//- (BOOL) shouldAutorotate {
//    return YES;
//}
//
- (BOOL)disablesAutomaticKeyboardDismissal {
    return NO;//ipad UIModalPresentationFormSheet 일때 Keyboard 안내려가는거 처리
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
