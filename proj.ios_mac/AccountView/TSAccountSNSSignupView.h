//
//  TSAccountSNSSignupView.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 24..
//
//

#import <UIKit/UIKit.h>
#import "TSAccountDelegateProtocol.h"

@interface TSAccountSNSSignupView : UIView

@property (nonatomic, assign) id<TSAccountDelegate> delegate;
@property (nonatomic, readonly) BOOL isEdited;
@property (nonatomic, assign) TSOriginLayer originLayer;
@property (nonatomic, strong) NSDictionary *loginForFacebookData;
@property (nonatomic, assign) BOOL needPostNotification;

- (void)postLoginNotification:(NSInteger)recallType;

@end
