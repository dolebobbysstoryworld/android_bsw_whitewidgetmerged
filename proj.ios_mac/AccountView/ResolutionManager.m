//
//  ResolutionManager.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 29..
//
//

#import "ResolutionManager.h"

NSString *const kDefaultFontName                = @"Helvetica";
NSString *const kDefaultBoldFontName            = @"Helvetica-Bold";
NSString *const tsNotificationPopupShow         = @"tsNotificationPopupShow";
NSString *const tsNotificationPopupClose        = @"tsNotificationPopupClose";

#define rectObject(x,y,w,h)         [NSValue valueWithCGRect:CGRectMake(x, y, w, h)]
#define pointObject(x,y)            [NSValue valueWithCGPoint:CGPointMake(x, y)]
#define sizeObject(w,h)             [NSValue valueWithCGSize:CGSizeMake(w, h)]
#define integerObject(value)        [NSNumber numberWithInteger:value]
#define floatObject(value)          [NSNumber numberWithFloat:value]

static ResolutionManager *sharedResolutionManager = nil;

@interface ResolutionManager ()

@property (nonatomic, strong) NSMutableArray *values;

@end

@implementation ResolutionManager

- (instancetype)init {
    self = [super init];
    if (self) {
        deviceType = TSDevicePad;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            NSString *version = [[UIDevice currentDevice] systemVersion];
            GFLog(@"System Version : %@", version);
            CGFloat height;
            CGRect screenBounds = [[UIScreen mainScreen] bounds];
            
#ifdef __IPHONE_8_0
            if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                height = screenBounds.size.height;
            } else {
                height = screenBounds.size.width;
            }
#else
            height = screenBounds.size.height;
#endif

            if (height > 480) {
                deviceType = TSDevicePhone;
            } else {
                deviceType = TSDevicePhoneSmall;
            }
        }
        
        self.values = [[NSMutableArray alloc] init];
        [self tsviewShare];
        [self tsviewLogin];
        [self tsviewSignin];
        [self tsviewAgreement];
        [self tsviewDoleCoin];
        [self tsviewFacebook];
        [self tsviewSettingMain];
        [self tsviewModify];
        [self tsviewHelp];
        [self tsviewDeleteAccount];
        [self tsviewModifySNS];
        [self tsviewSignupSNS];
        [self tsviewAbout];
        [self tsviewGuide];
    }
    return self;
}

+ (ResolutionManager *)sharedManger {
    if (sharedResolutionManager != nil) {
        return sharedResolutionManager;
    }
    sharedResolutionManager = [[[self class] alloc] init];
    return sharedResolutionManager;
}

- (BOOL)isLongPhone {
    if (deviceType == TSDevicePhone) {
        return YES;
    }
    return NO;
}

- (NSObject *)getValue:(TSViewIndex)viewIndex withObjecID:(NSInteger)objectIndex {
    // NSAssert([self.values count] > viewIndex, @"out og range viewIndex : %d", viewIndex);
    // NSAssert([self.values[viewIndex] count] > objectIndex, @"out og range objectIndex : %d", objectIndex);
    return self.values[viewIndex][objectIndex][deviceType];
}

- (void)tsviewShare {
    NSArray *tsViewShareValues =
    @[//
      @[rectObject(231, 78, 562, 612),     rectObject(0, 0, 320, 568),     rectObject(0, 0, 320, 480)],//TSShareViewControllerRect
      @[rectObject(0, 0, 1024, 768),     rectObject(0, 0, 320, 568),     rectObject(0, 0, 320, 480)],//TSShareSuperViewControllerRect
      @[rectObject( ((2048-828)/2+692)/2 , ((1536-1224)/2+60)/2, 40, 40),     rectObject(271, 9, 40, 40),     rectObject(271, 9, 40, 40)],//TSShareCloseButtonRect
      @[rectObject( (692)/2 , (60)/2, 40, 40),     rectObject(271, 9, 40, 40),     rectObject(271, 9, 40, 40)],//TSShareCloseButtonRectInView
      @[rectObject((1024-414)/2, (768-612)/2, 414, 612),     rectObject(0, 0, 320, 568),     rectObject(0, 0, 320, 480)],//TSShareSubViewRect
      @[rectObject(0, 0, 414, 612),     rectObject(0, 0, 320, 568),     rectObject(0, 0, 320, 480)],//TSShareSubViewOrgRect
      @[rectObject(0, 0, 414, 612),     rectObject(0, 307, 320, 261),   rectObject(0, 227, 320, 253)],//TSShareSeaRect
      @[rectObject(120, 67, 173, 75),   rectObject(73, 37, 173, 75),    rectObject(10, 10, 87, 44)],//TSShareLogoRect
      ];
    [self.values addObject:tsViewShareValues];
}

- (void)tsviewLogin {
    NSArray *tsViewLoginValues =
    @[
      @[rectObject(80, 182, 254, 36),   rectObject(33, 154, 254, 36),    rectObject(33, 77, 254, 36)],//TSLoginIDRect
      @[rectObject(80, 228, 254, 36),   rectObject(33, 200, 254, 36),    rectObject(33, 123, 254, 36)],//TSLoginPWRect
      @[rectObject(80, 281, 254, 37),   rectObject(33, 253, 254, 37),    rectObject(33, 176, 254, 37)],//TSLoginButtonRect
      @[rectObject(80, 459, 254, 16),   rectObject(33, 433, 254, 16),    rectObject(33, 347, 254, 16)],//TSLoginForPWRect
      @[rectObject(80, 497, 254, 16),   rectObject(33, 471, 254, 16),    rectObject(33, 385, 254, 16)],//TSLoginSigninRect
      @[rectObject(80, 535, 254, 16),   rectObject(33, 509, 254, 16),    rectObject(33, 423, 254, 16)],//TSLoginFaceBookRect
      @[rectObject(80, 156, 254, 65),   rectObject(33, 126, 254, 65),    rectObject(33, 56, 254, 65)],//TSLoginForgetLabelRect tuilise_todo : 세로 1px 늘어남.
      @[rectObject(259, 556, 95, 30),   rectObject(185, 530, 95, 30),    rectObject(185, 442, 95, 30)],//TSLoginSkipLabelRect
      @[rectObject(354, 556, 30, 30),   rectObject(280, 530, 30, 30),    rectObject(280, 442, 30, 30)],//TSLoginSKipButtonRect
      ];
    [self.values addObject:tsViewLoginValues];
}

- (void)tsviewSignin {
    NSArray *tsViewSigninValues =
    @[
      @[rectObject(0, 0, 414, 612),     rectObject(0, 307, 320, 261),   rectObject(0, 227, 320, 253)],//TSSigninFactorRect
      @[rectObject(80, 122, 254, 36),     rectObject(33, 106, 254, 36),   rectObject(33, 52, 254, 36)],//TSSigninEmailRect
      @[rectObject(80, 168, 254, 36),     rectObject(33, 152, 254, 36),   rectObject(33, 98, 254, 36)],//TSSigninPasswordRect
      @[rectObject(80, 214, 254, 36),     rectObject(33, 198, 254, 36),   rectObject(33, 144, 254, 36)],//TSSigninCPasswordRect
      @[rectObject(80, 260, 254, 36),     rectObject(33, 244, 254, 36),   rectObject(33, 190, 254, 36)],//TSSigninBirthRect
      @[rectObject(80, 306-(22+70)/2, 254, 36),
        rectObject(33, 290-(22+70)/2, 254, 36),
        rectObject(33, 236-(22+70)/2, 254, 36)],//TSSigninCityRect
      @[rectObject(80, 363, 29, 30),     rectObject(33, 347, 29, 30),   rectObject(33, 293, 29, 30)],//TSSigninMailRadioRect
      @[rectObject(212, 363, 29, 20),     rectObject(165, 347, 29, 30),   rectObject(165, 293, 29, 30)],//TSSigninFemailRadioRect
      @[rectObject(80, 416-(22+70)/2-(44+60)/2, 27, 28),
        rectObject(33, 400-(22+70)/2-(44+60)/2, 27, 28),
        rectObject(33, 347-(22+70)/2-(44+60)/2, 27, 28)],//TSSigninAgreeCheckRect
      @[rectObject(116, 416-(22+70)/2-(44+60)/2, 218, 30),
        rectObject(69, 400-(22+70)/2-(44+60)/2, 218, 30),
        rectObject(69, 347-(22+70)/2-(44+60)/2, 218, 30)],//TSSigninAgreeLabelRect
      @[rectObject(80, 523, 254, 37),     rectObject(33, 495, 254, 37),   rectObject(33, 408, 254, 37)],//TSSigninSignUpBtnRect
      ];
    [self.values addObject:tsViewSigninValues];
}

- (void)tsviewAgreement {
    NSArray *tsViewAgreementValues =
    @[
      @[rectObject(80, 55, 254, 179),     rectObject(33, 28, 254, 179),   rectObject(33, 46, 254, 150)],//TSAgreeTermsRect
      @[rectObject(80, 289, 254, 179),     rectObject(33, 262, 254, 179),   rectObject(33, 234, 254, 150)],//TSAgreePrivacyRect
      @[rectObject(80, 243, 27, 28),     rectObject(33, 216, 27, 28),   rectObject(33, 201, 27, 28)],//TSAgreeTermsCheckRect
      @[rectObject(116, 249, 218, 15),     rectObject(69, 222, 218, 15),   rectObject(69, 207, 218, 15)],//TSAgreeTermsLabelRect
      @[rectObject(80, 477, 27, 28),     rectObject(33, 450, 27, 28),   rectObject(33, 389, 27, 28)],//TSAgreePrivacyCheckRect
      @[rectObject(116, 483-(15/2), 218, 30),     rectObject(69, 456-(15/2), 218, 30),   rectObject(69, 395-(15/2), 218, 30)],//TSAgreePrivacyTwoLineLabelRect
      @[rectObject(116, 483, 218, 15),     rectObject(69, 456, 218, 15),   rectObject(69, 395, 218, 15)],//TSAgreePrivacyLabelRect
      @[rectObject(80, 523, 122, 37),     rectObject(33, 496, 122, 37),   rectObject(33, 422, 122, 37)],//TSAgreeCancelBtnRect
      @[rectObject(212, 523, 122, 37),     rectObject(165, 496, 122, 37),   rectObject(165, 422, 122, 37)],//TSAgreeNextBtnRect
      @[rectObject(80, 82, 254, 206),     rectObject(33, 58, 254, 206),   rectObject(33, 48, 254, 179)],//TSAgreeTermsForAboutRect
      @[rectObject(80, 328, 254, 206),     rectObject(33, 304, 254, 206),   rectObject(33, 264, 254, 179)],//TSAgreePrivacyForAboutRect
      @[rectObject(80, 50, 254, 149),     rectObject(33, 23, 254, 149),   rectObject(33, 41, 254, 120)],//TSAgreeTermsRectAus//hans added
      @[rectObject(80, 242, 254, 149),     rectObject(33, 214, 254, 149),   rectObject(33, 186, 254, 120)],//TSAgreePrivacyRectAus//hans added
      @[rectObject(80, 206, 27, 28),     rectObject(33, 179, 27, 28),   rectObject(33, 164, 27, 28)],//TSAgreeFirstCheckRectAus//hans added
      @[rectObject(112, 205, 222, 30),     rectObject(65, 178, 222, 30),   rectObject(65, 171, 222, 30)],//TSAgreeFirstLabelRectAus//hans added
      @[rectObject(80, 387, 27, 28),     rectObject(33, 370, 27, 28),   rectObject(33, 299, 27, 28)],//TSAgreeSecondCheckRectAus//hans added
      @[rectObject(112, 386, 222, 30),     rectObject(65, 369, 222, 30),   rectObject(65, 298, 222, 30)],//TSAgreeSecondLabelRect//hans added
      @[rectObject(80, 421, 27, 28),     rectObject(33, 404, 27, 28),   rectObject(33, 329, 27, 28)],//TSAgreeThirdCheckRectAus//hans added
      @[rectObject(112, 420, 222, 30),     rectObject(65, 403, 222, 30),   rectObject(65, 328, 222, 30)],//TSAgreeThirdLabelRect//hans added
      @[rectObject(80, 460, 27, 28),     rectObject(33, 443, 27, 28),   rectObject(33, 468, 27, 28)],//TSAgreeFourthCheckRectAus//hans added
      @[rectObject(112, 453, 222, 45),     rectObject(65, 436, 222, 45),   rectObject(65, 461, 222, 45)],//TSAgreeFourthLabelRect//hans added
      ];
    
    [self.values addObject:tsViewAgreementValues];
}

- (void)tsviewDoleCoin {
    NSArray *tsViewDoleCoinValues =
    @[
      @[rectObject(27, 29, 91, 47),   rectObject(10, 10, 87, 44),    rectObject(10, 10, 87, 44)],//TSDCoinLogoRect
      @[rectObject(76, 86, 89, 83),   rectObject(30, 67.5, 89, 83),    rectObject(30, 24, 89, 83)],//TSDCoinCoinRect,//tuilise_todo : iPhone 4inch 위치 소숫점
      @[rectObject(185, 97, 33, 62),   rectObject(139, 77.5, 33, 62),    rectObject(139, 34, 33, 62)],//TSDCoinCountRect,
      @[rectObject(230, 97, 110, 62),   rectObject(184, 77.5, 110, 62),    rectObject(184, 34, 110, 62)],//TSDCoinLabelRect,
      
      @[rectObject(150/2, 476/2, 528/2, 494/2),
        rectObject(56/2, 420/2, 528/2, 494/2),
        rectObject(56/2, 272/2, 528/2, 494/2) ],//TSDCoinQuestionBGRect,
      
      //tuilise_todo : ipad x좌표 가이드보다 5씩 더해야 한다.
      @[rectObject((150+129)/2, (476+(80-34)/2)/2,                                      378/2, 34/2),
        rectObject((56+129)/2,  (420+(80-34)/2)/2,                                      378/2, 34/2),
        rectObject((56+129)/2,  (272+(80-34)/2)/2,                                      378/2, 34/2) ],//TSDCoinQ1LableRect,
      
      @[rectObject((150+21)/2,  (476+80+33+((100-(30+6+30))/2))/2,                      378/2, (30+6+30)/2),
        rectObject((56+21)/2,   (420+80+33+((100-(30+6+30))/2))/2,                      378/2, (30+6+30)/2),
        rectObject((56+21)/2,   (272+80+33+((100-(30+6+30))/2))/2,                      378/2, (30+6+30)/2) ],//TSDCoinA1LableRect,
      
      @[rectObject((150+129)/2, (476+80+33+100+33+(80-34)/2)/2,                         378/2, 34/2),
        rectObject((56+129)/2,  (420+80+33+100+33+(80-34)/2)/2,                         378/2, 34/2),
        rectObject((56+129)/2,  (272+80+33+100+33+(80-34)/2)/2,                         378/2, 34/2)],//TSDCoinQ2LableRect,
      
      @[rectObject((150+21)/2,  (476+80+33+100+33+80+33+((135-(30+6+30+6+30))/2))/2,    378/2, (30+6+30+6+30)/2),
        rectObject((56+21)/2,   (420+80+33+100+33+80+33+((135-(30+6+30+6+30))/2))/2,    378/2, (30+6+30+6+30)/2),
        rectObject((56+21)/2,   (272+80+33+100+33+80+33+((135-(30+6+30+6+30))/2))/2,    378/2, (30+6+30+6+30)/2)],//TSDCoinA2LableRect,
      
      
//      @[rectObject(80, 475, 254, 37),   rectObject(33, 447, 254, 37),    rectObject(33, 359, 254, 37)],//TSDCoinQRRect
      @[rectObject(160/2, (1224-74-104)/2, 508/2, 74/2),
        rectObject(66/2, (1136-74-72)/2, 508/2, 74/2),
        rectObject(66/2, (960-74-72)/2, 508/2, 74/2)],//TSDCoinQRRect//TSDCoinFBRect
      ];
    [self.values addObject:tsViewDoleCoinValues];
}

- (void)tsviewFacebook {
    NSArray *tsViewFacebookValues =
    @[
      @[rectObject(0, 0, 0, 0),   rectObject(0, 0, 0, 0),    rectObject(0, 0, 0, 0)],//
      ];
    [self.values addObject:tsViewFacebookValues];
}

- (void)tsviewSettingMain {
    NSArray *tsViewSettingMainValues =
    @[
      @[rectObject(0, 0, 828/2, 1224/2),
        rectObject(0, (1136-540)/2, 640/2, 540/2),
        rectObject(0, (960-520)/2, 640/2, 520/2)],//TSSetMainBGSeaRect
      @[rectObject(160/2, (134+150+56)/2, 508/2+2, (32+32)/2+2),//tuilise_todo : 두줄이 되려면 가로 가이드가 2px 좀더 커야 한다.
        rectObject(66/2, (74+150+60)/2, 508/2+2, (32+32)/2+2),
        rectObject(66/2, (148)/2, 508/2+2, (32+32)/2+2)],//TSSetMainCommentLabelRect
      @[rectObject(160/2, (134+150+56+32+32+56)/2, 508/2, 74/2),
        rectObject(66/2, (74+150+60+32+32+52)/2, 508/2, 74/2),
        rectObject(66/2, (148+32+32+46)/2, 508/2, 74/2)],//TSSetMainEditButtonRect,
      @[rectObject(160/2, (134+150+70+36+70+74+26)/2, 508/2, 74/2),
        rectObject(66/2, (74+150+70+36+70+74+22)/2, 508/2, 74/2),
        rectObject(66/2, (158+36+64+74+22)/2, 508/2, 74/2)],//TSSetMainUsageButtonRect,
      @[rectObject(160/2, (1224-(34+86+214))/2, 34/2, 34/2),
        rectObject(66/2, (1136-(34+86+180))/2, 34/2, 34/2),
        rectObject(66/2, (960-(34+86+160))/2, 34/2, 34/2)],//TSSetMainBGMusicImageRect,
      
      @[rectObject((160+34+12)/2, (1224-(34+86+214))/2, 302/2, 34/2),
        rectObject((66+34+12)/2, (1136-(34+86+180))/2, 302/2, 34/2),
        rectObject((66+34+12)/2, (960-(34+86+160))/2, 302/2, 34/2)],//TSSetMainBGMusicLabelRect,
      
      @[rectObject((828-(136+160))/2, (1224-(62+72+74+140))/2, 136/2, 62/2),
        rectObject((640-(136+66))/2, (1136-(62+72+74+106))/2, 136/2, 62/2),
        rectObject((640-(136+66))/2, (960-(62+72+74+86))/2, 136/2, 62/2)],//TSSetMainBGMusicSwitchRect,
      
//      @[rectObject(80, 458, 254, 37),   rectObject(33, 431, 254, 37),    rectObject(33, 353, 254, 37)],//TSSetMainFacebookRect,
      @[rectObject(160/2, (1224-(74+140))/2, 244/2, 74/2),
        rectObject(66/2, (1136-(74+106))/2, 244/2, 74/2),
        rectObject(66/2, (960-(74+86))/2, 244/2, 74/2)],//TSSetMainAboutRect,
      
      @[rectObject((828-(244+160))/2, (1224-(74+140))/2, 244/2, 74/2),
        rectObject((640-(244+66))/2, (1136-(74+106))/2, 244/2, 74/2),
        rectObject((640-(244+66))/2, (960-(74+86))/2, 244/2, 74/2)],//TSSetMainHelpRect,
      
      @[rectObject(160/2, (1224-(32+78))/2, (244+20+244)/2, 32/2),
        rectObject(66/2, (1136-(32+44))/2, (244+20+244)/2, 32/2),
        rectObject(66/2, (960-(32+32))/2, (244+20+244)/2, 32/2)],//TSSetMainDeleteRect,
      ];
    [self.values addObject:tsViewSettingMainValues];
}

- (void)tsviewModify {
    NSArray *tsViewModifyValues =
    @[
      @[rectObject(80, 126, 254, 18),   rectObject(33, 107, 254, 18),    rectObject(33, 62, 254, 18)],//TSModifyIDLabelRect,
      @[rectObject(80, 158, 254, 36),   rectObject(33, 139, 254, 36),    rectObject(33, 94, 254, 36)],//TSModifyCurrentPWRect,
      @[rectObject(80, 204, 254, 36),   rectObject(33, 185, 254, 36),    rectObject(33, 140, 254, 36)],//TSModifyChangePWRect,
      @[rectObject(80, 250, 254, 36),   rectObject(33, 231, 254, 36),    rectObject(33, 186, 254, 36)],//TSModifyConfirmPWRect,
      @[rectObject(80, 296, 254, 36),   rectObject(33, 277, 254, 36),    rectObject(33, 232, 254, 36)],//TSModifyBirthYearRect,
      @[rectObject(80, 342-(22+70)/2, 254, 36),
        rectObject(33, 323-(22+70)/2, 254, 36),    rectObject(33, 278-(22+70)/2, 254, 36)],//TSModifyCityRect,
      @[rectObject(80, 399, 29, 30),   rectObject(33, 380, 29, 30),    rectObject(33, 335, 29, 30)],//TSModifyMaleRadioRect,
      @[rectObject(212, 399, 29, 30),   rectObject(165, 380, 29, 30),    rectObject(165, 335, 29, 30)],//TSModifyFemaleRadioRect,
      @[rectObject(80, 523, 254, 37),   rectObject(33, 495, 254, 37),    rectObject(33, 407, 254, 37)],//TSModifySaveButtonRect,
      ];
    [self.values addObject:tsViewModifyValues];
}

- (void)tsviewHelp {
    NSArray *tsViewFacebookValues =
    @[
      @[rectObject(80, 162, 254, 37),   rectObject(33, 134, 254, 37),    rectObject(33, 63, 254, 37)],//TSHelpFAQButtonRect = 0,
      @[rectObject(80, 210, 254, 36),   rectObject(33, 182, 254, 36),    rectObject(33, 111, 254, 36)],//TSHelpEmailTextFieldRect,
      @[rectObject(80, 256, 254, 155),   rectObject(33, 228, 254, 155),    rectObject(33, 157, 254, 155)],//TSHelpContentTextFieldRect,
      @[rectObject(80, 411, 254, 1),   rectObject(33, 383, 254, 1),    rectObject(33, 312, 254, 1)],//TSHelpSeperateViewRect,//tuilise_todo : 1px이다. 비레티나는 어쩔거냐?
      @[rectObject(80, 412, 254, 36),   rectObject(33, 384, 254, 36),    rectObject(33, 313, 254, 36)],//TSHelpDeviceViewRect,
      @[rectObject(80, 459, 254, 28+20),   rectObject(33, 431, 254, 28+20),    rectObject(33, 359, 254, 28+20)],//TSHelpDeviceCommentLabelRect,//tuilise_todo : 세로 사이즈 정의 안되어 있다.
      @[rectObject(80, 523, 254, 37),   rectObject(33, 495, 254, 37),    rectObject(33, 406, 254, 37)],//TSHelpSendButtonRect,
      ];
    [self.values addObject:tsViewFacebookValues];
}

- (void)tsviewDeleteAccount {
    NSArray *tsViewDeleteValues =
    @[
      @[rectObject(29, 30, 87, 44),   rectObject(10, 10, 87, 44),    rectObject(0, 0, 0, 0)],//TSDeleteATitleRect = 0,
      @[rectObject(80, 91, 197, 81),   rectObject(33, 68, 197, 81),    rectObject(33, 30, 197, 81)],//TSDeleteATalkBoxRect,
      @[rectObject(80, 91, 197, 64),   rectObject(33, 68, 197, 64),    rectObject(33, 30, 197, 64)],//TSDeleteATalkBoxLabelRect,
      @[rectObject(259, 136, 79, 61),   rectObject(212, 113, 79, 61),    rectObject(212, 75, 79, 61)],//TSDeleteAMonkeyRect,
      @[rectObject(80, 193, 254, 165),   rectObject(33, 170, 254, 165),    rectObject(33, 132, 254, 165)],//TSDeleteAListRect,
      @[rectObject(80, 369, 27, 28),   rectObject(33, 346, 27, 28),    rectObject(33, 308, 27, 28)],//TSDeleteACheckBoxRect,
      @[rectObject(115, 369, 219, 28),   rectObject(68, 346, 219, 28),    rectObject(68, 308, 219, 28)],//TSDeleteACheckBoxLabelRect,
      @[rectObject(80, 418, 254, 36),   rectObject(33, 395, 254, 36),    rectObject(33, 352, 254, 36)],//TSDeleteAConfirmPWRect,
      @[rectObject(80, 523, 254, 37),   rectObject(33, 495, 254, 37),    rectObject(33, 407, 254, 37)],//TSDeleteADeleteButtonRect,
      ];
    [self.values addObject:tsViewDeleteValues];
}

- (void)tsviewModifySNS {
    NSArray *tsViewModifyValues =
    @[
      @[rectObject(80, 126, 254, 18),   rectObject(33, 107, 254, 18),    rectObject(33, 62, 254, 18)],//TSModifySNSIDLabelRect,
      @[rectObject(80, 158, 254, 36),   rectObject(33, 139, 254, 36),    rectObject(33, 94, 254, 36)],//TSModifySNSCurrentPWRect,
      @[rectObject(80, 204, 254, 36),   rectObject(33, 185, 254, 36),    rectObject(33, 140, 254, 36)],//TSModifySNSBirthYearRect,
      @[rectObject(80, 250-(22+70)/2-(22+70)/2, 254, 36),
        rectObject(33, 231-(70+22)/2-(70+22)/2, 254, 36),
        rectObject(33, 186-(70+22)/2-(70+22)/2, 254, 36)],//TSModifySNSCityRect,
      @[rectObject(80, 307-(70+22)/2, 29, 30),   rectObject(33, 288-(70+22)/2, 29, 30),    rectObject(33, 243-(70+22)/2, 29, 30)],//TSModifySNSMaleRadioRect,
      @[rectObject(212, 307-(70+22)/2, 29, 30),   rectObject(165, 288-(70+22)/2, 29, 30),    rectObject(165, 243-(70+22)/2, 29, 30)],//TSModifySNSFemaleRadioRect,
      @[rectObject(80, 523, 254, 37),   rectObject(33, 495, 254, 37),    rectObject(33, 407, 254, 37)],//TSModifySNSSaveButtonRect,
      ];
    [self.values addObject:tsViewModifyValues];
}

- (void)tsviewSignupSNS {
    NSArray *tsViewSignupSNSValues =
    @[
      @[rectObject(80, 126, 254, 18),   rectObject(33, 107, 254, 18),    rectObject(33, 62, 254, 18)],//TSSignupSNSIDLabelRect,
      @[rectObject(80, 158, 254, 36),   rectObject(33, 139, 254, 36),    rectObject(33, 94, 254, 36)],//TSSignupSNSBirthYearRect,
      @[rectObject(80, 204-(22+70)/2, 254, 36),
        rectObject(33, 185-(22+70)/2, 254, 36),
        rectObject(33, 140-(22+70)/2, 254, 36)],//TSSignupSNSCityRect,
      @[rectObject(80, 261, 29, 30),   rectObject(33, 242, 29, 30),    rectObject(33, 197, 29, 30)],//TSModifySNSMaleRadioRect,
      @[rectObject(212, 261, 29, 30),   rectObject(165, 242, 29, 30),    rectObject(165, 197, 29, 30)],//TSModifySNSFemaleRadioRect,
      @[rectObject(80, 523, 254, 37),   rectObject(33, 495, 254, 37),    rectObject(33, 407, 254, 37)],//TSSignupSNSSignupButtonRect,
      ];
    [self.values addObject:tsViewSignupSNSValues];
}

- (void)tsviewAbout {
    NSArray *tsViewAboutValues =
    @[
      @[rectObject(91, 162, 232, 231),   rectObject(44, 134, 232, 231),    rectObject(44, 65, 232, 231)],//TSAboutServiceImageRect,
      @[rectObject(80, 416, 254, 34),   rectObject(33, 388, 254, 34),    rectObject(33, 315, 254, 34)],//TSAboutCommentRect,
      @[rectObject(80-(12/2), 416-(20/2), 254+12, 34+20),   rectObject(33-(12/2), 388-(20/2), 254+12, 34+20),    rectObject(33-(12/2), 315-(20/2), 254+12, 34+20)],//TSAboutCommentJaRect,
      @[rectObject(80, 473, 254, 17),   rectObject(33, 445, 254, 17),    rectObject(33, 366, 254, 17)],//TSAboutVersionRect,
      @[rectObject(80, 523, 254, 37),   rectObject(33, 495, 254, 37),    rectObject(33, 407, 254, 37)],//TSAboutTermsButtonRect,
      ];
    [self.values addObject:tsViewAboutValues];
}

- (void)tsviewGuide {
    NSArray *tsViewAboutValues =
    @[
      @[rectObject(0, 0, 768, 1024),   rectObject(0, 0, 320, 568),    rectObject(0, 0, 320, 480)],//TSGuideParentViewPortrait,
      @[rectObject(0, 0, 1024, 768),   rectObject(0, 0, 568, 320),    rectObject(0, 0, 480, 320)],//TSGuideParentViewLandscape,
      @[rectObject(0, 589, 1024, 179),   rectObject(0, (640-188), 1136/2, 188/2),    rectObject(0, (640-188), 960/2, 188/2)],//TSGuideCreateCharecterAlpha,
      @[rectObject(394, 210, 236, 236),   rectObject(448/2, 108/2, 240/2, 240/2),    rectObject(448/2-88, 108/2, 240/2, 240/2)],//TSGuideCreateCharecterImage,
      @[rectObject(176, 446, 673, 100),   rectObject(238/2, (108+240)/2, 330, 52),    rectObject(238/2-88, (108+240)/2, 330, 52)],//TSGuideCreateCharecterLabel,
//      @[rectObject(99, 351, 673, 100),   rectObject(176, 446, 673, 100),    rectObject(176, 446, 673, 100)],//TSGuideCreateCharecterLabel,
      
      
      @[rectObject(67, 260, 280, 78),   rectObject(20, 230, 280, 78),    rectObject(20, 147, 280, 78)],//TSGuideLoginMainLabel,
      
      
      @[rectObject(132/2, 772/2, (380+50)/2, 680/2),
        rectObject(122/2, (282)/2, (182+24)/2, 330/2),
        rectObject(122/2-88, (282)/2, (182+24)/2, 330/2)],//TSGuideCameraBobby
      @[rectObject(837/2, 416/2, 374/2, 466/2),
        rectObject(482/2, 120/2, 172/2, 220/2),
        rectObject(482/2-88, 120/2, 172/2, 220/2)],//TSGuideCameraAction
      @[rectObject((132+380)/2, (416+466)/2, 1148/2, 254/2),
        rectObject((122+182)/2, (120+220)/2, 534/2, 118/2),
        rectObject((122+182)/2-88, (120+220)/2, 534/2, 118/2)],//TSGuideCameraTalkBox
      @[rectObject((132+380+37+50)/2, (416+466+31)/2, 1011/2, (96+96)/2),
        rectObject((122+182+16+14)/2, (120+220+16)/2, 490/2, (44+44)/2),
        rectObject((122+182+16+14)/2-88, (120+220+16)/2, 490/2, (44+44)/2)],//TSGuideCameraLabel

      @[rectObject(88/2, 110/2, (552+50+50)/2, (330+62*3+36)/2),
        rectObject(90/2, 238/2, (428+16+16)/2, 378/2),
        rectObject(90/2, 72/2, (428+16+16)/2, 378/2)],//TSGUideLoginTalkBox
      @[rectObject((88+50)/2, (110+330)/2, 552/2, (62*3)/2),
        rectObject((90+16)/2, (238+220+4)/2, 428/2, (44*3)/2),
        rectObject((90+16)/2, (72+220+4)/2, 428/2, (44*3)/2)],//TSGuideLoginLabel

      @[rectObject(105, 351, 60, 50),   rectObject(58, 321, 60, 50),    rectObject(58, 238, 60, 50)],//TSGuideLoginCoinImage,
      @[rectObject(177, 351, 60, 50),   rectObject(130, 321, 60, 50),    rectObject(130, 238, 60, 50)],//TSGuideLoginItemImage,
      @[rectObject(249, 351, 60, 50),   rectObject(202, 321, 60, 50),    rectObject(202, 238, 60, 50)],//TSGuideLoginShareImage,
      
      @[rectObject(99, 406, 72, 28),   rectObject(52, 376, 72, 28),    rectObject(52, 293, 72, 28)],//TSGuideLoginCoinLabel,
      @[rectObject(171, 406, 72, 28),   rectObject(124, 376, 72, 28),    rectObject(124, 293, 72, 28)],//TSGuideLoginItemLabel,
      @[rectObject(243, 406, 72, 28),   rectObject(196, 376, 72, 28),    rectObject(196, 293, 72, 28)],//TSGuideLoginShareLabel,
      
      @[rectObject(207, 483, 100, 100),   rectObject(160, 453, 100, 100),    rectObject(160, 370, 100, 100)],//TSGuideLoginlauncherImage,
      ];
    [self.values addObject:tsViewAboutValues];
}

@end
