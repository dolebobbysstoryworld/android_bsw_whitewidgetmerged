//
//  TSDoleCoinView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 2..
//
//

#import "TSDoleCoinView.h"
#import "ResolutionManager.h"
#import "UIImage+PathExtention.h"
#import "UIButton+DolesUI.h"
#import "UIColor+RGB.h"
#import "TSDolesNet.h"
#import "TSToastView.h"
#import "TSMessageView.h"

@interface TSDoleCoinView () < TSNetDelegate, TSPopupViewDelegate >

@property (nonatomic, strong) UIButton *qrReadButton;
@property (nonatomic, strong) UIButton *inviteFacebookButton;
@property (nonatomic, strong) UILabel *balanceLabel;
@property (nonatomic, strong) TSNetGetBalance *net;

@end

@implementation TSDoleCoinView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        UIImageView *factorDoleCoinView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"bg_map_factor_dolecoin.png"]];
        factorDoleCoinView.frame = getRectValue(TSViewIndexShare, TSShareSubViewOrgRect);
        [self addSubview:factorDoleCoinView];
        
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        if (screenBounds.size.height > 480) {
            UIImageView *logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"dolecoin_logo.png" ignore:YES]];
            logoView.frame = getRectValue(TSViewIndexCoin, TSDCoinLogoRect);
            [self addSubview:logoView];
        }
    
        [self adjustCoinlayout];
        [self adjustQuestionDoleCoin];
        [self addButtons];
    }
    return self;
}

- (void)adjustCoinlayout {
    UIImageView *coinView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"dole_coin_pile.png" ignore:YES]];
    coinView.frame = getRectValue(TSViewIndexCoin, TSDCoinCoinRect);
    [self addSubview:coinView];
    
    UIImageView *xView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"dole_coin_x.png" ignore:YES]];
    xView.frame= getRectValue(TSViewIndexCoin, TSDCoinCountRect);
    [self addSubview:xView];
    
    self.balanceLabel = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexCoin, TSDCoinLabelRect)];
    self.balanceLabel.font = [UIFont fontWithName:kDefaultBoldFontName size:60];
    self.balanceLabel.textColor = RGBH(@"#ff5411");
    self.balanceLabel.textAlignment = NSTextAlignmentCenter;
    self.balanceLabel.numberOfLines = 1;
    self.balanceLabel.adjustsFontSizeToFitWidth = YES;
    self.balanceLabel.text = @"?";//tuilise_todo : shadow 추가 작업
    self.balanceLabel.shadowColor = RGBAH(@"#0d565b", 0.12f);
    self.balanceLabel.shadowOffset = CGSizeMake(1, 1);
    self.balanceLabel.layer.masksToBounds = NO;
    [self addSubview:self.balanceLabel];

}

- (void)viewDidAppeared {
    NSInteger userNo = [[[NSUserDefaults standardUserDefaults] valueForKey:UD_INTEGER_USERNO] integerValue];
    if (userNo > 0) {
        self.net = [[TSNetGetBalance alloc] init];
        self.net.delegate = self;
        [self.net start];
    }
}

- (void)adjustQuestionDoleCoin {
    UIImageView *bgView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"dole_coin_question.png"]];
    bgView.frame = getRectValue(TSViewIndexCoin, TSDCoinQuestionBGRect);
    [self addSubview:bgView];
    
    //tuilise_todo : 다중 라인 label이라고 하더라고 다국어에서 자동 폰트 사이즈 조절이 가능해야 한다.
    UILabel *q1label = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexCoin, TSDCoinQ1LableRect)];
    q1label.font = [UIFont fontWithName:kDefaultBoldFontName size:15];
    q1label.textColor = RGBH(@"#19a9d8");
    q1label.textAlignment = NSTextAlignmentCenter;
    q1label.numberOfLines = 1;
    q1label.adjustsFontSizeToFitWidth = YES;
//    q1label.backgroundColor = [UIColor lightGrayColor];
    q1label.text = NSLocalizedString(@"PF_DOLECOIN_ABOUTCOIN_1", @"What is a Dole Coin?");
    [self addSubview:q1label];
    
    UILabel *a1label = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexCoin, TSDCoinA1LableRect)];
    a1label.font = [UIFont fontWithName:kDefaultFontName size:13];
    a1label.textColor = RGBH(@"#4f2c00");
    a1label.textAlignment = NSTextAlignmentCenter;
    a1label.numberOfLines = 0;
    a1label.adjustsFontSizeToFitWidth = YES;
//    a1label.backgroundColor = [UIColor lightGrayColor];
    a1label.text = NSLocalizedString(@"PF_DOLECOIN_ABOUTCOIN_2", @"Earn and accumulate Dole Coins to win rewards and prizes");
    [self addSubview:a1label];

    UILabel *q2label = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexCoin, TSDCoinQ2LableRect)];
    q2label.font = [UIFont fontWithName:kDefaultBoldFontName size:15];
    q2label.textColor = RGBH(@"#19a9d8");
    q2label.textAlignment = NSTextAlignmentCenter;
    q2label.numberOfLines = 1;
    q2label.adjustsFontSizeToFitWidth = YES;
//    q2label.backgroundColor = [UIColor lightGrayColor];
    q2label.text = NSLocalizedString(@"PF_DOLECOIN_HOWEARN_1", @"How can I earn Dole coins?");
    [self addSubview:q2label];

    UILabel *a2label = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexCoin, TSDCoinA2LableRect)];
    a2label.font = [UIFont fontWithName:kDefaultFontName size:13];
    a2label.textColor = RGBH(@"#4f2c00");
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if ([language isEqualToString:@"ja"]) {
        a2label.textAlignment = NSTextAlignmentCenter;
    } else if ([language isEqualToString:@"ko"]) {
        a2label.textAlignment = NSTextAlignmentCenter;
    } else {
        a2label.textAlignment = NSTextAlignmentLeft;
    }
    a2label.numberOfLines = 0;
    a2label.adjustsFontSizeToFitWidth = YES;
//    a2label.backgroundColor = [UIColor lightGrayColor];
    a2label.text = NSLocalizedString(@"PF_DOLECOIN_HOWEARN_2", @"• Scan QR code\n• Share on social network");//\n• Invite Facebook friends
    [self addSubview:a2label];
}

#pragma mark - TSNetDelegate
- (void)net:(TSNet*)netObject didStartProcess:(TSNetResultType)result {
    
}

- (void)net:(TSNet*)netObject didEndProcess:(TSNetResultType)result {
    if (!result) {
        self.balanceLabel.text = self.net.eventBalance;
    } else {//tuilise_todo : 오류 결과다. 맞는 처리 필요
        NSInteger balance = [[[NSUserDefaults standardUserDefaults] valueForKey:UD_INTEGER_BALANCE] integerValue];
        if (balance != -1) {
            self.balanceLabel.text = [NSString stringWithFormat:@"%d", balance];
        }
    }
}

- (void)net:(TSNet*)netObject didFailWithError:(TSNetResultType)result {
    switch (result) {
        case TSNetErrorNetworkDisable:
        case TSNetErrorTimeout: {
            TSToastView *toast = [[TSToastView alloc] initWithString:NSLocalizedString(@"CM_NET_WARNING_NETWORK_UNSTABLE", @"The network is unstable.\nPlease try again")];
            [toast showInViewWithAnimation:self];
        } break;
        default: {
        } break;
    }
    NSInteger balance = [[[NSUserDefaults standardUserDefaults] valueForKey:UD_INTEGER_BALANCE] integerValue];
    if (balance != -1) {
        self.balanceLabel.text = [NSString stringWithFormat:@"%d", balance];
    }
}

- (void)addButtons {
    self.qrReadButton = [UIButton buttonWithType:TSDolsTypeOK Frame:getRectValue(TSViewIndexCoin, TSDCoinQRRect)
                                           Title:NSLocalizedString(@"PF_DOLECOIN_BUTTONTITLE_SCAN", @"Scan QR code")];
    [self.qrReadButton addTarget:self action:@selector(selectorQRReadButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.qrReadButton];

//    self.inviteFacebookButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    self.inviteFacebookButton.frame = getRectValue(TSViewIndexCoin, TSDCoinFBRect);
//    UIEdgeInsets inSet = UIEdgeInsetsMake(17, 38, 18, 24);
//    [self.inviteFacebookButton setBackgroundImage:[[UIImage imageNamedEx:@"invite_facebook_btn_normal.png" ignore:YES] resizableImageWithCapInsets:inSet]
//                      forState:UIControlStateNormal];
//    [self.inviteFacebookButton setBackgroundImage:[[UIImage imageNamedEx:@"invite_facebook_btn_press.png" ignore:YES] resizableImageWithCapInsets:inSet]
//                      forState:UIControlStateHighlighted];
//    [self.inviteFacebookButton setTitle:@"Invite Facebook friends" forState:UIControlStateNormal];
//    self.inviteFacebookButton.titleLabel.font = [UIFont fontWithName:kDefaultBoldFontName size:15];
//    [self.inviteFacebookButton setTitleColor:RGBH(@"#ffffff") forState:UIControlStateNormal];
//    self.inviteFacebookButton.titleLabel.numberOfLines = 1;
//    self.inviteFacebookButton.titleLabel.textAlignment = NSTextAlignmentCenter;
//    [self.inviteFacebookButton addTarget:self action:@selector(selectorInviteFacebookButton:) forControlEvents:UIControlEventTouchUpInside];
//    [self.inviteFacebookButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 50, 0, 13)];
//    [self addSubview:self.inviteFacebookButton];
}

- (IBAction)selectorQRReadButton:(id)sender {
    NSString *authKey = [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_AUTHKEY];
    if ([authKey length] > 0) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(qrViewControllerWithView:)]) {
            [self.delegate qrViewControllerWithView:self];
        }
    } else {
        //TODO::JIRAJAPAN
        TSMessageView *messageView =
        [[TSMessageView alloc] initWithMessage:NSLocalizedString(@"PF_DOLECOIN_NEED_LOGIN", @"You need to log in") icon:TSPopupIconTypeWarning];
        messageView.customButtonTitle = NSLocalizedString(@"CC_HOME_POPUP_NEED_LOGIN_BUTTON_TITLE_LOGIN", @"Log in");
        messageView.buttonLayout = TSPopupButtonLayoutCancelCustom;
//        messageView.tag = TAG_POPUP_FAIL_COUPONUNUSED;
        NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
        if ([language isEqualToString:@"ja"] == YES) {
            messageView.customCancelTitle = @"もどる";
            messageView.customButtonTitle = @"ログインする";
        }
        messageView.delegate = self;
        [messageView showInView:self];
    }
}

- (IBAction)selectorInviteFacebookButton:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(facebookViewControllerWithView:)]) {
        [self.delegate facebookViewControllerWithView:self];
    }
}

- (void)popview:(TSPopupView *)view didClickedButton:(TSPopupButtonType)type {
    if (type == TSPopupButtonTypeCustom) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(loginViewControllerWithView:)]) {
            [self.delegate loginViewControllerWithView:self];
        }
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
