//
//  TSSettingMainView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 10..
//
//

#import "TSSettingMainView.h"
#import "UIImage+PathExtention.h"
#import "UIColor+RGB.h"
#import "UIButton+DolesUI.h"
#import "UILabel+DolesUI.h"
#import "TSProxyHelper.h"
#import "MBSwitch.h"
#import "TSToastView.h"
//#import "TSDolesNet.h"

@interface TSSettingMainView ()

@property (nonatomic, strong) UIButton *inviteFacebookButton;
//@property (nonatomic, strong) TSNetGetPurchaseList *net;

@end

@implementation TSSettingMainView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"bg_setting_sea.png"]];
//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//            backgroundView.frame = getRectValue(TSViewIndexShare, TSShareSeaRect);
//        } else {
            backgroundView.frame = getRectValue(TSViewIndexSettingMain, TSSetMainBGSeaRect);
//        }
        [self addSubview:backgroundView];
        UIImageView *factorView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"bg_map_factor.png"]];
        factorView.frame = getRectValue(TSViewIndexShare, TSShareSubViewOrgRect);
        [self addSubview:factorView];
        
        UIImageView *logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"login_logo.png"]];
        logoView.frame = getRectValue(TSViewIndexShare, TSShareLogoRect);
        [self addSubview:logoView];
        
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:UD_BOOL_FACEBOOKLOGIN] boolValue] == YES) {
            [self addSubview:[UILabel viewForLoginIDLabel:getRectValue(TSViewIndexSettingMain, TSSetMainCommentLabelRect)]];
        } else {
            UILabel *commentLabel = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexSettingMain, TSSetMainCommentLabelRect)];
            commentLabel.font = [UIFont fontWithName:kDefaultFontName size:14];
            commentLabel.textAlignment = NSTextAlignmentCenter;
            commentLabel.textColor = RGBH(@"#4f2c00");
            commentLabel.numberOfLines = 0;
            commentLabel.text = NSLocalizedString(@"PF_SETTING_NOTICE_MORE_ADVANTAGE_LOGIN", @"You can enjoy more features\nby logging in.");
            NSString *authKey = [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_AUTHKEY];
            if ([authKey length] > 0) {
                commentLabel.font = [UIFont fontWithName:kDefaultBoldFontName size:32/2];
                commentLabel.text = [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_LOGINID];
            }
            [self addSubview:commentLabel];
        }

        [self adjustButtonLayout];
        [self adjustBackgroundMusicLayout];
//        [self adjustFacebookButtonLayout];
    }
    return self;
}

- (void)adjustButtonLayout {
    NSString *authKey = [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_AUTHKEY];
    if ([authKey length] > 0) {
        UIButton *editButton = [UIButton buttonWithType:TSDolsTypeOK Frame:getRectValue(TSViewIndexSettingMain, TSSetMainEditButtonRect)
                                                  Title:NSLocalizedString(@"PF_SETTING_BUTTON_TITLE_EDIT", @"Edit")];
        [editButton addTarget:self action:@selector(selectorEditButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:editButton];
        UIButton *usageButton = [UIButton buttonWithType:TSDolsTypeCancel Frame:getRectValue(TSViewIndexSettingMain, TSSetMainUsageButtonRect)
                                                   Title:NSLocalizedString(@"PF_SETTING_BUTTON_TITLE_USAGE_COIN", @"Dole Coin usage")];
        [usageButton addTarget:self action:@selector(selectorCoinUsageButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:usageButton];
    } else {
        UIButton *loginButton = [UIButton buttonWithType:TSDolsTypeOK Frame:getRectValue(TSViewIndexSettingMain, TSSetMainEditButtonRect)
                                                   Title:NSLocalizedString(@"PF_SETTING_BUTTON_TITLE_LOGIN", @"Login")];
        [loginButton addTarget:self action:@selector(selectorLoginButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:loginButton];
    }
    
    UIButton *aboutButton = [UIButton buttonWithType:TSDolsTypeAbout Frame:getRectValue(TSViewIndexSettingMain, TSSetMainAboutRect)
                                               Title:NSLocalizedString(@"PF_SETTING_BUTTON_TITLE_ABOUT", @"About")];
    [aboutButton addTarget:self action:@selector(selectorAboutButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:aboutButton];

    UIButton *helpButton = [UIButton buttonWithType:TSDolsTypeAbout Frame:getRectValue(TSViewIndexSettingMain, TSSetMainHelpRect)
                                              Title:NSLocalizedString(@"PF_SETTING_BUTTON_TITLE_HELP", @"Help")];
    [helpButton addTarget:self action:@selector(selectorHelpButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:helpButton];
    
    if ([authKey length] > 0) {
        UIButton *deleteAccountButton = [UIButton underlineButtonWithTitle:NSLocalizedString(@"PF_SETTING_BUTTON_TITLE_DELETE_ACCOUNT", @"Leave Bobby's Journey")
                                                                     Frame:getRectValue(TSViewIndexSettingMain, TSSetMainDeleteRect)];
        [deleteAccountButton addTarget:self action:@selector(selectorDeleteAccountButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:deleteAccountButton];
    }
}

- (IBAction)selectorCoinUsageButton:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(view:Page:userData:)]) {
        [self.delegate view:self Page:TSViewIndexUsage userData:nil];
    }
}

- (IBAction)selectorLoginButton:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(loginViewControllerWithView:)]) {
        [self.delegate loginViewControllerWithView:self];
    }
}

- (IBAction)selectorEditButton:(id)sender {
//    NSDate *startDate, *endDate;
//	NSDateComponents *com;
//	
//	com = [[NSDateComponents alloc] init];
//
//	[com setYear:2014];
//	[com setMonth:3];
//	[com setDay:1];
//	startDate = [[NSCalendar currentCalendar] dateFromComponents:com];
//	[com setYear:2014];
//	[com setMonth:7];
//	[com setDay:31];
//	endDate = [[NSCalendar currentCalendar] dateFromComponents:com];
//    
//    self.net =
//    [[TSNetGetPurchaseList alloc] initWithUserNo:[[[NSUserDefaults standardUserDefaults] valueForKey:UD_INTEGER_USERNO] integerValue] begin:startDate end:endDate];
//    [self.net start];
//    return;
    
    
    BOOL snsLogin = [[NSUserDefaults standardUserDefaults] valueForKey:UD_BOOL_FACEBOOKLOGIN];
    TSViewIndex viewIndex = TSViewIndexModify;
    if (snsLogin) {
        viewIndex = TSViewIndexModifySNS;
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(view:Page:userData:)]) {
        [self.delegate view:self Page:viewIndex userData:nil];
    }
}

- (IBAction)selectorAboutButton:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(view:Page:userData:)]) {
        [self.delegate view:self Page:TSViewIndexAbout userData:nil];
    }
}

- (IBAction)selectorHelpButton:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(view:Page:userData:)]) {
        [self.delegate view:self Page:TSViewIndexHelp userData:nil];
    }
}

- (IBAction)selectorDeleteAccountButton:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(view:Page:userData:)]) {
        [self.delegate view:self Page:TSViewIndexDeleteAccount userData:nil];
    }
}

- (void)adjustBackgroundMusicLayout {
    UIImageView *bgMusicView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"music_icon.png" ignore:YES]];
    bgMusicView.frame = getRectValue(TSViewIndexSettingMain, TSSetMainBGMusicImageRect);
    [self addSubview:bgMusicView];
    
    UILabel *bgMusicLabel = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexSettingMain, TSSetMainBGMusicLabelRect)];
    bgMusicLabel.font = [UIFont fontWithName:kDefaultBoldFontName size:15];
    bgMusicLabel.textAlignment = NSTextAlignmentLeft;
    bgMusicLabel.textColor = RGBH(@"#ffffff");
    bgMusicLabel.numberOfLines = 1;
    bgMusicLabel.adjustsFontSizeToFitWidth = YES;
    bgMusicLabel.text = NSLocalizedString(@"PF_SETTING_SOUND", @"Background music");
    [self addSubview:bgMusicLabel];
    
    MBSwitch *musicSwitch = [[MBSwitch alloc] initWithFrame:getRectValue(TSViewIndexSettingMain, TSSetMainBGMusicSwitchRect)];
    musicSwitch.onTintColor = RGBH(@"#3186a8");
    musicSwitch.offTintColor = RGBH(@"#32d7d7");//tuilise_todo : Image setting 되는 Switch control 확인 필요
    [musicSwitch addTarget:self action:@selector(selectorMusicSwitch:) forControlEvents:UIControlEventValueChanged];
    [self addSubview:musicSwitch];
    
//    float volume = [[[NSUserDefaults standardUserDefaults] valueForKey:UD_FLOAT_VOLUME] floatValue];
//    if (volume == 0) {
//        musicSwitch.on = NO;
//    } else {
//        musicSwitch.on = YES;
//    }
    BOOL mute = [[[NSUserDefaults standardUserDefaults] valueForKey:UD_BOOL_MUTEBGM] boolValue];
    if (mute == YES) {
        musicSwitch.on = NO;
    } else {
        musicSwitch.on = YES;
    }
}

- (IBAction)selectorMusicSwitch:(id)sender {
    if ([((UISwitch*)sender) isOn] == YES) {
        NSLog(@"Switch ON");
        [[NSNotificationCenter defaultCenter] postNotificationName:kTSNotifivationSetMute object:nil userInfo:@{@"volume": [NSNumber numberWithBool:YES]}];
    } else {
        NSLog(@"Switch OFF");
        [[NSNotificationCenter defaultCenter] postNotificationName:kTSNotifivationSetMute object:nil userInfo:@{@"volume": [NSNumber numberWithBool:NO]}];
    }
}

- (void)adjustFacebookButtonLayout {
    self.inviteFacebookButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    self.inviteFacebookButton.frame = getRectValue(TSViewIndexSettingMain, TSSetMainFacebookRect);
    UIEdgeInsets inSet = UIEdgeInsetsMake(17, 38, 18, 24);
    [self.inviteFacebookButton setBackgroundImage:[[UIImage imageNamedEx:@"invite_facebook_btn_normal.png" ignore:YES] resizableImageWithCapInsets:inSet]
                                         forState:UIControlStateNormal];
    [self.inviteFacebookButton setBackgroundImage:[[UIImage imageNamedEx:@"invite_facebook_btn_press.png" ignore:YES] resizableImageWithCapInsets:inSet]
                                         forState:UIControlStateHighlighted];
    [self.inviteFacebookButton setTitle:@"Invite Facebook friends" forState:UIControlStateNormal];
    self.inviteFacebookButton.titleLabel.font = [UIFont fontWithName:kDefaultBoldFontName size:15];
    [self.inviteFacebookButton setTitleColor:RGBH(@"#ffffff") forState:UIControlStateNormal];
    self.inviteFacebookButton.titleLabel.numberOfLines = 1;
    self.inviteFacebookButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.inviteFacebookButton addTarget:self action:@selector(selectorInviteFacebookButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.inviteFacebookButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 50, 0, 13)];
    [self addSubview:self.inviteFacebookButton];
}

- (IBAction)selectorInviteFacebookButton:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(facebookViewControllerWithView:)]) {
        [self.delegate facebookViewControllerWithView:self];
    }
}

- (void)setObjects:(id)objects {
    _objects = objects;
    [self performSelector:@selector(selectorAfterObjectProcess:) withObject:self.objects afterDelay:0.2f];
}

- (void)selectorAfterObjectProcess:(id)object {
    NSDictionary *objectsDic = (NSDictionary*)object;
    if ([objectsDic objectForKey:@"toast"] != nil) {
        TSToastView *toast = [[TSToastView alloc] initWithString:[objectsDic objectForKey:@"toast"]];
        [toast showInViewWithAnimation:self];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
