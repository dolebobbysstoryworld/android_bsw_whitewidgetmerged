//
//  TSSettingMainView.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 10..
//
//

#import <UIKit/UIKit.h>
#import "TSAccountDelegateProtocol.h"

@interface TSSettingMainView : UIView

@property (nonatomic, assign) id<TSAccountDelegate> delegate;
@property (nonatomic, strong) id objects;

@end
