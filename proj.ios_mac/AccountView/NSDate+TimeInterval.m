//
//  NSDate+TimeInterval.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 19..
//
//

#import "NSDate+TimeInterval.h"

@implementation NSDate (TimeInterval)

+ (NSString*)stringFromTimeIntervalSinceDate:(NSDate*)fromDate {
//    NSLog(@"from : %@ / now : %@", fromDate, [NSDate date]);
    double interval = [[NSDate date] timeIntervalSinceDate:fromDate] * 1000.0;
    
    NSInteger hour = interval / (60*60*1000);
    interval = interval - hour*(60*60*1000);
    NSInteger minute = interval / (60*1000);
    interval = interval - minute*(60*1000);
    NSInteger secound = interval / 1000;
    NSInteger millisecond = interval - secound*1000;
    NSString *string = [NSString stringWithFormat:@"%.2d:%.2d:%.2d.%.3d", hour, minute, secound, millisecond];
    return string;
}

- (NSString*)stringFromTimeIntervalNow {
    double interval = [self timeIntervalSinceNow] * -1000;

    NSInteger hour = interval / (60*60*1000);
    interval = interval - hour*(60*60*1000);
    NSInteger minute = interval / (60*1000);
    interval = interval - minute*(60*1000);
    NSInteger secound = interval / 1000;
    NSInteger millisecond = interval - secound*1000;
    NSString *string = [NSString stringWithFormat:@"%.2d:%.2d:%.2d.%.3d", hour, minute, secound, millisecond];
    return string;
}

@end
