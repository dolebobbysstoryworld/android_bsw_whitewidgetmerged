//
//  TSAccountDelegate.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 2..
//
//

#import <Foundation/Foundation.h>
#import "ResolutionManager.h"

@protocol TSAccountDelegate <NSObject>

- (void)view:(UIView*)view Page:(TSViewIndex)index userData:(id)object;
@optional
- (void)closeViewControllerWithView:(UIView *)view;
- (void)qrViewControllerWithView:(UIView *)view;
- (void)facebookViewControllerWithView:(UIView *)view;
- (void)loginViewControllerWithView:(UIView*)view;

@end
