//
//  TSHelpFAQView.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 10..
//
//

#import <UIKit/UIKit.h>
#import "TSAccountDelegateProtocol.h"

@interface TSHelpFAQView : UIView

@property (nonatomic, assign) id<TSAccountDelegate> delegate;

@end
