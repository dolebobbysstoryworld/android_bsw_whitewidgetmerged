//
//  TSTextField.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 30..
//
//

#import <UIKit/UIKit.h>

@class TSTextField;
@protocol TSTextFieldDelegate <NSObject>

@optional
- (void)tstextFieldDidEndEditing:(TSTextField *)textField;
- (void)tstextFieldTouched:(TSTextField *)textField;
- (BOOL)tstextField:(TSTextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

@end

@interface TSTextField : UIView

@property (nonatomic, assign) id<TSTextFieldDelegate> delegate;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, assign) BOOL editable;
@property (nonatomic, assign) BOOL enabled;
@property (nonatomic, assign) BOOL showActivity;
@property (nonatomic, assign) BOOL mandatory;

- (id)initWithFrame:(CGRect)frame icon:(NSString*)name placeholder:(NSString*)string;
- (id)initWithFrame:(CGRect)frame icon:(NSString*)name placeholder:(NSString*)string isPopup:(BOOL)popup;
- (id)initWithFrame:(CGRect)frame icon:(NSString*)name placeholder:(NSString*)string editable:(BOOL)editable;
- (id)initWithFrame:(CGRect)frame icon:(NSString*)name placeholder:(NSString*)string editable:(BOOL)editable isPopup:(BOOL)popup;
- (void)setComment:(NSString*)string;
- (void)setError:(NSString*)string;
- (void)setKeyboardType:(UIKeyboardType)type;
- (void)setPasswordType:(BOOL)secure;

//utility
- (BOOL)checkEmailStringValidationWithShowError:(NSString*)error;
- (BOOL)checkPasswordStringValidationWithShowError:(NSString*)error;
- (BOOL)equalPasswordString:(NSString*)target withShowError:(NSString*)error;

@end

@interface TSPTextField : UITextField

@end
