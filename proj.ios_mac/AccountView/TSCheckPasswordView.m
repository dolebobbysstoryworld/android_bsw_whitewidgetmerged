//
//  TSCheckPasswordView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 23..
//
//

#import "TSCheckPasswordView.h"
#import "TSTextField.h"

@interface TSCheckPasswordView () < TSTextFieldDelegate >

@property (nonatomic, assign) NSInteger coin;
@property (nonatomic, strong) TSTextField *pwTextField;
@property (nonatomic, strong) UIButton *forgotPasswordButton;

@end

@implementation TSCheckPasswordView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoin:(NSInteger)coin {
    self = [super init];
    if (self) {
        self.coin = coin;
        self.customButtonTitle = NSLocalizedString(@"CC_EXPANDEDITEM_POPUP_COIN_BUTTON_TITLE_UNLOCK", @"Unlock");
    }
    return self;
}

- (void)adjustContentViewLayout {
    self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0, self.topView.frame.size.height, 272, 208)];
    UIImageView *middle = [[UIImageView alloc] initWithImage:
                           [[UIImage imageNamedEx:@"popup_middle.png" ignore:YES]
                            resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)]];
    middle.frame = self.contentView.frame;
    [self addSubview:middle];
    
    UIImageView *iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"popup_warning.png" ignore:YES]];
    iconView.frame = CGRectMake(103, 0, 65, 65);//tuilis_todo : x pos 소수점 나온다. 우선 버렸음
//    iconView.backgroundColor = [UIColor purpleColor];
    [self.contentView addSubview:iconView];
    
    UILabel *labelNotice = [[UILabel alloc] initWithFrame:CGRectMake(9, 65, 254, 60)];
    labelNotice.font = [UIFont fontWithName:kDefaultFontName size:17];
    labelNotice.textColor = RGBH(@"#787878");
    labelNotice.textAlignment = NSTextAlignmentCenter;
    labelNotice.numberOfLines = 0;
    labelNotice.text =
    [NSString stringWithFormat:NSLocalizedString(@"CC_EXPANDEDITEM_POPUP_WARNING_USE_UNLOCK_COIN", @"Unlock with %d Dole Coin.\nThis action cannot be undone"), self.coin];
//    labelNotice.backgroundColor = [UIColor redColor];
    [self.contentView addSubview:labelNotice];
    
    self.pwTextField =
    [[TSTextField alloc] initWithFrame:CGRectMake(9, 125, 254, 35)
                                  icon:@"input_password_icon.png" placeholder:NSLocalizedString(@"PF_LOGIN_TEXTFIELD_PASSWORD", @"Password") isPopup:YES];
    self.pwTextField.delegate = self;
//    self.pwTextField.backgroundColor = [UIColor yellowColor];
    [self.pwTextField setPasswordType:YES];
    [self.contentView addSubview:self.pwTextField];
    
    self.forgotPasswordButton = [UIButton underlineButtonWithTitle:NSLocalizedString(@"PF_LOGIN_BUTTON_TITLE_FORGOT", @"Forgot your password?")
                                                             Frame:CGRectMake(9, self.pwTextField.frame.origin.y+35+15, 254, 16)
                                 textColor:RGBH(@"#11a6b4")];
    [self.forgotPasswordButton addTarget:self action:@selector(selectorUnderlineButton:) forControlEvents:UIControlEventTouchUpInside];
//    self.forgotPasswordButton.backgroundColor = [UIColor blueColor];
    [self.contentView addSubview:self.forgotPasswordButton];

    [self addSubview:self.contentView];
}

- (void)adjustBottomViewLayout {
    [super adjustBottomViewLayout];
    self.rightButton.enabled = NO;
}

- (NSString*)password {
    return self.pwTextField.text;
}

- (void)setKeyboardShow:(BOOL)keyboardShow {
    _keyboardShow = keyboardShow;
    self.forgotPasswordButton.userInteractionEnabled = !self.keyboardShow;
}

- (BOOL)buttonWillOKProcessing {
    NSString *loginPassword = [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_LOGINPW];
    NSString *inputPassword = self.pwTextField.text;
    if ([loginPassword isEqualToString:inputPassword] == NO) {
        [self.pwTextField setError:NSLocalizedString(@"PF_NET_ERROR__PASSWORD_INCORRECT", @"The password is incorrect")];
        self.rightButton.enabled = NO;
        return NO;
    }
    return YES;
}


#pragma mark - TSTextFiledDelegate
- (void)tstextFieldDidEndEditing:(TSTextField *)textField {
    BOOL validPassword = [self.pwTextField checkPasswordStringValidationWithShowError:
                          NSLocalizedString(@"PF_NET_ERROR__PASSWORD_SHORT", @"￼Please enter a password with at least 6 characters")];
    if (validPassword == YES) {
        self.rightButton.enabled = YES;
    } else {
        self.rightButton.enabled = NO;
    }
}

- (IBAction)selectorUnderlineButton:(id)sender {

}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
