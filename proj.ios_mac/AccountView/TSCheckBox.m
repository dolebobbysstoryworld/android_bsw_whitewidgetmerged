//
//  TSCheckBox.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 2..
//
//

#import "TSCheckBox.h"

@interface TSCheckBox ()

@property (nonatomic, strong) UIImageView *viewForNormalStatus;
@property (nonatomic, strong) UIImageView *viewForSelectedStatus;
@property (nonatomic, strong) UIImageView *viewForDisableStatus;

@end

@implementation TSCheckBox

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setImage:(UIImage*)image forState:(UIControlState)state {
    switch (state) {
        case UIControlStateNormal: {
            self.viewForNormalStatus = [[UIImageView alloc] initWithImage:image];
            [self addSubview:self.viewForNormalStatus];
        } break;
        case UIControlStateSelected: {
            self.viewForSelectedStatus = [[UIImageView alloc] initWithImage:image];
        } break;
        case UIControlStateDisabled: {
            self.viewForDisableStatus = [[UIImageView alloc] initWithImage:image];
        } break;
        default: { } break;
    }
    CGPoint origin = self.frame.origin;
    CGSize size = self.viewForNormalStatus.frame.size;
    self.frame = CGRectMake(origin.x, origin.y, size.width, size.height);
}

- (void)setSelected:(BOOL)selected {
    if (self.selected == selected) {
        return;
    }
    
    if (!self.enabled)
        return;
    
    super.selected = selected;
    if (self.selected) {
        [self.viewForNormalStatus removeFromSuperview];
        [self addSubview:self.viewForSelectedStatus];
    } else {
        [self.viewForSelectedStatus removeFromSuperview];
        [self addSubview:self.viewForNormalStatus];
    }
}

- (void)setEnabled:(BOOL)enabled {
    if (self.enabled == enabled) {
        return;
    }
    super.enabled = enabled;
    
    if (self.enabled) {
        [self.viewForDisableStatus removeFromSuperview];
        [self setSelected:self.selected];
    } else {
        if (self.selected) {
            [self.viewForSelectedStatus removeFromSuperview];
        } else {
            [self.viewForNormalStatus removeFromSuperview];
        }
        [self addSubview:self.viewForDisableStatus];
    }
}

#pragma mark - touches
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    if (!self.enabled)
        return;
    
    [super touchesBegan:touches withEvent:event];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    if (!self.enabled)
        return;
    
    [super touchesCancelled:touches withEvent:event];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    if (!self.enabled)
        return;
    
    self.alpha = 1.0f;
    
    if ([self superview]){
        UITouch *touch = [touches anyObject];
        CGPoint point = [touch locationInView:[self superview]];
        //        CGRect validTouchArea = CGRectMake((self.frame.origin.x - 5),
        //                                           (self.frame.origin.y - 10),
        //                                           (self.frame.size.width + 5),
        //                                           (self.frame.size.height + 10));
        if (CGRectContainsPoint(self.frame, point)){
            BOOL changed = YES;
            if (self.delegate && [self.delegate respondsToSelector:@selector(checkBox:willChangeSelectedStatus:)]) {
                changed = [self.delegate checkBox:self willChangeSelectedStatus:!self.selected];
            }
            
            if (changed) {
                self.selected = !self.selected;
                if (self.delegate && [self.delegate respondsToSelector:@selector(checkBox:didChangeSelectedStatus:)]) {
                    [self.delegate checkBox:self didChangeSelectedStatus:self.selected];
                }
            }
        }
    }
    
    [super touchesEnded:touches withEvent:event];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
