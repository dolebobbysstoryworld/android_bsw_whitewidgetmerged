//
//  TSTextField.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 30..
//
//

#import "TSTextField.h"
#import "UIImage+PathExtention.h"
#import "UIColor+RGB.h"
#import "ResolutionManager.h"

@interface TSTextField () < UITextFieldDelegate >

@property (nonatomic, strong) UIImageView *backgroundView;
@property (nonatomic, strong) UIImageView *iconView;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) NSString *placeholder;
@property (nonatomic, strong) UIFont *placeholderFont;
@property (nonatomic, strong) UIFont *textFont;
@property (nonatomic, strong) UIColor *placeholderColor;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) UILabel *commentLabel;
@property (nonatomic, strong) UIButton *errorButton;
@property (nonatomic, strong) UILabel *textLabel;

@end

#define TSTEXTFIELD_FRAME             CGRectMake(0, 0, 255, 36)

@implementation TSTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame icon:(NSString*)name placeholder:(NSString*)string {
    return [self initWithFrame:frame icon:name placeholder:string editable:YES];
}

- (id)initWithFrame:(CGRect)frame icon:(NSString*)name placeholder:(NSString*)string isPopup:(BOOL)popup {
    return [self initWithFrame:frame icon:name placeholder:string editable:YES isPopup:popup];
}

- (id)initWithFrame:(CGRect)frame icon:(NSString*)name placeholder:(NSString*)string editable:(BOOL)editable {
    return [self initWithFrame:frame icon:name placeholder:string editable:editable isPopup:NO];
}

- (id)initWithFrame:(CGRect)frame icon:(NSString*)name placeholder:(NSString*)string editable:(BOOL)editable isPopup:(BOOL)popup {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        NSString *backgroundImageName = @"input.png";
        if (popup) {
            backgroundImageName = @"popup_input.png";
        }
        self.backgroundView =
        [[UIImageView alloc] initWithImage:[[UIImage imageNamedEx:backgroundImageName ignore:YES] resizableImageWithCapInsets:UIEdgeInsetsMake(17, 17, 17, 17)]];
        self.backgroundView.frame = TSTEXTFIELD_FRAME;
        [self addSubview:self.backgroundView];
        
        self.iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:name ignore:YES]];
        self.iconView.frame = CGRectMake(10, 5, 26, 26);
        [self addSubview:self.iconView];
        
        self.placeholderColor = RGBAH(@"#9fa0a3", 0.6);
        self.placeholderFont = [UIFont fontWithName:kDefaultFontName size:15];
        self.textColor = RGBH(@"#606060");
        self.textFont = [UIFont fontWithName:kDefaultBoldFontName size:15];

        self.placeholder = string;
        self.editable = editable;
        self.enabled = YES;
    }
    return self;
}

- (NSAttributedString*)attributedPlaceHolder {
    NSString *mandatoryPlaceHolder = [NSString stringWithFormat:@"%@ *", self.placeholder];
    NSMutableAttributedString *attrPlaceHolder =
    [[NSMutableAttributedString alloc] initWithString:mandatoryPlaceHolder];
    [attrPlaceHolder addAttribute:NSFontAttributeName value:self.placeholderFont range:NSMakeRange(0, [mandatoryPlaceHolder length])];
    [attrPlaceHolder addAttribute:NSForegroundColorAttributeName value:self.placeholderColor range:NSMakeRange(0, [mandatoryPlaceHolder length])];
    [attrPlaceHolder addAttribute:NSForegroundColorAttributeName value:RGBH(@"ff3311") range:NSMakeRange([mandatoryPlaceHolder length]-1, 1)];
    return attrPlaceHolder;
}

- (void)setMandatory:(BOOL)mandatory {
    _mandatory = mandatory;
    if ([self.text length] <= 0) {
        if (self.mandatory) {
            if (self.editable) {
                self.textField.placeholder = nil;
                self.textField.attributedPlaceholder = [self attributedPlaceHolder];
            } else {
                self.textLabel.text = nil;
                self.textLabel.attributedText = [self attributedPlaceHolder];
            }
        } else {
            if (self.editable) {
                self.textField.attributedPlaceholder = nil;
                self.textField.placeholder = self.placeholder;
            } else {
                self.textLabel.attributedText = nil;
                self.textLabel.text = self.placeholder;
                self.textLabel.font = self.placeholderFont;
                self.textLabel.textColor = self.placeholderColor;
            }
        }
        if (self.commentLabel) {
            NSString *commentString = self.commentLabel.text;
            [self.commentLabel removeFromSuperview];
            self.commentLabel = nil;;
            [self setComment:commentString];
        }
    }
    
}
- (void)setComment:(NSString *)string {
    NSInteger leftMargin = 42;
    NSInteger middleMargin = 12;
    NSInteger rightMargin = 13;
    NSInteger placeholderWidth;
    if (self.mandatory) {
        placeholderWidth = [[NSString stringWithFormat:@"%@ *", self.placeholder] sizeWithAttributes:@{ NSFontAttributeName: self.placeholderFont }].width;
    } else {
        placeholderWidth = [self.placeholder sizeWithAttributes:@{ NSFontAttributeName: self.placeholderFont }].width;
    }
    CGFloat x = leftMargin + placeholderWidth + middleMargin;
    NSInteger labelWidth = 255 - ( x + rightMargin );
    if (labelWidth >= 56) {
        self.commentLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, 0, labelWidth, 36)];
//        self.commentLabel.backgroundColor = [UIColor lightGrayColor];
        self.commentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.commentLabel.text = string;
        self.commentLabel.font = [UIFont fontWithName:kDefaultFontName size:11];
        self.commentLabel.textColor = RGBH(@"#c2c2c2");
        self.commentLabel.numberOfLines = 0;
        [self addSubview:self.commentLabel];
    }
}

- (void)setError:(NSString*)string {
    if (string) {
        self.backgroundView.hidden = YES;
        self.iconView.hidden = YES;
        self.textField.hidden = YES;
        if (!self.errorButton) {
            self.errorButton = [UIButton buttonWithType:UIButtonTypeCustom];
            self.errorButton.frame = TSTEXTFIELD_FRAME;
            [self.errorButton setBackgroundImage:[[UIImage imageNamedEx:@"input_error.png" ignore:YES] resizableImageWithCapInsets:UIEdgeInsetsMake(17, 17, 17, 17)]
                                        forState:UIControlStateNormal];
            
            [self.errorButton setTitle:string forState:UIControlStateNormal];
            CGSize boundingRectSize = CGSizeMake(TSTEXTFIELD_FRAME.size.width, CGFLOAT_MAX);
            NSDictionary *attributes = @{NSFontAttributeName : [UIFont fontWithName:kDefaultFontName size:13]};
            CGRect labelSize = [string boundingRectWithSize:boundingRectSize
                                                    options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                                      attributes:attributes context:nil];
            if (labelSize.size.height > TSTEXTFIELD_FRAME.size.height) {
                NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
                if ([language isEqualToString:@"ko"]) {
                    self.errorButton.titleLabel.font = [UIFont fontWithName:kDefaultFontName size:11];
                } else if ([language isEqualToString:@"ja"]) {
                    self.errorButton.titleLabel.font = [UIFont fontWithName:kDefaultFontName size:11];
                } else {
                    self.errorButton.titleLabel.font = [UIFont fontWithName:kDefaultFontName size:13];
                }
            } else {
                self.errorButton.titleLabel.font = [UIFont fontWithName:kDefaultFontName size:13];
            }
            [self.errorButton setTitleColor:RGBH(@"#fec346") forState:UIControlStateNormal];
            self.errorButton.titleLabel.numberOfLines = 0;
            self.errorButton.titleLabel.textAlignment = NSTextAlignmentCenter;
            [self.errorButton addTarget:self action:@selector(selectorErrorButton:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:self.errorButton];
        } else {
            [self.errorButton setTitle:string forState:UIControlStateNormal];
        }
    } else {
        self.backgroundView.hidden = NO;
        self.iconView.hidden = NO;
        self.textField.hidden = NO;
        if (self.errorButton) {
            [self.errorButton removeFromSuperview];
            self.errorButton = nil;
        }
    }
}


- (void)setText:(NSString *)text {
    _text = text;
    if (self.editable) {
        self.textField.textColor = self.textColor;
        self.textField.font = self.textFont;
        self.textField.text = self.text;
    } else {
        if ([self.text length] > 0) {
            self.textLabel.attributedText = nil;
            self.textLabel.text = self.text;
            self.textLabel.font = self.textFont;
            self.textLabel.textColor = self.textColor;
        } else {
            if (self.mandatory) {
                self.textLabel.text = nil;
                self.textLabel.attributedText = [self attributedPlaceHolder];
            } else {
                self.textLabel.attributedText = nil;
                self.textLabel.text = self.placeholder;
                self.textLabel.font = self.placeholderFont;
                self.textLabel.textColor = self.placeholderColor;
            }
        }
    }
}

- (void)setKeyboardType:(UIKeyboardType)type {
    [self.textField setKeyboardType:type];
}

- (void)setPasswordType:(BOOL)secure {
    self.textField.secureTextEntry = secure;
}

- (void)setEditable:(BOOL)editable {
    _editable = editable;
    
    if (self.editable) {
        if (!self.textField) {
            self.textField = [[UITextField alloc] initWithFrame:CGRectMake(46, 0, 196, 36)];
//            self.textField.backgroundColor = [UIColor lightGrayColor];
            if ([self.text length] > 0) {
                self.textField.font = self.textFont;
                self.textField.textColor = self.textColor;
                self.textField.text = self.text;
            } else {
                self.textField.font = self.placeholderFont;
                self.textField.textColor = self.placeholderColor;
                if (self.mandatory) {
                    self.textField.placeholder = nil;
                    self.textField.attributedPlaceholder = [self attributedPlaceHolder];
                } else {
                    self.textField.attributedPlaceholder = nil;
                    self.textField.placeholder = self.placeholder;
                }
            }
            
            self.textField.delegate = self;
            self.textField.returnKeyType = UIReturnKeyDone;
            self.textField.autocorrectionType = UITextAutocorrectionTypeNo;
            self.textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            self.textField.spellCheckingType = UITextSpellCheckingTypeNo;
            [self addSubview:self.textField];
        }
        if (self.textLabel) {
            [self.textLabel removeFromSuperview];
            self.textLabel = nil;
        }
    } else {
        if (!self.textLabel) {
            self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake(46, 0, 196, 36)];
            if ([self.text length] > 0) {
                self.textLabel.attributedText = nil;
                self.textLabel.font = self.textFont;
                self.textLabel.textColor = self.textColor;
                self.textLabel.text = self.text;
            } else {
                if (self.mandatory) {
                    self.textLabel.text = nil;
                    self.textLabel.attributedText = [self attributedPlaceHolder];
                } else {
                    self.textLabel.attributedText = nil;
                    self.textLabel.font = self.placeholderFont;
                    self.textLabel.textColor = self.placeholderColor;
                    self.textLabel.text = self.placeholder;
                }
            }
            [self addSubview:self.textLabel];
        }
        if (self.textField) {
            [self.textField removeFromSuperview];
            self.textField = nil;
        }
    }
}

- (void)setEnabled:(BOOL)enabled {
    _enabled = enabled;
    if (self.editable == YES) {
        self.textField.enabled = self.enabled;
    }
}

- (void)setShowActivity:(BOOL)showActivity {
    _showActivity = showActivity;
    
    UIActivityIndicatorView *indicatorView = nil;
    if (self.showActivity) {
        indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        CGRect frame = indicatorView.frame;
        frame.origin = CGPointMake(self.frame.size.width - indicatorView.frame.size.width*2, (self.frame.size.height - indicatorView.frame.size.height)/2);
        indicatorView.tag = 999888;
        indicatorView.frame = frame;
        [self addSubview:indicatorView];
        [indicatorView startAnimating];
    } else {
        indicatorView = (UIActivityIndicatorView*)[self viewWithTag:999888];
        if (indicatorView) {
            [indicatorView stopAnimating];
            [indicatorView removeFromSuperview];
        }
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (BOOL)checkEmailStringValidationWithShowError:(NSString*)error {
    if ([self.textField.text length] == 0) {
        return NO;
    }
    
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL result = [emailTest evaluateWithObject:self.textField.text];
    if (!result && error) {
        [self setError:error];
    } else {
        [self setError:nil];
    }
    return result;
}

- (BOOL)checkPasswordStringValidationWithShowError:(NSString*)error {
    NSInteger length = [self.textField.text length];
    if (length == 0) {
        return NO;
    }
    
    if (length < 6 && ([error length] > 0)) {
        [self setError:error];
        return NO;
    } else {
        [self setError:nil];
    }
    return YES;
}

- (BOOL)equalPasswordString:(NSString*)target withShowError:(NSString*)error {
    NSInteger length = [self.textField.text length];
    if (length == 0) {
        return NO;
    }
    
    if ([target isEqualToString:self.textField.text] == NO) {
        [self setError:error];
        return NO;
    } else {
        [self setError:nil];
    }
    return YES;
}

#pragma mark - control selector
- (IBAction)selectorErrorButton:(id)sender {
    self.backgroundView.hidden = NO;
    self.iconView.hidden = NO;
    self.textField.hidden = NO;
    [self.errorButton removeFromSuperview];
    self.errorButton = nil;
    
    [self.textField becomeFirstResponder];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {// return NO to disallow editing.
    if(self.commentLabel && self.commentLabel.superview != nil) {
        [self.commentLabel removeFromSuperview];
    }
    NSInteger length = [textField.text length];
//    NSLog(@"text length : %lu", (unsigned long)length);
    if (length == 0) {
        self.textField.textColor = self.textColor;
    }
    
    return self.editable;
}

//- (void)textFieldDidBeginEditing:(UITextField *)textField {// became first responder
//    
//}
//
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if([textField.text length] > 0) {
        if(self.commentLabel && self.commentLabel.superview != nil) {
            [self.commentLabel removeFromSuperview];
        }
    } else {
        if (self.commentLabel) {
            [self addSubview:self.commentLabel];
        }
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    _text = self.textField.text;
    if (self.delegate && [self.delegate respondsToSelector:@selector(tstextFieldDidEndEditing:)]) {
        [self.delegate tstextFieldDidEndEditing:self];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.enabled == NO) {
        return;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(tstextFieldTouched:)]) {
        [self.delegate tstextFieldTouched:self];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    _text = [textField.text stringByReplacingCharactersInRange:range withString:string];
//    NSLog(@"shouldChangeCharactersInRange : range = %@, string = %@ self.text = %@", NSStringFromRange(range), string, self.text);
    if (self.delegate && [self.delegate respondsToSelector:@selector(tstextField:shouldChangeCharactersInRange:replacementString:)]) {
        return [self.delegate tstextField:self shouldChangeCharactersInRange:range replacementString:string];
    }
    return YES;
}

@end


@implementation TSPTextField

- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 10.0f, 10.0f);//14.0f, 23.0f
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [self textRectForBounds:bounds];
}


@end
