//
//  UIImage+PathExtention.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 29..
//
//

#import "UIImage+PathExtention.h"

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

@implementation UIImage (PathExtention)

+ (UIImage *)imageNamedEx:(NSString *)name {
    return [UIImage imageNamedEx:name ignore:NO];
}

//#define _IMAGE_CACHE_

//폴더로 관리
+ (UIImage *)imageNamedEx:(NSString *)name ignore:(BOOL)ignore {
    NSString *fileName = [name stringByDeletingPathExtension];
    NSString *formatString;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        formatString = @"iosnative/pad/%@";
    } else {
        if (ignore) {
            formatString = @"iosnative/phone/%@";
        } else {
            CGFloat height;
            CGRect screenBounds = [[UIScreen mainScreen] bounds];

#ifdef __IPHONE_8_0
//            if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
//                height = screenBounds.size.height;
//            } else {
//                height = screenBounds.size.width;
//            }
            if (screenBounds.size.height > screenBounds.size.width) {
                height = screenBounds.size.height;
            } else {
                height = screenBounds.size.width;
            }
#else
            height = screenBounds.size.height;
#endif
            if (height > 480) {
                formatString = @"iosnative/phone/%@";
            } else {
                formatString = @"iosnative/phone35/%@";
            }
        }
    }

#ifdef _IMAGE_CACHE_
    NSString *fileExtention = [name pathExtension];
    NSString *formatedImagePath = [NSString stringWithFormat:formatString, fileName];
    NSString *finalPath = [formatedImagePath stringByAppendingPathExtension:fileExtention];
    UIImage *newImage = [UIImage imageNamed:finalPath];
#else
    NSString *finalPath = [NSString stringWithFormat:formatString, fileName];
    NSString *path = [[NSBundle mainBundle] pathForResource:finalPath ofType:@"png"];
    UIImage *newImage = [UIImage imageWithContentsOfFile:path];
#endif
    NSAssert(newImage, @"not create image: %@", finalPath);
    return newImage;
}

//파일명으로 관리
/* + (UIImage *)imageNamedEx:(NSString *)name ignore:(BOOL)ignore {
    NSString *fileName = [name stringByDeletingPathExtension];
    NSString *fileExtention = [name pathExtension];
    NSString *formatString;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        formatString = @"iosnative/%@_pad.%@";
    } else {
        if (ignore) {
            formatString = @"iosnative/%@.%@";
        } else {
            CGRect screenBounds = [[UIScreen mainScreen] bounds];
            if (screenBounds.size.height > 480) {
                formatString = @"iosnative/%@.%@";
            } else {
                formatString = @"iosnative/%@_960.%@";
            }
        }
    }
    NSString *finalPath = [NSString stringWithFormat:formatString, fileName, fileExtention];
    UIImage *newImage = [UIImage imageNamed:finalPath];
//    NSString *test = [finalPath stringByDeletingPathExtension];
//    NSString *path = [[NSBundle mainBundle] pathForResource:test ofType:@"png"];
//    UIImage *newImage = [[UIImage alloc] initWithContentsOfFile:path];
    NSAssert(newImage, @"not create image: %@", finalPath);
    return newImage;
}*/

@end
