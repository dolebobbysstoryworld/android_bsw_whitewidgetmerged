//
//  TSOptionalAccountInputView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 11. 21..
//
//

#import "TSOptionalAccountInputView.h"
#import "ResolutionManager.h"
#import "TSTextField.h"
#import "UIImage+PathExtention.h"
#import "UIColor+RGB.h"
#import "TSRadioButton.h"
#import "TSPickerView.h"
#import "TSDolesNet.h"
#import "TSMessageView.h"
#import "TSToastView.h"

@interface TSOptionalAccountInputView () < TSTextFieldDelegate, TSRadioButtonDelegate, TSPopupViewDelegate, TSNetDelegate >

@property (nonatomic, strong) UIView *separatorView;

@property (nonatomic, strong) UIImageView *optionIcon;
@property (nonatomic, strong) UILabel *optionTitle;

@property (nonatomic, strong) TSTextField *birthDayTextFiled;
@property (nonatomic, strong) NSArray *birthYears;
@property (nonatomic, assign) NSInteger birthDayIndex;

@property (nonatomic, strong) TSTextField *cityTextFiled;
@property (nonatomic, strong) NSArray *countrys;
@property (nonatomic, strong) NSArray *countryCodes;
@property (nonatomic, assign) NSInteger countryIndex;
@property (nonatomic, assign) NSInteger cityIndex;
@property (nonatomic, strong) TSNet *net;

@property (nonatomic, strong) TSRadioButton *maleRadioButton;
@property (nonatomic, strong) TSRadioButton *femaleRadioButton;

@end

#define LAYOUT_MAINVIEW_WIDTH           508/2
#define LAYOUT_OPTIONICON_WIDTH         7

#define TAG_BIRTHDAY_TEXTFIELD          700001
#define TAG_CITY_TEXTFIELD              700002
#define TAG_MALE_RADIOBUTTON            700003
#define TAG_FEMALE_RADIOBUTTON          700004
#define TAG_BIRTHDAY_POPUP              700005
#define TAG_COUNTRY_POPUP               700006
#define TAG_CITY_POPUP                  700007
#define TAG_NET_PROVINCE                700008

@implementation TSOptionalAccountInputView

- (instancetype)initWithFrame:(CGRect)frame Separator:(BOOL)separator {
    self = [super initWithFrame:frame];
    if (self) {
//        self.backgroundColor = [UIColor whiteColor];
        CGFloat currentY = 0;
        if (separator) {
            self.separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, LAYOUT_MAINVIEW_WIDTH, 1)];
            self.separatorView.backgroundColor = RGBH(@"#b69973");
            [self addSubview:self.separatorView];
            currentY = currentY + 1 + deviceValueA(42, 42, 18)/2;//add separator view height & margin
        }
        
        self.optionIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"optional_icon.png" ignore:YES]];
        self.optionIcon.frame = CGRectMake(4/2, currentY+((34-LAYOUT_OPTIONICON_WIDTH)/2)/2,
                                           LAYOUT_OPTIONICON_WIDTH, LAYOUT_OPTIONICON_WIDTH);
        [self addSubview:self.optionIcon];
        
        self.optionTitle = [[UILabel alloc] initWithFrame:CGRectMake(4/2+(14+14)/2, currentY, 476/2, 34/2)];
        self.optionTitle.font = [UIFont fontWithName:kDefaultFontName size:30/2];
        self.optionTitle.textAlignment = NSTextAlignmentLeft;
        self.optionTitle.textColor = RGBH(@"#4f2c00");
        self.optionTitle.numberOfLines = 0;//tuilise_todo : 두줄 되게 하려고 2px 늘렸다.
        self.optionTitle.text = NSLocalizedString(@"PF_ACCOUNT_OPTION_TITLE", @"Optional");
        [self addSubview:self.optionTitle];
        
        currentY = currentY + 34/2 + 20/2;//add title label height & margin
        
        self.birthDayTextFiled =
        [[TSTextField alloc] initWithFrame:CGRectMake(0, currentY, LAYOUT_MAINVIEW_WIDTH, 70/2)
                                      icon:@"input_birthday_icon.png"
                               placeholder:NSLocalizedString(@"PF_SIGNUP_TEXTFIELD_BIRTHYEAR", @"Birthday") editable:NO];
        self.birthDayTextFiled.tag = TAG_BIRTHDAY_TEXTFIELD;
        self.birthDayTextFiled.delegate = self;
        [self addSubview:self.birthDayTextFiled];
        self.birthYears = [TSPickerView birthYearsData];
        self.birthDayIndex = [self.birthYears count] -1;

        currentY = currentY + 70/2 + 22/2;//add birthday textfield height & margin
        
        self.cityTextFiled =
        [[TSTextField alloc] initWithFrame:CGRectMake(0, currentY, LAYOUT_MAINVIEW_WIDTH, 70/2)
                                      icon:@"input_city_icon.png"
                               placeholder:NSLocalizedString(@"PF_ACCOUNT_PLACEHOLDER_COUNTRY", @"Country") editable:NO];
        self.cityTextFiled.tag = TAG_CITY_TEXTFIELD;
        self.cityTextFiled.delegate = self;
        [self addSubview:self.cityTextFiled];
        self.countrys = [TSPickerView countrys];
        self.countryCodes = [TSPickerView countryCodes];
        self.countryIndex = -1;
        self.cityIndex = -1;
        
        currentY = currentY + 70/2 + deviceValueA(32, 34, 22)/2;//add city textfield height & margin
        
        self.maleRadioButton = [TSRadioButton radioButtonWithTitle:NSLocalizedString(@"PF_SIGNUP_RADIOTITLE_MALE", @"Male")
                                                             Frame:CGRectMake(0, currentY, 58/2, 60/2)];
        self.maleRadioButton.tag = TAG_MALE_RADIOBUTTON;
//        self.maleRadioButton.selected = YES;
        self.maleRadioButton.delegate = self;
        
        [self addSubview:self.maleRadioButton];
        self.femaleRadioButton = [TSRadioButton radioButtonWithTitle:NSLocalizedString(@"PF_SIGNUP_RADIOTITLE_FEMALE", @"Female")
                                                               Frame:CGRectMake((58+18+168+20)/2, currentY, 58/2, 60/2)];
        self.femaleRadioButton.tag = TAG_FEMALE_RADIOBUTTON;
        self.femaleRadioButton.delegate = self;
        [self addSubview:self.femaleRadioButton];
        
        CGFloat viewHeight = currentY + 60/2;
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, viewHeight);
    }
    return self;
}

- (void)tstextFieldTouched:(TSTextField *)textField {
    TSPickerView *pickView = nil;
    textField.enabled = NO;
    switch (textField.tag) {
        case TAG_BIRTHDAY_TEXTFIELD: {
            pickView = [[TSPickerView alloc] initWithDataSource:self.birthYears
                                                          title:NSLocalizedString(@"PF_SIGNUP_POPUP_TITLE_BIRTHYEAR", @"Set birth year")];
            pickView.tag = TAG_BIRTHDAY_POPUP;
            pickView.selectedIndex = self.birthDayIndex;
        } break;
        case TAG_CITY_TEXTFIELD: {
            pickView = [[TSPickerView alloc] initWithDataSource:self.countrys
                                                          title:NSLocalizedString(@"PF_SIGNUP_PUPUP_TITLE_SELECT_COUNTRY", @"Enter the country")];
            pickView.tag = TAG_COUNTRY_POPUP;
            pickView.selectedIndex = self.countryIndex>=0?self.countryIndex:0;
//            self.isCitySelected = NO;
        } break;
            
        default:
            break;
    }
    /* else {//국가는 선택 되었다
          pickView = [[TSPickerView alloc] initWithDataSource:((TSNetProvince*)self.net).provinces
          title:NSLocalizedString(@"PF_SIGNUP_PUPUP_TITLE_SELECT_CITY", @"Enter the city")];
          pickView.tag = TAG_CITY_POPUP;
          pickView.selectedIndex = self.cityIndex;
          }*/
    pickView.buttonLayout = TSPopupButtonLayoutCancelSet;
    pickView.delegate = self;
    [pickView showInView:self.superview];
}

- (void)popview:(TSPopupView *)view didClickedButton:(TSPopupButtonType)type {
    if (type == TSPopupButtonTypeSet) {
        switch (view.tag) {
            case TAG_COUNTRY_POPUP: {
                self.countryIndex = ((TSPickerView*)view).selectedIndex;
                self.cityTextFiled.text = [self.countrys objectAtIndex:self.countryIndex];
                NSString *countryCode = [self.countryCodes objectAtIndex:self.countryIndex];
                if ([countryCode isEqualToString:@"Other"] == NO) {//display가 아니다....
                    self.cityIndex = 0;
                    self.net = [[TSNetProvince alloc] initWithCountryCode:countryCode];
                    self.net.delegate = self;
                    self.net.tag = TAG_NET_PROVINCE;
                    [self.net start];
                    return;
                }
                self.cityTextFiled.enabled = YES;
//                self.isCitySelected = YES;
            } break;
            case TAG_CITY_POPUP: {
                self.cityIndex = ((TSPickerView*)view).selectedIndex;
                self.cityTextFiled.text = [((TSNetProvince*)self.net).provinces objectAtIndex:self.cityIndex];
//                self.isCitySelected = YES;
                self.cityTextFiled.enabled = YES;
            } break;
            case TAG_BIRTHDAY_POPUP: {
                self.birthDayIndex = ((TSPickerView*)view).selectedIndex;
                self.birthDayTextFiled.text = [self.birthYears objectAtIndex:self.birthDayIndex];
                self.birthDayTextFiled.enabled = YES;
            } break;
            default:
                break;
        }
    } else {
        switch (view.tag) {
            case TAG_COUNTRY_POPUP: {
                if ([self.cityTextFiled.text length] <= 0) {
//                    self.isCitySelected = NO;
                    self.cityTextFiled.enabled = YES;
                    self.cityTextFiled.text = @"";
                    self.countryIndex = -1;
                    self.cityIndex = -1;
                }
            } break;
            case TAG_CITY_POPUP: {
//                self.isCitySelected = NO;
                self.cityTextFiled.enabled = YES;
                self.cityTextFiled.text = @"";
                self.countryIndex = -1;
                self.cityIndex = -1;
            } break;
            case TAG_BIRTHDAY_POPUP: {
                self.birthDayTextFiled.enabled = YES;
                self.birthDayTextFiled.text = @"";
                self.birthDayIndex = [self.birthYears count] -1;
            } break;
            default:
                break;
        }
    }
}

#pragma mark - TSNetDelegate
- (void)net:(TSNet*)netObject didStartProcess:(TSNetResultType)result {
    switch (netObject.tag) {
        case TAG_NET_PROVINCE: {
            self.cityTextFiled.showActivity = YES;
            self.superview.userInteractionEnabled = NO;
        } break;
        default: { } break;
    }
}

- (void)net:(TSNet*)netObject didEndProcess:(TSNetResultType)result {
    switch (netObject.tag) {
        case TAG_NET_PROVINCE: {
            self.cityTextFiled.showActivity = NO;
            self.superview.userInteractionEnabled = YES;
            if (!result) {
                TSPickerView *pickView = [[TSPickerView alloc] initWithDataSource:((TSNetProvince*)self.net).provinces
                                                                            title:NSLocalizedString(@"PF_SIGNUP_PUPUP_TITLE_SELECT_CITY", @"Enter the city")];
                pickView.tag = TAG_CITY_POPUP;
                pickView.selectedIndex = self.cityIndex;
                pickView.buttonLayout = TSPopupButtonLayoutCancelSet;
                pickView.delegate = self;
                [pickView showInView:self.superview];
            } else {//tuilise_todo : 오류 결과다. 맞는 처리 필요
                self.cityTextFiled.text = nil;
                TSMessageView *messageView =
                [[TSMessageView alloc] initWithMessage:[NSString stringWithFormat:@"Server Error\n%@.", [netObject serverErrorString]]
                                                  icon:TSPopupIconTypeWarning];
                messageView.buttonLayout = TSPopupButtonLayoutOK;
                [messageView showInView:self.superview];
            }
        } break;
        default: { } break;
    }
}

- (void)net:(TSNet*)netObject didFailWithError:(TSNetResultType)result {
    switch (netObject.tag) {
        case TAG_NET_PROVINCE: {
            self.cityTextFiled.showActivity = NO;
            self.superview.userInteractionEnabled = YES;
            switch (result) {
                case TSNetErrorNetworkDisable:
                case TSNetErrorTimeout: {
                    TSToastView *toast = [[TSToastView alloc] initWithString:NSLocalizedString(@"CM_NET_WARNING_NETWORK_UNSTABLE", @"The network is unstable.\nPlease try again")];
                    [toast showInViewWithAnimation:self.superview];
                } break;
                default: {
                } break;
            }
        } break;
        default: { } break;
    }
}

- (void)radioButton:(TSRadioButton *)radioButton didChangeSelectedStatus:(BOOL)selected {
    switch (radioButton.tag) {
        case TAG_MALE_RADIOBUTTON: {
            if (selected) {
                self.femaleRadioButton.selected = NO;
            }
        } break;
        case TAG_FEMALE_RADIOBUTTON: {
            if (selected) {
                self.maleRadioButton.selected = NO;
            }
        } break;
        default: { } break;
    }
}

- (NSString*)birthday {
    NSLog(@"birthday string : %@", self.birthDayTextFiled.text);
    if ([self.birthDayTextFiled.text length] <= 0) {
        return @"9999";
    }
    return self.birthDayTextFiled.text;
}

- (void)setBirthday:(NSString *)birthday {
    self.birthDayTextFiled.text = birthday;
    self.birthDayIndex = [birthday integerValue] - [[self.birthYears objectAtIndex:0] integerValue];
    NSLog(@"birthDay : %@ / index : %ld", self.birthDayTextFiled.text, (long)self.birthDayIndex);
}

- (NSString*)countryCode {
    if (self.countryIndex == -1) {
        return [self.countryCodes objectAtIndex:[self.countryCodes count]-1];
    }
    return [self.countryCodes objectAtIndex:self.countryIndex];
}

- (void)setCountryCode:(NSString *)countryCode {
    NSString *code;
    NSInteger index = 0;
    for (code in self.countryCodes) {
        if ([countryCode isEqualToString:code]) {
            self.countryIndex = index;
            return;
        }
        index++;
    }
}

- (NSString*)city {
    if (self.cityIndex == -1) {
        return [self.countrys objectAtIndex:[self.countrys count]-1];
    }
    return self.cityTextFiled.text;
}

- (void)setCity:(NSString *)city {
    self.cityTextFiled.text = city;
    if ([city length]) {
        self.cityIndex = 0;
    }
}

- (NSInteger)gender {
    if (self.maleRadioButton.selected == YES) {
        return 1;
    }
    
    if (self.femaleRadioButton.selected == YES) {
        return 2;
    }
    
    return 99;
}

- (void)setGender:(NSInteger)gender {
    switch (gender) {
        case 1:
            self.maleRadioButton.selected = YES;
            self.femaleRadioButton.selected = NO;
            break;
        case 2:
            self.maleRadioButton.selected = NO;
            self.femaleRadioButton.selected = YES;
            break;
        default:
            self.maleRadioButton.selected = NO;
            self.femaleRadioButton.selected = NO;
            break;
    }
}

- (BOOL)isEdited {
    if ([self.birthDayTextFiled.text length] > 0) {
        return YES;
    }
    
    if ([self.cityTextFiled.text length] > 0) {
        return YES;
    }

    if (self.maleRadioButton.selected == YES || self.femaleRadioButton.selected == YES) {
        return YES;
    }

    return NO;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
