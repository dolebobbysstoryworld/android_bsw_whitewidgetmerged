//
//  TSDoleCoinView.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 2..
//
//

#import <UIKit/UIKit.h>
#import "TSAccountDelegateProtocol.h"

@interface TSDoleCoinView : UIView

@property (nonatomic, assign) id<TSAccountDelegate> delegate;
- (void)viewDidAppeared;

@end
