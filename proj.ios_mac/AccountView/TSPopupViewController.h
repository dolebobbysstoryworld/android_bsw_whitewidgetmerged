//
//  TSPopupViewController.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 23..
//
//

#import <UIKit/UIKit.h>
#import "TSPopupView.h"
#import "TSShareCommentView.h"

@interface TSPopupViewController : UIViewController

- (id)initWithPopupMode:(TSPopupType)type userData:(id)object;

@end
