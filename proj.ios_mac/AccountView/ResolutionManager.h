//
//  ResolutionManager.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 29..
//
//

#import <Foundation/Foundation.h>
#include "Define.h"

typedef NS_ENUM(NSInteger, TSDevice) {
    TSDevicePad = 0,
    TSDevicePhone,
    TSDevicePhoneSmall,
};

typedef NS_ENUM(NSInteger, TSOriginLayer) {
    TSOriginLayerNone = 0,
    TSOriginLayerHome,
    TSOriginLayerBookMap,
    TSOriginLayerCharacterRecording,
};

typedef NS_ENUM(NSInteger, TSViewIndex) {
    TSViewIndexShare = 0,
    TSViewIndexLogin,
    TSViewIndexSignin,
    TSViewIndexAgreement,
    TSViewIndexCoin,
    TSViewIndexFacebook,
    TSViewIndexSettingMain,
    TSViewIndexModify,
    TSViewIndexHelp,
    TSViewIndexDeleteAccount,
    TSViewIndexModifySNS,
    TSViewIndexSignupSNS,
    TSViewIndexAbout,
    TSViewIndexGuide,
    TSViewIndexEnd,
    TSViewIndexUsage
};

typedef NS_ENUM(NSInteger, TSShareIndex) {
    TSShareViewControllerRect = 0,
    TSShareSuperViewControllerRect,
    TSShareCloseButtonRect,
    TSShareCloseButtonRectInView,
    TSShareSubViewRect,
    TSShareSubViewOrgRect,
    TSShareSeaRect,
    TSShareLogoRect,
};

typedef NS_ENUM(NSInteger, TSLoginIndex) {
    TSLoginIDRect = 0,
    TSLoginPWRect,
    TSLoginButtonRect,
    TSLoginFgPWRect,
    TSLoginSigninRect,
    TSLoginFaceBookRect,
    TSLoginForgetLabelRect,
    TSLoginSkipLabelRect,
    TSLoginSKipButtonRect,
};

typedef NS_ENUM(NSInteger, TSSigninIndex) {
    TSSigninFactorRect = 0,
    TSSigninEmailRect,
    TSSigninPasswordRect,
    TSSigninCPasswordRect,
    TSSigninBirthRect,
    TSSigninCityRect,
    TSSigninMailRadioRect,
    TSSigninFemailRadioRect,
    TSSigninAgreeCheckRect,
    TSSigninAgreeLabelRect,
    TSSigninSignUpBtnRect,
};

typedef NS_ENUM(NSInteger, TSAgreeIndex) {
    TSAgreeTermsRect = 0,
    TSAgreePrivacyRect,
    TSAgreeTermsCheckRect,
    TSAgreeTermsLabelRect,
    TSAgreePrivacyCheckRect,
    TSAgreePrivacyTwoLineLabelRect,
    TSAgreePrivacyLabelRect,
    TSAgreeCancelBtnRect,
    TSAgreeNextBtnRect,
    TSAgreeTermsForAboutRect,
    TSAgreePrivacyForAboutRect,
    TSAgreeTermsRectAus,//hans added
    TSAgreePrivacyRectAus,//hans added
    TSAgreeFirstCheckRectAus,//hans added----13---/
    TSAgreeFirstLabelRectAus,//hans added
    TSAgreeSecondCheckRectAus,//hans added----15---/
    TSAgreeSecondLabelRectAus,//hans added
    TSAgreeThirdCheckRectAus,//hans added----17----/
    TSAgreeThirdLabelRectAus,//hans added
    TSAgreeFourthCheckRectAus,//hans added----19---/
    TSAgreeFourthLabelRectAus,//hans added
};

typedef NS_ENUM(NSInteger, TSDoleCoinIndex) {
    TSDCoinLogoRect = 0,
    TSDCoinCoinRect,
    TSDCoinCountRect,
    TSDCoinLabelRect,
    TSDCoinQuestionBGRect,
    TSDCoinQ1LableRect,
    TSDCoinA1LableRect,
    TSDCoinQ2LableRect,
    TSDCoinA2LableRect,
    TSDCoinQRRect,
//    TSDCoinFBRect,
};

typedef NS_ENUM(NSInteger, TSFacebookIndex) {
    TSFacebookStart = 0,
};

typedef NS_ENUM(NSInteger, TSSetMainIndex) {
    TSSetMainBGSeaRect = 0,
    TSSetMainCommentLabelRect,
    TSSetMainEditButtonRect,
    TSSetMainUsageButtonRect,
    TSSetMainBGMusicImageRect,
    TSSetMainBGMusicLabelRect,
    TSSetMainBGMusicSwitchRect,
//    TSSetMainFacebookRect,
    TSSetMainAboutRect,
    TSSetMainHelpRect,
    TSSetMainDeleteRect,
};

typedef NS_ENUM(NSInteger, TSModifyAccountIndex) {
    TSModifyIDLabelRect = 0,
    TSModifyCurrentPWRect,
    TSModifyChangePWRect,
    TSModifyConfirmPWRect,
    TSModifyBirthYearRect,
    TSModifyCityRect,
    TSModifyMaleRadioRect,
    TSModifyFemaleRadioRect,
    TSModifySaveButtonRect,
};

typedef NS_ENUM(NSInteger, TSHelpIndex) {
    TSHelpFAQButtonRect = 0,
    TSHelpEmailTextFieldRect,
    TSHelpContentTextFieldRect,
    TSHelpSeperateViewRect,
    TSHelpDeviceViewRect,
    TSHelpDeviceCommentLabelRect,
    TSHelpSendButtonRect,
};

typedef NS_ENUM(NSInteger, TSDeleteAccountIndex) {
    TSDeleteATitleRect = 0,
    TSDeleteATalkBoxRect,
    TSDeleteATalkBoxLabelRect,
    TSDeleteAMonkeyRect,
    TSDeleteAListRect,
    TSDeleteACheckBoxRect,
    TSDeleteACheckBoxLabelRect,
    TSDeleteAConfirmPWRect,
    TSDeleteADeleteButtonRect,
};

typedef NS_ENUM(NSInteger, TSModifySNSAccountIndex) {
    TSModifySNSIDLabelRect = 0,
    TSModifySNSCurrentPWRect,
    TSModifySNSBirthYearRect,
    TSModifySNSCityRect,
    TSModifySNSMaleRadioRect,
    TSModifySNSFemaleRadioRect,
    TSModifySNSSaveButtonRect,
};

typedef NS_ENUM(NSInteger, TSSignupSNSIndex) {
    TSSignupSNSIDLabelRect = 0,
    TSSignupSNSBirthYearRect,
    TSSignupSNSCityRect,
    TSSignupSNSMaleRadioRect,
    TSSignupSNSFemaleRadioRect,
    TSSignupSNSSignupButtonRect,
};

typedef NS_ENUM(NSInteger, TSAboutIndex) {
    TSAboutServiceImageRect = 0,
    TSAboutCommentRect,
    TSAboutCommentJaRect,
    TSAboutVersionRect,
    TSAboutTermsButtonRect,
};

typedef NS_ENUM(NSInteger, TSGuideIndex) {
    TSGuideParentViewPortrait = 0,
    TSGuideParentViewLandscape,
    TSGuideCreateCharecterAlpha,
    TSGuideCreateCharecterImage,
    TSGuideCreateCharecterLabel,
    TSGuideLoginMainLabel,
    
    TSGuideCameraBobby,
    TSGuideCameraAction,
    TSGuideCameraTalkBox,
    TSGuideCameraLabel,
    
    TSGUideLoginTalkBox,
    TSGuideLoginLabel,
    
    TSGuideLoginCoinImage,
    TSGuideLoginItemImage,
    TSGuideLoginShareImage,
    TSGuideLoginCoinLabel,
    TSGuideLoginItemLabel,
    TSGuideLoginShareLabel,
    TSGuideLoginlauncherImage,
};

FOUNDATION_EXPORT NSString *const kDefaultFontName;
FOUNDATION_EXPORT NSString *const kDefaultBoldFontName;
FOUNDATION_EXPORT NSString *const tsNotificationPopupShow;
FOUNDATION_EXPORT NSString *const tsNotificationPopupClose;

#define UD_BOOL_APPFIRSTRUN     @UDKEY_BOOL_APPFIRSTRUN
#define UD_BOOL_FACEBOOKLOGIN   @UDKEY_BOOL_FACEBOOKLOGIN
#define UD_INTEGER_USERNO       @UDKEY_INTEGER_USERNO
#define UD_STRING_AUTHKEY       @UDKEY_STRING_AUTHKEY
#define UD_STRING_LOGINID       @UDKEY_STRING_LOGINID
#define UD_STRING_LOGINPW       @UDKEY_STRING_LOGINPW
#define UD_STRING_EMAIL         @UDKEY_STRING_EMAIL
#define UD_STRING_BIRTHDAY      @UDKEY_STRING_BIRTHDAY
#define UD_INTEGER_GENDER       @UDKEY_INTEGER_GENDER
#define UD_STRING_LANGUAGE      @UDKEY_STRING_LANGUAGE
#define UD_STRING_SNSCODE       @UDKEY_STRING_SNSCODE
#define UD_STRING_SNSID         @UDKEY_STRING_SNSID
#define UD_STRING_SNSNAME       @UDKEY_STRING_SNSNAME
#define UD_STRING_COUNTRY       @UDKEY_STRING_COUNTRY
#define UD_STRING_UUID          @UDKEY_STRING_UUID
#define UD_STRING_CITY          @UDKEY_STRING_CITY
#define UD_FLOAT_VOLUME         @UDKEY_FLOAT_VOLUME
#define UD_INTEGER_BALANCE      @UDKEY_INTEGER_BALANCE
#define UD_BOOL_MUTEBGM         @UDKEY_BOOL_MUTEBGM


#define getRectValue(viewIndex, objectIndex) [((NSValue *)[[ResolutionManager sharedManger] getValue:viewIndex withObjecID:objectIndex]) CGRectValue]
#define getPointValue(viewIndex, objectIndex) [((NSValue *)[[ResolutionManager sharedManger] getValue:viewIndex withObjecID:objectIndex]) CGPointValue]
#define getSizeValue(viewIndex, objectIndex) [((NSValue *)[[ResolutionManager sharedManger] getValue:viewIndex withObjecID:objectIndex]) CGSizeValue]
#define getIntegerValue(viewIndex, objectIndex) [((NSNumber*)[[ResolutionManager sharedManger] getValue:viewIndex withObjecID:objectIndex]) integerValue]
#define getFloatValue(viewIndex, objectIndex) [((NSNumber*)[[ResolutionManager sharedManger] getValue:viewIndex withObjecID:objectIndex]) floatValue]
#define CGCenterPointFromRect(rect) CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect))

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_LPHONE   [ResolutionManager sharedManger].isLongPhone
#define deviceValueA(pad,phone,sphone)  ( ( IS_IPAD?pad:(IS_LPHONE?phone:sphone) ) )
#define deviceValue(pad,phone)          ( ( IS_IPAD?pad:phone ) )

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#ifdef GFDEBUG_ENABLE
#define GFLog(s, ...)	\
NSLog(@"\n%@ Method:%s {\n%@\n}", NSStringFromClass([self class]), __PRETTY_FUNCTION__, [NSString stringWithFormat:(s), ##__VA_ARGS__]);
#else
#define GFLog( s, ... )
#endif

@interface ResolutionManager : NSObject {
    TSDevice deviceType;
}

@property (nonatomic, readonly) BOOL isLongPhone;

+ (ResolutionManager *)sharedManger;
- (NSObject *)getValue:(TSViewIndex)viewIndex withObjecID:(NSInteger)objectIndex;

@end
