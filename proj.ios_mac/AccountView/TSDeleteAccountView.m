//
//  TSDeleteAccountView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 10..
//
//

#import "TSDeleteAccountView.h"
#import "ResolutionManager.h"
#import "UIImage+PathExtention.h"
#import "UIColor+RGB.h"
#import "TSCheckBox.h"
#import "TSTextField.h"
#import "UIButton+DolesUI.h"
#import "TSDolesNet.h"
#import "TSToastView.h"
#import "TSMessageView.h"
#import <FacebookSDK/FacebookSDK.h>
#import "TSProxyHelper.h"
#import "TSDeleteAccountListCell.h"

#import "MixpanelHelper.h"

@interface TSDeleteAccountView () < UITableViewDataSource, UITableViewDelegate, TSCheckBoxDelegate, TSTextFieldDelegate, TSNetDelegate, TSPopupViewDelegate >

@property (nonatomic, strong) UITableView *contentTableView;
@property (nonatomic, strong) NSArray *dataSource;
@property (nonatomic, strong) TSCheckBox *checkBox;
@property (nonatomic, strong) TSTextField *confirmPWTextFiled;
@property (nonatomic, assign) BOOL confirmPWStringReady;
@property (nonatomic, strong) UIButton *deleteButton;
@property (nonatomic, strong) TSNet *net;

@end

@implementation TSDeleteAccountView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        UIImageView *factorView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"bg_map_factor.png"]];
        factorView.frame = getRectValue(TSViewIndexShare, TSShareSubViewOrgRect);
        [self addSubview:factorView];
        
        UIImageView *logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"title_mini.png" ignore:YES]];
        logoView.frame = getRectValue(TSViewIndexDeleteAccount, TSDeleteATitleRect);
        [self addSubview:logoView];

        UIImageView *talkBoxView = [[UIImageView alloc] initWithImage:
                                    [[UIImage imageNamedEx:@"invite_talk_box.png" ignore:YES] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 20, 25, 47)] ];
        talkBoxView.frame = getRectValue(TSViewIndexDeleteAccount, TSDeleteATalkBoxRect);
        [self addSubview:talkBoxView];

        UILabel *commentLabel = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexDeleteAccount, TSDeleteATalkBoxLabelRect)];
        commentLabel.font = [UIFont fontWithName:kDefaultFontName size:13];
        commentLabel.textAlignment = NSTextAlignmentCenter;
        commentLabel.textColor = RGBH(@"#4f2c00");
        commentLabel.numberOfLines = 0;
        commentLabel.text = NSLocalizedString(@"PF_DELETEACCOUNT_NOTICE", @"Are you sure you want to delete your account? The infomation below will be deleted.");
        [self addSubview:commentLabel];
        
        UIImageView *monkeyView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"delete_account_monkey.png" ignore:YES]];
        monkeyView.frame = getRectValue(TSViewIndexDeleteAccount, TSDeleteAMonkeyRect);
        [self addSubview:monkeyView];
        
        [self adjustTableViewLayout];
        [self adjustCheckBoxLayout];
        if ([[NSUserDefaults standardUserDefaults] valueForKey:UD_BOOL_FACEBOOKLOGIN] == NO) {
            [self adjustTextFieldLayoout];
        } else {
            self.confirmPWStringReady = YES;
        }
        [self adjustButtonLayout];
        
//        CGFloat fontSize = 34/2;
//        NSNumber *num1 = [NSNumber numberWithFloat:34/2];
//        NSNumber *num2 = [NSNumber numberWithFloat:24/2];
        
        UIFont *mainfont = [UIFont fontWithName:kDefaultFontName size:34/2];
        UIFont *subfont = [UIFont fontWithName:kDefaultFontName size:24/2];
        UIFont *lastmainfont = mainfont;
        UIFont *lastsubfont = subfont;
        NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
        if ([language isEqualToString:@"ja"]) {
            lastmainfont = [UIFont fontWithName:kDefaultBoldFontName size:20/2];
            lastsubfont = [UIFont fontWithName:kDefaultFontName size:19/2];
        }
        self.dataSource =
        @[@{kDeleteAccountIconName: @"delete_account_image_dole_coin.png",
            kDeleteAccountTitle: NSLocalizedString(@"PF_DELETEACCOUNT_TABLE_TITLEE_DOLECOIN", @"Dole coin"),
            kDeleteAccountDescription: NSLocalizedString(@"PF_DELETEACCOUNT_TABLE_SUBTITLE_DOLECOIN", @"Earn and usage record"),
            kDeleteAccountFont : mainfont,
            kDeleteAccountSubFont : subfont,
            kDeleteAccountCustomFrame : [NSNumber numberWithBool:NO]},
          @{kDeleteAccountIconName: @"delete_account_image_height_chart.png",
            kDeleteAccountTitle: @"Height chart",
            kDeleteAccountDescription:
                [NSString stringWithFormat:@"%@, %@", NSLocalizedString(@"PF_DELETEACCOUNT_TABLE_SUBTITLE_BOBBY", @"User infomation"),
                 NSLocalizedString(@"PF_DELETEACCOUNT_TABLE_SUBTITLE_BOBBY_ADD", @"Synced date")],
            kDeleteAccountFont : mainfont,
            kDeleteAccountSubFont : subfont,
            kDeleteAccountCustomFrame : [NSNumber numberWithBool:NO]},
          @{kDeleteAccountIconName: @"delete_account_image_bobby.png",
            kDeleteAccountTitle: NSLocalizedString(@"PF_ABOUT_APPTITLE", @"Bobby's Journey"),
            kDeleteAccountDescription: NSLocalizedString(@"PF_DELETEACCOUNT_TABLE_SUBTITLE_BOBBY", @"User infomation"),
            kDeleteAccountFont : lastmainfont,
            kDeleteAccountSubFont : lastsubfont,
            kDeleteAccountCustomFrame : [NSNumber numberWithBool:YES]}, ];
    }
    return self;
}

- (void)adjustTableViewLayout {
    UIImageView *background = [[UIImageView alloc] initWithImage:[[UIImage imageNamedEx:@"delete_account_list.png" ignore:YES]
                                                                  resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)]];
    background.frame = getRectValue(TSViewIndexDeleteAccount, TSDeleteAListRect);
    [self addSubview:background];
    
    NSInteger margin = 2;
//    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)]) {
//        if ( ([UIScreen mainScreen].scale == 2.0) ) {
//        // Retina display
//            margin = 2;
//        }
//    } else {
//        // non-Retina display
//    }
    
    CGRect tableviewFrame = CGRectMake(background.frame.origin.x+margin, background.frame.origin.y+margin,
                                       background.frame.size.width-margin*2, background.frame.size.height-margin*2);
    self.contentTableView = [[UITableView alloc] initWithFrame:tableviewFrame style:UITableViewStylePlain];
    self.contentTableView.backgroundColor = [UIColor clearColor];
    self.contentTableView.dataSource = self;
    self.contentTableView.delegate = self;
    self.contentTableView.separatorColor = RGBH(@"#a28459");
//    self.contentTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:self.contentTableView];
    
    if ([self.contentTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.contentTableView setSeparatorInset:UIEdgeInsetsZero];
    }

}

- (void)adjustCheckBoxLayout {
    self.checkBox = [[TSCheckBox alloc] initWithFrame:getRectValue(TSViewIndexDeleteAccount, TSDeleteACheckBoxRect)];
    self.checkBox.delegate = self;
    [self.checkBox setImage:[UIImage imageNamedEx:@"checkbox_off.png" ignore:YES] forState:UIControlStateNormal];
    [self.checkBox setImage:[UIImage imageNamedEx:@"checkbox_on.png" ignore:YES] forState:UIControlStateSelected];
    [self addSubview:self.checkBox];
    
    UILabel *agreeLabel = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexDeleteAccount, TSDeleteACheckBoxLabelRect)];
    agreeLabel.font = [UIFont fontWithName:kDefaultFontName size:13];
    agreeLabel.textColor = RGBH(@"#4f2c00");
    agreeLabel.numberOfLines = 1;
    agreeLabel.adjustsFontSizeToFitWidth = YES;
    agreeLabel.text = NSLocalizedString(@"PF_DELETEACCOUNT_CHECKBOX_CONFIRM", @"Yes, I want to delete account.");
    [self addSubview:agreeLabel];
}

- (void)checkBox:(TSCheckBox *)checkBox didChangeSelectedStatus:(BOOL)selected {
    [self checkEnableDeleteButton];
}

- (void)adjustTextFieldLayoout {
    self.confirmPWTextFiled =
    [[TSTextField alloc] initWithFrame:getRectValue(TSViewIndexDeleteAccount, TSDeleteAConfirmPWRect)
                                  icon:@"input_password_icon.png" placeholder:NSLocalizedString(@"PF_DELETEACCOUNT_TEXTFIELD_CONFIRM_PASSWORD", @"Conform Password")];
    [self.confirmPWTextFiled setPasswordType:YES];
    self.confirmPWTextFiled.delegate = self;
    [self addSubview:self.confirmPWTextFiled];
}

- (void)tstextFieldDidEndEditing:(TSTextField *)textField {
    self.confirmPWStringReady = [self.confirmPWTextFiled checkPasswordStringValidationWithShowError:
                                 NSLocalizedString(@"PF_NET_ERROR__PASSWORD_SHORT", @"￼Please enter a password with at least 6 characters")];
    [self checkEnableDeleteButton];
}

- (void)adjustButtonLayout {
    //tuilise_todo : keyboard Show에 따른 위치 이동 엑션 처리 필요
    self.deleteButton = [UIButton buttonWithType:TSDolsTypeOK Frame:getRectValue(TSViewIndexDeleteAccount, TSDeleteADeleteButtonRect)
                                           Title:NSLocalizedString(@"PF_DELETEACCOUNT_BUTTON_TITLE_DELETE", @"Delete Account")];
    [self.deleteButton addTarget:self action:@selector(selectorDeleteButton:) forControlEvents:UIControlEventTouchUpInside];
    self.deleteButton.enabled = NO;
    [self addSubview:self.deleteButton];
}

- (IBAction)selectorDeleteButton:(id)sender
{
    NSLog(@"selectorDeleteButton");
    BOOL loginWithSNS = [[[NSUserDefaults standardUserDefaults] valueForKey:UD_BOOL_FACEBOOKLOGIN] boolValue];
    if (loginWithSNS == NO) {
        self.net = [[TSNetWithDraw alloc] initWithPassword:self.confirmPWTextFiled.text Reason:@"withDraw"];
    } else {
#ifdef TSNET_REAL_SNSID_ENABLE
        self.net = [[TSNetDisconnectSNS alloc] initWithID:[[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_SNSID] reason:@"DisconnectSNS"];
#else
        self.net = [[TSNetDisconnectSNS alloc] initWithID:[[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_EMAIL] reason:@"DisconnectSNS"];
#endif
    }
    self.net.delegate = self;
    [self.net start];
}

- (void)popview:(TSPopupView *)view didClickedButton:(TSPopupButtonType)type {
    [[FBSession activeSession] closeAndClearTokenInformation];
    [FBSession setActiveSession:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kTSNotifivationDoleCoinUpdate object:nil userInfo:@{@"originlayer": [NSNumber numberWithInteger:self.originLayer]}];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(closeViewControllerWithView:)]) {
        [self.delegate closeViewControllerWithView:self];
    }
}

- (void)net:(TSNet *)netObject didStartProcess:(TSNetResultType)result {
    if (result == TSNetSuccess) {
        [self.deleteButton showActivity:YES];
    } else {
        TSToastView *toast = [[TSToastView alloc] initWithString:NSLocalizedString(@"CM_NET_WARNING_NETWORK_UNSTABLE", @"The network is unstable.\nPlease try again")];
        [toast showInViewWithAnimation:self];
    }
}

- (void)net:(TSNet *)netObject didEndProcess:(TSNetResultType)result {
    [self.deleteButton showActivity:NO];
    if (result == TSNetSuccess) {
        {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_AUTHKEY];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_INTEGER_USERNO];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_EMAIL];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_LOGINID];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_LOGINPW];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_BIRTHDAY];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_CITY];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_COUNTRY];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_INTEGER_GENDER];
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_BOOL_FACEBOOKLOGIN];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_SNSID];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_SNSNAME];
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_INTEGER_BALANCE];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@UDKEY_STRING_PRODUCTSALES];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@UDKEY_DOUBLE_PROODUCTCACHEVER];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@UDKEY_STRING_PROMOTION_TM];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        TSMessageView *messageView =
        [[TSMessageView alloc] initWithMessage:NSLocalizedString(@"PF_DELETEACCOUNT_DELETE_COMPLETE", @"Thank you for using Bobby's Story World!")
                                          icon:TSPopupIconTypeWarning];
        messageView.buttonLayout = TSPopupButtonLayoutOK;
        messageView.delegate = self;
        [messageView showInView:self];
        
    } else {
        switch (netObject.returnCode) {
            case 90106: {
                [self.confirmPWTextFiled setError:NSLocalizedString(@"PF_NET_ERROR__PASSWORD_INCORRECT", @"The password is incorrect")];
            }  break;
//            case 90105:
//            case 90003: {
//                [self.confirmPWTextFiled setError:@"The email does not exist or is incorrect"];
//            }  break;
            default: {
                TSMessageView *messageView =
                [[TSMessageView alloc] initWithMessage:[NSString stringWithFormat:@"Server Error\n%@", [netObject serverErrorString]] icon:TSPopupIconTypeWarning];
                messageView.buttonLayout = TSPopupButtonLayoutOK;
                [messageView showInView:self];
            } break;
        }
    }
}

- (void)net:(TSNet *)netObject didFailWithError:(TSNetResultType)result {
    [self.deleteButton showActivity:NO];
    TSToastView *toast = [[TSToastView alloc] initWithString:NSLocalizedString(@"CM_NET_WARNING_NETWORK_UNSTABLE", @"The network is unstable.\nPlease try again")];
    [toast showInViewWithAnimation:self];
}

- (void)checkEnableDeleteButton {
    if (self.confirmPWStringReady == YES && self.checkBox.selected == YES) {
        self.deleteButton.enabled = YES;
    } else {
        self.deleteButton.enabled = NO;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSInteger count = [self.dataSource count];
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    TSDeleteAccountListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[TSDeleteAccountListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    cell.customData = [self.dataSource objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
