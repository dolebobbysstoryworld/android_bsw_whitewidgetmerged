//
//  UIViewController+ChangeRootView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 11. 5..
//
//

#import "UIViewController+ChangeRootView.h"
#include "cocos2d.h"
#include "AppDelegate.h"
#import "AppController.h"

@implementation UIViewController (ChangeRootView)

+ (void)replaceRootViewControlller:(TSViewController*)newRootViewController {
    UIViewController *preRootViewController =
    (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        newRootViewController.modalPresentationStyle = UIModalPresentationFormSheet;
        if (IS_IOS8) {
            newRootViewController.preferredContentSize = CGSizeMake(1024, 768);
        }
        [preRootViewController presentViewController:newRootViewController animated:YES completion:nil];
    } else {
        if (IS_IOS8) {
            cocos2d::Director::getInstance()->stopAnimation();
            //http://stackoverflow.com/questions/12313330/use-presentmodalviewcontroller-but-with-modeuiviewanimationtransitioncurlup
            [UIView transitionWithView:preRootViewController.view.window
                              duration:0.5
                               options:UIViewAnimationOptionTransitionCurlUp | UIViewAnimationOptionCurveEaseInOut////UIModalTransitionStyleCoverVertical
                            animations:^{
                                [((AppController *)[UIApplication sharedApplication].delegate).mainWindow setRootViewController:newRootViewController];
                            } completion:^(BOOL finished) {
                                // Code to run after animation
                            }];
            
            newRootViewController.preRootViewController = preRootViewController;
        } else {
            [preRootViewController presentViewController:newRootViewController animated:YES completion:nil];
        }
    }
}

+ (void)restoreRootViewControlller:(TSViewController*)currentRootViewController {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [currentRootViewController dismissViewControllerAnimated:YES completion:nil];
    } else {
        if (IS_IOS8) {
            [UIView transitionWithView:currentRootViewController.view.window
                              duration:0.5
                               options:UIViewAnimationOptionTransitionCurlDown//UIModalTransitionStyleCoverVertical
                            animations:^{
                                [((AppController *)[UIApplication sharedApplication].delegate).mainWindow
                                 setRootViewController:currentRootViewController.preRootViewController];
                                cocos2d::Director::getInstance()->startAnimation();
                            } completion:^(BOOL finished) {
                                // Code to run after animation
                            }];
        } else {
            [currentRootViewController dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

@end
