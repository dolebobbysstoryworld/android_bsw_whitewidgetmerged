//
//  TSHelpFAQView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 10..
//
//

#import "TSHelpFAQView.h"
#import "UIImage+PathExtention.h"
#import "ResolutionManager.h"
#import "UIColor+RGB.h"
#import "UIButton+DolesUI.h"
#import "TSTextField.h"
#include <sys/sysctl.h>
#import <sys/utsname.h>
#import "TSDolesNet.h"
#import "TSMessageView.h"
#import "TSToastView.h"

#define TAG_INDICATOR_VIEW      10001

@interface TSHelpFAQView () < TSTextFieldDelegate, UITextViewDelegate, TSNetDelegate >

@property (nonatomic, strong) UIButton *sendButton;
@property (nonatomic, strong) TSTextField *emailTextField;
@property (nonatomic, assign) BOOL emailStringReady;
@property (nonatomic, strong) UITextView *feedbackView;
@property (nonatomic, strong) NSString *textviewPlaceholder;
@property (nonatomic, assign) BOOL feedbackStringReady;
@property (nonatomic, strong) TSNetRegisterCS *net;
@property (nonatomic, strong) UILabel *infoLabel;

@end

@implementation TSHelpFAQView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"bg_map_factor.png"]];
        backgroundView.frame = getRectValue(TSViewIndexShare, TSShareSubViewOrgRect);
        [self addSubview:backgroundView];

        UIImageView *logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"login_logo.png"]];
        logoView.frame = getRectValue(TSViewIndexShare, TSShareLogoRect);
        [self addSubview:logoView];
        
        UILabel *commentLabel = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexHelp, TSHelpDeviceCommentLabelRect)];
        commentLabel.font = [UIFont fontWithName:kDefaultFontName size:12];
        NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
        if ([language isEqualToString:@"ja"]) {
            commentLabel.textAlignment = NSTextAlignmentCenter;
        } else {
            commentLabel.textAlignment = NSTextAlignmentLeft;
        }
        commentLabel.textColor = RGBH(@"#4f2c00");
        commentLabel.numberOfLines = 0;//tuilise_todo : 두줄이 되려면 가로 가이드가 2px 좀더 커야 한다.
        commentLabel.text = NSLocalizedString(@"PF_HELP_WARNING_PRIVACYINFO", @"The device and entered data is used only for handling the inquiry.");
        [self addSubview:commentLabel];
        
        [self adjustButtonLayout];
        [self adjustTextFieldLayout];
        [self adjustFeedbackLayout];
    }
    return self;
}

- (void)adjustButtonLayout {
    UIButton *faqButton = [UIButton buttonWithType:TSDolsTypeCancel Frame:getRectValue(TSViewIndexHelp, TSHelpFAQButtonRect)
                                             Title:NSLocalizedString(@"PF_HELP_BUTTON_TITLE_FAQ", @"FAQ")];
    [faqButton addTarget:self action:@selector(selectorFAQButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:faqButton];
    
    self.sendButton = [UIButton buttonWithType:TSDolsTypeOK Frame:getRectValue(TSViewIndexHelp, TSHelpSendButtonRect)
                                         Title:NSLocalizedString(@"PF_HELP_BUTTON_TITLE_SEND", @"Send")];
    [self.sendButton addTarget:self action:@selector(selectorSendButton:) forControlEvents:UIControlEventTouchUpInside];
    self.sendButton.enabled = NO;
    [self addSubview:self.sendButton];
}

- (IBAction)selectorFAQButton:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.doleapps.com"]];
}

- (IBAction)selectorSendButton:(id)sender {
    self.net = [[TSNetRegisterCS alloc] initWithEmail:self.emailTextField.text description:self.feedbackView.text
                deviceInfo:self.infoLabel.text];
    self.net.delegate = self;
    [self.net start];
}

- (void)checkEnableLoginButton {
    if (self.emailStringReady == YES && self.feedbackStringReady == YES) {
        self.sendButton.enabled = YES;
    } else {
        self.sendButton.enabled = NO;
    }
}

- (void)adjustTextFieldLayout {
    self.emailTextField =
    [[TSTextField alloc] initWithFrame:getRectValue(TSViewIndexHelp, TSHelpEmailTextFieldRect)
                                  icon:@"input_mail_icon.png" placeholder:NSLocalizedString(@"PF_HELP_TEXTFIELD_EMAIL", @"Your email")];
    self.emailTextField.delegate = self;
    [self.emailTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [self addSubview:self.emailTextField];

    NSString *authKey = [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_AUTHKEY];
    if ([authKey length] > 0) {
        NSString *email = [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_LOGINID];
        if([email length] > 0) {
            self.emailTextField.text = email;
            self.emailStringReady = YES;
        }
    }

}

#pragma mark - TSTextFiledDelegate
- (void)tstextFieldDidEndEditing:(TSTextField *)textField {
    self.emailStringReady = [self.emailTextField checkEmailStringValidationWithShowError:
                             NSLocalizedString(@"PF_NET_ERROR_EMAIL_ENTER_FULL_ADDRESS", @"Please enter the full email address,\nincluding after the @ sign")];
    [self checkEnableLoginButton];
}

- (void)adjustFeedbackLayout {
    CGRect feedback = getRectValue(TSViewIndexHelp, TSHelpContentTextFieldRect);
    CGRect seperator = getRectValue(TSViewIndexHelp, TSHelpSeperateViewRect);
    CGRect device = getRectValue(TSViewIndexHelp, TSHelpDeviceViewRect);
    
    UIImageView *contentBackgroundView =
    [[UIImageView alloc] initWithImage:[[UIImage imageNamedEx:@"help_box_bg.png" ignore:YES]
                                        resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)]];
    contentBackgroundView.frame = CGRectMake(feedback.origin.x, feedback.origin.y,
                                             feedback.size.width, feedback.size.height+seperator.size.height+device.size.height);
    [self addSubview:contentBackgroundView];
    
    self.feedbackView = [[UITextView alloc] initWithFrame:feedback];
    self.feedbackView.backgroundColor = [UIColor clearColor];
    self.feedbackView.font = [UIFont fontWithName:kDefaultFontName size:15];
    self.feedbackView.textColor = RGBH(@"#9fa0a3");
    self.feedbackView.textContainer.lineFragmentPadding = 0;
    self.feedbackView.textContainerInset = UIEdgeInsetsMake(14, 23, 0, 14);
    self.feedbackView.textAlignment = NSTextAlignmentLeft;
    self.feedbackView.autocorrectionType = UITextAutocorrectionTypeNo;
    self.feedbackView.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.feedbackView.spellCheckingType = UITextSpellCheckingTypeNo;
    self.textviewPlaceholder = NSLocalizedString(@"PF_HELP_TEXTFIELD_FEEDBACK", @"Type feedback here");
    self.feedbackView.text = self.textviewPlaceholder;
    self.feedbackView.delegate = self;
    [self addSubview:self.feedbackView];
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:seperator];
    seperatorView.backgroundColor = RGBH(@"#b7b7b7");
    [self addSubview:seperatorView];
    
    UIView *deviceView = [[UIView alloc] initWithFrame:device];
    deviceView.backgroundColor = [UIColor clearColor];
    [self addSubview:deviceView];
    
    UIImageView *iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"help_input_question_box_device.png" ignore:YES]];
    [deviceView addSubview:iconView];
    self.infoLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 10, 196, 16)];//tuilise_todo : 가로 사이즈 잘못 표시됨
    self.infoLabel.backgroundColor =  [UIColor clearColor];
    self.infoLabel.font = [UIFont fontWithName:kDefaultFontName size:14];
    self.infoLabel.textAlignment = NSTextAlignmentLeft;
    self.infoLabel.textColor = RGBH(@"#606060");
    self.infoLabel.numberOfLines = 1;//tuilise_todo : 두줄이 되려면 가로 가이드가 2px 좀더 커야 한다.
    self.infoLabel.adjustsFontSizeToFitWidth = YES;
    self.infoLabel.text = [NSString stringWithFormat:@"%@ : iOS %@ / %@", NSLocalizedString(@"PF_HELP_DEVICE", Device), [UIDevice currentDevice].systemVersion, [self getModel]];
    [deviceView addSubview:self.infoLabel];
}

- (NSString *)getModel {//tuilise_todo
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *model = malloc(size);
    sysctlbyname("hw.machine", model, &size, NULL, 0);
    NSString *deviceModel = [NSString stringWithCString:model encoding:NSUTF8StringEncoding];
    free(model);
//    return deviceModel;
    return [self platformType:deviceModel];
}

- (NSString *) platformType:(NSString *)platform {
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5";// (GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5";// (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c";// (GSM)";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c";// (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s";// (GSM)";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s";// (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6+";// Plus";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2";// (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2";// (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2";// (CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2";// (WiFi)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini";// (WiFi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini";// (GSM)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini";// (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3";// (WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3";// (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3";// (GSM)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4";// (WiFi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4";// (GSM)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4";// (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air";        // 5th Generation iPad (iPad Air) - Wifi
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air";        // 5th Generation iPad (iPad Air) - Cellular
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPad Mini";       // (2nd Generation iPad Mini - Wifi)
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPad Mini";       // (2nd Generation iPad Mini - Cellular)
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    return platform;
}
//
//- (NSString*) deviceName {
//    struct utsname systemInfo;
//    
//    uname(&systemInfo);
//    
//    NSString* code = [NSString stringWithCString:systemInfo.machine
//                                        encoding:NSUTF8StringEncoding];
//    
//    static NSDictionary* deviceNamesByCode = nil;
//    
//    if (!deviceNamesByCode) {
//        
//        deviceNamesByCode = @{@"i386"      :@"Simulator",
//                              @"iPod1,1"   :@"iPod Touch",      // (Original)
//                              @"iPod2,1"   :@"iPod Touch",      // (Second Generation)
//                              @"iPod3,1"   :@"iPod Touch",      // (Third Generation)
//                              @"iPod4,1"   :@"iPod Touch",      // (Fourth Generation)
//                              @"iPhone1,1" :@"iPhone",          // (Original)
//                              @"iPhone1,2" :@"iPhone",          // (3G)
//                              @"iPhone2,1" :@"iPhone",          // (3GS)
//                              @"iPad1,1"   :@"iPad",            // (Original)
//                              @"iPad2,1"   :@"iPad 2",          //
//                              @"iPad3,1"   :@"iPad",            // (3rd Generation)
//                              @"iPhone3,1" :@"iPhone 4",        //
//                              @"iPhone4,1" :@"iPhone 4S",       //
//                              @"iPhone5,1" :@"iPhone 5",        // (model A1428, AT&T/Canada)
//                              @"iPhone5,2" :@"iPhone 5",        // (model A1429, everything else)
//                              @"iPad3,4"   :@"iPad",            // (4th Generation)
//                              @"iPad2,5"   :@"iPad Mini",       // (Original)
//                              @"iPhone5,3" :@"iPhone 5c",       // (model A1456, A1532 | GSM)
//                              @"iPhone5,4" :@"iPhone 5c",       // (model A1507, A1516, A1526 (China), A1529 | Global)
//                              @"iPhone6,1" :@"iPhone 5s",       // (model A1433, A1533 | GSM)
//                              @"iPhone6,2" :@"iPhone 5s",       // (model A1457, A1518, A1528 (China), A1530 | Global)
//                              @"iPad4,1"   :@"iPad Air",        // 5th Generation iPad (iPad Air) - Wifi
//                              @"iPad4,2"   :@"iPad Air",        // 5th Generation iPad (iPad Air) - Cellular
//                              @"iPad4,4"   :@"iPad Mini",       // (2nd Generation iPad Mini - Wifi)
//                              @"iPad4,5"   :@"iPad Mini"        // (2nd Generation iPad Mini - Cellular)
//                              };
//    }
//    
//    NSString* deviceName = [deviceNamesByCode objectForKey:code];
//    
//    if (!deviceName) {
//        // Not found on database. At least guess main device type from string contents:
//        
//        if ([code rangeOfString:@"iPod"].location != NSNotFound) {
//            deviceName = @"iPod Touch";
//        }
//        else if([code rangeOfString:@"iPad"].location != NSNotFound) {
//            deviceName = @"iPad";
//        }
//        else if([code rangeOfString:@"iPhone"].location != NSNotFound){
//            deviceName = @"iPhone";
//        }
//    }
//    
//    return deviceName;
//}
//
- (void)textViewDidBeginEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:self.textviewPlaceholder]) {
        textView.text = @"";
//        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@""]) {
        textView.text = self.textviewPlaceholder;
//        textView.textColor = [UIColor lightGrayColor]; //optional
        self.feedbackStringReady = NO;
    } else {
        self.feedbackStringReady = YES;
    }
    [textView resignFirstResponder];
    [self checkEnableLoginButton];
}

#pragma mark - TSNetDelegate
- (void)net:(TSNet*)netObject didStartProcess:(TSNetResultType)result {
    [self.sendButton showActivity:YES];
    self.sendButton.enabled = NO;
}

- (void)net:(TSNet*)netObject didEndProcess:(TSNetResultType)result {
    self.sendButton.enabled = YES;
    [self.sendButton showActivity:NO];
    
    if (!result) {
        NSDictionary *dic = @{@"toast": NSLocalizedString(@"PF_HELP_POPUP_SEND_COMPLETE", @"Your inquiry has been sent")};
        if (self.delegate && [self.delegate respondsToSelector:@selector(view:Page:userData:)]) {
            [self.delegate view:self Page:TSViewIndexSettingMain userData:dic];
        }
    } else {//tuilise_todo : 오류 결과다. 맞는 처리 필요
        TSMessageView *messageView =
        [[TSMessageView alloc] initWithMessage:[NSString stringWithFormat:@"Server Error\n%@", [netObject serverErrorString]] icon:TSPopupIconTypeWarning];
        messageView.buttonLayout = TSPopupButtonLayoutOK;
        [messageView showInView:self];
    }
}

- (void)net:(TSNet*)netObject didFailWithError:(TSNetResultType)result {
    switch (result) {
        case TSNetErrorNetworkDisable:
        case TSNetErrorTimeout: {
            TSToastView *toast = [[TSToastView alloc] initWithString:NSLocalizedString(@"CM_NET_WARNING_NETWORK_UNSTABLE", @"The network is unstable.\nPlease try again")];
            [toast showInViewWithAnimation:self];
        } break;
        default: {
        } break;
    }
    self.sendButton.enabled = YES;
    [self.sendButton showActivity:NO];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
