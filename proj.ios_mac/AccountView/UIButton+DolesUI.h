//
//  UIButton+DolesUI.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 30..
//
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, TSDolesType) {
    TSDolsTypeOK = 1,
    TSDolsTypeCancel,
    TSDolsTypePopOK,
    TSDolsTypePopCancel,
    TSDolsTypeAbout,
};

@interface UIButton (DolesUI)

+ (UIButton*)buttonWithType:(TSDolesType)type Frame:(CGRect)frame Title:(NSString*)title;
+ (UIButton*)buttonWithType:(TSDolesType)type Frame:(CGRect)frame Title:(NSString*)title Font:(UIFont*)font Color:(UIColor*)color;

+ (UIButton*)underlineButtonWithTitle:(NSString*)string Frame:(CGRect)frame;
+ (UIButton*)underlineButtonWithTitle:(NSString*)string Frame:(CGRect)frame textColor:(UIColor*)textColor;
+ (UIButton*)underlineButtonWithTitle:(NSString*)string Frame:(CGRect)frame Tag:(NSInteger)tag;
+ (UIButton*)underlineButtonWithTitle:(NSString*)string Frame:(CGRect)frame Tag:(NSInteger)tag textColor:(UIColor*)textColor;

- (void)showActivity:(BOOL)show;

@end
