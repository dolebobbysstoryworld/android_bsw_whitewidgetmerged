//
//  TSDeleteAccountListCell.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 11. 18..
//
//

#import "TSDeleteAccountListCell.h"
#import "UIImage+PathExtention.h"
#import "UIColor+RGB.h"

NSString *const kDeleteAccountIconName      = @"iconName";
NSString *const kDeleteAccountTitle         = @"titleString";
NSString *const kDeleteAccountDescription   = @"descriptionString";
NSString *const kDeleteAccountFont          = @"font";
NSString *const kDeleteAccountSubFont       = @"subfont";
NSString *const kDeleteAccountCustomFrame   = @"customframe";

@implementation TSDeleteAccountListCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setCustomData:(NSDictionary *)customData {
    _customData = customData;
    self.backgroundColor = [UIColor clearColor];
    
    NSString *iconName = [self.customData objectForKey:kDeleteAccountIconName];
    UIImageView *iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:iconName ignore:YES]];
    iconView.frame = CGRectMake(11, 6.5, 47, 47);
    [self addSubview:iconView];
    
    NSString *title = [self.customData objectForKey:kDeleteAccountTitle];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(69, 12, 167, 19)];
    if ([[self.customData objectForKey:kDeleteAccountCustomFrame] boolValue] == YES) {
        titleLabel.frame = CGRectMake((22+94+18)/2, 12, 342/2, 19);
    }
    //    NSLog(@"main font : %@", [self.customData objectForKey:kDeleteAccountFont]);
    titleLabel.font = [self.customData objectForKey:kDeleteAccountFont];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.textColor = RGBH(@"#4f2c00");//tuilise_todo : Delete account cell의 text color 값 확인 필요
    titleLabel.numberOfLines = 1;
    titleLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.text = title;
    //    titleLabel.backgroundColor = [UIColor greenColor];
    [self addSubview:titleLabel];
    
    NSString *description = [self.customData objectForKey:kDeleteAccountDescription];
    UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(69, 34, 167, 14)];
    //    NSLog(@"sub font : %@", [self.customData objectForKey:kDeleteAccountSubFont]);
    descriptionLabel.font = [self.customData objectForKey:kDeleteAccountSubFont];
    descriptionLabel.textAlignment = NSTextAlignmentLeft;
    descriptionLabel.textColor = RGBH(@"#6c410a");
    descriptionLabel.numberOfLines = 1;
    descriptionLabel.adjustsFontSizeToFitWidth = YES;
    descriptionLabel.text = description;
    //    descriptionLabel.backgroundColor = [UIColor yellowColor];
    [self addSubview:descriptionLabel];
    
    CGRect frame = self.frame;
    frame.origin.y = (24+38+6+28+24)/2;
    frame.size.height = 2/2;
    UIView *seperatorView = [[UIView alloc] initWithFrame:frame];
    seperatorView.backgroundColor = RGBH(@"#a28459");
    [self addSubview:seperatorView];
    
    //    NSLog(@"self.indentationLevel : %d self.indentationWidth : %f", self.indentationLevel, self.indentationWidth);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
