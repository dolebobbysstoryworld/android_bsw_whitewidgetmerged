//
//  TSProxyHelper.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 19..
//
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const kTSNotifivationLoginDidFinished;
FOUNDATION_EXPORT NSString *const kTSNotifivationRecallLoginDidFinished;
FOUNDATION_EXPORT NSString *const kTSNotifivationSetMute;
FOUNDATION_EXPORT NSString *const kTSNotifivationUseCoinPassword;
FOUNDATION_EXPORT NSString *const kTSNotifivationShare;
FOUNDATION_EXPORT NSString *const kTSNotifivationDoleCoinUpdate;
FOUNDATION_EXPORT NSString *const kTSNotifivationFirstRunAccountViewColsed;

@interface TSProxyHelper : NSObject

@property (nonatomic, assign) void *homeLayer;

+ (TSProxyHelper *)sharedInstance;

- (void)addNotification;
- (void)removeNotification;

@end
