//
//  TSAccountViewController.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 29..
//
//

#import <UIKit/UIKit.h>
#import "ResolutionManager.h"
#import "TSViewController.h"

typedef NS_ENUM(NSInteger, TSParentType) {
    TSParentTypeNone = 0,
    TSParentTypeSetting,
    TSParentTypeDoleCoin,
};

@interface TSAccountViewController : TSViewController

@property (nonatomic, assign) TSParentType parentType;
@property (nonatomic, assign) TSOriginLayer originLayer;
@property (nonatomic, assign) TSRecall recallType;

- (instancetype)initWithFirstRun:(BOOL)first;

@end
