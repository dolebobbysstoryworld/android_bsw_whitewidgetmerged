//
//  TSSettingViewController.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 10..
//
//

#import <UIKit/UIKit.h>
#import "TSViewController.h"

@interface TSSettingViewController : TSViewController

@property (nonatomic, assign) TSOriginLayer originLayer;

@end
