//
//  TSAboutView.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 1..
//
//

#import <UIKit/UIKit.h>
#import "TSAccountDelegateProtocol.h"
#import "TSMessageView.h"

@interface TSAboutView : UIView < TSPopupViewDelegate >

@property (nonatomic, assign) id<TSAccountDelegate> delegate;
@property (nonatomic, assign) TSOriginLayer originLayer;

@end
