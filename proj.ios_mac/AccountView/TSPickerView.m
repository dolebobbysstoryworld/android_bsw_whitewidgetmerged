//
//  TSPickerView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 3..
//
//

#import "TSPickerView.h"

#define MODE_

@interface TSPickerView () < UIPickerViewDelegate, UIPickerViewDataSource >
//@interface TSPickerView () < UITableViewDelegate, UITableViewDataSource >

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UITableView *contentTableView;
@property (nonatomic, strong) UIPickerView *contentPickerView;
@property (nonatomic, strong) NSArray *dataSource;
@property (nonatomic, strong) NSString *title;

@end

@implementation TSPickerView

- (id)initWithDataSource:(NSArray*)dataSource title:(NSString*)string {
    self = [super init];
    if (self) {
        self.title = string;
        self.backgroundColor = [UIColor clearColor];
        self.dataSource = dataSource;
    }
    return self;
}

- (void)adjustTopViewLayout {
//    NSLog(@"TSPickerView::adjustTopViewLayout");
    UIImageView *top = [[UIImageView alloc] initWithImage:
                        [[UIImage imageNamedEx:@"popup_top.png" ignore:YES]
                         resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)]];
    top.frame = CGRectMake(0, 0, POPUPVIEW_WIDTH, 42);
    self.topView = top;
    [self addSubview:self.topView];

    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(13, 10, 246, 21)];
    self.titleLabel.font = [UIFont fontWithName:kDefaultFontName size:19];
    self.titleLabel.textColor = RGBH(@"#ffffff");
    self.titleLabel.text = self.title;
    [self addSubview:self.titleLabel];
}

- (void)adjustContentViewLayout {
//    NSLog(@"TSPickerView::adjustContentViewLayout");
    self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0, self.topView.frame.size.height, POPUPVIEW_WIDTH, POPUPVIEW_CONTENT_HEIGHT)];
    UIImageView *middle = [[UIImageView alloc] initWithImage:
                           [[UIImage imageNamedEx:@"popup_middle.png" ignore:YES]
                            resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)]];
    middle.frame = CGRectMake(0, 0, POPUPVIEW_WIDTH, POPUPVIEW_CONTENT_HEIGHT);
    [self.contentView addSubview:middle];

    self.contentPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 272, POPUPVIEW_CONTENT_HEIGHT)];
    self.contentPickerView.dataSource = self;
    self.contentPickerView.delegate = self;
    self.contentPickerView.transform = CGAffineTransformMakeScale(232/self.contentPickerView.frame.size.width, 232/self.contentPickerView.frame.size.width);
    self.contentPickerView.center = CGPointMake(POPUPVIEW_WIDTH/2, POPUPVIEW_CONTENT_HEIGHT/2);//CGPointMake(136, 112);
//    self.contentPickerView.backgroundColor = [UIColor yellowColor];
//    NSLog(@"picker frame : %@", NSStringFromCGRect(self.contentPickerView.frame));
    self.contentPickerView.showsSelectionIndicator = NO;
    [self.contentView addSubview:self.contentPickerView];

    [self adjustOverlayLineViewLayout];
    
    [self addSubview:self.contentView];

    [self.contentPickerView selectRow:self.selectedIndex inComponent:0 animated:NO];
}

//- (void)adjustContentViewLayout {
//    self.contentTableView = [[UITableView alloc] initWithFrame:POPUPVIEW_CONTENTAREA_FRAME style:UITableViewStylePlain];
//    self.contentTableView.dataSource = self;
//    self.contentTableView.delegate = self;
//    self.contentTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    [self addSubview:self.contentTableView];
//}

- (void)adjustOverlayLineViewLayout {
    UIView *overlayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, POPUPVIEW_WIDTH, POPUPVIEW_CONTENT_HEIGHT)];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(20, 48, 232, 2)];//CGRectMake(20, 45, 232, 2)
    lineView.backgroundColor = RGBH(@"#6fd4dd");
    lineView.userInteractionEnabled = NO;
    [overlayView addSubview:lineView];
    
    lineView = [[UIView alloc] initWithFrame:CGRectMake(20, 90, 232, 2)];//CGRectMake(20, 93, 232, 2)
    lineView.backgroundColor = RGBH(@"#6fd4dd");
    lineView.userInteractionEnabled = NO;
    [overlayView addSubview:lineView];
    
    overlayView.userInteractionEnabled = NO;
    [self.contentView addSubview:overlayView];
}

- (void)showInView:(UIView *)parent {
    [super showInView:parent];
}

- (void)setSelectedIndex:(NSInteger)selectedIndex {
    _selectedIndex = selectedIndex;
    [self.contentPickerView selectRow:self.selectedIndex inComponent:0 animated:NO];
}

- (void)setSelectedString:(NSString *)selectedString {
    _selectedString = selectedString;
    NSString *searchObject;
    for (NSInteger index = [self.dataSource count] - 1; index >= 0; index--) {
        searchObject = [self.dataSource objectAtIndex:index];
        if ( [searchObject isEqualToString:selectedString] == YES ) {
            self.selectedIndex = index;
            return;
        }
    }
    self.selectedIndex = 0;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 46.0f;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *string = [self.dataSource objectAtIndex:row];
    return string;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    _selectedIndex = row;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSInteger count = [self.dataSource count];
    return count;
}

/*
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 92.0f/2;//tuilise_todo : 높이가 소숫점 나온다.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSInteger count = [self.dataSource count];
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    NSString *string = [self.dataSource objectAtIndex:indexPath.row];
    cell.textLabel.text = string;
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedIndex = indexPath.row;
}
*/
+ (NSArray*)birthYearsData {
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorian components:NSYearCalendarUnit fromDate:[NSDate date]];
    
    NSInteger startYear = 1901;
    NSInteger endYear = [components year];
    while (startYear <= endYear) {
        [array addObject:[NSString stringWithFormat:@"%ld", (long)startYear]];
        startYear++;
    }
    
    return array;
}

+ (NSArray*)countrys {
//    return @[@"日本", @"한국", @"New Zealand", @"Philippines", @"Singapore", @"Australia"];
    return @[@"日本", @"한국", @"New Zealand", @"Philippines", @"Singapore", NSLocalizedString(@"PF_SIGNUP_COUNTRY_LIST_OTHER", @"Other")];
}

+ (NSArray*)countryCodes {
//    return @[@"JP", @"KR", @"NZ", @"PH", @"SG", @"158"];
    return @[@"JP", @"KR", @"NZ", @"PH", @"SG", @"Other"];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
