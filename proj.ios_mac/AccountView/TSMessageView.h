//
//  TSMessageView.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 3..
//
//

#import "TSPopupView.h"

typedef NS_ENUM(NSInteger, TSPopupIconType) {
    TSPopupIconTypeWarning = 0,
    TSPopupIconTypeCoin,
};

@interface TSMessageView : TSPopupView

- (id)initWithMessage:(NSString*)string icon:(TSPopupIconType)iconType;

@end
