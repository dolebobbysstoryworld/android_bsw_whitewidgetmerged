//
//  TSPopupView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 3..
//
//

#import "TSPopupView.h"

@interface TSPopupView ()

@property (nonatomic, strong) UIButton *leftButton;

@property (nonatomic, strong) UIView *dimlayerView;

@end

@implementation TSPopupView

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code'
//        self.backgroundColor = [UIColor redColor];
        self.popupType = TSPopupTypeMessage;
        if ( [NSStringFromClass([self class]) isEqualToString:@"TSPickerView"] ) {
            self.popupType = TSPopupTypePicker;
        }
        self.pendingCancel = NO;
        self.nodimlayer = NO;
        self.customCancelTitle = nil;
    }
    return self;
}

- (void)adjustSubviews {
    if (self.topView == nil) {
        [self adjustTopViewLayout];
    }
    if (self.contentView == nil) {
        [self adjustContentViewLayout];
    }
    if (self.bottomView == nil) {
        [self adjustBottomViewLayout];
        self.frame = CGRectMake(0, 0, POPUPVIEW_WIDTH, self.topView.frame.size.height+self.contentView.frame.size.height+self.bottomView.frame.size.height);
        NSLog(@"topView : %@\ncontentView : %@\nbottomView : %@\npopupView : %@",
              NSStringFromCGRect(self.topView.frame), NSStringFromCGRect(self.contentView.frame),
              NSStringFromCGRect(self.bottomView.frame), NSStringFromCGRect(self.frame));
    }
}

- (void)adjustTopViewLayout {
//    NSLog(@"TSPopupView::adjustTopViewLayout");
    UIImageView *top = [[UIImageView alloc] initWithImage:
                        [UIImage imageNamedEx:@"popup_no_title.png" ignore:YES]];
    self.topView = top;
    [self addSubview:self.topView];
}

- (void)adjustContentViewLayout {
//    NSLog(@"TSPopupView::adjustContentViewLayout");
}

- (void)adjustBottomViewLayout {
//    NSLog(@"TSPopupView::adjustBottomViewLayout");
    self.bottomView = [[UIView alloc] initWithFrame:
                       CGRectMake(0, self.topView.frame.size.height+self.contentView.frame.size.height, POPUPVIEW_WIDTH, 45)];
    UIImageView *bottom = [[UIImageView alloc] initWithImage:
                           [[UIImage imageNamedEx:@"popup_bottom.png" ignore:YES]
                            resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)]];
    bottom.frame = CGRectMake(0, 0, POPUPVIEW_WIDTH, 45);
    [self.bottomView addSubview:bottom];
    
    switch (self.buttonLayout) {
        case TSPopupButtonLayoutCancel: {
            self.leftButton = [UIButton buttonWithType:TSDolsTypePopCancel Frame:POPUPVIEW_ONEBUTTON_FRAME
                                                 Title:NSLocalizedString(@"CM_POPUP_BUTTON_TITLE_CANCEL", @"Cancel")];
            [self.leftButton addTarget:self action:@selector(selectorCancelButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.bottomView addSubview:self.leftButton];
        } break;
        case TSPopupButtonLayoutCancelOK: {
            if (self.customCancelTitle == nil) {
                self.leftButton = [UIButton buttonWithType:TSDolsTypePopCancel Frame:POPUPVIEW_LEFTBUTTON_FRAME
                                                     Title:NSLocalizedString(@"CM_POPUP_BUTTON_TITLE_CANCEL", @"Cancel")];
            } else {
                self.leftButton = [UIButton buttonWithType:TSDolsTypePopCancel Frame:POPUPVIEW_LEFTBUTTON_FRAME
                                                     Title:self.customCancelTitle];
            }
            [self.leftButton addTarget:self action:@selector(selectorCancelButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.bottomView addSubview:self.leftButton];
            self.rightButton = [UIButton buttonWithType:TSDolsTypePopOK Frame:POPUPVIEW_RIGHTBUTTON_FRAME
                                                  Title:NSLocalizedString(@"CM_POPUP_BUTTON_TITLE_OK", @"OK")];
            [self.rightButton addTarget:self action:@selector(selectorOKButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.bottomView addSubview:self.rightButton];
        } break;
        case TSPopupButtonLayoutNoYes: {
            self.leftButton = [UIButton buttonWithType:TSDolsTypePopCancel Frame:POPUPVIEW_LEFTBUTTON_FRAME
                                                 Title:NSLocalizedString(@"CM_POPUP_BUTTON_TITLE_NO", @"No")];
            [self.leftButton addTarget:self action:@selector(selectorCancelButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.bottomView addSubview:self.leftButton];
            self.rightButton = [UIButton buttonWithType:TSDolsTypePopOK Frame:POPUPVIEW_RIGHTBUTTON_FRAME
                                                  Title:NSLocalizedString(@"CM_POPUP_BUTTON_TITLE_YES", @"Yes")];
            [self.rightButton addTarget:self action:@selector(selectorOKButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.bottomView addSubview:self.rightButton];
        } break;
        case TSPopupButtonLayoutOK: {
            self.leftButton = [UIButton buttonWithType:TSDolsTypePopOK Frame:POPUPVIEW_ONEBUTTON_FRAME
                                                 Title:NSLocalizedString(@"CM_POPUP_BUTTON_TITLE_OK", @"OK")];
            [self.leftButton addTarget:self action:@selector(selectorOKButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.bottomView addSubview:self.leftButton];
        } break;
        case TSPopupButtonLayoutCancelSet: {
            self.leftButton = [UIButton buttonWithType:TSDolsTypePopCancel Frame:POPUPVIEW_LEFTBUTTON_FRAME
                                                 Title:NSLocalizedString(@"CM_POPUP_BUTTON_TITLE_CANCEL", @"Cancel")];
            [self.leftButton addTarget:self action:@selector(selectorCancelButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.bottomView addSubview:self.leftButton];
            self.rightButton = [UIButton buttonWithType:TSDolsTypePopOK Frame:POPUPVIEW_RIGHTBUTTON_FRAME
                                                  Title:NSLocalizedString(@"PF_SIGNUP_BUTTON_TITLE_SET", @"Set")];
            [self.rightButton addTarget:self action:@selector(selectorOKButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.bottomView addSubview:self.rightButton];
        } break;
        case TSPopupButtonLayoutCancelCustom: {
            self.leftButton = [UIButton buttonWithType:TSDolsTypePopCancel Frame:POPUPVIEW_LEFTBUTTON_FRAME
                                                 Title:NSLocalizedString(@"CM_POPUP_BUTTON_TITLE_CANCEL", @"Cancel")];
            [self.leftButton addTarget:self action:@selector(selectorCancelButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.bottomView addSubview:self.leftButton];
            self.rightButton = [UIButton buttonWithType:TSDolsTypePopOK Frame:POPUPVIEW_RIGHTBUTTON_FRAME Title:self.customButtonTitle];
            [self.rightButton addTarget:self action:@selector(selectorOKButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.bottomView addSubview:self.rightButton];
        } break;
            
    }
    
    [self addSubview:self.bottomView];
}
#ifdef UI_TYPE_IOS8
- (void)showInView:(UIView*)parent {
    CGRect dimlayerFrame;
    if (parent.window) {
        NSLog(@"%@", NSStringFromCGRect(parent.window.frame));
        dimlayerFrame = parent.window.frame;
    } else {
        NSLog(@"%@", NSStringFromCGRect(parent.frame));
        dimlayerFrame = parent.frame;
    }
    if (!IS_IOS8 && (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)) {
        dimlayerFrame = CGRectMake(parent.window.frame.origin.x, parent.window.frame.origin.y, parent.window.frame.size.height, parent.window.frame.size.width);
    }
    //IOS8 - PAD : {{0, 0}, {1024, 768}}
    //IOS8 - PHONE : {{0, 0}, {320, 568}}
    //IOS7 - PAD : {{0, 0}, {768, 1024}}
    //IOS7 - PHONE : {{0, 0}, {320, 568}}
    NSLog(@"%@", NSStringFromCGRect(dimlayerFrame));
    NSLog(@"%@", NSStringFromCGRect(parent.frame));
    self.dimlayerView = [[UIView alloc] initWithFrame:dimlayerFrame];
    self.dimlayerView.backgroundColor = [UIColor blackColor];//tuilise_todo : pad에선 dim 배경색에 문제가 생긴다. 해결책 찾자.
    self.dimlayerView.alpha = 0.6f;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        CGFloat newX = 0 - (dimlayerFrame.size.width - parent.frame.size.width)/2;
        CGFloat newY = 0 - (dimlayerFrame.size.height - parent.frame.size.height)/2;
        self.dimlayerView.frame = CGRectMake(newX, newY, dimlayerFrame.size.width, dimlayerFrame.size.height);
    }
    [parent addSubview:self.dimlayerView];
    
    self.center = CGPointMake(parent.frame.size.width/2, parent.frame.size.height/2);
    [parent addSubview:self];
}

- (void)showInFullView:(UIView*)parent {
    [self showInView:parent];
}

#else
- (void)showInView:(UIView*)parent {
//    UIView *currentView = parent;
//    while (currentView) {
//        NSLog(@"Name:%@ RECT:%@",NSStringFromClass([currentView class]), NSStringFromCGRect(currentView.frame));
//        currentView = currentView.superview;
//    }
//    NSLog(@"Name:%@ RECT:%@",NSStringFromClass([parent.superview class]), NSStringFromCGRect(parent.superview.frame));
    
//    NSLog(@"UIWindow : %@ center : %@", NSStringFromCGRect(parent.window.frame), NSStringFromCGPoint(parent.window.center));
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        CGRect newFrame = CGRectMake(parent.window.frame.origin.x, parent.window.frame.origin.y,
                                     parent.window.frame.size.height, parent.window.frame.size.width);
        self.dimlayerView = [[UIView alloc] initWithFrame:newFrame];
    } else {
        self.dimlayerView = [[UIView alloc] initWithFrame:parent.window.frame];
    }
    self.dimlayerView.backgroundColor = [UIColor blackColor];//tuilise_todo : pad에선 dim 배경색에 문제가 생긴다. 해결책 찾자.
    self.dimlayerView.alpha = 0.6f;
    
    CGPoint center = [parent convertPoint:parent.window.center fromView:parent.window];
    self.dimlayerView.center = center;
//    NSLog(@"PopupBackground : %@ center : %@", NSStringFromCGRect(self.backgorundView.frame), NSStringFromCGPoint(self.backgorundView.center));
    if (self.nodimlayer == NO) {
        [parent addSubview:self.dimlayerView];
    }
    self.center = center;
//    NSLog(@"Popup : %@ center : %@", NSStringFromCGRect(self.frame), NSStringFromCGPoint(self.center));
    [parent addSubview:self];
    [[NSNotificationCenter defaultCenter] postNotificationName:tsNotificationPopupShow object:nil];
}

- (void)showInFullView:(UIView*)parent {
    NSLog(@"parent.window.frame : %@ center : %@", NSStringFromCGRect(parent.frame), NSStringFromCGPoint(parent.center));
    CGRect newFrame = CGRectMake(0, 0, parent.frame.size.width, parent.frame.size.height);
    self.dimlayerView = [[UIView alloc] initWithFrame: newFrame];
    self.dimlayerView.backgroundColor = [UIColor blackColor];// TODO:tuilise pad에선 dim 배경색에 문제가 생긴다. 해결책 찾자.
    self.dimlayerView.alpha = 0.4f;
    
    CGPoint parentCenter = CGPointMake(newFrame.size.width/2, newFrame.size.height/2);
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
//        parentCenter = CGPointMake(parent.center.y, parent.center.x);
//    }
//    self.dimlayerView.center = parentCenter;
    NSLog(@"dimlayerView : %@ center : %@", NSStringFromCGRect(self.dimlayerView.frame), NSStringFromCGPoint(self.dimlayerView.center));
    if (self.nodimlayer == NO) {
        [parent addSubview:self.dimlayerView];
    }
    self.center = parentCenter;
    NSLog(@"Popup : %@ center : %@", NSStringFromCGRect(self.frame), NSStringFromCGPoint(self.center));
    [parent addSubview:self];
    [[NSNotificationCenter defaultCenter] postNotificationName:tsNotificationPopupShow object:nil];
}
#endif

- (void)showInViewForLandscape:(UIView*)parent {
    CGRect newFrame = CGRectMake(0, 0, parent.frame.size.width, parent.frame.size.height);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        if (newFrame.size.height > newFrame.size.width) {
            newFrame = CGRectMake(0, 0, parent.frame.size.height, parent.frame.size.width);
        }
    }
    self.dimlayerView = [[UIView alloc] initWithFrame: newFrame];
    self.dimlayerView.backgroundColor = [UIColor blackColor];// TODO:tuilise pad에선 dim 배경색에 문제가 생긴다. 해결책 찾자.
    self.dimlayerView.alpha = 0.4f;
    
    [parent addSubview:self.dimlayerView];

    CGFloat newX = (newFrame.size.width - self.frame.size.width)/2;
    CGFloat newY = (newFrame.size.height - self.frame.size.height)/2;
    self.frame = CGRectMake(newX, newY, self.frame.size.width, self.frame.size.height);
    [parent addSubview:self];
}

- (void)hide {
    [self.dimlayerView removeFromSuperview];
    self.dimlayerView = nil;
    [self removeFromSuperview];
#ifndef UI_TYPE_IOS8
    [[NSNotificationCenter defaultCenter] postNotificationName:tsNotificationPopupClose object:nil];
#endif
}

- (void)setButtonLayout:(TSPopupButtonLayoutType)buttonLayout {
    _buttonLayout = buttonLayout;
    [self adjustSubviews];
}

- (void)setCustomButtonTitle:(NSString *)customButtonTitle {
    _customButtonTitle = customButtonTitle;
    if (self.rightButton) {
        CGSize boundingRectSize = CGSizeMake(self.rightButton.frame.size.width-4, CGFLOAT_MAX);
        NSDictionary *attributes = @{NSFontAttributeName : [UIFont fontWithName:kDefaultFontName size:34/2]};
        CGRect labelSize = [customButtonTitle boundingRectWithSize:boundingRectSize
                                                           options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                                        attributes:attributes context:nil];
        if (labelSize.size.height > self.rightButton.frame.size.height) {
            self.rightButton.titleLabel.font = [UIFont fontWithName:kDefaultBoldFontName size:24/2];
        }
        [self.rightButton setTitle:self.customButtonTitle forState:UIControlStateNormal];
    }
}

- (IBAction)selectorCancelButton:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(popview:didClickedButton:)]) {
        [self.delegate popview:self didClickedButton:TSPopupButtonTypeCancel];
    }
    if (self.pendingCancel == YES) {
        return;
    }
    [self hide];
}

- (void)processPendingCancel {
    [self hide];
}

- (IBAction)selectorOKButton:(id)sender {
    TSPopupButtonType buttonType;
    switch (self.buttonLayout) {
        case TSPopupButtonLayoutCancelSet: {
            buttonType = TSPopupButtonTypeSet;
        } break;
        case TSPopupButtonLayoutCancelCustom: {
            buttonType = TSPopupButtonTypeCustom;
        } break;
        default:
            buttonType = TSPopupButtonTypeOK;
            break;
    }

    if ([self buttonWillOKProcessing] == YES) {
//        [self performSelector:@selector(hide) withObject:nil afterDelay:0.2f];
//        if (self.delegate && [self.delegate respondsToSelector:@selector(popview:didClickedButton:)]) {
//            [self.delegate popview:self didClickedButton:buttonType];
//        }
        [self performSelector:@selector(selectorAfterOKAndHideProcess:) withObject:[NSNumber numberWithInteger:buttonType] afterDelay:0.1f];
        [self hide];
    }
}

- (void)selectorAfterOKAndHideProcess:(id)buttonTypeObject {
    TSPopupButtonType buttonType = [buttonTypeObject integerValue];
    if (self.delegate && [self.delegate respondsToSelector:@selector(popview:didClickedButton:)]) {
        [self.delegate popview:self didClickedButton:buttonType];
    }
}

- (BOOL)buttonWillOKProcessing {
    return YES;
}

- (void)setPopupHidden:(BOOL)popupHidden {
    _popupHidden = popupHidden;
    self.dimlayerView.hidden = popupHidden;
    self.hidden = popupHidden;
}

- (void)setCustomCancelTitle:(NSString *)title {
    _customCancelTitle = title;
    if (self.leftButton) {
        [self.leftButton setTitle:self.customCancelTitle forState:UIControlStateNormal];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
