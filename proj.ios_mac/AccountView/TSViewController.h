//
//  TSViewController.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 11. 6..
//
//

#import <UIKit/UIKit.h>
#import "ResolutionManager.h"
#import "UIImage+PathExtention.h"

typedef NS_ENUM(NSInteger, TSRecall) {
    TSRecallNone = 0,
    TSRecallHomeDoleCoin,
    TSRecallHomePurchase,
    TSRecallBookMapSharePopup,
    TSRecallBookMapDLDevicePopup,
    TSRecallDoleCoinScanQR,
    TSRecallCharRECUnlockItem,
};

@protocol TSViewControllerDelegate <NSObject>

@optional
- (void)viewController:(UIViewController*)vc resultObject:(id)object;

@end

#define LAYOUT_POSITION_X               ((IS_IPAD?160:66)/2)
#define LAYOUT_OBJECT_WIDTH             (508/2)

@interface TSViewController : UIViewController

@property (nonatomic, assign) id<TSViewControllerDelegate> delegate;
@property (nonatomic, strong) UIViewController *preRootViewController;
@property (nonatomic, strong) UIView *layoutView;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, assign) BOOL isKeyboardHide;
@property (nonatomic, assign) TSOriginLayer originLayer;
@property (nonatomic, assign) TSRecall recallType;
@property (nonatomic, strong) NSString *bgFactorName;
@property (nonatomic, assign) BOOL ignoreBGImage;
@property (nonatomic, strong) UIImageView *lion;
@property (nonatomic, strong) UIImageView *racoon;

- (IBAction)selectorCloseButton:(id)sender;
- (void)closeTSViewController;
- (void)closeTSViewControllerWithAnimation:(BOOL)animated;
- (void)noticeWillShowKeyboard:(NSNotification *)notification;
- (void)noticeWillHideKeyboard:(NSNotification *)notification;
- (void)selectorAfterObjectProcess:(id)object;
- (BOOL)isActivityShow;
- (void)showActivityInView:(BOOL)show;

//for debug
@property (nonatomic, strong) NSString *className;

@end
