//
//  TSAboutView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 1..
//
//

#import "TSAboutView.h"
#import "ResolutionManager.h"
#import "UIImage+PathExtention.h"
#import "UIColor+RGB.h"
#import "UIButton+DolesUI.h"
#if COCOS2D_DEBUG
#import <FacebookSDK/FacebookSDK.h>
#import "TSProxyHelper.h"
#endif

@implementation TSAboutView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        UIImageView *factorView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"bg_map_factor.png"]];
        factorView.frame = getRectValue(TSViewIndexShare, TSShareSubViewOrgRect);
        [self addSubview:factorView];
        
        UIImageView *logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"login_logo.png"]];
        logoView.frame = getRectValue(TSViewIndexShare, TSShareLogoRect);
        [self addSubview:logoView];

        UIImageView *serviceView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"about_service_image.png" ignore:YES]];
        serviceView.frame = getRectValue(TSViewIndexAbout, TSAboutServiceImageRect);
        [self addSubview:serviceView];

        [self adjustLabelLayout];
        [self adjustButtonLayout];
    }
    return self;
}

- (void)adjustLabelLayout {
    CGRect labelRect;
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    CGFloat fontSize;
    if ([language isEqualToString:@"ja"]) {
        fontSize = 24/2;
        labelRect = getRectValue(TSViewIndexAbout, TSAboutCommentJaRect);
    } else {
        fontSize = 28/2;
        labelRect = getRectValue(TSViewIndexAbout, TSAboutCommentRect);
    }
    
    UILabel *commentLabel = [[UILabel alloc] initWithFrame:labelRect];
    commentLabel.font = [UIFont fontWithName:kDefaultFontName size:fontSize];
    commentLabel.textAlignment = NSTextAlignmentCenter;
    commentLabel.textColor = RGBH(@"#4f2c00");
    commentLabel.numberOfLines = 0;//tuilise_todo : 두줄 되게 하려고 2px 늘렸다.
    commentLabel.text = NSLocalizedString(@"PF_ABOUT_SERVICE_INTRODUCE", @"Bobby's Journey lets kids\nhave fun making their own storybook.");
    [self addSubview:commentLabel];

    NSString *versionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
//    NSString *buildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];//build string empty 하면 앱 설치 오류 난다. 기억해 둬라...
#if COCOS2D_DEBUG
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = getRectValue(TSViewIndexAbout, TSAboutVersionRect);
    
    NSMutableAttributedString *normalTitle =
    [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Ver. %@", versionString]
                                           attributes:@{NSForegroundColorAttributeName: RGBH(@"#4f2c00"),
                                                        NSFontAttributeName:[UIFont fontWithName:kDefaultFontName size:15]}];
    [button setAttributedTitle:normalTitle forState:UIControlStateNormal];
    [button addTarget:self action:@selector(selectorVersionButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];
#else
    UILabel *versionLabel = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexAbout, TSAboutVersionRect)];
    versionLabel.font = [UIFont fontWithName:kDefaultFontName size:15];
    versionLabel.textAlignment = NSTextAlignmentCenter;
    versionLabel.textColor = RGBH(@"#4f2c00");
    versionLabel.numberOfLines = 1;
    
    versionLabel.text = [NSString stringWithFormat:@"Ver. %@", versionString];
    [self addSubview:versionLabel];
#endif
}

#if COCOS2D_DEBUG
- (IBAction)selectorVersionButton:(id)sender {
    TSMessageView *messageView = [[TSMessageView alloc] initWithMessage:@"for DEBUG\n가입 정보와 로그인 정보를 지우시겠습니까?" icon:TSPopupIconTypeWarning];
    messageView.delegate = self;
    messageView.buttonLayout = TSPopupButtonLayoutCancelOK;
    [messageView showInView:self];
}

- (void)popview:(TSPopupView *)view didClickedButton:(TSPopupButtonType)type {
    if (type == TSPopupButtonTypeOK) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_BOOL_APPFIRSTRUN];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_AUTHKEY];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_INTEGER_USERNO];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_LOGINID];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_EMAIL];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_BIRTHDAY];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_CITY];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_INTEGER_GENDER];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_BOOL_FACEBOOKLOGIN];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_SNSID];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_SNSNAME];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_INTEGER_BALANCE];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[FBSession activeSession] closeAndClearTokenInformation];
        [FBSession setActiveSession:nil];
        
        NSHTTPCookie *cookie;
        NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        for (cookie in [storage cookies])
        {
            NSString* domainName = [cookie domain];
            NSRange domainRange = [domainName rangeOfString:@"facebook"];
            if(domainRange.length > 0)
            {
                [storage deleteCookie:cookie];
            }
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(closeViewControllerWithView:)]) {
            [self.delegate closeViewControllerWithView:self];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:kTSNotifivationDoleCoinUpdate object:nil
                                                          userInfo:@{@"originlayer": [NSNumber numberWithInteger:self.originLayer]}];
    }
}
#endif

- (void)adjustButtonLayout {
    UIButton *termsButton = [UIButton buttonWithType:TSDolsTypeOK Frame:getRectValue(TSViewIndexAbout, TSAboutTermsButtonRect)
                                               Title:NSLocalizedString(@"PF_ABOUT_BUTTON_TITLE_TERMS", @"Terms and Conditions")];
    [termsButton addTarget:self action:@selector(selectorTermsButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:termsButton];
}

- (IBAction)selectorTermsButton:(id)sender {
    NSLog(@"selectorTermsButton");
    if (self.delegate && [self.delegate respondsToSelector:@selector(view:Page:userData:)]) {
        [self.delegate view:self Page:TSViewIndexAgreement userData:nil];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
