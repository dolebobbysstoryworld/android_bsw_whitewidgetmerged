//
//  TSAccountLoginView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 29..
//
//

#import "TSAccountLoginView.h"
#import "UIImage+PathExtention.h"
#import "ResolutionManager.h"
#import "TSTextField.h"
#import "UIButton+DolesUI.h"
#import "UIColor+RGB.h"
#import "TSDolesNet.h"
#import "TSMessageView.h"
#import "TSToastView.h"
#import "TSProxyHelper.h"
#import <FacebookSDK/FacebookSDK.h>

#define TAG_FORGOT_BUTTON           10001
#define TAG_SIGNIN_BUTTON           10002
#define TAG_LOGINFB_BUTTON          10003
#define TAG_ID_TEXTFILED            10004
#define TAG_PW_TEXTFILED            10005
#define TAG_NET_LOGIN               10006
#define TAG_INDICATOR_VIEW          10007
#define TAG_NET_FINDPASSWORD        10008
#define TAG_NET_GETPROFILE          10009
#define TAG_NET_SNSLOGIN            10010

@interface TSAccountLoginView () < TSTextFieldDelegate, TSNetDelegate, TSPopupViewDelegate >

@property (nonatomic, strong) TSTextField *idTextField;
@property (nonatomic, strong) TSTextField *pwTextField;
@property (nonatomic, strong) UIButton *loginButton;

@property (nonatomic, assign) BOOL emailStringReady;
@property (nonatomic, assign) BOOL passwordStringReady;
@property (nonatomic, strong) TSNet *net;

@property (nonatomic, strong) UIButton *skipTextButton;
@property (nonatomic, strong) UIButton *skipImageButton;

@property (nonatomic, strong) UILabel *forgotNoticeLabel;
@property (nonatomic, strong) NSDictionary *loginForFacebookData;

@end

@implementation TSAccountLoginView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.forgotMode = NO;
        self.needPostNotification = NO;
        UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"bg_sea.png"]];
        backgroundView.frame = getRectValue(TSViewIndexShare, TSShareSeaRect);
        [self addSubview:backgroundView];
        //tuilise_todo : iPad와 이미지 배치가 다름?
        
        UIImageView *factorView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"bg_map_factor.png"]];
        factorView.frame = getRectValue(TSViewIndexShare, TSShareSubViewOrgRect);
        [self addSubview:factorView];
        
        UIImageView *logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"login_logo.png"]];
        logoView.frame = getRectValue(TSViewIndexShare, TSShareLogoRect);
        [self addSubview:logoView];
        
        [self addLoginTextFiled];
        [self addLoginButton];
        [self addAccountButtons];
    }
    return self;
}

#pragma mark - id/password operation
- (void)addLoginTextFiled {
    self.idTextField =
    [[TSTextField alloc] initWithFrame:getRectValue(TSViewIndexLogin, TSLoginIDRect)
                                  icon:@"input_mail_icon.png" placeholder:NSLocalizedString(@"PF_LOGIN_TEXTFIELD_EMAIL", @"ExampleID@dole.com")];
    self.idTextField.delegate = self;
    [self.idTextField setTag:TAG_ID_TEXTFILED];
    [self.idTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    //        [idTextField setComment:@"at least 6 chracters"];
    //        [idTextField setText:@"test9998@abc.com"];
    [self addSubview:self.idTextField];
    
    self.pwTextField =
    [[TSTextField alloc] initWithFrame:getRectValue(TSViewIndexLogin, TSLoginPWRect)
                                  icon:@"input_password_icon.png" placeholder:NSLocalizedString(@"PF_LOGIN_TEXTFIELD_PASSWORD", @"Password")];
    self.pwTextField.delegate = self;
    [self.pwTextField setTag:TAG_PW_TEXTFILED];
    [self.pwTextField setPasswordType:YES];
    [self addSubview:self.pwTextField];
}

#pragma mark - login operation
- (void)addLoginButton {
    self.loginButton = [UIButton buttonWithType:TSDolsTypeOK Frame:getRectValue(TSViewIndexLogin, TSLoginButtonRect)
                                          Title:NSLocalizedString(@"PF_LOGIN_BUTTON_TITLE_LOGIN", @"Login")];
    [self.loginButton addTarget:self action:@selector(selectorLoginButton:) forControlEvents:UIControlEventTouchUpInside];
    self.loginButton.enabled = NO;
    [self addSubview:self.loginButton];
}

- (IBAction)selectorLoginButton:(id)sender {
    NSLog(@"selectorLoginButton");
    [self endEditing:YES];
    self.loginButton.enabled = NO;
    self.net = [[TSNetLogin alloc] initWithID:self.idTextField.text Password:self.pwTextField.text];
//    if (self.net == nil) {
//        TSMessageView *messageView =
//        [[TSMessageView alloc] initWithMessage:@"로그인 실패\n아이디 또는 비밀번호가 입력되지 않았습니다." icon:TSPopupIconTypeWarning];
//        messageView.buttonLayout = TSPopupButtonLayoutOK;
//        [messageView showInView:self];
//        return;
//    }
    [self.net setTag:TAG_NET_LOGIN];
    self.net.delegate = self;
    [self.net start];
}

- (IBAction)selectorSendButton:(id)sender {
    [self endEditing:YES];
    self.loginButton.enabled = NO;
    self.net = [[TSNetFindPassword alloc] initWithEmail:self.idTextField.text];
    [self.net setTag:TAG_NET_FINDPASSWORD];
    self.net.delegate = self;
    [self.net start];
}

#pragma mark - TSNetDelegate
- (void)net:(TSNet*)netObject didStartProcess:(TSNetResultType)result {
    switch (netObject.tag) {
        case TAG_NET_LOGIN: {
            [self.loginButton showActivity:YES];
        } break;
        case TAG_NET_SNSLOGIN: {
            self.userInteractionEnabled = NO;
            [self showActivityInView:YES];
        } break;
        case TAG_NET_FINDPASSWORD: {
            [self.loginButton showActivity:YES];
        } break;
        case TAG_NET_GETPROFILE: {
            [self.loginButton showActivity:YES];
            self.loginButton.enabled = NO;
        } break;
        default: { } break;
    }
}

- (void)net:(TSNet*)netObject didEndProcess:(TSNetResultType)result {
    switch (netObject.tag) {
        case TAG_NET_LOGIN: {
            [self.loginButton showActivity:NO];
            self.loginButton.enabled = YES;
            if (!result) {
                NSString *birthYear = [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_BIRTHDAY];
                if ([birthYear length] > 0) {
                    if (self.delegate && [self.delegate respondsToSelector:@selector(closeViewControllerWithView:)]) {
                        self.needPostNotification = YES;
                        [self.delegate closeViewControllerWithView:self];
//                        [[NSNotificationCenter defaultCenter] postNotificationName:kTSNotifivationLoginDidFinished object:nil userInfo:@{@"originlayer": [NSNumber numberWithInteger:self.originLayer]}];
                    }
                } else {
                    self.net = [[TSNetGetProfile alloc] init];
                    self.net.tag = TAG_NET_GETPROFILE;
                    self.net.delegate = self;
                    [self.net start];
                }
            } else {//tuilise_todo : 오류 결과다. 맞는 처리 필요
                switch (netObject.returnCode) {
                    case 90105: {
                        [self.idTextField setError:NSLocalizedString(@"PF_NET_ERROR_EMAIL_ADDRESS", @"The email does not exist or is incorrect")];
                    } break;
                    case 90113: {
                        [self.idTextField setError:NSLocalizedString(@"PF_NET_ERROR_EMAIL_FOR_SNS", @"This account is signed up with Facebook")];
                    } break;
                    case 90106: {
                        [self.pwTextField setError:NSLocalizedString(@"PF_NET_ERROR__PASSWORD_INCORRECT", @"The password is incorrect")];
                    } break;
                    default: {
                        TSMessageView *messageView =
                        [[TSMessageView alloc] initWithMessage:[NSString stringWithFormat:@"Server Error\n%@", [netObject serverErrorString]] icon:TSPopupIconTypeWarning];
                        messageView.buttonLayout = TSPopupButtonLayoutOK;
                        [messageView showInView:self];
                    } break;
                }
            }
        } break;
        case TAG_NET_FINDPASSWORD: {
            [self.loginButton showActivity:NO];
            self.loginButton.enabled = YES;
            if (!result) {
                TSMessageView *messageView =
                [[TSMessageView alloc] initWithMessage:NSLocalizedString(@"PF_FORGOT_POPUP_CONFIRM_PASSWORD",
                                                                         @"The temporary password was sent.\nPlease check your spam folder if the email is not in your inbox. (Takes around 5 to 10 minutes.)")
                                                  icon:TSPopupIconTypeWarning];
                messageView.buttonLayout = TSPopupButtonLayoutOK;
                messageView.delegate = self;
                [messageView showInView:self];
            } else {//tuilise_todo : 오류 결과다. 맞는 처리 필요
                if (netObject.returnCode == 90114) {
                    [self.idTextField setError:NSLocalizedString(@"PF_NET_ERROR_EMAIL_FOR_SNS", @"This account is signed up with Facebook")];
                } else if (netObject.returnCode == 90002) {//90002 번 (레코드 없음) 으로 보완 해서 배포 하도록 하겠습니다
                    [self.idTextField setError:NSLocalizedString(@"PF_NET_ERROR_EMAIL_ADDRESS", @"The email does not exist or is incorrect")];
                } else {
                    TSMessageView *messageView =
                    [[TSMessageView alloc] initWithMessage:[NSString stringWithFormat:@"Server Error\n%@", [netObject serverErrorString]] icon:TSPopupIconTypeWarning];
                    messageView.buttonLayout = TSPopupButtonLayoutOK;
                    [messageView showInView:self];
                }
            }
        } break;
        case TAG_NET_GETPROFILE: {
            if (!result) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(closeViewControllerWithView:)]) {
                    self.needPostNotification = YES;
                    [self.delegate closeViewControllerWithView:self];
//                    [[NSNotificationCenter defaultCenter] postNotificationName:kTSNotifivationLoginDidFinished object:nil userInfo:@{@"originlayer": [NSNumber numberWithInteger:self.originLayer]}];
                }
            } else {//tuilise_todo : 오류 결과다. 맞는 처리 필요
                TSMessageView *messageView =
                [[TSMessageView alloc] initWithMessage:[NSString stringWithFormat:@"Server Error\n%@", [netObject serverErrorString]] icon:TSPopupIconTypeWarning];
                messageView.buttonLayout = TSPopupButtonLayoutOK;
                [messageView showInView:self];
            }
        } break;
        case TAG_NET_SNSLOGIN: {
            self.userInteractionEnabled = YES;
            [self showActivityInView:NO];
            if (!result) {//SNS Login 성공
                [self saveFacebookLoginInfo];
                NSString *birthYear = [[NSUserDefaults standardUserDefaults] valueForKey:UD_STRING_BIRTHDAY];
                if ([birthYear length] > 0) {//개인 정보가 있는 상태이다.
                    if (self.delegate && [self.delegate respondsToSelector:@selector(closeViewControllerWithView:)]) {
                        self.needPostNotification = YES;
                        [self.delegate closeViewControllerWithView:self];
                    }
                } else {
                    self.net = [[TSNetGetProfile alloc] init];
                    self.net.tag = TAG_NET_GETPROFILE;
                    self.net.delegate = self;
                    [self.net start];
                }
            } else {// Login 실패 했으니 신규 가입
                if (netObject.returnCode == 90105 || netObject.returnCode == 90107) {
                    if (self.delegate && [self.delegate respondsToSelector:@selector(view:Page:userData:)]) {
                        [self.delegate view:self Page:TSViewIndexAgreement userData:self.loginForFacebookData];
                    }
                } else if (netObject.returnCode == 90137) {//탈퇴한 SNS 계정
                    TSToastView *toastView = [[TSToastView alloc] initWithString:NSLocalizedString(@"PF_NET_ERROR_EMAIL_NOT_AVAILABLE", @"This email address is not available")];
                    [toastView showInViewWithAnimation:self];
                } else {
                    TSMessageView *messageView =
                    [[TSMessageView alloc] initWithMessage:[NSString stringWithFormat:@"Server Error\n%@", [netObject serverErrorString]]
                                                      icon:TSPopupIconTypeWarning];
                    messageView.buttonLayout = TSPopupButtonLayoutOK;
                    [messageView showInView:self];
                }
            }
        } break;
        default: { } break;
    }
}

- (void)net:(TSNet*)netObject didFailWithError:(TSNetResultType)result {
    switch (result) {
        case TSNetErrorNetworkDisable:
        case TSNetErrorTimeout: {
            TSToastView *toast = [[TSToastView alloc] initWithString:NSLocalizedString(@"CM_NET_WARNING_NETWORK_UNSTABLE", @"The network is unstable.\nPlease try again")];
            [toast showInViewWithAnimation:self];
        } break;
        default: {
        } break;
    }
    switch (netObject.tag) {
        case TAG_NET_LOGIN: {
            [self.loginButton showActivity:NO];
            self.loginButton.enabled = YES;
        } break;
        case TAG_NET_FINDPASSWORD: {
            
        } break;
        case TAG_NET_GETPROFILE: {
            [self.loginButton showActivity:NO];
            self.loginButton.enabled = YES;
        } break;
        case TAG_NET_SNSLOGIN: {
            self.userInteractionEnabled = YES;
            [self showActivityInView:NO];
        } break;
        default: { } break;
    }
}

#pragma mark - TSPopupViewDelegate
- (void)popview:(TSPopupView *)view didClickedButton:(TSPopupButtonType)type {
    if (self.delegate && [self.delegate respondsToSelector:@selector(closeViewControllerWithView:)]) {
        [self.delegate closeViewControllerWithView:self];
    }
}

#pragma mark - accound manage operation
- (void)addAccountButtons {
    UIButton *button = [UIButton underlineButtonWithTitle:NSLocalizedString(@"PF_LOGIN_BUTTON_TITLE_FORGOT", @"Forgot your password?")
                                                    Frame:getRectValue(TSViewIndexLogin, TSLoginFgPWRect) Tag:TAG_FORGOT_BUTTON];
    [button addTarget:self action:@selector(selectorUnderlineButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];

    button = [UIButton underlineButtonWithTitle:NSLocalizedString(@"PF_LOGIN_BUTTON_TITLE_SIGNUP", @"Sign in")
                                          Frame:getRectValue(TSViewIndexLogin, TSLoginSigninRect) Tag:TAG_SIGNIN_BUTTON];
    [button addTarget:self action:@selector(selectorUnderlineButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];

    button = [UIButton underlineButtonWithTitle:NSLocalizedString(@"PF_LGOIN_BUTTON_FBLOGIN", @"Login with facebook")
                                          Frame:getRectValue(TSViewIndexLogin, TSLoginFaceBookRect) Tag:TAG_LOGINFB_BUTTON];
    [button addTarget:self action:@selector(selectorUnderlineButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];
}

- (void)removeAccountButton {
    UIView *underlineButton = [self viewWithTag:TAG_FORGOT_BUTTON];
    [underlineButton removeFromSuperview];
    
    underlineButton = [self viewWithTag:TAG_SIGNIN_BUTTON];
    [underlineButton removeFromSuperview];
    
    underlineButton = [self viewWithTag:TAG_LOGINFB_BUTTON];
    [underlineButton removeFromSuperview];
}

- (void)restoreLoginMode {
    [self.idTextField removeFromSuperview];
    self.idTextField = nil;
    [self.loginButton removeFromSuperview];
    self.loginButton = nil;
    [self.forgotNoticeLabel removeFromSuperview];
    self.forgotNoticeLabel = nil;

    [self addLoginTextFiled];
    [self addLoginButton];
    [self addAccountButtons];
    
    if (self.firstRun == YES) {
        self.closeButton.hidden = YES;
        if (self.skipTextButton) {
            self.skipTextButton.hidden = NO;
        }
        if (self.skipImageButton) {
            self.skipImageButton.hidden = NO;
        }
    }
    self.emailStringReady = NO;
    self.passwordStringReady = NO;
    self.forgotMode = NO;
}

- (IBAction)selectorUnderlineButton:(id)sender {
    UIButton *button = (UIButton*)sender;
    switch (button.tag) {
        case TAG_FORGOT_BUTTON: {
            [self.pwTextField removeFromSuperview];
            self.pwTextField = nil;
            [self.loginButton removeFromSuperview];
            self.loginButton = nil;
            
            self.idTextField.frame = getRectValue(TSViewIndexLogin, TSLoginPWRect);

            self.forgotNoticeLabel = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexLogin, TSLoginForgetLabelRect)];
            self.forgotNoticeLabel.text = NSLocalizedString(@"PF_FORGOT_NOTICE_FORGOT_PASSWORD",
                                                            @"Please enter the email address used as the ID during sign up.\nA temporary password will be sent to the address.");
            self.forgotNoticeLabel.font = [UIFont fontWithName:kDefaultFontName size:14];
            self.forgotNoticeLabel.textColor = RGBH(@"#4f2c00");
            self.forgotNoticeLabel.numberOfLines = 0;
            self.forgotNoticeLabel.textAlignment = NSTextAlignmentCenter;
            [self addSubview:self.forgotNoticeLabel];
            
            self.loginButton = [UIButton buttonWithType:TSDolsTypeOK Frame:getRectValue(TSViewIndexLogin, TSLoginButtonRect)
                                                  Title:NSLocalizedString(@"PF_FORGOT_BUTTON_TITLE_SEND", @"Send")];
            [self.loginButton addTarget:self action:@selector(selectorSendButton:) forControlEvents:UIControlEventTouchUpInside];
            self.loginButton.enabled = NO;
            [self addSubview:self.loginButton];

            [self removeAccountButton];
            self.emailStringReady = YES;
            self.forgotMode = YES;
            if (self.firstRun == YES) {
                self.closeButton.hidden = NO;
                if (self.skipTextButton) {
                    self.skipTextButton.hidden = YES;
                }
                if (self.skipImageButton) {
                    self.skipImageButton.hidden = YES;
                }
            }
        } break;
        case TAG_SIGNIN_BUTTON: {
            if (self.delegate && [self.delegate respondsToSelector:@selector(view:Page:userData:)]) {
                [self.delegate view:self Page:TSViewIndexSignin userData:nil];
            }
        } break;
        case TAG_LOGINFB_BUTTON: {
            [self loginForFacebook];
        } break;
    }
}

#pragma mark - skip operation
- (void)setFirstRun:(BOOL)firstRun {
    _firstRun = firstRun;
    if (self.firstRun == YES) {
        [self addSkipControl];
    }
}

- (void)addSkipControl {
    self.skipTextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.skipTextButton.frame = getRectValue(TSViewIndexLogin, TSLoginSkipLabelRect);
    [self.skipTextButton setTitle:NSLocalizedString(@"PF_LOGIN_BUTTON_TITLE_SKIP", @"Skip") forState:UIControlStateNormal];
    [self.skipTextButton setTitleColor:RGBH(@"#ffffff") forState:UIControlStateNormal];
    [self.skipTextButton setTitle:NSLocalizedString(@"PF_LOGIN_BUTTON_TITLE_SKIP", @"Skip") forState:UIControlStateHighlighted];
    [self.skipTextButton setTitleColor:RGBH(@"#104666") forState:UIControlStateHighlighted];
    self.skipTextButton.titleLabel.font = [UIFont fontWithName:kDefaultFontName size:16];
    [self.skipTextButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    [self.skipTextButton addTarget:self action:@selector(selectorSkipButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.skipTextButton];
    
    self.skipImageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.skipImageButton.frame = getRectValue(TSViewIndexLogin, TSLoginSKipButtonRect);
    [self.skipImageButton setImage:[UIImage imageNamedEx:@"btn_skip_normal.png" ignore:YES] forState:UIControlStateNormal];
    [self.skipImageButton setImage:[UIImage imageNamedEx:@"btn_skip_press.png" ignore:YES] forState:UIControlStateHighlighted];
    [self.skipImageButton addTarget:self action:@selector(selectorSkipButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.skipImageButton];
}

- (IBAction)selectorSkipButton:(id)sender {
    NSLog(@"selectorSkipButton");
    if (self.delegate && [self.delegate respondsToSelector:@selector(closeViewControllerWithView:)]) {
        [self.delegate closeViewControllerWithView:self];
    }
}

#pragma mark - TSTextFiledDelegate
- (void)tstextFieldDidEndEditing:(TSTextField *)textField {
    NSInteger tag = textField.tag;
    switch (tag) {
        case TAG_ID_TEXTFILED: {
            self.emailStringReady = [self.idTextField checkEmailStringValidationWithShowError:
                                     NSLocalizedString(@"PF_NET_ERROR_EMAIL_ENTER_FULL_ADDRESS", @"Please enter the full email address,\nincluding after the @ sign")];
        } break;
        case TAG_PW_TEXTFILED: {
            self.passwordStringReady = [self.pwTextField checkPasswordStringValidationWithShowError:
                                        NSLocalizedString(@"PF_NET_ERROR__PASSWORD_SHORT", @"￼Please enter a password with at least 6 characters")];
        } break;
        default: { } break;
    }
}

- (BOOL)tstextField:(TSTextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    switch (textField.tag) {
        case TAG_ID_TEXTFILED: {
            self.emailStringReady = [self.idTextField checkEmailStringValidationWithShowError:nil];
        } break;
        case TAG_PW_TEXTFILED: {
            if ([self.pwTextField.text length] >= 6) {
                self.passwordStringReady = YES;
            } else {
                self.passwordStringReady = NO;
            }
        } break;
        default: { } break;
    }
    return YES;
}

- (void)checkEnableLoginButton {
    if (self.forgotMode == NO) {
        if (self.emailStringReady == YES && self.passwordStringReady == YES) {
            self.loginButton.enabled = YES;
        } else {
            self.loginButton.enabled = NO;
        }
    } else {
        if ( self.emailStringReady == YES ) {
            self.loginButton.enabled = YES;
        } else {
            self.loginButton.enabled = NO;
        }
    }
}

- (void)setEmailStringReady:(BOOL)emailStringReady {
    _emailStringReady = emailStringReady;
    [self checkEnableLoginButton];
}

- (void)setPasswordStringReady:(BOOL)passwordStringReady {
    _passwordStringReady = passwordStringReady;
    [self checkEnableLoginButton];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark - Facebook API
- (void)loginForFacebook {
    if ([[FBSession activeSession] isOpen]) {
        [self loginCallback:YES];
    } else {
        NSArray *permissions = [[NSArray alloc] initWithObjects:
                                @"email", @"public_profile", @"publish_actions", //@"user_hometown", @"read_friendlists", @"uid", @"gender",
                                nil];
        
        // Attempt to open the session. If the session is not open, show the user the Facebook login UX
        [FBSession openActiveSessionWithReadPermissions:permissions allowLoginUI:true completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
            // Did something go wrong during login? I.e. did the user cancel?
//            NSLog(@"error : %@", [error localizedDescription]);
            if (status == FBSessionStateClosedLoginFailed || status == FBSessionStateClosed || status == FBSessionStateCreatedOpening) {
                
                // If so, just send them round the loop again
                [[FBSession activeSession] closeAndClearTokenInformation];
                [FBSession setActiveSession:nil];
                [self createNewSession];
                [self loginCallback:NO];
            }
            else {
                [self loginCallback:YES];
            }
        }];
    }

}


//#define _DEBUG_FACEBOOKLOGIN_
- (void)loginCallback:(BOOL)success {
    if (success) {
        // Start the facebook request
        [[FBRequest requestForMe]
         startWithCompletionHandler:
         ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *result, NSError *error)
         {
             BOOL loginFail = NO;
             if (!error && result) {
                 NSLog(@"FBGraphUser : %@", result);
                 NSString *email = [result objectForKey:@"email"];
                 if ([email length] > 0) {

#ifdef _DEBUG_FACEBOOKLOGIN_
//                     email = "m1@nemustech.com";
//                     "first_name" = MOne;
//                     gender = female;
//                     id = 1491932594379834;
//                     "last_name" = Nemustech;
//                     link = "https://www.facebook.com/app_scoped_user_id/1491932594379834/";
//                     locale = "ko_KR";
//                     name = "MOne Nemustech";
//                     timezone = 9;
//                     "updated_time" = "2014-08-31T06:23:41+0000";
//                     verified = 0;
                     NSDictionary *dummyFacebook =
                     @{@"email" : @"dummyfb0003@abc.com",
                       @"gender" : @"female",
                       @"id" : @"1491932594370003",//1491932594379834
                       @"name" : @"dummyfb0003" };
                     self.loginForFacebookData = [self facebookLoginData:dummyFacebook];
#else
                     self.loginForFacebookData = [self facebookLoginData:result];
#endif
                     self.net =
#ifdef TSNET_REAL_SNSID_ENABLE
                     [[TSNetSNSLogin alloc] initWithSNSID:[self.loginForFacebookData objectForKey:UD_STRING_SNSID]];
#else
                     [[TSNetSNSLogin alloc] initWithSNSID:[self.loginForFacebookData objectForKey:UD_STRING_EMAIL]];
#endif
                     self.net.delegate = self;
                     self.net.tag = TAG_NET_SNSLOGIN;
                     [self.net start];
                 } else {
                     loginFail = YES;
                 }
             }
             else {
                 NSLog(@"requestForMe error : %@", error);
                 loginFail = YES;
             }
             if (loginFail == YES) {
                 NSHTTPCookie *cookie;
                 NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
                 for (cookie in [storage cookies])
                 {
                     NSString* domainName = [cookie domain];
                     NSRange domainRange = [domainName rangeOfString:@"facebook"];
                     if(domainRange.length > 0)
                     {
                         [storage deleteCookie:cookie];
                     }
                 }
                 
                 [[FBSession activeSession] closeAndClearTokenInformation];
                 [FBSession setActiveSession:nil];
                 [self createNewSession];

                 TSMessageView *messageView =
                 [[TSMessageView alloc] initWithMessage:NSLocalizedString(@"PF_LOGIN_POPUP_ERROR_FBLOGIN",
                                                                          @"Facebook Log in failed.\nPlease check your\nFacebook account.")
                                                   icon:TSPopupIconTypeWarning];
                 messageView.buttonLayout = TSPopupButtonLayoutOK;
                 [messageView showInView:self];
             }
         }];
    } else {
        // TODO:handle error
        TSMessageView *messageView = [[TSMessageView alloc] initWithMessage:NSLocalizedString(@"PF_LOGIN_POPUP_ERROR_FBLOGIN",
                                                                                              @"Facebook Log in failed.\nPlease check your\nFacebook account.")
                                                                       icon:TSPopupIconTypeWarning];
        messageView.buttonLayout = TSPopupButtonLayoutOK;
        [messageView showInView:self];
    }
}

#define _SAVE_TO_DICTIONARY_
- (NSDictionary*)facebookLoginData:(NSDictionary*)result {
#ifdef _SAVE_TO_DICTIONARY_
    NSLog(@"result : %@", result);
    NSInteger genderNumber = 99;
//    NSString *gender = [result objectForKey:@"gender"];
//    if ([gender isEqualToString:@"male"] == YES) {
//        genderNumber = 1;
//    } else if ([gender isEqualToString:@"female"] == YES) {
//        genderNumber = 2;
//    }

    return
    @{UD_BOOL_FACEBOOKLOGIN : [NSNumber numberWithBool:YES],
      UD_STRING_LOGINID     : [result objectForKey:@"email"],
      UD_STRING_EMAIL       : [result objectForKey:@"email"],
      UD_INTEGER_GENDER     : [NSNumber numberWithInteger:genderNumber],
      UD_STRING_SNSID       : [result objectForKey:@"id"],
      UD_STRING_SNSNAME     : [result objectForKey:@"name"] };
    
//    
//    if (self.delegate && [self.delegate respondsToSelector:@selector(setfacebookLoginData:)]) {
//        [self.delegate setfacebookLoginData:facebookData];
//    }
#else
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:[NSNumber numberWithBool:YES] forKey:UD_BOOL_FACEBOOKLOGIN];
    NSString *email = [result objectForKey:@"email"];
    NSString *userName = [result objectForKey:@"name"];
    [ud setObject:email forKey:UD_STRING_LOGINID];
    [ud setObject:email forKey:UD_STRING_EMAIL];
    NSString *gender = [result objectForKey:@"gender"];
    if ([gender isEqualToString:@"male"] == NO) {
        [ud setObject:[NSNumber numberWithInteger:2] forKey:UD_INTEGER_GENDER];
    } else {
        [ud setObject:[NSNumber numberWithInteger:1] forKey:UD_INTEGER_GENDER];
    }
    NSString *idString = [result objectForKey:@"id"];
    [ud setObject:idString forKey:UD_STRING_SNSID];
    [ud setObject:userName forKey:UD_STRING_SNSNAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
    return nil;
#endif
}

- (void)saveFacebookLoginInfo {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:[NSNumber numberWithBool:YES] forKey:UD_BOOL_FACEBOOKLOGIN];
    NSString *email = [self.loginForFacebookData objectForKey:UD_STRING_LOGINID];
    NSString *userName = [self.loginForFacebookData objectForKey:UD_STRING_SNSNAME];
    [ud setObject:email forKey:UD_STRING_LOGINID];
    [ud setObject:email forKey:UD_STRING_EMAIL];
    [ud setObject:[NSNumber numberWithInteger:[[self.loginForFacebookData objectForKey:UD_INTEGER_GENDER] integerValue]] forKey:UD_INTEGER_GENDER];
    NSString *idString = [self.loginForFacebookData objectForKey:UD_STRING_SNSID];
    [ud setObject:idString forKey:UD_STRING_SNSID];
    [ud setObject:userName forKey:UD_STRING_SNSNAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)createNewSession {
    FBSession* session = [[FBSession alloc] init];
    [FBSession setActiveSession: session];
}

- (void)postLoginNotification:(NSInteger)recallType {
    [[NSNotificationCenter defaultCenter] postNotificationName:kTSNotifivationLoginDidFinished object:nil userInfo:@{@"originlayer": [NSNumber numberWithInteger:self.originLayer],
                                                                                                                     @"recalltype": [NSNumber numberWithInteger:recallType]}];
}

- (void)showActivityInView:(BOOL)show {
    UIActivityIndicatorView *indicatorView = nil;
    if (show) {
        indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        CGRect frame = indicatorView.frame;
        frame.origin = CGCenterPointFromRect(CGRectMake(0, 0, self.frame.size.width, self.frame.size.height));
        indicatorView.tag = 999777;
        indicatorView.frame = frame;
        [self addSubview:indicatorView];
        [indicatorView startAnimating];
    } else {
        indicatorView = (UIActivityIndicatorView*)[self viewWithTag:999777];
        if (indicatorView) {
            [indicatorView stopAnimating];
            [indicatorView removeFromSuperview];
        }
    }
}


@end
