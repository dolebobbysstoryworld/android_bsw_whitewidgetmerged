//
//  TSRadioButton.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 2..
//
//

#import <UIKit/UIKit.h>

@class TSRadioButton;
@protocol TSRadioButtonDelegate <NSObject>

@optional
- (void)radioButton:(TSRadioButton*)radioButton didChangeSelectedStatus:(BOOL)selected;

@end

@interface TSRadioButton : UIControl//tuilise_todo : selected group 처리가 아직 안되어 있다. 설계를 고민해 보자.

@property (nonatomic, assign) id< TSRadioButtonDelegate > delegate;
- (void)setImage:(UIImage*)image forState:(UIControlState)state;
- (void)setTitle:(NSString *)string;

+ (TSRadioButton*)radioButtonWithTitle:(NSString *)string Frame:(CGRect)frame;

@end
