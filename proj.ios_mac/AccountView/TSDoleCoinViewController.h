//
//  TSDoleCoinViewController.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 2..
//
//

#import <UIKit/UIKit.h>
#import "ResolutionManager.h"
#import "TSViewController.h"

@interface TSDoleCoinViewController : TSViewController

@property (nonatomic, assign) TSOriginLayer originLayer;

@end
