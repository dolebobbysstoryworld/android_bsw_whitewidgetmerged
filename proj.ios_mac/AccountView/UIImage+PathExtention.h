//
//  UIImage+PathExtention.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 29..
//
//

#import <UIKit/UIKit.h>

@interface UIImage (PathExtention)

+ (UIImage *)imageNamedEx:(NSString *)name;
+ (UIImage *)imageNamedEx:(NSString *)name ignore:(BOOL)ignore;

@end
