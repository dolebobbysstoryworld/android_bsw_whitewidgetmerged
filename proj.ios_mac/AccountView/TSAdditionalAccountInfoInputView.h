//
//  TSAdditionalAccountInfoInputView.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 11. 28..
//
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, TSAccountDataType) {
    kTSAccountDataCountryCode,
    kTSAccountDataCity,
    kTSAccountDataGender,
    kTSAccountDataBirthDay,
};

@protocol TSAdditionalAccountInfoData <NSObject>

- (void)changedTargetData:(TSAccountDataType)dataType dataObject:(id)object;

@end

@interface TSAdditionalAccountInfoInputView : UIView

@property (nonatomic, readonly) BOOL isEdited;
@property (nonatomic, strong) NSString *countryCode;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, assign) NSInteger gender;
@property (nonatomic, strong) NSString *birthday;
@property (nonatomic, assign) id<TSAdditionalAccountInfoData> delegate;

- (instancetype)initWithFrame:(CGRect)frame countryCode:(NSString*)code;

@end
