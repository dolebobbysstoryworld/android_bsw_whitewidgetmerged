//
//  TSDoleCoinViewController.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 2..
//
//

#import "TSDoleCoinViewController.h"
#import "TSAccountDelegateProtocol.h"
#import "UIImage+PathExtention.h"
#import "TSDoleCoinView.h"
#import "../FacebookFriendListViewController.h"
#import "../QRCodeReaderViewController.h"
#import "TSAccountViewController.h"
#import "AppController.h"

@interface TSDoleCoinViewController () < TSAccountDelegate >

@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) UIImageView *lion;
@property (nonatomic, strong) UIImageView *racoon;
@property (nonatomic, strong) TSDoleCoinView *doleCoinView;

@end

@implementation TSDoleCoinViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillLayoutSubviews {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.view.superview.layer.masksToBounds = YES;
        
        if(!IS_IOS8) {
            self.view.superview.bounds = getRectValue(TSViewIndexShare, TSShareSuperViewControllerRect);
            self.view.frame = getRectValue(TSViewIndexShare, TSShareSuperViewControllerRect);
        }
        
        self.view.superview.backgroundColor = [UIColor clearColor];
        self.view.backgroundColor = [UIColor clearColor];
    }
    [super viewWillLayoutSubviews];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(notificationPopupShow:) name:tsNotificationPopupShow object:nil];
    [center addObserver:self selector:@selector(notificationPopupClose:) name:tsNotificationPopupClose object:nil];
    self.view.backgroundColor = [UIColor clearColor];
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"bg_map_full.png"]];
    backgroundView.frame = getRectValue(TSViewIndexShare, TSShareSubViewRect);
    [self.view addSubview:backgroundView];

    self.doleCoinView = [[TSDoleCoinView alloc] initWithFrame:getRectValue(TSViewIndexShare, TSShareSubViewRect)];
    self.doleCoinView.delegate = self;
    [self.view addSubview:self.doleCoinView];
    
    [self backgroundResourceLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.doleCoinView viewDidAppeared];
}

- (void)backgroundResourceLoad {
    if (self.closeButton) {//tuilise_todo : firstRun 일때는 생성하지 마라
        [self.view bringSubviewToFront:self.closeButton];
    } else {
        self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.closeButton setImage:[UIImage imageNamedEx:@"btn_cancel_normal.png" ignore:YES] forState:UIControlStateNormal];
        [self.closeButton setImage:[UIImage imageNamedEx:@"btn_cancel_press.png" ignore:YES] forState:UIControlStateHighlighted];
        [self.closeButton addTarget:self action:@selector(selectorCloseButton:) forControlEvents:UIControlEventTouchUpInside];
        self.closeButton.frame = getRectValue(TSViewIndexShare, TSShareCloseButtonRect);
        [self.view addSubview:self.closeButton];
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        ///PAD만 표시라 가변 위치 필요 없음
        if (!self.lion) {
            self.lion = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"full_popup_character_01.png"]];
            CGRect newFrame = CGRectMake((((2048-828)/2)-224+32)/2, (768-612)/2+(70+472+210)/2, 346/2, 472/2);//CGRectMake(3, 376, 148, 236);
            self.lion.frame = newFrame;
            [self.view addSubview:self.lion];
        } else {
            [self.view bringSubviewToFront:self.lion];
        }
        
        if (!self.racoon) {
            self.racoon = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"full_popup_character_02.png"]];
            self.racoon.frame = CGRectMake( ((2048-828)/2+(828-116)-32)/2 , (768-612)/2+70/2, 296/2, 472/2);//CGRectMake(414, 35, 148, 236);
            [self.view addSubview:self.racoon];
        } else {
            [self.view bringSubviewToFront:self.racoon];
        }
    }
}

- (IBAction)selectorCloseButton:(id)sender {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:tsNotificationPopupShow object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:tsNotificationPopupClose object:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)qrViewControllerWithView:(UIView *)view {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:tsNotificationPopupShow object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:tsNotificationPopupClose object:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
    UIViewController *viewController = nil;
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
    } else {
        viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
    }
//    NSLog(@"viewController screen frame : %@",NSStringFromCGRect(viewController.view.frame));
    QRCodeReaderViewController *qrCodeReaderViewContoller = [[QRCodeReaderViewController alloc] init];
    
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//        qrCodeReaderViewContoller.modalPresentationStyle = UIModalPresentationFormSheet;
//    }
    qrCodeReaderViewContoller.originLayer = self.originLayer;
    [viewController presentViewController:qrCodeReaderViewContoller animated:YES completion:nil];
}

- (void)facebookViewControllerWithView:(UIView *)view {
    [self dismissViewControllerAnimated:NO completion:nil];
    UIViewController *selfViewController = nil;
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        selfViewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
    } else {
        selfViewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
    }
    FacebookFriendListViewController *facebookFriendListViewController = [[FacebookFriendListViewController alloc] init];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        facebookFriendListViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    //    [selfViewController presentModalViewController:facebookFriendListViewController animated:YES];
    [selfViewController presentViewController:facebookFriendListViewController animated:YES completion:nil];
}

- (void)loginViewControllerWithView:(UIView *)view {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:tsNotificationPopupShow object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:tsNotificationPopupClose object:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
    UIViewController *viewController = nil;
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
    } else {
        viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
    }
    TSAccountViewController *accountViewController = [[TSAccountViewController alloc] initWithFirstRun:NO];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        accountViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    accountViewController.parentType = TSParentTypeDoleCoin;
    accountViewController.originLayer = self.originLayer;
    if(IS_IOS8 && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        accountViewController.preferredContentSize = CGSizeMake(1024, 768);
    }
    [viewController presentViewController:accountViewController animated:NO completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)view:(UIView*)view Page:(TSViewIndex)index userData:(id)object {
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)notificationPopupShow:(NSNotification *)notification {
    self.closeButton.hidden = YES;
}

- (void)notificationPopupClose:(NSNotification *)notification {
    self.closeButton.hidden = NO;
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (NSUInteger)application:(UIApplication*)application supportedInterfaceOrientationsForWindow:(UIWindow*)window {
    return UIInterfaceOrientationMaskPortrait;
}
//
//- (BOOL) shouldAutorotate {
//    return NO;
//}

@end
