//
//  TSAccountAgreementView.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 6. 2..
//
//

#import <UIKit/UIKit.h>
#import "TSAccountDelegateProtocol.h"
#import "TSCheckBox.h"

@interface TSAccountAgreementView : UIView

- (id)initWithFrame:(CGRect)frame forAbout:(BOOL)about;

@property (nonatomic, assign) id<TSAccountDelegate> delegate;
@property (nonatomic, assign) TSCheckBox *agreeCheckBox;
@property (nonatomic, assign) BOOL forAboutMode;

@end
