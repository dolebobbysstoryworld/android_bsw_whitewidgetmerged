//
//  PromotionViewController.h
//  bobby
//
//  Created by oasis on 2014. 8. 18..
//
//
#ifndef MACOS
#import <UIKit/UIKit.h>

#define KEY_PROMOTION_CAN_SHOW_DATE @"promotioncanshow"


@interface PromotionViewController : UIViewController
- (id)initWithURL:(NSURL*)url;
@end
#endif
