//
//  CocosVideoRecoder.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 4. 17..
//
//

#import "CocosVideoRecoder.h"
#import "CCEAGLView.h"
#import "ios/AppController.h"
#import "ios/RootViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface CocosVideoRecoder ()

@property (nonatomic, assign) CCEAGLView *glView;
@property (nonatomic, assign) CGFloat contentScaleFactor;
@property (nonatomic, strong) NSString *fileName;
@property (nonatomic, assign) NSInteger fileIndex;

@end

@implementation CocosVideoRecoder

static CocosVideoRecoder *sharedRecoder = nil;

+ (CocosVideoRecoder *) sharedRecoder {
    @synchronized(self)     {
        if (!sharedRecoder) {
            sharedRecoder = [[CocosVideoRecoder alloc] init];
            AppController *controller = (AppController*)[UIApplication sharedApplication].delegate;
            sharedRecoder.glView = (CCEAGLView*)controller.viewController.view;
            NSLog(@"%@", sharedRecoder.glView.context);
        }
    }
    return sharedRecoder;
}

- (void)clearFolderAtPath:(NSString *)path withExtention:(NSString *)extention {
    NSArray *fileList = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
    NSString *imageFile = nil;
    for (imageFile in fileList) {
        if ([[imageFile pathExtension] isEqualToString:extention] == YES) {
            NSError *error = nil;
            [[NSFileManager defaultManager] removeItemAtPath:[path stringByAppendingPathComponent:imageFile] error:&error];
        }
    }
}


- (void)initialized:(CGFloat)scaleFactor {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    [self clearFolderAtPath:documentsDirectory withExtention:@"png"];
    
    self.contentScaleFactor = scaleFactor;
    self.fileName = [documentsDirectory stringByAppendingPathComponent:@"screenshot%03d.png"];
}

- (void)createFrameBuffer {
//    NSLog(@"createFrameBuffer");
    GLint backingWidth, backingHeight;
    
    // Bind the color renderbuffer used to render the OpenGL ES view
    // If your application only creates a single color renderbuffer which is already bound at this point,
    // this call is redundant, but it is needed if you're dealing with multiple renderbuffers.
    // Note, replace "_colorRenderbuffer" with the actual name of the renderbuffer object defined in your class.
    // In Cocos2D the render-buffer is already binded (and it's a private property...).
    //  glBindRenderbufferOES(GL_RENDERBUFFER_OES, _colorRenderbuffer);
    
    // Get the size of the backing CAEAGLLayer
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
    
    NSInteger x = 0, y = 0, width = backingWidth, height = backingHeight;
    NSInteger dataLength = width * height * 4;
    GLubyte *data = (GLubyte*)malloc(dataLength * sizeof(GLubyte));
    
    // Read pixel data from the framebuffer
    glPixelStorei(GL_PACK_ALIGNMENT, 4);
    glReadPixels(x, y, width, height, GL_RGBA, GL_UNSIGNED_BYTE, data);
    
    // Create a CGImage with the pixel data
    // If your OpenGL ES content is opaque, use kCGImageAlphaNoneSkipLast to ignore the alpha channel
    // otherwise, use kCGImageAlphaPremultipliedLast
    CGDataProviderRef ref = CGDataProviderCreateWithData(NULL, data, dataLength, NULL);
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    CGImageRef iref = CGImageCreate (
                                     width,
                                     height,
                                     8,
                                     32,
                                     width * 4,
                                     colorspace,
                                     // Fix from Apple implementation
                                     // (was: kCGBitmapByteOrder32Big | kCGImageAlphaPremultipliedLast).
                                     kCGBitmapByteOrderDefault,
                                     ref,
                                     NULL,
                                     true,
                                     kCGRenderingIntentDefault
                                     );
    
    // OpenGL ES measures data in PIXELS
    // Create a graphics context with the target size measured in POINTS
    NSInteger widthInPoints, heightInPoints;
    if (NULL != UIGraphicsBeginImageContextWithOptions)
    {
        // On iOS 4 and later, use UIGraphicsBeginImageContextWithOptions to take the scale into consideration
        // Set the scale parameter to your OpenGL ES view's contentScaleFactor
        // so that you get a high-resolution snapshot when its value is greater than 1.0
        CGFloat scale = self.contentScaleFactor;
        widthInPoints = width / scale;
        heightInPoints = height / scale;
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(widthInPoints, heightInPoints), NO, scale);
    }
    else {
        // On iOS prior to 4, fall back to use UIGraphicsBeginImageContext
        widthInPoints = width;
        heightInPoints = height;
        UIGraphicsBeginImageContext(CGSizeMake(widthInPoints, heightInPoints));
    }
    
    CGContextRef cgcontext = UIGraphicsGetCurrentContext();
    
    // UIKit coordinate system is upside down to GL/Quartz coordinate system
    // Flip the CGImage by rendering it to the flipped bitmap context
    // The size of the destination area is measured in POINTS
    CGContextSetBlendMode(cgcontext, kCGBlendModeCopy);
    CGContextDrawImage(cgcontext, CGRectMake(0.0, 0.0, widthInPoints, heightInPoints), iref);
    
    // Retrieve the UIImage from the current context
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    // Clean up
    free(data);
    CFRelease(ref);
    CFRelease(colorspace);
    CGImageRelease(iref);
    
    NSData *imageData = UIImagePNGRepresentation(image);
    [imageData writeToFile:[NSString stringWithFormat:self.fileName, self.fileIndex] atomically:YES];
    self.fileIndex++;
//    [delegate receivedScreenshot:image]; // Varningen är inga problem
}

//static int imageindex = 0;
- (void)saveFrameImage {
    NSLog(@"saveFrameImage");
//    UIImage *image = [self drawableToCGImage];
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"screenshot%03d.png", imageindex]];
//    
//    // Save image.
//    [UIImagePNGRepresentation(image) writeToFile:filePath atomically:YES];
}

- (void)startRecoding {
    NSLog(@"startRecoding");
    self.fileIndex = 0;
}

- (void)pauseRecoding {
    NSLog(@"pauseRecoding");
}

- (void)stopRecoding {
    NSLog(@"stopRecoding");
    self.fileIndex = 0;
}

@end

/*
#if OSG_GLES1_FEATURES
#import <OpenGLES/ES1/glext.h>
#else
#import <OpenGLES/ES2/glext.h>
// in GLES2, the OES suffix if dropped from function names (from rti)
#define glGenFramebuffersOES glGenFramebuffers
#define glGenRenderbuffersOES glGenRenderbuffers
#define glBindFramebufferOES glBindFramebuffer
#define glBindRenderbufferOES glBindRenderbuffer
#define glFramebufferRenderbufferOES glFramebufferRenderbuffer
#define glGetRenderbufferParameterivOES glGetRenderbufferParameteriv
#define glRenderbufferStorageOES glRenderbufferStorage
#define glDeleteRenderbuffersOES glDeleteRenderbuffers
#define glDeleteFramebuffersOES glDeleteFramebuffers
#define glCheckFramebufferStatusOES glCheckFramebufferStatus

#define GL_FRAMEBUFFER_OES GL_FRAMEBUFFER
#define GL_RENDERBUFFER_OES GL_RENDERBUFFER
#define GL_RENDERBUFFER_WIDTH_OES GL_RENDERBUFFER_WIDTH
#define GL_RENDERBUFFER_HEIGHT_OES GL_RENDERBUFFER_HEIGHT
#define GL_COLOR_ATTACHMENT0_OES GL_COLOR_ATTACHMENT0
#define GL_DEPTH_ATTACHMENT_OES GL_DEPTH_ATTACHMENT
#define GL_DEPTH_COMPONENT16_OES GL_DEPTH_COMPONENT16
#define GL_STENCIL_INDEX8_OES GL_STENCIL_INDEX8
#define GL_FRAMEBUFFER_COMPLETE_OES GL_FRAMEBUFFER_COMPLETE
#define GL_STENCIL_ATTACHMENT_OES GL_STENCIL_ATTACHMENT

#define GL_RGB5_A1_OES GL_RGB5_A1
#endif

-(UIImage *) drawableToCGImage {
    GLint backingWidth2, backingHeight2;
    //Bind the color renderbuffer used to render the OpenGL ES view
    // If your application only creates a single color renderbuffer which is already bound at this point,
    // this call is redundant, but it is needed if you're dealing with multiple renderbuffers.
    // Note, replace "_colorRenderbuffer" with the actual name of the renderbuffer object defined in your class.
    glBindRenderbufferOES(GL_RENDERBUFFER_OES, viewRenderbuffer);
    
    // Get the size of the backing CAEAGLLayer
    glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_WIDTH_OES, &backingWidth2);
    glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_HEIGHT_OES, &backingHeight2);
    
    NSInteger x = 0, y = 0, width2 = backingWidth2, height2 = backingHeight2;
    NSInteger dataLength = width2 * height2 * 4;
    GLubyte *data = (GLubyte*)malloc(dataLength * sizeof(GLubyte));
    
    // Read pixel data from the framebuffer
    glPixelStorei(GL_PACK_ALIGNMENT, 4);
    glReadPixels(x, y, width2, height2, GL_RGBA, GL_UNSIGNED_BYTE, data);
    
    // Create a CGImage with the pixel data
    // If your OpenGL ES content is opaque, use kCGImageAlphaNoneSkipLast to ignore the alpha channel
    // otherwise, use kCGImageAlphaPremultipliedLast
    CGDataProviderRef ref = CGDataProviderCreateWithData(NULL, data, dataLength, NULL);
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    CGImageRef iref = CGImageCreate(width2, height2, 8, 32, width2 * 4, colorspace, kCGBitmapByteOrder32Big | kCGImageAlphaPremultipliedLast,
                                    ref, NULL, true, kCGRenderingIntentDefault);
    
    // OpenGL ES measures data in PIXELS
    // Create a graphics context with the target size measured in POINTS
    NSInteger widthInPoints, heightInPoints;
    if (NULL != UIGraphicsBeginImageContextWithOptions) {
        // On iOS 4 and later, use UIGraphicsBeginImageContextWithOptions to take the scale into consideration
        // Set the scale parameter to your OpenGL ES view's contentScaleFactor
        // so that you get a high-resolution snapshot when its value is greater than 1.0
        CGFloat scale = self.contentScaleFactor;
        widthInPoints = width2 / scale;
        heightInPoints = height2 / scale;
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(widthInPoints, heightInPoints), NO, scale);
    }
    else {
        // On iOS prior to 4, fall back to use UIGraphicsBeginImageContext
        widthInPoints = width2;
        heightInPoints = height2;
        UIGraphicsBeginImageContext(CGSizeMake(widthInPoints, heightInPoints));
    }
    
    CGContextRef cgcontext = UIGraphicsGetCurrentContext();
    
    // UIKit coordinate system is upside down to GL/Quartz coordinate system
    // Flip the CGImage by rendering it to the flipped bitmap context
    // The size of the destination area is measured in POINTS
    CGContextSetBlendMode(cgcontext, kCGBlendModeCopy);
    CGContextDrawImage(cgcontext, CGRectMake(0.0, 0.0, widthInPoints, heightInPoints), iref);
    
    // Retrieve the UIImage from the current context
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    // Clean up
    free(data);
    CFRelease(ref);
    CFRelease(colorspace);
    CGImageRelease(iref);
    
    return image;
}

- (UIImage *)flipImageVertically:(UIImage *)originalImage {
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:originalImage];
    UIGraphicsBeginImageContext(tempImageView.frame.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGAffineTransform flipVertical = CGAffineTransformMake(
                                                           1, 0, 0, -1, 0, tempImageView.frame.size.height
                                                           );
    CGContextConcatCTM(context, flipVertical);
    
    [tempImageView.layer renderInContext:context];
    
    UIImage *flippedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //[tempImageView release];
    
    return flippedImage;
}
*/