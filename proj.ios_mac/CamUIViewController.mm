//
//  CamUIViewController.m
//  bobby
//
//  Created by oasis on 2014. 4. 14..
//
//

#ifndef MACOS

#import "CamUIViewController.h"
#include "CustomCameraScene.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "ImagePickerLandscapeController.h"
#import "UIImage+PathExtention.h"
#import "AppController.h"

#include "Define.h"

#import "TSGuideforCreateCharacterView.h"
#import "TSGuideForCamera.h"

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_4INCH (SYSTEM_VERSION_LESS_THAN(@"8.0")?[UIScreen mainScreen].bounds.size.height == 568:[UIScreen mainScreen].bounds.size.width == 568)
#define DOCUMENTS_FOLDER [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]
#define BG_ITEM_FOLDER_NAME @"bg"
#define FOLDER_MAIN @"main"
#define FOLDER_THUMB @"thumb"
#define FOLDER_BOOK_THUMB @"bookthumb"
#define BG_PHOTO_NUM_KEY "bg_photo_num_key"
#define DURATION_SHOW_GUIDE 1.8f

typedef enum buttons_type{
    BUTTONS_TYPE_DEFAULT = 0,
    BUTTONS_TYPE_RETAKE = 1,
    BUTTONS_TYPE_CANCEL = 2,
}buttons_type;

@interface CamUIViewController ()
//@property(nonatomic, strong) UIButton *recordButton;
//@property(nonatomic, strong) UIButton *cameraButton;
@property(nonatomic, strong) CamViewController *camViewController;
@property(nonatomic, strong) UIButton *stillButton;
@property(nonatomic, strong) UIButton *libraryButton;
@property(nonatomic, strong) UIButton *retakeButton;
@property(nonatomic, strong) UIButton *cancelButton;
@property(nonatomic, strong) UIButton *saveButton;
@property(nonatomic, strong) UIButton *backButton;

@property(nonatomic, strong) UIView *editView;
@property(nonatomic, strong) UIImageView *editImageView;

@property (nonatomic, strong) UIPopoverController *popover;
@property(nonatomic, strong) ImagePickerLandscapeController *imagePicker;
@property(nonatomic) CGFloat lastScale;
@property(nonatomic, strong) NSTimer *timer;
@end

@implementation CamUIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self setViewFrame];
    }
    return self;
}

- (void)orientationChanged
{
	[self.camViewController rotateCameraLayer];
}

- (void)dealloc
{
    self.camViewController = nil;
    if (self.timer != nil) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // Do any additional setup after loading the view, typically from a nib.
#ifdef __IPHONE_8_0
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
    } else {
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.height, self.view.frame.size.width);
    }
#endif
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orientationChanged)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
    
    self.camViewController = [[CamViewController alloc] init];
    
//    CGRect camFrame;
//    if (IS_IPAD) {
//        camFrame = CGRectMake((50+186+24)/2, 194/2, 1528/2, 1148/2);
//    } else {
//        camFrame = CGRectMake((20+90+22)/2, 74/2, self.view.frame.size.height - (((22+90+20)*2)/2), 492/2);
//    }
//    self.camViewController.view.frame = camFrame;
    
    self.camViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width);
    self.camViewController.camDelegate = self;
    [self.view addSubview:self.camViewController.view];
    
    CGRect screenFrame = [[UIScreen mainScreen] bounds];
    //    NSString *imgPath = nil;
    //    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
    //        imgPath = @"common//pad_cam_layer_img.png";
    //    } else {
    //        imgPath = @"common//phone_cam_layer_img.png";
    //    }
    //    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgPath]];
    //    imgView.frame = self.camViewController.view.frame;
    //    [self.view addSubview:imgView];
    
    //    self.recordButton = [[UIButton alloc] initWithFrame:CGRectMake(100, 84, 70, 15)];
    //    [self.recordButton setTitle:@"Record" forState:UIControlStateNormal];
    //    [self.recordButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //    [self.recordButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    //    self.recordButton.titleLabel.font = [UIFont systemFontOfSize:10];
    //    [self.recordButton addTarget:self action:@selector(toggleMovieRecording:) forControlEvents:UIControlEventTouchUpInside];
    //    [self.view addSubview:self.recordButton];
    
    self.backButton = [self getButton:@"btn_map_back_normal"
                           pressedName:@"btn_map_back_press"
                              iconName:nil
                               padRect:CGRectMake(self.view.frame.size.height-(self.view.frame.size.height-(50/2)), 40/2, 186/2, 186/2)
                             phoneRect:CGRectMake(self.view.frame.size.height-(self.view.frame.size.height-(20/2)-(90/2)), 18/2, 90/2, 90/2)
                            buttonText:nil textColor:nil];
    [self.backButton addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.backButton];
    
    self.stillButton = [self getButton:@"camera_btn_camera_bg_normal"
                           pressedName:@"camera_btn_camera_bg_pressed"
                              iconName:@"camera_btn_camera_icon"
                               padRect:CGRectMake((50+186+24+1528+36)/2, 462/2, 188/2, 274/2)
                             phoneRect:CGRectMake(self.view.frame.size.height-(20/2), 174/2, 90/2, 132/2)
                            buttonText:nil textColor:nil];
    [self.stillButton addTarget:self action:@selector(snapStillImage:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.stillButton];
    self.stillButton.enabled = NO;
    
    self.libraryButton = [self getButton:@"camera_btn_import_bg_normal"
                             pressedName:@"camera_btn_import_bg_pressed"
                                iconName:@"camera_btn_import_icon"
                                 padRect:CGRectMake((50+186+24+1528+36)/2, (462+274+68)/2, 188/2, 274/2)
                               phoneRect:CGRectMake(self.view.frame.size.height-(20/2), (174+132+30)/2, 90/2, 132/2)
                              buttonText:nil textColor:nil];
    [self.libraryButton addTarget:self action:@selector(photoLibrary:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.libraryButton];
    
    self.retakeButton = [self getButton:@"camera_btn_retake_bg_normal"
                            pressedName:@"camera_btn_retake_bg_pressed"
                               iconName:@"camera_btn_retake_icon"
                                padRect:CGRectMake((50+186+24+1528+36)/2, 462/2, 188/2, 274/2)
                              phoneRect:CGRectMake(self.view.frame.size.height-(20/2), 174/2, 90/2, 132/2)
                             buttonText:NSLocalizedString(@"PF_CREATECHAR_BUTTON_TITLE_RETAKE", @"Retake")
                              textColor:[UIColor colorWithRed:0/255.0f green:96.0f/255.0f blue:97.0f/255.0f alpha:255.0f/255.0f]];
    [self.retakeButton addTarget:self action:@selector(retakeAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.retakeButton];
    
    
    self.cancelButton = [self getButton:@"camera_btn_cancel_bg_normal"
                            pressedName:@"camera_btn_cancel_bg_pressed"
                               iconName:@"camera_btn_cancel_icon"
                                padRect:CGRectMake((50+186+24+1528+36)/2, 462/2, 188/2, 274/2)
                              phoneRect:CGRectMake(self.view.frame.size.height-(20/2), 174/2, 90/2, 132/2)
                             buttonText:NSLocalizedString(@"PF_CREATECHAR_BUTTON_TITLE_CANCEL", @"Cancel")
                              textColor:[UIColor colorWithRed:178.0f/255.0f green:88.0f/255.0f blue:8.0f/255.0f alpha:255.0f/255.0f]];
    [self.cancelButton addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.cancelButton];
    
    self.saveButton = [self getButton:@"camera_btn_save_bg_normal"
                          pressedName:@"camera_btn_save_bg_pressed"
                             iconName:@"camera_btn_save_icon"
                              padRect:CGRectMake((50+186+24+1528+36)/2, (462+274+68)/2, 188/2, 274/2)
                            phoneRect:CGRectMake(self.view.frame.size.height-(20/2), (174+132+30)/2, 90/2, 132/2)
                           buttonText:NSLocalizedString(@"PF_CREATECHAR_BUTTON_TITLE_SAVE", @"Save")
                            textColor:[UIColor colorWithRed:138.0f/255.0f green:49.0f/255.0f blue:14.0f/255.0f alpha:255.0f/255.0f]];
    [self.saveButton addTarget:self action:@selector(saveAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.saveButton];
    
    [self.retakeButton setHidden:true];
    [self.cancelButton setHidden:true];
    [self.saveButton setHidden:true];
    
    
    //    self.cameraButton = [[UIButton alloc] initWithFrame:CGRectMake(screenFrame.size.height-150, screenFrame.size.width/4+100, 100, 30)];
    //    [self.cameraButton setTitle:@"change" forState:UIControlStateNormal];
    //    [self.cameraButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //    [self.cameraButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    //    self.cameraButton.titleLabel.font = [UIFont systemFontOfSize:18];
    //    [self.cameraButton addTarget:self action:@selector(changeCamera:) forControlEvents:UIControlEventTouchUpInside];
    //    [self.view addSubview:self.cameraButton];
}

-(UIButton*)getButton:(NSString *)normalImgName pressedName:(NSString *)pressedImgNamed  iconName:(NSString*)iconName padRect:(CGRect)padRect phoneRect:(CGRect)phoneRect buttonText:(NSString*)buttonText textColor:(UIColor*)textColor
{
    UIImage *normalImage = [UIImage imageNamedEx:[NSString stringWithFormat:@"%@.png", normalImgName] ignore:YES];
    UIImage *pressImage = [UIImage imageNamedEx:[NSString stringWithFormat:@"%@.png", pressedImgNamed] ignore:YES];
    UIImage *buttonIcon = nil;
    if(iconName != nil) {
        buttonIcon = [UIImage imageNamedEx:[NSString stringWithFormat:@"%@.png", iconName] ignore:YES];
    }
    int iconYforText = 0;
    CGRect buttonRect;
    CGRect labelRect;
    if (IS_IPAD) {
//        normalImage = [UIImage imageNamed:[NSString stringWithFormat:@"iosnative/%@_pad.png", normalImgName]];
//        pressImage = [UIImage imageNamed:[NSString stringWithFormat:@"iosnative/%@_pad.png", pressedImgNamed]];
        buttonRect = CGRectMake(padRect.origin.x, padRect.origin.y, normalImage.size.width, normalImage.size.height);
//        if (iconName != nil) {
//            buttonIcon = [UIImage imageNamed:[NSString stringWithFormat:@"iosnative/%@_pad.png", iconName]];
//        }
        if (buttonText != nil) {
            labelRect = CGRectMake(normalImage.size.width/2 - buttonIcon.size.width/2 - 8/2, (normalImage.size.height - (buttonIcon.size.height+(46/2)))/2+buttonIcon.size.height, buttonIcon.size.width + 8, 46/2);
            iconYforText = (46/2)/2;
        }
    } else {
//        normalImage = [UIImage imageNamed:[NSString stringWithFormat:@"iosnative/%@.png", normalImgName]];
//        pressImage = [UIImage imageNamed:[NSString stringWithFormat:@"iosnative/%@.png", pressedImgNamed]];
        buttonRect = CGRectMake(phoneRect.origin.x-normalImage.size.width, phoneRect.origin.y, normalImage.size.width, normalImage.size.height);
//        if (iconName != nil) {
//            buttonIcon = [UIImage imageNamed:[NSString stringWithFormat:@"iosnative/%@.png", iconName]];
//        }
        if (buttonText != nil) {
            labelRect = CGRectMake(normalImage.size.width/2 - buttonIcon.size.width/2 - 4/2, (normalImage.size.height - (buttonIcon.size.height+(24/2)))/2+buttonIcon.size.height, buttonIcon.size.width + 4, 24/2);
            iconYforText = (24/2)/2;
        }
    }
    
    UIButton *button = [[UIButton alloc] initWithFrame:buttonRect];
    [button setImage:normalImage forState:UIControlStateNormal];
    [button setImage:pressImage forState:UIControlStateHighlighted];
    
    if (iconName != nil) {
        UIImageView *iconView = [[UIImageView alloc] initWithImage:buttonIcon];
        iconView.frame = CGRectMake(button.frame.size.width/2 - buttonIcon.size.width/2, button.frame.size.height/2 - buttonIcon.size.height/2 - iconYforText, buttonIcon.size.width, buttonIcon.size.height);
        [button addSubview:iconView];
    }
    
    if (buttonText != nil) {
//        CGFloat fontSize = 0.0f;
//        if ([buttonText isEqualToString:NSLocalizedString(@"PF_CREATECHAR_BUTTON_TITLE_SAVE", @"Save")] == YES) {
//            NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
//            if ([language isEqualToString:@"ko"]) {
//                fontSize = (IS_IPAD)?36/2:16/2;
//            } else if ([language isEqualToString:@"ja"]) {
//                fontSize = (IS_IPAD)?36/2:16/2;
//            } else {
//                fontSize = (IS_IPAD)?42/2:20/2;
//            }
//        } else {
//            NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
//            if ([language isEqualToString:@"ko"]) {
//                fontSize = (IS_IPAD)?36/2:16/2;
//            } else if ([language isEqualToString:@"ja"]) {
//                fontSize = (IS_IPAD)?32/2:14/2;
//            } else {
//                fontSize = (IS_IPAD)?42/2:20/2;
//            }
//        }
    
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:buttonText];
        [str addAttribute:NSForegroundColorAttributeName value:textColor range:NSMakeRange(0,buttonText.length)];
        [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica-bold" size:[self getLabelFontSize:buttonText]] range:NSMakeRange(0,buttonText.length)];
//        NSShadow *shadow = [[NSShadow alloc] init];
//        [shadow setShadowColor:[UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:255*0.33/255.0f]];
//        [shadow setShadowOffset:CGSizeMake(0.0f, 0.5f)];
//        shadow.shadowBlurRadius = 0.0f;
//        [str addAttribute:NSShadowAttributeName value:shadow range:NSMakeRange(0,buttonText.length)];
        
        UILabel *buttonLabel = [[UILabel alloc] initWithFrame:labelRect];
        //        buttonLabel.text = buttonText;
        //        buttonLabel.textColor = textColor;
        //        buttonLabel.font = [UIFont fontWithName:@"Helvetica" size:(IS_IPAD)?42/2:20/2];
        buttonLabel.attributedText = str;
        [buttonLabel setTextAlignment:NSTextAlignmentCenter];
        [button addSubview:buttonLabel];
    }
    
    return button;
}

- (int)getLabelFontSize:(NSString *)buttonText
{
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    int fontSize;
    if ([language isEqualToString:@"ja"]) {
        if ([buttonText isEqualToString:NSLocalizedString(@"PF_CREATECHAR_BUTTON_TITLE_CANCEL", @"Cancel")]) {
            fontSize = (IS_IPAD)?(32-4)/2:(14)/2;
        } else {
            fontSize = (IS_IPAD)?36/2:16/2;
        }
    } else if ([language isEqualToString:@"ko"]) {
        fontSize = (IS_IPAD)?36/2:16/2;
    } else {
        fontSize = (IS_IPAD)?42/2:20/2;
    }
    return fontSize;
}

- (void)photoLibrary:(UIButton*)sender
{
    [self setViewFrame];
    if ([ImagePickerLandscapeController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]) {
        if(self.imagePicker == nil){
            self.imagePicker = [[ImagePickerLandscapeController alloc] init];
            self.imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
            self.imagePicker.delegate = self;
            self.imagePicker.allowsEditing = NO;
        }
        
        if (IS_IPAD) {
            CGRect libraryRect;
            libraryRect = CGRectMake((50+186+24+1528+36)/2 - 454, (462+274+68)/2 - 80, self.libraryButton.frame.size.width, self.libraryButton.frame.size.height);
//            libraryRect = CGRectMake(self.view.frame.size.height-(20/2)-self.libraryButton.frame.size.width, (174+132+30)/2, self.libraryButton.frame.size.width, self.libraryButton.frame.size.height);
            
            if (!self.popover) {
                self.popover = [[UIPopoverController alloc] initWithContentViewController:self.imagePicker];
            }
            if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                [self.popover presentPopoverFromRect:libraryRect inView:self.view permittedArrowDirections:NO animated:YES];
            } else {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self.popover presentPopoverFromRect:libraryRect inView:self.view permittedArrowDirections:NO animated:YES];
                }];
            }
            
        } else {
            self.backButton.enabled = false;
            UIViewController *sceneViewController = nil;
            if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                sceneViewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
                [sceneViewController presentViewController:self.imagePicker animated:YES completion:^{ self.backButton.enabled = true;}];
            } else {
                sceneViewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [sceneViewController presentViewController:self.imagePicker animated:YES completion:^{ self.backButton.enabled = true;}];
                }];
            }
        }
    } else {
        
        return;
    }
}

- (void)navigationController:(UINavigationController*)navigationController willShowViewController:(UIViewController*)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:true];
    if (IS_IPAD) {
        viewController.preferredContentSize = CGSizeMake(1528/2 + 40, 1148/2);
//        float systemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
//        if (systemVersion >= 7.0) {
//            viewController.preferredContentSize = CGSizeMake(1528/2 + 40, 1148/2);
//        } else {
//            viewController.contentSizeForViewInPopover = CGSizeMake(1528/2 + 40, 1148/2);
//        }
    }
}

- (void)closeAction:(id)sender
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    
    [self resetEditView];
    
    [self.camViewController stopRunningAction];
    
//    [self dismissViewControllerAnimated:NO completion:nil];
    NSLog(@"closeAction: ");
    
    cocos2d::Scene *cameraScene = cocos2d::Director::getInstance()->getRunningScene();
    if (!cameraScene) {
        return;
    }
    CustomCamera *cameraLayer = (CustomCamera*)cameraScene->getChildByTag(TAG_CAMERA);
    if (!cameraLayer) {
        NSLog(@"not camera layer");
        return;
    }
    
    for (UIView *subView in [self.view subviews]) {
        [subView removeFromSuperview];
    }
    
    NSLog(@"close action : ");
    cameraLayer->btnBackCallback();
    NSLog(@"cameraLayer callback ");
}

- (void)buttonControl:(buttons_type)type
{
    switch (type) {
        case BUTTONS_TYPE_DEFAULT:
        {
            [self.stillButton setHidden:false];
            [self.libraryButton setHidden:false];
            [self.retakeButton setHidden:true];
            [self.saveButton setHidden:true];
            [self.cancelButton setHidden:true];
            self.stillButton.enabled = true;
            self.libraryButton.enabled = true;
            self.retakeButton.enabled = false;
            self.saveButton.enabled = false;
            self.cancelButton.enabled = false;
            break;
        }
        case BUTTONS_TYPE_RETAKE:
        {
            [self.stillButton setHidden:true];
            [self.libraryButton setHidden:true];
            [self.retakeButton setHidden:false];
            self.retakeButton.enabled = false;
            [self.saveButton setHidden:false];
            [self.cancelButton setHidden:true];
            self.stillButton.enabled = false;
            self.libraryButton.enabled = false;
            self.saveButton.enabled = true;
            self.cancelButton.enabled = false;
            
            self.backButton.enabled = false;
            break;
        }
        case BUTTONS_TYPE_CANCEL:
        {
            [self.stillButton setHidden:true];
            [self.libraryButton setHidden:true];
            [self.retakeButton setHidden:true];
            [self.saveButton setHidden:false];
            [self.cancelButton setHidden:false];
            self.stillButton.enabled = false;
            self.libraryButton.enabled = false;
            self.retakeButton.enabled = false;
            self.saveButton.enabled = true;
            self.cancelButton.enabled = true;
            break;
        }
        default:
            break;
    }
}

- (void)snapStillImage:(UIButton*)sender
{
    [self photoEditView];
    
    [self buttonControl:BUTTONS_TYPE_RETAKE];
    
    [self.camViewController snapStillImage:sender];
}

- (void)changeCamera:(UIButton*)sender
{
    [self.camViewController changeCamera:sender];
}

- (void)retakeAction:(UIButton *)sender
{
    [self buttonControl:BUTTONS_TYPE_DEFAULT];
    [self resetEditView];
}

- (void)cancelAction:(UIButton *)sender
{
    [self buttonControl:BUTTONS_TYPE_DEFAULT];
    
    [self resetEditView];
    
    [self.camViewController rotateCameraLayer];
}

- (void)saveAction:(UIButton *)sender
{
    if(!self.editView || !self.editImageView || self.editImageView.image == nil) return;
    
    self.backButton.enabled = false;
    UIImage *resultImage = [self captureView];
    NSLog(@"resultImageSize : %@", NSStringFromCGSize(resultImage.size));
    
//    NSData *testPngData = UIImagePNGRepresentation(resultImage);
//    UIImage *testImage = [UIImage imageWithData:testPngData];
//    
//    [[[ALAssetsLibrary alloc] init] writeImageToSavedPhotosAlbum:[testImage CGImage] orientation:(ALAssetOrientation)[resultImage imageOrientation] completionBlock:nil];
    
    [self checkAndCreateFolder:BG_ITEM_FOLDER_NAME];
    
    auto userDefault = UserDefault::getInstance();
    auto  lastBgNum = userDefault->getIntegerForKey(BG_PHOTO_NUM_KEY, -1);
    unsigned long int bgPhotoNum = lastBgNum + 1;
    userDefault->setIntegerForKey(BG_PHOTO_NUM_KEY, (int)bgPhotoNum);
    
    CGSize mainCropSize;
    CGSize bookThumbSize;
    if (IS_IPAD) {
        mainCropSize = CGSizeMake(856, 638); //850,636
        bookThumbSize = CGSizeMake(1434, 1080);
    } else {
        if(IS_4INCH) {
            mainCropSize = CGSizeMake(472, 266); //472, 266
        } else {
            mainCropSize = CGSizeMake(796/2, 532/2); //796/2, 532/2
        }
        bookThumbSize = CGSizeMake(694, 526);
    }
    
    float retinaScale =([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale == 2.0))?2.0:1.0;
    CGSize thumbSize = (IS_IPAD)?CGSizeMake((368/2)*retinaScale,(276/2)*retinaScale):CGSizeMake(204, 115);
    
    NSString *fileName = [NSString stringWithFormat:@"b%i.png",(int)bgPhotoNum];
    BOOL mainPhotoSaved = [self savePhotoData:resultImage folder:FOLDER_MAIN withName:fileName cropSize:mainCropSize];
    BOOL bookThumbPhotoSaved = [self savePhotoData:resultImage folder:FOLDER_BOOK_THUMB withName:fileName cropSize:bookThumbSize];
    BOOL thumbPhotoSaved = [self savePhotoData:resultImage folder:FOLDER_THUMB withName:fileName cropSize:thumbSize];
    
    [self buttonControl:BUTTONS_TYPE_DEFAULT];

    [self resetEditView];
    
    [self.camViewController rotateCameraLayer];
    self.backButton.enabled = true;
    
    if(!(mainPhotoSaved && bookThumbPhotoSaved && thumbPhotoSaved)){
        NSLog(@"photo not saved");
        return;
    }
    
    cocos2d::Scene *cameraScene = cocos2d::Director::getInstance()->getRunningScene();
    if (!cameraScene) {
        return;
    }
    CustomCamera *cameraLayer = (CustomCamera*)cameraScene->getChildByTag(TAG_CAMERA);
    if (!cameraLayer) {
        NSLog(@"not camera layer");
        return;
    }
    
    NSString *bg = @"bg_01_custom";
    std::string bgStr = std::string([bg UTF8String]);
    std::string mainFile = std::string([fileName UTF8String]);
    std::string thumbFile = std::string([fileName UTF8String]);
    
    cameraLayer->customBgPhotoSaved(bgStr, mainFile, thumbFile);
    
    [self closeAction:nil];
}

- (BOOL)savePhotoData:(UIImage*)originImage folder:(NSString*)folder withName:(NSString*)fileName cropSize:(CGSize)cropSize
{
    NSString *photoPath = [NSString stringWithFormat:@"%@/%@/%@/%@",DOCUMENTS_FOLDER,BG_ITEM_FOLDER_NAME,folder,fileName];
    NSLog(@"photoPath : %@", photoPath);
    BOOL saveResult = false;
    if (![[NSFileManager defaultManager] fileExistsAtPath:photoPath]) {
        CGRect cropRect = CGRectMake((originImage.size.width-cropSize.width*(originImage.size.height/cropSize.height))/2, 0, cropSize.width*(originImage.size.height/cropSize.height), originImage.size.height);
        
        UIImage *croppedImage = [self cropImage:originImage withRect:cropRect];
        UIImage *resizeImage = [self resizeImage:croppedImage withSize:cropSize];
        NSData *photoData = UIImagePNGRepresentation(resizeImage);
        saveResult = [[NSFileManager defaultManager] createFileAtPath:photoPath contents:photoData attributes:nil];
    } else {
        NSLog(@"photoPath already exist!");
    }
    return saveResult;
}

- (UIImage*)resizeImage:(UIImage*)image withSize:(CGSize)size
{
    UIGraphicsBeginImageContext(CGSizeMake(size.width, size.height));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0.0, size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    CGContextDrawImage(context, CGRectMake(0.0, 0.0, size.width, size.height), [image CGImage]);
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return scaledImage;
}

- (UIImage*)cropImage:(UIImage*)img withRect:(CGRect)rect
{
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect drawRect = CGRectMake(-rect.origin.x, -rect.origin.y, img.size.width, img.size.height);
    CGContextClipToRect(context, CGRectMake(0, 0, rect.size.width, rect.size.height));
    [img drawInRect:drawRect];
    UIImage* resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resultImage;
}

- (void)checkAndCreateFolder:(NSString*)folderName
{
    NSString *path = [DOCUMENTS_FOLDER stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",folderName]];
    if(![[NSFileManager defaultManager] fileExistsAtPath:path]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:NO attributes:nil error:nil];
    }
    NSString *mainPath = [DOCUMENTS_FOLDER stringByAppendingString:[NSString stringWithFormat:@"/%@/%@",folderName,FOLDER_MAIN]];
    if(![[NSFileManager defaultManager] fileExistsAtPath:mainPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:mainPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    NSString *thumbPath = [DOCUMENTS_FOLDER stringByAppendingString:[NSString stringWithFormat:@"/%@/%@",folderName,FOLDER_THUMB]];
    if(![[NSFileManager defaultManager] fileExistsAtPath:thumbPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:thumbPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    NSString *bookThumbPath = [DOCUMENTS_FOLDER stringByAppendingString:[NSString stringWithFormat:@"/%@/%@",folderName,FOLDER_BOOK_THUMB]];
    if(![[NSFileManager defaultManager] fileExistsAtPath:bookThumbPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:bookThumbPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
}

- (int)getFileCountInDirectory:(NSString*)directory
{
    NSArray *fileList = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:directory error:nil];
    return [fileList count];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - CamViewControllerDelegte

- (void)setImageView:(UIImage *)image
{
    NSLog(@"image size : %f * %f", image.size.width, image.size.height);
    CGRect cropRect;
    if (IS_IPAD) {
        cropRect = CGRectMake((image.size.width-self.view.frame.size.width*(image.size.height/self.view.frame.size.height))/2, 0, self.editView.frame.size.width*(image.size.height/self.view.frame.size.height), image.size.height);
//        if ([UIScreen mainScreen].scale == 2.0f) {
//            cropRect = CGRectMake((1920-1437.491289)/2, 0, 1437.491289, 1080);
//        } else {
//            cropRect = CGRectMake((1280-958.327526)/2, 0, 958.327526, 720);
//        }
    } else {
        cropRect = CGRectMake(0, (image.size.height-self.view.frame.size.height*(image.size.width/self.view.frame.size.width))/2, image.size.width, self.editView.frame.size.height*(image.size.width/self.view.frame.size.width));
//        if(IS_4INCH){
//            cropRect = CGRectMake(0, 0, 1914.14634, 1080);
//        } else{
//            cropRect = CGRectMake(0, 0, 1527.8048, 1080);
//        }
    }
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    UIImage *cropImage = [UIImage imageWithCGImage:imageRef scale:1.0 orientation:image.imageOrientation];
    CGImageRelease(imageRef);

//    AVCaptureSessionPresetPhoto
//    CGRect cropRect;
//    if (IS_IPAD) {
//        cropRect = CGRectMake(0, 0, image.size.width, image.size.height);
//    } else {
//        cropRect = CGRectMake(0, (image.size.height-self.view.frame.size.height*(image.size.width/self.view.frame.size.width))/2, image.size.width, self.view.frame.size.height*(image.size.width/self.view.frame.size.width));
//    }
//    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
//    UIImage *cropImage = [UIImage imageWithCGImage:imageRef scale:1.0 orientation:image.imageOrientation];
//    CGImageRelease(imageRef);
    
    self.editImageView.image = cropImage;
    self.editImageView.contentMode = UIViewContentModeScaleToFill;
    [self setEditViewBackground];
    self.retakeButton.enabled = true;
    self.backButton.enabled = true;
    
    if (self.timer != nil) {
        [self.timer invalidate];
        self.timer = nil;
    }
    self.timer = [NSTimer scheduledTimerWithTimeInterval:DURATION_SHOW_GUIDE target:self selector:@selector(showGuide:) userInfo:@"showguide_camera_char_taskPhoto" repeats:NO];
}

- (void)showGuide:(NSTimer*)timer
{
#ifdef _GUIDE_ENABLE_
#ifndef _DEBUG_GUIDE_
    NSString *guideTypeKey = (NSString*)timer.userInfo;
    BOOL show = [[[NSUserDefaults standardUserDefaults] valueForKey:guideTypeKey] boolValue];
    if (show == false) {
        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:guideTypeKey];
#endif
        //        TSGuideforCreateCharacterView *guideView =
        //        [[TSGuideforCreateCharacterView alloc] initWithFrame:getRectValue(TSViewIndexGuide, TSGuideParentViewLandscape)
        //                                            transparentFrame:getRectValue(TSViewIndexGuide, TSGuideCreateCharecterAlpha)];
        TSGuideForCamera *guideView = [[TSGuideForCamera alloc] initWithFrame:getRectValue(TSViewIndexGuide, TSGuideParentViewLandscape)];
        [guideView showInView:self.view];
#ifndef _DEBUG_GUIDE_
    }
#endif
#endif
}

- (void)setEditViewBackground
{
    UIImage *background = [UIImage imageNamedEx:@"camera_frame_background_view.png" ignore:YES];
    //    if (IS_IPAD) {
    //        background = [UIImage imageNamed:@"iosnative/camera_frame_background_view_pad.png"];
    //    } else {
    //        background = [UIImage imageNamed:@"iosnative/camera_frame_background_view.png"];
    //    }
    UIColor *pattern = [UIColor colorWithPatternImage:background];
    self.editView.backgroundColor = pattern;
    self.editView.contentMode = UIViewContentModeScaleAspectFit;
}

- (void)setCameraButtonEnabled:(NSString *)enabled
{
    //    if ([enabled isEqualToString:@"YES"]) {
    //        [[self cameraButton] setEnabled:YES];
    //    } else {
    //        [[self cameraButton] setEnabled:NO];
    //    }
}

- (void)setStillButtonEnabled:(NSString *)enabled
{
    if ([enabled isEqualToString:@"YES"]) {
        [[self stillButton] setEnabled:YES];
    } else {
        [[self stillButton] setEnabled:NO];
    }
}

- (void)setViewFrame
{
    CGRect screenFrame = [[UIScreen mainScreen] bounds];
#ifdef __IPHONE_8_0
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        self.view.frame = CGRectMake(screenFrame.origin.x,screenFrame.origin.y, screenFrame.size.height, screenFrame.size.width);
    } else {
        self.view.frame = CGRectMake(screenFrame.origin.x,screenFrame.origin.y, screenFrame.size.width, screenFrame.size.height);
    }
#else
    self.view.frame = CGRectMake(screenFrame.origin.x,screenFrame.origin.y, screenFrame.size.height, screenFrame.size.width);
#endif
    
}

#pragma mark - UIImagePickerControllerDelegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self photoEditView];
    [self setEditViewBackground];
    
//    [self setViewFrame];
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    self.editImageView.image = chosenImage;

    [self buttonControl:BUTTONS_TYPE_CANCEL];
    
    if (IS_IPAD && picker.sourceType == UIImagePickerControllerSourceTypeSavedPhotosAlbum) {
        [self.popover dismissPopoverAnimated:YES];
        [self releaseImagePicker];
    } else {
        self.backButton.enabled = false;
        UIViewController *sceneViewController = nil;
        if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
            sceneViewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
        } else {
            sceneViewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
        }
        [sceneViewController dismissViewControllerAnimated:YES completion:^{
            self.backButton.enabled = true;
            [self releaseImagePicker];
        }];
    }
    if (self.timer != nil) {
        [self.timer invalidate];
        self.timer = nil;
    }
    self.timer = [NSTimer scheduledTimerWithTimeInterval:DURATION_SHOW_GUIDE target:self selector:@selector(showGuide:) userInfo:@"showguide_camera_char_fromAlbum" repeats:NO];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self setViewFrame];
    if (IS_IPAD && picker.sourceType == UIImagePickerControllerSourceTypeSavedPhotosAlbum) {
        [self.popover dismissPopoverAnimated:YES];
        [self releaseImagePicker];
    } else {
        self.backButton.enabled = false;
        UIViewController *sceneViewController = nil;
        if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
            sceneViewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
        } else {
            sceneViewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
        }
        [sceneViewController dismissViewControllerAnimated:YES completion:^{
            self.backButton.enabled = true;
            [self releaseImagePicker];
        }];
    }
}

-(void)releaseImagePicker
{
    if (!SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        self.imagePicker = nil;
    }
}

-(void)rotate:(UIRotationGestureRecognizer *)recognizer
{
    if ([recognizer state] == UIGestureRecognizerStateBegan || [recognizer state] == UIGestureRecognizerStateChanged) {
        self.editImageView.transform = CGAffineTransformRotate(self.editImageView.transform, recognizer.rotation);
        [recognizer setRotation:0];
    }
}

- (void)scale:(UIPinchGestureRecognizer *)recognizer
{
    if([recognizer state] == UIGestureRecognizerStateBegan) {
        // Reset the last scale, necessary if there are multiple objects with different scales
        self.lastScale = [recognizer scale];
    }
    
    if ([recognizer state] == UIGestureRecognizerStateBegan ||
        [recognizer state] == UIGestureRecognizerStateChanged) {
        
        CGFloat currentScale = [[[recognizer view].layer valueForKeyPath:@"transform.scale"] floatValue];
        
        // Constants to adjust the max/min values of zoom
        const CGFloat kMaxScale = 2.0;
        const CGFloat kMinScale = 0.5;
        
        CGFloat newScale = 1 -  (self.lastScale - [recognizer scale]);
        newScale = MIN(newScale, kMaxScale / currentScale);
        newScale = MAX(newScale, kMinScale / currentScale);
        CGAffineTransform transform = CGAffineTransformScale([[recognizer view] transform], newScale, newScale);
        [recognizer view].transform = transform;
        
        self.lastScale = [recognizer scale];  // Store the previous scale factor for the next pinch gesture call
    }
    
//    if ([recognizer state] == UIGestureRecognizerStateBegan || [recognizer state] == UIGestureRecognizerStateChanged) {
//        self.editImageView.transform = CGAffineTransformScale([self.editImageView transform], [recognizer scale], [recognizer scale]);
//        [recognizer setScale:1];
//    }

}

- (void)pan:(UIPanGestureRecognizer *)recognizer
{
    CGPoint translation = [recognizer translationInView:self.editView];
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,
                                         recognizer.view.center.y + translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.editView];
    
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint velocity = [recognizer velocityInView:self.editView];
        CGFloat magnitude = sqrtf((velocity.x * velocity.x) + (velocity.y * velocity.y));
        CGFloat slideMult = magnitude / 200;
        
        float slideFactor = 0.0 * slideMult; //0.1 * slideMult;
        CGPoint finalPoint = CGPointMake(recognizer.view.center.x + (velocity.x * slideFactor),
                                         recognizer.view.center.y + (velocity.y * slideFactor));
        finalPoint.x = MIN(MAX(finalPoint.x, 0), self.editView.bounds.size.width);
        finalPoint.y = MIN(MAX(finalPoint.y, 0), self.editView.bounds.size.height);
        
        [UIView animateWithDuration:slideFactor*2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            recognizer.view.center = finalPoint;
        } completion:nil];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return ![gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]];
}

- (UIImage *)captureView {
//    float imageScale = sqrtf(powf(self.editImageView.transform.a, 2.f) + powf(self.editImageView.transform.c, 2.f));
//    CGFloat widthScale = self.editImageView.bounds.size.width / self.editImageView.image.size.width;
//    CGFloat heightScale = self.editImageView.bounds.size.height / self.editImageView.image.size.height;
//    float contentScale = MIN(widthScale, heightScale);
//    float effectiveScale = imageScale * contentScale;
//    
//    CGSize captureSize = CGSizeMake(self.editView.bounds.size.width / effectiveScale, self.editView.bounds.size.height / effectiveScale);
//    
//    NSLog(@"effectiveScale = %0.2f, captureSize = %@", effectiveScale, NSStringFromCGSize(captureSize));
//    *[UIScreen mainScreen].scale
    UIGraphicsBeginImageContextWithOptions(CGSizeMake((self.editView.bounds.size.width*2), (self.editView.bounds.size.height*2)), NO, 0.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextScaleCTM(context, 2.0, 2.0);
    [self.editView.layer renderInContext:context];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

- (void)photoEditView
{
    self.editView = [[UIView alloc] initWithFrame:CGRectMake(0,0,self.camViewController.view.frame.size.width, self.camViewController.view.frame.size.height)];
    self.editView.clipsToBounds = YES;
    self.editView.backgroundColor = [UIColor clearColor];
    [self.camViewController.view addSubview:self.editView];
    
    self.editImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,self.camViewController.view.frame.size.width, self.camViewController.view.frame.size.height)];
    self.editImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.editImageView.userInteractionEnabled = YES;
    [self.editView addSubview:self.editImageView];
    
    UIRotationGestureRecognizer *rotationGesture = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotate:)];
    rotationGesture.delegate = self;
    [self.editView addGestureRecognizer:rotationGesture];
    
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scale:)];
    pinchGesture.delegate =self;
    [self.editImageView addGestureRecognizer:pinchGesture];
    
    UIPanGestureRecognizer *panGesuture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
    [self.editImageView addGestureRecognizer:panGesuture];
}

- (void)resetEditView
{
    if (self.editView != nil) {
        self.editImageView.image = nil;
        self.editView.backgroundColor = [UIColor clearColor];
        self.editView = nil;
        for (UIGestureRecognizer *recognizer in self.view.gestureRecognizers) {
            [self.view removeGestureRecognizer:recognizer];
        }
    }
}

@end
#endif