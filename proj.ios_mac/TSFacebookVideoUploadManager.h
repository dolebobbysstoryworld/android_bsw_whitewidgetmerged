//
//  TSFacebookVideoUploadManager.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 8. 6..
//
//

//#define __FBUPLODTESTMODE_

#import <Foundation/Foundation.h>

@interface TSFacebookVideoUploadManager : NSObject

+ (TSFacebookVideoUploadManager *)sharedObject;
+ (void)releaseSharedObject;
- (void)prepareShareOnFaceBook:(void *)map;
- (void)uploadFacebookVideo:(NSString *)path title:(NSString*)string comment:(NSString*)message targetUI:(void *)progress;

- (void)shareOnFacebook:(NSString *)path title:(NSString*)string comment:(NSString*)message targetUI:(void *)progress;

- (NSInteger)videolength;

@end
