//
//  EditPhotoViewController.h
//  bobby
//
//  Created by oasis on 2014. 6. 12..
//
//

#ifndef MACOS
#import <UIKit/UIKit.h>
@protocol EditPhotoViewControllerDelegate <NSObject>
- (void)photoEditingDone:(UIImage*)croppedImage picker:(UIImagePickerController*)picker;
@end

@interface EditPhotoViewController : UIViewController <UIScrollViewDelegate>
@property (nonatomic, strong) NSObject<EditPhotoViewControllerDelegate> *delegate;

- (id)initWithPicker:(UIImagePickerController*)picker andImage:(UIImage*)image;
@end
#endif