#import <UIKit/UIKit.h>

@class RootViewController;

@interface AppController : NSObject <UIApplicationDelegate> {
    UIWindow *window;
}

@property(nonatomic, readonly) RootViewController* viewController;

@property(nonatomic, readonly) NSDictionary *userInfo;
@property(nonatomic, readonly) NSString *deviceTokenString;
@property(nonatomic, strong) NSString *appUUID;
@property(nonatomic, readonly) NSString *clientIP;
@property(nonatomic, readonly) BOOL networkAvailable;
@property (nonatomic, strong) UIWindow* mainWindow;

@end

