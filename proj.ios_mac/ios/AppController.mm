/****************************************************************************
 Copyright (c) 2010 cocos2d-x.org

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#import "AppController.h"
#import "CCEAGLView.h"
#import "cocos2d.h"
#import "AppDelegate.h"
#import "RootViewController.h"

#import <FacebookSDK/FacebookSDK.h>
#import "TSProxyHelper.h"

//#import <AdSupport/AdSupport.h>
#import "Reachability.h"

#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

#import "MixpanelHelper.h"

@interface AppController ()
@property (nonatomic, strong) Reachability *internetReachability;
@property (strong, nonatomic) id<GAITracker> tracker;
@end

@implementation AppController

#pragma mark -
#pragma mark Application lifecycle

// cocos2d application instance
static AppDelegate s_sharedApplication;
static NSString *const kGaPropertyId = @"UA-57435339-1"; // Placeholder property ID.
static NSString *const kTrackingPreferenceKey = @"allowTracking";
#if COCOS2D_DEBUG
static BOOL const kGaDryRun = YES;
#else
static BOOL const kGaDryRun = NO;
#endif
static int const kGaDispatchPeriod = 30;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    

    // Override point for customization after application launch.
    _userInfo = launchOptions;
    NSLog(@"launchOptions : %@", self.userInfo);
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert |
                                                                           UIRemoteNotificationTypeBadge |
                                                                           UIRemoteNotificationTypeSound)];
    [self updateAppUUID];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    self.internetReachability = [Reachability reachabilityForInternetConnection];
	[self.internetReachability startNotifier];
	[self updateInterfaceWithReachability:self.internetReachability];

    NSDictionary *appDefaults = @{kTrackingPreferenceKey: @(YES)};
    [[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];
    // User must be able to opt out of tracking
    [GAI sharedInstance].optOut =
    ![[NSUserDefaults standardUserDefaults] boolForKey:kTrackingPreferenceKey];
    [[GAI sharedInstance] setDispatchInterval:kGaDispatchPeriod];
    [[GAI sharedInstance] setDryRun:kGaDryRun];
    self.tracker = [[GAI sharedInstance] trackerWithTrackingId:kGaPropertyId];
    
    // Add the view controller's view to the window and display.
    window = [[UIWindow alloc] initWithFrame: [[UIScreen mainScreen] bounds]];

    // Init the CCEAGLView
    CCEAGLView *eaglView = [CCEAGLView viewWithFrame: [window bounds]
                                     pixelFormat: kEAGLColorFormatRGB565
                                     depthFormat: GL_DEPTH24_STENCIL8_OES
                              preserveBackbuffer: NO
                                      sharegroup: nil
                                   multiSampling: NO
                                 numberOfSamples: 0];

    [eaglView setMultipleTouchEnabled:YES];
    
    // Use RootViewController manage CCEAGLView 
    _viewController = [[RootViewController alloc] initWithNibName:nil bundle:nil];
    _viewController.wantsFullScreenLayout = YES;
    _viewController.view = eaglView;

    // Set RootViewController to window
    if ( [[UIDevice currentDevice].systemVersion floatValue] < 6.0)
    {
        // warning: addSubView doesn't work on iOS6
        [window addSubview: _viewController.view];
    }
    else
    {
        // use this method on ios6
        [window setRootViewController:_viewController];
    }

    [window makeKeyAndVisible];
    self.mainWindow = window;

    [[UIApplication sharedApplication] setStatusBarHidden:true];

    [[TSProxyHelper sharedInstance] addNotification];
    
    // IMPORTANT: Setting the GLView should be done after creating the RootViewController
    cocos2d::GLView *glview = cocos2d::GLView::createWithEAGLView(eaglView);
    cocos2d::Director::getInstance()->setOpenGLView(glview);

#ifdef COCOS2D_DEBUG
    // TODO: deviceToken 초기화 후로 cocos2d::Application::getInstance()->run(); 위치 이동 필요...
    if ([self.deviceTokenString length] <= 0) {
        _deviceTokenString = @"APA91bHWw0ijubpdPAOinLM6nh9AJr7C1FlGvvehvojAbX4I35iZ2pW9KvLo2Nq00hh5GdcUhMFt4UBnJCc2IshUaxYAXhkH2B8rGVwKzElbCRSWhOkVHslkcOd_ZfFZwMSWM57Cd8hYYxMUZvq6dWWakWLwj0UaKQ";
        NSLog(@"set temp deviceTokenString = %@", self.deviceTokenString);
    }
#endif
    cocos2d::Application::getInstance()->run();

    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"BobbyStoryWorld_IOS_AppInit"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    
    [MixpanelHelper initializeMixpanel];
    
    NSNumber * userNum = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserNumber"];
    if(userNum)
    {
        [MixpanelHelper loginWithUserNumber:[userNum integerValue]];
    }
    
    
    return YES;
}

-(NSString*)uniqueIDForDevice {
    NSString* uniqueIdentifier = nil;
    if( [UIDevice instancesRespondToSelector:@selector(identifierForVendor)] ) { // >=iOS 7
        uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    } else { //<=iOS6, Use UDID of Device
        CFUUIDRef uuid = CFUUIDCreate(NULL);
        //uniqueIdentifier = ( NSString*)CFUUIDCreateString(NULL, uuid);- for non- ARC
        uniqueIdentifier = ( NSString*)CFBridgingRelease(CFUUIDCreateString(NULL, uuid));// for ARC
        CFRelease(uuid);
    }
    return uniqueIdentifier;
}

- (void)updateAppUUID {
    NSString *UUIDString = [[NSUserDefaults standardUserDefaults] valueForKey:@"appUUID"];
    if ([UUIDString length] <= 0) {
        NSLog(@"[UUIDString length] <= 0, fail loading UUIDString from NSUserDefaults");
        UUIDString = [self uniqueIDForDevice];
        self.appUUID = [NSString stringWithString:UUIDString];
        [[NSUserDefaults standardUserDefaults] setValue:self.appUUID forKey:@"appUUID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"new created UUID = %@ and NSUserDefaults Update", self.appUUID);
    } else {
        self.appUUID = [NSString stringWithString:UUIDString];;
        NSLog(@"load UUIDString from NSUserDefaults = %@", self.appUUID);
    }
    
#ifdef COCOS2D_DEBUG
    if ([self.appUUID length] <= 0) {
        self.appUUID = @"00000000-0000-0000-0000-000000000000";
        NSLog(@"set temp UUIDString = %@", self.appUUID);
    }
#endif
    return;
    
    NSString *newUUID = [self uniqueIDForDevice];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0) {
        NSLog(@"OS Version under 7.0.. created identifierForVendor");
        newUUID = [[UIDevice currentDevice].identifierForVendor UUIDString];
    } else {
//        ASIdentifierManager *manager = [ASIdentifierManager sharedManager];
//        newUUID = [[manager advertisingIdentifier] UUIDString];
//        if ([newUUID length] <= 0) {//광고 제한시 걸리 수 있으니 여기까지 다시 얻자.
//            newUUID = [[UIDevice currentDevice].identifierForVendor UUIDString];
//            NSLog(@"created advertisingIdentifier fail!!!!\ncreated UUIDString identifierForVendor with UIDevice = %@", newUUID);
//        } else {
//            NSLog(@"created advertisingIdentifier = %@", newUUID);
//        }
    }
    
    if ([newUUID isEqualToString:UUIDString] == NO) {
        NSLog(@"identifier changed!!!!!!\nNEW UUID : %@\nOLD UUID : %@", newUUID, UUIDString);
    }
    if ([UUIDString length] <= 0) {
        self.appUUID = [NSString stringWithString:newUUID];
        [[NSUserDefaults standardUserDefaults] setValue:self.appUUID forKey:@"appUUID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"new created UUID = %@ and NSUserDefaults Update", self.appUUID);
    } else {
        self.appUUID = [NSString stringWithString:UUIDString];;
        NSLog(@"load UUIDString from NSUserDefaults = %@", self.appUUID);
    }
#ifdef COCOS2D_DEBUG
    if ([self.appUUID length] <= 0) {
        self.appUUID = @"00000000-0000-0000-0000-000000000000";
        NSLog(@"set temp UUIDString = %@", self.appUUID);
    }
#endif
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    //http://qnibus.com/blog/how-to-develop-ios-application-delegate/
    NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken : %@", deviceToken);
    
    NSString *tokenAsString = [[[deviceToken description]
                                stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]]
                               stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *deviceTokenFromUserDefault = [[NSUserDefaults standardUserDefaults] valueForKey:@"appDeviceToken"];

    if ([tokenAsString length] > 0) {
        if ([deviceTokenFromUserDefault length] > 0) {
            if ([tokenAsString isEqualToString:deviceTokenFromUserDefault] == YES) {
                _deviceTokenString = deviceTokenFromUserDefault;
                NSLog(@"no changed deviceTokenString = %@", self.deviceTokenString);
            } else {
                _deviceTokenString = tokenAsString;
                [[NSUserDefaults standardUserDefaults] setValue:self.deviceTokenString forKey:@"appDeviceToken"];
                NSLog(@"update deviceToken string\nOLD deiveToken = %@\nNEW deviceToken = %@", deviceTokenFromUserDefault, self.deviceTokenString);
            }
        } else {
            _deviceTokenString = tokenAsString;
            NSLog(@"[deviceTokenFromuserDefault length] <= 0, fail loading deviceToken from NSUserDefaults");
            [[NSUserDefaults standardUserDefaults] setValue:self.deviceTokenString forKey:@"appDeviceToken"];
            NSLog(@"new deviceToken save to NSUserDefaults : %@", self.deviceTokenString);
        }
    } else {
        if ([deviceTokenFromUserDefault length] > 0) {
            _deviceTokenString = deviceTokenFromUserDefault;
            NSLog(@"set deviceTokenString from NSUserDefault. system deviceToken Empty....");
        } else {
            NSLog(@"create fail deviceTokenString!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }
    }
#ifdef COCOS2D_DEBUG
    if ([self.deviceTokenString length] <= 0) {
        _deviceTokenString = @"APA91bHWw0ijubpdPAOinLM6nh9AJr7C1FlGvvehvojAbX4I35iZ2pW9KvLo2Nq00hh5GdcUhMFt4UBnJCc2IshUaxYAXhkH2B8rGVwKzElbCRSWhOkVHslkcOd_ZfFZwMSWM57Cd8hYYxMUZvq6dWWakWLwj0UaKQ";
        NSLog(@"set temp deviceTokenString = %@", self.deviceTokenString);
    }
#endif
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"didReceiveRemoteNotification userInfo : %@", userInfo);
}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"didFailToRegisterForRemoteNotificationsWithError : %@", error);
#ifdef COCOS2D_DEBUG
    if ([self.deviceTokenString length] <= 0) {
        _deviceTokenString = @"APA91bHWw0ijubpdPAOinLM6nh9AJr7C1FlGvvehvojAbX4I35iZ2pW9KvLo2Nq00hh5GdcUhMFt4UBnJCc2IshUaxYAXhkH2B8rGVwKzElbCRSWhOkVHslkcOd_ZfFZwMSWM57Cd8hYYxMUZvq6dWWakWLwj0UaKQ";
        NSLog(@"set temp deviceTokenString = %@", self.deviceTokenString);
    }
#endif
}

- (void) reachabilityChanged:(NSNotification *)note {
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
	[self updateInterfaceWithReachability:curReach];
}

- (void)updateInterfaceWithReachability:(Reachability *)reachability {
	if (reachability == self.internetReachability) {
        NetworkStatus netStatus = [reachability currentReachabilityStatus];
        BOOL connectionRequired = [reachability connectionRequired];
        NSString* statusString = @"";
        
        switch (netStatus) {
            case NotReachable: {
                statusString = NSLocalizedString(@"Access Not Available", @"Text field text for access is not available");
                connectionRequired = NO;
                _networkAvailable = NO;
            } break;
                
            case ReachableViaWWAN: {
                statusString = NSLocalizedString(@"Reachable WWAN", @"");
                _networkAvailable = YES;
            } break;
            case ReachableViaWiFi: {
                statusString= NSLocalizedString(@"Reachable WiFi", @"");
                _networkAvailable = YES;
            } break;
        }
        if (connectionRequired) {
            NSString *connectionRequiredFormatString = NSLocalizedString(@"%@, Connection Required", @"Concatenation of status string with connection requirement");
            statusString= [NSString stringWithFormat:connectionRequiredFormatString, statusString];
        }
        NSLog(@"networkAvailable : %@", statusString);
        return;
	}
    NSLog(@"call undefine Reachability Notification!!!");
}

- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
     //We don't need to call this method any more. It will interupt user defined game pause&resume logic
    /* cocos2d::Director::getInstance()->pause(); */
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
     //We don't need to call this method any more. It will interupt user defined game pause&resume logic
    /* cocos2d::Director::getInstance()->resume(); */
    [FBAppCall handleDidBecomeActive];
    [GAI sharedInstance].optOut =
    ![[NSUserDefaults standardUserDefaults] boolForKey:kTrackingPreferenceKey];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
    cocos2d::Application::getInstance()->applicationDidEnterBackground();
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
    cocos2d::Application::getInstance()->applicationWillEnterForeground();
}

- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
    [[FBSession activeSession] close];
    [[TSProxyHelper sharedInstance] removeNotification];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    [FBAppCall handleOpenURL:url sourceApplication:sourceApplication fallbackHandler:^(FBAppCall *call) {
        if (call.appLinkData && call.appLinkData.targetURL) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"APP_HANDLED_URL" object:call.appLinkData.targetURL];
        }
    }];
    
    return YES;
}

#pragma mark - IP Address
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <net/if.h>

#define IOS_CELLULAR    @"pdp_ip0"
#define IOS_WIFI        @"en0"
#define IP_ADDR_IPv4    @"ipv4"
#define IP_ADDR_IPv6    @"ipv6"

- (NSString*)clientIP {
    NSString* ipAddress = [self getIPAddress:YES];
//    NSLog(@"client IP : %@", ipAddress);
#ifdef COCOS2D_DEBUG
    if ([ipAddress length] <= 0) {
        ipAddress = @"1.1.1.1";
        NSLog(@"set temp clientIP = %@", ipAddress);
    }
#endif
    return ipAddress;
}

- (NSString *)getIPAddress:(BOOL)preferIPv4
{
    NSArray *searchArray = preferIPv4 ?
    @[ IOS_WIFI @"/" IP_ADDR_IPv4, IOS_WIFI @"/" IP_ADDR_IPv6, IOS_CELLULAR @"/" IP_ADDR_IPv4, IOS_CELLULAR @"/" IP_ADDR_IPv6 ] :
    @[ IOS_WIFI @"/" IP_ADDR_IPv6, IOS_WIFI @"/" IP_ADDR_IPv4, IOS_CELLULAR @"/" IP_ADDR_IPv6, IOS_CELLULAR @"/" IP_ADDR_IPv4 ] ;
    
    NSDictionary *addresses = [self getIPAddresses];
//    NSLog(@"addresses: %@", addresses);
    
    __block NSString *address;
    [searchArray enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL *stop)
     {
         address = addresses[key];
         if(address) *stop = YES;
     } ];
    return address ? address : @"0.0.0.0";
}

- (NSDictionary *)getIPAddresses
{
    NSMutableDictionary *addresses = [NSMutableDictionary dictionaryWithCapacity:8];
    
    // retrieve the current interfaces - returns 0 on success
    struct ifaddrs *interfaces;
    if(!getifaddrs(&interfaces)) {
        // Loop through linked list of interfaces
        struct ifaddrs *interface;
        for(interface=interfaces; interface; interface=interface->ifa_next) {
            if(!(interface->ifa_flags & IFF_UP) /* || (interface->ifa_flags & IFF_LOOPBACK) */ ) {
                continue; // deeply nested code harder to read
            }
            const struct sockaddr_in *addr = (const struct sockaddr_in*)interface->ifa_addr;
            char addrBuf[ MAX(INET_ADDRSTRLEN, INET6_ADDRSTRLEN) ];
            if(addr && (addr->sin_family==AF_INET || addr->sin_family==AF_INET6)) {
                NSString *name = [NSString stringWithUTF8String:interface->ifa_name];
                NSString *type;
                if(addr->sin_family == AF_INET) {
                    if(inet_ntop(AF_INET, &addr->sin_addr, addrBuf, INET_ADDRSTRLEN)) {
                        type = IP_ADDR_IPv4;
                    }
                } else {
                    const struct sockaddr_in6 *addr6 = (const struct sockaddr_in6*)interface->ifa_addr;
                    if(inet_ntop(AF_INET6, &addr6->sin6_addr, addrBuf, INET6_ADDRSTRLEN)) {
                        type = IP_ADDR_IPv6;
                    }
                }
                if(type) {
                    NSString *key = [NSString stringWithFormat:@"%@/%@", name, type];
                    addresses[key] = [NSString stringWithUTF8String:addrBuf];
                }
            }
        }
        // Free memory
        freeifaddrs(interfaces);
    }
    return [addresses count] ? addresses : nil;
}

#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}

- (void)dealloc {
    [window release];
    [super dealloc];
}


@end
