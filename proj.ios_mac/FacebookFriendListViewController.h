//
//  FacebookFriendListViewController.h
//  bobby
//
//  Created by Dongwook, Kim on 14. 5. 7..
//
//

#ifndef MACOS

#import <UIKit/UIKit.h>

@interface FacebookFriendListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@end

#endif
