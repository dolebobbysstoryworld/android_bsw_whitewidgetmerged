//
//  CharacterCamViewController.h
//  bobby
//
//  Created by oasis on 2014. 6. 25..
//
//
#ifndef MACOS
#import <UIKit/UIKit.h>
#import "CamViewController.h"

@interface CharacterCamViewController : UIViewController<CamViewControllerDelegte, UIImagePickerControllerDelegate,UINavigationControllerDelegate, UIGestureRecognizerDelegate>

@end
#endif
