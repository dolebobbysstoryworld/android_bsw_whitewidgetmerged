//
//  QRCodeReaderViewController.h
//  bobby
//
//  Created by oasis on 2014. 5. 21..
//
//

#ifndef MACOS

#import <UIKit/UIKit.h>
#import "ZBarReaderView.h"
#import <AVFoundation/AVFoundation.h>
#import "ResolutionManager.h"

@class AVCaptureDevice;
@interface QRCodeReaderViewController : UIViewController <ZBarReaderViewDelegate, UITextFieldDelegate,AVCaptureVideoDataOutputSampleBufferDelegate>

@property (nonatomic, assign) TSOriginLayer originLayer;

@end

#endif