//
//  DKImageCell.h
//
//  Created by dkhamsing on 3/20/14.
//
//
#ifndef MACOS
#import <UIKit/UIKit.h>


@interface DKImageCell : UICollectionViewCell
{
    UIImageView *selectMark;
}

@property (nonatomic,strong) UIImageView *DKImageView;
@property (nonatomic,strong) UIImageView *thumbView;
@property (nonatomic,strong) UILabel *testLabel;
- (void)setSelectedCell:(BOOL)selected;
@end

#endif