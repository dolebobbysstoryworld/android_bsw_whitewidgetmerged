//
//  MovieViewController.h
//  bobby
//
//  Created by oasis on 2014. 4. 16..
//
//

#ifndef MACOS

#import <UIKit/UIKit.h>
#include "JeVideoScene.h"

@interface MovieViewController : UIViewController
- (id)initWithMovieNames:(NSArray*)movieNames andType:(play_type)playType;
- (void)closeAction;
@end

#endif
