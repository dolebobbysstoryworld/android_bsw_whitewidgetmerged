//
//  TSGuideForLogin.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 8. 30..
//
//

#import "TSGuideForLogin.h"
#import "UIButton+DolesUI.h"

@implementation TSGuideForLogin

- (void)adjustSubviewsLayout {
    UIView *subView = [[UIView alloc] initWithFrame:getRectValue(TSViewIndexShare, TSShareSubViewRect)];
    [self addSubview:subView];

    UIImageView *talkBox = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"guide_login_launcher_talkbox.png" ignore:YES]];
    talkBox.frame = getRectValue(TSViewIndexGuide, TSGUideLoginTalkBox);
    [subView addSubview:talkBox];
    
    UILabel *talkLabel = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexGuide, TSGuideLoginLabel)];
    if ([self.language isEqualToString:@"ko"]) {
        talkLabel.font = [UIFont fontWithName:@"NanumPen" size:(IS_IPAD)?62/2:44/2];
    } else if ([self.language isEqualToString:@"ja"]) {
        talkLabel.font = [UIFont fontWithName:kDefaultFontName size:(IS_IPAD)?38/2:28/2];
    } else {
        talkLabel.font = [UIFont fontWithName:@"NanumPen" size:(IS_IPAD)?62/2:44/2];
    }
    
    talkLabel.textAlignment = NSTextAlignmentCenter;
    talkLabel.textColor = RGBH(@"#ffffff");
    talkLabel.numberOfLines = 0;
    talkLabel.text = NSLocalizedString(@"PF_GUIDE_LOGIN_SIGNUP", @"Register with\nBobby’s Story World\nto use special functions");
    [talkLabel sizeToFit];
    talkLabel.center = CGCenterPointFromRect(getRectValue(TSViewIndexGuide, TSGuideLoginLabel));
#ifdef _DEBUG_GUIDE_BGCOLOR_
    talkLabel.backgroundColor = [UIColor purpleColor];
#endif
    [subView addSubview:talkLabel];
    
    UIImageView *guideImageCoin = [[UIImageView alloc]initWithImage:[UIImage imageNamedEx:@"guide_login_coin.png" ignore:YES]];
    guideImageCoin.frame = getRectValue(TSViewIndexGuide, TSGuideLoginCoinImage);
    [subView addSubview:guideImageCoin];
    
    UIImageView *guideImageItem = [[UIImageView alloc]initWithImage:[UIImage imageNamedEx:@"guide_login_item.png" ignore:YES]];
    guideImageItem.frame = getRectValue(TSViewIndexGuide, TSGuideLoginItemImage);
    [subView addSubview:guideImageItem];
    
    UIImageView *guideImageShare = [[UIImageView alloc]initWithImage:[UIImage imageNamedEx:@"guide_login_share.png" ignore:YES]];
    guideImageShare.frame = getRectValue(TSViewIndexGuide, TSGuideLoginShareImage);
    [subView addSubview:guideImageShare];

    
    CGFloat fontSize = 24/2;
//    if ([self.language isEqualToString:@"ja"]) {
//        fontSize = 16/2;
//    }
    
    CGRect rect = getRectValue(TSViewIndexGuide, TSGuideLoginCoinLabel);
    if ([self.language isEqualToString:@"ja"]) {
        rect.size.height = rect.size.height + 28/2;
    }
    UILabel *guideLabelCoin = [[UILabel alloc] initWithFrame:rect];
    guideLabelCoin.font = [UIFont fontWithName:kDefaultFontName size:fontSize];
    guideLabelCoin.textAlignment = NSTextAlignmentCenter;
    guideLabelCoin.textColor = RGBH(@"#ffffff");
    guideLabelCoin.numberOfLines = 0;
    guideLabelCoin.text = NSLocalizedString(@"PF_GUIDE_LOGIN_EARNCOIN", @"Earn\n Dole Coins");
    guideLabelCoin.shadowColor = [UIColor blackColor];
    guideLabelCoin.shadowOffset = CGSizeMake(1, 1);
    guideLabelCoin.layer.masksToBounds = NO;
#ifdef _DEBUG_GUIDE_BGCOLOR_
    guideLabelCoin.backgroundColor = [UIColor yellowColor];
#endif
    [subView addSubview:guideLabelCoin];
    
    rect = getRectValue(TSViewIndexGuide, TSGuideLoginItemLabel);
    if ([self.language isEqualToString:@"ja"]) {
        rect.size.height = rect.size.height + 28/2;
    }
    UILabel *guideLabelItem = [[UILabel alloc] initWithFrame:rect];
    guideLabelItem.font = [UIFont fontWithName:kDefaultFontName size:fontSize];
    guideLabelItem.textAlignment = NSTextAlignmentCenter;
    guideLabelItem.textColor = RGBH(@"#ffffff");
    guideLabelItem.numberOfLines = 0;
    guideLabelItem.text = NSLocalizedString(@"PF_GUIDE_LOGIN_GETITEM", @"Get\n special items");
    guideLabelItem.shadowColor = [UIColor blackColor];
    guideLabelItem.shadowOffset = CGSizeMake(1, 1);
    guideLabelItem.layer.masksToBounds = NO;
#ifdef _DEBUG_GUIDE_BGCOLOR_
    guideLabelItem.backgroundColor = [UIColor yellowColor];
#endif
    [subView addSubview:guideLabelItem];
    
    rect = getRectValue(TSViewIndexGuide, TSGuideLoginShareLabel);
    if ([self.language isEqualToString:@"ja"]) {
        rect.size.height = rect.size.height + 28/2;
    }
    UILabel *guideLabelShare = [[UILabel alloc] initWithFrame:rect];
    guideLabelShare.font = [UIFont fontWithName:kDefaultFontName size:fontSize];
    guideLabelShare.textAlignment = NSTextAlignmentCenter;
    guideLabelShare.textColor = RGBH(@"#ffffff");
    guideLabelShare.numberOfLines = 0;
    guideLabelShare.text = NSLocalizedString(@"PF_GUIDE_LOGIN_SHARE", @"Share\n my story book");
    guideLabelShare.shadowColor = [UIColor blackColor];
    guideLabelShare.shadowOffset = CGSizeMake(1, 1);
    guideLabelShare.layer.masksToBounds = NO;
#ifdef _DEBUG_GUIDE_BGCOLOR_
    guideLabelShare.backgroundColor = [UIColor yellowColor];
#endif
    [subView addSubview:guideLabelShare];
    
    UIButton *button = [UIButton underlineButtonWithTitle:NSLocalizedString(@"PF_LOGIN_BUTTON_TITLE_SIGNUP", @"Sign in")
                                                    Frame:getRectValue(TSViewIndexLogin, TSLoginSigninRect) textColor:RGBH(@"#11a6b4")];
    button.userInteractionEnabled = NO;
    [subView addSubview:button];
    
    button = [UIButton underlineButtonWithTitle:NSLocalizedString(@"PF_LGOIN_BUTTON_FBLOGIN", @"Login with facebook")
                                          Frame:getRectValue(TSViewIndexLogin, TSLoginFaceBookRect) textColor:RGBH(@"#11a6b4")];
    button.userInteractionEnabled = NO;
    [subView addSubview:button];
    
    UIImageView *guideImageLauncher = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"guide_login_launcher.png" ignore:YES]];
    guideImageLauncher.frame = getRectValue(TSViewIndexGuide, TSGuideLoginlauncherImage);
    [subView addSubview:guideImageLauncher];
    
    NSString *audioFilePath = nil;
    if ([self.language isEqualToString:@"ja"]) {
        audioFilePath = @"common/guide_sounds/ja/01_login";
    } else if ([self.language isEqualToString:@"ko"]) {
        audioFilePath = @"common/guide_sounds/ko/01_login";
    } else {
        audioFilePath = @"common/guide_sounds/en/01_login";
    }
    
    self.avAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:audioFilePath ofType:@"mp3"]] error:nil];
    [self.avAudioPlayer play];
}

@end
