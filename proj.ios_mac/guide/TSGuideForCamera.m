//
//  TSGuideForCamera.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 8. 28..
//
//

#import "TSGuideForCamera.h"

@interface TSGuideForCamera ()

@end

@implementation TSGuideForCamera

- (void)adjustSubviewsLayout {
    UIImageView *bobby = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"guide_photo_edit_bobby.png" ignore:YES]];
    bobby.frame = getRectValue(TSViewIndexGuide, TSGuideCameraBobby);
    [self addSubview:bobby];

    UIImageView *action = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"guide_photo_edit.png" ignore:YES]];
    action.frame = getRectValue(TSViewIndexGuide, TSGuideCameraAction);
    [self addSubview:action];

    UIImageView *talk = [[UIImageView alloc] initWithImage:[[UIImage imageNamedEx:@"guide_photo_edit_talkbox.png" ignore:YES]
                         resizableImageWithCapInsets:UIEdgeInsetsMake(25, 50, 25, 25)]];
    talk.frame = getRectValue(TSViewIndexGuide, TSGuideCameraTalkBox);
    [self addSubview:talk];
    
    UILabel *talkLabel = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexGuide, TSGuideCameraLabel)];
    if ([self.language isEqualToString:@"ko"]) {
        talkLabel.font = [UIFont fontWithName:@"NanumPen" size:(IS_IPAD)?96/2:44/2];
    } else if ([self.language isEqualToString:@"ja"]) {
        talkLabel.font = [UIFont fontWithName:kDefaultFontName size:(IS_IPAD)?54/2:24/2];
    } else {
        talkLabel.font = [UIFont fontWithName:@"NanumPen" size:(IS_IPAD)?94/2:44/2];
    }
    
    talkLabel.textAlignment = NSTextAlignmentCenter;
    talkLabel.textColor = RGBH(@"#ffffff");
    talkLabel.numberOfLines = 0;
    talkLabel.text = NSLocalizedString(@"PF_GUIDE_EDIT_PICTURE", @"Adjust the picture according\n to the guide");
    [talkLabel sizeToFit];
    talkLabel.center = CGCenterPointFromRect(getRectValue(TSViewIndexGuide, TSGuideCameraLabel));
//    talkLabel.shadowColor = [UIColor blackColor];
//    talkLabel.shadowOffset = CGSizeMake(1, 1);
//    talkLabel.layer.masksToBounds = NO;
//    guideLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
#ifdef _DEBUG_GUIDE_BGCOLOR_
    talkLabel.backgroundColor = [UIColor purpleColor];
#endif
    [self addSubview:talkLabel];

    NSString *audioFilePath = nil;
    if ([self.language isEqualToString:@"ja"]) {
        audioFilePath = @"common/guide_sounds/ja/10_create";
    } else if ([self.language isEqualToString:@"ko"]) {
        audioFilePath = @"common/guide_sounds/ko/10_create";
    } else {
        audioFilePath = @"common/guide_sounds/en/10_create";
    }
    
    self.avAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:audioFilePath ofType:@"mp3"]] error:nil];
    [self.avAudioPlayer play];
}

@end
