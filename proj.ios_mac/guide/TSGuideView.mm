//
//  TSGuideView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 7..
//
//

#import "TSGuideView.h"
#include "BGMManager.h"

@interface TSGuideView ()

@property (nonatomic, strong) UIView *dimlayerView;

@end

@implementation TSGuideView

- (id)initWithFrame:(CGRect)frame {
    return [self initWithFrame:frame transparentFrame:CGRectZero];
}

- (id)initWithFrame:(CGRect)frame transparentFrame:(CGRect)rect {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSLog(@"full frame : %@ transparentFrame : %@", NSStringFromCGRect(frame), NSStringFromCGRect(rect));
        self.language = [[NSLocale preferredLanguages] objectAtIndex:0];
        self.backgroundColor = [UIColor clearColor];
        UIView *dimView;
        if (rect.size.height > 0) {
            dimView =
            [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height - rect.size.height)];
            dimView.backgroundColor = [UIColor blackColor];
            [self addSubview:dimView];
            
            UIView *transparentView =
            [[UIView alloc] initWithFrame:rect];
            transparentView.backgroundColor = [UIColor clearColor];
            [self addSubview:transparentView];
        } else {
            dimView =
            [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
            dimView.backgroundColor = [UIColor blackColor];
            [self addSubview:dimView];
        }
        dimView.alpha = 0.7f;
        [self adjustSubviewsLayout];
    }
    return self;
}

- (void)adjustSubviewsLayout {
    NSLog(@"TSGuideView::adjustSubviewsLayout");
}

- (void)showInView:(UIView*)parent {
//    self.dimlayerView = [[UIView alloc] initWithFrame:parent.window.frame];
//    self.dimlayerView.backgroundColor = [UIColor blackColor];//tuilise_todo : pad에선 dim 배경색에 문제가 생긴다. 해결책 찾자.
//    self.dimlayerView.alpha = 0.6f;
//    
//    CGPoint center = [parent convertPoint:parent.window.center fromView:parent.window];
//    self.dimlayerView.center = center;
//    [parent addSubview:self.dimlayerView];
//    self.center = center;
//    [parent addSubview:self];
    
//    우선 여기까지하고 ADD TEST 해보고 진행하자.
//    self.center = parent.center;
    NSLog(@"parent frame : %@", NSStringFromCGRect(parent.frame));
    NSLog(@"Guide frame : %@", NSStringFromCGRect(self.frame));
    [parent addSubview:self];
    cocos2d::TSBGMManager::getInstance()->pause();
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
//    [self.dimlayerView removeFromSuperview];
//    self.dimlayerView = nil;
    
    if (self.avAudioPlayer) {
        [self.avAudioPlayer stop];
        self.avAudioPlayer = nil;
    }
    
    [self removeFromSuperview];
    cocos2d::TSBGMManager::getInstance()->resume();
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
