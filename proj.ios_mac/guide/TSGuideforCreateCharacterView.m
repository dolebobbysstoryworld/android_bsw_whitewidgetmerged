//
//  TSGuideforCreateCharacterView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 7..
//
//

#import "TSGuideforCreateCharacterView.h"

#define FONT_NANUM_BIG [UIFont fontWithName:@"NanumPen" size:(IS_IPAD)?96/2:48/2]
#define FONT_NANUM_SMALL [UIFont fontWithName:@"NanumPen" size:(IS_IPAD)?36/2:24/2]

@interface TSGuideforCreateCharacterView ()

@property (nonatomic, assign) BOOL firstpage;
@property (nonatomic, strong) UIImageView *guideImage;
@property (nonatomic, strong) UILabel *guideLabel;

@end
@implementation TSGuideforCreateCharacterView

- (id)initWithFrame:(CGRect)frame isSecondPage:(BOOL)second {
    self.firstpage = second;
    return [super initWithFrame:frame];;
}


- (void)adjustSubviewsLayout {
    NSLog(@"TSGuideforCreateCharacterView::adjustSubviewsLayout");
    NSString *audioFilePath = nil;
    if ([self.language isEqualToString:@"ja"]) {
        audioFilePath = @"common/guide_sounds/ja/10_create";
    } else if ([self.language isEqualToString:@"ko"]) {
        audioFilePath = @"common/guide_sounds/ko/10_create";
    } else {
        audioFilePath = @"common/guide_sounds/en/10_create";
    }
    
    self.avAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:audioFilePath ofType:@"mp3"]] error:nil];
    [self.avAudioPlayer play];
    
    self.firstpage = YES;
    if (self.firstpage == NO) {
        self.guideImage = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"guide_camera_frame.png" ignore:YES]];
    } else {
        self.guideImage = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"guide_photo_edit.png" ignore:YES]];
    }
    self.guideImage.frame = getRectValue(TSViewIndexGuide, TSGuideCreateCharecterImage);
#ifdef _DEBUG_GUIDE_
    self.guideImage.backgroundColor = [UIColor redColor];
#endif
    [self addSubview:self.guideImage];
    
    CGRect guideRect = getRectValue(TSViewIndexGuide, TSGuideCreateCharecterLabel);
    self.guideLabel = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexGuide, TSGuideCreateCharecterLabel)];
    if ([self.language isEqualToString:@"ko"]) {
        self.guideLabel.font = [UIFont fontWithName:@"NanumPen" size:(IS_IPAD)?96/2:48/2];
    } else if ([self.language isEqualToString:@"en"]) {
        self.guideLabel.font = [UIFont fontWithName:@"NanumPen" size:(IS_IPAD)?96/2:48/2];
    } else {
        self.guideLabel.font = [UIFont fontWithName:kDefaultFontName size:(IS_IPAD)?60/2:30/2];
    }
//    self.guideLabel.font = [UIFont fontWithName:kDefaultFontName size:15];
    self.guideLabel.textAlignment = NSTextAlignmentCenter;
    self.guideLabel.textColor = RGBH(@"#ffb91c");
    self.guideLabel.numberOfLines = 0;
    if (self.firstpage == NO) {
        self.guideLabel.text = @"Slide the screen left and right\n to decorate the charecter";
    } else {
        self.guideLabel.text = NSLocalizedString(@"PF_GUIDE_EDIT_PICTURE", @"Adjust the picture according\n to the guideline");
    }
    [self.guideLabel sizeToFit];
    self.guideLabel.frame = CGRectMake(guideRect.origin.x+(guideRect.size.width - self.guideLabel.frame.size.width)/2, guideRect.origin.y, self.guideLabel.frame.size.width, self.guideLabel.frame.size.height);
    self.guideLabel.shadowColor = [UIColor blackColor];
    self.guideLabel.shadowOffset = CGSizeMake(1, 1);
    self.guideLabel.layer.masksToBounds = NO;
#ifdef _DEBUG_GUIDE_
    self.guideLabel.backgroundColor = [UIColor yellowColor];
#endif
    [self addSubview:self.guideLabel];
    self.firstpage = YES;
}
/*
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.firstpage == YES) {
        self.firstpage = NO;
        self.guideImage.image = [UIImage imageNamedEx:@"guide_photo_edit.png" ignore:YES];
        self.guideLabel.text = @"Adjust the picture according\n to the guideline";
    } else {
        [super touchesEnded:touches withEvent:event];
    }
}*/
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
