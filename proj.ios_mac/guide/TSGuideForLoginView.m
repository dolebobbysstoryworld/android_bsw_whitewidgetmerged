//
//  TSGuideForLoginView.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 9..
//
//

#import "TSGuideForLoginView.h"

#define FONT_NANUM_BIG_LOGIN [UIFont fontWithName:@"NanumPen" size:48/2]
#define FONT_NANUM_SMALL_LOGIN [UIFont fontWithName:@"NanumPen" size:24/2]

@implementation TSGuideForLoginView

- (void)adjustSubviewsLayout {
    NSString *audioFilePath = nil;
    if ([self.language isEqualToString:@"ja"]) {
        audioFilePath = @"common/guide_sounds/ja/01_login";
    } else if ([self.language isEqualToString:@"ko"]) {
        audioFilePath = @"common/guide_sounds/ko/01_login";
    } else {
        audioFilePath = @"common/guide_sounds/en/01_login";
    }
    
    self.avAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:audioFilePath ofType:@"mp3"]] error:nil];
    [self.avAudioPlayer play];
    
    NSLog(@"TSGuideforLoginView::adjustSubviewsLayout");
    UIView *subView = [[UIView alloc] initWithFrame:getRectValue(TSViewIndexShare, TSShareSubViewRect)];
    [self addSubview:subView];
    
    UILabel *guideLabel = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexGuide, TSGuideLoginMainLabel)];
    if ([self.language isEqualToString:@"ko"]) {
        guideLabel.font = [UIFont fontWithName:@"NanumPen" size:48/2];
    } else if ([self.language isEqualToString:@"en"]) {
        guideLabel.font = [UIFont fontWithName:@"NanumPen" size:48/2];
    } else {
        guideLabel.font = [UIFont fontWithName:kDefaultFontName size:30/2];
    }

    guideLabel.textAlignment = NSTextAlignmentCenter;
    guideLabel.textColor = RGBH(@"#ffb91c");
    guideLabel.numberOfLines = 0;
    guideLabel.text = NSLocalizedString(@"PF_GUIDE_LOGIN_SIGNUP", @"Register to Bobby's Journey to use special functions");
    guideLabel.shadowColor = [UIColor blackColor];
    guideLabel.shadowOffset = CGSizeMake(1, 1);
    guideLabel.layer.masksToBounds = NO;
//    guideLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
#ifdef _DEBUG_GUIDE_
    guideLabel.backgroundColor = [UIColor yellowColor];
#endif
    [subView addSubview:guideLabel];
    
    UIImageView *guideImageCoin = [[UIImageView alloc]initWithImage:[UIImage imageNamedEx:@"guide_login_coin.png" ignore:YES]];
    guideImageCoin.frame = getRectValue(TSViewIndexGuide, TSGuideLoginCoinImage);
    [subView addSubview:guideImageCoin];

    UIImageView *guideImageItem = [[UIImageView alloc]initWithImage:[UIImage imageNamedEx:@"guide_login_item.png" ignore:YES]];
    guideImageItem.frame = getRectValue(TSViewIndexGuide, TSGuideLoginItemImage);
    [subView addSubview:guideImageItem];

    UIImageView *guideImageShare = [[UIImageView alloc]initWithImage:[UIImage imageNamedEx:@"guide_login_share.png" ignore:YES]];
    guideImageShare.frame = getRectValue(TSViewIndexGuide, TSGuideLoginShareImage);
    [subView addSubview:guideImageShare];
    
    UILabel *guideLabelCoin = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexGuide, TSGuideLoginCoinLabel)];
//    if ([self.language isEqualToString:@"ja"]) {
//        guideLabelCoin.font = [UIFont fontWithName:kDefaultFontName size:24/2];
//    } else {
//        guideLabelCoin.font = FONT_NANUM_SMALL_LOGIN;
//    }
    guideLabelCoin.font = [UIFont fontWithName:kDefaultFontName size:22/2];
    guideLabelCoin.textAlignment = NSTextAlignmentCenter;
    guideLabelCoin.textColor = RGBH(@"#ffffff");
    guideLabelCoin.numberOfLines = 0;
    guideLabelCoin.text = NSLocalizedString(@"PF_GUIDE_LOGIN_EARNCOIN", @"Earn\n Dole Coins");
    guideLabelCoin.shadowColor = [UIColor blackColor];
    guideLabelCoin.shadowOffset = CGSizeMake(1, 1);
    guideLabelCoin.layer.masksToBounds = NO;
#ifdef _DEBUG_GUIDE_
    guideLabelCoin.backgroundColor = [UIColor yellowColor];
#endif
    [subView addSubview:guideLabelCoin];
    
    UILabel *guideLabelItem = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexGuide, TSGuideLoginItemLabel)];
//    if ([self.language isEqualToString:@"ja"]) {
//        guideLabelItem.font = [UIFont fontWithName:kDefaultFontName size:24/2];
//    } else {
//        guideLabelItem.font = FONT_NANUM_SMALL_LOGIN;
//    }
    guideLabelItem.font = [UIFont fontWithName:kDefaultFontName size:22/2];
    guideLabelItem.textAlignment = NSTextAlignmentCenter;
    guideLabelItem.textColor = RGBH(@"#ffffff");
    guideLabelItem.numberOfLines = 0;
    guideLabelItem.text = NSLocalizedString(@"PF_GUIDE_LOGIN_GETITEM", @"Get\n special items");
    guideLabelItem.shadowColor = [UIColor blackColor];
    guideLabelItem.shadowOffset = CGSizeMake(1, 1);
    guideLabelItem.layer.masksToBounds = NO;
#ifdef _DEBUG_GUIDE_
    guideLabelItem.backgroundColor = [UIColor yellowColor];
#endif
    [subView addSubview:guideLabelItem];

    UILabel *guideLabelShare = [[UILabel alloc] initWithFrame:getRectValue(TSViewIndexGuide, TSGuideLoginShareLabel)];
//    if ([self.language isEqualToString:@"ja"]) {
//        guideLabelShare.font = [UIFont fontWithName:kDefaultFontName size:24/2];
//    } else {
//        guideLabelShare.font = FONT_NANUM_SMALL_LOGIN;
//    }
    guideLabelShare.font = [UIFont fontWithName:kDefaultFontName size:22/2];
    guideLabelShare.textAlignment = NSTextAlignmentCenter;
    guideLabelShare.textColor = RGBH(@"#ffffff");
    guideLabelShare.numberOfLines = 0;
    guideLabelShare.text = NSLocalizedString(@"PF_GUIDE_LOGIN_SHARE", @"Share\n my story book");
    guideLabelShare.shadowColor = [UIColor blackColor];
    guideLabelShare.shadowOffset = CGSizeMake(1, 1);
    guideLabelShare.layer.masksToBounds = NO;
#ifdef _DEBUG_GUIDE_
    guideLabelShare.backgroundColor = [UIColor yellowColor];
#endif
    [subView addSubview:guideLabelShare];
    
    UIImageView *guideImageLauncher = [[UIImageView alloc] initWithImage:[UIImage imageNamedEx:@"guide_login_launcher.png" ignore:YES]];
    guideImageLauncher.frame = getRectValue(TSViewIndexGuide, TSGuideLoginlauncherImage);
    [subView addSubview:guideImageLauncher];
}

@end
