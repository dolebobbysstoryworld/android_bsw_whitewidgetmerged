//
//  TSGuideView.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 7..
//
//

#import <UIKit/UIKit.h>
#import "ResolutionManager.h"
#import "UIImage+PathExtention.h"
#import "UIColor+RGB.h"
#import <AVFoundation/AVFoundation.h>

#define _GUIDE_ENABLE_
#if COCOS2D_DEBUG
//#define _DEBUG_GUIDE_
#ifdef _DEBUG_GUIDE_
//#define _DEBUG_GUIDE_BGCOLOR_
#endif
#endif

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define FONTSIZE_NANUM      [UIFont fontWithName:@"NanumPen" size:(IS_IPAD)?96/2:48/2]
#define FONTSIZE_DEFAULT    [UIFont fontWithName:kDefaultFontName size:(IS_IPAD)?60/2:30/2]

@interface TSGuideView : UIView

- (id)initWithFrame:(CGRect)frame transparentFrame:(CGRect)rect;
- (void)showInView:(UIView*)parent;

@property (nonatomic, strong) NSString *language;//@"ja",@"ko",@"en"
@property (nonatomic, strong) AVAudioPlayer *avAudioPlayer;
@end
