//
//  TSGuideforCreateCharacterView.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 7..
//
//

#import "TSGuideView.h"

@interface TSGuideforCreateCharacterView : TSGuideView

- (id)initWithFrame:(CGRect)frame isSecondPage:(BOOL)second;
//@property (nonatomic, strong) NSString *language;
@end
