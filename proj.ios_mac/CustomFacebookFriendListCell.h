//
//  CustomFacebookFriendListCell.h
//  bobby
//
//  Created by Dongwook, Kim on 14. 6. 3..
//
//

#import <UIKit/UIKit.h>

@interface CustomFacebookFriendListCell : UITableViewCell

@property (nonatomic, strong) UIImageView *thumbNail;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIImageView *checkBox;

@end
