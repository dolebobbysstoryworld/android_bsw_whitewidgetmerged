require 'find'
require 'iconv' unless String.method_defined?(:encode)


#pattern = Regexp.new(".+\"\([a-zA-Z0-9\_]+\.png\)\".+")

pattern = Regexp.new("^(?!//).+\"\([a-zA-Z0-9\_]+\.png\)\".+")


result = Array.new
Dir.glob('Classes/**').each do |file|
    next unless File.file?(file)
        File.open(file) do |f|
            f.each_line do |line|
            	if String.method_defined?(:encode)
				  line.encode!('UTF-8', 'UTF-8', :invalid => :replace)
				else
				  ic = Iconv.new('UTF-8', 'UTF-8//IGNORE')
				  line = ic.iconv(line)
				end
            	if line.match(pattern)
				    #puts $1
				    result << $1
				end
        end
    end
end

result.uniq!.sort!

targets = ['Resources/android', 'Resources/common', 'Resources/guide']

result.each do |res_file|
	occurrences = 0
	targets.each do |t|
		Find.find(t) do |res|
			occurrences += 1 if res.include? res_file
		end
	end
	puts res_file if occurrences == 0 
end