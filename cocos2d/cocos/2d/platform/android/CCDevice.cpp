/****************************************************************************
Copyright (c) 2010-2012 cocos2d-x.org
Copyright (c) 2013-2014 Chukong Technologies Inc.

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/

#include "CCPlatformConfig.h"
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

#include "platform/CCDevice.h"
#include <string.h>
#include <android/log.h>
#include <jni.h>
#include "ccTypes.h"
#include "jni/DPIJni.h"
#include "jni/Java_org_cocos2dx_lib_Cocos2dxHelper.h"
#include "jni/JniHelper.h"
#include "platform/CCFileUtils.h"
#include "deprecated/CCString.h"

NS_CC_BEGIN

__String* tokenStr = nullptr;
__String* uuidStr = nullptr;
__String* ipAddrStr = nullptr;

int Device::getDPI()
{
    static int dpi = -1;
    if (dpi == -1)
    {
        dpi = (int)getDPIJNI();
    }
    return dpi;
}

void Device::setAccelerometerEnabled(bool isEnabled)
{
    if (isEnabled)
    {
        enableAccelerometerJni();
    }
    else
    {
        disableAccelerometerJni();
    }
}

void Device::setAccelerometerInterval(float interval)
{
	setAccelerometerIntervalJni(interval);
}

class BitmapDC
{
public:

    BitmapDC()
    : _data(nullptr)
    , _width(0)
    , _height(0)
    {
    }

    ~BitmapDC(void)
    {
    }

    bool getBitmapFromJavaShadowStroke(	const char *text,
    									int nWidth,
    									int nHeight,
    									Device::TextAlign eAlignMask,
    									const char * pFontName,
    									float fontSize,
    									float textTintR 		= 1.0,
    									float textTintG 		= 1.0,
    									float textTintB 		= 1.0,
    									bool shadow 			= false,
    									float shadowDeltaX 		= 0.0,
    									float shadowDeltaY 		= 0.0,
    									float shadowBlur 		= 0.0,
    									float shadowOpacity 	= 0.0,
    									bool stroke 			= false,
    									float strokeColorR 		= 0.0,
    									float strokeColorG 		= 0.0,
    									float strokeColorB 		= 0.0,
    									float strokeSize 		= 0.0,
    									bool underline			= false,
    									bool includeFontPadding	= false )
    {
           JniMethodInfo methodInfo;
           if (! JniHelper::getStaticMethodInfo(methodInfo, "org/cocos2dx/lib/Cocos2dxBitmap", "createTextBitmapShadowStroke",
               "(Ljava/lang/String;Ljava/lang/String;IFFFIIIZFFFFZFFFFZZ)Z"))
           {
               CCLOG("%s %d: error to get methodInfo", __FILE__, __LINE__);
               return false;
           }

           // Do a full lookup for the font path using FileUtils in case the given font name is a relative path to a font file asset,
           // or the path has been mapped to a different location in the app package:
           std::string fullPathOrFontName = FileUtils::getInstance()->fullPathForFilename(pFontName);
            
           // If the path name returned includes the 'assets' dir then that needs to be removed, because the android.content.Context
           // requires this portion of the path to be omitted for assets inside the app package.
           if (fullPathOrFontName.find("assets/") == 0)
           {
               fullPathOrFontName = fullPathOrFontName.substr(strlen("assets/"));	// Chop out the 'assets/' portion of the path.
           }

           /**create bitmap
            * this method call Cococs2dx.createBitmap()(java code) to create the bitmap, the java code
            * will call Java_org_cocos2dx_lib_Cocos2dxBitmap_nativeInitBitmapDC() to init the width, height
            * and data.
            * use this approach to decrease the jni call number
           */
           jstring jstrText = methodInfo.env->NewStringUTF(text);
           jstring jstrFont = methodInfo.env->NewStringUTF(fullPathOrFontName.c_str());

           if(!shadow)
           {
               shadowDeltaX = 0.0f;
               shadowDeltaY = 0.0f;
               shadowBlur = 0.0f;
               shadowOpacity = 0.0f;
           }
           if (!stroke)
           {
               strokeColorR = 0.0f;
               strokeColorG = 0.0f;
               strokeColorB = 0.0f;
               strokeSize = 0.0f;
           }
           if(!methodInfo.env->CallStaticBooleanMethod(methodInfo.classID, methodInfo.methodID, jstrText,
               jstrFont, (int)fontSize, textTintR, textTintG, textTintB, eAlignMask, nWidth, nHeight, shadow,
               shadowDeltaX, -shadowDeltaY, shadowBlur, shadowOpacity, stroke, strokeColorR, strokeColorG, strokeColorB,
               strokeSize, underline, includeFontPadding))
           {
                return false;
           }

           methodInfo.env->DeleteLocalRef(jstrText);
           methodInfo.env->DeleteLocalRef(jstrFont);
           methodInfo.env->DeleteLocalRef(methodInfo.classID);

           return true;
    }

    int getTextEllipsysIndex(const char* text, const char* ellipsys,
    		int nWidth, Device::TextAlign eAlignMask, const char* pFontName, float fontSize)
	{
		   JniMethodInfo methodInfo;
		   if(!JniHelper::getStaticMethodInfo(methodInfo, "org/cocos2dx/lib/Cocos2dxBitmap", "getTextEllipsysIndex",
			   "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)I"))
		   {
			   CCLOG("%s %d: error to get methodInfo", __FILE__, __LINE__);
			   return -1;
		   }

           std::string fullPathOrFontName = FileUtils::getInstance()->fullPathForFilename(pFontName);
            
           if (fullPathOrFontName.find("assets/") == 0)
           {
               fullPathOrFontName = fullPathOrFontName.substr(strlen("assets/"));	// Chop out the 'assets/' portion of the path.
           }

           jstring jstrText = methodInfo.env->NewStringUTF(text);
           jstring jstrEllipsys = methodInfo.env->NewStringUTF(ellipsys);
           jstring jstrFont = methodInfo.env->NewStringUTF(fullPathOrFontName.c_str());

		   int ret = methodInfo.env->CallStaticIntMethod(methodInfo.classID, methodInfo.methodID, jstrText, jstrEllipsys, 
			   jstrFont, (int)fontSize, eAlignMask, nWidth);

		   methodInfo.env->DeleteLocalRef(jstrText);
		   methodInfo.env->DeleteLocalRef(jstrEllipsys);
		   methodInfo.env->DeleteLocalRef(jstrFont);
		   methodInfo.env->DeleteLocalRef(methodInfo.classID);

		   return ret;
	}

    int getTextWidthForFont(const char* text, const char* fontName, float fontSize)
	{
		   JniMethodInfo methodInfo;
		   if(!JniHelper::getStaticMethodInfo(methodInfo, "org/cocos2dx/lib/Cocos2dxBitmap", "getTextWidthForFont",
			   "(Ljava/lang/String;Ljava/lang/String;I)I"))
		   {
			   CCLOG("%s %d: error to get methodInfo", __FILE__, __LINE__);
			   return -1;
		   }

		   std::string fullPathOrFontName = FileUtils::getInstance()->fullPathForFilename(fontName);
			
		   if (fullPathOrFontName.find("assets/") == 0)
		   {
			   fullPathOrFontName = fullPathOrFontName.substr(strlen("assets/"));	// Chop out the 'assets/' portion of the path.
		   }

		   jstring jstrText = methodInfo.env->NewStringUTF(text);
		   jstring jstrFont = methodInfo.env->NewStringUTF(fullPathOrFontName.c_str());

		   int ret = methodInfo.env->CallStaticIntMethod(methodInfo.classID, methodInfo.methodID, jstrText, 
			   jstrFont, (int)fontSize);

		   methodInfo.env->DeleteLocalRef(jstrText);
		   methodInfo.env->DeleteLocalRef(jstrFont);
		   methodInfo.env->DeleteLocalRef(methodInfo.classID);

		   return ret;
	}

public:
    int _width;
    int _height;
    unsigned char *_data;
};

static BitmapDC& sharedBitmapDC()
{
    static BitmapDC s_BmpDC;
    return s_BmpDC;
}

Data Device::getTextureDataForText(const char * text,const FontDefinition& textDefinition,TextAlign align,int &width,int &height)
{
    Data ret;
    do 
    {
        BitmapDC &dc = sharedBitmapDC();

        if(! dc.getBitmapFromJavaShadowStroke(text, 
            (int)textDefinition._dimensions.width, 
            (int)textDefinition._dimensions.height, 
            align, textDefinition._fontName.c_str(),
            textDefinition._fontSize,
            textDefinition._fontFillColor.r / 255.0f, 
            textDefinition._fontFillColor.g / 255.0f, 
            textDefinition._fontFillColor.b / 255.0f, 
            textDefinition._shadow._shadowEnabled,
            textDefinition._shadow._shadowOffset.width, 
            textDefinition._shadow._shadowOffset.height, 
            textDefinition._shadow._shadowBlur, 
            textDefinition._shadow._shadowOpacity,
            textDefinition._stroke._strokeEnabled, 
            textDefinition._stroke._strokeColor.r / 255.0f, 
            textDefinition._stroke._strokeColor.g / 255.0f, 
            textDefinition._stroke._strokeColor.b / 255.0f, 
            textDefinition._stroke._strokeSize,
            textDefinition._underline,
            textDefinition._includeFontPadding)) { break;};

        width = dc._width;
        height = dc._height;
        ret.fastSet(dc._data,width * height * 4);
    } while (0);

    return ret;
}

int Device::getEllipsisIndexForText(const char* text, const char* ellipsis, const FontDefinition& textDefinition, TextAlign align)
{
	int ret = -1;

	do 
	{
		BitmapDC &dc = sharedBitmapDC();
		ret = dc.getTextEllipsysIndex(text, ellipsis, (int)textDefinition._dimensions.width,
				align, textDefinition._fontName.c_str(), textDefinition._fontSize);
	} while (0);

	return ret;
}


int Device::getTextWidth(const char* text, const char* fontName, float fontSize)
{
	int ret = -1;

	do 
	{
		BitmapDC &dc = sharedBitmapDC();
		ret = dc.getTextWidthForFont(text, fontName, fontSize);
	} while (0);

    return ret;
}

const char *Device::getDeviceTokenString() {
	if(tokenStr == nullptr) {
	JniMethodInfo methodInfo;
	if(!JniHelper::getStaticMethodInfo(methodInfo, "org/cocos2dx/lib/Cocos2dxUtil", "getDeviceToken",
													"()Ljava/lang/String;"))
	{
		CCLOG("%s %d: error to get methodInfo", __FILE__, __LINE__);
		return "";
	}

	jstring token = (jstring)methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID);
		tokenStr = __String::create(JniHelper::jstring2string(token));
		tokenStr->retain();
	}
	return tokenStr->getCString();
}

const char *Device::getUUIDString() {
	if(uuidStr == nullptr) {
	JniMethodInfo methodInfo;
	if(!JniHelper::getStaticMethodInfo(methodInfo, "org/cocos2dx/lib/Cocos2dxUtil", "getUUID",
													"()Ljava/lang/String;"))
	{
		CCLOG("%s %d: error to get methodInfo", __FILE__, __LINE__);
		return "";
	}

	jstring uuid = (jstring)methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID);
		uuidStr = __String::create(JniHelper::jstring2string(uuid));
		uuidStr->retain();
	}
	return uuidStr->getCString();
}

const char *Device::getClientIPString() {
	if(ipAddrStr == nullptr) {
	JniMethodInfo methodInfo;
	if(!JniHelper::getStaticMethodInfo(methodInfo, "org/cocos2dx/lib/Cocos2dxUtil", "getIPAddress",
													"()Ljava/lang/String;"))
	{
		CCLOG("%s %d: error to get methodInfo", __FILE__, __LINE__);
		return "";
	}

	jstring ipAddr = (jstring)methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID);
		ipAddrStr = __String::create(JniHelper::jstring2string(ipAddr));
		ipAddrStr->retain();
	}
	return ipAddrStr->getCString();
}

bool Device::getNetworkAvailable() {
	JniMethodInfo methodInfo;
	if(!JniHelper::getStaticMethodInfo(methodInfo, "org/cocos2dx/lib/Cocos2dxUtil", "getNetworkStatus",
													"()I"))
	{
		CCLOG("%s %d: error to get methodInfo", __FILE__, __LINE__);
		return false;
	}

	int ret = methodInfo.env->CallStaticIntMethod(methodInfo.classID, methodInfo.methodID);

	// For future use for checking airplane mode (-1: unavailable, 0: airplane mode, 1: available)
	return (ret > 0);
}

const char *Device::getDocumentPath() {
    return "";
}

size_t Device::getFileSize(const char * path) {
	if(path == nullptr)
    return 0;

	FILE* file = fopen(path, "rb");
	if(file == nullptr)
		return 0;

	fseek(file, 0, SEEK_END);
	int size = ftell(file);
	fclose(file);
	return (size < 0 ? 0 : size);
}

bool Device::deleteFile(const char  *path) {
	JniMethodInfo methodInfo;
	if(!JniHelper::getStaticMethodInfo(methodInfo, "org/cocos2dx/lib/Cocos2dxUtil", "deleteFile",
													"(Ljava/lang/String;)Z"))
	{
		CCLOG("%s %d: error to get methodInfo", __FILE__, __LINE__);
    return false;
}

	jstring jstrPath = methodInfo.env->NewStringUTF(path);
	jboolean deleted = (jboolean)methodInfo.env->CallStaticBooleanMethod(methodInfo.classID, methodInfo.methodID, jstrPath);
	methodInfo.env->DeleteLocalRef(jstrPath);
	return (deleted == JNI_TRUE ? true : false);
}

const char* Device::getCurrentAppVersion() {
	JniMethodInfo methodInfo;
	if(!JniHelper::getStaticMethodInfo(methodInfo, "org/cocos2dx/lib/Cocos2dxUtil", "getVersion",
													"()Ljava/lang/String;"))
	{
		CCLOG("%s %d: error to get methodInfo", __FILE__, __LINE__);
		return false;
	}

	jstring version = (jstring)methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID);
	__String* versionStr = __String::create(JniHelper::jstring2string(version));
	return versionStr->getCString();
}

const char* Device::getPlatformString(int id) {
	JniMethodInfo methodInfo;
	if(!JniHelper::getStaticMethodInfo(methodInfo, "org/cocos2dx/lib/Cocos2dxUtil", "getPlatformString",
													"(I)Ljava/lang/String;"))
	{
		CCLOG("%s %d: error to get methodInfo", __FILE__, __LINE__);
		return false;
	}

	jstring read = (jstring)methodInfo.env->CallStaticObjectMethod(methodInfo.classID, methodInfo.methodID, id);
	__String* readStr = __String::create(JniHelper::jstring2string(read));
	return readStr->getCString();
}

const char* Device::getResString(Res::Strings key) {
    const char * resStr = NULL;
    switch (key) {
        case Res::Strings::CC_APP_UPDATE_NOTICE: resStr = Device::getPlatformString(0); break;
        case Res::Strings::CC_APPUPDATE_BUTTON_TITLE_OK: resStr = Device::getPlatformString(1); break;
        case Res::Strings::PF_NET_WARNING_3G: resStr = Device::getPlatformString(2); break;
        case Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL: resStr = Device::getPlatformString(3); break;
        case Res::Strings::CM_POPUP_BUTTON_TITLE_OK: resStr = Device::getPlatformString(4); break;
        case Res::Strings::PF_NET_POPUP_WARNING_NETWORK_UNSTABLE: resStr = Device::getPlatformString(5); break;
        case Res::Strings::PF_NET_POPUP_NOTICCE_UPDATE_AND_RESTART: resStr = Device::getPlatformString(6); break;
        case Res::Strings::PF_NET_POPUP_NOTICCE_DOWNLOAD_RESOURCCE: resStr = Device::getPlatformString(7); break;
        case Res::Strings::CM_NET_WARNING_NETWORK_UNSTABLE: resStr = Device::getPlatformString(8); break;
        case Res::Strings::PF_NET_ERROR_EMAIL_ADDRESS: resStr = Device::getPlatformString(9); break;
        case Res::Strings::PF_NET_ERROR_EMAIL_FOR_SNS: resStr = Device::getPlatformString(10); break;
        case Res::Strings::PF_NET_ERROR__PASSWORD_INCORRECT: resStr = Device::getPlatformString(11); break;
        case Res::Strings::PF_NET_ERROR_EMAIL_ENTER_FULL_ADDRESS: resStr = Device::getPlatformString(12); break;
        case Res::Strings::PF_NET_ERROR_EMAIL_ALREADY_USE: resStr = Device::getPlatformString(13); break;
        case Res::Strings::PF_NET_ERROR__PASSWORD_SHORT: resStr = Device::getPlatformString(14); break;
        case Res::Strings::PF_NET_ERROR__QRCODE_INPUT: resStr = Device::getPlatformString(15); break;
        case Res::Strings::CC_EVENTNOTICCE_DISPLAY_A_DAY: resStr = Device::getPlatformString(16); break;
        case Res::Strings::CM_POPUP_BUTTON_TITLE_CLOSE: resStr = Device::getPlatformString(17); break;
        case Res::Strings::CC_HOME_BOOKTITLE_NEW: resStr = Device::getPlatformString(18); break;
        case Res::Strings::CC_HOME_MENUTITLE_SETTING: resStr = Device::getPlatformString(19); break;
        case Res::Strings::CC_HOME_POPUP_DELETE_STORY: resStr = Device::getPlatformString(20); break;
        case Res::Strings::CC_HOME_POPUP_NEED_LOGIN: resStr = Device::getPlatformString(21); break;
        case Res::Strings::CC_HOME_POPUP_NEED_LOGIN_BUTTON_TITLE_LOGIN: resStr = Device::getPlatformString(22); break;
        case Res::Strings::PF_DOLECOIN_ABOUTCOIN: resStr = Device::getPlatformString(23); break;
        case Res::Strings::PF_DOLECOIN_HOWEARN: resStr = Device::getPlatformString(24); break;
        case Res::Strings::PF_DOLECOIN_BUTTONTITLE_SCAN: resStr = Device::getPlatformString(25); break;
        case Res::Strings::PF_DOLECOIN_NEED_LOGIN: resStr = Device::getPlatformString(26); break;
        case Res::Strings::PF_POPUP_RECEIVED_DOLECOIN: resStr = Device::getPlatformString(27); break;
        case Res::Strings::PF_QRCODE_INPUT_CODE: resStr = Device::getPlatformString(28); break;
        case Res::Strings::PF_QRCODE_SCAN_CODE: resStr = Device::getPlatformString(29); break;
        case Res::Strings::PF_QRCODE_ERROR_USED: resStr = Device::getPlatformString(30); break;
        case Res::Strings::PF_QRCODE_ERROR_NETWORK_NOT_CONNCET: resStr = Device::getPlatformString(31); break;
        case Res::Strings::CC_LIST_CHARACTER_MAKE: resStr = Device::getPlatformString(32); break;
        case Res::Strings::CC_LIST_NEED_UNLOCK_COIN: resStr = Device::getPlatformString(33); break;
        case Res::Strings::CC_LIST_CHARACTER_WARNING_DELETE: resStr = Device::getPlatformString(34); break;
        case Res::Strings::CC_EXPANDEDITEM_NEED_UNLOCK_COIN: resStr = Device::getPlatformString(35); break;
        case Res::Strings::CC_EXPANDEDITEM_NEED_UNLOCK_COIN_1: resStr = Device::getPlatformString(36); break;
        case Res::Strings::CC_EXPANDEDITEM_NEED_UNLOCK_COIN_2: resStr = Device::getPlatformString(37); break;
        case Res::Strings::CC_EXPANDEDITEM_NEED_UNLOCK_COIN_LIMITED_PERIOD: resStr = Device::getPlatformString(38); break;
        case Res::Strings::CC_EXPANDEDITEM_NEED_UNLOCK_COIN_LIMITED_NUMBER: resStr = Device::getPlatformString(39); break;
        case Res::Strings::CC_EXPANDEDITEM_POPUP_WARNING_USE_UNLOCK_COIN: resStr = Device::getPlatformString(40); break;
        case Res::Strings::CC_EXPANDEDITEM_POPUP_COIN_BUTTON_TITLE_UNLOCK: resStr = Device::getPlatformString(41); break;
        case Res::Strings::CC_EXPANDEDITEM_POPUP_NOT_ENOUGH_COIN: resStr = Device::getPlatformString(42); break;
        case Res::Strings::CC_EXPANDEDITEM_POPUP_BUTTON_TITLE_EARN_COIN: resStr = Device::getPlatformString(43); break;
        case Res::Strings::CC_EXPANDEDITEM_POPUP_NEED_LOGIN: resStr = Device::getPlatformString(44); break;
        case Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD: resStr = Device::getPlatformString(45); break;
        case Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_NUMBER: resStr = Device::getPlatformString(46); break;
        case Res::Strings::PF_CREATECHAR_BUTTON_TITLE_RETAKE: resStr = Device::getPlatformString(47); break;
        case Res::Strings::PF_CREATECHAR_BUTTON_TITLE_SAVE: resStr = Device::getPlatformString(48); break;
        case Res::Strings::PF_CREATECHAR_BUTTON_TITLE_CANCEL: resStr = Device::getPlatformString(49); break;
        case Res::Strings::CC_LIST_BACKGROUND_MAKE: resStr = Device::getPlatformString(50); break;
        case Res::Strings::CC_LIST_BACKGROUND_WARNING_DELETE: resStr = Device::getPlatformString(51); break;
        case Res::Strings::CC_EXPANDEDBG_NEED_UNLOCK_COIN: resStr = Device::getPlatformString(52); break;
        case Res::Strings::CC_EXPANDEDBG_NEED_UNLOCK_COIN_1: resStr = Device::getPlatformString(53); break;
        case Res::Strings::CC_EXPANDEDBG_NEED_UNLOCK_COIN_2: resStr = Device::getPlatformString(54); break;
        case Res::Strings::CC_EXPANDEDBG_NEED_UNLOCK_COIN_LIMITED_PERIOD: resStr = Device::getPlatformString(55); break;
        case Res::Strings::CC_EXPANDEDBG_NEED_UNLOCK_COIN_NUMBER: resStr = Device::getPlatformString(56); break;
        case Res::Strings::PF_LOGIN_TEXTFIELD_EMAIL: resStr = Device::getPlatformString(57); break;
        case Res::Strings::PF_LOGIN_TEXTFIELD_PASSWORD: resStr = Device::getPlatformString(58); break;
        case Res::Strings::PF_LOGIN_BUTTON_TITLE_LOGIN: resStr = Device::getPlatformString(59); break;
        case Res::Strings::PF_LOGIN_BUTTON_TITLE_FORGOT: resStr = Device::getPlatformString(60); break;
        case Res::Strings::PF_LOGIN_BUTTON_TITLE_SIGNUP: resStr = Device::getPlatformString(61); break;
        case Res::Strings::PF_LGOIN_BUTTON_FBLOGIN: resStr = Device::getPlatformString(62); break;
        case Res::Strings::PF_LOGIN_POPUP_ERROR_FBLOGIN: resStr = Device::getPlatformString(63); break;
        case Res::Strings::PF_LOGIN_BUTTON_TITLE_SKIP: resStr = Device::getPlatformString(64); break;
        case Res::Strings::PF_FORGOT_NOTICE_FORGOT_PASSWORD: resStr = Device::getPlatformString(65); break;
        case Res::Strings::PF_FORGOT_TEXTFIELD_EMAIL: resStr = Device::getPlatformString(66); break;
        case Res::Strings::PF_FORGOT_BUTTON_TITLE_SEND: resStr = Device::getPlatformString(67); break;
        case Res::Strings::PF_FORGOT_POPUP_CONFIRM_PASSWORD: resStr = Device::getPlatformString(68); break;
        case Res::Strings::PF_SIGNUP_TEXTFIELD_EMAIL: resStr = Device::getPlatformString(69); break;
        case Res::Strings::PF_SIGNUP_TEXTFIELD_PASSWORD: resStr = Device::getPlatformString(70); break;
        case Res::Strings::PF_SIGNUP_TEXTFIELD_PASSWORD_CONDITION: resStr = Device::getPlatformString(71); break;
        case Res::Strings::PF_SIGNUP_TEXTFIELD_PASSWORD_CONFIRM: resStr = Device::getPlatformString(72); break;
        case Res::Strings::PF_SIGNUP_TEXTFIELD_BIRTHYEAR: resStr = Device::getPlatformString(73); break;
        case Res::Strings::PF_SIGNUP_TEXTFIELD_CITY: resStr = Device::getPlatformString(74); break;
        case Res::Strings::PF_SIGNUP_RADIOTITLE_MALE: resStr = Device::getPlatformString(75); break;
        case Res::Strings::PF_SIGNUP_RADIOTITLE_FEMALE: resStr = Device::getPlatformString(76); break;
        case Res::Strings::PF_SIGNUP_CHECKBOX_TITLE_AGREE: resStr = Device::getPlatformString(77); break;
        case Res::Strings::PF_SIGNUP_BUTTON_TITLE_SIGNUP: resStr = Device::getPlatformString(78); break;
        case Res::Strings::PF_SIGNUP_POPUP_CANCEL_SIGNUP: resStr = Device::getPlatformString(79); break;
        case Res::Strings::PF_SIGNUP_POPUP_BUTTON_TITLE_OK: resStr = Device::getPlatformString(80); break;
        case Res::Strings::PF_SIGNUP_POPUP_BUTTON_TITLE_CANCEL: resStr = Device::getPlatformString(81); break;
        case Res::Strings::PF_SIGNUP_POPUP_TITLE_BIRTHYEAR: resStr = Device::getPlatformString(82); break;
        case Res::Strings::PF_SIGNUP_BUTTON_TITLE_SET: resStr = Device::getPlatformString(83); break;
        case Res::Strings::PF_SIGNUP_PUPUP_TITLE_SELECT_COUNTRY: resStr = Device::getPlatformString(84); break;
        case Res::Strings::PF_SIGNUP_COUNTRY_LIST_OTHER: resStr = Device::getPlatformString(85); break;
        case Res::Strings::PF_SIGNUP_PUPUP_TITLE_SELECT_CITY: resStr = Device::getPlatformString(86); break;
        case Res::Strings::PF_SIGNUP_CHECKBOX_TITLE_AGREE_TERMS: resStr = Device::getPlatformString(87); break;
        case Res::Strings::PF_SIGNUP_CHECKBOX_TITLE_AGREE_PRIVACY: resStr = Device::getPlatformString(88); break;
        case Res::Strings::PF_SIGNUP_BUTTON_TITLE_NEXT: resStr = Device::getPlatformString(89); break;
        case Res::Strings::CC_JOURNEYMAP_BUTTON_INTRO: resStr = Device::getPlatformString(90); break;
        case Res::Strings::CC_JOURNEYMAP_BUTTON_FINAL: resStr = Device::getPlatformString(91); break;
        case Res::Strings::CC_JOURNEYMAP_BOOKTITLE_DEFAULT: resStr = Device::getPlatformString(92); break;
        case Res::Strings::CC_JOURNEYMAP_SEPARATOR_BOOKNAME: resStr = Device::getPlatformString(93); break;
        case Res::Strings::CC_JOURNEYMAP_BOOKAUTHOR_DEFAULT: resStr = Device::getPlatformString(94); break;
        case Res::Strings::CC_JOURNEYMAP_BUTTON_TITLE_PLAY: resStr = Device::getPlatformString(95); break;
        case Res::Strings::CC_JOURNEYMAP_MENU_TITLE_DOWNLOADDEVICE: resStr = Device::getPlatformString(96); break;
        case Res::Strings::CC_JOURNEYMAP_MENU_TITLE_SHARE: resStr = Device::getPlatformString(97); break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_NOTICCE_DOWNLOADDEVICE: resStr = Device::getPlatformString(98); break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_NOTICE_SHARE: resStr = Device::getPlatformString(99); break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_TITLE_SHARE_LIST: resStr = Device::getPlatformString(100); break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_SHARELIST_FACEBOOK: resStr = Device::getPlatformString(101); break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_SHARELIST_YOUTUBE: resStr = Device::getPlatformString(102); break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_ERROR_SHARE: resStr = Device::getPlatformString(103); break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_WARNING_DELETE_PAGE: resStr = Device::getPlatformString(104); break;
        case Res::Strings::CC_JOURNEYMAP_BUTTON_TITLE_CANCEL: resStr = Device::getPlatformString(105); break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_WARNING_CANCEL_CONVERT: resStr = Device::getPlatformString(106); break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_WARNING_NOTENOUGH_STORAGE: resStr = Device::getPlatformString(107); break;
        case Res::Strings::PF_SHARECOMMENT_POPUP_TITLE: resStr = Device::getPlatformString(108); break;
        case Res::Strings::PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_FACEBOOK: resStr = Device::getPlatformString(109); break;
        case Res::Strings::PF_SHARECOMMNET_POPUP_HASHTAG_FACEBOOK: resStr = Device::getPlatformString(110); break;
        case Res::Strings::PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_YOUTUBE: resStr = Device::getPlatformString(111); break;
        case Res::Strings::PF_SHARECOMMNET_POPUP_HASHTAG_YOUTUBE: resStr = Device::getPlatformString(112); break;
        case Res::Strings::CC_SETPOSITION_BUTTON_TITLE_RECORD: resStr = Device::getPlatformString(113); break;
        case Res::Strings::CC_SETPOSITION_POPUP_NOTICE_AUTOSELECT_LASTPAGE: resStr = Device::getPlatformString(114); break;
        case Res::Strings::CC_SETPOSITION_POPUP_WARNING_LIMITED_REDUCED: resStr = Device::getPlatformString(115); break;
        case Res::Strings::CC_SETPOSITION_POPUP_WARNING_ITEM_REUNLOCK: resStr = Device::getPlatformString(116); break;
        case Res::Strings::CC_SETPOSTION_POPUP_WARNING_DELETE_CHARACTER: resStr = Device::getPlatformString(117); break;
        case Res::Strings::CC_SETPOSTION_POPUP_WARNING_DELETE_BACKGROUND: resStr = Device::getPlatformString(118); break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_NONE: resStr = Device::getPlatformString(119); break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_JOLLY: resStr = Device::getPlatformString(120); break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_DREAM: resStr = Device::getPlatformString(121); break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_BRAVE: resStr = Device::getPlatformString(122); break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_SHINE: resStr = Device::getPlatformString(123); break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_BRISK: resStr = Device::getPlatformString(124); break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_ANGRY: resStr = Device::getPlatformString(125); break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_SLEEPY: resStr = Device::getPlatformString(126); break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_ADVENTURE: resStr = Device::getPlatformString(127); break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_CRUISE: resStr = Device::getPlatformString(128); break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_JUNGLE: resStr = Device::getPlatformString(129); break;
        case Res::Strings::CC_RECORDING_BUTTON_TITLE_DONE: resStr = Device::getPlatformString(130); break;
        case Res::Strings::CC_RECORDING_POPUP_WARNING_WITHOUT_SAVING: resStr = Device::getPlatformString(131); break;
        case Res::Strings::CC_RECORDING_BUTTON_TITLE_RECODING: resStr = Device::getPlatformString(132); break;
        case Res::Strings::CC_RECORDING_BUTTON_TITLE_RERECORDING: resStr = Device::getPlatformString(133); break;
        case Res::Strings::CC_RECORDING_BUTTON_TITLE_SAVE: resStr = Device::getPlatformString(134); break;
        case Res::Strings::CC_RECORDING_POPUP_WARNING_RERECORDING: resStr = Device::getPlatformString(135); break;
//        case Res::Strings::CC_RECORDING_POPUP_WARNING_WITHOUT_SAVING: resStr = Device::getPlatformString(136); break;
        case Res::Strings::CC_PLAY_PAGE_TITLE_INTRO: resStr = Device::getPlatformString(137); break;
        case Res::Strings::CC_PLAY_PAGE_TITLE_PAGE_N: resStr = Device::getPlatformString(138); break;
        case Res::Strings::CC_PLAY_PAGE_TITLE_FINAL: resStr = Device::getPlatformString(139); break;
        case Res::Strings::PF_SETTING_NOTICE_MORE_ADVANTAGE_LOGIN: resStr = Device::getPlatformString(140); break;
        case Res::Strings::PF_SETTING_BUTTON_TITLE_LOGIN: resStr = Device::getPlatformString(141); break;
        case Res::Strings::PF_SETTING_SOUND: resStr = Device::getPlatformString(142); break;
        case Res::Strings::PF_SETTING_BUTTON_TITLE_ABOUT: resStr = Device::getPlatformString(143); break;
        case Res::Strings::PF_SETTING_BUTTON_TITLE_HELP: resStr = Device::getPlatformString(144); break;
        case Res::Strings::PF_SETTING_BUTTON_TITLE_EDIT: resStr = Device::getPlatformString(145); break;
        case Res::Strings::PF_SETTING_BUTTON_TITLE_USAGE_COIN: resStr = Device::getPlatformString(146); break;
        case Res::Strings::PF_SETTING_BUTTON_TITLE_DELETE_ACCOUNT: resStr = Device::getPlatformString(147); break;
        case Res::Strings::PF_EDITACCOUNT_TEXTFIELD_CURRENT_PASSWORD: resStr = Device::getPlatformString(148); break;
        case Res::Strings::PF_EDITACCOUNT_TEXTFIELD_PASSWORD: resStr = Device::getPlatformString(149); break;
        case Res::Strings::PF_EDITACCOUNT_TEXTFIELD_PASSWORD_CONDITION: resStr = Device::getPlatformString(150); break;
        case Res::Strings::PF_EDITACCOUNT_TEXTFIELD_CONFIRM_PASSWORD: resStr = Device::getPlatformString(151); break;
        case Res::Strings::PF_EDITACCOUNT_BUTTON_TITLE_SAVE: resStr = Device::getPlatformString(152); break;
        case Res::Strings::PF_USAGECOIN_NOTICE: resStr = Device::getPlatformString(153); break;
        case Res::Strings::PF_USAGECOIN_LISTFIELD_DETAILS: resStr = Device::getPlatformString(154); break;
        case Res::Strings::PF_USAGECOIN_LISTFIELD_DATE: resStr = Device::getPlatformString(155); break;
        case Res::Strings::PF_USAGECOIN_BUTTON_TITLE_HELP: resStr = Device::getPlatformString(156); break;
        case Res::Strings::PF_USAGECOIN_NOTICE_NOHISTORY: resStr = Device::getPlatformString(157); break;
        case Res::Strings::PF_USAGECOIN_UNLIMITED: resStr = Device::getPlatformString(158); break;
        case Res::Strings::PF_USAGECOIN_LIMITED_PERIOD: resStr = Device::getPlatformString(159); break;
        case Res::Strings::PF_DELETEACCOUNT_NOTICE: resStr = Device::getPlatformString(160); break;
        case Res::Strings::PF_DELETEACCOUNT_CHECKBOX_CONFIRM: resStr = Device::getPlatformString(161); break;
        case Res::Strings::PF_DELETEACCOUNT_TEXTFIELD_CONFIRM_PASSWORD: resStr = Device::getPlatformString(162); break;
        case Res::Strings::PF_DELETEACCOUNT_BUTTON_TITLE_DELETE: resStr = Device::getPlatformString(163); break;
        case Res::Strings::PF_DELETEACCOUNT_TABLE_TITLEE_DOLECOIN: resStr = Device::getPlatformString(164); break;
        case Res::Strings::PF_DELETEACCOUNT_TABLE_TITLEE_BOBBY: resStr = Device::getPlatformString(165); break;
        case Res::Strings::PF_DELETEACCOUNT_TABLE_SUBTITLE_DOLECOIN: resStr = Device::getPlatformString(166); break;
        case Res::Strings::PF_DELETEACCOUNT_TABLE_SUBTITLE_BOBBY: resStr = Device::getPlatformString(167); break;
        case Res::Strings::PF_DELETEACCOUNT_TABLE_SUBTITLE_BOBBY_ADD: resStr = Device::getPlatformString(168); break;
        case Res::Strings::PF_ABOUT_APPTITLE: resStr = Device::getPlatformString(169); break;
        case Res::Strings::PF_ABOUT_SERVICE_INTRODUCE: resStr = Device::getPlatformString(170); break;
        case Res::Strings::PF_ABOUT_BUTTON_TITLE_TERMS: resStr = Device::getPlatformString(171); break;
        case Res::Strings::PF_ABOUT_TITLE_SERVICE: resStr = Device::getPlatformString(172); break;
        case Res::Strings::PF_ABOUT_TITLE_PRIVACY: resStr = Device::getPlatformString(173); break;
        case Res::Strings::PF_HELP_BUTTON_TITLE_FAQ: resStr = Device::getPlatformString(174); break;
        case Res::Strings::PF_HELP_TEXTFIELD_EMAIL: resStr = Device::getPlatformString(175); break;
        case Res::Strings::PF_HELP_TEXTFIELD_FEEDBACK: resStr = Device::getPlatformString(176); break;
        case Res::Strings::PF_HELP_DEVICE: resStr = Device::getPlatformString(177); break;
        case Res::Strings::PF_HELP_WARNING_PRIVACYINFO: resStr = Device::getPlatformString(178); break;
        case Res::Strings::PF_HELP_BUTTON_TITLE_SEND: resStr = Device::getPlatformString(179); break;
        case Res::Strings::CC_GUIDE_HOME: resStr = Device::getPlatformString(180); break;
        case Res::Strings::CC_GUIDE_JOURNEYMAP_NEWPAGE: resStr = Device::getPlatformString(181); break;
        case Res::Strings::CC_GUIDE_JOURNEYMAP_ALLPLAY: resStr = Device::getPlatformString(182); break;
        case Res::Strings::CC_GUIDE_SETPOSITION_SELECT_ITEM: resStr = Device::getPlatformString(183); break;
        case Res::Strings::CC_GUIDE_SETPOSITION_SELECT_BACKGROUND: resStr = Device::getPlatformString(184); break;
        case Res::Strings::CC_GUIDE_SETPOSITION_SELECT_MUSIC: resStr = Device::getPlatformString(185); break;
        case Res::Strings::CC_GUIDE_SETPOSITION_DELETE_ITEM: resStr = Device::getPlatformString(186); break;
        case Res::Strings::CC_GUIDE_SETPOSITION_RECORDING: resStr = Device::getPlatformString(187); break;
        case Res::Strings::PF_GUIDE_EDIT_PICTURE: resStr = Device::getPlatformString(188); break;
        case Res::Strings::PF_GUIDE_LOGIN_SIGNUP: resStr = Device::getPlatformString(189); break;
        case Res::Strings::PF_GUIDE_LOGIN_EARNCOIN: resStr = Device::getPlatformString(190); break;
        case Res::Strings::PF_GUIDE_LOGIN_GETITEM: resStr = Device::getPlatformString(191); break;
        case Res::Strings::PF_GUIDE_LOGIN_SHARE: resStr = Device::getPlatformString(192); break;
        case Res::Strings::PF_LOGIN_POPUP_ERROR_NOTAVAILABLE_EMAIL: resStr = Device::getPlatformString(193); break;
        case Res::Strings::CC_HOME_POPUP_UNLOCK_ITEM_MESSAGE: resStr = Device::getPlatformString(194); break;
        case Res::Strings::CC_JOURNEYMAP__DOWNLOAD_DEVICE_COMPLETE: resStr = Device::getPlatformString(195); break;
        case Res::Strings::CC_JOURNEYMAP__SHARE_COMPLETE: resStr = Device::getPlatformString(196); break;
        case Res::Strings::CC_SETPOSITION_POPUP_WARNING_ITEM_FULL: resStr = Device::getPlatformString(197); break;
        case Res::Strings::PF_EDITACCOUNT_POPUP_INPUT_PASSWORD: resStr = Device::getPlatformString(198); break;
        case Res::Strings::PF_EDITACCOUNT_POPUP_SAVE_COMPLETE: resStr = Device::getPlatformString(199); break;
        case Res::Strings::PF_HELP_POPUP_SEND_COMPLETE: resStr = Device::getPlatformString(200); break;
        case Res::Strings::PF_DELETEACCOUNT_DELETE_COMPLETE: resStr = Device::getPlatformString(201); break;
        case Res::Strings::PF_PROMOTION_CHECKBOX: resStr = Device::getPlatformString(202); break;
//        case Res::Strings::PF_DOLECOIN_ABOUTCOIN_A: resStr = Device::getPlatformString(203); break;
//        case Res::Strings::PF_DOLECOIN_HOWEARN_A: resStr = Device::getPlatformString(204); break;
        case Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_DAYS: resStr = Device::getPlatformString(205); break;
        case Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_HOURS: resStr = Device::getPlatformString(206); break;
        case Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_HOUR: resStr = Device::getPlatformString(207); break;
        case Res::Strings::CC_JOURNEYMAP_SHARE_PROGRESS_NOTICE: resStr = Device::getPlatformString(208); break;
        case Res::Strings::CC_RECORDING_BUTTON_TITLE_RECOD: resStr = Device::getPlatformString(209); break;
        case Res::Strings::CC_EXPANDEDITEM_NEED_UNLOCK_COIN_CHA: resStr = Device::getPlatformString(210); break;
        case Res::Strings::CC_POPUP_CANCEL_SHARE: resStr = Device::getPlatformString(211); break;
        case Res::Strings::PF_SIGNUP_POPUP_BUTTON_TITLE_OK_SIGN_UP: resStr = Device::getPlatformString(212); break;
        case Res::Strings::CC_PRLOAD_BOOKTITLE_1: resStr = Device::getPlatformString(213); break;
        case Res::Strings::CC_PRLOAD_BOOKTITLE_2: resStr = Device::getPlatformString(214); break;
        case Res::Strings::CM_POPUP_BUTTON_TITLE_YES: resStr = Device::getPlatformString(215); break;
        case Res::Strings::CM_POPUP_BUTTON_TITLE_NO: resStr = Device::getPlatformString(216); break;
        case Res::Strings::PF_NET_ERROR_EMAIL_NOT_AVAILABLE: resStr = Device::getPlatformString(217); break;
    }
    return resStr;
}


NS_CC_END

#endif // CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

// this method is called by Cocos2dxBitmap
extern "C"
{
    /**
    * this method is called by java code to init width, height and pixels data
    */
    JNIEXPORT void JNICALL Java_org_cocos2dx_lib_Cocos2dxBitmap_nativeInitBitmapDC(JNIEnv*  env, jobject thiz, int width, int height, jbyteArray pixels)
    {
        int size = width * height * 4;
        cocos2d::BitmapDC& bitmapDC = cocos2d::sharedBitmapDC();
        bitmapDC._width = width;
        bitmapDC._height = height;
        bitmapDC._data = (unsigned char*)malloc(sizeof(unsigned char) * size);
        env->GetByteArrayRegion(pixels, 0, size, (jbyte*)bitmapDC._data);
    }
};
