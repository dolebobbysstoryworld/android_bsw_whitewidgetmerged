/*
 * Copyright 2013 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.cocos2dx.lib;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.media.MediaRecorder;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.os.Environment;
import android.os.Trace;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;


/**
 * Record video of a game in progress.
 *
 * Not generally thread-safe.  The recorder should be set up by the Activity before the
 * Render thread is started, then updates should be made from the GLSurfaceView render thread.
 */
public class Cocos2dxScreenRecorder {
    private static final String TAG = Cocos2dxScreenRecorder.class.getSimpleName();

    // parameters for the encoder
    private static final String MIME_TYPE_VIDEO = "video/avc";    // H.264 Advanced Video Coding
    private static final String MIME_TYPE_AUDIO = "audio/mp4a-latm"; // AAC audio
    private static final int FRAME_RATE = 30;               // 30fps
    private static final int IFRAME_INTERVAL = 10;          // 10 seconds between I-frames
    private static int VIDEO_WIDTH = 1280;            // 720p
    private static int VIDEO_HEIGHT = 720;
    private static final int BIT_RATE = 6000000;            // 6Mbps
    private static final int SAMPLE_RATE = 44100;

    private static final Object sSyncObj = new Object();
    private static volatile Cocos2dxScreenRecorder sInstance;

    private static File sOutputFile;
    private MediaCodec mVideoEncoder;
    private Cocos2dxInputSurface mInputSurface;
    private MediaCodec.BufferInfo mBufferInfo;
    private MediaCodec.BufferInfo mAudioBufferInfo;
    private MediaMuxer mMuxer;
    private MediaExtractor mVoiceExtractor;
    private ByteBuffer mVoiceInputBuffer;
    private ArrayList<String> mVoiceFile;
    private AssetManager mAssetManager;
    private long mVoicePresentTimeOffset = 0L;
    private boolean mMuxerStarted;

    private int mViewportWidth, mViewportHeight;
    private int mViewportXoff, mViewportYoff;
    private final float mProjectionMatrix[] = new float[16];

    public static float ARENA_WIDTH = 1196.f;
    public static float ARENA_HEIGHT = 768.f;

    private boolean mRecord;
    private boolean mPauseRecord;
    private long mPauseTime;
    private long mAudioStartTime = 0;
    private long mVideoStartTime = 0;
    private boolean mStopReceived;
    private int mNumTracksAdded = 0;
    private TrackIndex mTrackIndex = new TrackIndex();
    private TrackIndex mAudioTrackIndex = new TrackIndex();
    private int mAudioBufferSize = 0;
    private byte sData[] = null;

    private long mTimeline;
    private long mChangeSceneUs;
    private boolean mChangeScene;

    class TrackIndex {
        int index = 0;
    }

    // Muxer state
    private static final int TOTAL_NUM_TRACKS = 1;
    private static final int AUDIO_BUFFER_INCREASE_FACTOR = 10;

    /** singleton */
    private Cocos2dxScreenRecorder() {
    	mAudioBufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO,
    			AudioFormat.ENCODING_PCM_16BIT);
    	sData = new byte[mAudioBufferSize];
    }

    /**
     * Retrieves the singleton, creating the instance if necessary.
     */
    public static Cocos2dxScreenRecorder getInstance() {
        if (sInstance == null) {
            synchronized (sSyncObj) {
                if (sInstance == null) {
                    sInstance = new Cocos2dxScreenRecorder();
                }
            }
        }
        return sInstance;
    }

    public static int[] getVideoResulution() {
    	return new int[] { VIDEO_WIDTH, VIDEO_HEIGHT };
    }

    public static String getVideoName() {
    	return (sOutputFile == null ? "" : sOutputFile.toString());
    }

    public void changeScene() {
    	mChangeScene = true;
    	mChangeSceneUs = (System.nanoTime() - mVideoStartTime) / 1000;
    }

    /**
     * Creates a new encoder, and prepares the output file.
     * <p>
     * We can't set up the InputSurface yet, because we want the EGL contexts to share stuff,
     * and the primary context may not have been configured yet.
     */
    public void prepareEncoder(Context context) {
        MediaCodec encoderVideo;
        MediaMuxer muxer;
        MediaExtractor extractorVoice = null;
        MediaExtractor extractorBGM = null;
        ByteBuffer BGMInputBuffer = null;
        ByteBuffer voiceInputBuffer = null;

        File path = context.getCacheDir();
        mAssetManager = context.getAssets();
        sOutputFile = new File(path, "video.mp4");
        Log.d(TAG, "Video recording to file " + sOutputFile);
        mBufferInfo = new MediaCodec.BufferInfo();
        mAudioBufferInfo = new MediaCodec.BufferInfo();

        try {
            // Create and configure the MediaFormat.
            MediaFormat videoFormat = MediaFormat.createVideoFormat(MIME_TYPE_VIDEO,
                    VIDEO_WIDTH, VIDEO_HEIGHT);
            videoFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT,
                    MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface);
            videoFormat.setInteger(MediaFormat.KEY_BIT_RATE, BIT_RATE);
            videoFormat.setInteger(MediaFormat.KEY_FRAME_RATE, FRAME_RATE);
            videoFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, IFRAME_INTERVAL);

            // Create a MediaCodec encoder, and configure it with our format.
            encoderVideo = MediaCodec.createEncoderByType(MIME_TYPE_VIDEO);
            encoderVideo.configure(videoFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);

            // Create a MediaMuxer.  We can't add the video track and start() the muxer here,
            // because our MediaFormat doesn't have the Magic Goodies.  These can only be
            // obtained from the encoder after it has started processing data.
            muxer = new MediaMuxer(sOutputFile.getAbsolutePath(),
                    MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
            mMuxerStarted = false;

            boolean validFile = true;
            if(mVoiceFile.size() > 0) {
            	extractorVoice = new MediaExtractor();
            	voiceInputBuffer = ByteBuffer.allocate(256 * 1024);
				try {
					String destFile = mVoiceFile.remove(0);
					File tempFile = new File(destFile);
					if(tempFile.exists() && tempFile.length() > 0)
	            	extractorVoice.setDataSource(destFile);
					else
						validFile = false;
				} catch(IOException e) {
					e.printStackTrace();
				}

				if(validFile) {
            	extractorVoice.selectTrack(0);
    			mAudioTrackIndex.index = muxer.addTrack(extractorVoice.getTrackFormat(0));
				} else {
					extractorVoice.release();
					extractorVoice = null;
				}
            }

            mVideoEncoder = encoderVideo;
            mMuxer = muxer;
            mVoiceExtractor = extractorVoice;
            mVoiceInputBuffer = voiceInputBuffer;

            mVoicePresentTimeOffset = 0L;
            mChangeSceneUs = 0L;
            mChangeScene = false;
        } catch (Exception ex) {
            Log.w(TAG, "Something failed during recorder init: " + ex);
            releaseEncoder();
        }

        configureViewport();
    }

    /**
     * Finishes setup.  Call with the primary EGL context set.
     */
    public void firstTimeSetup() {
        if (!isRecording()) {
            // not recording, or already initialized
            return;
        }

        Cocos2dxInputSurface inputSurface;

        try {
    		inputSurface = new Cocos2dxInputSurface(mVideoEncoder.createInputSurface());
            mVideoEncoder.start();
        	mInputSurface = inputSurface;
        } catch (Exception ex) {
            Log.w(TAG, "Something failed during recorder init: " + ex);
            ex.printStackTrace();
            releaseEncoder();
        }
    }

    /**
     * Releases encoder resources.  May be called after partial / failed initialization.
     */
    private void releaseEncoder() {
        Log.d(TAG, "releasing encoder objects");
        if (mVideoEncoder != null) {
        	mVideoEncoder.stop();
            mVideoEncoder.release();
            mVideoEncoder = null;
        }
        if (mInputSurface != null) {
            mInputSurface.release();
            mInputSurface = null;
        }
        if(mVoiceExtractor != null) {
        	mVoiceExtractor.release();
        	mVoiceExtractor = null;
        	mVoiceInputBuffer = null;
        }
        if (mMuxer != null) {
        	if(mMuxerStarted)
        		mMuxer.stop();
            mMuxer.release();
            mMuxer = null;
        }
        mNumTracksAdded = 0;
        mVideoStartTime = 0;
        mStopReceived = true;
        mMuxerStarted = false;
    }

    private void stopEncoder() {
        mStopReceived = true;
        
        if (mVideoEncoder != null) {
        	mVideoEncoder.stop();
        	mVideoEncoder.release();
        	mVideoEncoder = null;
        }
        if(mVoiceExtractor != null) {
        	mVoiceExtractor.release();
        	mVoiceExtractor = null;
        	mVoiceInputBuffer = null;
        }
        if (mMuxer != null) {
        	if(mMuxerStarted) {
        		try {
        		mMuxer.stop();
                	mMuxer.release();
        		} catch(IllegalStateException e) {
        			e.printStackTrace();
        		}
        	}
        	mMuxer = null;
        }
        mNumTracksAdded = 0;
        mVideoStartTime = 0;
        mMuxerStarted = false;
    }

    /**
     * Returns true if a recording is in progress.
     */
    public boolean isRecording() {
        return (mVideoEncoder != null && mRecord);
    }

    /**
     * Configures our viewport projection matrix.
     * <p>
     * We always render at the surface's resolution, which matches the video encoder resolution.
     * The resolution and orientation of the device itself are irrelevant -- we're not recording
     * what's on screen, but rather what would be on screen if the device resolution matched our
     * video parameters.
     */
    private void configureViewport() {
        float arenaRatio = ARENA_HEIGHT / ARENA_WIDTH;
        int x, y, viewWidth, viewHeight;

        if (VIDEO_HEIGHT > (int) (VIDEO_WIDTH * arenaRatio)) {
            // limited by narrow width; restrict height
            viewWidth = VIDEO_WIDTH;
            viewHeight = (int) (VIDEO_WIDTH * arenaRatio);
        } else {
            // limited by short height; restrict width
            viewHeight = VIDEO_HEIGHT;
            viewWidth = (int) (VIDEO_HEIGHT / arenaRatio);
        }
        x = (VIDEO_WIDTH - viewWidth) / 2;
        y = (VIDEO_HEIGHT - viewHeight) / 2;

        mViewportXoff = x;
        mViewportYoff = y;
        mViewportWidth = viewWidth;
        mViewportHeight = viewHeight;

        Log.d("Viewport: ", "viewportXOff=" + Integer.valueOf(mViewportXoff) + 
        		" viewportYOff=" + Integer.valueOf(mViewportYoff) +
        		" mViewportWidth=" + Integer.valueOf(mViewportWidth) +
        		" mViewportHeight=" + Integer.valueOf(mViewportHeight));
        
        Matrix.orthoM(mProjectionMatrix, 0,  0, ARENA_WIDTH,
                0, ARENA_HEIGHT,  -1, 1);
    }

    /**
     * Returns the projection matrix.
     *
     * @param dest a float[16]
     */
    public void getProjectionMatrix(float[] dest) {
        System.arraycopy(dest, 0, mProjectionMatrix, 0, mProjectionMatrix.length);
    }

    /**
     * Sets the viewport for video.
     */
    public void setViewport() {
        GLES20.glViewport(mViewportXoff, mViewportYoff, mViewportWidth, mViewportHeight);
        Log.d("Viewport: ", "viewportXOff=" + Integer.valueOf(mViewportXoff) + 
        		" viewportYOff=" + Integer.valueOf(mViewportYoff) +
        		" mViewportWidth=" + Integer.valueOf(mViewportWidth) +
        		" mViewportHeight=" + Integer.valueOf(mViewportHeight));
    }

    /**
     * Configures EGL to output to our InputSurface.
     */
    public void makeCurrent() {
        mInputSurface.makeCurrent();
    }

    /**
     * Sends the current frame to the recorder.  Before doing so, we drain any pending output.
     */
    public void swapBuffers() {
        if (!isRecording()) {
            return;
        }

        if(mPauseRecord)
        	return;

        drainEncoder(mVideoEncoder, mBufferInfo, mTrackIndex, false);
        if (mVideoStartTime == 0) {
        	mVideoStartTime = System.nanoTime();
        }

        mInputSurface.setPresentationTime(System.nanoTime() - mVideoStartTime);
        mInputSurface.swapBuffers();
        
        drainVoiceData();
    }

    /**
     * Extracts all pending data from the encoder and forwards it to the muxer.
     * <p/>
     * If endOfStream is not set, this returns when there is no more data to drain.  If it
     * is set, we send EOS to the encoder, and then iterate until we see EOS on the output.
     * Calling this with endOfStream set should be done once, right before stopping the muxer.
     * <p/>
     * We're just using the muxer to get a .mp4 file (instead of a raw H.264 stream).  We're
     * not recording audio.
     */
    private void drainEncoder(MediaCodec encoder, MediaCodec.BufferInfo bufferInfo, TrackIndex trackIndex, boolean endOfStream) {
        final int TIMEOUT_USEC = 100;
        ByteBuffer[] encoderOutputBuffers = encoder.getOutputBuffers();
        while (true) {
            int encoderStatus = encoder.dequeueOutputBuffer(bufferInfo, TIMEOUT_USEC);
            if (encoderStatus == MediaCodec.INFO_TRY_AGAIN_LATER) {
                // no output available yet
                if (!endOfStream) {
                    break;      // out of while
                } else {
                    Log.d(TAG, "no output available, spinning to await EOS");
                    return;
                }
            } else if (encoderStatus == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                // not expected for an encoder
                encoderOutputBuffers = encoder.getOutputBuffers();
            } else if (encoderStatus == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                // should happen before receiving buffers, and should only happen once
                if (mMuxerStarted) {
                    throw new RuntimeException("format changed after muxer start");
                }
                MediaFormat newFormat = encoder.getOutputFormat();

                // now that we have the Magic Goodies, start the muxer
                trackIndex.index = mMuxer.addTrack(newFormat);
                mNumTracksAdded++;
                Log.d(TAG, "encoder output format changed: " + newFormat + ". Added track index: " + trackIndex.index);
                if (mNumTracksAdded == TOTAL_NUM_TRACKS) {
                    mMuxer.start();
                    mMuxerStarted = true;
                    Log.i(TAG, "All tracks added. Muxer started");
                }
            } else if (encoderStatus < 0) {
                Log.w(TAG, "unexpected result from encoder.dequeueOutputBuffer: " + encoderStatus);
                // let's ignore it
            } else {
                ByteBuffer encodedData = encoderOutputBuffers[encoderStatus];
                if (encodedData == null) {
                    throw new RuntimeException("encoderOutputBuffer " + encoderStatus +
                            " was null");
                }

                if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                    // The codec config data was pulled out and fed to the muxer when we got
                    // the INFO_OUTPUT_FORMAT_CHANGED status.  Ignore it.
                    Log.d(TAG, "ignoring BUFFER_FLAG_CODEC_CONFIG");
                    bufferInfo.size = 0;
                }

                if (bufferInfo.size != 0) {
                    if (!mMuxerStarted) {
//                        throw new RuntimeException("muxer hasn't started");
                    	Log.d(TAG, "muxer hasn't started for track " + trackIndex.index);
                    	return;
                    }

                    // adjust the ByteBuffer values to match BufferInfo (not needed?)
                    encodedData.position(bufferInfo.offset);
                    encodedData.limit(bufferInfo.offset + bufferInfo.size);
                    
                    if(trackIndex.index == mAudioTrackIndex.index && mTimeline >= bufferInfo.presentationTimeUs) {
                    	Log.w(TAG, "skip write audio");
                    } else {
                    	mMuxer.writeSampleData(trackIndex.index, encodedData, bufferInfo);
                    }
                    if(trackIndex.index == mAudioTrackIndex.index)
                    	mTimeline = bufferInfo.presentationTimeUs;
                }

                encoder.releaseOutputBuffer(encoderStatus, false);

                if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    if (!endOfStream) {
                        Log.w(TAG, "reached end of stream unexpectedly");
                    } else {
                        Log.d(TAG, "end of stream reached");
                    }
                    break;      // out of while
                }
            }
        }
    }

    private void drainVoiceData() {
    	drainVoiceData(false);
    }

    private void drainVoiceData(boolean finish) {
    	if(!mMuxerStarted)
            return;

    	if(mVoiceExtractor == null) {
    		if(mChangeScene) {
    			if(mVoiceFile.size() > 0) {
    				mChangeScene = false;
    				mChangeSceneUs = 0L;

					mVoiceExtractor = new MediaExtractor();
					String destFile = null;
					File tempFile = null;
					try {
						destFile = null;
						tempFile = null;

						if(mVoiceFile.size() > 0)
							destFile = mVoiceFile.remove(0);
						if(destFile != null)
							tempFile = new File(destFile);

						if(tempFile != null) {
							if(!tempFile.exists() || tempFile.length() <= 0) {
								tempFile = null;
							}
						}

						if(tempFile == null) {
							mVoiceExtractor.release();
							mVoiceExtractor = null;
							return;
						}

						mVoiceExtractor.setDataSource(destFile);
					} catch(IOException e) {
						e.printStackTrace();
					}

					mVoiceExtractor.selectTrack(0);
				}
    		} else {
    			return;
    		}
    	}

		mAudioBufferInfo.offset = 0;
		mAudioBufferInfo.size = mVoiceExtractor.readSampleData(mVoiceInputBuffer, 0);
		if(mAudioBufferInfo.size < 0) {
			if(mChangeScene) {
				mChangeScene = false;
				mChangeSceneUs = 0L;

			// EOS
			if(mVoiceFile.size() > 0) {
					mVoicePresentTimeOffset = mAudioBufferInfo.presentationTimeUs;
				mVoiceExtractor.release();
				mVoiceExtractor = new MediaExtractor();
				String destFile = null;
				File tempFile = null;
				try {
						destFile = null;
						tempFile = null;
					
						if(mVoiceFile.size() > 0)
							destFile = mVoiceFile.remove(0);
						if(destFile != null)
							tempFile = new File(destFile);
	            		
						if(tempFile != null) {
							if(!tempFile.exists() || tempFile.length() <= 0) {
								tempFile = null;
							}
						}
	            		
						if(tempFile == null) {
						mVoiceExtractor.release();
						mVoiceExtractor = null;
						return;
	            	}

					mVoiceExtractor.setDataSource(destFile);
				} catch(IOException e) {
					e.printStackTrace();
        }

				mVoiceExtractor.selectTrack(0);
			} else {
				mVoiceExtractor.release();
				mVoiceExtractor = null;
				}
			}
		} else {
			if(mChangeScene) {
				mChangeScene = false;

				while(mChangeSceneUs > mAudioBufferInfo.presentationTimeUs
						&& mAudioBufferInfo.size > 0) {
					mAudioBufferInfo.presentationTimeUs = mVoicePresentTimeOffset + mVoiceExtractor.getSampleTime();
					mAudioBufferInfo.flags = mVoiceExtractor.getSampleFlags();
					mMuxer.writeSampleData(mAudioTrackIndex.index, mVoiceInputBuffer, mAudioBufferInfo);
					mVoiceExtractor.advance();

					mAudioBufferInfo.offset = 0;
					mAudioBufferInfo.size = mVoiceExtractor.readSampleData(mVoiceInputBuffer, 0);
				}
				mChangeSceneUs = 0L;

				// play next voice file
				if(mVoiceFile.size() > 0) {
					boolean validFile = true;
					mVoicePresentTimeOffset = mAudioBufferInfo.presentationTimeUs;
					mVoiceExtractor.release();
					mVoiceExtractor = new MediaExtractor();
					try {
						String destFile = mVoiceFile.remove(0);
						File tempFile = new File(destFile);
						if(tempFile.exists() && tempFile.length() > 0)
						mVoiceExtractor.setDataSource(destFile);
						else
							validFile = false;
					} catch(IOException e) {
						e.printStackTrace();
					}

					if(validFile) {
					mVoiceExtractor.selectTrack(0);
				} else {
					mVoiceExtractor.release();
					mVoiceExtractor = null;
			}
		} else {
					mVoiceExtractor.release();
					mVoiceExtractor = null;
				}
			} else {
				if(finish) {
					while(mAudioBufferInfo.size > 0) {
			mAudioBufferInfo.presentationTimeUs = mVoicePresentTimeOffset + mVoiceExtractor.getSampleTime();
			mAudioBufferInfo.flags = mVoiceExtractor.getSampleFlags();
			mMuxer.writeSampleData(mAudioTrackIndex.index, mVoiceInputBuffer, mAudioBufferInfo);
			mVoiceExtractor.advance();

						mAudioBufferInfo.offset = 0;
						mAudioBufferInfo.size = mVoiceExtractor.readSampleData(mVoiceInputBuffer, 0);
					}
					mVoiceExtractor.release();
					mVoiceExtractor = null;
				} else {
					mAudioBufferInfo.presentationTimeUs = mVoicePresentTimeOffset + mVoiceExtractor.getSampleTime();
					mAudioBufferInfo.flags = mVoiceExtractor.getSampleFlags();
					mMuxer.writeSampleData(mAudioTrackIndex.index, mVoiceInputBuffer, mAudioBufferInfo);
					mVoiceExtractor.advance();
				}
			}
        }
        }
        
    /**
     * Handles activity pauses (could be leaving the game, could be switching back to the
     * main activity).  Stop recording, shut the codec down.
     */
    public void onPaused() {
        if(mVideoEncoder != null)
        	drainEncoder(mVideoEncoder, mBufferInfo, mTrackIndex, true);
        drainVoiceData(true);
        stopEncoder();
    }

    public void record(boolean r) {
    	mRecord = r;
    	if(!mRecord)
    		mPauseRecord = false;
    }

    public void pause(boolean pause) {
    	mPauseRecord = pause;
    	if(mPauseRecord) {
    		mPauseTime = System.nanoTime();
    	} else {
    		mVideoStartTime += (System.nanoTime() - mPauseTime);
    		mPauseTime = 0L;
    	}
    }

    public void setAudioFileList(ArrayList<String> voice) {
    	mVoiceFile = voice;
        mVideoStartTime = 0L;
    }
}