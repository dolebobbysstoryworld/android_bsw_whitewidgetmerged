/****************************************************************************
Copyright (c) 2010-2013 cocos2d-x.org

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 ****************************************************************************/
package org.cocos2dx.lib;

import org.cocos2dx.lib.Cocos2dxHelper.Cocos2dxHelperListener;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.util.Log;
import android.widget.FrameLayout;

public abstract class Cocos2dxActivity extends Activity implements Cocos2dxHelperListener {
	// ===========================================================
	// Constants
	// ===========================================================

	private static final String TAG = Cocos2dxActivity.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================
	
	private Cocos2dxGLSurfaceView mGLSurfaceView;
	private Cocos2dxHandler mHandler;
	private static Context sContext = null;
	private static boolean sDebug = true;
	private static String sExtensionFileName;
	
	private int mEditBoxLeftBg = -1;
	private int mEditBoxRightBg = -1;
	private int mEditTextTopPadding = -1;
	private int mEditTextWidth = -1;
	private int mEditTextHeight = -1;
	private int mLayoutResId[] = new int[] {-1, -1};

	private String mTitle;
	private String mAuthor;
	private String mComment;
	private String mPassword;
	private boolean mEditActionShare;
	private int mShareType = 0;

	private ArrayList<String> mVoiceList;

	public static Context getContext() {
		return sContext;
	}
	
	public static void setDebugMode(boolean debug) {
		sDebug = debug;
	}

	static boolean isDebugMode() {
		return sDebug;
	}

	public static void setExtensionFileName(String filename) {
		sExtensionFileName = filename;
	}

	static String getExtensionFileName() {
		return sExtensionFileName;
	}

	// ===========================================================
	// Constructors
	// ===========================================================
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			ApplicationInfo ai = getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
			Bundle bundle = ai.metaData;
			try {
        		String libName = bundle.getString("android.app.lib_name");
        		System.loadLibrary(libName);
			} catch (Exception e) {
		 		// ERROR
			}
		} catch (PackageManager.NameNotFoundException e) {
		 	// ERROR
		}

		sContext = this;
    	this.mHandler = new Cocos2dxHandler(this);

    	this.init();

		Cocos2dxHelper.init(this);

		onPostCreate();
	}

	protected void onPostCreate() {
	}

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	@Override
	protected void onResume() {
		super.onResume();

		Cocos2dxHelper.onResume();
		this.mGLSurfaceView.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();

		Cocos2dxHelper.onPause();
		this.mGLSurfaceView.onPause();
	}

	@Override
	public void showDialog(final String pTitle, final String pMessage) {
		Message msg = new Message();
		msg.what = Cocos2dxHandler.HANDLER_SHOW_DIALOG;
		msg.obj = new Cocos2dxHandler.DialogMessage(pTitle, pMessage);
		this.mHandler.sendMessage(msg);
	}

	@Override
	public void showEditTextDialog(final String pTitle, final String pContent, final int pInputMode, final int pInputFlag, final int pReturnType, final int pMaxLength, final int pDialogMode, final int pDialogParam) { 
		Message msg = new Message();
		msg.what = Cocos2dxHandler.HANDLER_SHOW_EDITBOX_DIALOG;
		msg.obj = new Cocos2dxHandler.EditBoxMessage(pTitle, pContent, pInputMode, pInputFlag, pReturnType, pMaxLength, pDialogMode, pDialogParam);
		this.mHandler.sendMessage(msg);
	}
	
	@Override
	public void runOnGLThread(final Runnable pRunnable) {
//		this.mGLSurfaceView.queueEvent(pRunnable);
		Cocos2dxGLSurfaceView.getInstance().queueEvent(pRunnable);
	}

	// ===========================================================
	// Methods
	// ===========================================================
	public void init() {
		
    	// FrameLayout
        ViewGroup.LayoutParams framelayout_params =
            new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                       ViewGroup.LayoutParams.MATCH_PARENT);
        FrameLayout framelayout = new FrameLayout(this);
        framelayout.setLayoutParams(framelayout_params);

        // Cocos2dxEditText layout
        ViewGroup.LayoutParams edittext_layout_params =
            new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                       ViewGroup.LayoutParams.WRAP_CONTENT);
        Cocos2dxEditText edittext = new Cocos2dxEditText(this);
        edittext.setLayoutParams(edittext_layout_params);

        // ...add to FrameLayout
        //framelayout.addView(edittext);

        // Cocos2dxGLSurfaceView
        this.mGLSurfaceView = this.onCreateView();

        // ...add to FrameLayout
        framelayout.addView(this.mGLSurfaceView);

        postInitViews(framelayout);

        // Switch to supported OpenGL (ARGB888) mode on emulator
        if (isAndroidEmulator())
           this.mGLSurfaceView.setEGLConfigChooser(8 , 8, 8, 8, 16, 0);

        this.mGLSurfaceView.setCocos2dxRenderer(new Cocos2dxRenderer());
        this.mGLSurfaceView.setCocos2dxEditText(edittext);

        // Set framelayout as the content view
		setContentView(framelayout);
	}
	
	public void postInitViews(ViewGroup container) {
	}

	public void onEditActionFinish(boolean isDone) {
	}

	protected void setEditTextBackgroundImages(int leftId, int rightId) {
		mEditBoxLeftBg = leftId;
		mEditBoxRightBg = rightId;
	}

	protected int[] getEditTextBackgroundImages() {
		return new int[] { mEditBoxLeftBg, mEditBoxRightBg };
	}

	protected void setEditTextTopPadding(int paddingId) {
		mEditTextTopPadding = paddingId;
	}

	protected int getEditTextTopPadding() {
		return mEditTextTopPadding;
	}

	protected void setEditTextSize(int widthId, int heightId) {
		mEditTextWidth = widthId;
		mEditTextHeight = heightId;
	}

	protected int[] getEditTextSize() {
		return new int[] { mEditTextWidth, mEditTextHeight };
	}

	protected String getDeviceToken() {
		return "";
	}

	protected String getPlatformString(int id) {
		return "";
	}

	public void setEditTextContents(String[] contents) {
		mTitle = contents[0];
		mAuthor = contents[1];
	}

	public String[] getEditTextContents() {
		return new String[] { mTitle, mAuthor };
	}

	public void setShareComment(int type, String comment) {
		mEditActionShare = true;
		mShareType = type;
		if(type == Cocos2dxEditBoxDialog.SHARE_POPUP_FACEBOOK)
			mComment = new String(comment);
		else
		mComment = new String(comment + getHashTag(type));
	}

	public String getShareComment() {
		return mComment;
	}

	public int getShareType() {
		return mShareType;
	}

	public void setPassword(String password) {
		mPassword = password;
	}

	public String getPassword() {
		return mPassword;
	}

	public int[] getEditTextWidths() {
		return new int[] { -1, -1, -1 };
	}

	public int[] getEditTextColor() {
		return new int[] { Color.BLACK, Color.BLACK };
	}

	public int[] getEditTextFontSize() {
		return new int[] { 10, 10 };
	}

	public int[] getEditTextShadow() {
		return new int[] { 0, 0, 0, Color.WHITE };
	}

	public Typeface[] getEditTextTypefaces() {
		return new Typeface[] { Typeface.create(Typeface.DEFAULT_BOLD, Typeface.BOLD),
				Typeface.create(Typeface.DEFAULT, Typeface.NORMAL) };
	}

	protected void setDialogLayoutId(int resId1, int resId2) {
		mLayoutResId[0] = resId1;
		mLayoutResId[1] = resId2;
	}

	protected int[] getDialogLayoutId() {
		return mLayoutResId;
	}

	protected void setAudioFileList(ArrayList<String> voice) {
		if(mVoiceList != null)
			mVoiceList.clear();
		mVoiceList = voice;
	}

	public String getSharePopupComment(int type, String[] content) {
		return null;
	}

	public String getHashTag(int type) {
		return "";
	}

	protected boolean isShareEditBox() {
		return mEditActionShare;
	}

	protected void clearEditData() {
		mTitle = null;
		mAuthor = null;
		mComment = null;
		mPassword = null;
		mEditActionShare = false;
		mShareType = 0;
	}

	protected void changeScene() {
		Cocos2dxScreenRecorder.getInstance().changeScene();
	}

    public Cocos2dxGLSurfaceView onCreateView() {
    	return new Cocos2dxGLSurfaceView(this);
    }

   private final static boolean isAndroidEmulator() {
      String model = Build.MODEL;
      Log.d(TAG, "model=" + model);
      String product = Build.PRODUCT;
      Log.d(TAG, "product=" + product);
      boolean isEmulator = false;
      if (product != null) {
         isEmulator = product.equals("sdk") || product.contains("_sdk") || product.contains("sdk_");
      }
      Log.d(TAG, "isEmulator=" + isEmulator);
      return isEmulator;
   }

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

   public void startRecord(int width, int height) {
	   Cocos2dxScreenRecorder recorder = Cocos2dxScreenRecorder.getInstance();
	   recorder.setAudioFileList(mVoiceList);
	   recorder.prepareEncoder(this);
	   recorder.record(true);
	   ((Cocos2dxRenderer)mGLSurfaceView.getRenderer()).initRecord();
   }

   public void stopRecord() {
	   Cocos2dxScreenRecorder.getInstance().record(false);
	   Cocos2dxScreenRecorder.getInstance().onPaused();
   }

    public void changeRecordStatus(boolean pause) {
		Cocos2dxScreenRecorder.getInstance().pause(pause);
    }

   public void nativeShow() {
	   mGLSurfaceView.setVisibility(View.VISIBLE);
	   mGLSurfaceView.requestFocus();
   }

   public void nativeHide() {
	   mGLSurfaceView.setVisibility(View.INVISIBLE);
   }

    public void nativeHandleTouchEvent(final MotionEvent event) {
    	mGLSurfaceView.onTouchEvent(event);
    }
}
