package org.cocos2dx.lib;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

import java.io.File;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.List;

import org.apache.http.conn.util.InetAddressUtils;

public class Cocos2dxUtil {
	public static final int NETWORK_STATUS_UNAVAILABLE		= -1;
	public static final int NETWORK_STATUS_AIRPLANE_MODE	= 0;
	public static final int NETWORK_STATUS_AVAILABLE		= 1;

	public static int getNetworkStatus() {
		Context context = Cocos2dxActivity.getContext();
		boolean isAirplaneMode = false;
	    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
	    	isAirplaneMode = Settings.System.getInt(context.getContentResolver(),
	                Settings.System.AIRPLANE_MODE_ON, 0) != 0;
	    } else {
	    	isAirplaneMode = Settings.Global.getInt(context.getContentResolver(),
	                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
	    }

		ConnectivityManager manager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo mobile = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		NetworkInfo wifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		if(isAirplaneMode) {
			return NETWORK_STATUS_AIRPLANE_MODE;
		} else {
			boolean mobileAvailable = (mobile == null ? false : mobile.isAvailable());
			boolean wifiAvailable = (wifi == null ? false : wifi.isAvailable());
			if(mobileAvailable || wifiAvailable)
				return NETWORK_STATUS_AVAILABLE;
			else
				return NETWORK_STATUS_UNAVAILABLE;
		}
	}

	public static String getIPAddress() {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface item: interfaces) {
                List<InetAddress> addrs = Collections.list(item.getInetAddresses());
                for(InetAddress addr: addrs) {
                    if(!addr.isLoopbackAddress()) {
                        String addrString = addr.getHostAddress().toUpperCase();
                        boolean isIPv4 = InetAddressUtils.isIPv4Address(addrString); 
                        if(isIPv4) { 
                            return addrString;
                        } else {
                            int index = addrString.indexOf('%');
                            return index < 0 ? addrString : addrString.substring(0, index);
                        }
                    }
                }
            }
        } catch(SocketException e) {
        	e.printStackTrace();
        }
        return "";
    }

	public static String getDeviceToken() {
		Cocos2dxActivity activity = (Cocos2dxActivity)Cocos2dxActivity.getContext();
		if(activity == null)
			return "";
		return activity.getDeviceToken();
	}

	public static String getUUID() {
		Context context = Cocos2dxActivity.getContext();
		String uuid = null;
		final TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
		uuid = tm.getDeviceId();

		if(uuid == null) {
			uuid = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
		}

		if(uuid == null)
			return null;
		
		try {
			uuid = makeSHA1Hash(uuid);
		} catch(NoSuchAlgorithmException e) {
			e.printStackTrace();
			uuid = null;
		}

		return uuid;
	}

	public static String getVersion() {
		Context context = Cocos2dxActivity.getContext();
		PackageManager pm = context.getPackageManager();
		if(pm != null) {
			try {
				PackageInfo info = pm.getPackageInfo(context.getPackageName(), 0);
				if(info != null) {
					return info.versionName;
				}
			} catch(NameNotFoundException e) {
				e.printStackTrace();
			}
		}
		return "";
	}

	public static String getPlatformString(int id) {
		Cocos2dxActivity activity = (Cocos2dxActivity)Cocos2dxActivity.getContext();
		if(activity == null)
			return "";
		return activity.getPlatformString(id);
	}

	public static boolean deleteFile(String file) {
		File deleteFile = new File(file);
		if(!deleteFile.exists())
			return false;
		return deleteFile.delete();
	}

	private static String makeSHA1Hash(String input) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA1");
		md.reset();
		byte[] buffer = input.getBytes();
		md.update(buffer);
		byte[] digest = md.digest();
		String hexStr = "";
		for(int i = 0; i < digest.length; i++) {
			hexStr += Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1);
		}
		return hexStr;
	}
}