/****************************************************************************
Copyright (c) 2010-2011 cocos2d-x.org

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 ****************************************************************************/
package org.cocos2dx.lib;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.app.Activity;
import android.opengl.EGL14;
import android.opengl.EGLContext;
import android.opengl.EGLDisplay;
import android.opengl.EGLSurface;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.DisplayMetrics;
import android.util.Log;

import org.cocos2dx.lib.Cocos2dxScreenRecorder;

public class Cocos2dxRenderer implements GLSurfaceView.Renderer {
	// ===========================================================
	// Constants
	// ===========================================================

	private final static long NANOSECONDSPERSECOND = 1000000000L;
	private final static long NANOSECONDSPERMICROSECOND = 1000000;

	private static long sAnimationInterval = (long) (1.0 / 60 * Cocos2dxRenderer.NANOSECONDSPERSECOND);

	// ===========================================================
	// Fields
	// ===========================================================

	private long mLastTickInNanoSeconds;
	private int mScreenWidth;
	private int mScreenHeight;

    // Orthographic projection matrix.  Must be updated when the available screen area
    // changes (e.g. when the device is rotated).
    static final float mProjectionMatrix[] = new float[16];

    // used by saveRenderState() / restoreRenderState()
    private final float mSavedMatrix[] = new float[16];
    private EGLDisplay mSavedEglDisplay;
    private EGLSurface mSavedEglDrawSurface;
    private EGLSurface mSavedEglReadSurface;
    private EGLContext mSavedEglContext;

	// ===========================================================
	// Constructors
	// ===========================================================

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	public static void setAnimationInterval(final double pAnimationInterval) {
		Cocos2dxRenderer.sAnimationInterval = (long) (pAnimationInterval * Cocos2dxRenderer.NANOSECONDSPERSECOND);
	}

	public void setScreenWidthAndHeight(final int pSurfaceWidth, final int pSurfaceHeight) {
		this.mScreenWidth = pSurfaceWidth;
		this.mScreenHeight = pSurfaceHeight;
	}

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

    /**
     * Saves the current projection matrix and EGL state.
     */
    private void saveRenderState() {
        System.arraycopy(mProjectionMatrix, 0, mSavedMatrix, 0, mProjectionMatrix.length);
        mSavedEglDisplay = EGL14.eglGetCurrentDisplay();
        mSavedEglDrawSurface = EGL14.eglGetCurrentSurface(EGL14.EGL_DRAW);
        mSavedEglReadSurface = EGL14.eglGetCurrentSurface(EGL14.EGL_READ);
        mSavedEglContext = EGL14.eglGetCurrentContext();
    }

    /**
     * Saves the current projection matrix and EGL state.
     */
    private void restoreRenderState() {
        // switch back to previous state
        if (!EGL14.eglMakeCurrent(mSavedEglDisplay, mSavedEglDrawSurface, mSavedEglReadSurface,
                mSavedEglContext)) {
            throw new RuntimeException("eglMakeCurrent failed");
        }
        System.arraycopy(mSavedMatrix, 0, mProjectionMatrix, 0, mProjectionMatrix.length);
    }

    /**
     * Performs one-time GL setup (creating programs, disabling optional features).
     */
    private void glSetup() {
        // Set the background color.
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    }

    public void initRecord() {
		Cocos2dxScreenRecorder recorder = Cocos2dxScreenRecorder.getInstance();
        if (recorder.isRecording()) {
            saveRenderState();
            recorder.firstTimeSetup();
            recorder.makeCurrent();
            glSetup();
            restoreRenderState();
        }
    }

	@Override
	public void onSurfaceCreated(final GL10 pGL10, final EGLConfig pEGLConfig) {
		Cocos2dxRenderer.nativeInit(this.mScreenWidth, this.mScreenHeight);
		this.mLastTickInNanoSeconds = System.nanoTime();
	}

	@Override
	public void onSurfaceChanged(final GL10 pGL10, final int pWidth, final int pHeight) {
		Cocos2dxRenderer.nativeOnSurfaceChanged(pWidth, pHeight);
	}

	@Override
	public void onDrawFrame(final GL10 gl) {
		/*
		 * FPS controlling algorithm is not accurate, and it will slow down FPS
		 * on some devices. So comment FPS controlling code.
		 */
		
		/*
		final long nowInNanoSeconds = System.nanoTime();
		final long interval = nowInNanoSeconds - this.mLastTickInNanoSeconds;
		*/

		// should render a frame when onDrawFrame() is called or there is a
		// "ghost"
		Cocos2dxRenderer.nativeRender();

		/*
		// fps controlling
		if (interval < Cocos2dxRenderer.sAnimationInterval) {
			try {
				// because we render it before, so we should sleep twice time interval
				Thread.sleep((Cocos2dxRenderer.sAnimationInterval - interval) / Cocos2dxRenderer.NANOSECONDSPERMICROSECOND);
			} catch (final Exception e) {
			}
		}

		this.mLastTickInNanoSeconds = nowInNanoSeconds;
		*/

		Cocos2dxScreenRecorder recorder = Cocos2dxScreenRecorder.getInstance();
		if (recorder.isRecording()) {
            saveRenderState();

            DisplayMetrics metrics = new DisplayMetrics();
            Cocos2dxHelper.getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int widthContentView = metrics.widthPixels;
            int heightContentView = metrics.heightPixels;
            
            Log.d("Device Screen Resolution:", "Device width:" + widthContentView +  
            		" " + "Device height: " + heightContentView);
            
            if (widthContentView < 2000) {
            	// switch to recorder state
                recorder.makeCurrent();
                recorder.getProjectionMatrix(mProjectionMatrix);
                recorder.setViewport();
                
                // render everything again
                int[] resolution = Cocos2dxScreenRecorder.getVideoResulution();
                Cocos2dxRenderer.nativeRecordInit(resolution[0], resolution[1]);
                
                Log.d("Recording Video Resolution:", "width:" + Integer.toString(resolution[0]) +  
                		" " + "height: " + Integer.toString(resolution[1]));
                Cocos2dxRenderer.nativeRenderForRecord(true);
            } else {
            	// Note 4
            	// render everything again
                int[] resolution = Cocos2dxScreenRecorder.getVideoResulution();
                Cocos2dxRenderer.nativeRecordInit(resolution[0], resolution[1]);
                
                Log.d("Recording Video Resolution:", "width:" + Integer.toString(resolution[0]) +  
                		" " + "height: " + Integer.toString(resolution[1]));
                Cocos2dxRenderer.nativeRenderForRecord(true);

                recorder.makeCurrent();
                recorder.setViewport();
            }
            
            Cocos2dxRenderer.nativeRender();
            Cocos2dxRenderer.nativeRenderForRecord(false);
            Cocos2dxRenderer.nativeRecordDone();

            recorder.swapBuffers();

            restoreRenderState();
        }
	}

	// ===========================================================
	// Methods
	// ===========================================================

	private static native void nativeTouchesBegin(final int pID, final float pX, final float pY);
	private static native void nativeTouchesEnd(final int pID, final float pX, final float pY);
	private static native void nativeTouchesMove(final int[] pIDs, final float[] pXs, final float[] pYs);
	private static native void nativeTouchesCancel(final int[] pIDs, final float[] pXs, final float[] pYs);
	private static native boolean nativeKeyDown(final int pKeyCode);
	private static native void nativeRender();
	private static native void nativeRenderForRecord(boolean record);
	private static native void nativeInit(final int pWidth, final int pHeight);
	private static native void nativeRecordInit(final int width, final int height);
	private static native void nativeRecordDone();
	private static native void nativeOnSurfaceChanged(final int pWidth, final int pHeight);
	private static native void nativeOnPause();
	private static native void nativeOnResume();

	public void handleActionDown(final int pID, final float pX, final float pY) {
		Cocos2dxRenderer.nativeTouchesBegin(pID, pX, pY);
	}

	public void handleActionUp(final int pID, final float pX, final float pY) {
		Cocos2dxRenderer.nativeTouchesEnd(pID, pX, pY);
	}

	public void handleActionCancel(final int[] pIDs, final float[] pXs, final float[] pYs) {
		Cocos2dxRenderer.nativeTouchesCancel(pIDs, pXs, pYs);
	}

	public void handleActionMove(final int[] pIDs, final float[] pXs, final float[] pYs) {
		Cocos2dxRenderer.nativeTouchesMove(pIDs, pXs, pYs);
	}

	public void handleKeyDown(final int pKeyCode) {
		Cocos2dxRenderer.nativeKeyDown(pKeyCode);
	}

	public void handleOnPause() {
		Cocos2dxRenderer.nativeOnPause();
		Cocos2dxScreenRecorder.getInstance().onPaused();
	}

	public void handleOnResume() {
		Cocos2dxRenderer.nativeOnResume();
	}

	private static native void nativeInsertText(final String pText);
	private static native void nativeDeleteBackward();
	private static native String nativeGetContentText();

	public void handleInsertText(final String pText) {
		Cocos2dxRenderer.nativeInsertText(pText);
	}

	public void handleDeleteBackward() {
		Cocos2dxRenderer.nativeDeleteBackward();
	}

	public String getContentText() {
		return Cocos2dxRenderer.nativeGetContentText();
	}

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================
}
