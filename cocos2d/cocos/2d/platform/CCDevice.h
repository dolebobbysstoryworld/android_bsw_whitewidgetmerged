/****************************************************************************
Copyright (c) 2010-2012 cocos2d-x.org
Copyright (c) 2013-2014 Chukong Technologies Inc.

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/

#ifndef __CCDEVICE_H__
#define __CCDEVICE_H__

#include "CCPlatformMacros.h"
#include "ccMacros.h"
#include "CCData.h"

NS_CC_BEGIN

struct FontDefinition;

namespace Res {
    enum class Strings {
        CC_APP_UPDATE_NOTICE,
        CC_APPUPDATE_BUTTON_TITLE_OK,
        PF_NET_WARNING_3G,
        CM_POPUP_BUTTON_TITLE_CANCEL,
        CM_POPUP_BUTTON_TITLE_OK,
        PF_NET_POPUP_WARNING_NETWORK_UNSTABLE,
        PF_NET_POPUP_NOTICCE_UPDATE_AND_RESTART,
        PF_NET_POPUP_NOTICCE_DOWNLOAD_RESOURCCE,
        CM_NET_WARNING_NETWORK_UNSTABLE,
        PF_NET_ERROR_EMAIL_ADDRESS,
        PF_NET_ERROR_EMAIL_FOR_SNS,
        PF_NET_ERROR__PASSWORD_INCORRECT,
        PF_NET_ERROR_EMAIL_ENTER_FULL_ADDRESS,
        PF_NET_ERROR_EMAIL_ALREADY_USE,
        PF_NET_ERROR__PASSWORD_SHORT,
        PF_NET_ERROR__QRCODE_INPUT,
        CC_EVENTNOTICCE_DISPLAY_A_DAY,
        CM_POPUP_BUTTON_TITLE_CLOSE,
        CC_HOME_BOOKTITLE_NEW,
        CC_HOME_MENUTITLE_SETTING,
        CC_HOME_POPUP_DELETE_STORY,
        CC_HOME_POPUP_NEED_LOGIN,
        CC_HOME_POPUP_NEED_LOGIN_BUTTON_TITLE_LOGIN,
        PF_DOLECOIN_ABOUTCOIN,
        PF_DOLECOIN_HOWEARN,
        PF_DOLECOIN_BUTTONTITLE_SCAN,
        PF_DOLECOIN_NEED_LOGIN,
        PF_POPUP_RECEIVED_DOLECOIN,
        PF_QRCODE_INPUT_CODE,
        PF_QRCODE_SCAN_CODE,
        PF_QRCODE_ERROR_USED,
        PF_QRCODE_ERROR_NETWORK_NOT_CONNCET,
        CC_LIST_CHARACTER_MAKE,
        CC_LIST_NEED_UNLOCK_COIN,
        CC_LIST_CHARACTER_WARNING_DELETE,
        CC_EXPANDEDITEM_NEED_UNLOCK_COIN,
        CC_EXPANDEDITEM_NEED_UNLOCK_COIN_CHA,
        CC_EXPANDEDITEM_NEED_UNLOCK_COIN_1,
        CC_EXPANDEDITEM_NEED_UNLOCK_COIN_2,
        CC_EXPANDEDITEM_NEED_UNLOCK_COIN_LIMITED_PERIOD,
        CC_EXPANDEDITEM_NEED_UNLOCK_COIN_LIMITED_NUMBER,
        CC_EXPANDEDITEM_POPUP_WARNING_USE_UNLOCK_COIN,
        CC_EXPANDEDITEM_POPUP_COIN_BUTTON_TITLE_UNLOCK,
        CC_EXPANDEDITEM_POPUP_NOT_ENOUGH_COIN,
        CC_EXPANDEDITEM_POPUP_BUTTON_TITLE_EARN_COIN,
        CC_EXPANDEDITEM_POPUP_NEED_LOGIN,
        CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD,
        CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_DAYS,
        CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_HOURS,
        CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_HOUR,
        CC_EXPANDEDITEM_NOTICE_LIMITED_NUMBER,
        PF_CREATECHAR_BUTTON_TITLE_RETAKE,
        PF_CREATECHAR_BUTTON_TITLE_SAVE,
        PF_CREATECHAR_BUTTON_TITLE_CANCEL,
        CC_LIST_BACKGROUND_MAKE,
        CC_LIST_BACKGROUND_WARNING_DELETE,
        CC_EXPANDEDBG_NEED_UNLOCK_COIN,
        CC_EXPANDEDBG_NEED_UNLOCK_COIN_1,
        CC_EXPANDEDBG_NEED_UNLOCK_COIN_2,
        CC_EXPANDEDBG_NEED_UNLOCK_COIN_LIMITED_PERIOD,
        CC_EXPANDEDBG_NEED_UNLOCK_COIN_NUMBER,
        PF_LOGIN_TEXTFIELD_EMAIL,
        PF_LOGIN_TEXTFIELD_PASSWORD,
        PF_LOGIN_BUTTON_TITLE_LOGIN,
        PF_LOGIN_BUTTON_TITLE_FORGOT,
        PF_LOGIN_BUTTON_TITLE_SIGNUP,
        PF_LGOIN_BUTTON_FBLOGIN,
        PF_LOGIN_POPUP_ERROR_FBLOGIN,
        PF_LOGIN_BUTTON_TITLE_SKIP,
        PF_FORGOT_NOTICE_FORGOT_PASSWORD,
        PF_FORGOT_TEXTFIELD_EMAIL,
        PF_FORGOT_BUTTON_TITLE_SEND,
        PF_FORGOT_POPUP_CONFIRM_PASSWORD,
        PF_SIGNUP_TEXTFIELD_EMAIL,
        PF_SIGNUP_TEXTFIELD_PASSWORD ,
        PF_SIGNUP_TEXTFIELD_PASSWORD_CONDITION,
        PF_SIGNUP_TEXTFIELD_PASSWORD_CONFIRM,
        PF_SIGNUP_TEXTFIELD_BIRTHYEAR,
        PF_SIGNUP_TEXTFIELD_CITY,
        PF_SIGNUP_RADIOTITLE_MALE,
        PF_SIGNUP_RADIOTITLE_FEMALE,
        PF_SIGNUP_CHECKBOX_TITLE_AGREE,
        PF_SIGNUP_BUTTON_TITLE_SIGNUP,
        PF_SIGNUP_POPUP_CANCEL_SIGNUP,
        PF_SIGNUP_POPUP_BUTTON_TITLE_OK,
        PF_SIGNUP_POPUP_BUTTON_TITLE_OK_SIGN_UP,
        PF_SIGNUP_POPUP_BUTTON_TITLE_CANCEL,
        PF_SIGNUP_POPUP_TITLE_BIRTHYEAR,
        PF_SIGNUP_BUTTON_TITLE_SET,
        PF_SIGNUP_PUPUP_TITLE_SELECT_COUNTRY,
        PF_SIGNUP_COUNTRY_LIST_OTHER,
        PF_SIGNUP_PUPUP_TITLE_SELECT_CITY,
        PF_SIGNUP_CHECKBOX_TITLE_AGREE_TERMS,
        PF_SIGNUP_CHECKBOX_TITLE_AGREE_PRIVACY,
        PF_SIGNUP_BUTTON_TITLE_NEXT,
        CC_JOURNEYMAP_BUTTON_INTRO,
        CC_JOURNEYMAP_BUTTON_FINAL,
        CC_JOURNEYMAP_BOOKTITLE_DEFAULT,
        CC_JOURNEYMAP_SEPARATOR_BOOKNAME,
        CC_JOURNEYMAP_BOOKAUTHOR_DEFAULT,
        CC_JOURNEYMAP_BUTTON_TITLE_PLAY,
        CC_JOURNEYMAP_MENU_TITLE_DOWNLOADDEVICE,
        CC_JOURNEYMAP_MENU_TITLE_SHARE,
        CC_JOURNEYMAP_POPUP_NOTICCE_DOWNLOADDEVICE,
        CC_JOURNEYMAP_POPUP_NOTICE_SHARE,
        CC_JOURNEYMAP_POPUP_TITLE_SHARE_LIST,
        CC_JOURNEYMAP_POPUP_SHARELIST_FACEBOOK,
        CC_JOURNEYMAP_POPUP_SHARELIST_YOUTUBE,
        CC_JOURNEYMAP_POPUP_ERROR_SHARE,
        CC_JOURNEYMAP_POPUP_WARNING_DELETE_PAGE,
        CC_JOURNEYMAP_BUTTON_TITLE_CANCEL,
        CC_JOURNEYMAP_POPUP_WARNING_CANCEL_CONVERT,
        CC_JOURNEYMAP_POPUP_WARNING_NOTENOUGH_STORAGE,
        PF_SHARECOMMENT_POPUP_TITLE,
        PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_FACEBOOK,
        PF_SHARECOMMNET_POPUP_HASHTAG_FACEBOOK,
        PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_YOUTUBE,
        PF_SHARECOMMNET_POPUP_HASHTAG_YOUTUBE,
        CC_JOURNEYMAP_SHARE_PROGRESS_NOTICE,
        CC_SETPOSITION_BUTTON_TITLE_RECORD,
        CC_SETPOSITION_POPUP_NOTICE_AUTOSELECT_LASTPAGE,
        CC_SETPOSITION_POPUP_WARNING_LIMITED_REDUCED,
        CC_SETPOSITION_POPUP_WARNING_ITEM_REUNLOCK,
        CC_SETPOSTION_POPUP_WARNING_DELETE_CHARACTER,
        CC_SETPOSTION_POPUP_WARNING_DELETE_BACKGROUND,
        CC_LIST_MUSIC_TITLE_NONE,
        CC_LIST_MUSIC_TITLE_JOLLY,
        CC_LIST_MUSIC_TITLE_DREAM,
        CC_LIST_MUSIC_TITLE_BRAVE,
        CC_LIST_MUSIC_TITLE_SHINE,
        CC_LIST_MUSIC_TITLE_BRISK,
        CC_LIST_MUSIC_TITLE_ANGRY,
        CC_LIST_MUSIC_TITLE_SLEEPY,
        CC_LIST_MUSIC_TITLE_ADVENTURE,
        CC_LIST_MUSIC_TITLE_CRUISE,
        CC_LIST_MUSIC_TITLE_JUNGLE,
        CC_RECORDING_BUTTON_TITLE_DONE,
        CC_RECORDING_POPUP_WARNING_WITHOUT_SAVING,
        CC_RECORDING_BUTTON_TITLE_RECOD,
        CC_RECORDING_BUTTON_TITLE_RECODING,
        CC_RECORDING_BUTTON_TITLE_RERECORDING,
        CC_RECORDING_BUTTON_TITLE_SAVE,
        CC_RECORDING_POPUP_WARNING_RERECORDING,
//        CC_RECORDING_POPUP_WARNING_WITHOUT_SAVING, //문서에 중복 정의 되어 있음
        CC_PLAY_PAGE_TITLE_INTRO,
        CC_PLAY_PAGE_TITLE_PAGE_N,
        CC_PLAY_PAGE_TITLE_FINAL,
        PF_SETTING_NOTICE_MORE_ADVANTAGE_LOGIN,
        PF_SETTING_BUTTON_TITLE_LOGIN,
        PF_SETTING_SOUND,
        PF_SETTING_BUTTON_TITLE_ABOUT,
        PF_SETTING_BUTTON_TITLE_HELP,
        PF_SETTING_BUTTON_TITLE_EDIT,
        PF_SETTING_BUTTON_TITLE_USAGE_COIN,
        PF_SETTING_BUTTON_TITLE_DELETE_ACCOUNT,
        PF_EDITACCOUNT_TEXTFIELD_CURRENT_PASSWORD,
        PF_EDITACCOUNT_TEXTFIELD_PASSWORD ,
        PF_EDITACCOUNT_TEXTFIELD_PASSWORD_CONDITION,
        PF_EDITACCOUNT_TEXTFIELD_CONFIRM_PASSWORD,
        PF_EDITACCOUNT_BUTTON_TITLE_SAVE,
        PF_USAGECOIN_NOTICE,
        PF_USAGECOIN_LISTFIELD_DETAILS,
        PF_USAGECOIN_LISTFIELD_DATE,
        PF_USAGECOIN_BUTTON_TITLE_HELP,
        PF_USAGECOIN_NOTICE_NOHISTORY,
        PF_USAGECOIN_UNLIMITED,
        PF_USAGECOIN_LIMITED_PERIOD,
        PF_DELETEACCOUNT_NOTICE,
        PF_DELETEACCOUNT_CHECKBOX_CONFIRM,
        PF_DELETEACCOUNT_TEXTFIELD_CONFIRM_PASSWORD,
        PF_DELETEACCOUNT_BUTTON_TITLE_DELETE,
        PF_DELETEACCOUNT_TABLE_TITLEE_DOLECOIN,
        PF_DELETEACCOUNT_TABLE_TITLEE_BOBBY,
        PF_DELETEACCOUNT_TABLE_SUBTITLE_DOLECOIN,
        PF_DELETEACCOUNT_TABLE_SUBTITLE_BOBBY,
        PF_DELETEACCOUNT_TABLE_SUBTITLE_BOBBY_ADD,
        PF_ABOUT_APPTITLE,
        PF_ABOUT_SERVICE_INTRODUCE,
        PF_ABOUT_BUTTON_TITLE_TERMS,
        PF_ABOUT_TITLE_SERVICE,
        PF_ABOUT_TITLE_PRIVACY,
        PF_HELP_BUTTON_TITLE_FAQ,
        PF_HELP_TEXTFIELD_EMAIL,
        PF_HELP_TEXTFIELD_FEEDBACK,
        PF_HELP_DEVICE,
        PF_HELP_WARNING_PRIVACYINFO,
        PF_HELP_BUTTON_TITLE_SEND,
        CC_GUIDE_HOME,
        CC_GUIDE_JOURNEYMAP_NEWPAGE,
        CC_GUIDE_JOURNEYMAP_ALLPLAY,
        CC_GUIDE_SETPOSITION_SELECT_ITEM,
        CC_GUIDE_SETPOSITION_SELECT_BACKGROUND,
        CC_GUIDE_SETPOSITION_SELECT_MUSIC,
        CC_GUIDE_SETPOSITION_DELETE_ITEM,
        CC_GUIDE_SETPOSITION_RECORDING,
        PF_GUIDE_EDIT_PICTURE,
        PF_GUIDE_LOGIN_SIGNUP,
        PF_GUIDE_LOGIN_EARNCOIN,
        PF_GUIDE_LOGIN_GETITEM,
        PF_GUIDE_LOGIN_SHARE,
        PF_LOGIN_POPUP_ERROR_NOTAVAILABLE_EMAIL,
        CC_HOME_POPUP_UNLOCK_ITEM_MESSAGE,
        CC_JOURNEYMAP__DOWNLOAD_DEVICE_COMPLETE,
        CC_JOURNEYMAP__SHARE_COMPLETE,
        CC_SETPOSITION_POPUP_WARNING_ITEM_FULL,
        PF_EDITACCOUNT_POPUP_INPUT_PASSWORD,
        PF_EDITACCOUNT_POPUP_SAVE_COMPLETE,
        PF_HELP_POPUP_SEND_COMPLETE,
        PF_DELETEACCOUNT_DELETE_COMPLETE,
        PF_PROMOTION_CHECKBOX,
        CC_POPUP_CANCEL_SHARE,
        CC_PRLOAD_BOOKTITLE_1,
        CC_PRLOAD_BOOKTITLE_2,
        CM_POPUP_BUTTON_TITLE_YES,
        CM_POPUP_BUTTON_TITLE_NO,
        PF_NET_ERROR_EMAIL_NOT_AVAILABLE,
    };
}

class CC_DLL Device
{
public:
    enum class TextAlign
    {
        CENTER        = 0x33, ///< Horizontal center and vertical center.
        TOP           = 0x13, ///< Horizontal center and vertical top.
        TOP_RIGHT     = 0x12, ///< Horizontal right and vertical top.
        RIGHT         = 0x32, ///< Horizontal right and vertical center.
        BOTTOM_RIGHT = 0x22, ///< Horizontal right and vertical bottom.
        BOTTOM        = 0x23, ///< Horizontal center and vertical bottom.
        BOTTOM_LEFT  = 0x21, ///< Horizontal left and vertical bottom.
        LEFT          = 0x31, ///< Horizontal left and vertical center.
        TOP_LEFT      = 0x11, ///< Horizontal left and vertical top.
    };
    /**
     *  Gets the DPI of device
     *  @return The DPI of device.
     */
    static int getDPI();
    
    /**
     * To enable or disable accelerometer.
     */
    static void setAccelerometerEnabled(bool isEnabled);
    /**
     *  Sets the interval of accelerometer.
     */
    static void setAccelerometerInterval(float interval);

    static Data getTextureDataForText(const char * text,const FontDefinition& textDefinition,TextAlign align,int &widht,int &height);

    static int getEllipsisIndexForText(const char* text, const char* ellipsys, const FontDefinition& textDefinition, TextAlign align);

    static int getTextWidth(const char* text, const char* fontName, float fontSize);
    
    static const char *getDeviceTokenString();
    
    static const char *getUUIDString();

    static const char *getClientIPString();
    
    static bool getNetworkAvailable();

    static const char *getDocumentPath();
    
    static size_t getFileSize(const char * path);
    static bool deleteFile(const char  *path);
    
    static const char* getResString(Res::Strings key);
    
    static const char* getCurrentAppVersion();
    
private:
    CC_DISALLOW_IMPLICIT_CONSTRUCTORS(Device);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    static const char* getPlatformString(int id);
#endif
};


NS_CC_END

#endif /* __CCDEVICE_H__ */
