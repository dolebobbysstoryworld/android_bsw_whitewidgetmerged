/****************************************************************************
Copyright (c) 2010-2012 cocos2d-x.org
Copyright (c) 2013-2014 Chukong Technologies Inc.

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/

#include "CCPlatformConfig.h"
#if CC_TARGET_PLATFORM == CC_PLATFORM_MAC

#include "platform/CCDevice.h"
#include <Foundation/Foundation.h>
#include <Cocoa/Cocoa.h>
#include <string>
#include "ccTypes.h"

NS_CC_BEGIN

int Device::getDPI()
{
//TODO: 
    return 160;
}

void Device::setAccelerometerEnabled(bool isEnabled)
{

}

void Device::setAccelerometerInterval(float interval)
{

}

typedef struct
{
    int height;
    int width;
    bool        hasAlpha;
    bool        isPremultipliedAlpha;
    unsigned char*  data;
} tImageInfo;

static bool _initWithString(const char * text, Device::TextAlign align, const char * fontName, int size, tImageInfo* info, Color3B* strokeColor)
{
    bool ret = false;
    
	CCASSERT(text, "Invalid pText");
	CCASSERT(info, "Invalid pInfo");
	
	do {
		NSString * string  = [NSString stringWithUTF8String:text];
		
		// font
		NSFont *font = [[NSFontManager sharedFontManager]
                        fontWithFamily:[NSString stringWithUTF8String:fontName]
						traits:NSUnboldFontMask | NSUnitalicFontMask
                        weight:0
                        size:size];
		
		if (font == nil) {
			font = [[NSFontManager sharedFontManager]
					fontWithFamily:@"Arial"
					traits:NSUnboldFontMask | NSUnitalicFontMask
					weight:0
					size:size];
		}
		CC_BREAK_IF(!font);
		
		// color
		NSColor* foregroundColor;
		if (strokeColor) {
			foregroundColor = [NSColor colorWithDeviceRed:strokeColor->r/255.0 green:strokeColor->g/255.0 blue:strokeColor->b/255.0 alpha:1];
		} else {
			foregroundColor = [NSColor whiteColor];
		}
		
		
		// alignment, linebreak
		unsigned horiFlag = (int)align & 0x0f;
		unsigned vertFlag = ((int)align >> 4) & 0x0f;
		NSTextAlignment textAlign = (2 == horiFlag) ? NSRightTextAlignment
        : (3 == horiFlag) ? NSCenterTextAlignment
        : NSLeftTextAlignment;
		
		NSMutableParagraphStyle *paragraphStyle = [[[NSMutableParagraphStyle alloc] init] autorelease];
		[paragraphStyle setParagraphStyle:[NSParagraphStyle defaultParagraphStyle]];
		[paragraphStyle setLineBreakMode:NSLineBreakByCharWrapping];
		[paragraphStyle setAlignment:textAlign];
        
		// attribute
		NSDictionary* tokenAttributesDict = [NSDictionary dictionaryWithObjectsAndKeys:
											 foregroundColor,NSForegroundColorAttributeName,
											 font, NSFontAttributeName,
											 paragraphStyle, NSParagraphStyleAttributeName, nil];
        
        // linebreak
		if (info->width > 0) {
			if ([string sizeWithAttributes:tokenAttributesDict].width > info->width) {
				NSMutableString *lineBreak = [[[NSMutableString alloc] init] autorelease];
				NSUInteger length = [string length];
				NSRange range = NSMakeRange(0, 1);
                CGSize textSize;
				NSUInteger lastBreakLocation = 0;
                NSUInteger insertCount = 0;
				for (NSUInteger i = 0; i < length; i++) {
					range.location = i;
					NSString *character = [string substringWithRange:range];
					[lineBreak appendString:character];
					if ([@"!?.,-= " rangeOfString:character].location != NSNotFound) {
                        lastBreakLocation = i + insertCount;
                    }
                    textSize = [lineBreak sizeWithAttributes:tokenAttributesDict];
                    if(textSize.height > info->height)
                        break;
					if (textSize.width > info->width) {
                        if(lastBreakLocation > 0) {
                            [lineBreak insertString:@"\r" atIndex:lastBreakLocation];
                            lastBreakLocation = 0;
                        }
                        else {
                            [lineBreak insertString:@"\r" atIndex:[lineBreak length] - 1];
                        }
                        insertCount += 1;
					}
				}
                
				string = lineBreak;
			}
		}
        
		NSAttributedString *stringWithAttributes =[[[NSAttributedString alloc] initWithString:string
                                                                                   attributes:tokenAttributesDict] autorelease];
        
		NSSize realDimensions = [stringWithAttributes size];
		// Mac crashes if the width or height is 0
		CC_BREAK_IF(realDimensions.width <= 0 || realDimensions.height <= 0);
        
		CGSize dimensions = CGSizeMake(info->width, info->height);
		
        
		if(dimensions.width <= 0 && dimensions.height <= 0) {
			dimensions.width = realDimensions.width;
			dimensions.height = realDimensions.height;
		} else if (dimensions.height <= 0) {
			dimensions.height = realDimensions.height;
		}
        
		NSInteger POTWide = dimensions.width;
		NSInteger POTHigh = MAX(dimensions.height, realDimensions.height);
		unsigned char*			data;
		//Alignment
        
		CGFloat xPadding = 0;
		switch (textAlign) {
			case NSLeftTextAlignment: xPadding = 0; break;
			case NSCenterTextAlignment: xPadding = (dimensions.width-realDimensions.width)/2.0f; break;
			case NSRightTextAlignment: xPadding = dimensions.width-realDimensions.width; break;
			default: break;
		}
        
		// 1: TOP
		// 2: BOTTOM
		// 3: CENTER
		CGFloat yPadding = (1 == vertFlag || realDimensions.height >= dimensions.height) ? (dimensions.height - realDimensions.height)	// align to top
		: (2 == vertFlag) ? 0																	// align to bottom
		: (dimensions.height - realDimensions.height) / 2.0f;									// align to center
		
		
		NSRect textRect = NSMakeRect(xPadding, POTHigh - dimensions.height + yPadding, realDimensions.width, realDimensions.height);
		//Disable antialias
		
		[[NSGraphicsContext currentContext] setShouldAntialias:NO];
		
		NSImage *image = [[NSImage alloc] initWithSize:NSMakeSize(POTWide, POTHigh)];
        
		[image lockFocus];
        
        // patch for mac retina display and lableTTF
        [[NSAffineTransform transform] set];
		
		//[stringWithAttributes drawAtPoint:NSMakePoint(xPadding, offsetY)]; // draw at offset position
		[stringWithAttributes drawInRect:textRect];
		//[stringWithAttributes drawInRect:textRect withAttributes:tokenAttributesDict];
		NSBitmapImageRep *bitmap = [[NSBitmapImageRep alloc] initWithFocusedViewRect:NSMakeRect (0.0f, 0.0f, POTWide, POTHigh)];
		[image unlockFocus];
		
		data = (unsigned char*) [bitmap bitmapData];  //Use the same buffer to improve the performance.
		
		NSUInteger textureSize = POTWide*POTHigh*4;
		
		unsigned char* dataNew = (unsigned char*)malloc(sizeof(unsigned char) * textureSize);
		if (dataNew) {
			memcpy(dataNew, data, textureSize);
			// output params
			info->width = static_cast<int>(POTWide);
			info->height = static_cast<int>(POTHigh);
			info->data = dataNew;
			info->hasAlpha = true;
			info->isPremultipliedAlpha = true;
			ret = true;
		}
		[bitmap release];
		[image release];
	} while (0);
    return ret;
}

Data Device::getTextureDataForText(const char * text,const FontDefinition& textDefinition,TextAlign align,int &width,int &height)
{
    Data ret;
    do {
        tImageInfo info = {0};
        info.width = textDefinition._dimensions.width;
        info.height = textDefinition._dimensions.height;
        
        if (! _initWithString(text, align, textDefinition._fontName.c_str(), textDefinition._fontSize, &info, nullptr)) //pStrokeColor))
        {
            break;
        }
        height = (short)info.height;
        width = (short)info.width;
        ret.fastSet(info.data,width * height * 4);
    } while (0);
    
    return ret;
}

int Device::getEllipsisIndexForText(const char* text, const char* ellipsis, const FontDefinition& textDefinition, TextAlign align)
{
    int width = textDefinition._dimensions.width;
    NSMutableString *truncatedString = [NSMutableString stringWithCString:text encoding:NSUTF8StringEncoding];
    NSString *ellipsisString = [NSString stringWithCString:ellipsis encoding:NSUTF8StringEncoding];
    
    NSString *fontName = [NSString stringWithCString:textDefinition._fontName.c_str() encoding:NSUTF8StringEncoding];
    NSFont *font = [NSFont fontWithName:fontName size:textDefinition._fontSize];
    NSDictionary *attr = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    NSSize size = [truncatedString sizeWithAttributes:attr];
    if (size.width > width) {
        width -= [ellipsisString sizeWithAttributes:attr].width;
        
        NSRange range = {truncatedString.length -1, 1};
        while ([truncatedString sizeWithAttributes:attr].width > width) {
            [truncatedString deleteCharactersInRange:range];
            range.location--;
        }
        return (int)range.location;
    }
    
    return -1;
}

int Device::getTextWidth(const char* text, const char* fontName, float fontSize)
{
    NSMutableString *str = [NSMutableString stringWithCString:text encoding:NSUTF8StringEncoding];
    NSFont *font = [NSFont fontWithName:[NSString stringWithCString:fontName encoding:NSUTF8StringEncoding] size:fontSize];
    NSDictionary *attr = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    return [str sizeWithAttributes:attr].width;
}

const char *Device::getDeviceTokenString() {
    return "APA91bHWw0ijubpdPAOinLM6nh9AJr7C1FlGvvehvojAbX4I35iZ2pW9KvLo2Nq00hh5GdcUhMFt4UBnJCc2IshUaxYAXhkH2B8rGVwKzElbCRSWhOkVHslkcOd_ZfFZwMSWM57Cd8hYYxMUZvq6dWWakWLwj0UaKQ";
}

const char *Device::getUUIDString() {
    return "00000000-0000-0000-0000-000000000000";
}

const char *Device::getClientIPString() {
    return "1.1.1.1";
}

bool Device::getNetworkAvailable() {
    return true;
}

const char *Device::getDocumentPath() {
    return "";
}

size_t Device::getFileSize(const char * path) {
    return 0;
}

bool Device::deleteFile(const char  *path) {
    return false;
}

const char* Device::getCurrentAppVersion() {
//    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
//    NSString *buildVersion = infoDictionary[(NSString*)kCFBundleVersionKey];
//    return [buildVersion UTF8String];
    return "";
}

const char* Device::getResString(Res::Strings key) {
    const char * resStr = NULL;
    switch (key) {
        case Res::Strings::CC_APP_UPDATE_NOTICE: { resStr = "Update in order to\ncontinue Bobby's Journey."; } break;
        case Res::Strings::CC_APPUPDATE_BUTTON_TITLE_OK: { resStr = "OK"; } break;
        case Res::Strings::PF_NET_WARNING_3G: { resStr = "There may be an additional charge \nwhen access is made through\na mobile communication network. "; } break;
        case Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL: { resStr = "Cancel"; } break;
        case Res::Strings::CM_POPUP_BUTTON_TITLE_OK: { resStr = "OK"; } break;
        case Res::Strings::PF_NET_POPUP_WARNING_NETWORK_UNSTABLE: { resStr = "The network is unstable.\nPlease check and try again."; } break;
        case Res::Strings::PF_NET_POPUP_NOTICCE_UPDATE_AND_RESTART: { resStr = "Update completed.\nRestart Bobby’s Journey app\nto continue."; } break;
        case Res::Strings::PF_NET_POPUP_NOTICCE_DOWNLOAD_RESOURCCE: { resStr = "We are downloading your fun storybooks and cute characters. \nPlease wait."; } break;
        case Res::Strings::CM_NET_WARNING_NETWORK_UNSTABLE: { resStr = "The network is unstable.\nPlease try again."; } break;
        case Res::Strings::PF_NET_ERROR_EMAIL_ADDRESS: { resStr = "The email does not exist or is incorrect"; } break;
        case Res::Strings::PF_NET_ERROR_EMAIL_FOR_SNS: { resStr = "This account is signed up\n with Facebook"; } break;
        case Res::Strings::PF_NET_ERROR__PASSWORD_INCORRECT: { resStr = "The password is incorrect"; } break;
        case Res::Strings::PF_NET_ERROR_EMAIL_ENTER_FULL_ADDRESS: { resStr = "Please enter the full email address, \nincluding after the @ sign"; } break;
        case Res::Strings::PF_NET_ERROR_EMAIL_ALREADY_USE: { resStr = "This email address is already in use \nor not available"; } break;
        case Res::Strings::PF_NET_ERROR__PASSWORD_SHORT: { resStr = "Please enter a password \nwith at least 6 characters"; } break;
        case Res::Strings::PF_NET_ERROR__QRCODE_INPUT: { resStr = "Enter the %d characters of the code"; } break;
        case Res::Strings::CC_EVENTNOTICCE_DISPLAY_A_DAY: { resStr = "Do not display for a day"; } break;
        case Res::Strings::CM_POPUP_BUTTON_TITLE_CLOSE: { resStr = "Close"; } break;
        case Res::Strings::CC_HOME_BOOKTITLE_NEW: { resStr = "New"; } break;
        case Res::Strings::CC_HOME_MENUTITLE_SETTING: { resStr = "Settings"; } break;
        case Res::Strings::CC_HOME_POPUP_DELETE_STORY: { resStr = "Selected story book\nwill be deleted. Once deleted, \nit cannot be restored."; } break;
        case Res::Strings::CC_HOME_POPUP_NEED_LOGIN: { resStr = "Log in to view Dole Coins \ninformation. "; } break;
        case Res::Strings::CC_HOME_POPUP_NEED_LOGIN_BUTTON_TITLE_LOGIN: { resStr = "Log in"; } break;
        case Res::Strings::PF_DOLECOIN_ABOUTCOIN: { resStr = "What is a Dole Coin?\nEarn and accumulate Dole Coins\nto win rewards and prizes."; } break;
        case Res::Strings::PF_DOLECOIN_HOWEARN: { resStr = "How can I earn Dole Coins? \n• Scan QR code \n• Share on social network "; } break;
        case Res::Strings::PF_DOLECOIN_BUTTONTITLE_SCAN: { resStr = "Scan QR code"; } break;
        case Res::Strings::PF_DOLECOIN_NEED_LOGIN: { resStr = "You need to log in."; } break;
        case Res::Strings::PF_POPUP_RECEIVED_DOLECOIN: { resStr = "Congratulations!\nYou received %d Dole Coins."; } break;
        case Res::Strings::PF_QRCODE_INPUT_CODE: { resStr = "Enter the alphanumeric code"; } break;
        case Res::Strings::PF_QRCODE_SCAN_CODE: { resStr = "Scan QR code"; } break;
        case Res::Strings::PF_QRCODE_ERROR_USED: { resStr = "This code cannot be used."; } break;
        case Res::Strings::PF_QRCODE_ERROR_NETWORK_NOT_CONNCET: { resStr = "The code cannot be recognized \nbecause there is no network \nconnection."; } break;
        case Res::Strings::CC_LIST_CHARACTER_MAKE: { resStr = "Create\ncharacter"; } break;
        case Res::Strings::CC_LIST_NEED_UNLOCK_COIN: { resStr = "It's locked.\nUse Dole Coins to unlock."; } break;
        case Res::Strings::CC_LIST_CHARACTER_WARNING_DELETE: { resStr = "The selected character\nwill be deleted. Once deleted,\nit cannot be restored."; } break;
        case Res::Strings::CC_EXPANDEDITEM_NEED_UNLOCK_COIN: { resStr = "Use Dole Coins to unlock\nthe special item."; } break;
        case Res::Strings::CC_EXPANDEDITEM_NEED_UNLOCK_COIN_CHA: { resStr = "Use Dole Coins to unlock\nthe special item."; } break;
        case Res::Strings::CC_EXPANDEDITEM_NEED_UNLOCK_COIN_1: { resStr = "Use Dole Coins to unlock"; } break;
        case Res::Strings::CC_EXPANDEDITEM_NEED_UNLOCK_COIN_2: { resStr = "the special item."; } break;
        case Res::Strings::CC_EXPANDEDITEM_NEED_UNLOCK_COIN_LIMITED_PERIOD: { resStr = "Once unlocked, you will be able to use \nthe item for %d days. "; } break;
        case Res::Strings::CC_EXPANDEDITEM_NEED_UNLOCK_COIN_LIMITED_NUMBER: { resStr = "Use Dole Coins and unlock the special item.\nThen you’ll be able to use it %d times after unlock."; } break;
        case Res::Strings::CC_EXPANDEDITEM_POPUP_WARNING_USE_UNLOCK_COIN: { resStr = "Use %d Dole Coins to unlock.\nThis action cannot be undone."; } break;
        case Res::Strings::CC_EXPANDEDITEM_POPUP_COIN_BUTTON_TITLE_UNLOCK: { resStr = "Unlock"; } break;
        case Res::Strings::CC_EXPANDEDITEM_POPUP_NOT_ENOUGH_COIN: { resStr = "Not enough Dole Coins."; } break;
        case Res::Strings::CC_EXPANDEDITEM_POPUP_BUTTON_TITLE_EARN_COIN: { resStr = "Earn \nDole Coins"; } break;
        case Res::Strings::CC_EXPANDEDITEM_POPUP_NEED_LOGIN: { resStr = "Please log in if you want \nto use Dole Coins."; } break;
        case Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD: { resStr = "The item will be available \nfor the period shown."; } break;
        case Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_DAYS: { resStr = "days"; } break;
        case Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_HOURS: { resStr = "hours"; } break;
        case Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_HOUR: { resStr = "hour"; } break;
        case Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_NUMBER: { resStr = "You’ll be able to use it N times after unlock."; } break;
        case Res::Strings::PF_CREATECHAR_BUTTON_TITLE_RETAKE: { resStr = "Retake"; } break;
        case Res::Strings::PF_CREATECHAR_BUTTON_TITLE_SAVE: { resStr = "Save"; } break;
        case Res::Strings::PF_CREATECHAR_BUTTON_TITLE_CANCEL: { resStr = "Cancel"; } break;
        case Res::Strings::CC_LIST_BACKGROUND_MAKE: { resStr = "Create\nbackground"; } break;
        case Res::Strings::CC_LIST_BACKGROUND_WARNING_DELETE: { resStr = "The selected background\nwill be deleted. Once deleted,\nit cannot be restored."; } break;
        case Res::Strings::CC_EXPANDEDBG_NEED_UNLOCK_COIN: { resStr = "Use Dole Coins to unlock\nthe special background"; } break;
        case Res::Strings::CC_EXPANDEDBG_NEED_UNLOCK_COIN_1: { resStr = "Use Dole Coins to unlock"; } break;
        case Res::Strings::CC_EXPANDEDBG_NEED_UNLOCK_COIN_2: { resStr = "the special background"; } break;
        case Res::Strings::CC_EXPANDEDBG_NEED_UNLOCK_COIN_LIMITED_PERIOD: { resStr = "Once unlocked, you will be able to use \nthe background for %d days. "; } break;
        case Res::Strings::CC_EXPANDEDBG_NEED_UNLOCK_COIN_NUMBER: { resStr = "Use Dole Coins and unlock the special background.\nThen you’ll be able to use it %d times after unlock."; } break;
        case Res::Strings::PF_LOGIN_TEXTFIELD_EMAIL: { resStr = "Your email "; } break;
        case Res::Strings::PF_LOGIN_TEXTFIELD_PASSWORD: { resStr = "Password"; } break;
        case Res::Strings::PF_LOGIN_BUTTON_TITLE_LOGIN: { resStr = "Log in"; } break;
        case Res::Strings::PF_LOGIN_BUTTON_TITLE_FORGOT: { resStr = "Forgot your password?"; } break;
        case Res::Strings::PF_LOGIN_BUTTON_TITLE_SIGNUP: { resStr = "Sign up"; } break;
        case Res::Strings::PF_LGOIN_BUTTON_FBLOGIN: { resStr = "Log in with Facebook "; } break;
        case Res::Strings::PF_LOGIN_POPUP_ERROR_FBLOGIN: { resStr = "Facebook Log in failed.\nPlease check your\nFacebook account."; } break;
        case Res::Strings::PF_LOGIN_BUTTON_TITLE_SKIP: { resStr = "Skip"; } break;
        case Res::Strings::PF_FORGOT_NOTICE_FORGOT_PASSWORD: { resStr = "Please enter the email address used \nas the ID during sign up.\nA temporary password will be sent\nto the address."; } break;
        case Res::Strings::PF_FORGOT_TEXTFIELD_EMAIL: { resStr = "Your email "; } break;
        case Res::Strings::PF_FORGOT_BUTTON_TITLE_SEND: { resStr = "Send"; } break;
        case Res::Strings::PF_FORGOT_POPUP_CONFIRM_PASSWORD: { resStr = "The temporary password\nwas sent. \nPlease check your spam folder\nif the email is not in your inbox. \n(Takes around 5 to 10 minutes.) "; } break;
        case Res::Strings::PF_SIGNUP_TEXTFIELD_EMAIL: { resStr = "Your email "; } break;
        case Res::Strings::PF_SIGNUP_TEXTFIELD_PASSWORD: { resStr = "Password"; } break;
        case Res::Strings::PF_SIGNUP_TEXTFIELD_PASSWORD_CONDITION: { resStr = "at least 6 characters"; } break;
        case Res::Strings::PF_SIGNUP_TEXTFIELD_PASSWORD_CONFIRM: { resStr = "Confirm Password"; } break;
        case Res::Strings::PF_SIGNUP_TEXTFIELD_BIRTHYEAR: { resStr = "Year of birth"; } break;
        case Res::Strings::PF_SIGNUP_TEXTFIELD_CITY: { resStr = "City"; } break;
        case Res::Strings::PF_SIGNUP_RADIOTITLE_MALE: { resStr = "Male"; } break;
        case Res::Strings::PF_SIGNUP_RADIOTITLE_FEMALE: { resStr = "Female"; } break;
        case Res::Strings::PF_SIGNUP_CHECKBOX_TITLE_AGREE: { resStr = "Agree to the Terms \n& Conditions and Privacy Policy "; } break;
        case Res::Strings::PF_SIGNUP_BUTTON_TITLE_SIGNUP: { resStr = "Sign up"; } break;
        case Res::Strings::PF_SIGNUP_POPUP_CANCEL_SIGNUP: { resStr = "Do you want to cancel sign up?"; } break;
        case Res::Strings::PF_SIGNUP_POPUP_BUTTON_TITLE_OK: { resStr = "No"; } break;
        case Res::Strings::PF_SIGNUP_POPUP_BUTTON_TITLE_OK_SIGN_UP: { resStr = "Yes"; } break;
        case Res::Strings::PF_SIGNUP_POPUP_BUTTON_TITLE_CANCEL: { resStr = "Yes"; } break;
        case Res::Strings::PF_SIGNUP_POPUP_TITLE_BIRTHYEAR: { resStr = "Enter the year of birth"; } break;
        case Res::Strings::PF_SIGNUP_BUTTON_TITLE_SET: { resStr = "Set"; } break;
        case Res::Strings::PF_SIGNUP_PUPUP_TITLE_SELECT_COUNTRY: { resStr = "Select the country"; } break;
        case Res::Strings::PF_SIGNUP_COUNTRY_LIST_OTHER: { resStr = "Other"; } break;
        case Res::Strings::PF_SIGNUP_PUPUP_TITLE_SELECT_CITY: { resStr = "Enter the city"; } break;
        case Res::Strings::PF_SIGNUP_CHECKBOX_TITLE_AGREE_TERMS: { resStr = "I agree to the Terms and Conditions "; } break;
        case Res::Strings::PF_SIGNUP_CHECKBOX_TITLE_AGREE_PRIVACY: { resStr = "I agree to the Privacy Policy "; } break;
        case Res::Strings::PF_SIGNUP_BUTTON_TITLE_NEXT: { resStr = "Next"; } break;
        case Res::Strings::CC_JOURNEYMAP_BUTTON_INTRO: { resStr = "Intro"; } break;
        case Res::Strings::CC_JOURNEYMAP_BUTTON_FINAL: { resStr = "Finale"; } break;
        case Res::Strings::CC_JOURNEYMAP_BOOKTITLE_DEFAULT: { resStr = "My story %d"; } break;
        case Res::Strings::CC_JOURNEYMAP_SEPARATOR_BOOKNAME: { resStr = "By"; } break;
        case Res::Strings::CC_JOURNEYMAP_BOOKAUTHOR_DEFAULT: { resStr = "Name"; } break;
        case Res::Strings::CC_JOURNEYMAP_BUTTON_TITLE_PLAY: { resStr = "Play"; } break;
        case Res::Strings::CC_JOURNEYMAP_MENU_TITLE_DOWNLOADDEVICE: { resStr = "Download to my device"; } break;
        case Res::Strings::CC_JOURNEYMAP_MENU_TITLE_SHARE: { resStr = "Share"; } break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_NOTICCE_DOWNLOADDEVICE: { resStr = "Once the storybook\nthat the user created was played, \nit will be saved."; } break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_NOTICE_SHARE: { resStr = "You can share your storybook \nafter playing this storybook."; } break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_TITLE_SHARE_LIST: { resStr = "Share"; } break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_SHARELIST_FACEBOOK: { resStr = "Facebook"; } break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_SHARELIST_YOUTUBE: { resStr = "YouTube"; } break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_ERROR_SHARE: { resStr = "Sharing failed. Please check\nthe network connection\nor social network status and\ntry again."; } break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_WARNING_DELETE_PAGE: { resStr = "The selected page\nwill be deleted. Once deleted,\nit cannot be restored."; } break;
        case Res::Strings::CC_JOURNEYMAP_BUTTON_TITLE_CANCEL: { resStr = "Cancel"; } break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_WARNING_CANCEL_CONVERT: { resStr = "Are you sure\nyou want to cancel?\nCancelled work will not be saved."; } break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_WARNING_NOTENOUGH_STORAGE: { resStr = "There is not enough\nstorage space."; } break;
        case Res::Strings::PF_SHARECOMMENT_POPUP_TITLE: { resStr = "Write a comment"; } break;
        case Res::Strings::PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_FACEBOOK: { resStr = "[(%s)] written by (%s)! I used the #DoleBobby’sJourney App!"; } break;
        case Res::Strings::PF_SHARECOMMNET_POPUP_HASHTAG_FACEBOOK: { resStr = "I used the #DoleBobby’sJourney App!"; } break;
        case Res::Strings::PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_YOUTUBE: { resStr = "[(%s)] written by (%s)! "; } break;
        case Res::Strings::PF_SHARECOMMNET_POPUP_HASHTAG_YOUTUBE: { resStr = "(%s) #DoleBobby’sJourney"; } break;
        case Res::Strings::CC_JOURNEYMAP_SHARE_PROGRESS_NOTICE: { resStr = "Uploading story…"; } break;
        case Res::Strings::CC_SETPOSITION_BUTTON_TITLE_RECORD: { resStr = "Record"; } break;
        case Res::Strings::CC_SETPOSITION_POPUP_NOTICE_AUTOSELECT_LASTPAGE: { resStr = "Do you want to use the item\nyou selected on the last page?"; } break;
        case Res::Strings::CC_SETPOSITION_POPUP_WARNING_LIMITED_REDUCED: { resStr = "The number of use will be reduced.\nDo you want to use it \nin this storybook? "; } break;
        case Res::Strings::CC_SETPOSITION_POPUP_WARNING_ITEM_REUNLOCK: { resStr = "This item is no longer available.\nUse Dole Coins to unlock it again."; } break;
        case Res::Strings::CC_SETPOSTION_POPUP_WARNING_DELETE_CHARACTER: { resStr = "The selected character\nwill be deleted. Once deleted,\nit cannot be restored."; } break;
        case Res::Strings::CC_SETPOSTION_POPUP_WARNING_DELETE_BACKGROUND: { resStr = "The selected background\nwill be deleted. Once deleted,\nit cannot be restored."; } break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_NONE: { resStr = "None"; } break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_JOLLY: { resStr = "Jolly"; } break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_DREAM: { resStr = "Dream"; } break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_BRAVE: { resStr = "Brave"; } break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_SHINE: { resStr = "Shine "; } break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_BRISK: { resStr = "Dashing "; } break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_ANGRY: { resStr = "Angry"; } break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_SLEEPY: { resStr = "Sleepy"; } break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_ADVENTURE: { resStr = "Adventure"; } break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_CRUISE: { resStr = "Voyager "; } break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_JUNGLE: { resStr = "Explore"; } break;
        case Res::Strings::CC_RECORDING_BUTTON_TITLE_DONE: { resStr = "Done"; } break;
        case Res::Strings::CC_RECORDING_POPUP_WARNING_WITHOUT_SAVING: { resStr = "Do you want to leave\nwithout saving?\nAll unsaved content will be lost."; } break;
        case Res::Strings::CC_RECORDING_BUTTON_TITLE_RECOD: { resStr = "Record"; } break;
        case Res::Strings::CC_RECORDING_BUTTON_TITLE_RECODING: { resStr = "Recording"; } break;
        case Res::Strings::CC_RECORDING_BUTTON_TITLE_RERECORDING: { resStr = "Retry"; } break;
        case Res::Strings::CC_RECORDING_BUTTON_TITLE_SAVE: { resStr = "Save"; } break;
        case Res::Strings::CC_RECORDING_POPUP_WARNING_RERECORDING: { resStr = "Delete all recorded content\nand start new recording."; } break;
//        case Res::Strings::CC_RECORDING_POPUP_WARNING_WITHOUT_SAVING: { resStr = "Do you want to leave\nwithout saving?\nAll unsaved story will be lost."; } break;
        case Res::Strings::CC_PLAY_PAGE_TITLE_INTRO: { resStr = "Intro"; } break;
        case Res::Strings::CC_PLAY_PAGE_TITLE_PAGE_N: { resStr = "Page %d"; } break;
        case Res::Strings::CC_PLAY_PAGE_TITLE_FINAL: { resStr = "Finale"; } break;
        case Res::Strings::PF_SETTING_NOTICE_MORE_ADVANTAGE_LOGIN: { resStr = "You can enjoy more features \nby logging in."; } break;
        case Res::Strings::PF_SETTING_BUTTON_TITLE_LOGIN: { resStr = "Log in"; } break;
        case Res::Strings::PF_SETTING_SOUND: { resStr = "Sound setting"; } break;
        case Res::Strings::PF_SETTING_BUTTON_TITLE_ABOUT: { resStr = "About"; } break;
        case Res::Strings::PF_SETTING_BUTTON_TITLE_HELP: { resStr = "Help"; } break;
        case Res::Strings::PF_SETTING_BUTTON_TITLE_EDIT: { resStr = "Edit"; } break;
        case Res::Strings::PF_SETTING_BUTTON_TITLE_USAGE_COIN: { resStr = "Dole Coin usage history"; } break;
        case Res::Strings::PF_SETTING_BUTTON_TITLE_DELETE_ACCOUNT: { resStr = "Delete account"; } break;
        case Res::Strings::PF_EDITACCOUNT_TEXTFIELD_CURRENT_PASSWORD: { resStr = "Current password"; } break;
        case Res::Strings::PF_EDITACCOUNT_TEXTFIELD_PASSWORD: { resStr = "Change password"; } break;
        case Res::Strings::PF_EDITACCOUNT_TEXTFIELD_PASSWORD_CONDITION: { resStr = "at least 6 characters"; } break;
        case Res::Strings::PF_EDITACCOUNT_TEXTFIELD_CONFIRM_PASSWORD: { resStr = "Confirm Password"; } break;
        case Res::Strings::PF_EDITACCOUNT_BUTTON_TITLE_SAVE: { resStr = "Save"; } break;
        case Res::Strings::PF_USAGECOIN_NOTICE: { resStr = "This is your Dole Coins History.\nFor more information,\nplease contact customer support."; } break;
        case Res::Strings::PF_USAGECOIN_LISTFIELD_DETAILS: { resStr = "Details"; } break;
        case Res::Strings::PF_USAGECOIN_LISTFIELD_DATE: { resStr = "Date"; } break;
        case Res::Strings::PF_USAGECOIN_BUTTON_TITLE_HELP: { resStr = "Help"; } break;
        case Res::Strings::PF_USAGECOIN_NOTICE_NOHISTORY: { resStr = "There is no usage history.\nSave Dole coins\nto enjoy a variety of services!"; } break;
        case Res::Strings::PF_USAGECOIN_UNLIMITED: { resStr = "Unlimited use"; } break;
        case Res::Strings::PF_USAGECOIN_LIMITED_PERIOD: { resStr = "Limited use"; } break;
        case Res::Strings::PF_DELETEACCOUNT_NOTICE: { resStr = "Are you sure you want to delete\nyour account? The information\nbelow will be deleted."; } break;
        case Res::Strings::PF_DELETEACCOUNT_CHECKBOX_CONFIRM: { resStr = "Yes, I want to delete account."; } break;
        case Res::Strings::PF_DELETEACCOUNT_TEXTFIELD_CONFIRM_PASSWORD: { resStr = "Confirm password"; } break;
        case Res::Strings::PF_DELETEACCOUNT_BUTTON_TITLE_DELETE: { resStr = "Delete account"; } break;
        case Res::Strings::PF_DELETEACCOUNT_TABLE_TITLEE_DOLECOIN: { resStr = "Dole Coin"; } break;
        case Res::Strings::PF_DELETEACCOUNT_TABLE_TITLEE_BOBBY: { resStr = "Bobby’s Journey"; } break;
        case Res::Strings::PF_DELETEACCOUNT_TABLE_SUBTITLE_DOLECOIN: { resStr = "Earn and usage record"; } break;
        case Res::Strings::PF_DELETEACCOUNT_TABLE_SUBTITLE_BOBBY: { resStr = "User information"; } break;
        case Res::Strings::PF_DELETEACCOUNT_TABLE_SUBTITLE_BOBBY_ADD: { resStr = "Synced data"; } break;
        case Res::Strings::PF_ABOUT_APPTITLE: { resStr = "Bobby’s Journey"; } break;
        case Res::Strings::PF_ABOUT_SERVICE_INTRODUCE: { resStr = "Bobby's Journey lets kids\nhave fun making their own storybook."; } break;
        case Res::Strings::PF_ABOUT_BUTTON_TITLE_TERMS: { resStr = "Terms and Conditions"; } break;
        case Res::Strings::PF_ABOUT_TITLE_SERVICE: { resStr = "Terms of Service"; } break;
        case Res::Strings::PF_ABOUT_TITLE_PRIVACY: { resStr = "Privacy Policy"; } break;
        case Res::Strings::PF_HELP_BUTTON_TITLE_FAQ: { resStr = "FAQ"; } break;
        case Res::Strings::PF_HELP_TEXTFIELD_EMAIL: { resStr = "Your email"; } break;
        case Res::Strings::PF_HELP_TEXTFIELD_FEEDBACK: { resStr = "Type feedback here"; } break;
        case Res::Strings::PF_HELP_DEVICE: { resStr = "Device"; } break;
        case Res::Strings::PF_HELP_WARNING_PRIVACYINFO: { resStr = "The device and entered data is used only\nfor handling the inquiry. "; } break;
        case Res::Strings::PF_HELP_BUTTON_TITLE_SEND: { resStr = "Send"; } break;
        case Res::Strings::CC_GUIDE_HOME: { resStr = "Create a new storybook"; } break;
        case Res::Strings::CC_GUIDE_JOURNEYMAP_NEWPAGE: { resStr = "Select a page to create a story"; } break;
        case Res::Strings::CC_GUIDE_JOURNEYMAP_ALLPLAY: { resStr = "Play the whole story \ncontinuously"; } break;
        case Res::Strings::CC_GUIDE_SETPOSITION_SELECT_ITEM: { resStr = "Select an item to add \non the screen"; } break;
        case Res::Strings::CC_GUIDE_SETPOSITION_SELECT_BACKGROUND: { resStr = "Select a picture to change \nbackground"; } break;
        case Res::Strings::CC_GUIDE_SETPOSITION_SELECT_MUSIC: { resStr = "Select background music"; } break;
        case Res::Strings::CC_GUIDE_SETPOSITION_DELETE_ITEM: { resStr = "Move items \nto the garbage can to delete"; } break;
        case Res::Strings::CC_GUIDE_SETPOSITION_RECORDING: { resStr = "Long tap or move an item \nor the background to record"; } break;
        case Res::Strings::PF_GUIDE_EDIT_PICTURE: { resStr = "Adjust the picture by pinching\nor spreading two fingers."; } break;
        case Res::Strings::PF_GUIDE_LOGIN_SIGNUP: { resStr = "Register with Bobby’s Journey\nto use special functions"; } break;
        case Res::Strings::PF_GUIDE_LOGIN_EARNCOIN: { resStr = "Earn\nDole Coins"; } break;
        case Res::Strings::PF_GUIDE_LOGIN_GETITEM: { resStr = "Get\nspecial items"; } break;
        case Res::Strings::PF_GUIDE_LOGIN_SHARE: { resStr = "Share my\nstorybook"; } break;
        case Res::Strings::PF_LOGIN_POPUP_ERROR_NOTAVAILABLE_EMAIL: { resStr = "This email address is already in use \nor not available"; } break;
        case Res::Strings::CC_HOME_POPUP_UNLOCK_ITEM_MESSAGE: { resStr = "Congratulations!\nYou received the item."; } break;
        case Res::Strings::CC_JOURNEYMAP__DOWNLOAD_DEVICE_COMPLETE: { resStr = "Download has been completed"; } break;
        case Res::Strings::CC_JOURNEYMAP__SHARE_COMPLETE: { resStr = "Successful! \nYour storybook has been shared"; } break;
        case Res::Strings::CC_SETPOSITION_POPUP_WARNING_ITEM_FULL: { resStr = "You can’t add any more"; } break;
        case Res::Strings::PF_EDITACCOUNT_POPUP_INPUT_PASSWORD: { resStr = "Enter your current password"; } break;
        case Res::Strings::PF_EDITACCOUNT_POPUP_SAVE_COMPLETE: { resStr = "It has been changed"; } break;
        case Res::Strings::PF_HELP_POPUP_SEND_COMPLETE: { resStr = "Your inquiry has been sent"; } break;
        case Res::Strings::PF_DELETEACCOUNT_DELETE_COMPLETE: { resStr = "Thank you for using Bobby's Journey!"; } break;
        case Res::Strings::PF_PROMOTION_CHECKBOX: { resStr = "Don't show during 1days"; } break;
        case Res::Strings::CC_POPUP_CANCEL_SHARE: { resStr = "Do you want to cancel?\nIf you cancel,\nit will not be shared."; } break;
        case Res::Strings::CC_PRLOAD_BOOKTITLE_1: { resStr = "The Banana Brothers"; } break;
        case Res::Strings::CC_PRLOAD_BOOKTITLE_2: { resStr = "Bob's Journey"; } break;
        case Res::Strings::CM_POPUP_BUTTON_TITLE_YES: { resStr = "Yes"; } break;
        case Res::Strings::CM_POPUP_BUTTON_TITLE_NO: { resStr = "No"; } break;
        case Res::Strings::PF_NET_ERROR_EMAIL_NOT_AVAILABLE: { resStr = "This email address is not available"; } break;
        default: {  resStr = "Undefined string";  } break;
    }
    return resStr;
}


NS_CC_END

#endif // CC_TARGET_PLATFORM == CC_PLATFORM_MAC
