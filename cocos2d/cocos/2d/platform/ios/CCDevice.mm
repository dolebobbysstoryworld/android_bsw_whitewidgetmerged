/****************************************************************************
 Copyright (c) 2010-2012 cocos2d-x.org
 Copyright (c) 2013-2014 Chukong Technologies Inc.

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/


#include "CCPlatformConfig.h"
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

#include "CCDevice.h"
#include "ccTypes.h"
#include "CCEventDispatcher.h"
#include "CCEventAcceleration.h"
#include "CCDirector.h"
#import <UIKit/UIKit.h>
#import "../../proj.ios_mac/ios/AppController.h"

// Accelerometer
#import<CoreMotion/CoreMotion.h>
#import<CoreFoundation/CoreFoundation.h>

@interface CCAccelerometerDispatcher : NSObject<UIAccelerometerDelegate>
{
    cocos2d::Acceleration *_acceleration;
    CMMotionManager *_motionManager;
}

+ (id) sharedAccelerometerDispather;
- (id) init;
- (void) setAccelerometerEnabled: (bool) isEnabled;
- (void) setAccelerometerInterval:(float) interval;

@end

@implementation CCAccelerometerDispatcher

static CCAccelerometerDispatcher* s_pAccelerometerDispatcher;

+ (id) sharedAccelerometerDispather
{
    if (s_pAccelerometerDispatcher == nil) {
        s_pAccelerometerDispatcher = [[self alloc] init];
    }
    
    return s_pAccelerometerDispatcher;
}

- (id) init
{
    if( (self = [super init]) ) {
        _acceleration = new cocos2d::Acceleration();
        _motionManager = [[CMMotionManager alloc] init];
    }
    return self;
}

- (void) dealloc
{
    s_pAccelerometerDispatcher = nullptr;
    delete _acceleration;
    [_motionManager release];
    [super dealloc];
}

- (void) setAccelerometerEnabled: (bool) isEnabled
{
    if (isEnabled)
    {
        [_motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
            [self accelerometer:accelerometerData];
        }];
    }
    else
    {
        [_motionManager stopAccelerometerUpdates];
    }
}

-(void) setAccelerometerInterval:(float)interval
{
    _motionManager.accelerometerUpdateInterval = interval;
}

- (void)accelerometer:(CMAccelerometerData *)accelerometerData
{
    _acceleration->x = accelerometerData.acceleration.x;
    _acceleration->y = accelerometerData.acceleration.y;
    _acceleration->z = accelerometerData.acceleration.z;
    _acceleration->timestamp = accelerometerData.timestamp;
    
    double tmp = _acceleration->x;
    
    switch ([[UIApplication sharedApplication] statusBarOrientation])
    {
        case UIInterfaceOrientationLandscapeRight:
            _acceleration->x = -_acceleration->y;
            _acceleration->y = tmp;
            break;
            
        case UIInterfaceOrientationLandscapeLeft:
            _acceleration->x = _acceleration->y;
            _acceleration->y = -tmp;
            break;
            
        case UIInterfaceOrientationPortraitUpsideDown:
            _acceleration->x = -_acceleration->y;
            _acceleration->y = -tmp;
            break;
            
        case UIInterfaceOrientationPortrait:
            break;
    }

    cocos2d::EventAcceleration event(*_acceleration);
    auto dispatcher = cocos2d::Director::getInstance()->getEventDispatcher();
    dispatcher->dispatchEvent(&event);
}

@end

//

NS_CC_BEGIN

int Device::getDPI()
{
    static int dpi = -1;

    if (dpi == -1)
    {
        float scale = 1.0f;
        
        if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
            scale = [[UIScreen mainScreen] scale];
        }
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            dpi = 132 * scale;
        } else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            dpi = 163 * scale;
        } else {
            dpi = 160 * scale;
        }
    }
    return dpi;
}




void Device::setAccelerometerEnabled(bool isEnabled)
{
    [[CCAccelerometerDispatcher sharedAccelerometerDispather] setAccelerometerEnabled:isEnabled];
}

void Device::setAccelerometerInterval(float interval)
{
    [[CCAccelerometerDispatcher sharedAccelerometerDispather] setAccelerometerInterval:interval];
}

typedef struct
{
    unsigned int height;
    unsigned int width;
    bool         isPremultipliedAlpha;
    bool         hasShadow;
    CGSize       shadowOffset;
    float        shadowBlur;
    float        shadowOpacity;
    bool         hasStroke;
    float        strokeColorR;
    float        strokeColorG;
    float        strokeColorB;
    float        strokeSize;
    float        tintColorR;
    float        tintColorG;
    float        tintColorB;
    
    unsigned char*  data;
    
} tImageInfo;

static bool s_isIOS7OrHigher = false;

static inline void lazyCheckIOS7()
{
    static bool isInited = false;
    if (!isInited)
    {
        s_isIOS7OrHigher = [[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] != NSOrderedAscending;
        isInited = true;
    }
}

static CGSize _calculateStringSize(NSString *str, id font, CGSize *constrainSize)
{
    NSArray *listItems = [str componentsSeparatedByString: @"\n"];
    CGSize dim = CGSizeZero;
    CGSize textRect = CGSizeZero;
    textRect.width = constrainSize->width > 0 ? constrainSize->width
    : 0x7fffffff;
    textRect.height = constrainSize->height > 0 ? constrainSize->height
    : 0x7fffffff;
    
    for (NSString *s in listItems)
    {
        CGSize tmp = [s sizeWithFont:font constrainedToSize:textRect];
        
        if (tmp.width > dim.width)
        {
            dim.width = tmp.width;
        }
        
        dim.height += tmp.height;
    }
    
    dim.width = ceilf(dim.width);
    dim.height = ceilf(dim.height);
    
    return dim;
}

// refer Image::ETextAlign
#define ALIGN_TOP    1
#define ALIGN_CENTER 3
#define ALIGN_BOTTOM 2


static bool _initWithString(const char * text, cocos2d::Device::TextAlign align, const char * fontName, int size, tImageInfo* info)
{
    // lazy check whether it is iOS7 device
    lazyCheckIOS7();
    
    bool bRet = false;
    do
    {
        CC_BREAK_IF(! text || ! info);
        
        NSString * str          = [NSString stringWithUTF8String:text];
        NSString * fntName      = [NSString stringWithUTF8String:fontName];
        
        CGSize dim, constrainSize;
        
        constrainSize.width     = info->width;
        constrainSize.height    = info->height;
        
        // On iOS custom fonts must be listed beforehand in the App info.plist (in order to be usable) and referenced only the by the font family name itself when
        // calling [UIFont fontWithName]. Therefore even if the developer adds 'SomeFont.ttf' or 'fonts/SomeFont.ttf' to the App .plist, the font must
        // be referenced as 'SomeFont' when calling [UIFont fontWithName]. Hence we strip out the folder path components and the extension here in order to get just
        // the font family name itself. This stripping step is required especially for references to user fonts stored in CCB files; CCB files appear to store
        // the '.ttf' extensions when referring to custom fonts.
        fntName = [[fntName lastPathComponent] stringByDeletingPathExtension];
        
        // create the font
        id font = [UIFont fontWithName:fntName size:size];
        
        if (font)
        {
            dim = _calculateStringSize(str, font, &constrainSize);
        }
        else
        {
            if (!font)
            {
                font = [UIFont systemFontOfSize:size];
            }
            
            if (font)
            {
                dim = _calculateStringSize(str, font, &constrainSize);
            }
        }
        
        CC_BREAK_IF(! font);
        
        // compute start point
        int startH = 0;
        if (constrainSize.height > dim.height)
        {
            // vertical alignment
            unsigned int vAlignment = ((int)align >> 4) & 0x0F;
            if (vAlignment == ALIGN_TOP)
            {
                startH = 0;
            }
            else if (vAlignment == ALIGN_CENTER)
            {
                startH = (constrainSize.height - dim.height) / 2;
            }
            else
            {
                startH = constrainSize.height - dim.height;
            }
        }
        
        // adjust text rect
        if (constrainSize.width > 0 && constrainSize.width > dim.width)
        {
            dim.width = constrainSize.width;
        }
        if (constrainSize.height > 0 && constrainSize.height > dim.height)
        {
            dim.height = constrainSize.height;
        }
        
        
        // compute the padding needed by shadow and stroke
        float shadowStrokePaddingX = 0.0f;
        float shadowStrokePaddingY = 0.0f;
        
        if ( info->hasStroke )
        {
            shadowStrokePaddingX = ceilf(info->strokeSize);
            shadowStrokePaddingY = ceilf(info->strokeSize);
        }
        
        // add the padding (this could be 0 if no shadow and no stroke)
        dim.width  += shadowStrokePaddingX*2;
        dim.height += shadowStrokePaddingY*2;
        
        
        unsigned char* data = (unsigned char*)malloc(sizeof(unsigned char) * (int)(dim.width * dim.height * 4));
        memset(data, 0, (int)(dim.width * dim.height * 4));
        
        // draw text
        CGColorSpaceRef colorSpace  = CGColorSpaceCreateDeviceRGB();
        CGContextRef context        = CGBitmapContextCreate(data,
                                                            dim.width,
                                                            dim.height,
                                                            8,
                                                            (int)(dim.width) * 4,
                                                            colorSpace,
                                                            kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
        if (!context)
        {
            CGColorSpaceRelease(colorSpace);
            CC_SAFE_FREE(data);
            break;
        }
        
        // text color
        CGContextSetRGBFillColor(context, info->tintColorR, info->tintColorG, info->tintColorB, 1);
        // move Y rendering to the top of the image
        CGContextTranslateCTM(context, 0.0f, (dim.height - shadowStrokePaddingY) );
        CGContextScaleCTM(context, 1.0f, -1.0f); //NOTE: NSString draws in UIKit referential i.e. renders upside-down compared to CGBitmapContext referential
        
        // store the current context
        UIGraphicsPushContext(context);
        
        // measure text size with specified font and determine the rectangle to draw text in
        unsigned uHoriFlag = (int)align & 0x0f;
        NSTextAlignment nsAlign = (2 == uHoriFlag) ? NSTextAlignmentRight
                                                  : (3 == uHoriFlag) ? NSTextAlignmentCenter
                                                  : NSTextAlignmentLeft;
         
        
        CGColorSpaceRelease(colorSpace);
        
        // compute the rect used for rendering the text
        // based on wether shadows or stroke are enabled
        
        float textOriginX  = 0;
        float textOrigingY = startH;
        
        float textWidth    = dim.width;
        float textHeight   = dim.height;
        
        CGRect rect = CGRectMake(textOriginX, textOrigingY, textWidth, textHeight);
        
        CGContextSetShouldSubpixelQuantizeFonts(context, false);
        
        CGContextBeginTransparencyLayerWithRect(context, rect, NULL);
        
        if ( info->hasStroke )
        {
            CGContextSetTextDrawingMode(context, kCGTextStroke);
            
            if(s_isIOS7OrHigher)
            {
                NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
                paragraphStyle.alignment = nsAlign;
                paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
                [str drawInRect:rect withAttributes:@{
                                                      NSFontAttributeName: font,
                                                      NSStrokeWidthAttributeName: [NSNumber numberWithFloat: info->strokeSize / size * 100 ],
                                                      NSForegroundColorAttributeName:[UIColor colorWithRed:info->tintColorR
                                                                                                     green:info->tintColorG
                                                                                                      blue:info->tintColorB
                                                                                                     alpha:1.0f],
                                                      NSParagraphStyleAttributeName:paragraphStyle,
                                                      NSStrokeColorAttributeName: [UIColor colorWithRed:info->strokeColorR
                                                                                                  green:info->strokeColorG
                                                                                                   blue:info->strokeColorB
                                                                                                  alpha:1.0f]
                                                      }
                 ];
                
                [paragraphStyle release];
            }
            else
            {
                CGContextSetRGBStrokeColor(context, info->strokeColorR, info->strokeColorG, info->strokeColorB, 1);
                CGContextSetLineWidth(context, info->strokeSize);
                
                //original code that was not working in iOS 7
                [str drawInRect: rect withFont:font lineBreakMode:NSLineBreakByWordWrapping alignment:nsAlign];
            }
        }
        
        CGContextSetTextDrawingMode(context, kCGTextFill);
        
        // actually draw the text in the context
        [str drawInRect: rect withFont:font lineBreakMode:NSLineBreakByWordWrapping alignment:nsAlign];
        
        CGContextEndTransparencyLayer(context);
        
        // pop the context
        UIGraphicsPopContext();
        
        // release the context
        CGContextRelease(context);
        
        // output params
        info->data                 = data;
        info->isPremultipliedAlpha = true;
        info->width                = dim.width;
        info->height               = dim.height;
        bRet                        = true;
        
    } while (0);
    
    return bRet;
}


Data Device::getTextureDataForText(const char * text,const FontDefinition& textDefinition,TextAlign align,int &width,int &height)
{
    Data ret;
    
    do {
        tImageInfo info = {0};
        info.width                  = textDefinition._dimensions.width;
        info.height                 = textDefinition._dimensions.height;
        info.hasShadow              = textDefinition._shadow._shadowEnabled;
        info.shadowOffset.width     = textDefinition._shadow._shadowOffset.width;
        info.shadowOffset.height    = textDefinition._shadow._shadowOffset.height;
        info.shadowBlur             = textDefinition._shadow._shadowBlur;
        info.shadowOpacity          = textDefinition._shadow._shadowOpacity;
        info.hasStroke              = textDefinition._stroke._strokeEnabled;
        info.strokeColorR           = textDefinition._stroke._strokeColor.r;
        info.strokeColorG           = textDefinition._stroke._strokeColor.g;
        info.strokeColorB           = textDefinition._stroke._strokeColor.b;
        info.strokeSize             = textDefinition._stroke._strokeSize;
        info.tintColorR             = textDefinition._fontFillColor.r;
        info.tintColorG             = textDefinition._fontFillColor.g;
        info.tintColorB             = textDefinition._fontFillColor.b;
        
        if (! _initWithString(text, align, textDefinition._fontName.c_str(), textDefinition._fontSize, &info))
        {
            break;
        }
        height = (short)info.height;
        width = (short)info.width;
		ret.fastSet(info.data,width * height * 4);
    } while (0);
    
    return ret;
}

int Device::getEllipsisIndexForText(const char* text, const char* ellipsis, const FontDefinition& textDefinition, TextAlign align)
{
    int width = textDefinition._dimensions.width;
    NSMutableString *truncatedString = [NSMutableString stringWithCString:text encoding:NSUTF8StringEncoding];
    NSString *ellipsisString = [NSString stringWithCString:ellipsis encoding:NSUTF8StringEncoding];
    
    UIFont *font = [UIFont fontWithName:[NSString stringWithCString:textDefinition._fontName.c_str() encoding:NSUTF8StringEncoding] size:textDefinition._fontSize];
    if ([truncatedString sizeWithFont:font].width > width) {
        width -= [ellipsisString sizeWithFont:font].width;
        
        NSRange range = {truncatedString.length -1, 1};
        while ([truncatedString sizeWithFont:font].width > width) {
            [truncatedString deleteCharactersInRange:range];
            range.location--;
        }
        return (int)range.location;
    }
    
    return -1;
}

int Device::getTextWidth(const char* text, const char* fontName, float fontSize)
{
    NSMutableString *str = [NSMutableString stringWithCString:text encoding:NSUTF8StringEncoding];
    UIFont *font = [UIFont fontWithName:[NSString stringWithCString:fontName encoding:NSUTF8StringEncoding] size:fontSize];
    return [str sizeWithFont:font].width;
}

const char *Device::getDeviceTokenString() {
    AppController *controller = (AppController*)[UIApplication sharedApplication].delegate;
    const char *deviceToken = [controller.deviceTokenString cStringUsingEncoding:NSUTF8StringEncoding];
    if (deviceToken == NULL) {
        log("device token string Empty !!!");
        return "aaaabbbbccccddddeeeeffffgggghhhhiiii_null";
    }
    if (strlen(deviceToken) <= 0) {
        return "aaaabbbbccccddddeeeeffffgggghhhhiiii_length0";
    }
//    log("device token string : %s", deviceToken);
    return deviceToken;
}

const char *Device::getUUIDString() {
    AppController *controller = (AppController*)[UIApplication sharedApplication].delegate;
    const char *UUIDString = [controller.appUUID cStringUsingEncoding:NSUTF8StringEncoding];
    if (UUIDString == NULL) {
        log("UUID string Empty !!!");
        return NULL;
    }
//    log("UUID string : %s", UUIDString);
    return UUIDString;
}

const char *Device::getClientIPString() {
    AppController *controller = (AppController*)[UIApplication sharedApplication].delegate;
    const char *ipString = [controller.clientIP cStringUsingEncoding:NSUTF8StringEncoding];
    if (ipString == NULL) {
        log("IP string Empty !!!");
        return NULL;
    }
    //    log("UUID string : %s", UUIDString);
    return ipString;
}

bool Device::getNetworkAvailable() {
    AppController *controller = (AppController*)[UIApplication sharedApplication].delegate;
    if (controller.networkAvailable == YES) {
        return true;
    }
    return false;
}

const char *Device::getDocumentPath() {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory UTF8String];
}

size_t Device::getFileSize(const char * path) {
    NSURL *fileURL = [NSURL URLWithString:[NSString stringWithCString:path encoding:NSUTF8StringEncoding]];
    NSString *pathString = [fileURL path];
    NSError *error;
    NSDictionary *fileAttribute = [[NSFileManager defaultManager] attributesOfItemAtPath:pathString error:&error];
    NSNumber *fileSize = [fileAttribute objectForKey:NSFileSize];
    return [fileSize longValue];
}

bool Device::deleteFile(const char  *path) {
    NSError *error;
    BOOL result = [[NSFileManager defaultManager] removeItemAtURL:[NSURL URLWithString:[NSString stringWithCString:path encoding:NSUTF8StringEncoding]] error:&error];
    if (result == YES) {
        return true;
    }
    return false;
}

const char* Device::getCurrentAppVersion() {
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *buildVersion = infoDictionary[(NSString*)kCFBundleVersionKey];
    return [buildVersion UTF8String];
}

const char* Device::getResString(Res::Strings key) {
    NSString *keyString = nil;
    switch (key) {
        case Res::Strings::CC_APP_UPDATE_NOTICE: { keyString = @"CC_APP_UPDATE_NOTICE"; } break;
        case Res::Strings::CC_APPUPDATE_BUTTON_TITLE_OK: { keyString = @"CC_APPUPDATE_BUTTON_TITLE_OK"; } break;
        case Res::Strings::PF_NET_WARNING_3G: { keyString = @"PF_NET_WARNING_3G"; } break;
        case Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL: { keyString = @"CM_POPUP_BUTTON_TITLE_CANCEL"; } break;
        case Res::Strings::CM_POPUP_BUTTON_TITLE_OK: { keyString = @"CM_POPUP_BUTTON_TITLE_OK"; } break;
        case Res::Strings::PF_NET_POPUP_WARNING_NETWORK_UNSTABLE: { keyString = @"PF_NET_POPUP_WARNING_NETWORK_UNSTABLE"; } break;
        case Res::Strings::PF_NET_POPUP_NOTICCE_UPDATE_AND_RESTART: { keyString = @"PF_NET_POPUP_NOTICCE_UPDATE_AND_RESTART"; } break;
        case Res::Strings::PF_NET_POPUP_NOTICCE_DOWNLOAD_RESOURCCE: { keyString = @"PF_NET_POPUP_NOTICCE_DOWNLOAD_RESOURCCE"; } break;
        case Res::Strings::CM_NET_WARNING_NETWORK_UNSTABLE: { keyString = @"CM_NET_WARNING_NETWORK_UNSTABLE"; } break;
        case Res::Strings::PF_NET_ERROR_EMAIL_ADDRESS: { keyString = @"PF_NET_ERROR_EMAIL_ADDRESS"; } break;
        case Res::Strings::PF_NET_ERROR_EMAIL_FOR_SNS: { keyString = @"PF_NET_ERROR_EMAIL_FOR_SNS"; } break;
        case Res::Strings::PF_NET_ERROR__PASSWORD_INCORRECT: { keyString = @"PF_NET_ERROR__PASSWORD_INCORRECT"; } break;
        case Res::Strings::PF_NET_ERROR_EMAIL_ENTER_FULL_ADDRESS: { keyString = @"PF_NET_ERROR_EMAIL_ENTER_FULL_ADDRESS"; } break;
        case Res::Strings::PF_NET_ERROR_EMAIL_ALREADY_USE: { keyString = @"PF_NET_ERROR_EMAIL_ALREADY_USE"; } break;
        case Res::Strings::PF_NET_ERROR__PASSWORD_SHORT: { keyString = @"PF_NET_ERROR__PASSWORD_SHORT"; } break;
        case Res::Strings::PF_NET_ERROR__QRCODE_INPUT: { keyString = @"PF_NET_ERROR__QRCODE_INPUT"; } break;
        case Res::Strings::CC_EVENTNOTICCE_DISPLAY_A_DAY: { keyString = @"CC_EVENTNOTICCE_DISPLAY_A_DAY"; } break;
        case Res::Strings::CM_POPUP_BUTTON_TITLE_CLOSE: { keyString = @"CM_POPUP_BUTTON_TITLE_CLOSE"; } break;
        case Res::Strings::CC_HOME_BOOKTITLE_NEW: { keyString = @"CC_HOME_BOOKTITLE_NEW"; } break;
        case Res::Strings::CC_HOME_MENUTITLE_SETTING: { keyString = @"CC_HOME_MENUTITLE_SETTING"; } break;
        case Res::Strings::CC_HOME_POPUP_DELETE_STORY: { keyString = @"CC_HOME_POPUP_DELETE_STORY"; } break;
        case Res::Strings::CC_HOME_POPUP_NEED_LOGIN: { keyString = @"CC_HOME_POPUP_NEED_LOGIN"; } break;
        case Res::Strings::CC_HOME_POPUP_NEED_LOGIN_BUTTON_TITLE_LOGIN: { keyString = @"CC_HOME_POPUP_NEED_LOGIN_BUTTON_TITLE_LOGIN"; } break;
        case Res::Strings::PF_DOLECOIN_ABOUTCOIN: { keyString = @"PF_DOLECOIN_ABOUTCOIN"; } break;
        case Res::Strings::PF_DOLECOIN_HOWEARN: { keyString = @"PF_DOLECOIN_HOWEARN"; } break;
        case Res::Strings::PF_DOLECOIN_BUTTONTITLE_SCAN: { keyString = @"PF_DOLECOIN_BUTTONTITLE_SCAN"; } break;
        case Res::Strings::PF_DOLECOIN_NEED_LOGIN: { keyString = @"PF_DOLECOIN_NEED_LOGIN"; } break;
        case Res::Strings::PF_POPUP_RECEIVED_DOLECOIN: { keyString = @"PF_POPUP_RECEIVED_DOLECOIN"; } break;
        case Res::Strings::PF_QRCODE_INPUT_CODE: { keyString = @"PF_QRCODE_INPUT_CODE"; } break;
        case Res::Strings::PF_QRCODE_SCAN_CODE: { keyString = @"PF_QRCODE_SCAN_CODE"; } break;
        case Res::Strings::PF_QRCODE_ERROR_USED: { keyString = @"PF_QRCODE_ERROR_USED"; } break;
        case Res::Strings::PF_QRCODE_ERROR_NETWORK_NOT_CONNCET: { keyString = @"PF_QRCODE_ERROR_NETWORK_NOT_CONNCET"; } break;
        case Res::Strings::CC_LIST_CHARACTER_MAKE: { keyString = @"CC_LIST_CHARACTER_MAKE"; } break;
        case Res::Strings::CC_LIST_NEED_UNLOCK_COIN: { keyString = @"CC_LIST_NEED_UNLOCK_COIN"; } break;
        case Res::Strings::CC_LIST_CHARACTER_WARNING_DELETE: { keyString = @"CC_LIST_CHARACTER_WARNING_DELETE"; } break;
        case Res::Strings::CC_EXPANDEDITEM_NEED_UNLOCK_COIN: { keyString = @"CC_EXPANDEDITEM_NEED_UNLOCK_COIN"; } break;
        case Res::Strings::CC_EXPANDEDITEM_NEED_UNLOCK_COIN_CHA: { keyString = @"CC_EXPANDEDITEM_NEED_UNLOCK_COIN_CHA"; } break;
        case Res::Strings::CC_EXPANDEDITEM_NEED_UNLOCK_COIN_1: { keyString = @"CC_EXPANDEDITEM_NEED_UNLOCK_COIN_1"; } break;
        case Res::Strings::CC_EXPANDEDITEM_NEED_UNLOCK_COIN_2: { keyString = @"CC_EXPANDEDITEM_NEED_UNLOCK_COIN_2"; } break;
        case Res::Strings::CC_EXPANDEDITEM_NEED_UNLOCK_COIN_LIMITED_PERIOD: { keyString = @"CC_EXPANDEDITEM_NEED_UNLOCK_COIN_LIMITED_PERIOD"; } break;
        case Res::Strings::CC_EXPANDEDITEM_NEED_UNLOCK_COIN_LIMITED_NUMBER: { keyString = @"CC_EXPANDEDITEM_NEED_UNLOCK_COIN_LIMITED_NUMBER"; } break;
        case Res::Strings::CC_EXPANDEDITEM_POPUP_WARNING_USE_UNLOCK_COIN: { keyString = @"CC_EXPANDEDITEM_POPUP_WARNING_USE_UNLOCK_COIN"; } break;
        case Res::Strings::CC_EXPANDEDITEM_POPUP_COIN_BUTTON_TITLE_UNLOCK: { keyString = @"CC_EXPANDEDITEM_POPUP_COIN_BUTTON_TITLE_UNLOCK"; } break;
        case Res::Strings::CC_EXPANDEDITEM_POPUP_NOT_ENOUGH_COIN: { keyString = @"CC_EXPANDEDITEM_POPUP_NOT_ENOUGH_COIN"; } break;
        case Res::Strings::CC_EXPANDEDITEM_POPUP_BUTTON_TITLE_EARN_COIN: { keyString = @"CC_EXPANDEDITEM_POPUP_BUTTON_TITLE_EARN_COIN"; } break;
        case Res::Strings::CC_EXPANDEDITEM_POPUP_NEED_LOGIN: { keyString = @"CC_EXPANDEDITEM_POPUP_NEED_LOGIN"; } break;
        case Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD: { keyString = @"CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD"; } break;
        case Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_DAYS: { keyString = @"CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_DAYS"; } break;
        case Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_HOURS: { keyString = @"CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_HOURS"; } break;
        case Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_HOUR: { keyString = @"CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_HOUR"; } break;
        case Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_NUMBER: { keyString = @"CC_EXPANDEDITEM_NOTICE_LIMITED_NUMBER"; } break;
        case Res::Strings::PF_CREATECHAR_BUTTON_TITLE_RETAKE: { keyString = @"PF_CREATECHAR_BUTTON_TITLE_RETAKE"; } break;
        case Res::Strings::PF_CREATECHAR_BUTTON_TITLE_SAVE: { keyString = @"PF_CREATECHAR_BUTTON_TITLE_SAVE"; } break;
        case Res::Strings::PF_CREATECHAR_BUTTON_TITLE_CANCEL: { keyString = @"PF_CREATECHAR_BUTTON_TITLE_CANCEL"; } break;
        case Res::Strings::CC_LIST_BACKGROUND_MAKE: { keyString = @"CC_LIST_BACKGROUND_MAKE"; } break;
        case Res::Strings::CC_LIST_BACKGROUND_WARNING_DELETE: { keyString = @"CC_LIST_BACKGROUND_WARNING_DELETE"; } break;
        case Res::Strings::CC_EXPANDEDBG_NEED_UNLOCK_COIN: { keyString = @"CC_EXPANDEDBG_NEED_UNLOCK_COIN"; } break;
        case Res::Strings::CC_EXPANDEDBG_NEED_UNLOCK_COIN_1: { keyString = @"CC_EXPANDEDBG_NEED_UNLOCK_COIN_1"; } break;
        case Res::Strings::CC_EXPANDEDBG_NEED_UNLOCK_COIN_2: { keyString = @"CC_EXPANDEDBG_NEED_UNLOCK_COIN_2"; } break;
        case Res::Strings::CC_EXPANDEDBG_NEED_UNLOCK_COIN_LIMITED_PERIOD: { keyString = @"CC_EXPANDEDBG_NEED_UNLOCK_COIN_LIMITED_PERIOD"; } break;
        case Res::Strings::CC_EXPANDEDBG_NEED_UNLOCK_COIN_NUMBER: { keyString = @"CC_EXPANDEDBG_NEED_UNLOCK_COIN_NUMBER"; } break;
        case Res::Strings::PF_LOGIN_TEXTFIELD_EMAIL: { keyString = @"PF_LOGIN_TEXTFIELD_EMAIL"; } break;
        case Res::Strings::PF_LOGIN_TEXTFIELD_PASSWORD: { keyString = @"PF_LOGIN_TEXTFIELD_PASSWORD"; } break;
        case Res::Strings::PF_LOGIN_BUTTON_TITLE_LOGIN: { keyString = @"PF_LOGIN_BUTTON_TITLE_LOGIN"; } break;
        case Res::Strings::PF_LOGIN_BUTTON_TITLE_FORGOT: { keyString = @"PF_LOGIN_BUTTON_TITLE_FORGOT"; } break;
        case Res::Strings::PF_LOGIN_BUTTON_TITLE_SIGNUP: { keyString = @"PF_LOGIN_BUTTON_TITLE_SIGNUP"; } break;
        case Res::Strings::PF_LGOIN_BUTTON_FBLOGIN: { keyString = @"PF_LGOIN_BUTTON_FBLOGIN"; } break;
        case Res::Strings::PF_LOGIN_POPUP_ERROR_FBLOGIN: { keyString = @"PF_LOGIN_POPUP_ERROR_FBLOGIN"; } break;
        case Res::Strings::PF_LOGIN_BUTTON_TITLE_SKIP: { keyString = @"PF_LOGIN_BUTTON_TITLE_SKIP"; } break;
        case Res::Strings::PF_FORGOT_NOTICE_FORGOT_PASSWORD: { keyString = @"PF_FORGOT_NOTICE_FORGOT_PASSWORD"; } break;
        case Res::Strings::PF_FORGOT_TEXTFIELD_EMAIL: { keyString = @"PF_FORGOT_TEXTFIELD_EMAIL"; } break;
        case Res::Strings::PF_FORGOT_BUTTON_TITLE_SEND: { keyString = @"PF_FORGOT_BUTTON_TITLE_SEND"; } break;
        case Res::Strings::PF_FORGOT_POPUP_CONFIRM_PASSWORD: { keyString = @"PF_FORGOT_POPUP_CONFIRM_PASSWORD"; } break;
        case Res::Strings::PF_SIGNUP_TEXTFIELD_EMAIL: { keyString = @"PF_SIGNUP_TEXTFIELD_EMAIL"; } break;
        case Res::Strings::PF_SIGNUP_TEXTFIELD_PASSWORD: { keyString = @"PF_SIGNUP_TEXTFIELD_PASSWORD"; } break;
        case Res::Strings::PF_SIGNUP_TEXTFIELD_PASSWORD_CONDITION: { keyString = @"PF_SIGNUP_TEXTFIELD_PASSWORD_CONDITION"; } break;
        case Res::Strings::PF_SIGNUP_TEXTFIELD_PASSWORD_CONFIRM: { keyString = @"PF_SIGNUP_TEXTFIELD_PASSWORD_CONFIRM"; } break;
        case Res::Strings::PF_SIGNUP_TEXTFIELD_BIRTHYEAR: { keyString = @"PF_SIGNUP_TEXTFIELD_BIRTHYEAR"; } break;
        case Res::Strings::PF_SIGNUP_TEXTFIELD_CITY: { keyString = @"PF_SIGNUP_TEXTFIELD_CITY"; } break;
        case Res::Strings::PF_SIGNUP_RADIOTITLE_MALE: { keyString = @"PF_SIGNUP_RADIOTITLE_MALE"; } break;
        case Res::Strings::PF_SIGNUP_RADIOTITLE_FEMALE: { keyString = @"PF_SIGNUP_RADIOTITLE_FEMALE"; } break;
        case Res::Strings::PF_SIGNUP_CHECKBOX_TITLE_AGREE: { keyString = @"PF_SIGNUP_CHECKBOX_TITLE_AGREE"; } break;
        case Res::Strings::PF_SIGNUP_BUTTON_TITLE_SIGNUP: { keyString = @"PF_SIGNUP_BUTTON_TITLE_SIGNUP"; } break;
        case Res::Strings::PF_SIGNUP_POPUP_CANCEL_SIGNUP: { keyString = @"PF_SIGNUP_POPUP_CANCEL_SIGNUP"; } break;
        case Res::Strings::PF_SIGNUP_POPUP_BUTTON_TITLE_OK: { keyString = @"PF_SIGNUP_POPUP_BUTTON_TITLE_OK"; } break;
        case Res::Strings::PF_SIGNUP_POPUP_BUTTON_TITLE_OK_SIGN_UP: { keyString = @"PF_SIGNUP_POPUP_BUTTON_TITLE_OK_SIGN_UP"; } break;
        case Res::Strings::PF_SIGNUP_POPUP_BUTTON_TITLE_CANCEL: { keyString = @"PF_SIGNUP_POPUP_BUTTON_TITLE_CANCEL"; } break;
        case Res::Strings::PF_SIGNUP_POPUP_TITLE_BIRTHYEAR: { keyString = @"PF_SIGNUP_POPUP_TITLE_BIRTHYEAR"; } break;
        case Res::Strings::PF_SIGNUP_BUTTON_TITLE_SET: { keyString = @"PF_SIGNUP_BUTTON_TITLE_SET"; } break;
        case Res::Strings::PF_SIGNUP_PUPUP_TITLE_SELECT_COUNTRY: { keyString = @"PF_SIGNUP_PUPUP_TITLE_SELECT_COUNTRY"; } break;
        case Res::Strings::PF_SIGNUP_COUNTRY_LIST_OTHER: { keyString = @"PF_SIGNUP_COUNTRY_LIST_OTHER"; } break;
        case Res::Strings::PF_SIGNUP_PUPUP_TITLE_SELECT_CITY: { keyString = @"PF_SIGNUP_PUPUP_TITLE_SELECT_CITY"; } break;
        case Res::Strings::PF_SIGNUP_CHECKBOX_TITLE_AGREE_TERMS: { keyString = @"PF_SIGNUP_CHECKBOX_TITLE_AGREE_TERMS"; } break;
        case Res::Strings::PF_SIGNUP_CHECKBOX_TITLE_AGREE_PRIVACY: { keyString = @"PF_SIGNUP_CHECKBOX_TITLE_AGREE_PRIVACY"; } break;
        case Res::Strings::PF_SIGNUP_BUTTON_TITLE_NEXT: { keyString = @"PF_SIGNUP_BUTTON_TITLE_NEXT"; } break;
        case Res::Strings::CC_JOURNEYMAP_BUTTON_INTRO: { keyString = @"CC_JOURNEYMAP_BUTTON_INTRO"; } break;
        case Res::Strings::CC_JOURNEYMAP_BUTTON_FINAL: { keyString = @"CC_JOURNEYMAP_BUTTON_FINAL"; } break;
        case Res::Strings::CC_JOURNEYMAP_BOOKTITLE_DEFAULT: { keyString = @"CC_JOURNEYMAP_BOOKTITLE_DEFAULT"; } break;
        case Res::Strings::CC_JOURNEYMAP_SEPARATOR_BOOKNAME: { keyString = @"CC_JOURNEYMAP_SEPARATOR_BOOKNAME"; } break;
        case Res::Strings::CC_JOURNEYMAP_BOOKAUTHOR_DEFAULT: { keyString = @"CC_JOURNEYMAP_BOOKAUTHOR_DEFAULT"; } break;
        case Res::Strings::CC_JOURNEYMAP_BUTTON_TITLE_PLAY: { keyString = @"CC_JOURNEYMAP_BUTTON_TITLE_PLAY"; } break;
        case Res::Strings::CC_JOURNEYMAP_MENU_TITLE_DOWNLOADDEVICE: { keyString = @"CC_JOURNEYMAP_MENU_TITLE_DOWNLOADDEVICE"; } break;
        case Res::Strings::CC_JOURNEYMAP_MENU_TITLE_SHARE: { keyString = @"CC_JOURNEYMAP_MENU_TITLE_SHARE"; } break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_NOTICCE_DOWNLOADDEVICE: { keyString = @"CC_JOURNEYMAP_POPUP_NOTICCE_DOWNLOADDEVICE"; } break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_NOTICE_SHARE: { keyString = @"CC_JOURNEYMAP_POPUP_NOTICE_SHARE"; } break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_TITLE_SHARE_LIST: { keyString = @"CC_JOURNEYMAP_POPUP_TITLE_SHARE_LIST"; } break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_SHARELIST_FACEBOOK: { keyString = @"CC_JOURNEYMAP_POPUP_SHARELIST_FACEBOOK"; } break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_SHARELIST_YOUTUBE: { keyString = @"CC_JOURNEYMAP_POPUP_SHARELIST_YOUTUBE"; } break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_ERROR_SHARE: { keyString = @"CC_JOURNEYMAP_POPUP_ERROR_SHARE"; } break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_WARNING_DELETE_PAGE: { keyString = @"CC_JOURNEYMAP_POPUP_WARNING_DELETE_PAGE"; } break;
        case Res::Strings::CC_JOURNEYMAP_BUTTON_TITLE_CANCEL: { keyString = @"CC_JOURNEYMAP_BUTTON_TITLE_CANCEL"; } break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_WARNING_CANCEL_CONVERT: { keyString = @"CC_JOURNEYMAP_POPUP_WARNING_CANCEL_CONVERT"; } break;
        case Res::Strings::CC_JOURNEYMAP_POPUP_WARNING_NOTENOUGH_STORAGE: { keyString = @"CC_JOURNEYMAP_POPUP_WARNING_NOTENOUGH_STORAGE"; } break;
        case Res::Strings::PF_SHARECOMMENT_POPUP_TITLE: { keyString = @"PF_SHARECOMMENT_POPUP_TITLE"; } break;
        case Res::Strings::PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_FACEBOOK: { keyString = @"PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_FACEBOOK"; } break;
        case Res::Strings::PF_SHARECOMMNET_POPUP_HASHTAG_FACEBOOK: { keyString = @"PF_SHARECOMMNET_POPUP_HASHTAG_FACEBOOK"; } break;
        case Res::Strings::PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_YOUTUBE: { keyString = @"PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_YOUTUBE"; } break;
        case Res::Strings::PF_SHARECOMMNET_POPUP_HASHTAG_YOUTUBE: { keyString = @"PF_SHARECOMMNET_POPUP_HASHTAG_YOUTUBE"; } break;
        case Res::Strings::CC_JOURNEYMAP_SHARE_PROGRESS_NOTICE: { keyString = @"CC_JOURNEYMAP_SHARE_PROGRESS_NOTICE"; } break;
        case Res::Strings::CC_SETPOSITION_BUTTON_TITLE_RECORD: { keyString = @"CC_SETPOSITION_BUTTON_TITLE_RECORD"; } break;
        case Res::Strings::CC_SETPOSITION_POPUP_NOTICE_AUTOSELECT_LASTPAGE: { keyString = @"CC_SETPOSITION_POPUP_NOTICE_AUTOSELECT_LASTPAGE"; } break;
        case Res::Strings::CC_SETPOSITION_POPUP_WARNING_LIMITED_REDUCED: { keyString = @"CC_SETPOSITION_POPUP_WARNING_LIMITED_REDUCED"; } break;
        case Res::Strings::CC_SETPOSITION_POPUP_WARNING_ITEM_REUNLOCK: { keyString = @"CC_SETPOSITION_POPUP_WARNING_ITEM_REUNLOCK"; } break;
        case Res::Strings::CC_SETPOSTION_POPUP_WARNING_DELETE_CHARACTER: { keyString = @"CC_SETPOSTION_POPUP_WARNING_DELETE_CHARACTER"; } break;
        case Res::Strings::CC_SETPOSTION_POPUP_WARNING_DELETE_BACKGROUND: { keyString = @"CC_SETPOSTION_POPUP_WARNING_DELETE_BACKGROUND"; } break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_NONE: { keyString = @"CC_LIST_MUSIC_TITLE_NONE"; } break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_JOLLY: { keyString = @"CC_LIST_MUSIC_TITLE_JOLLY"; } break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_DREAM: { keyString = @"CC_LIST_MUSIC_TITLE_DREAM"; } break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_BRAVE: { keyString = @"CC_LIST_MUSIC_TITLE_BRAVE"; } break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_SHINE: { keyString = @"CC_LIST_MUSIC_TITLE_SHINE"; } break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_BRISK: { keyString = @"CC_LIST_MUSIC_TITLE_BRISK"; } break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_ANGRY: { keyString = @"CC_LIST_MUSIC_TITLE_ANGRY"; } break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_SLEEPY: { keyString = @"CC_LIST_MUSIC_TITLE_SLEEPY"; } break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_ADVENTURE: { keyString = @"CC_LIST_MUSIC_TITLE_ADVENTURE"; } break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_CRUISE: { keyString = @"CC_LIST_MUSIC_TITLE_CRUISE"; } break;
        case Res::Strings::CC_LIST_MUSIC_TITLE_JUNGLE: { keyString = @"CC_LIST_MUSIC_TITLE_JUNGLE"; } break;
        case Res::Strings::CC_RECORDING_BUTTON_TITLE_DONE: { keyString = @"CC_RECORDING_BUTTON_TITLE_DONE"; } break;
        case Res::Strings::CC_RECORDING_POPUP_WARNING_WITHOUT_SAVING: { keyString = @"CC_RECORDING_POPUP_WARNING_WITHOUT_SAVING"; } break;
        case Res::Strings::CC_RECORDING_BUTTON_TITLE_RECOD: { keyString = @"CC_RECORDING_BUTTON_TITLE_RECOD"; } break;
        case Res::Strings::CC_RECORDING_BUTTON_TITLE_RECODING: { keyString = @"CC_RECORDING_BUTTON_TITLE_RECODING"; } break;
        case Res::Strings::CC_RECORDING_BUTTON_TITLE_RERECORDING: { keyString = @"CC_RECORDING_BUTTON_TITLE_RERECORDING"; } break;
        case Res::Strings::CC_RECORDING_BUTTON_TITLE_SAVE: { keyString = @"CC_RECORDING_BUTTON_TITLE_SAVE"; } break;
        case Res::Strings::CC_RECORDING_POPUP_WARNING_RERECORDING: { keyString = @"CC_RECORDING_POPUP_WARNING_RERECORDING"; } break;
//        case Res::Strings::CC_RECORDING_POPUP_WARNING_WITHOUT_SAVING: { keyString = @"CC_RECORDING_POPUP_WARNING_WITHOUT_SAVING"; } break;
        case Res::Strings::CC_PLAY_PAGE_TITLE_INTRO: { keyString = @"CC_PLAY_PAGE_TITLE_INTRO"; } break;
        case Res::Strings::CC_PLAY_PAGE_TITLE_PAGE_N: { keyString = @"CC_PLAY_PAGE_TITLE_PAGE_N"; } break;
        case Res::Strings::CC_PLAY_PAGE_TITLE_FINAL: { keyString = @"CC_PLAY_PAGE_TITLE_FINAL"; } break;
        case Res::Strings::PF_SETTING_NOTICE_MORE_ADVANTAGE_LOGIN: { keyString = @"PF_SETTING_NOTICE_MORE_ADVANTAGE_LOGIN"; } break;
        case Res::Strings::PF_SETTING_BUTTON_TITLE_LOGIN: { keyString = @"PF_SETTING_BUTTON_TITLE_LOGIN"; } break;
        case Res::Strings::PF_SETTING_SOUND: { keyString = @"PF_SETTING_SOUND"; } break;
        case Res::Strings::PF_SETTING_BUTTON_TITLE_ABOUT: { keyString = @"PF_SETTING_BUTTON_TITLE_ABOUT"; } break;
        case Res::Strings::PF_SETTING_BUTTON_TITLE_HELP: { keyString = @"PF_SETTING_BUTTON_TITLE_HELP"; } break;
        case Res::Strings::PF_SETTING_BUTTON_TITLE_EDIT: { keyString = @"PF_SETTING_BUTTON_TITLE_EDIT"; } break;
        case Res::Strings::PF_SETTING_BUTTON_TITLE_USAGE_COIN: { keyString = @"PF_SETTING_BUTTON_TITLE_USAGE_COIN"; } break;
        case Res::Strings::PF_SETTING_BUTTON_TITLE_DELETE_ACCOUNT: { keyString = @"PF_SETTING_BUTTON_TITLE_DELETE_ACCOUNT"; } break;
        case Res::Strings::PF_EDITACCOUNT_TEXTFIELD_CURRENT_PASSWORD: { keyString = @"PF_EDITACCOUNT_TEXTFIELD_CURRENT_PASSWORD"; } break;
        case Res::Strings::PF_EDITACCOUNT_TEXTFIELD_PASSWORD: { keyString = @"PF_EDITACCOUNT_TEXTFIELD_PASSWORD"; } break;
        case Res::Strings::PF_EDITACCOUNT_TEXTFIELD_PASSWORD_CONDITION: { keyString = @"PF_EDITACCOUNT_TEXTFIELD_PASSWORD_CONDITION"; } break;
        case Res::Strings::PF_EDITACCOUNT_TEXTFIELD_CONFIRM_PASSWORD: { keyString = @"PF_EDITACCOUNT_TEXTFIELD_CONFIRM_PASSWORD"; } break;
        case Res::Strings::PF_EDITACCOUNT_BUTTON_TITLE_SAVE: { keyString = @"PF_EDITACCOUNT_BUTTON_TITLE_SAVE"; } break;
        case Res::Strings::PF_USAGECOIN_NOTICE: { keyString = @"PF_USAGECOIN_NOTICE"; } break;
        case Res::Strings::PF_USAGECOIN_LISTFIELD_DETAILS: { keyString = @"PF_USAGECOIN_LISTFIELD_DETAILS"; } break;
        case Res::Strings::PF_USAGECOIN_LISTFIELD_DATE: { keyString = @"PF_USAGECOIN_LISTFIELD_DATE"; } break;
        case Res::Strings::PF_USAGECOIN_BUTTON_TITLE_HELP: { keyString = @"PF_USAGECOIN_BUTTON_TITLE_HELP"; } break;
        case Res::Strings::PF_USAGECOIN_NOTICE_NOHISTORY: { keyString = @"PF_USAGECOIN_NOTICE_NOHISTORY"; } break;
        case Res::Strings::PF_USAGECOIN_UNLIMITED: { keyString = @"PF_USAGECOIN_UNLIMITED"; } break;
        case Res::Strings::PF_USAGECOIN_LIMITED_PERIOD: { keyString = @"PF_USAGECOIN_LIMITED_PERIOD"; } break;
        case Res::Strings::PF_DELETEACCOUNT_NOTICE: { keyString = @"PF_DELETEACCOUNT_NOTICE"; } break;
        case Res::Strings::PF_DELETEACCOUNT_CHECKBOX_CONFIRM: { keyString = @"PF_DELETEACCOUNT_CHECKBOX_CONFIRM"; } break;
        case Res::Strings::PF_DELETEACCOUNT_TEXTFIELD_CONFIRM_PASSWORD: { keyString = @"PF_DELETEACCOUNT_TEXTFIELD_CONFIRM_PASSWORD"; } break;
        case Res::Strings::PF_DELETEACCOUNT_BUTTON_TITLE_DELETE: { keyString = @"PF_DELETEACCOUNT_BUTTON_TITLE_DELETE"; } break;
        case Res::Strings::PF_DELETEACCOUNT_TABLE_TITLEE_DOLECOIN: { keyString = @"PF_DELETEACCOUNT_TABLE_TITLEE_DOLECOIN"; } break;
        case Res::Strings::PF_DELETEACCOUNT_TABLE_TITLEE_BOBBY: { keyString = @"PF_DELETEACCOUNT_TABLE_TITLEE_BOBBY"; } break;
        case Res::Strings::PF_DELETEACCOUNT_TABLE_SUBTITLE_DOLECOIN: { keyString = @"PF_DELETEACCOUNT_TABLE_SUBTITLE_DOLECOIN"; } break;
        case Res::Strings::PF_DELETEACCOUNT_TABLE_SUBTITLE_BOBBY: { keyString = @"PF_DELETEACCOUNT_TABLE_SUBTITLE_BOBBY"; } break;
        case Res::Strings::PF_DELETEACCOUNT_TABLE_SUBTITLE_BOBBY_ADD: { keyString = @"PF_DELETEACCOUNT_TABLE_SUBTITLE_BOBBY_ADD"; } break;
        case Res::Strings::PF_ABOUT_APPTITLE: { keyString = @"PF_ABOUT_APPTITLE"; } break;
        case Res::Strings::PF_ABOUT_SERVICE_INTRODUCE: { keyString = @"PF_ABOUT_SERVICE_INTRODUCE"; } break;
        case Res::Strings::PF_ABOUT_BUTTON_TITLE_TERMS: { keyString = @"PF_ABOUT_BUTTON_TITLE_TERMS"; } break;
        case Res::Strings::PF_ABOUT_TITLE_SERVICE: { keyString = @"PF_ABOUT_TITLE_SERVICE"; } break;
        case Res::Strings::PF_ABOUT_TITLE_PRIVACY: { keyString = @"PF_ABOUT_TITLE_PRIVACY"; } break;
        case Res::Strings::PF_HELP_BUTTON_TITLE_FAQ: { keyString = @"PF_HELP_BUTTON_TITLE_FAQ"; } break;
        case Res::Strings::PF_HELP_TEXTFIELD_EMAIL: { keyString = @"PF_HELP_TEXTFIELD_EMAIL"; } break;
        case Res::Strings::PF_HELP_TEXTFIELD_FEEDBACK: { keyString = @"PF_HELP_TEXTFIELD_FEEDBACK"; } break;
        case Res::Strings::PF_HELP_DEVICE: { keyString = @"PF_HELP_DEVICE"; } break;
        case Res::Strings::PF_HELP_WARNING_PRIVACYINFO: { keyString = @"PF_HELP_WARNING_PRIVACYINFO"; } break;
        case Res::Strings::PF_HELP_BUTTON_TITLE_SEND: { keyString = @"PF_HELP_BUTTON_TITLE_SEND"; } break;
        case Res::Strings::CC_GUIDE_HOME: { keyString = @"CC_GUIDE_HOME"; } break;
        case Res::Strings::CC_GUIDE_JOURNEYMAP_NEWPAGE: { keyString = @"CC_GUIDE_JOURNEYMAP_NEWPAGE"; } break;
        case Res::Strings::CC_GUIDE_JOURNEYMAP_ALLPLAY: { keyString = @"CC_GUIDE_JOURNEYMAP_ALLPLAY"; } break;
        case Res::Strings::CC_GUIDE_SETPOSITION_SELECT_ITEM: { keyString = @"CC_GUIDE_SETPOSITION_SELECT_ITEM"; } break;
        case Res::Strings::CC_GUIDE_SETPOSITION_SELECT_BACKGROUND: { keyString = @"CC_GUIDE_SETPOSITION_SELECT_BACKGROUND"; } break;
        case Res::Strings::CC_GUIDE_SETPOSITION_SELECT_MUSIC: { keyString = @"CC_GUIDE_SETPOSITION_SELECT_MUSIC"; } break;
        case Res::Strings::CC_GUIDE_SETPOSITION_DELETE_ITEM: { keyString = @"CC_GUIDE_SETPOSITION_DELETE_ITEM"; } break;
        case Res::Strings::CC_GUIDE_SETPOSITION_RECORDING: { keyString = @"CC_GUIDE_SETPOSITION_RECORDING"; } break;
        case Res::Strings::PF_GUIDE_EDIT_PICTURE: { keyString = @"PF_GUIDE_EDIT_PICTURE"; } break;
        case Res::Strings::PF_GUIDE_LOGIN_SIGNUP: { keyString = @"PF_GUIDE_LOGIN_SIGNUP"; } break;
        case Res::Strings::PF_GUIDE_LOGIN_EARNCOIN: { keyString = @"PF_GUIDE_LOGIN_EARNCOIN"; } break;
        case Res::Strings::PF_GUIDE_LOGIN_GETITEM: { keyString = @"PF_GUIDE_LOGIN_GETITEM"; } break;
        case Res::Strings::PF_GUIDE_LOGIN_SHARE: { keyString = @"PF_GUIDE_LOGIN_SHARE"; } break;
        case Res::Strings::PF_LOGIN_POPUP_ERROR_NOTAVAILABLE_EMAIL: { keyString = @"PF_LOGIN_POPUP_ERROR_NOTAVAILABLE_EMAIL"; } break;
        case Res::Strings::CC_HOME_POPUP_UNLOCK_ITEM_MESSAGE: { keyString = @"CC_HOME_POPUP_UNLOCK_ITEM_MESSAGE"; } break;
        case Res::Strings::CC_JOURNEYMAP__DOWNLOAD_DEVICE_COMPLETE: { keyString = @"CC_JOURNEYMAP__DOWNLOAD_DEVICE_COMPLETE"; } break;
        case Res::Strings::CC_JOURNEYMAP__SHARE_COMPLETE: { keyString = @"CC_JOURNEYMAP__SHARE_COMPLETE"; } break;
        case Res::Strings::CC_SETPOSITION_POPUP_WARNING_ITEM_FULL: { keyString = @"CC_SETPOSITION_POPUP_WARNING_ITEM_FULL"; } break;
        case Res::Strings::PF_EDITACCOUNT_POPUP_INPUT_PASSWORD: { keyString = @"PF_EDITACCOUNT_POPUP_INPUT_PASSWORD"; } break;
        case Res::Strings::PF_EDITACCOUNT_POPUP_SAVE_COMPLETE: { keyString = @"PF_EDITACCOUNT_POPUP_SAVE_COMPLETE"; } break;
        case Res::Strings::PF_HELP_POPUP_SEND_COMPLETE: { keyString = @"PF_HELP_POPUP_SEND_COMPLETE"; } break;
        case Res::Strings::PF_DELETEACCOUNT_DELETE_COMPLETE: { keyString = @"PF_DELETEACCOUNT_DELETE_COMPLETE"; } break;
        case Res::Strings::PF_PROMOTION_CHECKBOX: { keyString = @"PF_PROMOTION_CHECKBOX"; } break;
        case Res::Strings::CC_POPUP_CANCEL_SHARE: { keyString = @"CC_POPUP_CANCEL_SHARE"; } break;
        case Res::Strings::CC_PRLOAD_BOOKTITLE_1: { keyString = @"CC_PRLOAD_BOOKTITLE_1"; } break;
        case Res::Strings::CC_PRLOAD_BOOKTITLE_2: { keyString = @"CC_PRLOAD_BOOKTITLE_2"; } break;
        case Res::Strings::CM_POPUP_BUTTON_TITLE_YES: { keyString = @"CM_POPUP_BUTTON_TITLE_YES"; } break;
        case Res::Strings::CM_POPUP_BUTTON_TITLE_NO: { keyString = @"CM_POPUP_BUTTON_TITLE_NO"; } break;
        case Res::Strings::PF_NET_ERROR_EMAIL_NOT_AVAILABLE: { keyString = @"PF_NET_ERROR_EMAIL_NOT_AVAILABLE"; } break;
        default: { } break;
    }
    
    if (keyString == nil) {
        return "Undefined string";
    }
    
    NSString *localizedString = NSLocalizedString(keyString, keyString);
    const char * localizedCString = [localizedString cStringUsingEncoding:NSUTF8StringEncoding];
    return localizedCString;
}


NS_CC_END

#endif // CC_PLATFORM_IOS

