import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

public class XmlCreator {
	private static final String SIZE = "size";
	private static final String NAME = "name";

	private static final String DEFAULT_PATH = "Z:\\usr\\Rig\\cocos2d-x-3.0\\workspace\\bobby\\Resources";
	private static final String INFO_FILE = "info.xml";

	public static void main(String[] args) {
		Document doc = new Document();  
		Element resources = new Element("resources");

		File current = new File(DEFAULT_PATH);
		listUp(resources, current.listFiles());

		doc.setRootElement(resources);

		try {                                                             
			FileOutputStream out = new FileOutputStream(DEFAULT_PATH + File.separator + INFO_FILE);
			XMLOutputter serializer = new XMLOutputter();

			Format f = serializer.getFormat();
			f.setEncoding("UTF-8");
			f.setIndent("\t");
			f.setLineSeparator("\r\n");
			f.setTextMode(Format.TextMode.TRIM);
			serializer.setFormat(f);

			serializer.output(doc, out);
			out.flush();
			out.close();
		} catch (IOException e) {
			System.err.println(e);
		}
	}

	private static void listUp(Element resources, File[] files) {
		for(int i = 0 ; i < files.length ; i++) {
			String filename = files[i].toString();
			if(files[i].isDirectory()) {
				listUp(resources, files[i].listFiles());
			} else {
				Element item = new Element("file");
				String currentName = filename.substring(DEFAULT_PATH.length() + 1);
				if(currentName.equals(INFO_FILE))
					continue;
				item.setText(currentName.replace("\\", "/"));
				item.setAttribute(SIZE, Long.toString(files[i].length()));
				resources.addContent(item);
			}
		}
	}
}