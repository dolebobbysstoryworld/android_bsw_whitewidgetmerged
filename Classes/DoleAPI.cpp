//
//  DoleAPI.cpp
//  bobby
//
//  Created by Jaemok Jeong on 5/19/14.
//
//

#include "DoleAPI.h"
#include "Define.h"

#include "network/HttpClient.h"
#include "external/tinyxml2/tinyxml2.h"

using namespace rapidjson;

static DoleAPI *p_DoleAPI;

//#define SERVICE_URL             "https://%s.ithinkcore.net/%s/Api.svc/%s"
//#define SERVER_TYPE_ALPHA       "dole-alpha"
//#define SERVER_TYPE_PRODUCTION  "dole"

#define SERVICE_URL             "http://doleserver.com:%s/Api.svc/%s"
#define SERVER_TYPE_ALPHA       "4000"
#define SERVER_TYPE_PRODUCTION  "3000"

#if COCOS2D_DEBUG
//#define CURRENT_SERVER_TYPE     SERVER_TYPE_ALPHA
#define CURRENT_SERVER_TYPE     SERVER_TYPE_PRODUCTION
#else
#define CURRENT_SERVER_TYPE     SERVER_TYPE_PRODUCTION
#endif

#define SERVICE_CODE            "SVR004"


static inline const char *getPlatformName() {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    return "Android";
#else
    return "iOS";
#endif
}


DoleAPI *DoleAPI::getInstance() {
    if (p_DoleAPI) {
        return p_DoleAPI;
    }
    else {
        p_DoleAPI = new DoleAPI();
        return p_DoleAPI;
    }
}

#pragma mark - Shop API
void DoleAPI::getProductSales(const cocos2d::network::ccHttpRequestCallback& callback) {
    cocos2d::network::HttpRequest *request = new cocos2d::network::HttpRequest();
    __String *urlString =
    __String::createWithFormat("http://doleserver.com:%s/api/v2/Shop/odata/%s/ProductSales",
                               CURRENT_SERVER_TYPE, SERVICE_CODE);
    request->setUrl(urlString->getCString());
    log("getProductSales URL : %s", urlString->getCString());
    request->setRequestType(cocos2d::network::HttpRequest::Type::GET);
    
    request->setResponseCallback(CC_CALLBACK_2(DoleAPI::onProductSalesRequestCompleted, this));
    _productsRequestCallback = callback;

    request->setTag("GetProductSales");
    std::vector<std::string> headers;
    headers.push_back("Content-Type: application/json; charset=utf-8");
    request->setHeaders(headers);
    cocos2d::network::HttpClient::getInstance()->setTimeoutForConnect(15);
    cocos2d::network::HttpClient::getInstance()->send(request);
    request->release();
}

void DoleAPI::onProductSalesRequestCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response) {
    if (!response) { return; }
    
    if (0 != strlen(response->getHttpRequest()->getTag())) {
        log("%s completed", response->getHttpRequest()->getTag());
        log("HTTP Status Code: %ld, tag = %s", response->getResponseCode(), response->getHttpRequest()->getTag());
    }
    if (!response->isSucceed()) {
        log("response failed");
        log("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    std::vector<char> *buffer = response->getResponseData();
    std::string str(buffer->begin(), buffer->end());
    
    UserDefault::getInstance()->setStringForKey(UDKEY_STRING_PRODUCTSALES, str);
    UserDefault::getInstance()->flush();
    
    if (_productsRequestCallback)
        _productsRequestCallback(sender, response);
}

//void DoleAPI::onHttpRequestCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response) {
//    if (!response) {
//        return;
//    }
//    
//    // You can get original request type from: response->request->reqType
//    if (0 != strlen(response->getHttpRequest()->getTag())) {
//        log("%s completed", response->getHttpRequest()->getTag());
//        log("HTTP Status Code: %ld, tag = %s", response->getResponseCode(), response->getHttpRequest()->getTag());
//    }
//    if (!response->isSucceed()) {
//        log("response failed");
//        log("error buffer: %s", response->getErrorBuffer());
//        return;
//    }
//    
//    // dump data
//    std::vector<char> *buffer = response->getResponseData();
//    printf("Http Test, dump data: ");
//    for (unsigned int i = 0; i < buffer->size(); i++) {
//        printf("%c", (*buffer)[i]);
//    }
//    printf("\n");
//    
//    std::string str(buffer->begin(), buffer->end());
//    printf("Json: %s\n", str.c_str());
//    if (strlen(str.c_str()) > 0) {
//        rapidjson::Document ret;
//        ret.Parse<0>(str.c_str());
//        int retValue = ret["Return"].GetBool();
//        int retCode = ret["ReturnCode"].GetInt();
//        printf("Parsed Data: retValue: %d, retCode: %d\n", retValue, retCode);
//    }
//}
