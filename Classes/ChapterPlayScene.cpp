//
//  ChapterPlayScene.cpp
//  bobby
//
//  Created by Dongwook, Kim on 14. 5. 29..
//
//

#include "ChapterPlayScene.h"
#include "ResourceManager.h"
#include "Character.h"
#include "Define.h"
#include "SimpleAudioEngine.h"
#include "external/json/document.h"
#include "external/json/prettywriter.h"
#include "external/json/stringbuffer.h"
#include "BGMManager.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
#include "VoiceRecorder.h"
#endif
#endif

#define TAG_PROGRESS_LEFT   10
#define TAG_PROGRESS_CENTER 11
#define TAG_PROGRESS_RIGHT  12

#define UI_HIDE_DELAY   2.0f

Scene* ChapterPlay::createScene(__String* bookKey, int chapterIndex)
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = ChapterPlay::create(bookKey, chapterIndex);

    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

ChapterPlay* ChapterPlay::create(__String* bookKey, int chapterIndex)
{
    ChapterPlay *pRet = new ChapterPlay();
    pRet->bookKey = bookKey;
    pRet->chapterIndex = chapterIndex;
    
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

// on "init" you need to initialize your instance
bool ChapterPlay::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LayerColor::initWithColor( Color4B(0xba,0xea,0xed,0xff)) )
    {
        return false;
    }
    
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    
    btnBack = MenuItemImage::create("btn_map_back_normal.png",
                                         "btn_map_back_press.png",
                                         CC_CALLBACK_1(ChapterPlay::btnBackCallback, this));
    btnBack->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
	btnBack->setPosition(cocos2d::Point(BOOKMAP_BTN_BACK));
    
    playMenuItem = MenuItemImage::create("btn_set_record_play_normal.png", "btn_set_record_play_pressed.png",
                                              CC_CALLBACK_1(ChapterPlay::playMenuCallback, this));
    playMenuItem->setPosition(cocos2d::Point(visibleSize.width/2, visibleSize.height/2));
    playMenuItem->setVisible(false);

    pauseMenuItem = MenuItemImage::create("btn_set_record_pause_normal.png", "btn_set_record_pause_pressed.png",
                                         CC_CALLBACK_1(ChapterPlay::playMenuCallback, this));
    pauseMenuItem->setPosition(cocos2d::Point(visibleSize.width/2, visibleSize.height/2));
    
    auto menu = Menu::create(btnBack, NULL);
    menu->setPosition(cocos2d::Point::ZERO);
    this->addChild(menu, 13);

    controlMenu = Menu::create(playMenuItem, pauseMenuItem, NULL);
    controlMenu->setPosition(cocos2d::Point::ZERO);
    this->addChild(controlMenu, 13);
    
    this->createProgressBar();

    hideUIAction = nullptr;
    visibleUI =false;
    
    auto pageTitle = this->createTitle(this->chapterIndex);
    pageTitle->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_BOTTOM);
    pageTitle->setPosition(cocos2d::Point(playMenuItem->getContentSize().width/2, MAP_PLAY_TITLE_BOTTOM_MARGIN));
    playMenuItem->addChild(pageTitle);
    
    auto pageTitle2 = this->createTitle(this->chapterIndex);
    pageTitle2->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_BOTTOM);
    pageTitle2->setPosition(cocos2d::Point(pauseMenuItem->getContentSize().width/2, MAP_PLAY_TITLE_BOTTOM_MARGIN));
    pauseMenuItem->addChild(pageTitle2);
    
    player = ChapterPlayer::create();
    this->addChild(player, 0);
    player->setDelegate(this);
    player->loadJsonData(this->bookKey->getCString(), chapterIndex);
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(ChapterPlay::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(ChapterPlay::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(ChapterPlay::onTouchEnded, this);
    listener->onTouchCancelled = CC_CALLBACK_2(ChapterPlay::onTouchCancelled, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    showUI(false);
    
    this->scheduleUpdate();

    return true;
}

Label *ChapterPlay::createTitle(int chapterIndex)
{
    __String *chapterTitle = NULL;

    switch (chapterIndex) {
        case 1:
            chapterTitle = __String::create(Device::getResString(Res::Strings::CC_PLAY_PAGE_TITLE_INTRO));//"Intro"
            break;
        case 2:
        case 3:
        case 4:
            chapterTitle = __String::createWithFormat(Device::getResString(Res::Strings::CC_PLAY_PAGE_TITLE_PAGE_N), chapterIndex);//"Page %d"
            break;
        case 5:
            chapterTitle = __String::create(Device::getResString(Res::Strings::CC_PLAY_PAGE_TITLE_FINAL));//"Final"
            break;
        default:
            return NULL;
    }
    
    auto pageTitle = cocos2d::Label::createWithSystemFont(chapterTitle->getCString(), FONT_NAME_BOLD, MAP_PLAY_TITLE_TEXT_SIZE);
    pageTitle->setDimensions(MAP_PLAY_TITLE_SIZE.width, MAP_PLAY_TITLE_SIZE.height);
    pageTitle->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::CENTER);
    pageTitle->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    pageTitle->setColor(cocos2d::Color3B(255, 255, 255));
    return pageTitle;
}

void ChapterPlay::createProgressBar()
{
    playProgressBarHolder = Node::create();
    this->addChild(playProgressBarHolder, 15);
    
    // play progress bar bg
    playProgressBarBg = cocos2d::Node::create();
    playProgressBarBg->setContentSize(RECORDING_PROGRESS_BAR_SIZE);
    playProgressBarBg->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarBg->setPosition(RECORDING_PROGRESS_BAR);
    auto playProgressBarBgLeft = Sprite::create("recording_play_progress_bg_left.png");
    playProgressBarBgLeft->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    
    auto playProgressBarBgRight = Sprite::create("recording_play_progress_bg_right.png");
    
    auto playCenterBgWidth = RECORDING_PROGRESS_BAR_SIZE.width - playProgressBarBgLeft->getContentSize().width - playProgressBarBgRight->getContentSize().width;
    auto playProgressBarBgCenter = Sprite::create("recording_play_progress_bg_center.png", cocos2d::Rect(0, 0, playCenterBgWidth, RECORDING_PROGRESS_BAR_SIZE.height));
    playProgressBarBgCenter->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarBgCenter->setPosition(cocos2d::Point(playProgressBarBgLeft->getContentSize().width, 0));
    
    playProgressBarBgRight->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarBgRight->setPosition(cocos2d::Point(playProgressBarBgLeft->getContentSize().width + playCenterBgWidth, 0));
    
    playProgressBarBg->addChild(playProgressBarBgLeft);
    playProgressBarBg->addChild(playProgressBarBgRight);
    playProgressBarBg->addChild(playProgressBarBgCenter);
//    playProgressBarBg->setVisible(true);
    
    playProgressBarHolder->addChild(playProgressBarBg, 15);
    
    // play progress bar
    playProgressBar = cocos2d::Node::create();
    playProgressBar->setContentSize(RECORDING_PROGRESS_BAR_SIZE);
    playProgressBar->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBar->setPosition(RECORDING_PROGRESS_BAR);
    playProgressBar->setVisible(false);
    auto playProgressBarLeft = Sprite::create("recording_play_progress_left.png");
    playProgressBarLeft->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarLeft->setTag(TAG_PROGRESS_LEFT);
    
    auto playProgressBarRight = Sprite::create("recording_play_progress_right.png");
    playProgressBarRight->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarRight->setPosition(cocos2d::Point(playProgressBarLeft->getContentSize().width, 0));
    playProgressBarRight->setTag(TAG_PROGRESS_RIGHT);
    
    playProgressBar->addChild(playProgressBarLeft);
    playProgressBar->addChild(playProgressBarRight);
//    playProgressBar->setVisible(true);
    
    playProgressBarHolder->addChild(playProgressBar, 15);
}

void ChapterPlay::setProgress(float value)
{
    if (value > 1.0f) {
        log("progress bar - reached to end");
        return;
    }
    
    if (value == 0) {
        playProgressBar->setVisible(false);
    } else {
        playProgressBar->setVisible(true);
    }

    Node *targetBar = playProgressBar;
    std::string centerFileName = "recording_play_progress_center.png";
    
    auto center = targetBar->getChildByTag(TAG_PROGRESS_CENTER);
    
    auto progressBarLeft = targetBar->getChildByTag(TAG_PROGRESS_LEFT);
    auto progressBarRight = targetBar->getChildByTag(TAG_PROGRESS_RIGHT);
    
    auto progressWidth = RECORDING_PROGRESS_BAR_SIZE.width - progressBarLeft->getContentSize().width - progressBarRight->getContentSize().width;
    auto centerWidth = std::ceil(progressWidth * value);
    
    if (center) {
        center->removeFromParentAndCleanup(true);
    }
    
    if (centerWidth > 0) {
        auto newCenter = Sprite::create(centerFileName, cocos2d::Rect(0, 0, centerWidth, RECORDING_PROGRESS_BAR_SIZE.height));
        newCenter->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        newCenter->setPosition(cocos2d::Point(progressBarLeft->getContentSize().width, 0));
        newCenter->setTag(TAG_PROGRESS_CENTER);
        targetBar->addChild(newCenter);
    }
    progressBarRight->setPosition(cocos2d::Point(progressBarLeft->getContentSize().width + centerWidth, 0));
}

void ChapterPlay::onEnter()
{
    Layer::onEnter();

    TSBGMManager::getInstance()->stop();
    auto keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyReleased = CC_CALLBACK_2(ChapterPlay::onKeyReleased, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(keyboardListener, this);
}

void ChapterPlay::onEnterTransitionDidFinish()
{
    this->play();
    
//    showUI(true);
//    triggerHideUI();
}

void ChapterPlay::onExit()
{
    _eventDispatcher->removeEventListenersForTarget(this);

    Layer::onExit();
}

bool ChapterPlay::onTouchBegan(cocos2d::Touch* touch, cocos2d::Event  *event)
{
    if (visibleUI) {
        showUI(false);
    } else {
        showUI(true);
        triggerHideUI();
    }

    return true;
}

void ChapterPlay::onTouchMoved(cocos2d::Touch* touch, cocos2d::Event  *event)
{
}

void ChapterPlay::onTouchEnded(cocos2d::Touch* touch, cocos2d::Event  *event)
{
}

void ChapterPlay::onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event  *event)
{
}

void ChapterPlay::chapterPlayFinished(int chapterIndex)
{
    pauseMenuItem->setVisible(false);
    playMenuItem->setVisible(true);
    
    showUI(true);
    this->setProgress(0.0);
}

void ChapterPlay::onTransitionFinished(int chapterIndex)
{
    
}

void ChapterPlay::showUI(bool show)
{
    visibleUI = show;
    
    controlMenu->setVisible(show);
    btnBack->setVisible(show);
    playProgressBarHolder->setVisible(show);
    
    if(show) {
        cancelHideUI();
    }
}

void ChapterPlay::triggerHideUI()
{
    if(hideUIAction == NULL) {
        auto delay = DelayTime::create(UI_HIDE_DELAY);
        hideUIAction = Sequence::create(delay, CallFunc::create(CC_CALLBACK_0(ChapterPlay::showUI, this, false)), NULL);
        this->runAction(hideUIAction);
    }
}

void ChapterPlay::cancelHideUI()
{
    if (hideUIAction != NULL){
        this->stopAction(hideUIAction);
        hideUIAction = NULL;
        log("hideUI cancel");
    }
}

void ChapterPlay::play()
{
    player->play();
    
    pauseMenuItem->setVisible(true);
    playMenuItem->setVisible(false);
    
    showUI(false);
    cancelHideUI();
}

void ChapterPlay::pausePlay()
{
    player->pausePlay();
    
    pauseMenuItem->setVisible(false);
    playMenuItem->setVisible(true);

    showUI(true);
}

void ChapterPlay::resumePlay()
{
    player->resumePlay();
    
    pauseMenuItem->setVisible(true);
    playMenuItem->setVisible(false);

    showUI(false);
}

void ChapterPlay::finishPlaying()
{
    player->finishPlay();

    pauseMenuItem->setVisible(false);
    playMenuItem->setVisible(true);
    
    showUI(true);
    this->setProgress(0.0);
}

void ChapterPlay::update(float delta)
{
    if (player->isPlaying()) {
        float progress = player->getPlayProgress();
        this->setProgress(progress);
    }
}

void ChapterPlay::btnBackCallback(Ref* pSender)
{
    if (pSender != nullptr) {
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Tap.wav", false);
    }
    
    player->clear();
    
//    this->removeAllChildren();
    auto director = Director::getInstance();
    director->popSceneWithTransition([](Scene *scene) {
        return TransitionProgressOutIn::create(0.4f, scene);
    });
}

void ChapterPlay::playMenuCallback(Ref* pSender)
{
    if (pSender != nullptr) {
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Tap.wav", false);
    }

    if (player->isPlaying()) {
        this->pausePlay();
    } else {
        if (player->isPaused()) {
            this->resumePlay();
        } else {
            this->play();
        }
    }
//    this->play();
}

void ChapterPlay::onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	if(keyCode == cocos2d::EventKeyboard::KeyCode::KEY_BACKSPACE) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		btnBackCallback(nullptr);
#endif
	}
}