#include "AppDelegate.h"
#include "HomeScene.h"
#include <cocosbuilder/CocosBuilder.h>
#include <SimpleAudioEngine.h>
#include <sys/time.h>
#include "LabelExtension.h"
#include "TableViewExtension.h"
#include "ResourceManager.h"
#include "Define.h"
#include "ToastLayer.h"
#include "Util.h"
#include "Character.h"

#include "external/json/document.h"
#include "external/json/prettywriter.h"
#include "external/json/stringbuffer.h"

#include "guide/GuideHome.h"
#include "TSDoleNetProgressLayer.h"
#include "CharacterRecordingScene.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include <iterator>
#include <time.h>
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#include "CocosVideoRecoderWrapper.h"
#include "QRCodeReaderViewController.h"
#include "PromotionViewController.h"

#ifndef MACOS
#include "TSAccountViewController.h"
#include "TSDoleCoinViewController.h"
#include "TSSettingViewController.h"
#include "FacebookFriendListViewController.h"
#include "TSPopupViewController.h"
#include "AppController.h"

#import "UIViewController+ChangeRootView.h"
#import "TSAccountLoginViewController.h"
#import "TSDoleCoinInfoViewController.h"
#import "TSSettingMainViewController.h"

#endif
#endif

#include "BGMManager.h"

//#define _ACCOUNT_ACTIVE_
//#define TESTMENU

#define MENU_BG                 0
#define MENU_ITEM               1

#define POPUP_TEST              0
#define DELETE_BOOK_POPUP       1
#define DELETE_BTN_TAG          1010

#define TAG_FRAME_BG            300
#define TAG_BG_SELECT_COIN      400

#define MAX_BOOK_ROTATION       5

#define TAG_BOOK_SIDE_CHARACTER_1       500
#define TAG_BOOK_SIDE_CHARACTER_2       600

#define TAG_EXPANDED_ITEM_COIN          700
#define TAG_EXPANDED_ITEM_TALKBOX       800
#define TAG_EXPANDED_ITEM_PRODUCT       900

#define TAG_BTN_DOLECOIN_COIN           400001
#define TAG_BTN_DOLECOIN_X              400002
#define TAG_BTN_DOLECOIN_UNKNOWN        400003
#define TAG_BTN_DOLECOIN_COUNT          400004

#define TAG_BTN_SELECT_BACKGROUND       500001

#define NEW_BOOK_KEY                        "new_book_key"
#define BOOK_SCROLL_VIEW                    1
#define TAG_BACKGROUND_SCROLL_VIEW          2
#define TAG_CHARACTER_SCROLL_VIEW           3

#define TAG_SIDE_CHARACTER_ANIMATION        10

#define FRAME_BASE_TAG                      10000
#define FRAME_ANCHOR_Y                      0.95

#define BOOK_CUSTOM_NUM_KEY "book_custom_num_key"
#define TAG_BOOK_TITLE_LABEL 10
#define TAG_BOOK_TITLE_SUBLABEL 11
#define TAG_BOOK_AUTHOR_LABEL 12
#define BOOK_BODY           20

#define TAG_DELETE_ITEM_SHADOW 0
#define TAG_DELETE_ITEM_LABEL 1

#define TAG_POPUP_ERRORCOIN         9870001
#define TAG_POPUP_NEEDLOGIN         9870002
#define TAG_POPUP_COINLOGIN         9870003
#define TAG_POPUP_UNLOCKITEM        9870004
#define TAG_POPUP_CRITICAL_UPDATE   9870005
#define TAG_POPUP_NONE   			9870007

//#define TAG_POPUP_PROGRESS          9870008

#define TAG_NET_RENEWAUTH           8760001
#define TAG_NET_GETBALANCE          8760002
#define TAG_NET_PURCHASE            8760003
#define TAG_NET_LAST_VERSION        8760004
#define TAG_NET_NOTICELIST          8760005
#define TAG_NET_CACHEVERSION        8760006
#define TAG_NET_INVENTORY           8760007

#define TAG_BOOK_PICTURE 777
#define TAG_HOME_TEXTURE 151
#define TAG_SPLASH 161
#define TAG_SPLASH_LOGO 100011

#define TAG_ITEM_TALKBOX_LABEL 54321

#define PURCHASE_START_WITH_LOGIN   0x0001
#define PURCHASE_LOGIN_READY        0x0010
#define PURCHASE_GETBALANSE_READY   0x0100
#define PURCHASE_INVENTORY_READY    0x1000

#define PRODUCT_TYPE_NIL -1
#define PRODUCT_TYPE_CHARACTER 0
#define PRODUCT_TYPE_BACKGROUND 1

const double PI = std::acos(-1);

using namespace rapidjson;

Scene* Home::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    scene->setTag(TAG_HOME_SCENE);
    // 'layer' is an autorelease object
    auto layer = Home::create();
    layer->setTag(TAG_HOME);

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool Home::init()
{
    if ( !LayerColor::initWithColor( Color4B(0xba,0xea,0xed,0xff)) )
    {
        return false;
    }

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    _protocolReNewAuthKey = nullptr;
    _protocolGetBalance = nullptr;
    _protocolCacheVersion = nullptr;
    _protocolInventoryList = nullptr;
    _protocolNoticeList = nullptr;
    _protocolAppVersion = nullptr;
    _protocolExecutePurchase = nullptr;
#endif

    
//#if COCOS2D_DEBUG
//    {//외부 릴리즈시에는 절대 포함되면 안되는 코드.
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_AUTHKEY];
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_INTEGER_USERNO];
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_EMAIL];
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_LOGINID];
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_LOGINPW];
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_BIRTHDAY];
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_CITY];
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_COUNTRY];
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_INTEGER_GENDER];
//        
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_BOOL_FACEBOOKLOGIN];
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_SNSID];
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_SNSNAME];
//        
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_INTEGER_BALANCE];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//    }
//#endif
    _seletedProductName = "";
    _selectedProductType = PRODUCT_TYPE_NIL;
    _productList = nullptr;
    _doleCoinBalance = 0;
    _charecLayer = nullptr;
    _isPurchasReady = 0;
    
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    
    bgItemClicked = false;
    movingScene = false;
    
    reachedToEnd = false;
    
    scrollViewToggleAnimation = false;
    isAddingBookAnimation =false;
    
    bgLayer = LayerColor::create(Color4B(0xba,0xea,0xed,0xff), visibleSize.width, visibleSize.height+HOME_MAIN_LAYER_HIDE_HEIGHT);
    this->addChild(bgLayer);
    
    mainLayer = Layer::create();
    mainLayer->setPosition(cocos2d::Point::ZERO);
    mainLayer->setContentSize(cocos2d::Size(visibleSize.width, visibleSize.height+HOME_MAIN_LAYER_HIDE_HEIGHT));
    bgLayer->addChild(mainLayer,3);
    
    this->initCloud(visibleSize);
    
    cocosbuilder::NodeLoaderLibrary *nodeLoaderLibrary = cocosbuilder::NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
    cocosbuilder::CCBReader *ccbReader = new cocosbuilder::CCBReader(nodeLoaderLibrary);
    Node *ccbiObj = ccbReader->readNodeGraphFromFile("splash.ccbi", this);
    ccbiObj->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    ccbiObj->setPosition(cocos2d::Point::ZERO);
    bgLayer->addChild(ccbiObj,5, TAG_SPLASH);
    
    auto animationManager = ccbReader->getAnimationManager();
    animationManager->setDelegate(this);
//    animationManager->runAnimationsForSequenceNamed("Default Timeline");
    
    ccbReader->release();
    
    __NotificationCenter::getInstance()->addObserver(this, SEL_CallFuncO(&Home::deleteCharItemInRecordScene), "deleteCharItemInRecordScene", NULL);
    __NotificationCenter::getInstance()->addObserver(this, SEL_CallFuncO(&Home::deleteBgItemInRecordScene), "deleteBgItemInRecordScene", NULL);
    
    // dole logo
    auto logo = Sprite::create("logo.png");
    logo->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    logo->setPosition(HOME_LOGO);
    
    
    auto divider = Sprite::create("divider_home.png");
    divider->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    divider->setPosition(HOME_LOGO_DIVIDER);
    mainLayer->addChild(divider, 100);
    
    auto logoDole = Sprite::create("logo_dole.png");
    logoDole->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    logoDole->setPosition(HOME_LOGO_DOLE);
    mainLayer->addChild(logoDole, 100);
    
    this->initMenu(visibleSize);
    this->initItems(visibleSize);
    this->initBookScrollView(visibleSize);
    
    cocos2d::Size size = Director::getInstance()->getWinSize();
    
    auto homeTexture = RenderTexture::create((int)size.width, (int)size.height);
    
    if (!(nullptr == homeTexture))
    {
        homeTexture->getSprite()->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        homeTexture->setPosition(cocos2d::Point::ZERO);
        homeTexture->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        
        homeTexture->begin();
        mainLayer->visit();
        homeTexture->end();
        
        BlendFunc blend2 = {GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA};
        homeTexture->getSprite()->setBlendFunc(blend2);
        homeTexture->getSprite()->setOpacity(0);
        
        bgLayer->addChild(homeTexture, 3, TAG_HOME_TEXTURE);
    }
    
    mainLayer->setVisible(false);
    
    mainLayer->addChild(logo, 100);
    
    scrollOffset = cocos2d::Point::ZERO;
    
    backgroundScrollView = BackgroundScrollView::create(BG_HOME);
    backgroundScrollView->setPosition(HOME_BACKGROUND_SCROLL_VIEW);
    backgroundScrollView->setDelegate(this);
    backgroundScrollView->setVisible(false);
    mainLayer->addChild(backgroundScrollView, 1);
    
    characterScrollView = CharacterScrollView::create(CHAR_HOME);
    characterScrollView->setPosition(HOME_BACKGROUND_SCROLL_VIEW);
    characterScrollView->setDelegate(this);
    characterScrollView->setVisible(false);
    mainLayer->addChild(characterScrollView, 1);
    
    isBookAnimation = false;
    isScrollByAdd = false;
    isPlatformShown = false;
    isCharacterSelected = false;
    
    float volume = UserDefault::getInstance()->getFloatForKey(UDKEY_FLOAT_VOLUME, DEFAULT_VOLUME_BACKGROUND_SOUND);
    CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(volume);
    CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(DEFAULT_VOLUME_BACKGROUND_SOUND);
    
    //    DoleAPI* api = DoleAPI::getInstance();
    //    api->mobileSignUp("jmjeong", "test", "jmjeong@gmail.com", "1990-12-14", 1, "english", "SNS001", "jmjeong@gmail.com", "jmjeong", "KR", "1234-1234-1234-1234", "Seoul", "192.168.1.1", nullptr, nullptr);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    preloadSoundEffects();
	AppDelegate* app = (AppDelegate*)Application::getInstance();
	if(app != nullptr) {
		app->removeLogo();
	}
#endif
    
    this->scheduleOnce(SEL_SCHEDULE(&Home::fadeInAfterSplash), 2.5f);
    
    this->doleGetProductProtocol();
    
    return true;
}

void Home::reloadScrollViews()
{
    this->characterScrollView->reloadData();
    this->backgroundScrollView->reloadData();
}

void Home::fadeInAfterSplash()
{
    TSBGMManager::getInstance()->play();
    auto homeTexture = (RenderTexture*)bgLayer->getChildByTag(TAG_HOME_TEXTURE);
    
    auto afterSplashCallback = CallFunc::create([=](){
        if (homeTexture) {
            homeTexture->removeFromParentAndCleanup(true);
        }
        mainLayer->setVisible(true);
        this->DoleAccountInitialized();
        auto splashBgLogo = (Sprite*)bgLayer->getChildByTag(TAG_SPLASH_LOGO);
        if (splashBgLogo) {
            splashBgLogo->removeFromParentAndCleanup(true);
        }
        
        if (UserDefault::getInstance()->getBoolForKey("showguide_home", false) == true) {
            this->showPromotionPopup();
            this->savePromotionFirstInitDate();
        }
        
#ifdef _DEBUG_GUIDE_
        GuideHome::create(this);
#endif
    });
                                                 
    auto layerAction = Sequence::create
    (
        FadeTo::create(0.7f, 255.0f),
        afterSplashCallback,
        NULL
     );
    
    homeTexture->getSprite()->runAction(layerAction);
}

void Home::completedAnimationSequenceNamed(const char *name)
{
//    log("name %s",name);

    if (strcmp(name, "splash_default") == 0) {
        auto splash = (Node*)bgLayer->getChildByTag(TAG_SPLASH);
        splash->removeFromParentAndCleanup(true);
        
        auto logo = Sprite::create("logo.png");
        logo->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        logo->setPosition(HOME_LOGO);
        logo->setTag(TAG_SPLASH_LOGO);
        bgLayer->addChild(logo);
    }
}

void Home::savePromotionFirstInitDate()
{
    time_t currentT;
    time(&currentT);
    struct tm *currentTm = localtime(&currentT);
    std::string currentTmStr(asctime(currentTm));
    log("current date : %s", currentTmStr.c_str());
    
    UserDefault::getInstance()->setStringForKey(UDKEY_STRING_PROMOTION_TM, currentTmStr);
}

void Home::showGuide() {
    GuideHome::create(this);
    this->showPromotionPopup();
    this->savePromotionFirstInitDate();
}

void Home::preloadSoundEffects() {
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("Tap.wav");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("Move.wav");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("item_01_tap.wav");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("item_02_drag.wav");
}

std::string Home::addNewBook()
{    
    auto ud = UserDefault::getInstance();
    auto orderJsonString = ud->getStringForKey(ORDER_KEY, "");
    rapidjson::Document orderDocument;
    Document::AllocatorType& allocator = orderDocument.GetAllocator();
    orderDocument.Parse<0>(orderJsonString.c_str());
    rapidjson::Value& bookOrder = orderDocument["book_order"];
    
    auto userDefault = UserDefault::getInstance();
    
    auto  lastBookNum = userDefault->getIntegerForKey(BOOK_CUSTOM_NUM_KEY, -1);
    unsigned long int bookNum = lastBookNum + 1;
    userDefault->setIntegerForKey(BOOK_CUSTOM_NUM_KEY, (int)bookNum);
    
    auto createdBookKey = __String::createWithFormat("my_story_%ld", bookNum);
    auto createdTitle = __String::createWithFormat(Device::getResString(Res::Strings::CC_JOURNEYMAP_BOOKTITLE_DEFAULT), bookNum+1);//"My story %ld"
    auto authorName = Device::getResString(Res::Strings::CC_JOURNEYMAP_BOOKAUTHOR_DEFAULT);//"Author Name";
//    auto spriteName = __String::createWithFormat("home_book_story_%02d.png", rand()%13+1);
//    auto imageName = __String::createWithFormat("home_book_story_bg_%02d.png", rand()%13+1);
    auto spriteName = __String::createWithFormat("home_book_story_%02d.png", 1);
    auto imageName = __String::createWithFormat("home_book_story_bg_%02d.png", 1);
    
    // add to user order
    rapidjson::Value bookKey;
    bookKey.SetString(createdBookKey->getCString());
    bookOrder.PushBack(bookKey, allocator);
    
    rapidjson::StringBuffer strbuf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
    orderDocument.Accept(writer);
    auto resultOrderJsonString = strbuf.GetString();
    
    // save to user default
    ud->setStringForKey(ORDER_KEY, resultOrderJsonString);

    rapidjson::Document bookData;
    bookData.SetObject();
    bookData.AddMember("title", createdTitle->getCString(), bookData.GetAllocator());
    bookData.AddMember("author", authorName, bookData.GetAllocator());
    bookData.AddMember("background", spriteName->getCString(), bookData.GetAllocator());
    bookData.AddMember("background_image", imageName->getCString(), bookData.GetAllocator());
    bookData.AddMember("editable", true, bookData.GetAllocator());
    bookData.AddMember("default_title", true, bookData.GetAllocator());
    bookData.AddMember("default_author", true, bookData.GetAllocator());
    
    rapidjson::Value fontColor(rapidjson::kArrayType);
    
    if (strcmp(spriteName->getCString(), "home_book_story_01.png") == 0) {
        fontColor.PushBack(0xff, bookData.GetAllocator());
        fontColor.PushBack(0x84, bookData.GetAllocator());
        fontColor.PushBack(0x00, bookData.GetAllocator());
    } else if (strcmp(spriteName->getCString(), "home_book_story_02.png") == 0 || strcmp(spriteName->getCString(), "home_book_story_08.png") == 0) {
        fontColor.PushBack(0x9f, bookData.GetAllocator());
        fontColor.PushBack(0xd3, bookData.GetAllocator());
        fontColor.PushBack(0x13, bookData.GetAllocator());
    } else if (strcmp(spriteName->getCString(), "home_book_story_03.png") == 0 || strcmp(spriteName->getCString(), "home_book_story_06.png") == 0) {
        fontColor.PushBack(0x31, bookData.GetAllocator());
        fontColor.PushBack(0x92, bookData.GetAllocator());
        fontColor.PushBack(0xe4, bookData.GetAllocator());
    } else if (strcmp(spriteName->getCString(), "home_book_story_04.png") == 0 || strcmp(spriteName->getCString(), "home_book_story_09.png") == 0) {
        fontColor.PushBack(0xff, bookData.GetAllocator());
        fontColor.PushBack(0xd2, bookData.GetAllocator());
        fontColor.PushBack(0x00, bookData.GetAllocator());
    } else if (strcmp(spriteName->getCString(), "home_book_story_05.png") == 0) {
        fontColor.PushBack(0x28, bookData.GetAllocator());
        fontColor.PushBack(0xd2, bookData.GetAllocator());
        fontColor.PushBack(0xb8, bookData.GetAllocator());
    } else if (strcmp(spriteName->getCString(), "home_book_story_07.png") == 0) {
        fontColor.PushBack(0xff, bookData.GetAllocator());
        fontColor.PushBack(0x84, bookData.GetAllocator());
        fontColor.PushBack(0x00, bookData.GetAllocator());
    } else if (strcmp(spriteName->getCString(), "home_book_story_10.png") == 0) {
        fontColor.PushBack(0xff, bookData.GetAllocator());
        fontColor.PushBack(0xd2, bookData.GetAllocator());
        fontColor.PushBack(0x00, bookData.GetAllocator());
    } else if (strcmp(spriteName->getCString(), "home_book_story_11.png") == 0) {
        fontColor.PushBack(0x80, bookData.GetAllocator());
        fontColor.PushBack(0x4f, bookData.GetAllocator());
        fontColor.PushBack(0xca, bookData.GetAllocator());
    } else if (strcmp(spriteName->getCString(), "home_book_story_12.png") == 0) {
        fontColor.PushBack(0xe4, bookData.GetAllocator());
        fontColor.PushBack(0x7b, bookData.GetAllocator());
        fontColor.PushBack(0x29, bookData.GetAllocator());
    } else if (strcmp(spriteName->getCString(), "home_book_story_13.png") == 0) {
        fontColor.PushBack(0xff, bookData.GetAllocator());
        fontColor.PushBack(0xd2, bookData.GetAllocator());
        fontColor.PushBack(0x00, bookData.GetAllocator());
    } else if (strcmp(spriteName->getCString(), "home_book_story_14.png") == 0) {
        fontColor.PushBack(0x30, bookData.GetAllocator());
        fontColor.PushBack(0xcf, bookData.GetAllocator());
        fontColor.PushBack(0xb5, bookData.GetAllocator());
    } else if (strcmp(spriteName->getCString(), "home_book_story_15.png") == 0) {
        fontColor.PushBack(0x9f, bookData.GetAllocator());
        fontColor.PushBack(0xd3, bookData.GetAllocator());
        fontColor.PushBack(0x13, bookData.GetAllocator());
    } else {
        fontColor.PushBack(0x28, bookData.GetAllocator());
        fontColor.PushBack(0xd2, bookData.GetAllocator());
        fontColor.PushBack(0xb8, bookData.GetAllocator());
    }
    
    bookData.AddMember("font_color", fontColor, bookData.GetAllocator());
    
    rapidjson::Value chapters(rapidjson::kObjectType);
    rapidjson::Value nullValue(rapidjson::kNullType);
    chapters.AddMember("chapter1", nullValue, bookData.GetAllocator());
    chapters.AddMember("chapter2", nullValue, bookData.GetAllocator());
    chapters.AddMember("chapter3", nullValue, bookData.GetAllocator());
    chapters.AddMember("chapter4", nullValue, bookData.GetAllocator());
    chapters.AddMember("chapter5", nullValue, bookData.GetAllocator());
    
    bookData.AddMember("chapters", chapters, bookData.GetAllocator());

    rapidjson::StringBuffer bookStrbuf;
    rapidjson::Writer<rapidjson::StringBuffer> writer2(bookStrbuf);
    bookData.Accept(writer2);
    auto resultbookJsonString = bookStrbuf.GetString();

//    __String* bookJsonString = __String::createWithFormat("{\"title\" : \"%s\", \"author\" : \"%s\", \"background\" : \"%s\", \"background_image\" : \"%s\", \"font_color\" : [255,235,9], \"editable\": true, \"chapters\" : {\"chapter1\" : null, \"chapter2\" : null, \"chapter3\" : null, \"chapter4\" : null, \"chapter5\" : null} }",
//                                                     createdTitle->getCString(), authorName, spriteName->getCString(), imageName->getCString());
//    log("bookJsonString : %s", bookJsonString->getCString());
    
    ud->setStringForKey(createdBookKey->getCString(), resultbookJsonString);
    ud->flush();
    
    movingScene = false;
    
    std::string createdBookKeyString(createdBookKey->getCString());
    
    return createdBookKeyString;
}

void Home::runAddNewBookAnimation(__String* bookKey)
{
    isAddingBookAnimation = true;
    
    auto ud = UserDefault::getInstance();
    auto bookJsonString = ud->getStringForKey(bookKey->getCString());
    rapidjson::Document bookDocument;
    bookDocument.Parse<0>(bookJsonString.c_str());
    
    rapidjson::Value& chapters = bookDocument["chapters"];
    
    auto isCustom = false;
    rapidjson::Document backgroundInfo;
    for (int i = 1; i < 6; i++) {
        __String *chapterName = __String::createWithFormat("chapter%d", i);
        rapidjson::Value& chapterData = chapters[chapterName->getCString()];
        if (!chapterData.IsNull() && chapterData.HasMember("char_datas")) {
            auto backgroundKey = chapterData["background"].GetString();
            
            auto backgroundString = ud->getStringForKey(backgroundKey);
            backgroundInfo.Parse<0>(backgroundString.c_str());
            isCustom = backgroundInfo["custom"].GetBool();
            
//            if (isCustom) {
//                auto spriteName = __String::createWithFormat("home_create_book_story_%02d.png", rand()%3+1);
//                bookDocument["background"].SetString(spriteName->getCString());
//                
//                rapidjson::Value fontColor(rapidjson::kArrayType);
//                if (strcmp(spriteName->getCString(), "home_create_book_story_01.png") == 0) {
//                    fontColor.PushBack(0x9f, bookDocument.GetAllocator());
//                    fontColor.PushBack(0xd3, bookDocument.GetAllocator());
//                    fontColor.PushBack(0x13, bookDocument.GetAllocator());
//                } else if (strcmp(spriteName->getCString(), "home_create_book_story_02.png") == 0) {
//                    fontColor.PushBack(0x31, bookDocument.GetAllocator());
//                    fontColor.PushBack(0x92, bookDocument.GetAllocator());
//                    fontColor.PushBack(0xe4, bookDocument.GetAllocator());
//                } else if (strcmp(spriteName->getCString(), "home_create_book_story_03.png") == 0) {
//                    fontColor.PushBack(0xff, bookDocument.GetAllocator());
//                    fontColor.PushBack(0xd2, bookDocument.GetAllocator());
//                    fontColor.PushBack(0x00, bookDocument.GetAllocator());
//                } else if (strcmp(spriteName->getCString(), "home_create_book_story_04.png") == 0) {
//                    fontColor.PushBack(0x28, bookDocument.GetAllocator());
//                    fontColor.PushBack(0xd2, bookDocument.GetAllocator());
//                    fontColor.PushBack(0xb8, bookDocument.GetAllocator());
//                }
//                bookDocument["font_color"] = fontColor;
//                
//                rapidjson::StringBuffer strbuf;
//                rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
//                bookDocument.Accept(writer);
//                auto jsonString = strbuf.GetString();
//                auto ud = UserDefault::getInstance();
//                ud->setStringForKey(bookKey->getCString(), jsonString);
//                ud->flush();
//                
//                bookJsonString = ud->getStringForKey(bookKey->getCString());
//                bookDocument.Parse<0>(bookJsonString.c_str());
//            }
            
            this->saveNewBookBgtoJson(bookDocument, bookKey->getCString(), isCustom, backgroundKey);
            
            break;
        }
    }

    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    cocos2d::Point scrollOffset = bookScrollView->getContentOffset();
    auto moveByPoint = cocos2d::Point(HOME_BOOK_SIZE.width+HOME_BOOK_PADDING, 0);
    
    float moveDuration = 0.25f;
    
    this->bookScrollView->setTouchEnabled(false);
    this->bookScrollView->setContentOffsetInDuration(cocos2d::Point(scrollOffset.x - moveByPoint.x, scrollOffset.y), moveDuration);
    
    auto newBookLayer = (Layer *)booksSpriteDic->objectForKey(NEW_BOOK_KEY);
    auto moveAction = MoveBy::create(moveDuration, moveByPoint);
    auto newBookCallback = CallFunc::create([&](){
        this->bookScrollView->setTouchEnabled(true);
    });
    auto newBookAnimationSequence = Sequence::create(moveAction, newBookCallback, NULL);
    newBookLayer->runAction(newBookAnimationSequence);
    
    isScrollByAdd = true;
    
    // TODO::how to determine background?
    auto bookBg = Sprite::create(bookDocument["background"].GetString());
    bookBg->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    bookBg->setPosition(HOME_BOOK_BG);
    
    if (isCustom) {
        auto bgPhoto = (char *)backgroundInfo["main_photo_file"].GetString();
        auto bgPhotoImage = __String::create(bgPhoto);
        if (bgPhotoImage != nullptr) {
            std::string photoType("bg");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			std::string path = std::string(bgPhotoImage->getCString() + Util::getBookCoverPhotoPath(photoType));
#else
            std::string path = Util::getBookThumbnailPhotoPath(photoType);
            path.append(bgPhotoImage->getCString());
#endif
            if (cocos2d::FileUtils::getInstance()->isFileExist(path) ){
                cocos2d::Data imageData = cocos2d::FileUtils::getInstance()->getDataFromFile(path.c_str());
                unsigned long nSize = imageData.getSize();
                
                cocos2d::Image *image = new cocos2d::Image();
                image->initWithImageData(imageData.getBytes(), nSize);
                
                cocos2d::Texture2D *texture = new cocos2d::Texture2D();
                texture->initWithImage(image);
                
                auto picture = Sprite::createWithTexture(texture);
                picture->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                picture->setPosition(HOME_CUSTOM_BG_BOOK_THUMB);
                picture->setTag(TAG_BOOK_PICTURE);
                picture->setScale(HOME_CUSTOM_BG_WIDTH_INBOOK/picture->getContentSize().width);
                bookBg->addChild(picture);
            } else {
                log("error! no file : %s", path.c_str());
            }
        }
    }
    
//    auto bookBgImage = Sprite::create(bookDocument["background_image"].GetString());
//    bookBgImage->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//    bookBgImage->setPosition(cocos2d::Point::ZERO);
//    bookBg->addChild(bookBgImage);
//    
//    auto bookCover = Sprite::create("home_book_story_cover.png");
//    bookCover->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//    bookCover->setPosition(cocos2d::Point::ZERO);
//    bookCover->setContentSize(bookBg->getContentSize());
//    bookBg->addChild(bookCover);
    
    auto createdBook = Sprite::create();
    createdBook->setContentSize(HOME_BOOK_LAYER_SIZE);
    createdBook->setAnchorPoint( cocos2d::Point::ANCHOR_MIDDLE);
    createdBook->setPosition( cocos2d::Point(-visibleSize.width, -visibleSize.height) );
    createdBook->setScale(0.5f, 0.5f);
    
    auto bookShadow = Sprite::create("home_book_story_shadow.png");
    bookShadow->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    bookShadow->setPosition(cocos2d::Point::ZERO);
    createdBook->addChild(bookShadow);
    createdBook->addChild(bookBg, 2, BOOK_BODY);

//    auto deleteBtn = MenuItemImage::create("btn_home_book_story_delete_normal.png","btn_home_book_story_delete_press.png");
//    deleteBtn->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
//    deleteBtn->setPosition(cocos2d::Point(-visibleSize.width, -visibleSize.height));
//    deleteBtn->setScale(0.5f);

    auto deleteBtn = cocos2d::ui::Button::create("btn_home_book_story_delete_normal.png", "btn_home_book_story_delete_press.png");
    deleteBtn->addTouchEventListener(this, (cocos2d::ui::SEL_TouchEvent)(&Home::deleteButtonClicked));
    deleteBtn->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
    deleteBtn->setPosition(HOME_DELETE_BTN_TYPE01);
    
    bookScrollView->addChild(createdBook, TOUCHABLE_LAYER);
    
    std::string bookTitle = bookDocument["title"].GetString();
    std::string author = bookDocument["author"].GetString();
    
    __String *bookKeyClone = bookKey->clone();
    bookKeyClone->retain();
    
    createdBook->setUserData(bookKeyClone);
    deleteBtn->setUserData(bookKeyClone);
    booksKeyArray->insertObject(bookKeyClone, booksKeyArray->count()-1);
    booksSpriteDic->setObject(createdBook, bookKeyClone->getCString());
    
    rapidjson::Value& titleColor = bookDocument["font_color"];
    
    cocos2d::Size titleSize;
    cocos2d::Point linePosition;
    if (bookDocument["editable"].GetBool() == true) {
        titleSize = HOME_BOOK_TITLE_SIZE;
        linePosition = HOME_BOOK_TITLE;
    } else {
        titleSize = HOME_PRE_BOOK_TITLE_SIZE;
        linePosition = HOME_PRE_BOOK_TITLE;
    }
    
    // label
    auto bookLabelTitle = bookTitle.c_str();

    std::string bookTitleMain, bookTitleSub;
    Util::splitTitleString(bookLabelTitle, ' ', bookTitleMain, bookTitleSub, titleSize.width, FONT_NAME_BOLD, HOME_BOOK_TITLE_FONTSIZE);
    
    auto bookLabel = LabelExtension::create(bookTitleMain, FONT_NAME_BOLD, HOME_BOOK_TITLE_FONTSIZE, titleSize, TextHAlignment::CENTER, TextVAlignment::CENTER);
    if(bookLabel->getContentSize().width < HOME_BOOK_TITLE_SIZE.width) bookLabel->setWidth(HOME_BOOK_TITLE_SIZE.width);
    bookLabel->sizeToFitWidth(HOME_BOOK_TITLE_SIZE.width);
    bookLabel->setAnchorPoint( cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    if(strcmp(bookTitleSub.c_str(), "") == 0) {
        bookLabel->setPosition(HOME_BOOK_TITLE_A_LINE);
    } else {
        bookLabel->setPosition(linePosition);
    }
    
//    bookLabel->setColor(Color3B(255,235,9));
    bookLabel->setColor(Color3B(titleColor[SizeType(0)].GetInt(),
                                titleColor[SizeType(1)].GetInt(),
                                titleColor[SizeType(2)].GetInt()));
    bookLabel->setTag(TAG_BOOK_TITLE_LABEL);
    bookBg->addChild(bookLabel, 2);
    bookLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    
    bookLabel->enableShadow(Color4B(0,0,0,40.8f), LABEL_SHADOW_DEFAULT_SIZE, 0);
    
    auto bookSubLabel = LabelExtension::create(bookTitleSub, FONT_NAME_BOLD, HOME_BOOK_TITLE_FONTSIZE, HOME_BOOK_TITLE_SIZE, TextHAlignment::CENTER, TextVAlignment::CENTER);
    bookSubLabel->ellipsis(FONT_NAME_BOLD,HOME_BOOK_TITLE_FONTSIZE);
    bookSubLabel->setAnchorPoint( cocos2d::Point(0,0));
    bookSubLabel->setPosition(HOME_BOOK_TITLE_SUB);
//    bookSubLabel->setColor(Color3B(255,235,9));
    bookSubLabel->setColor(Color3B(titleColor[SizeType(0)].GetInt(),
                                titleColor[SizeType(1)].GetInt(),
                                titleColor[SizeType(2)].GetInt()));
    bookSubLabel->setTag(TAG_BOOK_TITLE_SUBLABEL);
    bookSubLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    bookBg->addChild(bookSubLabel, 2);
    bookSubLabel->enableShadow(Color4B(0,0,0,40.8f), LABEL_SHADOW_DEFAULT_SIZE, 0);
    
    auto authorString = author.c_str();
    
    auto bookAuthor = LabelExtension::create(authorString , FONT_NAME_BOLD, HOME_BOOK_AUTHOR_FONTSIZE, HOME_BOOK_AUTHOR_SIZE, TextHAlignment::CENTER, TextVAlignment::CENTER);
    bookAuthor->ellipsis(FONT_NAME_BOLD, HOME_BOOK_AUTHOR_FONTSIZE);
    bookAuthor->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    bookAuthor->setPosition(HOME_BOOK_AUTHOR);
//    bookAuthor->setColor(Color3B(0xff,0xeb,0x09));
    bookAuthor->setColor(Color3B(titleColor[SizeType(0)].GetInt(),
                                titleColor[SizeType(1)].GetInt(),
                                titleColor[SizeType(2)].GetInt()));
    bookAuthor->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    bookAuthor->enableShadow(Color4B(0,0,0,40.8f), LABEL_SHADOW_AUTHOR_SIZE, 0);
    bookAuthor->setTag(TAG_BOOK_AUTHOR_LABEL);
    bookBg->addChild(bookAuthor,5);
    
    auto currentScrollViewContentsSize = bookScrollView->getContentSize();
    auto newContensSize = cocos2d::Size(currentScrollViewContentsSize.width + HOME_BOOK_SIZE.width + HOME_BOOK_PADDING, currentScrollViewContentsSize.height);
    bookScrollView->setContentSize(newContensSize);
    
    // scale animation
    auto startDelay = DelayTime::create(moveDuration);
    auto placeTo = Place::create( cocos2d::Point(currentScrollViewContentsSize.width - HOME_BOOK_PADDING - HOME_NEW_BOOK_LAYER_SIZE.width + createdBook->getContentSize().width/2,
                                                 HOME_BOOK_START_Y+createdBook->getContentSize().height/2));
    
    auto scaleTo = ScaleTo::create(moveDuration, 1.0f);
    
    auto startDelay2 = DelayTime::create(moveDuration+0.5f);
    
//    auto placeTo2 = Place::create( cocos2d::Point(currentScrollViewContentsSize.width - HOME_BOOK_PADDING - HOME_NEW_BOOK_LAYER_SIZE.width + HOME_DELETE_BTN_TYPE01.x,
//                                                  HOME_DELETE_BTN_TYPE01.y));
    
    auto scaleTo2 = ScaleTo::create(moveDuration, 1.0f);
    
    deleteBtn->setScale(0.0f);
    bookBg->addChild(deleteBtn, TOUCHABLE_LAYER, DELETE_BTN_TAG);
    
    auto addCallback = CallFunc::create([=](){
        if( selectedBookKey != nullptr) {
            selectedBookKey = nullptr;
        }
        
        this->showBookSideCharacter(bookScrollView->getContentOffset().x);
        
        isBookAnimation = false;
        isAddingBookAnimation = false;
    });
    auto appearAction = Sequence::create(startDelay, placeTo, scaleTo, addCallback, NULL);
    createdBook->runAction(appearAction);

    auto appearAction2 = Sequence::create(startDelay2,
//                                          placeTo2,
                                          scaleTo2, NULL);
    deleteBtn->runAction(appearAction2);
    
    movingScene = false;
}

void Home::removeBook(__String* bookKey)
{
    // TODO: remove timeline datas and voice files
    auto bookToBeDeleteKey = bookKey;

    auto ud = UserDefault::getInstance();
    ud->setStringForKey(bookToBeDeleteKey->getCString(), "");
    
    auto orderJsonString = ud->getStringForKey(ORDER_KEY);
    rapidjson::Document orderDocument;
    orderDocument.Parse<0>(orderJsonString.c_str());
    rapidjson::Value& bookOrder = orderDocument["book_order"];
    rapidjson::Value newOrder(rapidjson::kArrayType);
    Document::AllocatorType& allocator = orderDocument.GetAllocator();
    
    for (SizeType i = 0; i < bookOrder.Size(); i++) {
        auto bookKey = bookOrder[i].GetString();
        if (bookToBeDeleteKey->compare(bookKey) == 0) {
            continue; // pass
        }
        newOrder.PushBack(bookKey, allocator);
    }
    orderDocument["book_order"] = newOrder;
    rapidjson::StringBuffer strbuf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
    orderDocument.Accept(writer);
    auto resultOrderJsonString = strbuf.GetString();
    ud->setStringForKey(ORDER_KEY, resultOrderJsonString);
    ud->flush();
}

void Home::runRemoveBookAnimation(__String* bookKey)
{
    auto bookIndex = booksKeyArray->getIndexOfObject(bookKey);
    cocos2d::log("deleted book index = %ld", bookIndex);
    if (bookIndex == CC_INVALID_INDEX) {
        // not found
    } else {
        this->removeBook(bookKey);

        cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
        Sprite *bookToBeDelete = (Sprite*)booksSpriteDic->objectForKey(bookKey->getCString());
        
        float moveDuration = 0.25f;
        auto scaleTo = ScaleTo::create(moveDuration, 0.5f);
        auto placeTo = Place::create( cocos2d::Point(-visibleSize.width*2, -visibleSize.height*2));
        auto removeCallback = CallFunc::create([&, visibleSize, bookIndex, moveDuration](){

            auto bookKey = (__String *)booksKeyArray->getObjectAtIndex(bookIndex);
            booksSpriteDic->removeObjectForKey(bookKey->getCString());
            booksKeyArray->removeObjectAtIndex(bookIndex);
            
            cocos2d::Point scrollOffset = bookScrollView->getContentOffset();
            auto moveByPoint = cocos2d::Point(-(HOME_BOOK_SIZE.width+HOME_BOOK_PADDING), 0);
            this->bookScrollView->setTouchEnabled(false);
            this->bookScrollView->setContentOffsetInDuration(cocos2d::Point(scrollOffset.x - moveByPoint.x, scrollOffset.y), moveDuration);
            
            auto moveBy = MoveBy::create(moveDuration, moveByPoint);
            auto currentBookCount = booksKeyArray->count();
            
            int nextIndex = currentBookCount;
            bool processNearBook = false;
            cocos2d::log("deleted book index = %ld", bookIndex);
            for (int index = bookIndex; index < currentBookCount; index++) {
                auto bookKey = (__String *)booksKeyArray->getObjectAtIndex(index);
                Sprite *sprite = (Sprite*)booksSpriteDic->objectForKey(bookKey->getCString());
                auto boundingRect = sprite->getBoundingBox();
                auto originXInScreen = boundingRect.origin.x + scrollOffset.x;
                
                if (originXInScreen < visibleSize.width) { // if sprite is on screen
                    if (index == currentBookCount-1) { // last object
                        auto lastAnimationBookCallback = CallFunc::create([&, index](){
                            this->bookScrollView->setTouchEnabled(true);
                            auto currentContentsSize = this->bookScrollView->getContentSize();
                            this->bookScrollView->setContentSize( cocos2d::Size(currentContentsSize.width-(HOME_BOOK_SIZE.width+HOME_BOOK_PADDING), currentContentsSize.height ));
                            this->showBookSideCharacter(bookScrollView->getContentOffset().x);

                            //                            isBookAnimation = false;
                        });
                        auto lastBookAnimationSequence = Sequence::create(moveBy->clone(), lastAnimationBookCallback, NULL);
                        sprite->runAction(lastBookAnimationSequence);
                    } else {
                        sprite->runAction(moveBy->clone());
                    }
                } else {
                    if (!processNearBook) { // Will be moved in screen but now in out screen.
                        processNearBook = true;
                        auto lastAnimationBookCallback = CallFunc::create([&](){
                            this->bookScrollView->setTouchEnabled(true);
                            auto currentContentsSize = this->bookScrollView->getContentSize();
                            this->bookScrollView->setContentSize( cocos2d::Size(currentContentsSize.width-(HOME_BOOK_SIZE.width+HOME_BOOK_PADDING), currentContentsSize.height ));
                            this->showBookSideCharacter(bookScrollView->getContentOffset().x);

                            //                            isBookAnimation = false;
                        });
                        auto lastBookAnimationSequence = Sequence::create(moveBy->clone(), lastAnimationBookCallback, NULL);
                        sprite->runAction(lastBookAnimationSequence);
                    } else {
                        nextIndex = index;
                        break;
                    }
                }
                isBookAnimation = false;
            } //remove callback
            
            
            log("sprites at index(%ld ~ %d) run action", bookIndex+1, nextIndex-1);
            
            for (int index = nextIndex; index < currentBookCount; index++) {
                auto bookKey = (__String *)booksKeyArray->getObjectAtIndex(index);
                Sprite *sprite = (Sprite*)booksSpriteDic->objectForKey(bookKey->getCString());
                auto originPosition = sprite->getPosition();
                sprite->setPosition( cocos2d::Point(originPosition.x+moveByPoint.x, originPosition.y+moveByPoint.y));
            }
            log("sprites at index(%d ~ %ld) set position", nextIndex, currentBookCount);
        });
        
        auto disappearAction = Sequence::create(scaleTo, placeTo, removeCallback, NULL);
        bookToBeDelete->runAction(disappearAction);
    }
}

void Home::deleteButtonClicked(Ref *obj, ui::TouchEventType type)
{
    switch (type) {
        case ui::TOUCH_EVENT_ENDED: {
            log("delete btn");
            selDeleteBtn = ((ui::Button*)obj);
            selectedBookKey = (__String*)selDeleteBtn->getUserData();
            Director::getInstance()->pause();
            
            PopupLayer *modal;
            if (Application::getInstance()->getCurrentLanguage() != LanguageType::JAPANESE) {
                modal = PopupLayer::create(POPUP_TYPE_WARNING, Device::getResString(Res::Strings::CC_HOME_POPUP_DELETE_STORY),
                                           Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL),
                                           Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK));
            } else {
                modal = PopupLayer::create(POPUP_TYPE_WARNING, Device::getResString(Res::Strings::CC_HOME_POPUP_DELETE_STORY),
                                           Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_NO),
                                           Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_YES));
            }
            //"Selected storybook will be deleted. Deleted records can't be restored.", "Cancel", "OK"
            
            modal->setTag(DELETE_BOOK_POPUP);
            modal->setDelegate(this);
            this->addChild(modal, 1000);
        } break;
        case ui::TOUCH_EVENT_BEGAN: break;
        case ui::TOUCH_EVENT_MOVED: break;
        case ui::TOUCH_EVENT_CANCELED: break;
        default: break;
    }
}


//int Home::splitTitleString(const std::string &s, char delim, std::string &firstLine, std::string &secondLine) {
//    std::vector<std::string> elems;
//    std::stringstream ss(s);
//    std::string item;
//    while (std::getline(ss, item, delim)) {
//        elems.push_back(item);
//    }
//    
//    int maximumLength = 10;
//    
//    auto firstWord = elems.at(0);
//    if (elems.size() > 1) {
//        int startIndex = 1;
//        auto secondWord = elems.at(1);
//        if ( firstWord.length() + secondWord.length() < maximumLength) {
//            firstLine = firstWord + delim + secondWord;
//            startIndex++;
//        } else {
//            firstLine = firstWord;
//        }
//        const char* const delim = " ";
//        std::ostringstream imploded;
//        std::copy(elems.begin()+startIndex, elems.end(),
//                  std::ostream_iterator<std::string>(imploded, delim));
//        secondLine = imploded.str();
//    } else {
//        firstLine = firstWord;
//        secondLine = "";
//    }
//    return 0;
//}

Home::~Home()
{
    int size = booksKeyArray->count();
    for (int index = 0; index < size; index++) {
        auto sprite = (Sprite *)booksSpriteDic->objectForKey( (char*)booksKeyArray->getObjectAtIndex(index) );
        if (sprite) {
            sprite->removeFromParentAndCleanup(true);
        }
    }
    booksKeyArray->release();
    booksSpriteDic->removeAllObjects();
    booksSpriteDic->release();
}

void Home::initMenu(cocos2d::Size visibleSize)
{
    // btn_menu
    auto menuItem = MenuItemImage::create("btn_home_menu_normal.png",
                                          "btn_home_menu_press.png",
                                          CC_CALLBACK_1(Home::menuCallback, this));
    menuItem->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
	menuItem->setPosition(HOME_MENU_BTN);
    
    auto menu = Menu::create(menuItem, NULL);
    menu->setPosition(cocos2d::Point::ZERO);
    mainLayer->addChild(menu, 2);
    
    menuListLayer = MenuListLayer::create(LIST_TYPE_01);
    menuListLayer->setDelegate(this);
    this->addChild(menuListLayer);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS && defined(TESTMENU))
    auto photoMenu = MenuItemImage::create("btn_home_menu_normal.png",
                                           "btn_home_menu_press.png",
                                           CC_CALLBACK_1(Home::photoCallback, this));
    photoMenu->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
	photoMenu->setPosition(HOME_TEST_MENU_PHOTO);
    
    auto menu1 = Menu::create(photoMenu, NULL);
    menu1->setPosition(cocos2d::Point::ZERO);
    this->addChild(menu1, 2);
    
    auto facebookMenu = MenuItemImage::create("btn_home_menu_normal.png",
                                              "btn_home_menu_press.png",
                                           CC_CALLBACK_1(Home::facebookCallback, this));
    facebookMenu->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
	facebookMenu->setPosition(HOME_TEST_MENU_FACEBOOK);
    
    auto menu2 = Menu::create(facebookMenu, NULL);
    menu2->setPosition(cocos2d::Point::ZERO);
    this->addChild(menu2, 2);
    
    auto qrReaderMenuItem = MenuItemImage::create("btn_home_menu_normal.png",
                                                  "btn_home_menu_press.png",
                                                  CC_CALLBACK_1(Home::qrCodeCallback, this));
    qrReaderMenuItem->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
	qrReaderMenuItem->setPosition(HOME_TEST_MENU_QRCODE);
    
    auto qrReaderMenu = Menu::create(qrReaderMenuItem, NULL);
    qrReaderMenu->setPosition(cocos2d::Point::ZERO);
    this->addChild(qrReaderMenu, 2);
    
    auto popupMenuItem = MenuItemImage::create("btn_home_menu_normal.png",
                                               "btn_home_menu_press.png",
                                           CC_CALLBACK_1(Home::popupCallback, this));
    popupMenuItem->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
	popupMenuItem->setPosition(HOME_TEST_MENU_POPUP);
    
    auto popupMenu = Menu::create(popupMenuItem, NULL);
    popupMenu->setPosition(cocos2d::Point::ZERO);
    this->addChild(popupMenu, 2);
    
    auto toastLayerItem = MenuItemImage::create("btn_home_menu_normal.png",
                                               "btn_home_menu_press.png",
                                               CC_CALLBACK_1(Home::toastCallback, this));
    toastLayerItem->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
	toastLayerItem->setPosition(HOME_TEST_MENU_TOAST);
    
    auto toastLayerMenu = Menu::create(toastLayerItem, NULL);
    toastLayerMenu->setPosition(cocos2d::Point::ZERO);
    this->addChild(toastLayerMenu, 2);
#endif
}

void Home::initCloud(cocos2d::Size visibleSize)
{
    auto homeBg = Sprite::create("bg.png");
    homeBg->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    homeBg->setPosition(HOME_BG);
    bgLayer->addChild(homeBg, 0);
    
    // scrolling clouds
    auto cloud1 = Sprite::create("bg_home_cloud_01.png");
    auto cloud2 = Sprite::create("bg_home_cloud_02.png");
    
    cocos2d::Point cloud1StartPosition = HOME_BG_HOME_CLOUD_01;
    cocos2d::Point cloud2StartPosition = HOME_BG_HOME_CLOUD_02;
    
    cloud1->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    cloud2->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    
    cloud1->setPosition(cloud1StartPosition);
    cloud2->setPosition(cloud2StartPosition);
    
    float velocity = 30.0f;
    float endSideOffset = -MAX(cloud1->getContentSize().width, cloud2->getContentSize().width);
    
    auto cloud1MoveAction = MoveTo::create((visibleSize.width-cloud1StartPosition.x)/velocity, cocos2d::Point(visibleSize.width, cloud1StartPosition.y));
    auto cloud2MoveAction = MoveTo::create((visibleSize.width-cloud2StartPosition.x)/velocity, cocos2d::Point(visibleSize.width, cloud2StartPosition.y));
    
    auto cloud1PlaceAction = Place::create(cocos2d::Point(endSideOffset,  cloud1StartPosition.y));
    auto cloud2PlaceAction = Place::create(cocos2d::Point(endSideOffset,  cloud2StartPosition.y));
    
    auto cloud1MoveToStartPositionAction = MoveTo::create((-endSideOffset+cloud1StartPosition.x)/velocity, cocos2d::Point(cloud1StartPosition.x,  cloud1StartPosition.y));
    auto cloud2MoveToStartPositionAction = MoveTo::create((-endSideOffset+cloud2StartPosition.x)/velocity, cocos2d::Point(cloud2StartPosition.x,  cloud2StartPosition.y));
    
    auto cloud1ActionSquence = Sequence::create(cloud1MoveAction, cloud1PlaceAction, cloud1MoveToStartPositionAction, NULL);
    auto cloud2ActionSquence = Sequence::create(cloud2MoveAction, cloud2PlaceAction, cloud2MoveToStartPositionAction, NULL);
    
    auto cloud1RepeatAction = RepeatForever::create(cloud1ActionSquence);
    auto cloud2RepeatAction = RepeatForever::create(cloud2ActionSquence);
    
    cloud1->runAction(cloud1RepeatAction);
    cloud2->runAction(cloud2RepeatAction);
    
    bgLayer->addChild(cloud1,1);
    bgLayer->addChild(cloud2,1);
}

void Home::initItems(cocos2d::Size visibleSize)
{
//    auto coin = Sprite::create("btn_home_coin.png");
//    coin->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//    coin->setPosition(HOME_COIN);
//    mainLayer->addChild(coin, 1);
//    
//    auto btnX = Sprite::create("btn_home_x.png");
//    btnX->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//    btnX->setPosition(HOME_X);
//    mainLayer->addChild(btnX, 1);
//    
//    auto coinNum = Label::createWithSystemFont("8", FONT_NAME_BOLD, HOME_COIN_NUM_FONTSIZE,HOME_COIN_NUM_SIZE,TextHAlignment::LEFT, TextVAlignment::CENTER);
//    coinNum->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//    coinNum->setColor(Color3B(0xff,0x54,0x11));
//    coinNum->enableShadow(Color4B(0,0,0,30.6f),cocos2d::Size(0,-0.5), 0);
//    coinNum->setPosition(HOME_COIN_NUM);
//    mainLayer->addChild(coinNum, 1);
    
    this->doleCoinInit();
    _doleCoinBalance = UserDefault::getInstance()->getIntegerForKey(UDKEY_INTEGER_BALANCE, -1);
    if (_doleCoinBalance == -1) {//초기값 저장, iOS에서는 default 값을 처리할 수 없어 우선 저장한다.
        UserDefault::getInstance()->setIntegerForKey(UDKEY_INTEGER_BALANCE, _doleCoinBalance);
        UserDefault::getInstance()->flush();
    } else {
        this->updateDoleCoin(_doleCoinBalance);
    }
    
//    auto bgItemButton = Sprite::create("btn_home_bg.png");
//    bgItemButton->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//    bgItemButton->setPosition(HOME_BACKGROUND_ITEM_BTN);
//    mainLayer->addChild(bgItemButton, 1);
    
//    auto itemButton = Sprite::create("btn_home_item.png");
//    itemButton->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//    itemButton->setPosition(HOME_ITEM_BTN);
//    mainLayer->addChild(itemButton, 1);
    
//    auto bgItemButtonShadow = Sprite::create("btn_home_shadow.png");
//    bgItemButtonShadow->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//    bgItemButtonShadow->setPosition(HOME_ITEM_MENU);
//    mainLayer->addChild(bgItemButtonShadow, 0);
//    
//    auto itemButtonShadow = Sprite::create("btn_home_shadow.png");
//    itemButtonShadow->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//    itemButtonShadow->setPosition(HOME_ITEM_SHADOW);
//    mainLayer->addChild(itemButtonShadow, 0);
    
    openBgItem = false;
    openCharacterItem = false;
    auto bgItemButton = MenuItemImage::create("btn_home_bg_normal.png","btn_home_bg_selected.png",CC_CALLBACK_1(Home::itemButtonClicked, this));
    bgItemButton->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    bgItemButton->setPosition(cocos2d::Point::ZERO);
    
    auto itemButton = MenuItemImage::create("btn_home_item_normal.png","btn_home_item_selected.png",CC_CALLBACK_1(Home::itemButtonClicked, this));
    itemButton->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    itemButton->setPosition(HOME_ITEM_BTN);

    bgItemButton->setTag(MENU_BG);
    itemButton->setTag(MENU_ITEM);
    
    bottomMenu = Menu::create(bgItemButton, itemButton, NULL);
    bottomMenu->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    bottomMenu->setPosition(HOME_ITEM_MENU);
    bottomMenu->setTag(TAG_BTN_SELECT_BACKGROUND);
    mainLayer->addChild(bottomMenu, 10);
    
//    auto deleteSprite = Sprite::create();
//    deleteSprite->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//    deleteSprite->setContentSize(HOME_DELETE_BTN_GLOW_SIZE);
//    deleteSprite->setPosition(cocos2d::Point(visibleSize.width - HOME_CREATE_BTN_VISIBLE_RECT.width - HOME_DELETE_BTN_GLOW_SIZE.width - HOME_CREATE_BG_BTN.x - HOME_DELETE_BTN_PADDING,-HOME_ITEM_SCROLL_HEIGHT+HOME_DELETE_BTN_GLOW_SIZE.height));
//    mainLayer->addChild(deleteSprite);
//    
//    deleteButton = ui::Button::create("home_background_frame_del_normal.png", "home_background_frame_del_pressed.png");
//    deleteButton->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//    deleteButton->setPosition(cocos2d::Point(visibleSize.width - HOME_CREATE_BTN_VISIBLE_RECT.width - HOME_DELETE_BTN_GLOW_SIZE.width - HOME_CREATE_BG_BTN.x - HOME_DELETE_BTN_PADDING + (HOME_DELETE_BTN_GLOW_SIZE.width/2 - deleteButton->getContentSize().width/2), -HOME_ITEM_SCROLL_HEIGHT+HOME_CREATE_BG_BTN.y));
//    deleteButton->addTouchEventListener(this, (cocos2d::ui::SEL_TouchEvent)(&Home::itemDeleteButtonClicked));
//    mainLayer->addChild(deleteButton, 15);
//    
//    auto shadow = Sprite::create("home_background_frame_del_normal_shadow.png");
//    shadow->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//    shadow->setPosition(cocos2d::Point(-(HOME_DELETE_BTN_GLOW_SIZE.width/2 - deleteButton->getContentSize().width/2), (deleteButton->getContentSize().height - HOME_DELETE_BTN_GLOW_SIZE.height)));
//    shadow->setTag(TAG_DELETE_ITEM_SHADOW);
//    shadow->setVisible(true);
//    deleteButton->addChild(shadow);
    
//    deleteButton = ui::Button::create("home_background_frame_del_normal.png", "home_background_frame_del_pressed.png");
//    deleteButton->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//    deleteButton->setPosition(cocos2d::Point(visibleSize.width - (HOME_CREATE_BTN_VISIBLE_RECT.width)*2 - HOME_CREATE_BG_BTN.x - HOME_DELETE_BTN_PADDING,-HOME_ITEM_SCROLL_HEIGHT+HOME_CREATE_BG_BTN.y));
//    deleteButton->addTouchEventListener(this, (cocos2d::ui::SEL_TouchEvent)(&Home::itemDeleteButtonClicked));
//    mainLayer->addChild(deleteButton, 15);
    
//    auto deleteButtonLabel = Label::createWithSystemFont("Delete Character", FONT_NAME, HOME_CREATE_FONTSIZE, HOME_CREATE_TEXTSIZE, TextHAlignment::CENTER, TextVAlignment::CENTER);
//    deleteButtonLabel->setColor(Color3B(0xb2, 0x58, 0x08));
//    deleteButtonLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//    deleteButtonLabel->setPosition(HOME_CREATE_TEXT);
//    deleteButtonLabel->setTag(TAG_DELETE_ITEM_LABEL);
//    deleteButtonLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
//    
//    deleteButtonLabel->enableShadow(Color4B(255,255,255,255.0*0.5f),cocos2d::Size(0,-1), 0);
//    deleteButton->addChild(deleteButtonLabel);
    
    createBgButton = ui::Button::create("btn_home_item_create.png", "home_background_frame_del_pressed.png");
    createBgButton->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    createBgButton->setPosition(cocos2d::Point(visibleSize.width - HOME_CREATE_BTN_VISIBLE_RECT.width - HOME_CREATE_BG_BTN.x,-HOME_ITEM_SCROLL_HEIGHT+HOME_CREATE_BG_BTN.y));
    createBgButton->addTouchEventListener(this, (cocos2d::ui::SEL_TouchEvent)(&Home::createButtonClicked));
    mainLayer->addChild(createBgButton, 15);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    createButtonLabel = LabelExtension::create(Device::getResString(Res::Strings::CC_LIST_BACKGROUND_MAKE), FONT_NAME_BOLD, HOME_CREATE_FONTSIZE, HOME_CREATE_TEXTSIZE, TextHAlignment::CENTER, TextVAlignment::CENTER, false, false);//"Create background"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
    createButtonLabel = Label::createWithSystemFont(Device::getResString(Res::Strings::CC_LIST_BACKGROUND_MAKE), FONT_NAME_BOLD, HOME_CREATE_FONTSIZE, HOME_CREATE_TEXTSIZE, TextHAlignment::CENTER, TextVAlignment::CENTER);//"Create background"
#endif
    createButtonLabel->setColor(Color3B(0x8d, 0x33, 0x0f));
    createButtonLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    createButtonLabel->setPosition(HOME_CREATE_TEXT);
    createButtonLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    
    createButtonLabel->enableShadow(Color4B(254,177,149,255),cocos2d::Size(0,-1), 0);
    createBgButton->addChild(createButtonLabel);
//    createBgButtom->setRotation(-15); // y값 width*sin(15)
    
//    isItemDeleteMode = false;
    this->setCreateDeleteButtons(false);
}

void Home::createButtonClicked(Ref* obj, ui::TouchEventType type)
{
    switch (type) {
        case ui::TOUCH_EVENT_ENDED: {
            log("create bg clicked");
            if (cameraScene) return;
            
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
            AppDelegate* app = (AppDelegate*)Application::getInstance();
            if(!isPlatformShown) {
                if(openBgItem) {
                    app->showPlatformView(ANDROID_TAKE_PICTURE, TAG_HOME);
                } else {
                    app->showPlatformView(ANDROID_CREATE_CHARACTER, TAG_HOME);
                }
            }
            isPlatformShown = true;
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#ifndef MACOS
            auto director = Director::getInstance();
            if (openBgItem) {
                cameraScene = CustomCamera::createWithViewType(camera_type_bg);
            } else {
                cameraScene = CustomCamera::createWithViewType(camera_type_character);
            }
            CustomCamera *cameraLayer = (CustomCamera*)cameraScene->getChildByTag(TAG_CAMERA);
            cameraLayer->setDelegate(this);
            director->pushScene(cameraScene);
#endif
#endif
        } break;
        case ui::TOUCH_EVENT_BEGAN: break;
        case ui::TOUCH_EVENT_MOVED: break;
        case ui::TOUCH_EVENT_CANCELED: break;
        default: break;
    }
}

void Home::itemButtonClicked(Ref* obj)
{
    auto button = (MenuItemImage*)obj;
    int tag = button->getTag();
    
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Tap.wav", false);
    
    if (tag == MENU_BG) {
        if (!scrollViewToggleAnimation){
            log("bg item button clicked!");
            createButtonLabel->setString(Device::getResString(Res::Strings::CC_LIST_BACKGROUND_MAKE));
            button->setEnabled(false);
            
            //        if(isItemDeleteMode) {
            //            this->deleteModeChange();
            //        }
            //        auto deleteLabel = (Label*)deleteButton->getChildByTag(TAG_DELETE_ITEM_LABEL);
            //        deleteLabel->setString("Delete background");
            
            auto afterCallBack = CallFunc::create(CC_CALLBACK_0(Home::showBackgroundScrollView, this));
            auto toggleActionFinished = CallFunc::create([=](){
                button->setEnabled(true);
            });
            auto actions = Sequence::create(afterCallBack, toggleActionFinished, NULL);
            button->runAction(actions);
            
            Sprite *img;
            if (openBgItem) {
//                img = Sprite::create("btn_home_bg_normal.png");
//                this->setEnableButtons(true);
            } else {
                img = Sprite::create("btn_home_bg_selected.png");
                this->setEnableButtons(false);
                button->setNormalImage(img);
                openBgItem = true;
            }
//            button->setNormalImage(img);
//            openBgItem = !openBgItem;
            
            if (openCharacterItem) {
                MenuItemImage* characterItem = (MenuItemImage*)bottomMenu->getChildByTag(MENU_ITEM);
                characterItem->setNormalImage(Sprite::create("btn_home_item_normal.png"));
                characterItem->setEnabled(true);
                openCharacterItem = !openCharacterItem;
            }
        }
    } else if (tag == MENU_ITEM ){
        if (!scrollViewToggleAnimation){
            log("item button clicked!");
            createButtonLabel->setString(Device::getResString(Res::Strings::CC_LIST_CHARACTER_MAKE));//"Create character"
            button->setEnabled(false);
            
            //        if(isItemDeleteMode) {
            //            this->deleteModeChange();
            //        }
            //        auto deleteLabel = (Label*)deleteButton->getChildByTag(TAG_DELETE_ITEM_LABEL);
            //        deleteLabel->setString("Delete character");
            
            auto afterCallBack = CallFunc::create(CC_CALLBACK_0(Home::showCharacterScrollView, this));
            auto toggleActionFinished = CallFunc::create([=](){
                button->setEnabled(true);
            });
            auto actions = Sequence::create(afterCallBack, toggleActionFinished, NULL);
            button->runAction(actions);
            
            Sprite *img;
            if (openCharacterItem) {
//                img = Sprite::create("btn_home_item_normal.png");
//                this->setEnableButtons(true);
            } else {
                img = Sprite::create("btn_home_item_selected.png");
                this->setEnableButtons(false);
                button->setNormalImage(img);
                openCharacterItem = true;
            }
//            button->setNormalImage(img);
//            openCharacterItem = !openCharacterItem;
            
            if (openBgItem) {
                MenuItemImage* bgItem = (MenuItemImage*)bottomMenu->getChildByTag(MENU_BG);
                bgItem->setNormalImage(Sprite::create("btn_home_bg_normal.png"));
                bgItem->setEnabled(true);
                openBgItem = !openBgItem;
            }
        }
    }
}

void Home::setEnableButtons(bool enable)
{
    auto doleCoin = (ui::Button*)mainLayer->getChildByTag(TAG_BTN_DOLECOIN_COIN);
    doleCoin->setPressedActionEnabled(enable);
    doleCoin->setTouchEnabled(enable);
    auto doleCoinX = (ui::Button*)mainLayer->getChildByTag(TAG_BTN_DOLECOIN_X);
    doleCoinX->setTouchEnabled(enable);
    auto doleCoinUnknown = (ui::Button*)mainLayer->getChildByTag(TAG_BTN_DOLECOIN_UNKNOWN);
    if(doleCoinUnknown) {
        doleCoinUnknown->setTouchEnabled(enable);
    }
    
    for (int i=0;i<booksSpriteDic->count();i++) {
        auto bookKey = ((__String*)booksKeyArray->getObjectAtIndex(i))->getCString();
        auto bookHolder = (Layer*)booksSpriteDic->objectForKey(bookKey);
        auto book = (Sprite*)bookHolder->getChildByTag(BOOK_BODY);
        if (book) {
            auto deleteButton = (ui::Button*)book->getChildByTag(DELETE_BTN_TAG);
            if (deleteButton) {
                deleteButton->setTouchEnabled(enable);
            }
        }
        
    }
}

void Home::initBookScrollView(cocos2d::Size visibleSize)
{
    // load json
    // user order
    auto ud = UserDefault::getInstance();
    auto orderJsonString = ud->getStringForKey(ORDER_KEY, "");
    rapidjson::Document orderDocument;
    orderDocument.Parse<0>(orderJsonString.c_str());
    rapidjson::Value& bookOrder = orderDocument["book_order"];
    
    // pre load obj order
    auto preLoadObjOrder = ud->getStringForKey(PRE_LOAD_OBJ_ORDER_KEY);
    rapidjson::Document preLoadObjOrderDocument;
    preLoadObjOrderDocument.Parse<0>(preLoadObjOrder.c_str());
    rapidjson::Value& preLoadObjBookOrder = preLoadObjOrderDocument["book_order"];
    
    auto bookCount = preLoadObjBookOrder.Size() + bookOrder.Size();

    // create merged book Key array
    auto mergedBookKeyArray = __Array::createWithCapacity(bookCount);

    for(int index = 0; index < preLoadObjBookOrder.Size(); index++){
        mergedBookKeyArray->addObject( __String::create(preLoadObjBookOrder[rapidjson::SizeType(index)].GetString()) );
    }

    for(int index = 0; index < bookOrder.Size(); index++){
        mergedBookKeyArray->addObject( __String::create(bookOrder[rapidjson::SizeType(index)].GetString()) );
    }

    //booksKeyArray = __Array::createWithCapacity(bookCount+1);
    booksKeyArray = __Array::create();
    booksKeyArray->retain();
    
    booksSpriteDic = __Dictionary::create();
    booksSpriteDic->retain();
    
    // create scroll view
    cocos2d::Size contentsSize = HOME_BOOK_LAYER_SIZE;

    float padding = HOME_BOOK_PADDING;
   
    bookScrollView = ScrollViewExtension::create(cocos2d::Size(visibleSize.width,HOME_BOOK_SCROLL_HIEGHT), nullptr);
    bookScrollView->retain();
    bookScrollView->setDirection(TableView::Direction::HORIZONTAL);
    bookScrollView->setPosition(HOME_BOOK_LIST_VIEW);
    bookScrollView->setExtensionDelegate(this);
    bookScrollView->setBounceable(true);
    bookScrollView->setClippingToBounds(false);

    bookScrollView->setTag(BOOK_SCROLL_VIEW);

    int parseErrorCount = 0;
    // add books
    for (int index = 0; index < bookCount; index++) {
        auto bookKey = ((__String*)mergedBookKeyArray->getObjectAtIndex(index))->getCString();
        auto bookJsonString = ud->getStringForKey(bookKey);
        rapidjson::Document bookJson;
        bookJson.Parse<0>(bookJsonString.c_str());
        
        if (bookJson.HasParseError()) {
            log("parse error - bookKey : %s", bookKey);
            Util::removeParseErrorObject("book_order", (__String*)mergedBookKeyArray->getObjectAtIndex(index));
            parseErrorCount++;
        } else {
            auto bookBackground = bookJson["background"].GetString();
            log("bookBackground : %s", bookBackground);
            auto bookBg = Sprite::create(bookBackground);
            bookBg->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
            bookBg->setPosition(HOME_BOOK_BG);
            
            if (bookJson["editable"].GetBool() == true) {
                rapidjson::Value& chapters = bookJson["chapters"];
                bool allEmptyChapter = true;
                for (int i = 1; i < 6; i++) {
                    __String *chapterName = __String::createWithFormat("chapter%d", i);
                    rapidjson::Value& chapterData = chapters[chapterName->getCString()];
                    rapidjson::Document backgroundInfo;
                    if (!chapterData.IsNull() && chapterData.HasMember("char_datas")) {
                        auto backgroundKey = chapterData["background"].GetString();
                        auto backgroundString = ud->getStringForKey(backgroundKey);
                        backgroundInfo.Parse<0>(backgroundString.c_str());
                        
                        if (backgroundInfo["custom"].GetBool()) {
                            auto oldBookBg = bookJson["background"].GetString();
                            bool isCustomBookBg = this->isBookBgForCustom(oldBookBg);
                            
                            if (!isCustomBookBg) {
                                auto bgName = this->saveNewBookBgtoJson(bookJson, bookKey, !isCustomBookBg, backgroundKey);
                                bookBg->setTexture(bgName->getCString());
                            }
                            
                            auto bgPhoto = (char *)backgroundInfo["main_photo_file"].GetString();
                            auto bgPhotoImage = __String::create(bgPhoto);
                            if (bgPhotoImage != nullptr) {
                                std::string photoType("bg");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
                                std::string path = std::string(bgPhotoImage->getCString() + Util::getBookCoverPhotoPath(photoType));
#else
                                std::string path = Util::getBookThumbnailPhotoPath(photoType);
                                path.append(bgPhotoImage->getCString());
#endif
                                if (cocos2d::FileUtils::getInstance()->isFileExist(path) ){
                                    
                                    cocos2d::Data imageData = cocos2d::FileUtils::getInstance()->getDataFromFile(path.c_str());
                                    unsigned long nSize = imageData.getSize();
                                    
                                    cocos2d::Image *image = new cocos2d::Image();
                                    image->initWithImageData(imageData.getBytes(), nSize);
                                    
                                    cocos2d::Texture2D *texture = new cocos2d::Texture2D();
                                    texture->initWithImage(image);
                                    
                                    auto picture = Sprite::createWithTexture(texture);
                                    log("picture size : w%f h%f", picture->getContentSize().width, picture->getContentSize().height);
                                    picture->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                                    picture->setPosition(HOME_CUSTOM_BG_BOOK_THUMB);
                                    picture->setTag(TAG_BOOK_PICTURE);
                                    picture->setScale(HOME_CUSTOM_BG_WIDTH_INBOOK/picture->getContentSize().width);
                                    bookBg->addChild(picture);
                                } else {
                                    log("error! no file : %s", path.c_str());
                                }
                            }
                        } else {
                            auto oldBookBg = bookJson["background"].GetString();
                            bool isCustomBookBg = this->isBookBgForCustom(oldBookBg);
                            if (isCustomBookBg) {
                                auto bgName = this->saveNewBookBgtoJson(bookJson, bookKey, !isCustomBookBg,backgroundKey);
                                bookBg->setTexture(bgName->getCString());
                            } else {
                                auto bgName = this->saveNewBookBgtoJson(bookJson, bookKey, isCustomBookBg,backgroundKey);
                                bookBg->setTexture(bgName->getCString());
                            }
                        }
                        allEmptyChapter = false;
                        break;
                    }
                }
                
                if (allEmptyChapter) {
                    auto bgName = this->setDefaultBookBg(bookJson, bookKey);
                    bookBg->setTexture(bgName->getCString());
                }
            }
            
            
            //        auto bookBgImageName = bookJson["background_image"].GetString();
            //        auto bookBgImage = Sprite::create(bookBgImageName);
            //        bookBgImage->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
            //        bookBgImage->setPosition(cocos2d::Point::ZERO);
            //        bookBg->addChild(bookBgImage);
            //
            //        auto bookCover = Sprite::create("home_book_story_cover.png");
            //        bookCover->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
            //        bookCover->setPosition(cocos2d::Point::ZERO);
            ////        bookCover->setContentSize(newbook->getContentSize());
            //        bookBg->addChild(bookCover);
            
            auto book = Sprite::create();
            book->setContentSize(contentsSize);
            book->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
            book->setPosition( cocos2d::Point( HOME_NEW_BOOK_START_PADDING + (contentsSize.width + padding) * (index-parseErrorCount), HOME_BOOK_START_Y) );
            
            auto bookShadow = Sprite::create("home_book_story_shadow.png");
            bookShadow->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
            bookShadow->setPosition(cocos2d::Point::ZERO);
            //        bookShadow->setContentSize(newbook->getContentSize());
            book->addChild(bookShadow);
            
            book->addChild(bookBg, 2, BOOK_BODY);
            
            auto bookKeyString = __String::create(bookKey);
            bookKeyString->retain();
            book->setUserData(bookKeyString);
            
            auto authorString = bookJson["author"].GetString();
            rapidjson::Value& bookTitleColor = bookJson["font_color"];
            
            if (bookKeyString->compare("book1") == 0 || bookKeyString->compare("book2") == 0) {
                auto bookDoleLogo = Sprite::create("home_book_story_aouthor.png");
                bookDoleLogo->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                bookDoleLogo->setPosition(HOME_BOOK_PRELOAD_AUTHOR);
                //            bookDoleLogo->setContentSize(newbook->getContentSize());
                bookBg->addChild(bookDoleLogo,3);
            } else {
                auto bookAuthor = LabelExtension::create(authorString, FONT_NAME_BOLD, HOME_BOOK_AUTHOR_FONTSIZE, HOME_BOOK_AUTHOR_SIZE, TextHAlignment::CENTER, TextVAlignment::CENTER);
                bookAuthor->ellipsis(FONT_NAME_BOLD, HOME_BOOK_AUTHOR_FONTSIZE);
                bookAuthor->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                bookAuthor->setPosition(HOME_BOOK_AUTHOR);
                bookAuthor->setColor(Color3B(bookTitleColor[SizeType(0)].GetInt(),
                                             bookTitleColor[SizeType(1)].GetInt(),
                                             bookTitleColor[SizeType(2)].GetInt()));
                bookAuthor->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
                bookAuthor->enableShadow(Color4B(0,0,0,40.8f), LABEL_SHADOW_AUTHOR_SIZE, 0);
                bookAuthor->setTag(TAG_BOOK_AUTHOR_LABEL);
                bookBg->addChild(bookAuthor, 3);
            }
            
            // book titles
            char *bookLabelTitle = (char *)bookJson["title"].GetString();
            //        std::string bookTitleMain, bookTitleSub;
            //        this->splitTitleString(bookLabelTitle, ' ', bookTitleMain, bookTitleSub);
            
            //        int splitNum = this->getSplitStringStartIndex(bookLabelTitle, FONT_NAME_BOLD, HOME_BOOK_TITLE_FONTSIZE, HOME_BOOK_TITLE_SIZE.width);
            //
            //        if (splitNum <=1) {
            //            splitNum = this->getSplitStringWithoutWhitespace(bookLabelTitle, FONT_NAME_BOLD, HOME_BOOK_TITLE_FONTSIZE, HOME_BOOK_TITLE_SIZE.width);
            //        }
            //
            //        bookTitleMain = bookLabelTitle;
            //        bookTitleMain = bookTitleMain.substr(0, splitNum-1);
            //
            //        bookTitleSub = bookLabelTitle;
            //        bookTitleSub = bookTitleSub.substr(splitNum, bookTitleSub.length()-splitNum);
//            if (Application::getInstance()->getCurrentLanguage() == LanguageType::JAPANESE) {
                if (bookJson["editable"].GetBool() == false) {
                    if (strcmp(bookLabelTitle, "The Banana Brothers") == 0) {
                        bookLabelTitle = (char*)Device::getResString(Res::Strings::CC_PRLOAD_BOOKTITLE_1);
                    } else if (strcmp(bookLabelTitle, "Bob's Journey") == 0) {
                        bookLabelTitle = (char*)Device::getResString(Res::Strings::CC_PRLOAD_BOOKTITLE_2);
                    }
                }
//            }
            cocos2d::Size titleSize;
            cocos2d::Point linePosition;
            float bookTitleFontSize = 0;
            if (bookJson["editable"].GetBool() == true) {
                titleSize = HOME_BOOK_TITLE_SIZE;
                linePosition = HOME_BOOK_TITLE;
                bookTitleFontSize = HOME_BOOK_TITLE_FONTSIZE;
            } else {
                if (Application::getInstance()->getCurrentLanguage() != LanguageType::KOREAN) {
                    titleSize = HOME_PRE_BOOK_TITLE_SIZE;
                    linePosition = HOME_PRE_BOOK_TITLE;
                    bookTitleFontSize = HOME_BOOK_TITLE_PRE_FONTSIZE;
                } else {
                    titleSize = HOME_BOOK_TITLE_SIZE;
                    linePosition = HOME_BOOK_TITLE;
                    bookTitleFontSize = HOME_BOOK_TITLE_FONTSIZE;
                }
            }
        
            std::string bookTitleMain, bookTitleSub;
            Util::splitTitleString(bookLabelTitle, ' ', bookTitleMain, bookTitleSub, titleSize.width, FONT_NAME_BOLD, bookTitleFontSize);
            
            auto bookLabel = LabelExtension::create(bookTitleMain, FONT_NAME_BOLD, bookTitleFontSize, titleSize, TextHAlignment::CENTER, TextVAlignment::CENTER);
            if(bookLabel->getContentSize().width < HOME_BOOK_TITLE_SIZE.width) bookLabel->setWidth(HOME_BOOK_TITLE_SIZE.width);
            bookLabel->sizeToFitWidth(HOME_BOOK_TITLE_SIZE.width);
            bookLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
            if(strcmp(bookTitleSub.c_str(), "") == 0) {
                bookLabel->setPosition(HOME_BOOK_TITLE_A_LINE);
            } else {
                bookLabel->setPosition(linePosition);
            }
            bookLabel->setColor(Color3B(bookTitleColor[SizeType(0)].GetInt(),
                                        bookTitleColor[SizeType(1)].GetInt(),
                                        bookTitleColor[SizeType(2)].GetInt()));
            bookLabel->setTag(TAG_BOOK_TITLE_LABEL);
            bookBg->addChild(bookLabel, 2);
            bookLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
            bookLabel->enableShadow(Color4B(0,0,0,40.8f),LABEL_SHADOW_DEFAULT_SIZE, 0);
            
            auto bookSubLabel = LabelExtension::create(bookTitleSub, FONT_NAME_BOLD, bookTitleFontSize, HOME_BOOK_TITLE_SIZE, TextHAlignment::CENTER, TextVAlignment::CENTER);
            bookSubLabel->ellipsis(FONT_NAME_BOLD, HOME_BOOK_TITLE_FONTSIZE);
            bookSubLabel->setAnchorPoint(cocos2d::Point(0,0));
            bookSubLabel->setPosition(HOME_BOOK_TITLE_SUB);
            bookSubLabel->setColor(Color3B(bookTitleColor[SizeType(0)].GetInt(),
                                           bookTitleColor[SizeType(1)].GetInt(),
                                           bookTitleColor[SizeType(2)].GetInt()));
            bookSubLabel->setTag(TAG_BOOK_TITLE_SUBLABEL);
            bookBg->addChild(bookSubLabel, 2);
            bookSubLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
            bookSubLabel->enableShadow(Color4B(0,0,0,40.8f),LABEL_SHADOW_DEFAULT_SIZE, 0);
            
            log("load book : %s", bookKeyString->getCString());
            booksKeyArray-> addObject(bookKeyString);
            cocos2d::log("book key is added to array : %s", bookKeyString->getCString());
            booksSpriteDic->setObject(book, bookKeyString->getCString());
            
            bookScrollView->addChild(book, TOUCHABLE_LAYER);
            
            if( bookJson["editable"].GetBool() ) {
                //            auto deleteBtn = Sprite::create("btn_home_book_story_delete_normal.png");
                //            deleteBtn->setPosition(HOME_BOOK_DELETE_BUTTON);
                //            auto deleteBtn = MenuItemImage::create("btn_home_book_story_delete_normal.png","btn_home_book_story_delete_press.png");
                auto deleteBtn = cocos2d::ui::Button::create("btn_home_book_story_delete_normal.png", "btn_home_book_story_delete_press.png");
                deleteBtn->addTouchEventListener(this, (cocos2d::ui::SEL_TouchEvent)(&Home::deleteButtonClicked));
                deleteBtn->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
                //            deleteBtn->setPosition(cocos2d::Point(HOME_NEW_BOOK_START_PADDING + (contentsSize.width + padding) * index + HOME_DELETE_BTN_TYPE01.x, HOME_DELETE_BTN_TYPE01.y));
                deleteBtn->setPosition(HOME_DELETE_BTN_TYPE01);
                deleteBtn->setUserData(bookKeyString);
                
                bookBg->addChild(deleteBtn, TOUCHABLE_LAYER, DELETE_BTN_TAG);
            }
            //        this->showBookSideCharacter(bookKeyString);
        }
    }
    
    float totalContentsWidth = HOME_NEW_BOOK_START_PADDING + ((contentsSize.width + padding) * (bookCount-parseErrorCount)) + HOME_NEW_BOOK_LAYER_SIZE.width + padding;
    bookScrollView->setContentSize(cocos2d::Size(totalContentsWidth,HOME_BOOK_SCROLL_HIEGHT));

    auto newBookLayer = Layer::create();
    newBookLayer->setContentSize(HOME_NEW_BOOK_LAYER_SIZE);
    newBookLayer->setPosition(HOME_NEW_BOOK_START_PADDING + (contentsSize.width + padding) * (bookCount-parseErrorCount), HOME_BOOK_START_Y);
    
    // add new book
    auto newbook = Sprite::create("home_new_book.png");
    newbook->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    newbook->setPosition(HOME_BOOK_BG);
    newbook->setTag(0);
    auto newBookKey = __String::create(NEW_BOOK_KEY);
    newBookKey->retain();
    newBookLayer->setUserData(newBookKey);
    
    auto homeBookCover = Sprite::create("home_new_book_cover.png");
    homeBookCover->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    homeBookCover->setPosition(cocos2d::Point::ZERO);
    homeBookCover->setContentSize(newbook->getContentSize());
    newbook->addChild(homeBookCover);
    
    auto newBookShadow = Sprite::create("home_new_book_shadow.png");
    newBookShadow->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    newBookShadow->setPosition(cocos2d::Point::ZERO);
    newBookShadow->setContentSize(newbook->getContentSize());
    newBookLayer->addChild(newBookShadow);
    
    newBookLayer->addChild(newbook);
    
    LanguageType currentLanguageType = Application::getInstance()->getCurrentLanguage();
    switch (currentLanguageType) {
        case LanguageType::JAPANESE: {
            std::string firstJpStr, secondJpStr;
            Util::splitTitleString(Device::getResString(Res::Strings::CC_HOME_BOOKTITLE_NEW), ' ', firstJpStr, secondJpStr, HOME_NEW_BOOK_JP_TEXT_SIZE.width, FONT_NAME_BOLD, HOME_NEW_BOOK_JP_TITLE_FONTSIZE);
            
            auto newBookLabel = LabelExtension::create(firstJpStr, FONT_NAME_BOLD, HOME_NEW_BOOK_JP_TITLE_FONTSIZE, HOME_NEW_BOOK_JP_TEXT_SIZE, TextHAlignment::CENTER, TextVAlignment::CENTER);//"NEW"
            newBookLabel->setAnchorPoint(cocos2d::Point(0,0));
            newBookLabel->setPosition(HOME_NEW_BOOK_JP_TEXT_FIRST);
            newBookLabel->setColor(Color3B(0xee,0xea,0xde));
            newbook->addChild(newBookLabel);
            newBookLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
            newBookLabel->enableShadow(Color4B(0,0,0,40.8f),LABEL_SHADOW_DEFAULT_SIZE, 0);
            
            auto newBookSubLabel = LabelExtension::create(secondJpStr, FONT_NAME_BOLD, HOME_NEW_BOOK_JP_TITLE_FONTSIZE, HOME_NEW_BOOK_JP_TEXT_SIZE, TextHAlignment::CENTER, TextVAlignment::CENTER);//"NEW"
            newBookSubLabel->setAnchorPoint(cocos2d::Point(0,0));
            newBookSubLabel->setPosition(HOME_NEW_BOOK_JP_TEXT_SECOND);
            newBookSubLabel->setColor(Color3B(0xee,0xea,0xde));
            newbook->addChild(newBookSubLabel);
            newBookSubLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
            newBookSubLabel->enableShadow(Color4B(0,0,0,40.8f),LABEL_SHADOW_DEFAULT_SIZE, 0);
        } break;
        default: {
            auto newBookLabel = LabelExtension::create(Device::getResString(Res::Strings::CC_HOME_BOOKTITLE_NEW), FONT_NAME_BOLD, HOME_NEW_BOOK_TITLE_FONTSIZE, HOME_NEW_BOOK_TEXT_SIZE, TextHAlignment::CENTER, TextVAlignment::CENTER);//"NEW"
            //    newBookLabel->ellipsis();
            newBookLabel->setAnchorPoint(cocos2d::Point(0,0));
            newBookLabel->setPosition(HOME_NEW_BOOK_TEXT);
            
            //    auto frameSize = Director::getInstance()->getVisibleSize();
            //    if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS && (frameSize.width == 960 || frameSize.width == 1136)) {
            //        newBookLabel->setColor(Color3B(0xee,0xea,0xde));
            //    } else {
            //    newBookLabel->setColor(Color3B(0xad,0xfb,0xed));
            //    }
            newBookLabel->setColor(Color3B(0xee,0xea,0xde));
            
            newbook->addChild(newBookLabel);
            
            newBookLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
            newBookLabel->enableShadow(Color4B(0,0,0,40.8f),LABEL_SHADOW_DEFAULT_SIZE, 0);
        } break;
    }
    
    
    
    bookScrollView->addChild(newBookLayer, TOUCHABLE_LAYER);
    booksKeyArray->addObject(newBookKey);
    booksSpriteDic->setObject(newBookLayer, NEW_BOOK_KEY);
    
    mainLayer->addChild(bookScrollView, 1);
    
    bookScrollView->setContentOffset(cocos2d::Point(-totalContentsWidth+visibleSize.width, 0), false);
}

void Home::runBookSideCharacterAnimation(__String *bookKey, float delayTime, bool isUpdate)
{
//    log("show ani : %s", bookKey->getCString());
    
    auto ud = UserDefault::getInstance();
    auto bookJsonString = ud->getStringForKey(bookKey->getCString());
    
    if (bookJsonString.length() == 0) return;
    
    rapidjson::Document bookJson;
    bookJson.Parse<0>(bookJsonString.c_str());

    __Array *charKeys = __Array::create();
    
    auto *bookParent = (Sprite *)booksSpriteDic->objectForKey(bookKey->getCString());
    auto *book = bookParent->getChildByTag(BOOK_BODY);

    Sprite *char1 = (Sprite *)book->getChildByTag(TAG_BOOK_SIDE_CHARACTER_1);
    Sprite *char2 = (Sprite *)book->getChildByTag(TAG_BOOK_SIDE_CHARACTER_2);
    
    if (bookJson.HasMember("chapters") && !bookJson["chapters"].IsNull()) {
        rapidjson::Value& chapters = bookJson["chapters"];

        for (int index = 0; index < 5; index++) {
            __String* chapterName = __String::createWithFormat("chapter%d", index+1);
            rapidjson::Value& chapterDatas = chapters[chapterName->getCString()];
            if (!chapterDatas.IsNull() ) {
                rapidjson::Value& charDatas = chapterDatas["char_datas"];
                if ( charDatas.IsArray() && charDatas.Size() > 0 ){//!charDatas.IsNull() ){
                    for (int index = 0; index < charDatas.Size(); index++){
                        rapidjson::Value& dataDic = charDatas[rapidjson::SizeType(index)];
                        std::string charKey = dataDic["char_key"].GetString();
                        __String *charKeyString = __String::create(charKey);
                        
                        auto firstWord = charKey.substr(0,1);
                        if (charKeys->getIndexOfObject(charKeyString) == CC_INVALID_INDEX) {
                            if (strcmp(firstWord.c_str(), "I") != 0) {
                                charKeys->addObject(charKeyString);
                            }
                        }
                        if (charKeys->count() >= 2) {
                            break;
                        }
                    }
                }
            }
            if (charKeys->count() >= 2) {
                break;
            }
        }
        
        float duration = 0.1f;
        
        if (charKeys->count() >= 1) {
            Sprite *targetChar = nullptr;
            if (char1 == nullptr || isUpdate) {
                if (isUpdate && char1 != nullptr) {
                    char1->removeFromParent();
                }
                
                auto char1Key = (__String *)charKeys->getObjectAtIndex(0);
                
                auto character1 = this->createSideCharacter(char1Key);
                if (character1 != NULL) {
                    character1->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                    character1->setPosition(cocos2d::Point(HOME_BOOK_CHARACTER_01.x - character1->getContentSize().width,
                                                           HOME_BOOK_CHARACTER_01.y));
                    character1->setTag(TAG_BOOK_SIDE_CHARACTER_1);
                    
                    book->addChild(character1, -1);
                    targetChar = character1;
                }
                
            } else {
                if ( char1->getPositionX() != HOME_BOOK_CHARACTER_01.x) {
                    targetChar = char1;
                }
            }
            
            if (targetChar != nullptr) {
                auto moveToPoint = cocos2d::Point(HOME_BOOK_CHARACTER_01.x, HOME_BOOK_CHARACTER_01.y);
                auto moveTo = MoveTo::create(duration, moveToPoint);
                
                auto prevAni = targetChar->getActionByTag(TAG_SIDE_CHARACTER_ANIMATION);
                if (prevAni == nullptr) {
                    if (delayTime > 0) {
                        auto delay = DelayTime::create(delayTime);
                        auto sequence = Sequence::create(delay, moveTo, NULL);
                        sequence->setTag(TAG_SIDE_CHARACTER_ANIMATION);
                        targetChar->runAction(sequence);
                    } else {
                        moveTo->setTag(TAG_SIDE_CHARACTER_ANIMATION);
                        targetChar->runAction(moveTo);
                    }
                }
            }
        }

        if (charKeys->count() >= 2) {
            Sprite *targetChar = nullptr;
            if (char2 == nullptr || isUpdate) {
                if (isUpdate && char2 != nullptr) {
                    char2->removeFromParent();
                }

                auto char2Key = (__String *)charKeys->getObjectAtIndex(1);
                
                auto character2 = this->createSideCharacter(char2Key);
                if (character2 != NULL) {
                    character2->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                    character2->setScale(0.9f, 0.9f);
                    character2->setPosition(cocos2d::Point(HOME_BOOK_CHARACTER_02.x - character2->getContentSize().width,
                                                           HOME_BOOK_CHARACTER_02.y));
                    
                    character2->setTag(TAG_BOOK_SIDE_CHARACTER_2);
                    book->addChild(character2, -1);
                    targetChar = character2;
                }
                
            } else {
                if ( char2->getPositionX() != HOME_BOOK_CHARACTER_02.x) {
                    targetChar = char2;
                }
            }
            
            if (targetChar != nullptr) {
                
                auto prevAni = targetChar->getActionByTag(TAG_SIDE_CHARACTER_ANIMATION);
                if (prevAni == nullptr) {
                    auto moveToPoint = cocos2d::Point(HOME_BOOK_CHARACTER_02.x, HOME_BOOK_CHARACTER_02.y);
                    auto moveTo = MoveTo::create(duration, moveToPoint);
                    
                    auto delay = DelayTime::create(delayTime + duration);
                    auto sequence = Sequence::create(delay, moveTo, NULL);
                    sequence->setTag(TAG_SIDE_CHARACTER_ANIMATION);
                    targetChar->runAction(sequence);
                }
            }
        }
    }
}

Sprite* Home::createSideCharacter(__String* characterKey)
{
    auto ud = UserDefault::getInstance();
    auto char1JsonString = ud->getStringForKey(characterKey->getCString());
    if(char1JsonString.length() > 0 ){
        rapidjson::Document char1Json;
        char1Json.Parse<0>(char1JsonString.c_str());
        __String *char1FileName;
        __String *photoFileName = nullptr;
        int customID = -1;
        if (char1Json.HasMember("base_char")) { // custom character
            auto baseChar1 = char1Json["base_char"].GetString();
            auto baseCharacter1JsonString = ud->getStringForKey(baseChar1);
            rapidjson::Document baseCharacter1Info;
            baseCharacter1Info.Parse<0>(baseCharacter1JsonString.c_str());
            char1FileName = __String::create(baseCharacter1Info["side"].GetString());
            
            photoFileName = __String::create(char1Json["thumbnail_photo_file"].GetString());
            customID = baseCharacter1Info["custom_id"].GetInt();
        } else {
            char1FileName = __String::create(char1Json["side"].GetString());
        }
        auto sideCharacter = Sprite::create(char1FileName->getCString());
        
        if (photoFileName != nullptr) {
            std::string photoType("character");
            std::string path = Util::getThumbnailPhotoPath(photoType);
            path.append(photoFileName->getCString());
            if (!cocos2d::FileUtils::getInstance()->isFileExist(path) ){
                return NULL;
            }
            
            cocos2d::Data imageData = cocos2d::FileUtils::getInstance()->getDataFromFile(path.c_str());
            unsigned long nSize = imageData.getSize();
            
            cocos2d::Image *image = new cocos2d::Image();
            image->initWithImageData(imageData.getBytes(), nSize);
            
            cocos2d::Texture2D *texture = new cocos2d::Texture2D();
            texture->initWithImage(image);
            
            auto photoSprite = Sprite::createWithTexture(texture);
            
            cocos2d::Point photoPosition;
            cocos2d::Size photoSize;
            float angle = 45.0f;
            switch (customID) {
                case 10: // C010_custom
                    photoPosition = CUSTOM_CHARACTER_C010_SIDE_PHOTO;
                    photoSize = CUSTOM_CHARACTER_C010_SIDE_PHOTO_SIZE;
                    break;
                case 11: // C011_custom
                    photoPosition = CUSTOM_CHARACTER_C011_SIDE_PHOTO;
                    photoSize = CUSTOM_CHARACTER_C011_SIDE_PHOTO_SIZE;
                    break;
                case 12: // C012_custom
                    photoPosition = CUSTOM_CHARACTER_C012_SIDE_PHOTO;
                    photoSize = CUSTOM_CHARACTER_C012_SIDE_PHOTO_SIZE;
                    angle = 51.0f;
                    break;
                case 5: // C005_custom
                    photoPosition = CUSTOM_CHARACTER_C005_SIDE_PHOTO;
                    photoSize = CUSTOM_CHARACTER_C005_SIDE_PHOTO_SIZE;
                    angle = 50.0f;
                    break;
                case 9: // C009_custom
                    photoPosition = CUSTOM_CHARACTER_C009_SIDE_PHOTO;
                    photoSize = CUSTOM_CHARACTER_C009_SIDE_PHOTO_SIZE;
                    break;
                case 6: // C006_custom
                    photoPosition = CUSTOM_CHARACTER_C006_SIDE_PHOTO;
                    photoSize = CUSTOM_CHARACTER_C006_SIDE_PHOTO_SIZE;
                    break;
                case 7: // C007_custom
                    photoPosition = CUSTOM_CHARACTER_C007_SIDE_PHOTO;
                    photoSize = CUSTOM_CHARACTER_C007_SIDE_PHOTO_SIZE;
                    angle = 49.0f;
                    break;
                case 8: // C008_custom
                    photoPosition = CUSTOM_CHARACTER_C008_SIDE_PHOTO;
                    photoSize = CUSTOM_CHARACTER_C008_SIDE_PHOTO_SIZE;
                    angle = 51.0f;
                    break;
                case 1: // C001_custom
                    photoPosition = CUSTOM_CHARACTER_C001_SIDE_PHOTO;
                    photoSize = CUSTOM_CHARACTER_C001_SIDE_PHOTO_SIZE;
                    break;
                case 2: // C002_custom
                    photoPosition = CUSTOM_CHARACTER_C002_SIDE_PHOTO;
                    photoSize = CUSTOM_CHARACTER_C002_SIDE_PHOTO_SIZE;
                    break;
                case 3: // C003_custom
                    photoPosition = CUSTOM_CHARACTER_C003_SIDE_PHOTO;
                    photoSize = CUSTOM_CHARACTER_C003_SIDE_PHOTO_SIZE;
                    break;
                case 4: // C004_custom
                    photoPosition = CUSTOM_CHARACTER_C004_SIDE_PHOTO;
                    photoSize = CUSTOM_CHARACTER_C004_SIDE_PHOTO_SIZE;
                    break;
                case 15: // C015_custom, currently json declare 13, 14 (boy, girl image) as custom 15, 16
                    photoPosition = CUSTOM_CHARACTER_C013_SIDE_PHOTO;
                    photoSize = CUSTOM_CHARACTER_C013_SIDE_PHOTO_SIZE;
                    angle = 54.0f;
                    break;
                case 16: // C016_custom, currently json declare 13, 14 (boy, girl image) as custom 15, 16
                    photoPosition = CUSTOM_CHARACTER_C014_SIDE_PHOTO;
                    photoSize = CUSTOM_CHARACTER_C014_SIDE_PHOTO_SIZE;
                    break;
                default:
                    photoPosition = CUSTOM_CHARACTER_C010_SIDE_PHOTO;
                    photoSize = CUSTOM_CHARACTER_C010_SIDE_PHOTO_SIZE;
                    break;
            }

            photoSprite->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
            auto currentSize = photoSprite->getContentSize();
            auto scaleX = photoSize.width/currentSize.width;
            auto scaleY = photoSize.height/currentSize.height;
            photoSprite->setPosition(photoPosition);
            photoSprite->setScale(scaleX, scaleY);
//            changePositionAtAnchor(photoSprite, cocos2d::Point::ANCHOR_MIDDLE);
            photoSprite->setRotation(angle);
            sideCharacter->addChild(photoSprite, -1);
        }
        return sideCharacter;
    }
    
    return NULL;
}

void Home::showBookSideCharacter(float currentOffset)
{
    cocos2d::Size contentsSize = HOME_BOOK_LAYER_SIZE;
    float padding = HOME_BOOK_PADDING;
    
    auto itemSize = contentsSize.width + padding;
    
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    int displayedBookCountOnScreen = visibleSize.width / itemSize;
    auto remainInScreen = visibleSize.width - (displayedBookCountOnScreen * itemSize);
    
    auto newBookArea = fabs(currentOffset) - HOME_NEW_BOOK_START_PADDING;
    int newStartIndex = newBookArea/itemSize;
    
    int newRemainOffset = newBookArea - (newStartIndex * itemSize);
    int newEndIndex = newStartIndex + displayedBookCountOnScreen - 1;
    
    if (newRemainOffset >= contentsSize.width) {
        newStartIndex++;
    }
    
    if (newRemainOffset + remainInScreen > contentsSize.width) {
        newEndIndex++;
    }
    
//    log("show character : (%d ~ %d)", newStartIndex, newEndIndex);

    float animationDuration = 0.1f;
    float delay = animationDuration * 2;
    int delayCount = 0;
    for (int index = newEndIndex; index >= newStartIndex; index--) {
        auto bookKey = (__String *)booksKeyArray->getObjectAtIndex(index);
        
        if (bookKey->compare(NEW_BOOK_KEY) == 0) {
            continue;
        } else if (selectedBookKey != nullptr && bookKey->compare(selectedBookKey->getCString()) == 0 ){
            continue;
        }

        this->runBookSideCharacterAnimation(bookKey, delay * delayCount, false);
        delayCount++;
    }
}


void Home::hideBookSideCharacter(float oldOffset, float newOffset)
{
    cocos2d::Size contentsSize = HOME_BOOK_LAYER_SIZE;
    float padding = HOME_BOOK_PADDING;

    auto itemSize = contentsSize.width + padding;

    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    int displayedBookCountOnScreen = visibleSize.width / itemSize;
    auto remainInScreen = visibleSize.width - (displayedBookCountOnScreen * itemSize);
    
    auto oldBookArea = fabs(oldOffset) - HOME_NEW_BOOK_START_PADDING;
    int oldStartIndex = oldBookArea/itemSize;
    int oldRemainOffset = oldBookArea - (oldStartIndex * itemSize);
    int oldEndIndex = oldStartIndex + displayedBookCountOnScreen - 1;
    if (oldRemainOffset + remainInScreen > contentsSize.width) {
        oldEndIndex++;
    }
    
    auto newBookArea = fabs(newOffset) - HOME_NEW_BOOK_START_PADDING;
    int newStartIndex = newBookArea/itemSize;
    int newRemainOffset = newBookArea - (newStartIndex * itemSize);
    int newEndIndex = newStartIndex + displayedBookCountOnScreen - 1;
    if (newRemainOffset + remainInScreen > contentsSize.width) {
        newEndIndex++;
    }

//    log("hide character : old(%d ~ %d), new(%d ~ %d)", oldStartIndex, oldEndIndex, newStartIndex, newEndIndex);

    if (newStartIndex == oldStartIndex && newEndIndex == oldEndIndex){
        // no need to hide
        return;
    }
    
    int fivot1 = -1;
    int fivot2 = -1;
    
    if (oldStartIndex < newStartIndex) {
        fivot1 = newStartIndex -1;
    }
    
    if (newEndIndex < oldEndIndex) {
        fivot2 = newEndIndex + 1;
    }

    // left area
    if (fivot1 > -1){
        for (int index = oldStartIndex; index <= fivot1; index++) {
            if (index >= booksKeyArray->count()) break;

            auto bookKey = (__String *)booksKeyArray->getObjectAtIndex(index);
            Sprite *bookParent = (Sprite *)booksSpriteDic->objectForKey(bookKey->getCString());
            
            if (bookParent == nullptr) continue;
            
            auto *book = bookParent->getChildByTag(BOOK_BODY);
            
            if (book == nullptr) continue;
            
            auto char1 = book->getChildByTag(TAG_BOOK_SIDE_CHARACTER_1);
            if (char1 != nullptr) {
                char1->setPosition(cocos2d::Point(HOME_BOOK_CHARACTER_01.x - char1->getContentSize().width, char1->getPositionY()));
            }
            
            auto char2 = book->getChildByTag(TAG_BOOK_SIDE_CHARACTER_2);
            
            if (char2 != nullptr){
                char2->setPosition(cocos2d::Point(HOME_BOOK_CHARACTER_02.x - char2->getContentSize().width, char2->getPositionY()));
            }
        }
    }

    // right area
    if (fivot2 > -1){
        for (int index = fivot2; index <= oldEndIndex; index++) {
            if (index >= booksKeyArray->count()) break;
            
            auto bookKey = (__String *)booksKeyArray->getObjectAtIndex(index);
            Sprite *bookParent = (Sprite *)booksSpriteDic->objectForKey(bookKey->getCString());
            
            if (bookParent == nullptr) continue;
            
            auto *book = bookParent->getChildByTag(BOOK_BODY);

            if (book == nullptr) continue;
            
            auto char1 = book->getChildByTag(TAG_BOOK_SIDE_CHARACTER_1);
            if (char1 != nullptr) {
                char1->setPosition(cocos2d::Point(HOME_BOOK_CHARACTER_01.x - char1->getContentSize().width, char1->getPositionY()));
            }
            
            auto char2 = book->getChildByTag(TAG_BOOK_SIDE_CHARACTER_2);
            if (char2 != nullptr){
                char2->setPosition(cocos2d::Point(HOME_BOOK_CHARACTER_02.x - char2->getContentSize().width, char2->getPositionY()));
            }
            
        }
    }
//    log("hide character : (%d ~ %d / %d ~ %d)", oldStartIndex, fivot1, fivot2, oldEndIndex);
}

Node* Home::getBookCharacter(const char *ccbiName, cocos2d::Point position, float scale)
{
    cocosbuilder::NodeLoaderLibrary *nodeLoaderLibrary = cocosbuilder::NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
    
    cocosbuilder::CCBReader *ccbReader = new cocosbuilder::CCBReader(nodeLoaderLibrary);
    auto bookCharacter = ccbReader->readNodeGraphFromFile(ccbiName, this);
    bookCharacter->setPosition(position);
    bookCharacter->setScale(scale);
    ccbReader->release();
    
    return bookCharacter;
}

void Home::scrollViewDidScroll(ScrollViewExtension *view)
{
    int tag = view->getTag();
    if(tag == BOOK_SCROLL_VIEW) {
        auto oldOffset = scrollOffset.x;
//        log("scrollViewDidScroll old = %f, new = %f, drag = %d", oldOffset, view->getContentOffset().x, view->isDragging());
        
        this->hideBookSideCharacter(oldOffset, view->getContentOffset().x);
        
        scrollOffset = view->getContentOffset();

        if (view->getContentOffset().x == view->minContainerOffset().x) {
            if (oldOffset == view->minContainerOffset().x){
                if (view->isDragging()){
                    if (!reachedToEnd) {
                        reachedToEnd = true;
                        return;
                    }
                    
                    auto newBookLayer = (Layer *)booksSpriteDic->objectForKey(NEW_BOOK_KEY);
                    auto newBook = (Sprite *)newBookLayer->getChildByTag(0);
                    float currentRotation = newBook->getRotation();
                    
                    if (currentRotation > -MAX_BOOK_ROTATION) {
                        currentRotation -= 1;
                    }
                    
                    newBook->setRotation(currentRotation);
                } else {
                    if (reachedToEnd) {
                        auto newBookLayer = (Layer *)booksSpriteDic->objectForKey(NEW_BOOK_KEY);
                        auto newBook = (Sprite *)newBookLayer->getChildByTag(0);
                        newBook->setRotation(0);
                        this->showBookSideCharacter(view->getContentOffset().x);
                        reachedToEnd = false;
                    }
                }
            } else {
                if (isScrollByAdd) {
                    isScrollByAdd = false;
                } else {
                    if (view->isDragging()) return;
                    
                    auto newBookLayer = (Layer *)booksSpriteDic->objectForKey(NEW_BOOK_KEY);
                    auto newBook = (Sprite *)newBookLayer->getChildByTag(0);
                    
                    auto orgPosition = newBook->getPosition();
                    changePositionAtAnchor(newBook, cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                    
                    float rotationDuration = 0.1f;
                    float rotationAngle = -5.0f;
                    auto rotateAction = RotateTo::create(rotationDuration, rotationAngle);
                    auto rotateReverseAction = RotateTo::create(rotationDuration, 0);
                    auto rotationCallback = CallFunc::create([&, view](){
                        auto newBook = (Sprite *)booksSpriteDic->objectForKey(NEW_BOOK_KEY);
                        changePositionAtAnchor(newBook, cocos2d::Point::ANCHOR_MIDDLE);
                        this->showBookSideCharacter(view->getContentOffset().x);

                        //                newBook->setAnchorPoint(Point(0.5, 0.5));
                        //                newBook->setPosition(72+newBook->getContentSize().width/2, newBook->getContentSize().height/2);
                    });
                    auto newBookSequence = Sequence::create(rotateAction, rotateReverseAction, rotationCallback, NULL);
                    newBook->runAction(newBookSequence);
                }
            }
        } else if (view->getContentOffset().x == view->maxContainerOffset().x) {
            if (oldOffset == view->maxContainerOffset().x){
                if (view->isDragging()){
                    if (!reachedToEnd) {
                        reachedToEnd = true;
                        return;
                    }
                    
                    auto bookKey = (__String *)booksKeyArray->getObjectAtIndex(0);
                    auto lastBookLayer = (Layer *)booksSpriteDic->objectForKey(bookKey->getCString());
                    auto lastBook = (Sprite *)lastBookLayer->getChildByTag(BOOK_BODY);
                    float currentRotation = lastBook->getRotation();
                    
                    changePositionAtAnchor(lastBook, cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
                    
                    if (currentRotation < MAX_BOOK_ROTATION) {
                        currentRotation += 1;
                    }
                    
                    lastBook->setRotation(currentRotation);
                } else {
                    if (reachedToEnd) {
                        auto bookKey = (__String *)booksKeyArray->getObjectAtIndex(0);
                        auto lastBookLayer = (Layer *)booksSpriteDic->objectForKey(bookKey->getCString());
                        auto lastBook = (Sprite *)lastBookLayer->getChildByTag(BOOK_BODY);
                        lastBook->setRotation(0);
                        changePositionAtAnchor(lastBook, cocos2d::Point::ANCHOR_MIDDLE);
                        this->showBookSideCharacter(view->getContentOffset().x);
                        reachedToEnd = false;
                    }
                }
            } else {
                if (view->isDragging()) return;
                
                auto bookKey = (__String *)booksKeyArray->getObjectAtIndex(0);
                auto lastBookLayer = (Layer *)booksSpriteDic->objectForKey(bookKey->getCString());
                auto lastBook = (Sprite *)lastBookLayer->getChildByTag(BOOK_BODY);
                
                auto orgPosition = lastBook->getPosition();
                changePositionAtAnchor(lastBook, cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
                
                float rotationDuration = 0.1f;
                float rotationAngle = 5.0f;
                auto rotateAction = RotateTo::create(rotationDuration, rotationAngle);
                auto rotateReverseAction = RotateTo::create(rotationDuration, 0);
                auto rotationCallback = CallFunc::create([&, view](){
                    auto bookKey = (__String *)booksKeyArray->getObjectAtIndex(0);
                    auto lastBookLayer = (Layer *)booksSpriteDic->objectForKey(bookKey->getCString());
                    auto lastBook = (Sprite *)lastBookLayer->getChildByTag(BOOK_BODY);
                    changePositionAtAnchor(lastBook, cocos2d::Point::ANCHOR_MIDDLE);
                    
                    this->showBookSideCharacter(view->getContentOffset().x);
                });
                auto newBookSequence = Sequence::create(rotateAction, rotateReverseAction, rotationCallback, NULL);
                lastBook->runAction(newBookSequence);
            }
        } else {
            if (view->isDragging() && reachedToEnd) {
                if (oldOffset - view->getContentOffset().x > 0) {
                    auto bookKey = (__String *)booksKeyArray->getObjectAtIndex(0);
                    auto lastBookLayer = (Layer *)booksSpriteDic->objectForKey(bookKey->getCString());
                    auto lastBook = (Sprite *)lastBookLayer->getChildByTag(BOOK_BODY);
                    lastBook->setRotation(0);
                    reachedToEnd = false;
                } else {
                    auto newBookLayer = (Layer *)booksSpriteDic->objectForKey(NEW_BOOK_KEY);
                    auto newBook = (Sprite *)newBookLayer->getChildByTag(0);
                    newBook->setRotation(0);
                    reachedToEnd = false;
                }
            }
        }
    } else {}
}

void Home::scrollViewDidZoom(ScrollViewExtension *view)
{
    log("scrollViewDidZoom");
}

void Home::scrollViewDidScrollEnd(ScrollViewExtension *view)
{
    log("scroll end!");
    if( !isAddingBookAnimation ) {
        this->showBookSideCharacter(view->getContentOffset().x);
    }
}

//void Home::backgroundButtonCallback(Ref* pSender)
//{
//    auto frame = (Sprite *)((MenuItemSprite*)pSender);
//    float scaleDuration = 0.05f;
//    float shrinkRatio = 0.97;
//    auto scaleAction = ScaleTo::create(scaleDuration, shrinkRatio);
//    auto reverse = ScaleTo::create(scaleDuration, 1.0f);
//    auto afterCallBack = CallFunc::create(CC_CALLBACK_0(Home::toggleBackgroundScrollView, this));
//    auto actions = Sequence::create(scaleAction, reverse, afterCallBack, NULL);
//    frame->runAction(actions);
//}

bool Home::isScrollViewVisible()
{
    float positionY = bgLayer->getPosition().y;
    float scrollViewHeight = HOME_ITEM_SCROLL_HEIGHT;
    if (positionY < scrollViewHeight) {
        return false;
    } else {
        return true;
    }
}

void Home::setCreateDeleteButtons(bool visible)
{
    createBgButton->setVisible(visible);
    createBgButton->setEnabled(visible);
//    deleteButton->setVisible(visible);
//    deleteButton->setEnabled(visible);
}

//void Home::itemDeleteButtonClicked(Ref *obj, ui::TouchEventType type)
//{
//    switch (type) {
//        case ui::TOUCH_EVENT_ENDED: {
//            this->deleteModeChange();
//        } break;
//        case ui::TOUCH_EVENT_BEGAN: break;
//        case ui::TOUCH_EVENT_MOVED: break;
//        case ui::TOUCH_EVENT_CANCELED: break;
//        default: break;
//    }
//}

//void Home::deleteModeChange()
//{
//    log("itemDeleteButtonClicked");
//    isItemDeleteMode = !isItemDeleteMode;
//    
//    if (openBgItem) {
//        
//    } else {
//        if (isItemDeleteMode) {
//            createBgButton->setLocalZOrder(0);
//            createBgButton->setTouchEnabled(false);
//            deleteButton->loadTextureNormal("home_background_frame_del_02_normal.png");
//        } else {
//            createBgButton->setLocalZOrder(15);
//            createBgButton->setTouchEnabled(true);
//            deleteButton->loadTextureNormal("home_background_frame_del_normal.png");
//        }
//        auto shadow = deleteButton->getChildByTag(TAG_DELETE_ITEM_SHADOW);
//        shadow->setVisible(isItemDeleteMode);
//        characterScrollView->deleteModeDim->setVisible(isItemDeleteMode);
//        auto scrollView =  (ScrollViewExtension*)characterScrollView->getChildByTag(TAG_SCROLLVIEW);
//        scrollView->deleteMode = isItemDeleteMode;
//        
//        __Array *deleteButtons = characterScrollView->deleteButtons;
//        auto size = deleteButtons->count();
//        for(int index = 0 ; index < size ; index++){
//            ui::Button *deleteBtn = (ui::Button*)deleteButtons->getObjectAtIndex(index);
//            deleteBtn->setVisible(isItemDeleteMode);
//            deleteBtn->setEnabled(isItemDeleteMode);
//        }
//    }
//}

void Home::scrollViewButtonEnable(bool backgroundButtonEnable)
{
    __Array *chardeleteButtons = characterScrollView->deleteButtons;
    auto size1 = chardeleteButtons->count();
    for(int index = 0 ; index < size1 ; index++){
        ui::Button *deleteBtn = (ui::Button*)chardeleteButtons->getObjectAtIndex(index);
        deleteBtn->setEnabled(!backgroundButtonEnable);
    }
    __Array *deleteButtons = backgroundScrollView->deleteButtons;
    auto size2 = deleteButtons->count();
    for(int index = 0 ; index < size2 ; index++){
        ui::Button *deleteBtn = (ui::Button*)deleteButtons->getObjectAtIndex(index);
        deleteBtn->setEnabled(backgroundButtonEnable);
    }
}

void Home::scrollToFirst(ScrollView *scrollView)
{
    scrollView->setContentOffsetInDuration(cocos2d::Point::ZERO, 0.5f);
}

void Home::hideBottomScrollView()
{
    scrollViewToggleAnimation = true;
    float scrollDuration = 0.2;

    auto action = EaseInOut::create(MoveTo::create(scrollDuration, cocos2d::Point::ZERO), 0.8f);
    auto actionCallback = CallFunc::create([&](){
        backgroundScrollView->setVisible(false);
        characterScrollView->setVisible(false);

        this->setCreateDeleteButtons(false);
        scrollViewToggleAnimation = false;
    });
    auto squence = Sequence::create(action, actionCallback, NULL);
    bgLayer->runAction(squence);
    
    isBackgroundScrollViewVisible = false;
    isCharacterScrollViewVisible = false;
    
    this->scrollViewButtonEnable(false);
}

void Home::showBackgroundScrollView()
{
    if (menuListLayer->menuListVisible) {
        menuListLayer->menuListControl(false);
    }
    if (scrollViewToggleAnimation) return;
    
    characterScrollView->setVisible(false);
    isCharacterScrollViewVisible = false;

    float scrollDuration = 0.2;
    if (!isBackgroundScrollViewVisible) {
        if(this->isScrollViewVisible()){
            backgroundScrollView->setVisible(!backgroundScrollView->isVisible());
            isBackgroundScrollViewVisible = !isBackgroundScrollViewVisible;
            scrollViewToggleAnimation = false;
        } else {
            scrollViewToggleAnimation = true;
            backgroundScrollView->setVisible(true);
            auto action = EaseInOut::create(MoveTo::create(scrollDuration, cocos2d::Point(0, HOME_ITEM_SCROLL_HEIGHT)), 0.8f);
            auto actionCallback = CallFunc::create([&](){
                scrollViewToggleAnimation = false;
            });
            auto squence = Sequence::create(action, actionCallback, NULL);
            bgLayer->runAction(squence);
            isBackgroundScrollViewVisible = true;
        }
        this->setCreateDeleteButtons(true);
        this->scrollViewButtonEnable(true);
    } else {
//        this->hideBottomScrollView();
        backgroundScrollView->scrollToFirstItem();
    }
}

void Home::showCharacterScrollView()
{
    if (menuListLayer->menuListVisible) {
        menuListLayer->menuListControl(false);
    }
    if (scrollViewToggleAnimation) return;
    
    backgroundScrollView->setVisible(false);
    isBackgroundScrollViewVisible = false;

    float scrollDuration = 0.2;
    if (!isCharacterScrollViewVisible) {
        if(this->isScrollViewVisible()){
            characterScrollView->setVisible(!characterScrollView->isVisible());
            isCharacterScrollViewVisible = !isCharacterScrollViewVisible;
            scrollViewToggleAnimation = false;
        } else {
            scrollViewToggleAnimation = true;
            characterScrollView->setVisible(true);
            auto action = EaseInOut::create(MoveTo::create(scrollDuration, cocos2d::Point(0, HOME_ITEM_SCROLL_HEIGHT)), 0.8f);
            auto actionCallback = CallFunc::create([&](){
                scrollViewToggleAnimation = false;
            });
            auto squence = Sequence::create(action, actionCallback, NULL);
            bgLayer->runAction(squence);
            isCharacterScrollViewVisible = true;
        }
        this->setCreateDeleteButtons(true);
        this->scrollViewButtonEnable(false);
    } else {
//        this->hideBottomScrollView();
        characterScrollView->scrollToFirstItem();
    }
}


void Home::onSelectItem(ScrollViewExtension* view, Node* selectedItem, Touch *pTouch, Event *pEvent)
{
    if (view == bookScrollView) {
        if (isBackgroundScrollViewVisible) return;
        if (isCharacterScrollViewVisible) return;
        if (isBookAnimation) return;
        if (movingScene) return;
        
        movingScene = true;
        Sprite *sprite = (Sprite *)selectedItem;
        selectedBookKey = (__String *)sprite->getUserData();
        changePositionAtAnchor(sprite, cocos2d::Point::ANCHOR_MIDDLE);

        float scaleDuration = 0.2f;
        float shrinkRatio = 0.97;
        auto scaleAction = ScaleTo::create(scaleDuration, shrinkRatio);
        auto reverse = ScaleTo::create(scaleDuration, 1.0f);
        
//        if (sprite->getTag() == DELETE_BTN_TAG) {
//			log("delete btn");
//            selDeleteBtn = sprite;
//            Director::getInstance()->pause();
//
//            PopupLayer *modal = PopupLayer::create(POPUP_TYPE_WARNING, Device::getResString(Res::Strings::CC_HOME_POPUP_DELETE_STORY),
//                                                   Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL),
//                                                   Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK));
//            //"Selected storybook will be deleted. Deleted records can't be restored.", "Cancel", "OK"
//
//            modal->setTag(DELETE_BOOK_POPUP);
//            modal->setDelegate(this);
//            this->addChild(modal, 1000);
//            
//			return;
//		}
        
//        Sprite *deleteBtn = (Sprite *)sprite->getChildByTag(DELETE_BTN_TAG);
//        selDeleteBtn = (Sprite *)sprite->getChildByTag(DELETE_BTN_TAG);
//        if (selDeleteBtn) {
//            cocos2d::Point touchLocation = pTouch->getLocation();
//            cocos2d::Point local = sprite->convertToNodeSpace(touchLocation);
//            cocos2d::Rect bb = selDeleteBtn->getBoundingBox();
//            if (bb.containsPoint( local ) ){
//                log("delete btn");
//                Director::getInstance()->pause();
//                PopupLayer *modal = PopupLayer::create("Selected storybook will be deleted. Deleted records can't be restored.");
//                modal->setTag(DELETE_BOOK_POPUP);
//                modal->setDelegate(this);
//                this->addChild(modal, 1000);
//                
//                return;
//            }
//        }
        
        if (selectedBookKey->compare(NEW_BOOK_KEY) == 0 )
        {
            // new book
            auto scaleCallback = CallFunc::create([&](){
                log("create new book");
                std::string createdBookKey = this->addNewBook();
                selectedBookKey = __String::create(createdBookKey.c_str());
                selectedBookKey->retain();
                
                isBookAnimation = false;
                auto director = Director::getInstance();
                auto bookMapScene = BookMap::createScene(this->selectedBookKey, true);
                BookMap* bookMapLayer = (BookMap*)(bookMapScene->getChildByTag(TAG_BOOKMAP));
                bookMapLayer->setDelegate(this);
                bookMapLayer->setHome(this);
                auto transition = TransitionProgressInOut::create(0.4f, bookMapScene);
                director->pushScene(transition);

                movingScene = false;
            });
            auto scaleSequence = Sequence::create(scaleAction, reverse, scaleCallback, NULL);
            sprite->runAction(scaleSequence);
            
            AppDelegate* app = (AppDelegate*)Application::getInstance();
            if(app != nullptr)
            {
                app->mixpanelTrack("Started Creating Custom Storybook", "{\"Storybook\": \"Custom\"}");
            }
//        }
//        else if ( selectedBookKey->compare("book1") == 0 || selectedBookKey->compare("book2") == 0 ) {
//#ifndef MACOS
//            auto scaleCallback = CallFunc::create([&](){
//                isBookAnimation = false;
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
//            	AppDelegate* app = (AppDelegate*)Application::getInstance();
//            	if(!isPlatformShown) {
//            		app->showPlatformView(ANDROID_PLAY_VIDEO);
//            	}
//            	isPlatformShown = true;
//#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
//                auto director = Director::getInstance();
//                auto video = JeVideo::createScene();
//                auto transition = TransitionProgressInOut::create(0.4f, video);
//                director->pushScene(transition);
////                director->replaceScene(video);
////                this->cleanup();
//#endif
//                movingScene = false;
//            });
//            auto scaleSequence = Sequence::create(scaleAction, reverse, scaleCallback, NULL);
//            sprite->runAction(scaleSequence);
//#else
//            isBookAnimation = false;
//#endif
        } else {
            auto scaleCallback = CallFunc::create([&](){
                isBookAnimation = false;
                auto director = Director::getInstance();
                //                log("bookkey : %s", selectedBookKey->getCString());
                auto bookMapScene = BookMap::createScene(this->selectedBookKey, false);
                BookMap* bookMapLayer = (BookMap*)(bookMapScene->getChildByTag(TAG_BOOKMAP));
                bookMapLayer->setDelegate(this);
                bookMapLayer->setHome(this);
                auto transition = TransitionProgressInOut::create(0.4f, bookMapScene);
                director->pushScene(transition);
//                auto transition = TransitionProgressInOut::create(0.4f, bookMapScene);
//                director->pushScene(transition);
                // this->cleanup();
                movingScene = false;
            });
            auto scaleSequence = Sequence::create(scaleAction, reverse, scaleCallback, NULL);
            sprite->runAction(scaleSequence);
            
            AppDelegate* app = (AppDelegate*)Application::getInstance();
            if(app != nullptr)
            {
                if (selectedBookKey->compare("book1") == 0)
                {
                    app->mixpanelTrack("Opened Book Sample A", "{\"Storybook\": \"Sample A\"}");
                }
                else if (selectedBookKey->compare("book2") == 0)
                {
                    app->mixpanelTrack("Opened Book Sample B", "{\"Storybook\": \"Sample B\"}");
                } else
                {
                    app->mixpanelTrack("Opened Custom Storybook", "{\"Storybook\": \"Custom\"}");
                }
            }
            
        }
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Journey_map.wav", false);
    }
}

void Home::updateCurrentBook(__String *bookKey)
{
    auto ud = UserDefault::getInstance();
    auto bookTitleFromJson = "";
    auto authorTitleFromJson = "";
    auto bookJsonString = ud->getStringForKey(bookKey->getCString());
    rapidjson::Document bookJson;
    if (bookJsonString != "") {
        this->runBookSideCharacterAnimation(bookKey, 0, true);
        
        bookJson.Parse<0>(bookJsonString.c_str());
        
        bool editable = bookJson["editable"].GetBool();
        if (!editable) {
            return;
        }
        
        bookTitleFromJson = bookJson["title"].GetString();
        authorTitleFromJson = bookJson["author"].GetString();
    }
    
//    log("bookTitle : %s",bookTitleFromJson);
    
//    std::string bookTitleMain, bookTitleSub;
//    this->splitTitleString(bookTitleFromJson, ' ', bookTitleMain, bookTitleSub);
    
//    int splitNum = this->getSplitStringStartIndex(bookTitleFromJson, FONT_NAME_BOLD, HOME_BOOK_TITLE_FONTSIZE, HOME_BOOK_TITLE_SIZE.width);
//    
//    if (splitNum <=1) {
//        splitNum = this->getSplitStringWithoutWhitespace(bookTitleFromJson, FONT_NAME_BOLD, HOME_BOOK_TITLE_FONTSIZE, HOME_BOOK_TITLE_SIZE.width);
//    }
//    
//    bookTitleMain = bookTitleFromJson;
//    bookTitleMain = bookTitleMain.substr(0, splitNum-1);
//    
//    bookTitleSub = bookTitleFromJson;
//    bookTitleSub = bookTitleSub.substr(splitNum, bookTitleSub.length()-splitNum);
    
    std::string bookTitleMain, bookTitleSub;
    Util::splitTitleString(bookTitleFromJson, ' ', bookTitleMain, bookTitleSub, HOME_BOOK_TITLE_SIZE.width, FONT_NAME_BOLD, HOME_BOOK_TITLE_FONTSIZE);
    
    auto updateBookHolder = (Layer*)booksSpriteDic->objectForKey(bookKey->getCString());
    auto updateBook = (Sprite*)updateBookHolder->getChildByTag(BOOK_BODY);
    
    rapidjson::Value& chapters = bookJson["chapters"];
    bool allEmptyChapter = true;
    for (int i = 1; i < 6; i++) {
        __String *chapterName = __String::createWithFormat("chapter%d", i);
        rapidjson::Value& chapterData = chapters[chapterName->getCString()];
        rapidjson::Document backgroundInfo;
        if (!chapterData.IsNull() && chapterData.HasMember("char_datas")) {
            auto backgroundKey = chapterData["background"].GetString();
            auto backgroundString = ud->getStringForKey(backgroundKey);
            backgroundInfo.Parse<0>(backgroundString.c_str());
            
            if (backgroundInfo["custom"].GetBool()) {
                auto oldBookBg = bookJson["background"].GetString();
                bool isCustomBookBg = this->isBookBgForCustom(oldBookBg);
                log("isCustomBookBg : %d", isCustomBookBg);
                if (!isCustomBookBg) {
                    auto bgName = this->saveNewBookBgtoJson(bookJson, bookKey->getCString(), !isCustomBookBg, backgroundKey);
                    updateBook->setTexture(bgName->getCString());
                    auto updatePicture = (Sprite*)updateBook->getChildByTag(TAG_BOOK_PICTURE);
                    if(updatePicture != NULL) {
                        updatePicture->removeFromParentAndCleanup(true);
                    }
                }
                
                auto bgPhoto = (char *)backgroundInfo["main_photo_file"].GetString();
                auto bgPhotoImage = __String::create(bgPhoto);
                if (bgPhotoImage != nullptr) {
                    std::string photoType("bg");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
					std::string path = std::string(bgPhotoImage->getCString() + Util::getBookCoverPhotoPath(photoType));
#else
                    std::string path = Util::getBookThumbnailPhotoPath(photoType);
                    path.append(bgPhotoImage->getCString());
#endif
                    if (cocos2d::FileUtils::getInstance()->isFileExist(path) ){
                        
                        cocos2d::Data imageData = cocos2d::FileUtils::getInstance()->getDataFromFile(path.c_str());
                        unsigned long nSize = imageData.getSize();
                        
                        cocos2d::Image *image = new cocos2d::Image();
                        image->initWithImageData(imageData.getBytes(), nSize);
                        
                        cocos2d::Texture2D *texture = new cocos2d::Texture2D();
                        texture->initWithImage(image);
                        
                        auto updatePicture = (Sprite*)updateBook->getChildByTag(TAG_BOOK_PICTURE);
                        if(updatePicture != NULL) {
                            updatePicture->setTexture(texture);
                        } else {
                            if (!isCustomBookBg) {
                                auto picture = Sprite::createWithTexture(texture);
                                log("picture size : w%f h%f", picture->getContentSize().width, picture->getContentSize().height);
                                picture->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                                picture->setPosition(HOME_CUSTOM_BG_BOOK_THUMB);
                                picture->setTag(TAG_BOOK_PICTURE);
                                picture->setScale(HOME_CUSTOM_BG_WIDTH_INBOOK/picture->getContentSize().width);
                                updateBook->addChild(picture);
                            }
                        }
                    } else {
                        log("error! no file : %s", path.c_str());
                    }
                }
            } else {
                auto oldBookBg = bookJson["background"].GetString();
                bool isCustomBookBg = this->isBookBgForCustom(oldBookBg);
                if (isCustomBookBg) {
                    auto bgName = this->saveNewBookBgtoJson(bookJson, bookKey->getCString(), !isCustomBookBg, backgroundKey);
                    updateBook->setTexture(bgName->getCString());
                    auto updatePicture = (Sprite*)updateBook->getChildByTag(TAG_BOOK_PICTURE);
                    if(updatePicture != NULL) {
                        updatePicture->removeFromParentAndCleanup(true);
                    }
                } else {
                    auto bgName = this->saveNewBookBgtoJson(bookJson, bookKey->getCString(), false, backgroundKey);
                    updateBook->setTexture(bgName->getCString());
                }
                
            } // custom
            allEmptyChapter = false;
            break;
        }
    }
    
    if (allEmptyChapter) {
        auto bgName = this->setDefaultBookBg(bookJson, bookKey->getCString());
        updateBook->setTexture(bgName->getCString());
        
        auto updatePicture = (Sprite*)updateBook->getChildByTag(TAG_BOOK_PICTURE);
        if(updatePicture != NULL) {
            updatePicture->removeFromParentAndCleanup(true);
        }
    }
    
    rapidjson::Value& bookTitleColor = bookJson["font_color"];

    auto titleLabel = (LabelExtension*)updateBook->getChildByTag(TAG_BOOK_TITLE_LABEL);
    titleLabel->setColor(Color3B(bookTitleColor[SizeType(0)].GetInt(),
                                 bookTitleColor[SizeType(1)].GetInt(),
                                 bookTitleColor[SizeType(2)].GetInt()));
    titleLabel->setString(bookTitleMain);
    if(strcmp(bookTitleSub.c_str(), "") == 0) {
        titleLabel->setPosition(HOME_BOOK_TITLE_A_LINE);
    } else {
        titleLabel->setPosition(HOME_PRE_BOOK_TITLE);
    }
    
    auto subTitleLabel = (LabelExtension*)updateBook->getChildByTag(TAG_BOOK_TITLE_SUBLABEL);
    subTitleLabel->setString(bookTitleSub);
    subTitleLabel->setColor(Color3B(bookTitleColor[SizeType(0)].GetInt(),
                                    bookTitleColor[SizeType(1)].GetInt(),
                                    bookTitleColor[SizeType(2)].GetInt()));
    subTitleLabel->ellipsis(FONT_NAME_BOLD, HOME_BOOK_TITLE_FONTSIZE);
    
    auto authorLabel = (LabelExtension*)updateBook->getChildByTag(TAG_BOOK_AUTHOR_LABEL);
    authorLabel->setString(authorTitleFromJson);
    authorLabel->setColor(Color3B(bookTitleColor[SizeType(0)].GetInt(),
                                  bookTitleColor[SizeType(1)].GetInt(),
                                  bookTitleColor[SizeType(2)].GetInt()));
    authorLabel->ellipsis(FONT_NAME_BOLD, HOME_BOOK_AUTHOR_FONTSIZE);
    
//    this->showBookSideCharacter(bookKey);
}

bool Home::isBookBgForCustom(const char* bookBgName)
{
    std::string bookBgStr(bookBgName);
    auto bookBgSubStr = bookBgStr.substr(5,6);
    if (strcmp(bookBgSubStr.c_str(), "create") == 0) {
        return true;
    } else {
        return false;
    }
}

__String* Home::saveNewBookBgtoJson(rapidjson::Document &bookJson, const char* bookKey, bool isCustomBg, const char* chapterBgKey)
{
    __String* spriteName = NULL;
    if (isCustomBg) {
        spriteName = __String::createWithFormat("home_create_book_story_%02d.png", rand()%3+1);
        bookJson["background"].SetString(spriteName->getCString());
        
        rapidjson::Value fontColor(rapidjson::kArrayType);
        if (strcmp(spriteName->getCString(), "home_create_book_story_01.png") == 0) {
            fontColor.PushBack(0x9f, bookJson.GetAllocator());
            fontColor.PushBack(0xd3, bookJson.GetAllocator());
            fontColor.PushBack(0x13, bookJson.GetAllocator());
        } else if (strcmp(spriteName->getCString(), "home_create_book_story_02.png") == 0) {
            fontColor.PushBack(0x31, bookJson.GetAllocator());
            fontColor.PushBack(0x92, bookJson.GetAllocator());
            fontColor.PushBack(0xe4, bookJson.GetAllocator());
        } else if (strcmp(spriteName->getCString(), "home_create_book_story_03.png") == 0) {
            fontColor.PushBack(0xff, bookJson.GetAllocator());
            fontColor.PushBack(0xd2, bookJson.GetAllocator());
            fontColor.PushBack(0x00, bookJson.GetAllocator());
        } else if (strcmp(spriteName->getCString(), "home_create_book_story_04.png") == 0) {
            fontColor.PushBack(0x28, bookJson.GetAllocator());
            fontColor.PushBack(0xd2, bookJson.GetAllocator());
            fontColor.PushBack(0xb8, bookJson.GetAllocator());
        }
        bookJson["font_color"] = fontColor;
        
    } else {
        std::string bgKey(chapterBgKey);
        auto subBgKey = bgKey.substr(2,2);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        int bookBgNum = atoi(subBgKey.c_str());
#else
        int bookBgNum = std::stoi(subBgKey);
#endif
        log("bgKey sub : %02d", bookBgNum);
        
        if (bookBgNum == 16) {
            spriteName = __String::createWithFormat("home_book_story_%02d.png", 07);
        } else if (bookBgNum == 15) {
            spriteName = __String::createWithFormat("home_book_story_%02d.png", 14);
        } else if (bookBgNum == 14) {
            spriteName = __String::createWithFormat("home_book_story_%02d.png", 15);
        } else if (bookBgNum == 17) {
            spriteName = __String::createWithFormat("home_book_story_%02d.png", 10);
        } else {
            spriteName = __String::createWithFormat("home_book_story_%02d.png", bookBgNum);
        }
        
        bookJson["background"].SetString(spriteName->getCString());
        
        rapidjson::Value fontColor(rapidjson::kArrayType);
        
        if (strcmp(spriteName->getCString(), "home_book_story_01.png") == 0) {
            fontColor.PushBack(0xff, bookJson.GetAllocator());
            fontColor.PushBack(0x84, bookJson.GetAllocator());
            fontColor.PushBack(0x00, bookJson.GetAllocator());
        } else if (strcmp(spriteName->getCString(), "home_book_story_02.png") == 0 || strcmp(spriteName->getCString(), "home_book_story_08.png") == 0) {
            fontColor.PushBack(0x9f, bookJson.GetAllocator());
            fontColor.PushBack(0xd3, bookJson.GetAllocator());
            fontColor.PushBack(0x13, bookJson.GetAllocator());
        } else if (strcmp(spriteName->getCString(), "home_book_story_03.png") == 0 || strcmp(spriteName->getCString(), "home_book_story_06.png") == 0) {
            fontColor.PushBack(0x31, bookJson.GetAllocator());
            fontColor.PushBack(0x92, bookJson.GetAllocator());
            fontColor.PushBack(0xe4, bookJson.GetAllocator());
        } else if (strcmp(spriteName->getCString(), "home_book_story_04.png") == 0 || strcmp(spriteName->getCString(), "home_book_story_09.png") == 0) {
            fontColor.PushBack(0xff, bookJson.GetAllocator());
            fontColor.PushBack(0xd2, bookJson.GetAllocator());
            fontColor.PushBack(0x00, bookJson.GetAllocator());
        } else if (strcmp(spriteName->getCString(), "home_book_story_05.png") == 0) {
            fontColor.PushBack(0x28, bookJson.GetAllocator());
            fontColor.PushBack(0xd2, bookJson.GetAllocator());
            fontColor.PushBack(0xb8, bookJson.GetAllocator());
        } else if (strcmp(spriteName->getCString(), "home_book_story_07.png") == 0) {
            fontColor.PushBack(0xff, bookJson.GetAllocator());
            fontColor.PushBack(0x84, bookJson.GetAllocator());
            fontColor.PushBack(0x00, bookJson.GetAllocator());
        } else if (strcmp(spriteName->getCString(), "home_book_story_10.png") == 0) {
            fontColor.PushBack(0xff, bookJson.GetAllocator());
            fontColor.PushBack(0xd2, bookJson.GetAllocator());
            fontColor.PushBack(0x00, bookJson.GetAllocator());
        } else if (strcmp(spriteName->getCString(), "home_book_story_11.png") == 0) {
            fontColor.PushBack(0x80, bookJson.GetAllocator());
            fontColor.PushBack(0x4f, bookJson.GetAllocator());
            fontColor.PushBack(0xca, bookJson.GetAllocator());
        } else if (strcmp(spriteName->getCString(), "home_book_story_12.png") == 0) {
            fontColor.PushBack(0xe4, bookJson.GetAllocator());
            fontColor.PushBack(0x7b, bookJson.GetAllocator());
            fontColor.PushBack(0x29, bookJson.GetAllocator());
        } else if (strcmp(spriteName->getCString(), "home_book_story_13.png") == 0) {
            fontColor.PushBack(0xff, bookJson.GetAllocator());
            fontColor.PushBack(0xd2, bookJson.GetAllocator());
            fontColor.PushBack(0x00, bookJson.GetAllocator());
        } else if (strcmp(spriteName->getCString(), "home_book_story_14.png") == 0) {
            fontColor.PushBack(0x30, bookJson.GetAllocator());
            fontColor.PushBack(0xcf, bookJson.GetAllocator());
            fontColor.PushBack(0xb5, bookJson.GetAllocator());
        } else if (strcmp(spriteName->getCString(), "home_book_story_15.png") == 0) {
            fontColor.PushBack(0x9f, bookJson.GetAllocator());
            fontColor.PushBack(0xd3, bookJson.GetAllocator());
            fontColor.PushBack(0x13, bookJson.GetAllocator());
        } else {
            fontColor.PushBack(0x28, bookJson.GetAllocator());
            fontColor.PushBack(0xd2, bookJson.GetAllocator());
            fontColor.PushBack(0xb8, bookJson.GetAllocator());
        }
        
        bookJson["font_color"] = fontColor;
    }
    
    rapidjson::StringBuffer strbuf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
    bookJson.Accept(writer);
    auto jsonString = strbuf.GetString();
    auto ud = UserDefault::getInstance();
    ud->setStringForKey(bookKey, jsonString);
    ud->flush();
    
    auto bookJsonString = ud->getStringForKey(bookKey);
    bookJson.Parse<0>(bookJsonString.c_str());
    
    return spriteName;
}

__String* Home::setDefaultBookBg(rapidjson::Document &bookJson, const char* bookKey)
{
    auto spriteName = __String::create("home_book_story_01.png");
    bookJson["background"].SetString(spriteName->getCString());
    
    rapidjson::Value fontColor(rapidjson::kArrayType);
    
    fontColor.PushBack(0xff, bookJson.GetAllocator());
    fontColor.PushBack(0x84, bookJson.GetAllocator());
    fontColor.PushBack(0x00, bookJson.GetAllocator());
    bookJson["font_color"] = fontColor;
    
    rapidjson::StringBuffer strbuf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
    bookJson.Accept(writer);
    auto jsonString = strbuf.GetString();
    auto ud = UserDefault::getInstance();
    ud->setStringForKey(bookKey, jsonString);
    ud->flush();
    
    auto bookJsonString = ud->getStringForKey(bookKey);
    bookJson.Parse<0>(bookJsonString.c_str());
    
    return spriteName;
}

void Home::onExitBookMap(__String *bookKey, bool added)
{
    if(added) {
        this->runAddNewBookAnimation(bookKey);
    } else {
        __String *toRemoveBookKey = bookKey->clone();
        this->removeBook(toRemoveBookKey);
        
        auto userDefault = UserDefault::getInstance();
        auto lastBookNum = userDefault->getIntegerForKey(BOOK_CUSTOM_NUM_KEY, -1);
        unsigned long int bookNum = lastBookNum - 1;
        userDefault->setIntegerForKey(BOOK_CUSTOM_NUM_KEY, (int)bookNum);
        userDefault->flush();
    }
}

void Home::textColorButtonNormal(TextColorButton* button)
{
    Label *buttonLabel = (Label*)button->getChildByTag(TAG_BG_SELECT_COIN);
    buttonLabel->setColor(Color3B(0xfc,0x42,0x10));
    buttonLabel->enableShadow(Color4B(255,255,255,255.0f*0.51f),cocos2d::Size(1.0,-0.866), 0);
}

void Home::textColorButtonPressed(TextColorButton* button)
{
    Label *buttonLabel = (Label*)button->getChildByTag(TAG_BG_SELECT_COIN);
    buttonLabel->setColor(Color3B(0xff,0x54,0x11));
    buttonLabel->enableShadow(Color4B(255,255,255,255.0f*0.51f),cocos2d::Size(1.0,-0.866), 0);
}

void Home::bgItemCoinClicked(Ref *pSender, ui::TouchEventType type)
{
    log("bgItem coin clicked");
    switch (type) {
        case ui::TOUCH_EVENT_ENDED: {
            // TODO:tuilise password input control call 후 처리해야 한다.
            std::string authKey = UserDefault::getInstance()->getStringForKey(UDKEY_STRING_AUTHKEY);
            if (authKey.length() > 0) {
//                bool isSNSAccount = UserDefault::getInstance()->getBoolForKey(UDKEY_BOOL_FACEBOOKLOGIN);
//                if (isSNSAccount) {
//                    this->doleExecutePurchase();
//                } else {
                    this->runPasswordView();
////                    this->doleExecutePurchase();
//                }
//                ui::Button *coinButton = (ui::Button*)pSender;
//                coinButton->setEnabled(false);
            } else {
                PopupLayer *needLogin = PopupLayer::create(POPUP_TYPE_WARNING,
                                                           Device::getResString(Res::Strings::CC_EXPANDEDITEM_POPUP_NEED_LOGIN),
                                                           Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL),
                                                           Device::getResString(Res::Strings::CC_HOME_POPUP_NEED_LOGIN_BUTTON_TITLE_LOGIN));
                //"Log in to use Dole Coin.", "Cancel", "Log in"
                needLogin->setTag(TAG_POPUP_NEEDLOGIN);
                needLogin->setDelegate(this);
                this->addChild(needLogin);
            }
        } break;
        case ui::TOUCH_EVENT_BEGAN: log("Button Touch Down"); break;
        case ui::TOUCH_EVENT_MOVED: log("Button Touch Move"); break;
        case ui::TOUCH_EVENT_CANCELED: log("Button Touch Cancelled"); break;
        default: break;
    }
}

void Home::dimLayerRemoved()
{
    bgItemClicked = false;
    isCharacterSelected = false;
}

#ifndef MACOS
void Home::photoCallback(Ref* pSender)
{
    auto director = Director::getInstance();
    auto cameraScene = CustomCamera::createWithViewType(camera_type_bg);
    auto transition = TransitionProgressInOut::create(0.4f, cameraScene);
    director->pushScene(transition);
//    director->replaceScene(transition);
//    this->cleanup();
}
#endif

void Home::popupCallback(Ref* pSender)
{
    Director::getInstance()->pause();
    PopupLayer *modal = PopupLayer::create(POPUP_TYPE_WARNING, "Do you want to cancel signup process?","Cancel","OK");
    modal->setTag(POPUP_TEST);
    modal->setDelegate(this);
    this->addChild(modal, 1000);
}

void Home::toastCallback(Ref* pSender)
{
    auto toastLayer = ToastLayer::create("This is a test message.", 2);
    toastLayer->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    toastLayer->setPosition(cocos2d::Point::ZERO);
    this->addChild(toastLayer);
}

void Home::qrCodeCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	AppDelegate* app = (AppDelegate*)Application::getInstance();
	app->showPlatformView(ANDROID_SCAN_QRCODE, TAG_HOME);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
    UIViewController *viewController = nil;
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
    } else {
        viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
    }
    NSLog(@"viewController screen frame : %@",NSStringFromCGRect(viewController.view.frame));
    QRCodeReaderViewController *qrCodeReaderViewContoller = [[QRCodeReaderViewController alloc] init];
    
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//        qrCodeReaderViewContoller.modalPresentationStyle = UIModalPresentationFormSheet;
//    }
    qrCodeReaderViewContoller.originLayer = TSOriginLayerHome;
    [viewController presentViewController:qrCodeReaderViewContoller animated:YES completion:nil];
    [qrCodeReaderViewContoller release];
#endif
#endif
}

void Home::popupButtonClicked(PopupLayer *popup, cocos2d::Ref *obj)
{
    Director::getInstance()->resume();
    auto clickedButton = (MenuItem*)obj;
    int tag = clickedButton->getTag();
    int popupTag = popup->getTag();
    switch (popupTag) {
        case DELETE_BOOK_POPUP: {
            if (tag == 0) {
                log("delete cancelButton clicked!!");
            } else {
                
                AppDelegate* app = (AppDelegate*)Application::getInstance();
                if(app != nullptr)
                {
                    app->mixpanelTrack("Deleted Custom Storybook", NULL);
                }
                
                isBookAnimation = true;
                log("delete okButton clicked!!");
                float scaleDuration = 0.2f;
                float shrinkRatio = 0.97;
                auto scaleAction = ScaleTo::create(scaleDuration, shrinkRatio);
                auto reverse = ScaleTo::create(scaleDuration, 1.0f);
                
                auto scaleCallback = CallFunc::create([&](){
                    this->runRemoveBookAnimation(selectedBookKey);
                });
                auto scaleSequence = Sequence::create(scaleAction, reverse, scaleCallback, NULL);
                selDeleteBtn->runAction(scaleSequence);
            }
            movingScene = false;
        } break;
        case TAG_POPUP_NEEDLOGIN: {
            if (tag == 2) {
                _isPurchasReady = _isPurchasReady | PURCHASE_START_WITH_LOGIN;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
                AppDelegate* app = (AppDelegate*)Application::getInstance();
                if(app != nullptr) {
                    app->setPendingAction(ANDROID_PENDING_PURCHASE);
                    app->showPlatformView(ANDROID_LOGIN, TAG_HOME);
                }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#ifdef UI_TYPE_IOS8
                TSAccountLoginViewController *loginVC = [[TSAccountLoginViewController alloc] init];
                loginVC.originLayer = TSOriginLayerHome;
                loginVC.recallType = TSRecallHomePurchase;
                [UIViewController replaceRootViewControlller:loginVC];
                [loginVC release];
#else//UI_TYPE_IOS8
                UIViewController *viewController = nil;
                if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                    viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
                } else {
                    viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
                }
                NSLog(@"viewController screen frame : %@",NSStringFromCGRect(viewController.view.frame));
                TSAccountViewController *accountViewController = [[TSAccountViewController alloc] initWithFirstRun:NO];
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                    accountViewController.modalPresentationStyle = UIModalPresentationFormSheet;
                }
                accountViewController.originLayer = TSOriginLayerHome;
                accountViewController.recallType = TSRecallHomePurchase;
                if(IS_IOS8 && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                    accountViewController.preferredContentSize = CGSizeMake(1024, 768);
                }
                [viewController presentViewController:accountViewController animated:YES completion:nil];
                [accountViewController release];
#endif//UI_TYPE_IOS8
#endif
            }
        } break;
        case TAG_POPUP_ERRORCOIN: {
            if (tag == 2) {
                this->runDoleCoinView();
            }
        } break;
        case TAG_POPUP_COINLOGIN: {
            if (tag == 2) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
                AppDelegate* app = (AppDelegate*)Application::getInstance();
                if(app != nullptr) {
                    app->setPendingAction(ANDROID_DOLE_COIN);
                    app->showPlatformView(ANDROID_LOGIN, TAG_HOME);
                }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#ifdef UI_TYPE_IOS8
                TSAccountLoginViewController *loginVC = [[TSAccountLoginViewController alloc] init];
                loginVC.originLayer = TSOriginLayerHome;
                loginVC.recallType = TSRecallHomeDoleCoin;
                [UIViewController replaceRootViewControlller:loginVC];
                [loginVC release];
#else//UI_TYPE_IOS8
                UIViewController *viewController = nil;
                if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                    viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
                } else {
                    viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
                }
                NSLog(@"viewController screen frame : %@",NSStringFromCGRect(viewController.view.frame));
                TSAccountViewController *accountViewController = [[TSAccountViewController alloc] initWithFirstRun:NO];
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                    accountViewController.modalPresentationStyle = UIModalPresentationFormSheet;
                }
                accountViewController.originLayer = TSOriginLayerHome;
                accountViewController.recallType = TSRecallHomeDoleCoin;
                if(IS_IOS8 && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                    accountViewController.preferredContentSize = CGSizeMake(1024, 768);
                }
                [viewController presentViewController:accountViewController animated:YES completion:nil];
                [accountViewController release];
#endif//UI_TYPE_IOS8
#endif
            } else {
                this->runDoleCoinView();
            }
        }
        case TAG_POPUP_UNLOCKITEM: {
            if (tag == 2) {
                this->doleExecutePurchase();
            } else {
                _isPurchasReady = 0;
            }
        } break;
        case TAG_POPUP_CRITICAL_UPDATE: {
            if (tag == 1) {
//                log("appurl  : %s", appURL.c_str());
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
                if(!appURL.empty()) {
                    Application::getInstance()->openURL(appURL.c_str());
                }
#endif
            }
        } break;
        default: {
            if (tag == 0) {
                log("cancelButton clicked!");
            } else {
                log("okButton clicked!!");
            }
        } break;
    }
    
//    if (popup->getTag() == DELETE_BOOK_POPUP) {
//        if (tag == 0) {
//            log("delete cancelButton clicked!!");
//        } else {
//            isBookAnimation = true;
//            log("delete okButton clicked!!");
//            float scaleDuration = 0.2f;
//            float shrinkRatio = 0.97;
//            auto scaleAction = ScaleTo::create(scaleDuration, shrinkRatio);
//            auto reverse = ScaleTo::create(scaleDuration, 1.0f);
//            
//            auto scaleCallback = CallFunc::create([&](){
//                this->runRemoveBookAnimation(selectedBookKey);
//                selDeleteBtn->removeFromParentAndCleanup(true);
//            });
//            auto scaleSequence = Sequence::create(scaleAction, reverse, scaleCallback, NULL);
//            selDeleteBtn->runAction(scaleSequence);
//        }
//        movingScene = false;
//    } else {
//        if (tag == 0) {
//            log("cancelButton clicked!");
//        } else {
//            log("okButton clicked!!");
//        }
//    }
//    isBookAnimation = false;
}

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
void Home::facebookCallback(Ref* pSender)
{    
    UIViewController *selfViewController = nil;
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        selfViewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
    } else {
        selfViewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
    }
    FacebookFriendListViewController *facebookFriendListViewController = [[FacebookFriendListViewController alloc] init];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        facebookFriendListViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
//    [selfViewController presentModalViewController:facebookFriendListViewController animated:YES];
    [selfViewController presentViewController:facebookFriendListViewController animated:YES completion:nil];
}
#endif
#endif

void Home::menuCallback(Ref* pSender)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Tap.wav", false);
    menuListLayer->menuListControl(!menuListLayer->menuListVisible);
}

void Home::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void Home::listButtonClicked(int buttonTag)
{
    switch (buttonTag) {
        case BUTTON_TYPE_SETTINGS: {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	AppDelegate* app = (AppDelegate*)Application::getInstance();
	if(app != nullptr) {
		app->showPlatformView(ANDROID_SETTING, TAG_HOME);
	}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
            //    std::string authKey = UserDefault::getInstance()->getStringForKey(UDKEY_STRING_AUTHKEY);
            //    if (authKey.length() > 0 ) {// login 되어 있음.
            //        this->runSettingView();
            //    } else {
            //        this->runAccountView();
            //    }
             this->runSettingView();//내부 View 에서 분기가 이루어 진다.
#endif
           
        } break;
        default:
            break;
    }
}

void Home::onSelectCharacter(CharacterScrollView* view, Node* selectedItem, __String* characterKey, int index)
{
	if(isCharacterSelected) return;

    if (characterKey) {
        log("frame pressed : %d key : %s", index, characterKey->getCString());
        
    	isCharacterSelected = true;
        
        auto ud = UserDefault::getInstance();
        auto charString = ud->getStringForKey(characterKey->getCString());
        rapidjson::Document characterInfo;
        characterInfo.Parse<0>(charString.c_str());
        
        bool isCustom = characterInfo.HasMember("base_char");
        if (!characterInfo.HasMember("ccbi") && !isCustom){
            return; // !!!: temp code because character is not ready
        }
        
        _selectedProductType = PRODUCT_TYPE_CHARACTER;
        
        this->setSelectedProduct(characterKey->getCString());
        
        Node *characterItem = Node::create();

        auto zoomScale = Util::getCharacterExtendZoomScale();

        auto characterCcbi = Character::create(characterKey->getCString());
        characterCcbi->setScale(zoomScale * Util::getCharacterScaleFactor());
        characterCcbi->enableExpansionEffect(false);
        
        cocos2d::Size characterSize = characterCcbi->characterObj->getContentSize() * zoomScale * Util::getCharacterScaleFactor();
        
        characterCcbi->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
        characterCcbi->setPosition(cocos2d::Point(characterSize.width/2, characterSize.height/2));
        
        characterItem->setContentSize(characterSize);
        characterItem->addChild(characterCcbi);
        
        auto visibleSize = Director::getInstance()->getVisibleSize();
        
        if (!isCustom) {
            bool isLocked = characterInfo["locked"].GetBool();
            
            if (isLocked) {
                auto coinBtn = TextColorButton::create("home_bg_frame_locked_coin_normal.png", "home_bg_frame_locked_coin_pressed.png");
                coinBtn->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
                coinBtn->setDelegate(this);
                coinBtn->setPosition(cocos2d::Point(characterSize.width+HOME_ITEM_SELECT_COIN_MARGIN.x, HOME_ITEM_SELECT_COIN_MARGIN.y));
                coinBtn->addTouchEventListener(this, cocos2d::ui::SEL_TouchEvent(&Home::bgItemCoinClicked));
                coinBtn->setVisible(false);
                characterItem->addChild(coinBtn, 0, TAG_EXPANDED_ITEM_COIN);
                
                int price = characterInfo["price"].GetInt();
                std::stringstream ss;
                ss << price;
                
                std::string coinValue = ss.str();
                
                int coinFontSize = 0;
                if (coinValue.length() == 1) {
                    coinFontSize = HOME_BG_SELECT_COIN_FONTSIZE_01;
                } else if (coinValue.length() == 2) {
                    coinFontSize = HOME_BG_SELECT_COIN_FONTSIZE_02;
                } else {
                    coinFontSize = HOME_BG_SELECT_COIN_FONTSIZE_03;
                }
                
                auto coinNum = Label::createWithSystemFont(coinValue, FONT_NAME_BOLD, coinFontSize,HOME_BG_SELECT_COIN_TEXT_SIZE,TextHAlignment::CENTER, TextVAlignment::CENTER);
                coinNum->setTag(TAG_BG_SELECT_COIN);
                coinNum->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
                coinNum->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
                coinNum->setColor(Color3B(0xff,0x54,0x11));
                coinNum->enableShadow(Color4B(255,255,255,255.0f*0.51f),cocos2d::Size(1.0,-0.866), 0);
                coinNum->setPosition(cocos2d::Point(coinBtn->getContentSize().width/2, coinBtn->getContentSize().height/2));
                coinBtn->addChild(coinNum);
                
                
                int expiredDay = 0;
                
                if (characterInfo.HasMember("expired_hour")) {
                    auto expiredMin = characterInfo["expired_hour"].GetInt();
                    expiredDay = expiredMin/60/24;
                }
                
                std::string originString;
                
                if(expiredDay == 0) {
                    originString = Device::getResString(Res::Strings::CC_EXPANDEDITEM_NEED_UNLOCK_COIN_CHA);// "Use Dole Coins to unlock\nthe special item."
                } else {
                    auto foramtString = Device::getResString(Res::Strings::CC_EXPANDEDITEM_NEED_UNLOCK_COIN_LIMITED_PERIOD); // "Once unlocked, you will be able to use \nthe item for %d days. "
                    auto result = __String::createWithFormat(foramtString, expiredDay);
                    originString = result->getCString();
                }
                
                std::stringstream stringStream(originString);
                std::string line;
                std::vector<std::string> lines;
                while (std::getline(stringStream, line, '\n')) { // split string by '\n'
                    lines.push_back(line);
                }
                
                int lineCount = (int)lines.size();
                float maxWidth = 0;
                vector<std::string>::iterator itr;
                for(itr = lines.begin(); itr != lines.end(); itr++)
                {
                    auto textWidth = Device::getTextWidth(itr->c_str(), FONT_NAME, HOME_TALK_BOX_FONTSIZE);
                    maxWidth = MAX(maxWidth, textWidth);
                }
                
                float textLimit = (lineCount > 2) ? HOME_TALK_BOX_MAX_WIDTH_LONG : HOME_TALK_BOX_MAX_WIDTH_SHORT;
                float textWidth = MIN(maxWidth, textLimit);
                
                float textHeight = 0;
#ifdef MACOS
                textHeight = 58;
#endif
                auto textLabel = LabelExtension::create(originString, FONT_NAME, HOME_TALK_BOX_FONTSIZE, cocos2d::Size(textWidth, textHeight),
                                                        TextHAlignment::LEFT, TextVAlignment::BOTTOM);
                float textAreaHeight = textLabel->getContentSize().height;
                
                textLabel->setColor(Color3B(0xff,0xff,0xff));
                textLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                textLabel->setPosition(cocos2d::Point(HOME_TALK_BOX_SIDE_MARGIN, HOME_TALK_BOX_BOTTOM_MARGIN));
                textLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
                
                cocos2d::Rect talkBoxInset = cocos2d::Rect(HOME_ITEM_TALKBOX_INSET_LEFT_TAIL.x, HOME_ITEM_TALKBOX_INSET_LEFT_TAIL.y,
                                                           HOME_ITEM_TALKBOX_INSET_LEFT_TAIL_SIZE.width, HOME_ITEM_TALKBOX_INSET_LEFT_TAIL_SIZE.height);
                auto talkBox = Scale9Sprite::create(talkBoxInset, "home_character_lock_talk_box_bg.png");
                
                auto talkBoxSize = cocos2d::Size(textWidth + HOME_TALK_BOX_SIDE_MARGIN * 2, textAreaHeight + HOME_TALK_BOX_BOTTOM_MARGIN + HOME_TALK_BOX_TOP_MARGIN);
                
                talkBox->setContentSize(talkBoxSize);
                talkBox->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                talkBox->setPosition(cocos2d::Point(coinBtn->getPositionX() - coinBtn->getContentSize().width + HOME_ITEM_TALKBOX.x,
                                                    coinBtn->getPositionY() + coinBtn->getContentSize().height + HOME_ITEM_TALKBOX.y));
                talkBox->setVisible(false);
                characterItem->addChild(talkBox, 0, TAG_EXPANDED_ITEM_TALKBOX);
                
                talkBox->addChild(textLabel, 0, TAG_ITEM_TALKBOX_LABEL);

            } else {
                if (_productList) {
                    const char* productName = this->getSelectedProductName();
                    ssize_t count = (_productList ? _productList->count() : 0);
                    DoleProduct* product = nullptr;
                    for (ssize_t index = 0; index<count; index++) {
                        product = (DoleProduct*)_productList->getObjectAtIndex(index);
                        const char *code = product->getProductCode();
                        if (product && strcmp(code, productName) == 0 ) {
                            auto isPurchased = product->getPurchase();
                            if (isPurchased)  {
                                if (product->getExpiredHour() != 0) {
                                    log("description : %s",product->description());
                                    auto registerTimeChar = product->getRegisterDateTime();
                                    log("registerTime : %s", registerTimeChar);
                                    
                                    auto leftTime = Util::getItemLeftTime(registerTimeChar,product->getExpiredHour());
                                    auto day = (leftTime/60/60)/24;
                                    auto hour = leftTime/60/60;
                                    auto minute = leftTime/60;
                                    
                                    std::string bgfileName;
                                    std::string leftTimeStr;
                                    std::string nameLabelStr;
                                    Color3B numcolor;
                                    Color3B namecolor;
                                    if (day >= 1) {
                                        if (hour == 23 && minute > 30) hour = 24;
                                        
                                        stringstream daystream;
                                        daystream << day;
                                        std::string dayStr = daystream.str();
                                        leftTimeStr = dayStr;
                                        
                                        std::string fileName("home_bg_frame_limited_days.png");
                                        bgfileName = fileName;
                                        
                                        std::string nameStr = Device::getResString(Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_DAYS);//"days";
                                        nameLabelStr = nameStr;
                                        
                                        numcolor = Color3B(0x03, 0x3e, 0xa1);
                                        namecolor = Color3B(0x2a, 0x64, 0xc5);
                                    } else {
                                        if (hour < 30) hour = 1;
                                        
                                        stringstream hourtream;
                                        hourtream << hour;
                                        std::string hourStr = hourtream.str();
                                        leftTimeStr = hourStr;
                                        
                                        std::string fileName("home_bg_frame_limited_hours.png");
                                        bgfileName = fileName;
                                        
                                        std::string nameStr;
                                        if (hour == 1) {
                                            nameStr = Device::getResString(Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_HOUR);
                                        } else {
                                            nameStr = Device::getResString(Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_HOURS);//"hours";
                                        }
                                        
                                        nameLabelStr = nameStr;
                                        
                                        numcolor = Color3B(0xff, 0x00, 0x00);
                                        namecolor = Color3B(0xff, 0x24, 0x00);
                                    }
                                    
                                    float timeFontSize = (leftTimeStr.length() > 1)?HOME_DIM_LIMIT_TIME_FONTSIZE_01:HOME_DIM_LIMIT_TIME_FONTSIZE_02;
                                    
                                    auto limitBackground = Sprite::create(bgfileName);
                                    limitBackground->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
                                    limitBackground->setPosition(cocos2d::Point(characterSize.width+HOME_ITEM_SELECT_COIN_MARGIN.x, HOME_ITEM_SELECT_COIN_MARGIN.y));
                                    limitBackground->setVisible(false);
                                    characterItem->addChild(limitBackground,0,TAG_EXPANDED_ITEM_COIN);
                                    
                                    auto timeLabel = Label::createWithSystemFont(leftTimeStr, FONT_NAME_BOLD, timeFontSize,HOME_DIM_LIMIT_TIME_LABLE_SIZE,TextHAlignment::CENTER, TextVAlignment::CENTER);
                                    timeLabel->setTag(TAG_BG_SELECT_COIN);
                                    timeLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                                    timeLabel->setColor(numcolor);
                                    timeLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
                                    timeLabel->enableShadow(Color4B(255,255,255,255.0f*0.51f),cocos2d::Size(0,-1), 0);
                                    timeLabel->setPosition(cocos2d::Point(limitBackground->getContentSize().width/2 - timeLabel->getContentSize().width/2,HOME_DIM_LIMIT_TIME_Y));
                                    limitBackground->addChild(timeLabel);
                                    
                                    auto nameTextLabel = Label::createWithSystemFont(nameLabelStr, FONT_NAME_BOLD,HOME_DIM_LIMIT_NAME_LABEL_FONTSIZE, HOME_DIM_LIMIT_NAME_LABEL_SIZE,TextHAlignment::CENTER,TextVAlignment::CENTER);
                                    nameTextLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                                    nameTextLabel->setColor(namecolor);
                                    nameTextLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
                                    nameTextLabel->enableShadow(Color4B(255,255,255,255.0f*0.51f),cocos2d::Size(0,-1), 0);
                                    nameTextLabel->setPosition(cocos2d::Point(limitBackground->getContentSize().width/2 - nameTextLabel->getContentSize().width/2,HOME_DIM_LIMIT_NAME_LABEL_Y));
                                    limitBackground->addChild(nameTextLabel);
                                    
                                    std::string originString = Device::getResString(Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD);
                                    if (Application::getInstance()->getCurrentLanguage() == LanguageType::ENGLISH) {
                                        originString = __String::createWithFormat(originString.c_str(), day)->getCString();
                                    }
                                    
                                    std::stringstream stringStream(originString);
                                    std::string line;
                                    std::vector<std::string> lines;
                                    while (std::getline(stringStream, line, '\n')) { // split string by '\n'
                                        lines.push_back(line);
                                    }
                                    
                                    int lineCount = (int)lines.size();
                                    float maxWidth = 0;
                                    vector<std::string>::iterator itr;
                                    for(itr = lines.begin(); itr != lines.end(); itr++)
                                    {
                                        auto textWidth = Device::getTextWidth(itr->c_str(), FONT_NAME, HOME_TALK_BOX_FONTSIZE);
                                        maxWidth = MAX(maxWidth, textWidth);
                                    }
                                    
                                    float textLimit = (lineCount > 2) ? HOME_TALK_BOX_MAX_WIDTH_LONG : HOME_TALK_BOX_MAX_WIDTH_SHORT;
                                    float textWidth = MIN(maxWidth, textLimit);
                                    
                                    float textHeight = 0;
#ifdef MACOS
                                    textHeight = 58;
#endif
                                    auto textLabel = LabelExtension::create(originString, FONT_NAME, HOME_TALK_BOX_FONTSIZE, cocos2d::Size(textWidth, textHeight),
                                                                            TextHAlignment::LEFT, TextVAlignment::BOTTOM);
                                    float textAreaHeight = textLabel->getContentSize().height;
                                    
                                    textLabel->setColor(Color3B(0xff,0xff,0xff));
                                    textLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                                    textLabel->setPosition(cocos2d::Point(HOME_TALK_BOX_SIDE_MARGIN, HOME_TALK_BOX_BOTTOM_MARGIN));
                                    textLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
                                    
                                    cocos2d::Rect talkBoxInset = cocos2d::Rect(HOME_ITEM_TALKBOX_INSET_LEFT_TAIL.x, HOME_ITEM_TALKBOX_INSET_LEFT_TAIL.y,
                                                                               HOME_ITEM_TALKBOX_INSET_LEFT_TAIL_SIZE.width, HOME_ITEM_TALKBOX_INSET_LEFT_TAIL_SIZE.height);
                                    auto talkBox = Scale9Sprite::create(talkBoxInset, "home_character_lock_talk_box_bg.png");
                                    
                                    auto talkBoxSize = cocos2d::Size(textWidth + HOME_TALK_BOX_SIDE_MARGIN * 2, textAreaHeight + HOME_TALK_BOX_BOTTOM_MARGIN + HOME_TALK_BOX_TOP_MARGIN);
                                    
                                    talkBox->setContentSize(talkBoxSize);
                                    talkBox->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                                    talkBox->setPosition(cocos2d::Point(characterSize.width+HOME_ITEM_SELECT_COIN_MARGIN.x -(limitBackground->getContentSize().width-HOME_ITEM_TALKBOX.x), limitBackground->getPositionY() + limitBackground->getContentSize().height + HOME_ITEM_TALKBOX.y));
                                    talkBox->setVisible(false);
                                    characterItem->addChild(talkBox, 0, TAG_EXPANDED_ITEM_TALKBOX);
                                    
                                    talkBox->addChild(textLabel);
                                }
                            }//ispurchased
                            break;
                        } else {
                            product = nullptr;
                        }
                    }
                }
            }
        }

        auto positionInScreen = selectedItem->convertToWorldSpace(cocos2d::Point::ZERO);
        auto widthRatio = selectedItem->getContentSize().width/characterSize.width;
        
//        auto heightRatio = selectedItem->getContentSize().height /characterSize.height;
//        log("ratio : %f, %f", widthRatio, heightRatio);
        
        auto startPosition = cocos2d::Point(positionInScreen.x + selectedItem->getContentSize().width/2,
                                            positionInScreen.y + selectedItem->getContentSize().height/2 );
        
//        characterItem->setScale(widthRatio, heightRatio);
        characterItem->setScale(widthRatio);
        characterItem->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
        characterItem->setPosition(startPosition);
        characterItem->setTag(TAG_EXPANDED_ITEM_PRODUCT);
        
        auto dimLayer = DimLayer::create(Color4B(0x00, 0x00, 0x00, 0xff*0.75), visibleSize.width, visibleSize.height, DIM_TYPE_CHAR);
        dimLayer->setTag(TAG_DIM);
        dimLayer->setDelegate(this);
        dimLayer->setAnchorPoint(cocos2d::Point::ZERO);
        dimLayer->setPosition(cocos2d::Point::ZERO);
        this->addChild(dimLayer);
        dimLayer->addChild(characterItem);
        
        auto dimAniFinished = CallFunc::create([=](){
            auto coin = characterItem->getChildByTag(TAG_EXPANDED_ITEM_COIN);
            if (coin) {
                coin->setScale(0.1f);
                coin->setVisible(true);
                changePositionAtAnchor(coin, cocos2d::Point::ANCHOR_MIDDLE);
                auto scaleAnimation = ScaleTo::create(0.2f, 1.0f);
                coin->runAction(scaleAnimation);
            }
            
            auto talkBox = characterItem->getChildByTag(TAG_EXPANDED_ITEM_TALKBOX);
            if (talkBox) {
                talkBox->setScale(0.1f);
                talkBox->setVisible(true);
                changePositionAtAnchor(talkBox, cocos2d::Point::ANCHOR_MIDDLE);
                auto scaleAnimation = ScaleTo::create(0.2f, 1.0f);
                talkBox->runAction(scaleAnimation);
            }
            
            dimLayer->enableDelete();
        });
        
        float chainDuration = 0.5f;
        auto scaleTo = ScaleTo::create(chainDuration, 1.0);
        cocos2d::Point destPoint = cocos2d::Point( visibleSize.width/2, visibleSize.height/2);
        
        auto moveTo = MoveTo::create(chainDuration, destPoint);
        auto chain = Spawn::create(scaleTo, moveTo, NULL);
        auto sequence = Sequence::create(chain, dimAniFinished, NULL);
        
        auto scaleToReverse = ScaleTo::create(chainDuration, widthRatio, widthRatio);
        auto moveToReverse = MoveTo::create(chainDuration, startPosition);
        auto chainReverse = Spawn::create(scaleToReverse, moveToReverse, NULL);
        characterItem->setUserObject(chainReverse);
        characterItem->runAction(sequence);
    }
    
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Bigger.wav", false);
}

void Home::onSelectBackground(BackgroundScrollView* view, Node* selectedItem, __String* backgroundKey, int index)
{
    if (bgItemClicked) return;

    auto scaleFactor = Util::getCharacterScaleFactor();
    
    bgItemClicked = true;
    Sprite *frame = (Sprite *)selectedItem;
    log("frame pressed : %d key : %s", index, backgroundKey->getCString());
    
    _selectedProductType = PRODUCT_TYPE_BACKGROUND;
    this->setSelectedProduct(backgroundKey->getCString());

    auto ud = UserDefault::getInstance();
    auto bgString = ud->getStringForKey(backgroundKey->getCString());
    rapidjson::Document backgroundInfo;
    backgroundInfo.Parse<0>(bgString.c_str());
    
    auto cloneFrame = Layer::create();
    bool isCustom = backgroundInfo["custom"].GetBool();
    
    const char *bgFileName = nullptr;
    __String *bgPhotoImage = nullptr;
    if (isCustom) {
        auto bgPhoto = (char *)backgroundInfo["main_photo_file"].GetString();
        bgPhotoImage = __String::create(bgPhoto);
        auto borderBg = "home_background_select_custom.png";
        bgFileName = borderBg;
    } else {
        bgFileName = backgroundInfo["select"].GetString();
    }
    
    auto bgImage = Sprite::create(bgFileName);
    cloneFrame->setContentSize(cocos2d::Size(bgImage->getContentSize().width*scaleFactor, bgImage->getContentSize().height*scaleFactor));
    bgImage->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    bgImage->setPosition(cocos2d::Point::ZERO);
    bgImage->setScale(scaleFactor, scaleFactor);
    cloneFrame->addChild(bgImage);
    
    auto hasAni = backgroundInfo.HasMember("ani_ccbi");
    if (hasAni) {
        auto bgID = backgroundInfo["bg_id"].GetInt();
        cocos2d::Point aniItemPos;
        auto aniCcbi = backgroundInfo["ani_ccbi"].GetString();
        
        switch (bgID) {
            case 1:// B001
                aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI01;
                break;
            case 2:// B002
                aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI02;
                break;
            case 4:// B004
                aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI04;
                break;
            case 6:// B006
                aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI06;
                break;
//            case 7:// B007
//                aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI07;
//                break;
            case 8:// B008
                aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI08;
                break;
            case 9:// B009
                aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI09;
                break;
//            case 10:// B010
//                aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI10;
                break;
            case 11:// B011
                aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI11;
                break;
            case 12:// B012
                aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI12;
                break;
            default:
                break;
        }
        
        cocosbuilder::NodeLoaderLibrary *nodeLoaderLibrary = cocosbuilder::NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
        cocosbuilder::CCBReader *ccbReader = new cocosbuilder::CCBReader(nodeLoaderLibrary);
        Node *ccbiObj = ccbReader->readNodeGraphFromFile(aniCcbi, this);
        auto ccbiSprite = (Sprite*)ccbiObj;
        ccbiSprite->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        ccbiSprite->setPosition(cocos2d::Point(aniItemPos.x/scaleFactor, aniItemPos.y/scaleFactor));
        ccbReader->release();
        
        if (ccbiSprite) {
            bgImage->addChild(ccbiSprite);
        }
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    
    if (!isCustom) {
        bool isLocked = backgroundInfo["locked"].GetBool();
        if (isLocked) {
            int price = backgroundInfo["price"].GetInt();
            
            auto coinBtn = TextColorButton::create("home_bg_frame_locked_coin_normal.png", "home_bg_frame_locked_coin_pressed.png");
            coinBtn->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
            coinBtn->setDelegate(this);
            coinBtn->setPosition(cocos2d::Point((bgImage->getContentSize().width*scaleFactor)+HOME_BG_SELECT_COIN_MARGIN.x, -HOME_BG_SELECT_COIN_MARGIN.y));
            coinBtn->addTouchEventListener(this, cocos2d::ui::SEL_TouchEvent(&Home::bgItemCoinClicked));
            coinBtn->setVisible(false);
            cloneFrame->addChild(coinBtn, 0, TAG_EXPANDED_ITEM_COIN);
            
            std::stringstream ss;
            ss << price;
            
            std::string coinValue = ss.str();
            int coinFontSize = 0;
            if (coinValue.length() == 1) {
                coinFontSize = HOME_BG_SELECT_COIN_FONTSIZE_01;
            } else if (coinValue.length() == 2) {
                coinFontSize = HOME_BG_SELECT_COIN_FONTSIZE_02;
            } else {
                coinFontSize = HOME_BG_SELECT_COIN_FONTSIZE_03;
            }
            
            auto coinNum = Label::createWithSystemFont(coinValue, FONT_NAME_BOLD, coinFontSize,HOME_BG_SELECT_COIN_TEXT_SIZE,TextHAlignment::CENTER, TextVAlignment::CENTER);
            coinNum->setTag(TAG_BG_SELECT_COIN);
            coinNum->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
            coinNum->setColor(Color3B(0xff,0x54,0x11));
            coinNum->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
            coinNum->enableShadow(Color4B(255,255,255,255.0f*0.51f),cocos2d::Size(1.0,-0.866), 0);
            coinNum->setPosition(cocos2d::Point(coinBtn->getContentSize().width/2, coinBtn->getContentSize().height/2));
            coinBtn->addChild(coinNum);
            
            int expiredDay = 0;
            
            if (backgroundInfo.HasMember("expired_hour")) {
                auto expiredMin = backgroundInfo["expired_hour"].GetInt();
                expiredDay = expiredMin/60/24;
            }
            
            std::string originString;
            
            if(expiredDay == 0) {
                originString = Device::getResString(Res::Strings::CC_EXPANDEDBG_NEED_UNLOCK_COIN); //"Use Dole Coins to unlock\nthe special background"
            } else {
                auto foramtString = Device::getResString(Res::Strings::CC_EXPANDEDBG_NEED_UNLOCK_COIN_LIMITED_PERIOD); // "Once unlocked, you will be able to use \nthe background for %d days. "
                auto result = __String::createWithFormat(foramtString, expiredDay);
                originString = result->getCString();
            }

            std::stringstream stringStream(originString);
            std::string line;
            std::vector<std::string> lines;
            while (std::getline(stringStream, line, '\n')) { // split string by '\n'
                lines.push_back(line);
            }
            
            int lineCount = (int)lines.size();
            float maxWidth = 0;
            vector<std::string>::iterator itr;
            for(itr = lines.begin(); itr != lines.end(); itr++)
            {
                auto textWidth = Device::getTextWidth(itr->c_str(), FONT_NAME, HOME_TALK_BOX_FONTSIZE);
                maxWidth = MAX(maxWidth, textWidth);
            }
            
            float textLimit = (lineCount > 2) ? HOME_TALK_BOX_MAX_WIDTH_LONG : HOME_TALK_BOX_MAX_WIDTH_SHORT;
            float textWidth = MIN(maxWidth, textLimit);
            
            float textHeight = 0;
#ifdef MACOS
            textHeight = 58;
#endif
            auto textLabel = LabelExtension::create(originString, FONT_NAME, HOME_TALK_BOX_FONTSIZE, cocos2d::Size(textWidth, textHeight),
                                                         TextHAlignment::LEFT, TextVAlignment::BOTTOM);
            float textAreaHeight = textLabel->getContentSize().height;
            
            textLabel->setColor(Color3B(0xff,0xff,0xff));
            textLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
            textLabel->setPosition(cocos2d::Point(HOME_TALK_BOX_SIDE_MARGIN, HOME_TALK_BOX_BOTTOM_MARGIN));
            textLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
            
            cocos2d::Rect talkBoxInset = cocos2d::Rect(HOME_ITEM_TALKBOX_INSET_RIGHT_TAIL.x, HOME_ITEM_TALKBOX_INSET_RIGHT_TAIL.y,
                                                       HOME_ITEM_TALKBOX_INSET_RIGHT_TAIL_SIZE.width, HOME_ITEM_TALKBOX_INSET_RIGHT_TAIL_SIZE.height);
            auto talkBox = Scale9Sprite::create(talkBoxInset, "home_talk_box_bg.png");
                                
            auto talkBoxSize = cocos2d::Size(textWidth + HOME_TALK_BOX_SIDE_MARGIN * 2, textAreaHeight + HOME_TALK_BOX_BOTTOM_MARGIN + HOME_TALK_BOX_TOP_MARGIN);
                                   
            talkBox->setContentSize(talkBoxSize);
            talkBox->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
            talkBox->setPosition(cocos2d::Point(cloneFrame->getContentSize().width + HOME_TALK_BOX.x, HOME_TALK_BOX.y));
            talkBox->setVisible(false);
            cloneFrame->addChild(talkBox, 0, TAG_EXPANDED_ITEM_TALKBOX);
            
            talkBox->addChild(textLabel, 0, TAG_ITEM_TALKBOX_LABEL);
        } else {
            if (_productList) {
                const char* productName = this->getSelectedProductName();
                ssize_t count = (_productList ? _productList->count() : 0);
                DoleProduct* product = nullptr;
                for (ssize_t index = 0; index<count; index++) {
                    product = (DoleProduct*)_productList->getObjectAtIndex(index);
                    const char *code = product->getProductCode();
                    if (product && strcmp(code, productName) == 0 ) {
                        auto isPurchased = product->getPurchase();
                        if (isPurchased) {
                            if (product->getExpiredHour() != 0) {
                                log("description : %s",product->description());
                                auto registerTimeChar = product->getRegisterDateTime();
                                log("registerTime : %s", registerTimeChar);
                                
                                auto leftTime = Util::getItemLeftTime(registerTimeChar,product->getExpiredHour());
                                auto day = (leftTime/60/60)/24;
                                auto hour = leftTime/60/60;
                                auto minute = leftTime/60;
                                
                                std::string bgfileName;
                                std::string leftTimeStr;
                                std::string nameLabelStr;
                                Color3B numcolor;
                                Color3B namecolor;
                                if (day >= 1) {
                                    if (hour == 23 && minute > 30) hour = 24;
                                    
                                    stringstream daystream;
                                    daystream << day;
                                    std::string dayStr = daystream.str();
                                    leftTimeStr = dayStr;
                                    
                                    std::string fileName("home_bg_frame_limited_days.png");
                                    bgfileName = fileName;
                                    
                                    std::string nameStr = Device::getResString(Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_DAYS);//"days";
                                    nameLabelStr = nameStr;
                                    
                                    numcolor = Color3B(0x03, 0x3e, 0xa1);
                                    namecolor = Color3B(0x2a, 0x64, 0xc5);
                                } else {
                                    if (hour < 30) hour = 1;
                                    
                                    stringstream hourtream;
                                    hourtream << hour;
                                    std::string hourStr = hourtream.str();
                                    leftTimeStr = hourStr;
                                    
                                    std::string fileName("home_bg_frame_limited_hours.png");
                                    bgfileName = fileName;
                                    
                                    std::string nameStr;
                                    if (hour == 1) {
                                        nameStr = Device::getResString(Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_HOUR);
                                    } else {
                                        nameStr = Device::getResString(Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_HOURS);//"hours";
                                    }
                                    nameLabelStr = nameStr;
                                    
                                    numcolor = Color3B(0xff, 0x00, 0x00);
                                    namecolor = Color3B(0xff, 0x24, 0x00);
                                }
                                
                                float timeFontSize = (leftTimeStr.length() > 1)?HOME_DIM_LIMIT_TIME_FONTSIZE_01:HOME_DIM_LIMIT_TIME_FONTSIZE_02;
                                
                                auto limitBackground = Sprite::create(bgfileName);
                                limitBackground->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
                                limitBackground->setPosition(cocos2d::Point((bgImage->getContentSize().width*scaleFactor)+HOME_BG_SELECT_COIN_MARGIN.x, -HOME_BG_SELECT_COIN_MARGIN.y));
                                limitBackground->setVisible(false);
                                cloneFrame->addChild(limitBackground,0,TAG_EXPANDED_ITEM_COIN);
                                
                                auto timeLabel = Label::createWithSystemFont(leftTimeStr, FONT_NAME_BOLD, timeFontSize,HOME_DIM_LIMIT_TIME_LABLE_SIZE,TextHAlignment::CENTER, TextVAlignment::CENTER);
                                timeLabel->setTag(TAG_BG_SELECT_COIN);
                                timeLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                                timeLabel->setColor(numcolor);
                                timeLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
                                timeLabel->enableShadow(Color4B(255,255,255,255.0f*0.51f),cocos2d::Size(0,-1), 0);
                                timeLabel->setPosition(cocos2d::Point(limitBackground->getContentSize().width/2 - timeLabel->getContentSize().width/2,HOME_DIM_LIMIT_TIME_Y));
                                limitBackground->addChild(timeLabel);
                                
                                auto nameTextLabel = Label::createWithSystemFont(nameLabelStr, FONT_NAME_BOLD,HOME_DIM_LIMIT_NAME_LABEL_FONTSIZE, HOME_DIM_LIMIT_NAME_LABEL_SIZE,TextHAlignment::CENTER,TextVAlignment::CENTER);
                                nameTextLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                                nameTextLabel->setColor(namecolor);
                                nameTextLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
                                nameTextLabel->enableShadow(Color4B(255,255,255,255.0f*0.51f),cocos2d::Size(0,-1), 0);
                                nameTextLabel->setPosition(cocos2d::Point(limitBackground->getContentSize().width/2 - nameTextLabel->getContentSize().width/2,HOME_DIM_LIMIT_NAME_LABEL_Y));
                                limitBackground->addChild(nameTextLabel);
                                
                                std::string originString = Device::getResString(Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD);
                                if (Application::getInstance()->getCurrentLanguage() == LanguageType::ENGLISH) {
                                    originString = __String::createWithFormat(originString.c_str(), day)->getCString();
                                }
                                
                                std::stringstream stringStream(originString);
                                std::string line;
                                std::vector<std::string> lines;
                                while (std::getline(stringStream, line, '\n')) { // split string by '\n'
                                    lines.push_back(line);
                                }
                                
                                int lineCount = (int)lines.size();
                                float maxWidth = 0;
                                vector<std::string>::iterator itr;
                                for(itr = lines.begin(); itr != lines.end(); itr++)
                                {
                                    auto textWidth = Device::getTextWidth(itr->c_str(), FONT_NAME, HOME_TALK_BOX_FONTSIZE);
                                    maxWidth = MAX(maxWidth, textWidth);
                                }
                                
                                float textLimit = (lineCount > 2) ? HOME_TALK_BOX_MAX_WIDTH_LONG : HOME_TALK_BOX_MAX_WIDTH_SHORT;
                                float textWidth = MIN(maxWidth, textLimit);
                                
                                float textHeight = 0;
#ifdef MACOS
                                textHeight = 58;
#endif
                                auto textLabel = LabelExtension::create(originString, FONT_NAME, HOME_TALK_BOX_FONTSIZE, cocos2d::Size(textWidth, textHeight),
                                                                        TextHAlignment::LEFT, TextVAlignment::BOTTOM);
                                float textAreaHeight = textLabel->getContentSize().height;
                                
                                textLabel->setColor(Color3B(0xff,0xff,0xff));
                                textLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                                textLabel->setPosition(cocos2d::Point(HOME_TALK_BOX_SIDE_MARGIN, HOME_TALK_BOX_BOTTOM_MARGIN));
                                textLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
                                
                                cocos2d::Rect talkBoxInset = cocos2d::Rect(HOME_ITEM_TALKBOX_INSET_RIGHT_TAIL.x, HOME_ITEM_TALKBOX_INSET_RIGHT_TAIL.y,
                                                                           HOME_ITEM_TALKBOX_INSET_RIGHT_TAIL_SIZE.width, HOME_ITEM_TALKBOX_INSET_RIGHT_TAIL_SIZE.height);
                                auto talkBox = Scale9Sprite::create(talkBoxInset, "home_talk_box_bg.png");
                                
                                auto talkBoxSize = cocos2d::Size(textWidth + HOME_TALK_BOX_SIDE_MARGIN * 2, textAreaHeight + HOME_TALK_BOX_BOTTOM_MARGIN + HOME_TALK_BOX_TOP_MARGIN);
                                
                                talkBox->setContentSize(talkBoxSize);
                                talkBox->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
                                talkBox->setPosition(cocos2d::Point(cloneFrame->getContentSize().width + HOME_TALK_BOX.x,
                                                                    HOME_TALK_BOX.y + HOME_TALK_BOX_PURCHASE_BOTTOM_PADDING));
                                talkBox->setVisible(false);
                                cloneFrame->addChild(talkBox, 0, TAG_EXPANDED_ITEM_TALKBOX);
                                
                                talkBox->addChild(textLabel);
                            }
                            
                        }
                        break;
                    } else {
                        product = nullptr;
                    }
                    
                    
                }
            } //_productList
        }
    } else {
        if (bgPhotoImage != nullptr) {
            std::string photoType("bg");
            std::string path = Util::getMainPhotoPath(photoType);
            path.append(bgPhotoImage->getCString());
            if (cocos2d::FileUtils::getInstance()->isFileExist(path) ){
                
                cocos2d::Data imageData = cocos2d::FileUtils::getInstance()->getDataFromFile(path.c_str());
                unsigned long nSize = imageData.getSize();
                
                cocos2d::Image *image = new cocos2d::Image();
                image->initWithImageData(imageData.getBytes(), nSize);
                
                cocos2d::Texture2D *texture = new cocos2d::Texture2D();
                texture->initWithImage(image);
                
                auto picture = Sprite::createWithTexture(texture);
                picture->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                float scaleFactor = Util::getCharacterScaleFactor();
                picture->setPosition(cocos2d::Point(HOME_CUSTOM_BG_BIG.x/scaleFactor,HOME_CUSTOM_BG_BIG.y/scaleFactor));
                bgImage->addChild(picture);
            } else {
                log("error! no file : %s", path.c_str());
            }
        }
    }
    
    auto positionInScreen = frame->convertToWorldSpace(cocos2d::Point::ZERO);
    //    log("convert position : %f,%f", positionInScreen.x, positionInScreen.y);
    
    auto widthRatio = frame->getContentSize().width/cloneFrame->getContentSize().width;
    auto heightRatio = frame->getContentSize().height/cloneFrame->getContentSize().height;
    
    auto rotationAngle = 0.0f;
    switch (index%5) {
        case 0:case 2:case 3:
            rotationAngle = rotationAngle*-1;
            break;
        case 1:case 4:
            // nothing to do
            break;
        default:
            break;
    }
    auto startPosition = cocos2d::Point(positionInScreen.x, frame->getPosition().y-(frame->getContentSize().height*frame->getAnchorPoint().y) );
    
    cloneFrame->setScale(widthRatio, heightRatio);
    cloneFrame->setRotation(rotationAngle);
    cloneFrame->setAnchorPoint(cocos2d::Point::ZERO);
    cloneFrame->setPosition(startPosition);
    cloneFrame->setTag(TAG_EXPANDED_ITEM_PRODUCT);
    
    auto dimLayer = DimLayer::create(Color4B(0x00, 0x00, 0x00, 0xff*0.6), visibleSize.width, visibleSize.height, DIM_TYPE_BG);
    dimLayer->setTag(TAG_DIM);
    dimLayer->setDelegate(this);
    dimLayer->setAnchorPoint(cocos2d::Point::ZERO);
    dimLayer->setPosition(cocos2d::Point::ZERO);
    this->addChild(dimLayer);
    dimLayer->addChild(cloneFrame);
    
    auto dimAniFinished = CallFunc::create([=](){
        auto coin = cloneFrame->getChildByTag(TAG_EXPANDED_ITEM_COIN);
        if (coin) {
            coin->setScale(0.1f);
            coin->setVisible(true);
            changePositionAtAnchor(coin, cocos2d::Point::ANCHOR_MIDDLE);
            auto scaleAnimation = ScaleTo::create(0.2f, 1.0f);
            coin->runAction(scaleAnimation);
        }

        auto talkBox = cloneFrame->getChildByTag(TAG_EXPANDED_ITEM_TALKBOX);
        if (talkBox) {
            talkBox->setScale(0.1f);
            talkBox->setVisible(true);
            changePositionAtAnchor(talkBox, cocos2d::Point::ANCHOR_MIDDLE);
            auto scaleAnimation = ScaleTo::create(0.2f, 1.0f);
            talkBox->runAction(scaleAnimation);
        }
        
        dimLayer->enableDelete();
    });
    
    float chainDuration = 0.5f;
    auto scaleTo = ScaleTo::create(chainDuration, 1.0);
    auto rotateTo = RotateTo::create(chainDuration, 0);
    auto moveTo = MoveTo::create(chainDuration, cocos2d::Point( (visibleSize.width-cloneFrame->getContentSize().width)/2,
                                                               (visibleSize.height-cloneFrame->getContentSize().height)/2));
    auto chain = Spawn::create(rotateTo, scaleTo, moveTo, NULL);
    auto sequence = Sequence::create(chain, dimAniFinished, NULL);
    
    auto scaleToReverse = ScaleTo::create(chainDuration, widthRatio, heightRatio);
    auto rotateToReverse = RotateTo::create(chainDuration, rotationAngle);
    auto moveToReverse = MoveTo::create(chainDuration, startPosition);
    auto chainReverse = Spawn::create(scaleToReverse, rotateToReverse, moveToReverse, NULL);
    cloneFrame->setUserObject(chainReverse);
    
    cloneFrame->runAction(sequence);
    
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Bigger.wav");
    
//    auto frame = (Sprite *)selectedItem;
//    float scaleDuration = 0.05f;
//    float shrinkRatio = 0.97;
//    auto scaleAction = ScaleTo::create(scaleDuration, shrinkRatio);
//    auto reverse = ScaleTo::create(scaleDuration, 1.0f);
//    auto afterCallBack = CallFuncN::create(CC_CALLBACK_1(Home::framePressedCallback, this));
//    auto actions = Sequence::create(scaleAction, reverse, afterCallBack, NULL);
//    frame->runAction(actions);
}

Sprite* Home::getCcbiSprite(const char* ccbiName, cocos2d::Point position)
{
    cocosbuilder::NodeLoaderLibrary *nodeLoaderLibrary = cocosbuilder::NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
    cocosbuilder::CCBReader *ccbReader = new cocosbuilder::CCBReader(nodeLoaderLibrary);
    Node *ccbiObj = ccbReader->readNodeGraphFromFile(ccbiName, this);
    auto ccbiSprite = (Sprite*)ccbiObj;
    ccbiSprite->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    ccbiSprite->setPosition(position);
    ccbReader->release();
    return ccbiSprite;
}

void Home::onEnter()
{
    Layer::onEnter();
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(false);
    
    listener->onTouchBegan = [=](Touch* touch, Event* event) {
        log("touch began...");
        return true;
    };
    
    listener->onTouchEnded = [=](Touch* touch, Event* event){
        if (menuListLayer->menuListVisible) {
            menuListLayer->menuListControl(false);
        }
        if ( this->isScrollViewVisible() ) {
            auto target = event->getCurrentTarget();
            cocos2d::Point locationInNode = target->convertToNodeSpace(touch->getLocation());
            cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
            cocos2d::Rect upperRect = cocos2d::Rect(0, HOME_ITEM_SCROLL_HEIGHT, visibleSize.width, visibleSize.height-HOME_ITEM_SCROLL_HEIGHT);
            
            if (upperRect.containsPoint(locationInNode)) {
                log("touched");
                
                if (openBgItem) {
                    auto child = mainLayer->getChildByTag(TAG_BTN_SELECT_BACKGROUND);
                    if(child != nullptr) {
                        auto grandChild = child->getChildByTag(MENU_BG);
                        MenuItemImage* button = dynamic_cast<MenuItemImage*>(grandChild);
                        if(button != nullptr && !scrollViewToggleAnimation) {
                            this->hideBottomScrollView();

                            auto img = Sprite::create("btn_home_bg_normal.png");
                            this->setEnableButtons(true);
                            
                            openBgItem = !openBgItem;
                            button->setNormalImage(img);
                            button->setEnabled(true);
                        }
                    }
                } else if(openCharacterItem) {
                    auto child = mainLayer->getChildByTag(TAG_BTN_SELECT_BACKGROUND);
                    if(child != nullptr) {
                        auto grandChild = child->getChildByTag(MENU_ITEM);
                        MenuItemImage* button = dynamic_cast<MenuItemImage*>(grandChild);
                        if(button != nullptr && !scrollViewToggleAnimation) {
                            
                            this->hideBottomScrollView();
                            auto img = Sprite::create("btn_home_item_normal.png");
                            this->setEnableButtons(true);
                            
                            openCharacterItem = !openCharacterItem;
                            button->setNormalImage(img);
                            button->setEnabled(true);
                        }
                    }
                }
            }
        }
    };
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    // clear touches vector in scrollview to prevent zooming on scroll view
    bookScrollView->setTouchEnabled(false);
    bookScrollView->setTouchEnabled(true);

    auto keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyReleased = CC_CALLBACK_2(Home::onKeyReleased, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(keyboardListener, this);

//	this->dolesAccount();
    isBookAnimation = false;
    movingScene = false;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    updateDoleCoinAndroid();

    AppDelegate* app = (AppDelegate*)Application::getInstance();
    if(app->needCheckUpdate()) {
    	getUpdateVersion();
    }

    if(app->getPendingGuideHome()) {
    	showGuide();
    }
#endif
    
    if (!isAddingBookAnimation) {
        this->showBookSideCharacter(bookScrollView->getContentOffset().x);
        
        if (selectedBookKey != nullptr){
            selectedBookKey = nullptr;
        }
    }
}

void Home::onExit()
{
    _eventDispatcher->removeEventListenersForTarget(this);
    
    float currentOffset = bookScrollView->getContentOffset().x;
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();

    // hide all character
    this->hideBookSideCharacter(currentOffset, currentOffset-visibleSize.width);

    Layer::onExit();
}

void Home::onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	if(keyCode == cocos2d::EventKeyboard::KeyCode::KEY_BACKSPACE) {
		
        if(getChildByTag(TAG_GUIDE_LAYER) != NULL) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			removeChildByTag(TAG_GUIDE_LAYER);
#endif
		} else if(getChildByTag(TAG_POPUP_CRITICAL_UPDATE) != NULL) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			removeChildByTag(TAG_POPUP_CRITICAL_UPDATE);
#endif
		} else if(getChildByTag(TAG_POPUP_COINLOGIN) != NULL) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			removeChildByTag(TAG_POPUP_COINLOGIN);
#endif
		} else if(getChildByTag(TAG_POPUP_UNLOCKITEM) != NULL) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			removeChildByTag(TAG_POPUP_UNLOCKITEM);
#endif
		} else if(getChildByTag(TAG_POPUP_ERRORCOIN) != NULL) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			removeChildByTag(TAG_POPUP_ERRORCOIN);
#endif
		} else if(getChildByTag(TAG_POPUP_NEEDLOGIN) != NULL) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			removeChildByTag(TAG_POPUP_NEEDLOGIN);
#endif
		} else if(getChildByTag(POPUP_TEST) != NULL) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			removeChildByTag(POPUP_TEST);
#endif
		} else if(getChildByTag(DELETE_BOOK_POPUP) != NULL) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		    Director::getInstance()->resume();
			removeChildByTag(DELETE_BOOK_POPUP);
#endif
		} else if(getChildByTag(TAG_POPUP_NONE) != NULL) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			removeChildByTag(TAG_POPUP_NONE);
#endif
		} else if(isPlatformShown) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			AppDelegate* app = (AppDelegate*)Application::getInstance();
			isPlatformShown = !app->hidePlatformView();
#endif
		} else if(menuListLayer->menuListVisible) {
			menuListLayer->menuListControl(false);
		} else if(isCharacterSelected) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			DimLayer* dim = (DimLayer*)getChildByTag(TAG_DIM);
			if(dim != nullptr) {
				dim->removeItem();
			}
#endif
			isCharacterSelected = false;
		} else if(bgItemClicked) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			DimLayer* dim = (DimLayer*)getChildByTag(TAG_DIM);
			if(dim != nullptr) {
				dim->removeItem();
			}
#endif
			bgItemClicked = false;
		} else if (openBgItem) {
            this->hideBottomScrollView();
            auto child = mainLayer->getChildByTag(TAG_BTN_SELECT_BACKGROUND);
            if(child != nullptr) {
				auto grandChild = child->getChildByTag(MENU_BG);
                MenuItemImage* button = dynamic_cast<MenuItemImage*>(grandChild);
                if(button != nullptr) {
					auto img = Sprite::create("btn_home_bg_normal.png");
					this->setEnableButtons(true);
                    openBgItem = !openBgItem;
                    button->setNormalImage(img);
					button->setEnabled(true);
				}
			}
		} else if(openCharacterItem) {
			this->hideBottomScrollView();
			auto child = mainLayer->getChildByTag(TAG_BTN_SELECT_BACKGROUND);
			if(child != nullptr) {
				auto grandChild = child->getChildByTag(MENU_ITEM);
				MenuItemImage* button = dynamic_cast<MenuItemImage*>(grandChild);
				if(button != nullptr) {
					auto img = Sprite::create("btn_home_item_normal.png");
					this->setEnableButtons(true);
					openCharacterItem = !openCharacterItem;
					button->setNormalImage(img);
					button->setEnabled(true);
                }
            }
		} else {
			menuCloseCallback(nullptr);
		}
	}
}

#pragma mark - Account & Dole Coin
void Home::runDoleCoinView(bool animated) {
    if (menuListLayer->menuListVisible) {
        menuListLayer->menuListControl(false);
    }
    
//    TSDoleNetProgressLayer *progressLayer = TSDoleNetProgressLayer::create();
//    this->addChild(progressLayer);
//    return;
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
//    {
////        UIViewController *viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
////        NSLog(@"viewController screen frame : %@",NSStringFromCGRect(viewController.view.frame));
////        TSPopupViewController *popupViewController = [[TSPopupViewController alloc] initWithPopupMode:TSPopupTypePassword userData:[NSNumber numberWithInt:600]];
////        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
////            popupViewController.modalPresentationStyle = UIModalPresentationFormSheet;
////        }
////        [viewController presentViewController:popupViewController animated:YES completion:nil];
////        [popupViewController release];
//        
//        UIViewController *viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
//        NSLog(@"viewController screen frame : %@",NSStringFromCGRect(viewController.view.frame));
//        
//        TSPopupViewController *popupViewController =
//        [[TSPopupViewController alloc] initWithPopupMode:TSPopupTypeShareComment userData:@{@"booktitle": @"tuilisebook", @"authorname": @"tuilise"}];
//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//            popupViewController.modalPresentationStyle = UIModalPresentationFormSheet;
//        }
//        [viewController presentViewController:popupViewController animated:NO completion:nil];
//        [popupViewController release];
//        return;
//    }
//#endif
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	AppDelegate* app = (AppDelegate*)Application::getInstance();
	if(app != nullptr) {
		app->showPlatformView(ANDROID_DOLE_COIN, TAG_HOME);
	}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#ifdef UI_TYPE_IOS8
    TSDoleCoinInfoViewController *doleCoinVC = [[TSDoleCoinInfoViewController alloc] init];
    doleCoinVC.originLayer = TSOriginLayerHome;
    [UIViewController replaceRootViewControlller:doleCoinVC];
    [doleCoinVC release];
#else//UI_TYPE_IOS8
    BOOL isAnimated = YES;
    if (animated == false) {
        isAnimated = NO;
    }
    UIViewController *viewController = nil;
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
    } else {
        viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
    }
    NSLog(@"viewController screen frame : %@",NSStringFromCGRect(viewController.view.frame));
    TSDoleCoinViewController *doleCoinViewController = [[TSDoleCoinViewController alloc] init];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        doleCoinViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    doleCoinViewController.originLayer = TSOriginLayerHome;
    if(IS_IOS8 && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        doleCoinViewController.preferredContentSize = CGSizeMake(1024, 768);
    }
    [viewController presentViewController:doleCoinViewController animated:isAnimated completion:nil];
    [doleCoinViewController release];
#endif//UI_TYPE_IOS8
#endif
}

void Home::runAccountView(bool firstActive) {
    bool firstRun = UserDefault::getInstance()->getBoolForKey(UDKEY_BOOL_APPFIRSTRUN, true);
    if (firstRun == true) {
        UserDefault::getInstance()->setBoolForKey(UDKEY_BOOL_APPFIRSTRUN, false);
        UserDefault::getInstance()->setFloatForKey(UDKEY_FLOAT_VOLUME, DEFAULT_VOLUME_BACKGROUND_SOUND);
        UserDefault::getInstance()->setBoolForKey(UDKEY_BOOL_MUTEBGM, false);
        UserDefault::getInstance()->flush();
        TSBGMManager::getInstance()->play();
    } else if (firstActive == true) {
        return;
    }

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	AppDelegate* app = (AppDelegate*)Application::getInstance();
	if(app != nullptr) {
		app->showPlatformView(ANDROID_LOGIN, TAG_HOME);
	}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
#ifdef UI_TYPE_IOS8
    TSAccountLoginViewController *loginVC = [[TSAccountLoginViewController alloc] init];
    loginVC.appFirstRun = firstRun;
    loginVC.originLayer = TSOriginLayerHome;
    [UIViewController replaceRootViewControlller:loginVC];
    [loginVC release];
#else//UI_TYPE_IOS8
    UIViewController *viewController = nil;
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
    } else {
        viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
    }
    NSLog(@"viewController screen frame : %@",NSStringFromCGRect(viewController.view.frame));
    TSAccountViewController *accountViewController = [[TSAccountViewController alloc] initWithFirstRun:firstRun];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        accountViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    accountViewController.originLayer = TSOriginLayerHome;
    if(IS_IOS8 && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        accountViewController.preferredContentSize = CGSizeMake(1024, 768);
    }
    [viewController presentViewController:accountViewController animated:YES completion:nil];
    [accountViewController release];
#endif//UI_TYPE_IOS8
    
#endif
}

//#import "TSAccountSNSSignupViewController.h"
void Home::runSettingView() {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#ifdef UI_TYPE_IOS8
//    TSAccountSNSSignupViewController *snsSignupSV = [[TSAccountSNSSignupViewController alloc] init];
//    snsSignupSV.loginForFacebookData =
//    @{@"UserID" : @"dummyfb0003@abc.com",
//        @"UserGender" : @"female",
//        @"id" : @"1491932594370003",//1491932594379834
//        @"name" : @"dummyfb0003" };
//    snsSignupSV.originLayer = TSOriginLayerHome;
//    [UIViewController replaceRootViewControlller:snsSignupSV];
//    return;
//    
    TSSettingMainViewController *settingMainVC = [[TSSettingMainViewController alloc] init];
    settingMainVC.originLayer = TSOriginLayerHome;
    [UIViewController replaceRootViewControlller:settingMainVC];
    [settingMainVC release];
#else//UI_TYPE_IOS8
    TSSettingViewController *settingViewController = [[TSSettingViewController alloc] init];
    settingViewController.originLayer = TSOriginLayerHome;
    [UIViewController replaceRootViewControlller:settingViewController];
//    [viewController presentViewController:settingViewController animated:YES completion:nil];
    [settingViewController release];
#endif//UI_TYPE_IOS8
#endif//CC_PLATFORM_IOS
}

void Home::runPasswordView() {
    if ( _seletedProductName.length() <= 0 ) {
        log("doleExecutePurchase::empty product name!!!!");
        return;
    }
    
    if (_isPurchasReady & PURCHASE_START_WITH_LOGIN) {
        int readyStatus = PURCHASE_START_WITH_LOGIN | PURCHASE_LOGIN_READY | PURCHASE_GETBALANSE_READY | PURCHASE_INVENTORY_READY;
        if (_isPurchasReady != readyStatus) {
            return;
        }
    }
    
    const char* productName = this->getSelectedProductName();
    ssize_t count = (_productList ? _productList->count() : 0);
    if (count == 0) {
        return;
    }
    DoleProduct* product = nullptr;
    for (ssize_t index = 0; index<count; index++) {
        product = (DoleProduct*)_productList->getObjectAtIndex(index);
        const char *code = product->getProductCode();
        if (product && strcmp(code, productName) == 0 ) {
            break;
        } else {
            product = nullptr;
        }
    }
//    CCASSERT(product, __String::createWithFormat("invalid item Name : %s", productName)->getCString());
    if (!product) {
#if COCOS2D_DEBUG
        PopupLayer *errorCoin = PopupLayer::create(POPUP_TYPE_WARNING, "For DEBUG\nProduct Infomation not exist.",
                                                   Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK));
        errorCoin->setTag(TAG_POPUP_NONE);
        //"Not enough Dole Coins.", "Cancel", "Earn Dole Coins"
        this->addChild(errorCoin);
#endif
        return;
    }
    
    int salePrice = product->getSalePrice();
    if (salePrice > _doleCoinBalance) {
        
        PopupLayer *errorCoin = PopupLayer::create(POPUP_TYPE_WARNING, Device::getResString(Res::Strings::CC_EXPANDEDITEM_POPUP_NOT_ENOUGH_COIN),
                                                   Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL),
                                                   Device::getResString(Res::Strings::CC_EXPANDEDITEM_POPUP_BUTTON_TITLE_EARN_COIN));
        //"Not enough Dole Coins.", "Cancel", "Earn Dole Coins"
        errorCoin->setTag(TAG_POPUP_ERRORCOIN);
        errorCoin->setDelegate(this);
        this->addChild(errorCoin);
        return;
    }

    
    __String *message = __String::createWithFormat(Device::getResString(Res::Strings::CC_EXPANDEDITEM_POPUP_WARNING_USE_UNLOCK_COIN), salePrice);//"Unlock with %d Dole Coin.\nThis action cannot be undone"
    PopupLayer *errorCoin = PopupLayer::create(POPUP_TYPE_WARNING, message->getCString(),
                                               Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL),//"Cancel",
                                               Device::getResString(Res::Strings::CC_EXPANDEDITEM_POPUP_COIN_BUTTON_TITLE_UNLOCK));// "Unlock"
    errorCoin->setTag(TAG_POPUP_UNLOCKITEM);
    errorCoin->setDelegate(this);
    this->addChild(errorCoin);
    
//    this->doleExecutePurchase();// password input view call 기능 삭재
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
//    this->doleExecutePurchase();// TODO:임시로 호출함. password input view 호출 필요
//#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
//    UIViewController *viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
//    NSLog(@"viewController screen frame : %@",NSStringFromCGRect(viewController.view.frame));
//    TSPopupViewController *popupViewController = [[TSPopupViewController alloc] initWithPopupMode:TSPopupTypePassword userData:[NSNumber numberWithInt:salePrice]];
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//        popupViewController.modalPresentationStyle = UIModalPresentationFormSheet;
//    }
//    [viewController presentViewController:popupViewController animated:YES completion:nil];
//    [popupViewController release];
//#endif
}

void Home::doleCoinInit() {
    auto coin = ui::Button::create("btn_home_coin.png", "btn_home_coin_press.png");
    coin->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    coin->setPosition(HOME_COIN);
    coin->addTouchEventListener(this, (cocos2d::ui::SEL_TouchEvent)(&Home::touchEventForUIButton));
    coin->setTag(TAG_BTN_DOLECOIN_COIN);
    mainLayer->addChild(coin, 10);
    
    auto btnX = ui::Button::create("btn_home_x.png", "btn_home_x_press.png");
    btnX->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    btnX->setPosition(HOME_X);
    btnX->addTouchEventListener(this, (cocos2d::ui::SEL_TouchEvent)(&Home::touchEventForUIButton));
    btnX->setTag(TAG_BTN_DOLECOIN_X);
    mainLayer->addChild(btnX, 10);
    
   //befor login ******* do not delete *******
    auto coinNumUnknown = ui::Button::create("home_dole_coin_before_login.png", "home_dole_coin_before_login_press.png");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    coinNumUnknown->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_LEFT);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    coinNumUnknown->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
#endif
    coinNumUnknown->setPosition(HOME_COIN_NUM);
    coinNumUnknown->addTouchEventListener(this, cocos2d::ui::SEL_TouchEvent(&Home::touchEventForUIButton));
    coinNumUnknown->setTag(TAG_BTN_DOLECOIN_UNKNOWN);
    mainLayer->addChild(coinNumUnknown, 10);
}

void Home::touchEventForUIButton(Ref *pSender, ui::TouchEventType type)
{
//    if (isBackgroundScrollViewVisible || isCharacterScrollViewVisible) return;
    
    switch (type) {
        case ui::TOUCH_EVENT_ENDED: {
            std::string authKey = UserDefault::getInstance()->getStringForKey(UDKEY_STRING_AUTHKEY);
            if (authKey.length() > 0 ) {// login 되어 있음.
                this->runDoleCoinView();
            } else {
                PopupLayer *needLogin = PopupLayer::create(POPUP_TYPE_WARNING,
                                                           Device::getResString(Res::Strings::CC_HOME_POPUP_NEED_LOGIN),// "Log in to view Dole Coins infomation.",
                                                           Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL),// "cancel",
                                                           Device::getResString(Res::Strings::CC_HOME_POPUP_NEED_LOGIN_BUTTON_TITLE_LOGIN));//"Log in");
                needLogin->setTag(TAG_POPUP_COINLOGIN);
                needLogin->setDelegate(this);
                this->addChild(needLogin);
//                this->runAccountView();
            }
        } break;
        case ui::TOUCH_EVENT_BEGAN: log("Button Touch Down"); break;
        case ui::TOUCH_EVENT_MOVED: log("Button Touch Move"); break;
        case ui::TOUCH_EVENT_CANCELED: log("Button Touch Cancelled"); break;
        default: break;
    }
}

void Home::DoleAccountInitialized() {
    this->doleCacheSyncVersionProtocol();
    std::string authKey = UserDefault::getInstance()->getStringForKey(UDKEY_STRING_AUTHKEY);
    if (authKey.length() > 0 ) {// login 되어 있으니 Authkey를 갱신하자.
        this->doleUpdateAuthKey();//all process
        this->doleCoinGetBalanceProtocol();
    } else {//login 되지 않았거나 signup 되어 있지 않았으니 loginView를 보여 주자
        this->runAccountView(true);
    }
}

void Home::doleUpdateAuthKey() {
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	if(_protocolReNewAuthKey != nullptr) {
		if(_protocolReNewAuthKey->getStatus() == TSProtocolStatus::Finished) {
    CC_SAFE_RELEASE_NULL(_protocolReNewAuthKey);
		} else {
			_protocolReNewAuthKey->setStatus(TSProtocolStatus::Canceled);
		}
	}
//#else
//    CC_SAFE_RELEASE_NULL(_protocolReNewAuthKey);
//#endif
    _protocolReNewAuthKey = new TSDoleGetReNewAuthKey();
    _protocolReNewAuthKey->setDelegate(this);
    _protocolReNewAuthKey->setTag(TAG_NET_RENEWAUTH);
    _protocolReNewAuthKey->init(UserDefault::getInstance()->getIntegerForKey(UDKEY_INTEGER_USERNO),
                                UserDefault::getInstance()->getStringForKey(UDKEY_STRING_AUTHKEY));
    _protocolReNewAuthKey->request();
}

void Home::doleCoinGetBalanceProtocol() {
//    std::string authKey = UserDefault::getInstance()->getStringForKey(UDKEY_STRING_AUTHKEY);
    int userNO = UserDefault::getInstance()->getIntegerForKey(UDKEY_INTEGER_USERNO, -1);
    if (userNO > 0) {
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		if(_protocolGetBalance != nullptr) {
			if(_protocolGetBalance->getStatus() == TSProtocolStatus::Finished) {
				CC_SAFE_RELEASE_NULL(_protocolGetBalance);
			} else {
				_protocolGetBalance->setStatus(TSProtocolStatus::Canceled);
			}
		}
//#else
//        CC_SAFE_RELEASE_NULL(_protocolGetBalance);
//#endif
        _protocolGetBalance = new TSDoleGetBalance();
        _protocolGetBalance->setDelegate(this);
        _protocolGetBalance->setTag(TAG_NET_GETBALANCE);
        _protocolGetBalance->init(UserDefault::getInstance()->getIntegerForKey(UDKEY_INTEGER_USERNO));
        _protocolGetBalance->request();
    } else {
        this->updateDoleCoin(-1);
    }
}

void Home::doleNoticeListProtocol() {
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	if(_protocolNoticeList != nullptr) {
		if(_protocolNoticeList->getStatus() == TSProtocolStatus::Finished) {
    CC_SAFE_RELEASE_NULL(_protocolNoticeList);
		} else {
			_protocolNoticeList->setStatus(TSProtocolStatus::Canceled);
		}
	}
//#else
//	CC_SAFE_RELEASE_NULL(_protocolNoticeList);
//#endif
    _protocolNoticeList = new TSDoleGetEventNoticeList();
    _protocolNoticeList->setDelegate(this);
    _protocolNoticeList->setTag(TAG_NET_NOTICELIST);
    _protocolNoticeList->init();
    _protocolNoticeList->request();
}

void Home::doleGetProductProtocol() {
    DoleAPI::getInstance()->getProductSales(CC_CALLBACK_2(Home::onHttpRequestCompleted, this));
}

void Home::doleCacheSyncVersionProtocol() {
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	if(_protocolCacheVersion != nullptr) {
		if(_protocolCacheVersion->getStatus() == TSProtocolStatus::Finished) {
			CC_SAFE_RELEASE_NULL(_protocolCacheVersion);
		} else {
			_protocolCacheVersion->setStatus(TSProtocolStatus::Canceled);
		}
	}
//#else
//    CC_SAFE_RELEASE_NULL(_protocolCacheVersion);
//#endif
    _protocolCacheVersion = new TSDoleGetCacheSyncVersion();
    _protocolCacheVersion->setDelegate(this);
    _protocolCacheVersion->setTag(TAG_NET_CACHEVERSION);
    _protocolCacheVersion->init(UserDefault::getInstance()->getStringForKey(UDKEY_STRING_AUTHKEY));
    _protocolCacheVersion->request();
}

void Home::doleCheckInventoryListUpdate() {
    if (_productList) {
        this->doleGetInventoryListProtocol();
    } else {
        auto sequence = Sequence::create(DelayTime::create(0.5f), CallFunc::create(CC_CALLBACK_0(Home::doleCheckInventoryListUpdate, this)), NULL);
        this->runAction(sequence);
    }
}

void Home::doleGetInventoryListProtocol() {
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	if(_protocolInventoryList != nullptr) {
		if(_protocolInventoryList->getStatus() == TSProtocolStatus::Finished) {
			CC_SAFE_RELEASE_NULL(_protocolInventoryList);
		} else {
			_protocolInventoryList->setStatus(TSProtocolStatus::Canceled);
		}
	}
//#else
//    CC_SAFE_RELEASE_NULL(_protocolInventoryList);
//#endif
    
    std::string authKey = UserDefault::getInstance()->getStringForKey(UDKEY_STRING_AUTHKEY);
    if (authKey.length() <= 0) {
        log("doleGetInventoryListProtocol::auth key empty!, return");
        return;
    }
    _protocolInventoryList = new TSDoleGetInventoryList();
    _protocolInventoryList->setDelegate(this);
    _protocolInventoryList->setTag(TAG_NET_INVENTORY);
    _protocolInventoryList->init(authKey, _productList);
    _protocolInventoryList->request();
}

void Home::doleExecutePurchase() {
    if ( _seletedProductName.length() <= 0 ) {
        log("doleExecutePurchase::empty product name!!!!");
        return;
    }
    const char* productName = this->getSelectedProductName();
    ssize_t count = (_productList ? _productList->count() : 0);
    if (count == 0) {
        return;
    }
    DoleProduct* product = nullptr;
    for (ssize_t index = 0; index<count; index++) {
        product = (DoleProduct*)_productList->getObjectAtIndex(index);
        const char *code = product->getProductCode();
        if (product && strcmp(code, productName) == 0 ) {
            break;
        } else {
            product = nullptr;
        }
    }
    if (!product) { return; }

//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	if(_protocolExecutePurchase != nullptr) {
		if(_protocolExecutePurchase->getStatus() == TSProtocolStatus::Finished) {
			CC_SAFE_RELEASE_NULL(_protocolExecutePurchase);
		} else {
			_protocolExecutePurchase->setStatus(TSProtocolStatus::Canceled);
		}
	}
//#else
//    CC_SAFE_RELEASE_NULL(_protocolExecutePurchase);
//#endif
    _protocolExecutePurchase = new TSDoleExecutePurchase();
    _protocolExecutePurchase->setDelegate(this);
    _protocolExecutePurchase->setTag(TAG_NET_PURCHASE);
    _protocolExecutePurchase->init(product);
    _protocolExecutePurchase->request();
}

void Home::doleExecuteCancel() {
    DimLayer *dimLayer =  (DimLayer *)this->getChildByTag(TAG_DIM);
    if (dimLayer) {
        Node *product = (Node*)dimLayer->getChildByTag(TAG_EXPANDED_ITEM_PRODUCT);
        if (product) {
            ui::Button *coinButton = (ui::Button*)product->getChildByTag(TAG_EXPANDED_ITEM_COIN);
            if (coinButton) {
                coinButton->setEnabled(true);
            }
        }
    }
}

void Home::onRequestStarted(TSDoleProtocol *protocol, TSNetResult result) {
    switch (protocol->getTag()) {
        case TAG_NET_PURCHASE: {
            TSDoleNetProgressLayer *layer = (TSDoleNetProgressLayer*)this->getChildByTag(TAG_NET_PROGRESS);
            if (layer == nullptr) {
                TSDoleNetProgressLayer *progressLayer = TSDoleNetProgressLayer::create();
                progressLayer->activityStart();
                this->addChild(progressLayer);
            }
        } break;
        default:
            break;
    }
}

void Home::onResponseEnded(TSDoleProtocol *protocol, TSNetResult result) {
    switch (result) {
        case TSNetResult::Success : {
            switch (protocol->getTag()) {
                case TAG_NET_RENEWAUTH: {
                    //AuthKey가 갱신 되었으니 Coin을 얻고 아이템 진열 버전 정보를 가져온다.
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	AppDelegate* app = (AppDelegate*)Application::getInstance();
    app->updateAuthKey(UserDefault::getInstance()->getStringForKey(UDKEY_STRING_AUTHKEY).c_str());
#endif
                    this->doleCheckInventoryListUpdate();
                    this->getUpdateVersion();
                } break;
                case TAG_NET_GETBALANCE: {
                    _doleCoinBalance = ((TSDoleGetBalance*)protocol)->getBalance();
                    updateDoleCoin(_doleCoinBalance);
                    if (_isPurchasReady & PURCHASE_START_WITH_LOGIN) {
                        _isPurchasReady = _isPurchasReady | PURCHASE_GETBALANSE_READY;
                        this->runPasswordView();
                    }
                } break;
                case TAG_NET_PURCHASE: {
                    TSDoleNetProgressLayer *progressLayer = (TSDoleNetProgressLayer *)this->getChildByTag(TAG_NET_PROGRESS);
                    if (progressLayer) {
                        progressLayer->activityStop(Device::getResString(Res::Strings::CC_HOME_POPUP_UNLOCK_ITEM_MESSAGE));//"Congratulations!\nYou received the item.");
                    }
                    _isPurchasReady = 0;
                    this->doleCoinGetBalanceProtocol();
                    this->doleGetInventoryListProtocol();
                    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
                    AppDelegate* app = (AppDelegate*)Application::getInstance();
                    if (_selectedProductType == PRODUCT_TYPE_CHARACTER) {
                        app->purchasedCharacter(this->getSelectedProductName());
                        
                    } else if (_selectedProductType == PRODUCT_TYPE_BACKGROUND) {
                        app->purchasedBackground(this->getSelectedProductName());
                    }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#   ifndef MACOS
#   endif
#endif
                    
                    return;
                } break;
                case TAG_NET_LAST_VERSION: {
                    //서버 정보 update되면 사용
                    auto lastAppVersion = ((TSDoleGetAppVersion*)protocol)->getAppVersion();
                    if (lastAppVersion.empty()) return;
                    
                    appURL = ((TSDoleGetAppVersion*)protocol)->getAppUrl();
                    log("lastAppVersion : %s", lastAppVersion.c_str());
                    log("appURL : %s", appURL.c_str());
                    
                    auto currentAppVersion = Device::getCurrentAppVersion();
                    std::string currentVersion(currentAppVersion);
                    log("current version : %s ",currentVersion.c_str());
                    
                    //test code
//                    std::string lastAppVersion("1.2.3");
//                    std::string appUrlstr("http://www.google.com");
//                    appURL = appUrlstr;
//                    
//                    std::string currentVersion("1.2.3");
                    //test
                    
                    const char *delim = ".";
                    char delimeter = *delim;
                    
                    auto lastVer = this->splitByDelim(lastAppVersion, delimeter);
                    auto currentVer = this->splitByDelim(currentVersion, delimeter);
                    
                    int lastVer01 = std::atoi(((std::string)lastVer[0]).c_str());
                    int lastVer02 = std::atoi(((std::string)lastVer[1]).c_str());
//                  int lastVer03 = std::atoi(((std::string)lastVer[2]).c_str());
                    
                    int currentVer01 = std::atoi(((std::string)currentVer[0]).c_str());
                    int currentVer02 = std::atoi(((std::string)currentVer[1]).c_str());
//                  int currentVer03 = std::atoi(((std::string)currentVer[2]).c_str());
//                    
//                  log("last : %i %i %i cur : %i %i %i", lastVer01, lastVer02, lastVer03, currentVer01, currentVer02, currentVer03);
//                    
                    if (lastVer01 > currentVer01 || lastVer02 > currentVer02) {
                        if (lastVer01 < currentVer01) {
//                            this->showPromotionPopup();
                            return;
                        }
                        
                        PopupLayer *popup = PopupLayer::create(POPUP_TYPE_WARNING, "You need to update\nBobby's Journey to continue.", "OK");
                        popup->setDelegate(this);
                        popup->setTag(TAG_POPUP_CRITICAL_UPDATE);
                        this->addChild(popup);
                    } else {
//                        this->showPromotionPopup();
                    }
                } break;
                case TAG_NET_NOTICELIST: {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
                	TSDoleGetEventNoticeList* eventNoticeList = (TSDoleGetEventNoticeList*)protocol;
					if (eventNoticeList->isEventList()) {
#else
                    if (_protocolNoticeList->isEventList()) {
#endif
                        std::string eventURL = _protocolNoticeList->getEventURL();
                        //URL이 있으니 promotion URL 보여주면 됨
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
                    	AppDelegate* app = (AppDelegate*)Application::getInstance();
                    	if(app != nullptr) {
                    		app->loadPromotionPage(eventURL.c_str());
                    	}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
                        NSString *urlstr = [NSString stringWithUTF8String:eventURL.c_str()];
//                        UIViewController *viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
                        UIViewController *viewController = nil;
                        if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                            viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
                        } else {
                            viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
                        }
                        PromotionViewController *promotionViewContoller = [[PromotionViewController alloc] initWithURL:[NSURL URLWithString:urlstr]];
                        [viewController presentViewController:promotionViewContoller animated:YES completion:nil];
                        [PromotionViewController release];
#endif
                    }
                } break;
                case TAG_NET_CACHEVERSION: {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
                	double newVersion = ((TSDoleGetCacheSyncVersion*)protocol)->getVersion();
#else
                    double newVersion = _protocolCacheVersion->getVersion();
#endif
                    double oldVersion = UserDefault::getInstance()->getDoubleForKey(UDKEY_DOUBLE_PROODUCTCACHEVER);
                    if (newVersion == oldVersion) { //Cache Version이 같으니 바로 인벤토리 정보 호출 하자
                        if( this->loadProductSalesInfomation() == true ) {
                            this->updateProductJsonInfomation();
//                            this->doleGetInventoryListProtocol();
                        } else {
                            UserDefault::getInstance()->setDoubleForKey(UDKEY_DOUBLE_PROODUCTCACHEVER, -1);
                            UserDefault::getInstance()->flush();
                            this->doleCacheSyncVersionProtocol();
                        }
                    } else {// 같지 않으니 product 정보 업데이트를 먼저 하자.
                        UserDefault::getInstance()->setDoubleForKey(UDKEY_DOUBLE_PROODUCTCACHEVER, newVersion);
                        this->doleGetProductProtocol();
                    }
                } break;
                case TAG_NET_INVENTORY: {
                    this->updateProductJsonInfomation();
                    DimLayer* dim = (DimLayer*)getChildByTag(TAG_DIM);
                    if(dim != nullptr) {
                        dim->displayItemUnlock(this->getLimitTime());
                    }
                    if (_charecLayer) {
                        _charecLayer->checkExecutePurchase();
                        _charecLayer = nullptr;
                    } else {
                        if (_isPurchasReady & PURCHASE_START_WITH_LOGIN) {
                            _isPurchasReady = _isPurchasReady | PURCHASE_INVENTORY_READY;
                            this->runPasswordView();
                        }
                    }
                } break;
                default:
                    break;
            } break;
        } break;
        case TSNetResult::ErrorTimeout: {
            switch (protocol->getTag()) {
                case TAG_NET_PURCHASE: {
                    TSDoleNetProgressLayer *progressLayer = (TSDoleNetProgressLayer *)this->getChildByTag(TAG_NET_PROGRESS);
                    if (progressLayer) {
                        progressLayer->activityStop(Device::getResString(Res::Strings::CM_NET_WARNING_NETWORK_UNSTABLE));
                    }
                } break;
                case TAG_NET_GETBALANCE:
                case TAG_NET_INVENTORY:
                case TAG_NET_CACHEVERSION: {
                    if (_charecLayer) {
                        _charecLayer->cancelExecutePurchase(true);
                        _charecLayer = nullptr;
                    } else {
                        TSNetToastLayer::show(this);
                    }
                } break;
                default:
                    TSNetToastLayer::show(this);
                    break;
            }
        } break;
        case TSNetResult::ErrorResultFail: {
            switch (protocol->getTag()) {
                case TAG_NET_GETBALANCE: {
                    if ( protocol->getReturnCode() != 1 ) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
                    	UserDefault::getInstance()->removeKey(UDKEY_INTEGER_BALANCE);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_INTEGER_BALANCE];
#endif
                        updateDoleCoin(-1);
                        if (_charecLayer) {
                            _charecLayer->cancelExecutePurchase(true);
                            _charecLayer = nullptr;
                        }
                    }
                } break;
                case TAG_NET_RENEWAUTH: {
                    if ( protocol->getReturnCode() == 90213  ) {
                        AppDelegate* app = (AppDelegate*)Application::getInstance();
                        app->resetToInitialData();
                        UserDefault::getInstance()->setBoolForKey(UDKEY_BOOL_DELETEACCOUNT, true);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
                        UserDefault::getInstance()->removeKey(UDKEY_STRING_AUTHKEY);
                        UserDefault::getInstance()->removeKey(UDKEY_INTEGER_USERNO);
                        UserDefault::getInstance()->removeKey(UDKEY_STRING_EMAIL);
                        UserDefault::getInstance()->removeKey(UDKEY_STRING_LOGINID);
                        UserDefault::getInstance()->removeKey(UDKEY_STRING_LOGINPW);
                        UserDefault::getInstance()->removeKey(UDKEY_STRING_BIRTHDAY);
                        UserDefault::getInstance()->removeKey(UDKEY_STRING_CITY);
                        UserDefault::getInstance()->removeKey(UDKEY_STRING_COUNTRY);
                        UserDefault::getInstance()->removeKey(UDKEY_INTEGER_GENDER);

                        UserDefault::getInstance()->removeKey(UDKEY_BOOL_FACEBOOKLOGIN);
                        UserDefault::getInstance()->removeKey(UDKEY_STRING_SNSID);
                        UserDefault::getInstance()->removeKey(UDKEY_STRING_SNSNAME);

                        UserDefault::getInstance()->removeKey(UDKEY_INTEGER_BALANCE);
                        UserDefault::getInstance()->removeKey(UDKEY_STRING_PRODUCTSALES);
                        UserDefault::getInstance()->removeKey(UDKEY_DOUBLE_PROODUCTCACHEVER);
                        UserDefault::getInstance()->removeKey(UDKEY_STRING_PROMOTION_TM);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
                        {
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_AUTHKEY];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_INTEGER_USERNO];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_EMAIL];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_LOGINID];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_LOGINPW];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_BIRTHDAY];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_CITY];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_COUNTRY];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_INTEGER_GENDER];
                            
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_BOOL_FACEBOOKLOGIN];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_SNSID];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_STRING_SNSNAME];
                            
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_INTEGER_BALANCE];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@UDKEY_STRING_PRODUCTSALES];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@UDKEY_DOUBLE_PROODUCTCACHEVER];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@UDKEY_STRING_PROMOTION_TM];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                        }
#endif
                        updateDoleCoin(-1);
                    }
                } break;
                case TAG_NET_INVENTORY:
                case TAG_NET_CACHEVERSION: {
                    if (_charecLayer) {
                        _charecLayer->cancelExecutePurchase(true);
                        _charecLayer = nullptr;
                    } else {
                        TSNetToastLayer::show(this);
                    }
                } break;
                default:
                    TSNetToastLayer::show(this);
                    break;
            }
        } break;
        default: {
        } break;
    }
}

void Home::onProtocolFailed(TSDoleProtocol *protocol, TSNetResult result) {
    switch (result) {
        case cocos2d::TSNetResult::ErrorNetworkDisable: {
            switch (protocol->getTag()) {
                case TAG_NET_GETBALANCE:
                case TAG_NET_INVENTORY:
                case TAG_NET_CACHEVERSION: {
                    if (_charecLayer) {
                        _charecLayer->cancelExecutePurchase(true);
                        _charecLayer = nullptr;
                    } else {
                        TSNetToastLayer::show(this);
                    }
                } break;
                default:
                    TSNetToastLayer::show(this);
                    break;
            }
        } break;
            
        default:
            break;
    }
}

long Home::getLimitTime()
{
    long leftTime = 0;
    if (_productList) {
        const char* productName = this->getSelectedProductName();
        ssize_t count = (_productList ? _productList->count() : 0);
        DoleProduct* product = nullptr;
        for (ssize_t index = 0; index<count; index++) {
            product = (DoleProduct*)_productList->getObjectAtIndex(index);
            const char *code = product->getProductCode();
            if (product && strcmp(code, productName) == 0 ) {
                if (product->getExpiredHour() != 0) {
                    log("description : %s",product->description());
                    auto registerTimeChar = product->getRegisterDateTime();
                    log("registerTime : %s", registerTimeChar);
                    
                    leftTime = Util::getItemLeftTime(registerTimeChar,product->getExpiredHour());
                    return leftTime;
                }
            }
        }
    }
    
    return 0;
}
    
void Home::showPromotionPopup()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    AppDelegate* app = (AppDelegate*)Application::getInstance();
    if(app != nullptr) {
        /* JP commented out 02/16/2015
        if(app->showPromotionPopup()) {
            this->doleNoticeListProtocol();
        }*/
    }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    NSDate *displayDateUd = [[NSUserDefaults standardUserDefaults] valueForKey:KEY_PROMOTION_CAN_SHOW_DATE];
//    NSLog(@"displayDateUd : %@", displayDateUd);
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *displayDate;
    if (displayDateUd != nil) {
        NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:displayDateUd];
        components.timeZone = [NSTimeZone localTimeZone];
        displayDate = [calendar dateFromComponents:components];
        NSLog(@"displayDate : %@", displayDate);
    }

    NSDate *now = [NSDate date];
    NSDateComponents *nowComponents = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:now];
    nowComponents.timeZone = [NSTimeZone localTimeZone];
    NSDate *nowDate = [calendar dateFromComponents:nowComponents];
//    NSLog(@"nowDate : %@", nowDate);
    
    if (displayDateUd == nil || [displayDate compare:nowDate] == NSOrderedSame || [displayDate compare:nowDate] == NSOrderedAscending) {
//        this->doleNoticeListProtocol();
//          UIViewController *viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
//          PromotionViewController *promotionViewContoller = [[PromotionViewController alloc] initWithURL:[NSURL URLWithString:@"http://www.google.com"]];
//          [viewController presentViewController:promotionViewContoller animated:NO completion:nil];
//          [PromotionViewController release];
    }
#endif
}

std::vector<std::string> Home::splitByDelim(const std::string &s, char delim)
{
    std::vector<std::string> elems;
    std::string item;

    std::stringstream ss(s);
    while (std::getline(ss, item, delim))
        if(!item.empty())
            elems.push_back(item);
    
    return elems;
}

void Home::onHttpRequestCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response) {
    if (!response){
        log("response is empty...");
        return;
    }

    if (response->getResponseCode() == -1) {
        log("responseCode is -1");
        TSNetToastLayer::show(this);
        return;
    }
    
    const char *tag = response->getHttpRequest()->getTag();
    log("Tag: %s", tag);
    if (strcmp(tag, "GetProductSales") == 0) {//JSON 도 아니고 이미 네트워크쪽에서 선처리 한 상태이다.
        if( this->loadProductSalesInfomation() == true ) {
            this->updateProductJsonInfomation();
//            this->doleGetInventoryListProtocol();
        } else {
            UserDefault::getInstance()->setDoubleForKey(UDKEY_DOUBLE_PROODUCTCACHEVER, -1);
            this->doleCacheSyncVersionProtocol();
        }
        return;
    }
}

void Home::updateDoleCoin(int balance) {
    //signup info local saving
    {//remove unknown balance
//        ui::Button *unknownCoin = (ui::Button *)mainLayer->getChildByTag(TAG_BTN_DOLECOIN);
        mainLayer->removeChildByTag(TAG_BTN_DOLECOIN_UNKNOWN);
        mainLayer->removeChildByTag(TAG_BTN_DOLECOIN_COUNT);
    }
    {//add balance value
        if (UserDefault::getInstance()->getBoolForKey(UDKEY_BOOL_DELETEACCOUNT, false) == true) {
            balance = -1;
        }
        //after login  ******* do not delete *******
        if (balance >= 0) {
            char balanceString[100];
            sprintf(balanceString, "%d", balance);
            auto coinNum = LabelExtension::create(balanceString, FONT_NAME_BOLD, HOME_COIN_NUM_FONTSIZE,HOME_COIN_NUM_SIZE,TextHAlignment::LEFT, TextVAlignment::CENTER);
            coinNum->addEllipsisWidthDigitNum(5);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
            coinNum->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_LEFT);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
            coinNum->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
#endif
            coinNum->setColor(Color3B(0xff,0x54,0x11));
            coinNum->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
            coinNum->enableShadow(Color4B(0,0,0,30.6f),LABEL_SHADOW_DEFAULT_SIZE, 0);
            coinNum->setPosition(HOME_COIN_NUM);
            coinNum->setTag(TAG_BTN_DOLECOIN_COUNT);
            mainLayer->addChild(coinNum, 10);
        } else {
            //befor login ******* do not delete *******
            auto coinNumUnknown = ui::Button::create("home_dole_coin_before_login.png", "home_dole_coin_before_login_press.png");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
            coinNumUnknown->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_LEFT);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
            coinNumUnknown->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
#endif
            coinNumUnknown->setPosition(HOME_COIN_NUM);
            coinNumUnknown->addTouchEventListener(this, cocos2d::ui::SEL_TouchEvent(&Home::touchEventForUIButton));
            coinNumUnknown->setTag(TAG_BTN_DOLECOIN_UNKNOWN);
            mainLayer->addChild(coinNumUnknown, 10);
        }
    }
	log("GetBalance success EventBalance : %d\n", balance);
}

void Home::renewDoleCoin(int balance) {
	_doleCoinBalance = balance;
   	updateDoleCoin(balance);
}

bool Home::loadProductSalesInfomation() {
    std::string str = UserDefault::getInstance()->getStringForKey(UDKEY_STRING_PRODUCTSALES);
    if (str.length() <= 0) {
        return false;
    }
//    log("load product xml : \n%s", str.c_str());
    
    tinyxml2::XMLDocument document;
    document.Parse(str.c_str());
    
    tinyxml2::XMLElement *root = document.RootElement();
//    CCASSERT(root, "XML error  or  XML is empty.");
    if (root == NULL) {
        log("XML error  or  XML is empty. reload ProductSale Infomation!!!!!!!!!!!!!!!!");
        return false;
    }
    log("root name : %s", root->Name());
    tinyxml2::XMLElement *entryElement = root->FirstChildElement("entry");
    if (_productList) {
        _productList->removeAllObjects();
    } else {
        _productList = __Array::create();
        _productList->retain();
    }
    while (entryElement) {
        tinyxml2::XMLElement *contentTag = entryElement->FirstChildElement("content");
        tinyxml2::XMLElement *properties = contentTag->FirstChildElement("m:properties");
        if (properties) {
            DoleProduct *newProduct = DoleProduct::create(properties);
            if (newProduct) {
                _productList->addObject(newProduct);
//                log("%s", newProduct->description());
            }
        }
        entryElement = entryElement->NextSiblingElement("entry");
    }
    return true;
}

void Home::updateProductJsonInfomation() {
    size_t size = (_productList ? _productList->count() : 0);
    if (size == 0) {
        return;
    }
    DoleProduct *product = nullptr;
    bool isAllUpdate = false;
    for (int index = 0; index < size; index++) {
        bool isProductUpdate = false;
        product = (DoleProduct *)_productList->getObjectAtIndex(index);
//        log("product info\n%s", product->description());
        std::string productCode = product->getProductCode();
        auto productJsonString = UserDefault::getInstance()->getStringForKey(productCode.c_str());
        if (productJsonString.length() > 0) {
            rapidjson::Document productInfo;
            productInfo.Parse<0>(productJsonString.c_str());

//            {
//                GenericStringBuffer< UTF8<> > buffer;
//                Writer< GenericStringBuffer< UTF8<> > > writer(buffer);
//                productInfo.Accept(writer);
//                log("%s/%s product before JSON\n%s", productCode.c_str(), product->getProductName(), buffer.GetString());
//            }
            
            bool free = productInfo["free"].GetBool();
            if (free == false) {
                int price = productInfo["price"].GetInt();
                int salePrice = product->getSalePrice();
                if (price != salePrice) {
                    productInfo.RemoveMember("price");
                    productInfo.AddMember("price", salePrice, productInfo.GetAllocator());
                    isProductUpdate = true;
                }
                
                bool locked = productInfo["locked"].GetBool();
                bool purchased = product->getPurchase();
                if (purchased == locked) {
                    productInfo.RemoveMember("locked");
                    productInfo.AddMember("locked", !purchased, productInfo.GetAllocator());
                    isProductUpdate = true;
                }
                bool hasMember = productInfo.HasMember("expiration_date");
                std::string registerDate = product->getRegisterDateTime();
                if (registerDate.length() > 0) {
                    if (hasMember) {
                        std::string expire = productInfo["expiration_date"].GetString();
                        // TODO:tuilise expire date string을 current time zone에 맞추어 변경 처리 해야 함
                        if (strcmp(expire.c_str(), registerDate.c_str()) != 0) {
                            productInfo.RemoveMember("expiration_date");
                            productInfo.AddMember("expiration_date", product->getRegisterDateTime(), productInfo.GetAllocator());
                            isProductUpdate = true;
                        }
                    } else {
                        productInfo.AddMember("expiration_date", product->getRegisterDateTime(), productInfo.GetAllocator());
                        isProductUpdate = true;
                    }
                } else {
                    if (hasMember) {
                        productInfo.RemoveMember("expiration_date");
                        isProductUpdate = true;
                    }
                }
                
                hasMember = productInfo.HasMember("expired_hour");
                int expiredHour = product->getExpiredHour();
                if (expiredHour > 0) {
                    if (hasMember) {
                        int orgExpiredHour = productInfo["expired_hour"].GetInt();
                        if (expiredHour != orgExpiredHour) {
                            productInfo.RemoveMember("expired_hour");
                            productInfo.AddMember("expired_hour", expiredHour, productInfo.GetAllocator());
                            isProductUpdate = true;
                        }
                    } else {
                        productInfo.AddMember("expired_hour", expiredHour, productInfo.GetAllocator());
                        isProductUpdate = true;
                    }
                } else {
                    if (hasMember) {
                        productInfo.RemoveMember("expired_hour");
                        isProductUpdate = true;
                    }
                }
                if (isProductUpdate == true) {
                    isAllUpdate = true;
                    GenericStringBuffer< UTF8<> > buffer;
                    Writer< GenericStringBuffer< UTF8<> > > writer(buffer);
                    productInfo.Accept(writer);
                    UserDefault::getInstance()->setStringForKey(productCode.c_str(), buffer.GetString());
//                    log("%s/%s product after JSON\n%s", product->getProductCode(), product->getProductName(), buffer.GetString());
//                    std::string updateJsonString = UserDefault::getInstance()->getStringForKey(productCode.c_str());
//                    log("%s/%s save JSON\n%s", product->getProductCode(), product->getProductName(), updateJsonString.c_str());
                }
            }
        } else {
            log("%s/%s product JSON Empty!!!!!", product->getProductCode(), product->getProductName());
        }
    }
    
    if (isAllUpdate == true) {
        UserDefault::getInstance()->flush();
        backgroundScrollView->reloadData();
        characterScrollView->reloadData();
    }
}

void Home::getUpdateVersion()
{
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	if(_protocolAppVersion != nullptr) {
		if(_protocolAppVersion->getStatus() == TSProtocolStatus::Finished) {
    CC_SAFE_RELEASE_NULL(_protocolAppVersion);
		} else {
			_protocolAppVersion->setStatus(TSProtocolStatus::Canceled);
		}
	}
//#else
//    CC_SAFE_RELEASE_NULL(_protocolAppVersion);
//#endif
    _protocolAppVersion = new TSDoleGetAppVersion();
    _protocolAppVersion->setDelegate(this);
    _protocolAppVersion->setTag(TAG_NET_LAST_VERSION);
    _protocolAppVersion->init();
    _protocolAppVersion->request();
}

#pragma mark -

void Home::characterPhotoSaved(std::string characterKey)
{
    characterScrollView->addCharacter(characterKey);
}

void Home::bgPhotoSaved(std::string bgKey)
{
    backgroundScrollView->addBackground(bgKey);
}

void Home::cameraScenePopped()
{
    cameraScene = nullptr;
}

    void Home::onRequestCanceled(TSDoleProtocol *protocol) {
        CC_SAFE_RELEASE_NULL(protocol);
    }
    

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
void Home::updateCustomCharacater(int index, const char* path)
{
	std::string characterKey = Util::saveCustomCharacterData(index, path);
	characterPhotoSaved(characterKey);
}

void Home::updateCustomBackground(const char* path)
{
	std::string bgKey = Util::saveCustomBackgroundData(path);
	bgPhotoSaved(bgKey);
}

void Home::pendingShowGuide() {
	this->scheduleOnce(SEL_SCHEDULE(&Home::showGuide), 0.0f);
}

void Home::updateDoleCoinAndroid() {
    AppDelegate* app = (AppDelegate*)Application::getInstance();
    int balance = app->getPendingDoleCoin();
    if(balance >= 0) {
    	renewDoleCoinAndroid(balance);
//    	doleCacheSyncVersionProtocol();
    }
}

void Home::renewDoleCoinAndroid(int balance)
{
	renewDoleCoin(balance);
    if (_isPurchasReady & PURCHASE_START_WITH_LOGIN) {
        _isPurchasReady = _isPurchasReady | PURCHASE_GETBALANSE_READY;
        this->runPasswordView();
    }
}
#endif

void Home::characterPhotoSavedInRecord(std::string characterKey)
{
    characterScrollView->addCharacter(characterKey);
}

void Home::bgPhotoSavedInRecord(std::string bgKey)
{
    backgroundScrollView->addBackground(bgKey);
}

void Home::deleteCharItemInRecordScene(Ref *obj)
{
    characterScrollView->removeFromParentAndCleanup(true);
    characterScrollView = CharacterScrollView::create(CHAR_HOME);
    characterScrollView->setPosition(HOME_BACKGROUND_SCROLL_VIEW);
    characterScrollView->setDelegate(this);
    characterScrollView->setVisible(false);
    mainLayer->addChild(characterScrollView, 1);
    
    characterScrollView->scrollToLastItem();
}

void Home::deleteBgItemInRecordScene(Ref *obj)
{
    backgroundScrollView = BackgroundScrollView::create(BG_HOME);
    backgroundScrollView->setPosition(HOME_BACKGROUND_SCROLL_VIEW);
    backgroundScrollView->setDelegate(this);
    backgroundScrollView->setVisible(false);
    mainLayer->addChild(backgroundScrollView, 1);
    
    backgroundScrollView->scrollToLastItem();
}

void Home::checkRecall(int type) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	switch(type) {
		case ANDROID_PENDING_PURCHASE: {
            _isPurchasReady = _isPurchasReady | PURCHASE_LOGIN_READY;
            this->runPasswordView();
		} break;
	}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
    switch (type) {
        case TSRecallHomeDoleCoin: {
            this->runDoleCoinView(false);
        } break;
        case TSRecallHomePurchase: {
            _isPurchasReady = _isPurchasReady | PURCHASE_LOGIN_READY;
            this->runPasswordView();
        } break;
        default:
            break;
    }
#endif
}
