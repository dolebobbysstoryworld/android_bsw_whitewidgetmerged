#ifndef  _APP_DELEGATE_H_
#define  _APP_DELEGATE_H_

#include "cocos2d.h"

/**
@brief    The cocos2d Application.

The reason for implement as private inheritance is to hide some interface call by Director.
*/
class  AppDelegate : private cocos2d::Application
{
public:
    AppDelegate();
    virtual ~AppDelegate();

    /**
    @brief    Implement Director and Scene init code here.
    @return true    Initialize success, app continue.
    @return false   Initialize failed, app terminate.
    */
    virtual bool applicationDidFinishLaunching();

    /**
    @brief  The function be called when the application enter background
    @param  the pointer of the application
    */
    virtual void applicationDidEnterBackground();

    /**
    @brief  The function be called when the application enter foreground
    @param  the pointer of the application
    */
    virtual void applicationWillEnterForeground();
    
    void loadInitialData();
    void createInitialUserData();
    void resetToInitialData();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    void showPlatformView(int type, int sceneId);
    void setPendingAction(int type);
    void sharedVideoFacebook();
    void sharedVideoYoutube();
    void purchasedCharacter(const char* purchaseKey);
    void purchasedBackground(const char* purchaseKey);
    void showBook(const char* bookKey);
    void showBookA();
    void showBookB();
    void createdCustomBook();
    void createStoryStart(int tag, int finalCounter);
    void createStorySuccess();
    void showPlatformVideoView(const char* filename, int sceneId);
    bool hidePlatformView();
    void setEditTextContents(const char* title, const char* author, int sceneId);
    void changeVoiceRecordStatus(int status, const char* filename, int sceneId);
    void changeVoicePlayStatus(int status, const char* filename, int sceneId);
    void changeBGMPlayStatus(int status, const char* filename, int sceneId);
    void startVideoRecord(int width, int height, int sceneId);
    void stopVideoRecord();
    void pauseVideoRecord();
    void resumeVideoRecord();
    void feedAudioFileName(const char* voice[]);
    int getPendingDoleCoin();
    bool getPendingGuideHome();
    bool needCheckUpdate();
    void updateAuthKey(const char* authKey);
    std::string getRecordingFilename();
    void saveVideoToDevice(const char* file, const char* title, const char* author);
    void onRecordSceneChange();
    void checkFacebookPermission();
    void uploadVideoToFacebook(const char* title, const char* description, const char* path);
    bool showPromotionPopup();
    void loadPromotionPage(const char* url);
    void showPlatformVideoViewWithList(int count, const char* list[], int sceneId);
    void playVideoList(bool show, int sceneId);
    void removeLogo();
    void mixpanelTrack(const char* eventName, const char* dictString);
#endif

private:
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    bool _bNeedCheckUpdate;
#endif
};

#endif // _APP_DELEGATE_H_

