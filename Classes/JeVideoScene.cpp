#include "cocos2d.h"

//
//  VideoScene.cpp
//  bobby
//
//  Created by oasis on 2014. 4. 16..
//
//

#include "JeVideoScene.h"
#include "HomeScene.h"
#include "Define.h"
#include "ResourceManager.h"
#include "BGMManager.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "AppDelegate.h"
#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS //#ifndef MACOS
#import "MovieViewController.h"
#import "AppController.h"
MovieViewController *movieViewController;
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#endif

#include "SimpleAudioEngine.h"

#define TAG_BG 0
#define TAG_BG_ENDING 5
#define UI_HIDE_DELAY   2.0f

#define TAG_PROGRESS_LEFT   10
#define TAG_PROGRESS_CENTER 11
#define TAG_PROGRESS_RIGHT  12

Scene* JeVideo::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = JeVideo::create();
    layer->setTag(TAG_VIDEO);
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

Scene* JeVideo::createScene(__String *bookTitle, __String *authorTitle, __Array *movieNames, play_type playType)
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = JeVideo::create(bookTitle, authorTitle, movieNames, playType);
    layer->setTag(TAG_VIDEO);
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

JeVideo* JeVideo::create(__String *bookTitle, __String *authorTitle, __Array *movieNames, play_type playType)
{
    JeVideo *layer = new JeVideo();
    if (layer && layer->init(bookTitle, authorTitle, movieNames, playType))
    {
        layer->autorelease();
        return layer;
    }
    CC_SAFE_DELETE(layer);
    return nullptr;
}

bool JeVideo::init(__String *bookTitle, __String *authorTitle, __Array *movieNames, play_type playType)
{
    if ( !LayerColor::initWithColor( Color4B(0,0,0,0)) )
    {
        return false;
    }
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    isPlatformShown = false;
#endif
    
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    
    if (strcmp(bookTitle->getCString(), Device::getResString(Res::Strings::CC_PRLOAD_BOOKTITLE_1)) == 0) {
        bookType = BOOK_TYPE_BOBBY;
    } else {
        bookType = BOOK_TYPE_SPACE;
    }
    
    avMovieItems = movieNames;
    moviePlayType = playType;
    opening = nullptr;
    
    playing = false;
    paused = false;
    
    hideUIAction = nullptr;
    visibleUI = false;
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    if (bookType == BOOK_TYPE_BOBBY) {
        auto bg = Sprite::create("scene_05_end.png");
        bg->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
        bg->setTag(TAG_BG_ENDING);
        bg->setScale((visibleSize.height/bg->getContentSize().height));
        bg->setPosition(cocos2d::Point(visibleSize.width/2, visibleSize.height/2));
        this->addChild(bg, 0);
    } else {
        auto bg = Sprite::create("scene_10_end.png");
        bg->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
        bg->setTag(TAG_BG_ENDING);
        bg->setScale((visibleSize.height/bg->getContentSize().height));
        bg->setPosition(cocos2d::Point(visibleSize.width/2, visibleSize.height/2));
        this->addChild(bg, 0);
    }
#endif

    if (bookType == BOOK_TYPE_BOBBY) {
        auto bg = Sprite::create("scene_01_start.png");
        bg->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
        bg->setTag(TAG_BG);
        bg->setScale((visibleSize.height/bg->getContentSize().height));
        bg->setPosition(cocos2d::Point(visibleSize.width/2, visibleSize.height/2));
        this->addChild(bg, 0);
    } else {
        auto bg = Sprite::create("scene_06_start.png");
        bg->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
        bg->setTag(TAG_BG);
        bg->setScale((visibleSize.height/bg->getContentSize().height));
        bg->setPosition(cocos2d::Point(visibleSize.width/2, visibleSize.height/2));
        this->addChild(bg, 0);
    }
    
    if (moviePlayType == PLAY_TYPE_ALL) {
        btnBack = MenuItemImage::create("btn_map_back_normal.png",
                                        "btn_map_back_press.png",
                                        CC_CALLBACK_0(JeVideo::btnBackCallback, this));
        
        btnBack->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        btnBack->setPosition(cocos2d::Point(BOOKMAP_BTN_BACK));
        
        playMenuItem = MenuItemImage::create("btn_set_record_play_normal.png", "btn_set_record_play_pressed.png",
                                             CC_CALLBACK_1(JeVideo::playMenuCallback, this));
        playMenuItem->setPosition(cocos2d::Point(visibleSize.width/2, visibleSize.height/2));
        playMenuItem->setVisible(false);
        titleLabel = this->createTitleLabel();
        titleLabel->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_BOTTOM);
        titleLabel->setPosition(cocos2d::Point(playMenuItem->getContentSize().width/2, MAP_PLAY_TITLE_BOTTOM_MARGIN));
        playMenuItem->addChild(titleLabel);
        
        pauseMenuItem = MenuItemImage::create("btn_set_record_pause_normal.png", "btn_set_record_pause_pressed.png",
                                              CC_CALLBACK_1(JeVideo::playMenuCallback, this));
        pauseMenuItem->setPosition(cocos2d::Point(visibleSize.width/2, visibleSize.height/2));
        titleLabel2 = this->createTitleLabel();
        titleLabel2->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_BOTTOM);
        titleLabel2->setPosition(cocos2d::Point(playMenuItem->getContentSize().width/2, MAP_PLAY_TITLE_BOTTOM_MARGIN));
        pauseMenuItem->addChild(titleLabel2);
        
        auto titleString = this->getTitle(1);
        if (titleString != nullptr) {
            titleLabel->setString(titleString->getCString());
            titleLabel2->setString(titleString->getCString());
        }
        
        auto menu = Menu::create(btnBack, NULL);
        menu->setPosition(cocos2d::Point::ZERO);
        this->addChild(menu, 13);
        
        controlMenu = Menu::create(playMenuItem, pauseMenuItem, NULL);
        controlMenu->setPosition(cocos2d::Point::ZERO);
        this->addChild(controlMenu, 13);
        this->createProgressBar();

        showUI(false);
        
        playedTime = 0;        

        opening = Opening::createOpening(bookTitle, authorTitle);
        opening->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        opening->setPosition(cocos2d::Point(0, 0));
        this->addChild(opening);
        opening->setDelegate(this);

        if (bookType == BOOK_TYPE_BOBBY) {
            totalDuration = OPENING_DURATION + (DURATION_SCENE01 + DURATION_SCENE02 + DURATION_SCENE03 + DURATION_SCENE04 + DURATION_SCENE05 + (DURATION_TRANSITION * 4) ) + ENDING_DURATION;
        } else {
            totalDuration = OPENING_DURATION + (DURATION_SCENE06 + DURATION_SCENE07 + DURATION_SCENE08 + DURATION_SCENE09 + DURATION_SCENE10 + (DURATION_TRANSITION * 4) ) + ENDING_DURATION;
        }
        
        this->scheduleUpdate();
        
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		int videoCount = avMovieItems->count();
		const char* videoList[videoCount];
		for(int i = 0 ; i < videoCount ; i++) {
			videoList[i] = ((__String*)avMovieItems->getObjectAtIndex(i))->getCString();
		}

		AppDelegate* app = (AppDelegate*)Application::getInstance();
		if(!isPlatformShown) {
			app->showPlatformVideoViewWithList(videoCount, videoList, TAG_VIDEO);
		}
#endif
    } else {
        this->playAVPlayer();
    }
    return true;
}

// on "init" you need to initialize your instance
bool JeVideo::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LayerColor::initWithColor( Color4B(0xba,0xea,0xed,0xff)) )
    {
        return false;
    }

//#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS //#ifndef MACOS
//    UIViewController *selfViewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
//    MovieViewController *movieViewController = [[MovieViewController alloc] init];
//    [selfViewController.view addSubview:movieViewController.view];
//#endif
//    
    return true;
}

void JeVideo::onEnterTransitionDidFinish()
{
    if (opening != nullptr)
    {
        playing = true;
        opening->startAnimation();
        opening->playBackgroundMusic();
    }
}

void JeVideo::btnBackCallback()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	AppDelegate* app = (AppDelegate*)Application::getInstance();
	app->playVideoList(false, TAG_BOOKMAP);
	if(isPlatformShown) {
		isPlatformShown = false;
	} else {
		movieDidFinishCallback();
	}
	
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
    [movieViewController closeAction];
#endif
}

void JeVideo::playAVPlayer()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	AppDelegate* app = (AppDelegate*)Application::getInstance();
	if(!isPlatformShown) {
		app->playVideoList(true, TAG_VIDEO);
	}
	isPlatformShown = true;
#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS //#ifndef MACOS
    UIViewController *selfViewController = nil;
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        selfViewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
    } else {
        selfViewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
    }
    //    NSString *movieNameStr = [NSString stringWithUTF8String:movieName.c_str()];
    
    NSMutableArray *movieArray = [NSMutableArray array];
    for (int i=0;i < avMovieItems->count(); i++){
        auto mName = (__String*)avMovieItems->getObjectAtIndex(i);
        std::string movieName(mName->getCString());
        log("movieName : %s", movieName.c_str());
        NSString *movieNameStr = [NSString stringWithUTF8String:mName->getCString()];
        [movieArray addObject:movieNameStr];
    }
    
    NSArray *movieNameArray = (NSArray*)movieArray;
    NSLog(@"movieNAmeArray count : %lu", (unsigned long)[movieNameArray count]);
    movieViewController = [[MovieViewController alloc] initWithMovieNames:movieNameArray  andType:moviePlayType];
    [selfViewController.view addSubview:movieViewController.view];
    
#endif
    avMovieItems->removeAllObjects();
    
    if (moviePlayType == PLAY_TYPE_ALL) {
        showUI(false);
    }
}

void JeVideo::movieDidFinishCallback()
{
    this->closeScene();

//    auto homeScene = Home::createScene();
//    director->replaceScene(homeScene);
//    this->cleanup();
}

void JeVideo::playEnding()
{
    ending = Ending::create();
    ending->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    ending->setPosition(cocos2d::Point(0, 0));
    this->addChild(ending);
    ending->setDelegate(this);
    ending->startAnimation();
    playedTime = (totalDuration - ENDING_DURATION);
    
    auto titleString = this->getTitle(5);
    if (titleString != nullptr) {
        titleLabel->setString(titleString->getCString());
        titleLabel2->setString(titleString->getCString());
    }
    
    this->scheduleUpdate();
}

void JeVideo::changeTextureForEnding()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    auto bg = (Sprite*)this->getChildByTag(TAG_BG);
    if(bg != nullptr) {
        bg->setVisible(false);
    }
#else
    auto bg = (Sprite*)this->getChildByTag(TAG_BG);
    if (bookType == BOOK_TYPE_BOBBY) {
        bg->setTexture("scene_05_end.png");
    } else {
        bg->setTexture("scene_10_end.png");
    }
#endif
}

void JeVideo::onOpeningFinished(Opening* opening)
{
    this->unscheduleUpdate();
    this->playAVPlayer();
}

void JeVideo::onEndingFinished(Ending* ending)
{
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS //#ifndef MACOS
    UIViewController *selfViewController = nil;
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        selfViewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
    } else {
        selfViewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
    }
    for (UIView *subView in [selfViewController.view subviews]) {
        [subView removeFromSuperview];
        subView = nil;
    }
#endif
    this->movieDidFinishCallback();
}

void JeVideo::closeScene()
{
    this->unscheduleUpdate();
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("preload_sounds/scene_opening.wav", false);
    if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()) {
        CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    }
    
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS //#ifndef MACOS
    UIViewController *selfViewController = nil;
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        selfViewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
    } else {
        selfViewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
    }
    for (UIView *subView in [selfViewController.view subviews]) {
        [subView removeFromSuperview];
        subView = nil;
    }
#endif

    auto director = Director::getInstance();
    director->popScene();
}

void JeVideo::onEnter()
{
    Layer::onEnter();
    TSBGMManager::getInstance()->stop();

    if (moviePlayType == PLAY_TYPE_ALL) {
        auto listener = EventListenerTouchOneByOne::create();
        listener->setSwallowTouches(true);
        listener->onTouchBegan = CC_CALLBACK_2(JeVideo::onTouchBegan, this);
        listener->onTouchMoved = CC_CALLBACK_2(JeVideo::onTouchMoved, this);
        listener->onTouchEnded = CC_CALLBACK_2(JeVideo::onTouchEnded, this);
        listener->onTouchCancelled = CC_CALLBACK_2(JeVideo::onTouchCancelled, this);
        _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    }
    
    auto keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyReleased = CC_CALLBACK_2(JeVideo::onKeyReleased, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(keyboardListener, this);
}

void JeVideo::onExit()
{
    _eventDispatcher->removeEventListenersForTarget(this);

    Layer::onExit();
}

void JeVideo::onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	if(keyCode == cocos2d::EventKeyboard::KeyCode::KEY_BACKSPACE) {
		this->btnBackCallback();
	}
}

bool JeVideo::onTouchBegan(cocos2d::Touch* touch, cocos2d::Event *event)
{
    if (visibleUI) {
        showUI(false);
    } else {
        showUI(true);
        triggerHideUI();
    }
    return true;
}

void JeVideo::onTouchMoved(cocos2d::Touch* touch, cocos2d::Event *event)
{
    
}

void JeVideo::onTouchEnded(cocos2d::Touch* touch, cocos2d::Event *event)
{
    
}

void JeVideo::onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event *event)
{
    
}

void JeVideo::showUI(bool show)
{
    visibleUI = show;
    
    controlMenu->setVisible(show);
    btnBack->setVisible(show);
    playProgressBarHolder->setVisible(show);
    
    if(show) {
        cancelHideUI();
    }
}

void JeVideo::triggerHideUI()
{
    if(hideUIAction == NULL) {
        auto delay = DelayTime::create(UI_HIDE_DELAY);
        hideUIAction = Sequence::create(delay, CallFunc::create(CC_CALLBACK_0(JeVideo::showUI, this, false)), NULL);
        this->runAction(hideUIAction);
    }
}

void JeVideo::cancelHideUI()
{
    if (hideUIAction != NULL){
        this->stopAction(hideUIAction);
        hideUIAction = NULL;
    }
}

void JeVideo::playMenuCallback(Ref* pSender)
{
    if (pSender != nullptr) {
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Tap.wav", false);
    }
    
    if (playing) {
        if (paused) { // resume
            paused = false;
            if (opening != nullptr) {
                if (opening->isAnimationStarted()) {
                    if(opening->isAnimationPaused()){
                        opening->resumeAnimation();
                    }
                }
            }
            
            if (ending != nullptr) {
                if (ending->isAnimationStarted()) {
                    if(ending->isAnimationPaused()){
                        ending->resumeAnimation();
                    }
                }
            }
            pauseMenuItem->setVisible(true);
            playMenuItem->setVisible(false);
            showUI(false);
        } else { // pause
            paused = true;
            if (opening != nullptr) {
                if (opening->isAnimationStarted()) {
                    opening->pauseAnimation();
                }
            }
            
            if (ending != nullptr) {
                if (ending->isAnimationStarted()) {
                    ending->pauseAnimation();
                }
            }
            pauseMenuItem->setVisible(false);
            playMenuItem->setVisible(true);
            showUI(true);
        }
    }
}

void JeVideo::createProgressBar()
{
    playProgressBarHolder = Node::create();
    this->addChild(playProgressBarHolder, 15);
    
    // play progress bar bg
    playProgressBarBg = cocos2d::Node::create();
    playProgressBarBg->setContentSize(RECORDING_PROGRESS_BAR_SIZE);
    playProgressBarBg->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarBg->setPosition(RECORDING_PROGRESS_BAR);
    auto playProgressBarBgLeft = Sprite::create("recording_play_progress_bg_left.png");
    playProgressBarBgLeft->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    
    auto playProgressBarBgRight = Sprite::create("recording_play_progress_bg_right.png");
    
    auto playCenterBgWidth = RECORDING_PROGRESS_BAR_SIZE.width - playProgressBarBgLeft->getContentSize().width - playProgressBarBgRight->getContentSize().width;
    auto playProgressBarBgCenter = Sprite::create("recording_play_progress_bg_center.png", cocos2d::Rect(0, 0, playCenterBgWidth, RECORDING_PROGRESS_BAR_SIZE.height));
    playProgressBarBgCenter->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarBgCenter->setPosition(cocos2d::Point(playProgressBarBgLeft->getContentSize().width, 0));
    
    playProgressBarBgRight->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarBgRight->setPosition(cocos2d::Point(playProgressBarBgLeft->getContentSize().width + playCenterBgWidth, 0));
    
    playProgressBarBg->addChild(playProgressBarBgLeft);
    playProgressBarBg->addChild(playProgressBarBgRight);
    playProgressBarBg->addChild(playProgressBarBgCenter);
//    playProgressBarBg->setVisible(true);
    
    playProgressBarHolder->addChild(playProgressBarBg, 15);
    
    // play progress bar
    playProgressBar = cocos2d::Node::create();
    playProgressBar->setContentSize(RECORDING_PROGRESS_BAR_SIZE);
    playProgressBar->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBar->setPosition(RECORDING_PROGRESS_BAR);
    playProgressBar->setVisible(false);
    auto playProgressBarLeft = Sprite::create("recording_play_progress_left.png");
    playProgressBarLeft->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarLeft->setTag(TAG_PROGRESS_LEFT);
    
    auto playProgressBarRight = Sprite::create("recording_play_progress_right.png");
    playProgressBarRight->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarRight->setPosition(cocos2d::Point(playProgressBarLeft->getContentSize().width, 0));
    playProgressBarRight->setTag(TAG_PROGRESS_RIGHT);
    
    playProgressBar->addChild(playProgressBarLeft);
    playProgressBar->addChild(playProgressBarRight);
//    playProgressBar->setVisible(true);
    
    playProgressBarHolder->addChild(playProgressBar, 15);
}

void JeVideo::setProgress(float value)
{
    if (value > 1.0f) {
        log("progress bar - reached to end");
        return;
    }
    
    if (value == 0) {
        playProgressBar->setVisible(false);
    } else {
        playProgressBar->setVisible(true);
    }
    
    Node *targetBar = playProgressBar;
    std::string centerFileName = "recording_play_progress_center.png";
    
    auto center = targetBar->getChildByTag(TAG_PROGRESS_CENTER);
    
    auto progressBarLeft = targetBar->getChildByTag(TAG_PROGRESS_LEFT);
    auto progressBarRight = targetBar->getChildByTag(TAG_PROGRESS_RIGHT);
    
    auto progressWidth = RECORDING_PROGRESS_BAR_SIZE.width - progressBarLeft->getContentSize().width - progressBarRight->getContentSize().width;
    auto centerWidth = std::ceil(progressWidth * value);
    
    if (center) {
        center->removeFromParentAndCleanup(true);
    }
    
    if (centerWidth > 0) {
        auto newCenter = Sprite::create(centerFileName, cocos2d::Rect(0, 0, centerWidth, RECORDING_PROGRESS_BAR_SIZE.height));
        newCenter->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        newCenter->setPosition(cocos2d::Point(progressBarLeft->getContentSize().width, 0));
        newCenter->setTag(TAG_PROGRESS_CENTER);
        targetBar->addChild(newCenter);
    }
    progressBarRight->setPosition(cocos2d::Point(progressBarLeft->getContentSize().width + centerWidth, 0));
}

Label *JeVideo::createTitleLabel()
{
    auto pageTitle = cocos2d::Label::createWithSystemFont("", FONT_NAME_BOLD, MAP_PLAY_TITLE_TEXT_SIZE);
    pageTitle->setDimensions(MAP_PLAY_TITLE_SIZE.width, MAP_PLAY_TITLE_SIZE.height);
    pageTitle->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::CENTER);
    pageTitle->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    pageTitle->setColor(cocos2d::Color3B(255, 255, 255));
    
    return pageTitle;
}

void JeVideo::update(float delta)
{
    if (playing && !paused) {
        playedTime += delta;
        float currentProgress = playedTime / totalDuration;
        this->setProgress(currentProgress);
    }
}

__String *JeVideo::getTitle(int chapterIndex)
{
    __String *chapterTitle = NULL;
    
    switch (chapterIndex) {
        case 1:
            chapterTitle = __String::create(Device::getResString(Res::Strings::CC_PLAY_PAGE_TITLE_INTRO));//"Intro"
            break;
        case 2:
        case 3:
        case 4:
            chapterTitle = __String::createWithFormat(Device::getResString(Res::Strings::CC_PLAY_PAGE_TITLE_PAGE_N), chapterIndex);//"Page %d"
            break;
        case 5:
            chapterTitle = __String::create(Device::getResString(Res::Strings::CC_PLAY_PAGE_TITLE_FINAL));//"Final"
            break;
        default:
            break;
    }
    
    return chapterTitle;
}
