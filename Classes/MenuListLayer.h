//
//  MenuListLayer.h
//  bobby
//
//  Created by oasis on 2014. 7. 29..
//
//

#ifndef __bobby__MenuListLayer__
#define __bobby__MenuListLayer__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

typedef enum list_type{
    LIST_TYPE_01 = 0,
    LIST_TYPE_02 = 1,
} list_type;

typedef enum button_type{
    BUTTON_TYPE_SETTINGS = 1,
    BUTTON_TYPE_DOWNLOAD = 0,
    BUTTON_TYPE_SHARE = 2,
}button_type;

class MenuListLayer;
class MenuListLayerDelegate
{
public:
    virtual ~MenuListLayerDelegate() {}
    virtual void listButtonClicked(int buttonTag) = 0;
};

class MenuListLayer : public cocos2d::LayerColor
{
public:
    virtual ~MenuListLayer();
    static MenuListLayer* create(list_type listType);
    bool initWithType(list_type listType);
    
    bool menuListVisible;
    void menuListControl(bool visible);
    
    MenuListLayerDelegate *getDelegate() { return _delegate; }
    void setDelegate(MenuListLayerDelegate *pDelegate) { _delegate = pDelegate; }
protected:
    MenuListLayerDelegate *_delegate;
private:
    cocos2d::extension::Scale9Sprite* listBg;
    void buttonClicked(Ref *pSender);
    Menu* getMenuButton(const std::string& normal, const std::string& press, cocos2d::Size contentSize, cocos2d::Point position, int tag);
    
};

#endif /* defined(__bobby__MenuListLayer__) */
