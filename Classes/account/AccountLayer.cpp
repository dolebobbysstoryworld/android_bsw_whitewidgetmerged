//
//  AccountLayer.cpp
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 16..
//
//

#include "AccountLayer.h"
#include "LoginLayer.h"
#include "SignupLayer.h"
#include "SignTermsLayer.h"

USING_NS_CC;

AccountLayer::AccountLayer():
_delegate(nullptr),
_isfirstRun(false) {
    memset(&_accountInfo, 0, sizeof(DoleAccountInfomation));
}

AccountLayer::~AccountLayer() {
    log("AccountLayer release");
}

AccountLayer* AccountLayer::create(bool firstRun)
{
    AccountLayer *layer = new AccountLayer();
    if (layer && layer->init())
    {
        layer->setFirstRun(firstRun);
        layer->autorelease();
        return layer;
    }
    CC_SAFE_DELETE(layer);
    return nullptr;
}

void AccountLayer::onEnter() {
    Layer::onEnter();
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [=](Touch* touch, Event* event) {return true;};
    listener->onTouchMoved = [=](Touch* touch, Event* event){};
    listener->onTouchEnded = [=](Touch* touch, Event* event){};
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

void AccountLayer::onExit() {
    _eventDispatcher->removeEventListenersForTarget(this);
    Layer::onExit();
}

bool AccountLayer::init() {
    if ( !LayerColor::initWithColor(Color4B(0, 0, 0, 179)) ) return false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    auto loginLayer = Loginlayer::create();
//    loginLayer->setAnchorPoint(Point::ANCHOR_TOP_RIGHT);//layer는 anchorpoint가 무시됨
    loginLayer->setPosition( Point( (visibleSize.width - AccountLayerProtocol::getLayerSize().width)/2,
                                   (visibleSize.height - AccountLayerProtocol::getLayerSize().height)/2 ) );
    this->addChild(loginLayer);
    
    return true;
}

void AccountLayer::closeMenuCallback(Ref* pSender) {

}

void AccountLayer::closeChildLayer() {
    log("AccountLayer::closeChildLayer call");
	this->removeFromParentAndCleanup(true);
    Director::getInstance()->resume();
}

void AccountLayer::nextPageLayer(int page) {
    switch (page) {
        case 1: {
            Size visibleSize = Director::getInstance()->getVisibleSize();
            auto loginLayer = Loginlayer::create();
            loginLayer->setPosition( Point( (visibleSize.width - AccountLayerProtocol::getLayerSize().width)/2,
                                           (visibleSize.height - AccountLayerProtocol::getLayerSize().height)/2 ) );
            this->addChild(loginLayer);
        } break;
        case 2: {
            Size visibleSize = Director::getInstance()->getVisibleSize();
            auto signupLayer = SignupLayer::create();
            signupLayer->setPosition( Point( (visibleSize.width - AccountLayerProtocol::getLayerSize().width)/2,
                                            (visibleSize.height - AccountLayerProtocol::getLayerSize().height)/2 ) );
            this->addChild(signupLayer);
        } break;
        case 3: {
            Size visibleSize = Director::getInstance()->getVisibleSize();
            auto signTermsLayer = SignTermsLayer::create();
            signTermsLayer->setPosition( Point( (visibleSize.width - AccountLayerProtocol::getLayerSize().width)/2,
                                               (visibleSize.height - AccountLayerProtocol::getLayerSize().height)/2 ) );
            this->addChild(signTermsLayer);
        } break;
    }
}

void AccountLayer::setFirstRun(bool firstRun) {
    _isfirstRun = firstRun;
}

void AccountLayer::setAccountInfo(const char* idString, const char* pwString, const char* birthdayString, const char* cityString, int gender) {
    if (idString) {
        strcpy(_accountInfo.idString, idString);
    }
    if (pwString) {
        strcpy(_accountInfo.pwString, pwString);
    }
    if (birthdayString) {
        strcpy(_accountInfo.birthString, birthdayString);
    }
    if (cityString) {
        strcpy(_accountInfo.cityString, cityString);
    }
    _accountInfo.gender = gender;
}

void AccountLayer::loginDidEnded(bool result) {
    if (_delegate == nullptr) {
        return;
    }
    
//    _delegate->loginDidFinished(result);
}

void AccountLayer::signupDidEnded(bool result) {
    if (_delegate == nullptr) {
        return;
    }
    
}
