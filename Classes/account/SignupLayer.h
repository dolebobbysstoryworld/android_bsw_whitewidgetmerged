//
//  SignupLayer.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 16..
//
//

#ifndef __SIGNUPLAYER_H__
#define __SIGNUPLAYER_H__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "TSIconEditBox.h"
#include "TSRadioButton.h"
#include "AccountLayerProtocol.h"
#include "DoleAPI.h"
#include "PopupLayer.h"

NS_CC_BEGIN

class SignupLayer : public LayerColor, public TSEditBoxDelegate, public PopupLayerDelegate
{
public:
    SignupLayer();
    virtual ~SignupLayer();
    static SignupLayer* create();
    bool init();
    virtual void onEnter();
    virtual void onExit();
    
    void characterImageLayoutSetting();
    void imageLayoutSetting();
    void accountInfomationInputControlSetting();
    void optionButtonSetting();
    void signupButtonSetting();
    
    bool checkSignupButtonEnable(int tag);
    
    virtual void tsEditBoxReturn(extension::EditBox* editBox);

#pragma mark - Error Status Display
    void showIDStringError();
    void showPasswordStringError();
    void showConfirmPasswordStringError();

#pragma mark - EditBox Utiltiy
    bool checkingIDStringInvalidate(const std::string &idString, bool errorDisplay);
    bool checkingPasswordStringInvalidate(const std::string &pwString, bool errorDisplay);
    bool checkingComfirmPasswordStringInvalidate(const std::string &pwString, const std::string &cpwString, bool errorDisplay);
    
    void getProvinceInfoProtocol();
    void responseCallback(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response );
    void popupButtonClicked(PopupLayer *popup, cocos2d::Ref *obj);
    
protected:
    void touchEventForButton(Ref *pSender, ui::TouchEventType type);
    void eventForControlButton(Ref *pSender, extension::Control::EventType type);
    void selectedStateEventForRadioButton(Ref *pSender, TSRadioButtonEventType eventType);
    void selectedStateEventForCheckButton(Ref *pSender, ui::CheckBoxEventType eventType);
    
    TSIconEditBox *_idEditBox;
    TSIconEditBox *_pwEditBox;
    TSIconEditBox *_pwcEditBox;
    TSIconEditBox *_birthEditBox;
    TSIconEditBox *_cityEditBox;
    TSRadioButton *_maleRadio;
    TSRadioButton *_femaleRadio;
    ui::CheckBox* _termAgreeCheckBox;
    extension::ControlButton *_signUpButton;
};

NS_CC_END

#endif /* defined(__SIGNUPLAYER_H__) */
