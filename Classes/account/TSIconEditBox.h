//
//  TSIconEditBox.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 19..
//
//

#ifndef __TSICONEDITBOX_H__
#define __TSICONEDITBOX_H__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"

NS_CC_BEGIN

class TSEditBoxDelegate
{
public:
    /**
     * @js NA
     * @lua NA
     */
    virtual ~TSEditBoxDelegate() {};
    
    /**
     * This method is called when an edit box gains focus after keyboard is shown.
     * @param editBox The edit box object that generated the event.
     * @js NA
     * @lua NA
     */
    virtual void tsEditBoxEditingDidBegin(extension::EditBox* editBox) {};
    
    
    /**
     * This method is called when an edit box loses focus after keyboard is hidden.
     * @param editBox The edit box object that generated the event.
     * @js NA
     * @lua NA
     */
    virtual void tsEditBoxEditingDidEnd(extension::EditBox* editBox) {};
    
    /**
     * This method is called when the edit box text was changed.
     * @param editBox The edit box object that generated the event.
     * @param text The new text.
     * @js NA
     * @lua NA
     */
    virtual void tsEditBoxTextChanged(extension::EditBox* editBox, const std::string& text) {};
    
    /**
     * This method is called when the return button was pressed or the outside area of keyboard was touched.
     * @param editBox The edit box object that generated the event.
     * @js NA
     * @lua NA
     */
    virtual void tsEditBoxReturn(extension::EditBox* editBox) = 0;
    
};


class TSIconEditBox : public LayerColor, public cocos2d::extension::EditBoxDelegate
{
public:
    TSIconEditBox();
    virtual ~TSIconEditBox();
    
    static TSIconEditBox *create(const std::string& iconfilename, const std::string& placeholder, const std::string& text);
    bool init(const std::string& iconfilename,
              const std::string& placeholder, const std::string& text);
    void setInputMode(extension::EditBox::InputMode inputMode);
    void setInputFlag(extension::EditBox::InputFlag inputFlag);
    void showErrorComment(const std::string& errorComment);
    void setInputComment(const std::string& inputComment);
    
    void setText(const char *text);
    const char* getText(void);
    int getTextLength();
    
    void eventForControlButton(Ref *pSender, extension::Control::EventType type);
    
    void setDelegate(TSEditBoxDelegate* pDelegate);
    void editBoxEditingDidBegin(extension::EditBox* editBox);
    void editBoxEditingDidEnd(extension::EditBox* editBox);
    void editBoxTextChanged(extension::EditBox* editBox, const std::string& text);
    void editBoxReturn(extension::EditBox* editBox);
    
    virtual void onEnter();
    virtual void onExit();
    virtual void setTag(int tag);
    void setEnabled(bool enabled);

protected:
    TSEditBoxDelegate*  _delegate;
    extension::Scale9Sprite *_backgroundSprite;
    Sprite *_iconSprite;
    extension::EditBox *_textEditBox;
    extension::ControlButton *_errorCommentButton;
    Label *_inputCommentLabel;
};

NS_CC_END


#endif /* defined(__TSICONEDITBOX_H__) */
