//
//  TSRadioButton.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 20..
//
//

#ifndef __TSRADIOBUTTON_H__
#define __TSRADIOBUTTON_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

NS_CC_BEGIN

typedef enum
{
    TSRADIOBUTTON_STATE_EVENT_SELECTED,
    TSRADIOBUTTON_STATE_EVENT_UNSELECTED
}TSRadioButtonEventType;

typedef void (Ref::*SEL_TSSelectedStateEvent)(Ref*,TSRadioButtonEventType);
#define radiobuttonselectedeventselector(_SELECTOR) (SEL_TSSelectedStateEvent)(&_SELECTOR)


class TSRadioButton : public ui::Widget {
  
    DECLARE_CLASS_GUI_INFO
    
public:
    TSRadioButton();
    virtual ~TSRadioButton();
    
    static TSRadioButton *create();
    static TSRadioButton *create(const std::string& title, const std::string& on, const std::string& off,
                                 bool selected = false, ui::TextureResType texType = ui::UI_TEX_TYPE_LOCAL);
    void setLabel(const std::string& title);
    void loadTextures(const std::string& on, const std::string& off, ui::TextureResType texType);
    void loadTextureOnStatus(const std::string& on,ui::TextureResType texType);
    void loadTextureOffStatus(const std::string& off,ui::TextureResType texType);
    
    void setSelectedState(bool selected);
    bool getSelectedState();
    void addEventListenerRadioButton(Ref *target, SEL_TSSelectedStateEvent selector);
    virtual void onTouchEnded(Touch *touch, Event *unusedEvent);
    virtual const Size& getVirtualRendererSize() const override;
    virtual Node* getVirtualRenderer() override;
    virtual std::string getDescription() const override;

CC_CONSTRUCTOR_ACCESS:
    virtual bool init();
    virtual bool init(const std::string& title, const std::string& on, const std::string& off,
                      bool selected = false, ui::TextureResType texType = ui::UI_TEX_TYPE_LOCAL);

protected:
    virtual void initRenderer() override;
    virtual void onPressStateChangedToNormal() override;
    virtual void onPressStateChangedToPressed() override;
    virtual void onPressStateChangedToDisabled() override;
    void selectedEvent();
    void unSelectedEvent();
    virtual void onSizeChanged() override;
    virtual void updateTextureColor() override;
    virtual void updateTextureOpacity() override;
    virtual void updateTextureRGBA() override;
    virtual void updateFlippedX() override;
    virtual void updateFlippedY() override;
    void backGroundOnStatusTextureScaleChangedWithSize();
    void backGroundOffStatusTextureScaleChangedWithSize();
    virtual Widget* createCloneInstance() override;
    virtual void copySpecialProperties(Widget *widget) override;
    virtual void adaptRenderers() override;
    Size calculateContentSize();

protected:
    Sprite* _backGroundOnStatusRenderer;
    Sprite* _backGroundOffStatusRenderer;
    Label *_titleLabel;
    bool _isSelected;
    
    Ref*       _radioButtonEventListener;
    SEL_TSSelectedStateEvent    _radioButtonEventSelector;
    
    ui::TextureResType _backGroundOnStatusTexType;
    ui::TextureResType _backGroundOffStatusTexType;
    
    std::string _backGroundOnStatusFileName;
    std::string _backGroundOffStatusFileName;
    
    bool _backGroundOnStatusRendererAdaptDirty;
    bool _backGroundOffStatusRendererAdaptDirty;
};

NS_CC_END

#endif /* defined(__TSRADIOBUTTON_H__) */
