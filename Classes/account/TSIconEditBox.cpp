//
//  TSIconEditBox.cpp
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 19..
//
//

#include "TSIconEditBox.h"
#include "ResourceManager.h"

#define DEFAULT_FONT            "Helvetica"

USING_NS_CC;
using namespace ui;
using namespace extension;

#define TAG_SPRT_BACKGROUND         900001
#define TAG_SPRT_ICON               900002
#define TAG_EDTBX_TEXT              900003
#define TAG_CBTN_ERRCMT             900004

TSIconEditBox::TSIconEditBox() :
_backgroundSprite(nullptr),
_iconSprite(nullptr),
_textEditBox(nullptr),
_errorCommentButton(nullptr),
_inputCommentLabel(nullptr),
_delegate(nullptr) {

}

TSIconEditBox::~TSIconEditBox() {
    
}


TSIconEditBox *TSIconEditBox::create(const std::string& iconfilename,
                                     const std::string& placeholder, const std::string& text) {
    TSIconEditBox *editBox = new TSIconEditBox();
    if (editBox && editBox->initWithColor(Color4B(0, 0, 0, 0), TSICONEDITBOX_SIZE_WIDTH, TSICONEDITBOX_SIZE_HEIGHT))
    {
        if (editBox->init(iconfilename, placeholder, text)) {
            editBox->autorelease();
            return editBox;
        }
    }
    CC_SAFE_DELETE(editBox);
    return nullptr;

}

bool TSIconEditBox::init(const std::string& iconfilename, const std::string& placeholder, const std::string& text) {
    _backgroundSprite = Scale9Sprite::create("input.png");//, Rect(0, 0, 510, 70), Rect(35,33,63,37));
    _backgroundSprite->setContentSize(Size(TSICONEDITBOX_SIZE_WIDTH, TSICONEDITBOX_SIZE_HEIGHT));
    _backgroundSprite->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    _backgroundSprite->setPosition(Point(0,0));
    this->addChild(_backgroundSprite, 0);
    
    _iconSprite = Sprite::create(iconfilename);
    _iconSprite->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    _iconSprite->setPosition(TSICONEDITBOX_POSITION_ICON);//Point(20,9)
    this->addChild(_iconSprite);
    
    auto textFieldSize = TSICONEDITBOX_SIZE_TEXTFILED;// Size(392,34);
    auto textFieldPosition = TSICONEDITBOX_POSITION_TEXTFIELD;// Point(20+52+20, 18);
    int fontSize = TSICONEDITBOX_FONTSIZE_TEXTFIELD;// 30;
    auto placeholderColor = Color3B(0xd9, 0xc0, 0xa0);
    auto fontColor = Color3B(0x4f, 0x2c, 0x00);
    
//    TextFieldTTF *textField;
//    textField = TextFieldTTF::textFieldWithPlaceHolder("placeholder", DEFAULT_FONT, 30);
//    textField->setAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
//    textField->setDimensions(textFieldSize.width, textFieldSize.height);
//    textField->setContentSize(textFieldSize);
//    textField->setColor(Color3B(0x00, 0x00, 0x00));
//    textField->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
//    textField->setPosition(Point(20+52+20, 18));
//    this->addChild(textField, 5);
    
    //tuilise_todo : edit모드일 때 폰트 하부가 짤린다.
    _textEditBox = EditBox::create(textFieldSize, Scale9Sprite::create("button_background_alpha.png"));
    _textEditBox->setFontName(DEFAULT_FONT);
    if (!placeholder.empty()) {
        _textEditBox->setPlaceHolder(placeholder.c_str());
    }
    _textEditBox->setPlaceholderFontColor(placeholderColor);
    _textEditBox->setFontSize(fontSize);
    _textEditBox->setFontColor(fontColor);
    if (!text.empty()) {
        _textEditBox->setText(text.c_str());
    }
//    _textEditBox->setMaxLength(20);
    _textEditBox->setReturnType(EditBox::KeyboardReturnType::DONE);
    _textEditBox->setDelegate(this);
    
    _textEditBox->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    _textEditBox->setPosition(textFieldPosition);
    this->addChild(_textEditBox);
    
    return true;
}

void TSIconEditBox::showErrorComment(const std::string &errorComment) {
    _backgroundSprite->setVisible(false);
    _iconSprite->setVisible(false);
    _textEditBox->setEnabled(false);
    _textEditBox->setVisible(false);
    
    if (_errorCommentButton) {
        _errorCommentButton->removeTargetWithActionForControlEvents(this, cccontrol_selector(TSIconEditBox::eventForControlButton), Control::EventType::TOUCH_UP_INSIDE);
        this->removeChild(_errorCommentButton);
        _errorCommentButton = nullptr;
    }

    Scale9Sprite *normal = Scale9Sprite::create("input_error.png",
                                                Rect(0,0,ACCOUNTLAYER_BUTTON_NORMAL_01_WIDTH,ACCOUNTLAYER_BUTTON_NORMAL_01_HEIGHT),
                                                Rect(ACCOUNTLAYER_BUTTON_NORMAL_01_INLEFT, ACCOUNTLAYER_BUTTON_NORMAL_01_INTOP,
                                                     ACCOUNTLAYER_BUTTON_NORMAL_01_INWIDTH, ACCOUNTLAYER_BUTTON_NORMAL_01_INHEIGHT));
    Label *commentLabel = Label::createWithSystemFont(errorComment, DEFAULT_FONT, TSICONEDITBOX_FONTSIZE_ERRORCOMMENT,
                                                      TSICONEDITBOX_SIZE_ERRORCOMMENT, TextHAlignment::CENTER, TextVAlignment::CENTER);//Size(458,60)
//    commentLabel->setColor(Color3B(0xfe, 0xc3, 0x46));//이 함수 불러줄 필요 없이 setTitleColorForState 함수 불러주면 된다.
    _errorCommentButton = ControlButton::create();
    _errorCommentButton->setBackgroundSpriteForState(normal, Control::State::NORMAL);
//    _errorCommentButton->setTitleForState(errorComment, Control::State::NORMAL);
    _errorCommentButton->setTitleLabelForState(commentLabel, Control::State::NORMAL);
//    _errorCommentButton->setTitleTTFForState("Helvetica", Control::State::NORMAL);//text가 아니라 Label일 때는 이함수 불러주면 text가 보이지 않는다.
//    _errorCommentButton->setTitleTTFSizeForState(26, Control::State::NORMAL);//text가 아니라 Label일 때는 이함수 불러주면 text가 보이지 않는다.
    _errorCommentButton->setTitleColorForState(Color3B(0xfe, 0xc3, 0x46), Control::State::NORMAL);
    _errorCommentButton->setPreferredSize(Size(TSICONEDITBOX_SIZE_WIDTH, TSICONEDITBOX_SIZE_HEIGHT));//이 함수를 불러주지 않으면 Scale9Sprite가 정상 작동하지 않는다.
    _errorCommentButton->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    _errorCommentButton->setPosition(Point(0, 0));
    _errorCommentButton->addTargetWithActionForControlEvents(this, cccontrol_selector(TSIconEditBox::eventForControlButton), Control::EventType::TOUCH_UP_INSIDE);
    this->addChild(_errorCommentButton);
}

void TSIconEditBox::setInputComment(const std::string &inputComment) {
    int xpos = 0;
    int labelWidth = 0;
    const char * placeHolder = _textEditBox->getPlaceHolder();
    int placeHolderLength = strlen(placeHolder);
    if (placeHolderLength > 16 ) {//tuilise_todo : 가변 조건 찾자.
        return;
    }

    if (placeHolderLength <= 8) { //임시 코드
        xpos = TSICONEDITBOX_POSITION_INPUTCOMMENT1_X;//20+52+20+128+12;//tuilise_todo : 128 값 gui_guide 문서에 안나와 있음. 물어봐랴 함.
        labelWidth = TSICONEDITBOX_SIZE_INPUTCOMMENT_LABEL_WIDTHDELTA*2;
    } else {
        xpos = TSICONEDITBOX_POSITION_INPUTCOMMENT2_X;//20+52+20+128*2+12;//tuilise_todo : 128 값 gui_guide 문서에 안나와 있음. 물어봐랴 함.
        labelWidth = TSICONEDITBOX_SIZE_INPUTCOMMENT_LABEL_WIDTHDELTA;
    }
    int ypos = TSICONEDITBOX_POSITION_INPUTCOMMENT_Y;//(70-26*2)/2;
    
    _inputCommentLabel = Label::createWithSystemFont(inputComment, DEFAULT_FONT, TSICONEDITBOX_FONTSIZE_INPUTCOMMENT,
                                                     Size(labelWidth,TSICONEDITBOX_SIZE_INPUTCOMMENT_LABEL_HEIGHT),
                                                     TextHAlignment::LEFT, TextVAlignment::CENTER);
    _inputCommentLabel->setColor(Color3B(0xac, 0x88, 0x5a));//이 함수 불러줄 필요 없이 setTitleColorForState 함수 불러주면 된다.
    _inputCommentLabel->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    _inputCommentLabel->setPosition(Point(xpos, ypos));
    this->addChild(_inputCommentLabel, 10);
    
    
    int textLength = strlen(_textEditBox->getText());
    if (textLength > 0) {
        _inputCommentLabel->setVisible(false);//tuilise_todo : 이 방식이 gui guide 에 맞나 화인 필요
    }
}

void TSIconEditBox::setInputMode(extension::EditBox::InputMode inputMode) {
    _textEditBox->setInputMode(inputMode);
}

void TSIconEditBox::setInputFlag(extension::EditBox::InputFlag inputFlag) {
    _textEditBox->setInputFlag(inputFlag);
}

void TSIconEditBox::setText(const char *text) {
    _textEditBox->setText(text);
}

const char* TSIconEditBox::getText(void) {
    return _textEditBox->getText();
}

int TSIconEditBox::getTextLength() {
    const char *inputText = _textEditBox->getText();
    return strlen(inputText);
}

void TSIconEditBox::eventForControlButton(Ref *pSender, Control::EventType type) {//ControlButton class callback
    switch (type) {
        case Control::EventType::TOUCH_UP_INSIDE: {
//            _errorCommentButton->setEnabled(false);
//            _errorCommentButton->setVisible(false);
            if (_errorCommentButton) {
                _errorCommentButton->removeTargetWithActionForControlEvents(this, cccontrol_selector(TSIconEditBox::eventForControlButton), Control::EventType::TOUCH_UP_INSIDE);
                this->removeChild(_errorCommentButton);
                _errorCommentButton = nullptr;
            }
            
            _backgroundSprite->setVisible(true);
            _iconSprite->setVisible(true);
            _textEditBox->setEnabled(true);
            _textEditBox->setVisible(true);
        } break;
        default:
            break;
    }
}

void TSIconEditBox::onEnter() {
    Layer::onEnter();
}

void TSIconEditBox::onExit() {
    Layer::onExit();
}

#pragma mark - EditBox Delegate
void TSIconEditBox::setDelegate(TSEditBoxDelegate* pDelegate) {
    _delegate = pDelegate;
}

void TSIconEditBox::editBoxEditingDidBegin(EditBox* editBox) {
    if (_inputCommentLabel != nullptr) {
        _inputCommentLabel->setVisible(false);//tuilise_todo : 이 방식이 gui guide 에 맞나 화인 필요
    }
    if (_delegate) {
        _delegate->tsEditBoxEditingDidBegin(editBox);
    }
}

void TSIconEditBox::editBoxEditingDidEnd(EditBox* editBox) {
    if (_inputCommentLabel != nullptr) {
        if (strlen(_textEditBox->getText()) == 0) {
            _inputCommentLabel->setVisible(true);//tuilise_todo : 이 방식이 gui guide 에 맞나 화인 필요
        }
    }
    if (_delegate) {
        _delegate->tsEditBoxEditingDidEnd(editBox);
    }
}

void TSIconEditBox::editBoxTextChanged(EditBox* editBox, const std::string& text) {
    if (_delegate) {
        _delegate->tsEditBoxTextChanged(editBox, text);
    }
}

void TSIconEditBox::editBoxReturn(cocos2d::extension::EditBox* editBox) {
    log("editBox %p was returned !",editBox);
    if (_delegate) {
        _delegate->tsEditBoxReturn(editBox);
    }
}

void TSIconEditBox::setTag(int tag) {
    Node::setTag(tag);
    _textEditBox->setTag(tag);
}

void TSIconEditBox::setEnabled(bool enabled) {
    _textEditBox->setEnabled(enabled);
}

