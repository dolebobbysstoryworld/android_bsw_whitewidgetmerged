//
//  LoginLayer.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 16..
//
//

#ifndef __LOGINLAYER_H__
#define __LOGINLAYER_H__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "TSIconEditBox.h"
#include "DoleAPI.h"
#include "PopupLayer.h"

NS_CC_BEGIN

class Loginlayer : public LayerColor, public TSEditBoxDelegate, public PopupLayerDelegate
{
#pragma mark - class init
public:
    Loginlayer();
    virtual ~Loginlayer();
    static Loginlayer* create();
    bool init();
    virtual void onEnter();
    virtual void onExit();
    
protected:
#pragma mark - UI layout Setting
    void characterImageLayoutSetting();
    void imageLayoutSetting();
    void idInputControlSetting(const Point& position);
    void loginControlSetting();
    void loginButtonSetting();
    void accountManageControlSetting();
    void accountManageControlAtMenuSetting();
    void skipControlSetting();
    void forgotPasswordStatusUISetting();
    
#pragma mark - Error Status Display
    void showIDStringError();
    void showPasswordStringError();
    
#pragma mark - UI Controls Event Callback
    void menuItemCallback(Ref* pSender);
    void touchEventForUIButton(Ref *pSender, ui::TouchEventType type);
    void eventForControlButton(Ref *pSender, extension::Control::EventType type);

#pragma mark - EditBox Delegate
    virtual void tsEditBoxReturn(extension::EditBox* editBox);//EditBox Delegate
    
#pragma mark - EditBox Utiltiy
    bool checkingIDStringInvalidate(const std::string &idString);
    bool checkingPasswordStringInvalidate(const std::string &pwString);

    
    void getAuthKeyMobileProtocol(std::string userID, std::string password);
    void responseCallback(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response );
    void popupButtonClicked(PopupLayer *popup, cocos2d::Ref *obj);
    
#pragma mark - class Member Value
    TSIconEditBox *_idEditBox;
    TSIconEditBox *_pwEditBox;
    extension::ControlButton *_loginButton;
    Menu *_accountMenu;
    bool _isForgotPasswordStatus;
    ui::Button *_skipButton;
    Label *_skipLabel;
};

NS_CC_END

#endif /* defined(__LOGINLAYER_H__) */
