//
//  SignTermsLayer.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 20..
//
//

#ifndef __bobby__SignTermsLayer__
#define __bobby__SignTermsLayer__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "AccountLayerProtocol.h"
#include "DoleAPI.h"
#include "PopupLayer.h"

NS_CC_BEGIN
using namespace ui;

class SignTermsLayer : public LayerColor, public PopupLayerDelegate
{
public:
    SignTermsLayer();
    virtual ~SignTermsLayer();
    static SignTermsLayer* create();
    bool init();
    virtual void onEnter();
    virtual void onExit();
    
    void characterImageLayoutSetting();
    void imageLayoutSetting();
    void termsTextSetting();
    void checkBoxSetting();
    void buttonSetting();
    void signupProtocol();
    void responseCallback(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response );
    void popupButtonClicked(PopupLayer *popup, cocos2d::Ref *obj);
    void selectedStateEventForCheckButton(Ref *pSender, ui::CheckBoxEventType eventType);
    
protected:
    void eventForControlButton(Ref *pSender, extension::Control::EventType type);
    
    CheckBox *_termsCheckBox;
    CheckBox *_privacyCheckBox;
    ControlButton *_cancelButton;
    ControlButton *_nextButton;
};

NS_CC_END


#endif /* defined(__bobby__SignTermsLayer__) */
