//
//  TSDimLayer.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 16..
//
//

#ifndef __ACCOUNTLAYER_H__
#define __ACCOUNTLAYER_H__

#include "cocos2d.h"
#include "LoginLayer.h"
#include "SignupLayer.h"
#include "AccountLayerProtocol.h"

NS_CC_BEGIN

class AccountLayer : public LayerColor, public AccountLayerProtocol
{
public:
    AccountLayer();
    virtual ~AccountLayer();
    static AccountLayer* create(bool firstRun);
    bool init();
    void setDelegate(AccountLayerDelegate * pDelegate) { _delegate = pDelegate; }
    virtual void onEnter();
    virtual void onExit();
    void closeChildLayer();
    void nextPageLayer(int page);
    bool getFirstRun() { return _isfirstRun; }
    
    void setAccountInfo(const char* idString, const char* pwString, const char* birthdayString, const char* cityString, int gender);
    DoleAccountInfomation *getAccountInfo() { return &_accountInfo; }
    
    void loginDidEnded(bool result);
    void signupDidEnded(bool result);
    
protected:
    void setFirstRun(bool firstRun);
    void closeMenuCallback(Ref* pSender);
    
    bool _isfirstRun;
    DoleAccountInfomation _accountInfo;
    AccountLayerDelegate *_delegate;
};

NS_CC_END

#endif /* defined(__ACCOUNTLAYER_H__) */
