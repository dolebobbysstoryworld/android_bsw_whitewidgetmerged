//
//  SignTermsLayer.cpp
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 20..
//
//

#include "SignTermsLayer.h"
#include "Define.h"

USING_NS_CC;
using namespace extension;

#define TAG_CHECK_TERMS         30001
#define TAG_CHECK_PRIVACY       30002
#define TAG_CBTN_CANCEL         30003
#define TAG_CBTN_NEXT           30004

SignTermsLayer::SignTermsLayer():
_termsCheckBox(nullptr),
_privacyCheckBox(nullptr) {
    
}

SignTermsLayer::~SignTermsLayer() {}

SignTermsLayer* SignTermsLayer::create()
{
    SignTermsLayer *layer = new SignTermsLayer();
    if (layer && layer->init())
    {
        layer->autorelease();
        return layer;
    }
    CC_SAFE_DELETE(layer);
    return nullptr;
}

void SignTermsLayer::onEnter() {
    Layer::onEnter();
}

void SignTermsLayer::onExit() {
    Layer::onExit();
}

bool SignTermsLayer::init() {
    if ( !LayerColor::initWithColor(Color4B(0, 0, 0, 0), AccountLayerProtocol::getLayerSize().width, AccountLayerProtocol::getLayerSize().height) ) return false;
    
    this->imageLayoutSetting();
    this->termsTextSetting();
    this->checkBoxSetting();
    this->buttonSetting();
    
    return true;
}

void SignTermsLayer::characterImageLayoutSetting() {
    float width = Director::getInstance()->getOpenGLView()->getFrameSize().width;
    if ((width == 1024 || width == 2048) && IsIOS() == true) {//pad 계열만 표시
        Sprite *lionSprite = Sprite::create("full_popup_character_01.png");
        lionSprite->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        lionSprite->setPosition(ACCOUNTLAYER_POSITION_FULL_POPUP_CHARACTER_01);//Point(-143, 0)
        this->addChild(lionSprite);
        
        Sprite *racoonSprite = Sprite::create("full_popup_character_02.png");
        racoonSprite->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        racoonSprite->setPosition(ACCOUNTLAYER_POSITION_FULL_POPUP_CHARACTER_02);//Point(680, 472+210)
        this->addChild(racoonSprite);
    }
}

void SignTermsLayer::imageLayoutSetting() {
    Sprite *backgroundMap = Sprite::create("bg_map_full.png");
    backgroundMap->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    backgroundMap->setPosition(Point::ZERO);
    this->addChild(backgroundMap, 0);
    
    Sprite *backgroundFactor = Sprite::create("bg_map_factor.png");
    backgroundFactor->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    backgroundFactor->setPosition(Point::ZERO);
    this->addChild(backgroundFactor, 0);
    
    this->characterImageLayoutSetting();
    
    Button *closeButton = Button::create("btn_cancel_normal.png", "btn_cancel_press.png");
    closeButton->setContentSize(ACCOUNTLAYER_SIZE_BTN_CANCEL_NORMAL);//Size(80, 80)
    closeButton->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    closeButton->setPosition(ACCOUNTLAYER_POSITION_BTN_CANCEL_NORMAL);//Point(692,1085)
    closeButton->setTag(30101);
//    closeButton->addTouchEventListener(this, toucheventselector(SignTermsLayer::touchEventForButton));
//    this->addChild(closeButton);
}

void SignTermsLayer::termsTextSetting() {
    {
        Sprite *top = Sprite::create("terms_box_top.png");
        top->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        top->setPosition(SIGNTERMSLAYER_POSITION_TERMTEXTTOP);//Point(160, 1224-(102+20))
        Sprite *bottom = Sprite::create("terms_box_btm.png");
        bottom->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        bottom->setPosition(SIGNTERMSLAYER_POSITION_TERMTEXTBOTTOM);//Point(160, 1224-(102+20+318+20))
        Scale9Sprite *middle =
        Scale9Sprite::create("terms_box_mid.png", Rect(0,0,SIGNTERMSLAYER_DELTA_TERMTEXTMID_WIDTH,//510
                                                       SIGNTERMSLAYER_DELTA_TERMTEXTMID_HEIGHT-(top->getContentSize().height+bottom->getContentSize().height)));//358
        middle->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        middle->setPosition(SIGNTERMSLAYER_POSITION_TERMTEXTMID);//Point(160, 1224-(102+20+318))
        this->addChild(top);
        this->addChild(middle);
        this->addChild(bottom);
        
        Label *title = Label::createWithSystemFont("Terms of Service Contents", "Helvetica", SIGNTERMSLAYER_FONTSIZE_TERMTEXT_TITLE);//28
        title->setColor(Color3B(0x4f, 0x2c, 0x00));
        title->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        title->setPosition(SIGNTERMSLAYER_POSITION_TERMTEXT_TITLE);//Point(160+20, 1224-(106+22+34))
        this->addChild(title, 10);
        
        const char *termsText = "Terms of Service Contents\nThe WebKit, WebCore and JavaScriptCore software is open source software with portions licensed under a BSD license (see Apple's example below) and portions licensed under the GNU Library General Public License Version 2. Please refer to the individual files to determine the license terms that apply to that file. You may obtain a complete machine-readable copy of the source code for the LGPL-licensed portions under the terms of LGPL, without charge except for the cost of media, shipping, and handling, upon written request to Apple.  The WebKit software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the LGPL for more details; a copy of the LGPL is included with this product. The khtml software is released under the GNU Library General Public License Version 2. The kjs software is released under the GNU Library General Public License Version 2.  The authors also thank the following people for their help:  Richard Moore, Daegeun Lee, Marco Pinelli, and Christian Kirsch.  The kcanvas, kdom and ksvg2 software is released under the GNU Library General Public License Version 2.";
        
        Label *termsTextLabel =
        Label::createWithSystemFont(termsText, FONT_NAME, SIGNTERMSLAYER_FONTSIZE_TERMTEXT_TEXT_LABEL);
        termsTextLabel->setColor(Color3B(0x66, 0x4b, 0x2a));
        termsTextLabel->setWidth(SIGNTERMSLAYER_SIZE_TERMTEXT_TEXT_LABEL_WIDTH);//460
        termsTextLabel->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        termsTextLabel->setPosition(Point::ZERO);
        
        ScrollView *termsScrollview = ScrollView::create();
        termsScrollview->setSize(SIGNTERMSLAYER_SIZE_TERMTEXT_SCROLLVIEW);//Size(470, 385-(22+34+21+2+18))
        termsScrollview->setDirection(SCROLLVIEW_DIR::SCROLLVIEW_DIR_BOTH);
        termsScrollview->setInnerContainerSize(termsTextLabel->getContentSize());
        termsScrollview->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        termsScrollview->setPosition(SIGNTERMSLAYER_POSITION_TERMTEXT_SCROLLVIEW);//Point(160+20, 1224-(102+358))
        termsScrollview->addChild(termsTextLabel);
        this->addChild(termsScrollview);
    }
    {
        Sprite *top = Sprite::create("terms_box_top.png");
        top->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        top->setPosition(SIGNTERMSLAYER_POSITION_POLICYTEXTTOP);//Point(160, 106+72+36+56+18+358-20)
        Sprite *bottom = Sprite::create("terms_box_btm.png");
        bottom->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        bottom->setPosition(SIGNTERMSLAYER_POSITION_POLICYTEXTBOTTOM);//Point(160, 106+72+36+56+18)
        Scale9Sprite *middle =
        Scale9Sprite::create("terms_box_mid.png", Rect(0,0,SIGNTERMSLAYER_DELTA_POLICYTEXTMID_WIDTH,//510
                                                       SIGNTERMSLAYER_DELTA_POLICYTEXTMID_HEIGHT-(top->getContentSize().height+bottom->getContentSize().height)));//358
        middle->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        middle->setPosition(SIGNTERMSLAYER_POSITION_POLICYTEXTMID);//Point(160, 106+72+36+56+18+20)
        this->addChild(top);
        this->addChild(middle);
        this->addChild(bottom);
        
        Label *title = Label::createWithSystemFont("Privacy Policy Contents", FONT_NAME, SIGNTERMSLAYER_FONTSIZE_POLICYTEXT_TITLE);//28
        title->setColor(Color3B(0x4f, 0x2c, 0x00));
        title->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        title->setPosition(SIGNTERMSLAYER_POSITION_POLICYTEXT_TITLE);//Point(160+20, 106+72+36+56+18+358-(22+34))
        this->addChild(title, 10);
        
        const char *termsText = "Privacy Policy Contents\nThe WebKit, WebCore and JavaScriptCore software is open source software with portions licensed under a BSD license (see Apple's example below) and portions licensed under the GNU Library General Public License Version 2. Please refer to the individual files to determine the license terms that apply to that file. You may obtain a complete machine-readable copy of the source code for the LGPL-licensed portions under the terms of LGPL, without charge except for the cost of media, shipping, and handling, upon written request to Apple.  The WebKit software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the LGPL for more details; a copy of the LGPL is included with this product. The khtml software is released under the GNU Library General Public License Version 2. The kjs software is released under the GNU Library General Public License Version 2.  The authors also thank the following people for their help:  Richard Moore, Daegeun Lee, Marco Pinelli, and Christian Kirsch.  The kcanvas, kdom and ksvg2 software is released under the GNU Library General Public License Version 2.";
        
        Label *termsTextLabel =
        Label::createWithSystemFont(termsText, FONT_NAME, SIGNTERMSLAYER_FONTSIZE_POLICYTEXT_TEXT_LABEL);//24
        termsTextLabel->setColor(Color3B(0x66, 0x4b, 0x2a));
        termsTextLabel->setWidth(SIGNTERMSLAYER_SIZE_TERMTEXT_TEXT_LABEL_WIDTH);//460
        termsTextLabel->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        termsTextLabel->setPosition(Point::ZERO);
        
        ScrollView *termsScrollview = ScrollView::create();
        termsScrollview->setSize(SIGNTERMSLAYER_SIZE_POLICYTEXT_SCROLLVIEW);//Size(470, 385-(22+34+21+2+18))
        termsScrollview->setDirection(SCROLLVIEW_DIR::SCROLLVIEW_DIR_BOTH);
        termsScrollview->setInnerContainerSize(termsTextLabel->getContentSize());
        termsScrollview->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        termsScrollview->setPosition(SIGNTERMSLAYER_POSITION_POLICYTEXT_SCROLLVIEW);//Point(160+20, 106+72+36+56+18)
        termsScrollview->addChild(termsTextLabel);
        this->addChild(termsScrollview);
    }
}

void SignTermsLayer::checkBoxSetting() {
    {
        _termsCheckBox = CheckBox::create("checkbox_off.png", "checkbox_off.png", "checkbox_on.png", "checkbox_off.png", "checkbox_on.png");
        _termsCheckBox->setTag(TAG_CHECK_TERMS);
//        _termsCheckBox->setSelectedState(true);
        _termsCheckBox->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        _termsCheckBox->setPosition(Point(160, 1224-(56+18+358+102)));
        _termsCheckBox->addEventListenerCheckBox(this, checkboxselectedeventselector(SignTermsLayer::selectedStateEventForCheckButton));
        this->addChild(_termsCheckBox);
        
        Label *checkBoxText = Label::createWithSystemFont("Conditions and Privacy Policy", "Helvetica", 26);
        checkBoxText->setColor(Color3B(0x4f, 0x2c, 0x00));
        checkBoxText->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        checkBoxText->setPosition(Point(160+54+18, (1224-(56+18+358+102))+((_termsCheckBox->getContentSize().height-checkBoxText->getContentSize().height)/2)));
        this->addChild(checkBoxText);
    }
    {
        _privacyCheckBox = CheckBox::create("checkbox_off.png", "checkbox_off.png", "checkbox_on.png", "checkbox_off.png", "checkbox_on.png");
        _privacyCheckBox->setTag(TAG_CHECK_TERMS);
//        _privacyCheckBox->setSelectedState(true);
        _privacyCheckBox->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        _privacyCheckBox->setPosition(Point(160, 106+72+36));
        _privacyCheckBox->addEventListenerCheckBox(this, checkboxselectedeventselector(SignTermsLayer::selectedStateEventForCheckButton));
        this->addChild(_privacyCheckBox);
        
        Label *checkBoxText = Label::createWithSystemFont("Conditions and Privacy Policy", "Helvetica", 26);
        checkBoxText->setColor(Color3B(0x4f, 0x2c, 0x00));
        checkBoxText->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        checkBoxText->setPosition(Point(160+54+18, 106+72+36+((_privacyCheckBox->getContentSize().height-checkBoxText->getContentSize().height)/2)));
        this->addChild(checkBoxText);
        
    }
}

void SignTermsLayer::buttonSetting() {
    {
        Scale9Sprite *normal = Scale9Sprite::create("button_normal_02.png", Rect(0,0,98,72), Rect(34, 35, 30, 2));
        Scale9Sprite *press = Scale9Sprite::create("button_press_02.png", Rect(0,0,98,72), Rect(34, 35, 30, 2));
        Scale9Sprite *dim = Scale9Sprite::create("button_dim_02.png", Rect(0,0,98,72), Rect(34, 35, 30, 2));
        _cancelButton = ControlButton::create();
        _cancelButton->setTag(TAG_CBTN_CANCEL);
        _cancelButton->setBackgroundSpriteForState(normal, Control::State::NORMAL);
        _cancelButton->setBackgroundSpriteForState(press, Control::State::HIGH_LIGHTED);
        _cancelButton->setBackgroundSpriteForState(dim, Control::State::DISABLED);
        _cancelButton->setTitleForState("Cancel", Control::State::NORMAL);
        _cancelButton->setTitleTTFForState("Helvetica", Control::State::NORMAL);
        _cancelButton->setTitleTTFSizeForState(34, Control::State::NORMAL);
        _cancelButton->setTitleColorForState(Color3B(0x79, 0x1b, 0x00), Control::State::NORMAL);
        _cancelButton->setPreferredSize(Size(244, 72));
        _cancelButton->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        _cancelButton->setPosition(Point(160, 106));
        _cancelButton->addTargetWithActionForControlEvents(this, cccontrol_selector(SignTermsLayer::eventForControlButton), Control::EventType::TOUCH_UP_INSIDE);
        this->addChild(_cancelButton);
    }
    {
        Scale9Sprite *normal = Scale9Sprite::create("button_normal_01.png", Rect(0,0,98,72), Rect(34, 35, 30, 2));
        Scale9Sprite *press = Scale9Sprite::create("button_press_01.png", Rect(0,0,98,72), Rect(34, 35, 30, 2));
        Scale9Sprite *dim = Scale9Sprite::create("button_dim_01.png", Rect(0,0,98,72), Rect(34, 35, 30, 2));
        _nextButton = ControlButton::create();
        _nextButton->setTag(TAG_CBTN_NEXT);
        _nextButton->setBackgroundSpriteForState(normal, Control::State::NORMAL);
        _nextButton->setBackgroundSpriteForState(press, Control::State::HIGH_LIGHTED);
        _nextButton->setBackgroundSpriteForState(dim, Control::State::DISABLED);
        _nextButton->setTitleForState("Next", Control::State::NORMAL);
        _nextButton->setTitleTTFForState("Helvetica", Control::State::NORMAL);
        _nextButton->setTitleTTFSizeForState(34, Control::State::NORMAL);
        _nextButton->setTitleColorForState(Color3B(0x79, 0x1b, 0x00), Control::State::NORMAL);
        _nextButton->setPreferredSize(Size(244, 72));
        _nextButton->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        _nextButton->setPosition(Point(160+244+20, 106));
        _nextButton->addTargetWithActionForControlEvents(this, cccontrol_selector(SignTermsLayer::eventForControlButton), Control::EventType::TOUCH_UP_INSIDE);
        _nextButton->setEnabled(false);
        this->addChild(_nextButton);
    }
}

void SignTermsLayer::selectedStateEventForCheckButton(Ref *pSender, CheckBoxEventType eventType) {
    if ( (_termsCheckBox->getSelectedState() == true) && (_privacyCheckBox->getSelectedState() == true) ) {
        _nextButton->setEnabled(true);
    }
}

void SignTermsLayer::eventForControlButton(Ref *pSender, Control::EventType type) {
    int tag = ((Node*)pSender)->getTag();
    switch (type) {
        case Control::EventType::TOUCH_UP_INSIDE: {
            switch (tag) {
                case TAG_CBTN_CANCEL:{
                    AccountLayerProtocol *parent = dynamic_cast<AccountLayerProtocol*>(this->getParent());
                    parent->closeChildLayer();
                } break;
                case TAG_CBTN_NEXT:{
                    this->signupProtocol();
                } break;
                default: break;
            }
        } break;
        default:
            break;
    }
}

void SignTermsLayer::signupProtocol() {
    AccountLayerProtocol *parent = dynamic_cast<AccountLayerProtocol*>(this->getParent());
//    DoleAccountInfomation *pAccountInfo = parent->getAccountInfo();
//    DoleAPI* api = DoleAPI::getInstance();
//    api->mobileSignUp(pAccountInfo->idString,//string userID,
//                      pAccountInfo->pwString,//string password,
//                      pAccountInfo->idString,//string email,
//                      pAccountInfo->birthString,//string birthday,
//                      pAccountInfo->gender,//int gender,
//                      "English",//string language,
//                      "",//"SNS001",//string snsCode,
//                      "",//"mhlee@nemustech.com",//string snsID,
//                      "",//"mhlee",//string snsUserName,
//                      "KR",//string standaryCountryCode,
//                      "",//string UUID,
//                      pAccountInfo->cityString,//string addressMain,
//                      "1.1.1.1",//string clientIP,
//                      this,//cocos2d::Ref *target,
//                      httpresponse_selector(SignTermsLayer::responseCallback)//cocos2d::network::SEL_HttpResponse pSelector
//                      );
}

void SignTermsLayer::responseCallback(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response )
{
    if (!response) return;
    
    printf("Tag: %s\n", response->getHttpRequest()->getTag());
    std::vector<char>*buffer = response->getResponseData();
    std::string str(buffer->begin(), buffer->end());
    
    printf("Json: %s\n", str.c_str());
    
    if (strlen(str.c_str()) > 0) {
        rapidjson::Document ret;
        ret.Parse<0>(str.c_str());
        bool retValue = ret["Return"].GetBool();
        int retCode = ret["ReturnCode"].GetInt();
        printf("Parsed Data: retValue: %d, retCode: %d\n", retValue, retCode);
        AccountLayerProtocol *parent = dynamic_cast<AccountLayerProtocol*>(this->getParent());
        if (retValue) {
            //signup info local saving
            DoleAccountInfomation *pAccountInfo = parent->getAccountInfo();
            UserDefault *userDefault = UserDefault::getInstance();
            int userNo = ret["UserNo"].GetInt();
            userDefault->setIntegerForKey(UDKEY_INTEGER_USERNO, userNo);
            userDefault->setStringForKey(UDKEY_STRING_LOGINID, pAccountInfo->idString);
            userDefault->setStringForKey(UDKEY_STRING_LOGINPW, pAccountInfo->pwString);
            userDefault->setStringForKey(UDKEY_STRING_EMAIL, pAccountInfo->idString);
            userDefault->setStringForKey(UDKEY_STRING_BIRTHDAY, pAccountInfo->birthString);
            userDefault->setIntegerForKey(UDKEY_INTEGER_GENDER, pAccountInfo->gender);
            userDefault->setStringForKey(UDKEY_STRING_LANGUAGE, "English");
            userDefault->setStringForKey(UDKEY_STRING_SNSCODE, "");
            userDefault->setStringForKey(UDKEY_STRING_SNSID, "");
            userDefault->setStringForKey(UDKEY_STRING_SNSNAME, "");
            userDefault->setStringForKey(UDKEY_STRING_COUNTRY, "KR");
            userDefault->setStringForKey(UDKEY_STRING_UUID, "");
            userDefault->setStringForKey(UDKEY_STRING_CITY, pAccountInfo->cityString);
            
            log("signup success UserNo : %d\n", userNo);
            parent->signupDidEnded(true);
            parent->nextPageLayer(1);
            this->removeFromParentAndCleanup(true);
        } else {
            //Error Display
            parent->signupDidEnded(false);
            __String *failMessgge = __String::createWithFormat("Sign up Fail : code : %d", retCode);
            PopupLayer *popup = PopupLayer::create(POPUP_TYPE_WARNING, failMessgge->getCString(), "Cancel", "OK");
            popup->setDelegate(this);
            popup->setPosition(Point::ZERO);
            this->addChild(popup);
        }
    }
}

void SignTermsLayer::popupButtonClicked(PopupLayer *popup, cocos2d::Ref *obj) {
    
}