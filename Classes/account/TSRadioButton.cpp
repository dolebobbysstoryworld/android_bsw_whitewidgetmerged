//
//  TSRadioButton.cpp
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 20..
//
//

#include "TSRadioButton.h"
#include "ResourceManager.h"

USING_NS_CC;
using namespace ui;

static const int BACKGROUNDON_RENDERER_Z = (-1);
static const int BACKGROUNDOFF_RENDERER_Z = (-1);
static const int BACKGROUNDLABEL_RENDERER_Z = (-1);

TSRadioButton::TSRadioButton():
_backGroundOnStatusRenderer(nullptr),
_backGroundOffStatusRenderer(nullptr),
_titleLabel(nullptr),
_isSelected(false),
_radioButtonEventListener(nullptr),
_radioButtonEventSelector(nullptr),
_backGroundOnStatusTexType(UI_TEX_TYPE_LOCAL),
_backGroundOffStatusTexType(UI_TEX_TYPE_LOCAL),
_backGroundOnStatusFileName(""),
_backGroundOffStatusFileName(""),
_backGroundOnStatusRendererAdaptDirty(true),
_backGroundOffStatusRendererAdaptDirty(true)
{
    
}

TSRadioButton::~TSRadioButton() {
    
}

TSRadioButton* TSRadioButton::create()
{
    TSRadioButton* widget = new TSRadioButton();
    if (widget && widget->init())
    {
        widget->autorelease();
        return widget;
    }
    CC_SAFE_DELETE(widget);
    return nullptr;
}

TSRadioButton *TSRadioButton::create(const std::string& title, const std::string& on, const std::string& off, bool selected, TextureResType texType) {
    TSRadioButton *pWidget = new TSRadioButton;
    if (pWidget && pWidget->init(title, on, off, selected, texType)) {
        pWidget->autorelease();
        return pWidget;
    }
    CC_SAFE_DELETE(pWidget);
    return nullptr;
}

bool TSRadioButton::init()
{
    if (Widget::init())
    {
        setSelectedState(false);
        setTouchEnabled(true);
        return true;
    }
    return false;
}

bool TSRadioButton::init(const std::string& title, const std::string& on, const std::string& off, bool selected, TextureResType texType) {
    bool ret = true;
    do {
        if (!Widget::init()) {
            ret = false;
            break;
        }
        
        setSelectedState(selected);
        setTouchEnabled(true);
        loadTextures(on,off,texType);
        setLabel(title);
    } while (0);
    return ret;
}

void TSRadioButton::initRenderer()
{
    _backGroundOnStatusRenderer = Sprite::create();
    _backGroundOffStatusRenderer = Sprite::create();
//    _titleLabel = Label::create();
    
    addProtectedChild(_backGroundOffStatusRenderer, BACKGROUNDOFF_RENDERER_Z, -1);
    addProtectedChild(_backGroundOnStatusRenderer, BACKGROUNDON_RENDERER_Z, -1);
//    addProtectedChild(_titleLabel, BACKGROUNDLABEL_RENDERER_Z, -1);
}

Size TSRadioButton::calculateContentSize() {
    Size spriteContentSize = _backGroundOffStatusRenderer->getContentSize();
    Size labelContentSize;
    Size contentSize;
    if (_titleLabel) {
        labelContentSize = _titleLabel->getContentSize();
        contentSize = Size(spriteContentSize.width + spriteContentSize.width*0.3 + labelContentSize.width, spriteContentSize.height);
    } else {
        return spriteContentSize;
    }
    return contentSize;
}

void TSRadioButton::setLabel(const std::string& title) {
    _titleLabel = Label::createWithSystemFont(title, "Helvetica", ACCOUNTLAYER_FONTSIZE_RADIOLABEL);
    _titleLabel->setColor(Color3B(0x4f, 0x2c, 0x00));
    addProtectedChild(_titleLabel, BACKGROUNDLABEL_RENDERER_Z, -1);
    
    Size spriteContentSize = _backGroundOffStatusRenderer->getContentSize();
    Size labelSize = _titleLabel->getContentSize();
    _titleLabel->setVerticalAlignment(TextVAlignment::CENTER);
    _titleLabel->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    _titleLabel->setPosition(Point(spriteContentSize.width+spriteContentSize.width*0.3, (spriteContentSize.height-labelSize.height)/2));
    
    updateContentSizeWithTextureSize(calculateContentSize());
}

void TSRadioButton::loadTextures(const std::string& on, const std::string& off, TextureResType texType)
{
    loadTextureOnStatus(on,texType);
    loadTextureOffStatus(off,texType);
}

void TSRadioButton::loadTextureOnStatus(const std::string& on,TextureResType texType)
{
    if (on.empty())
    {
        return;
    }
    _backGroundOnStatusFileName = on;
    _backGroundOnStatusTexType = texType;
    switch (_backGroundOnStatusTexType)
    {
        case UI_TEX_TYPE_LOCAL:
            _backGroundOnStatusRenderer->setTexture(on);
            break;
        case UI_TEX_TYPE_PLIST:
            _backGroundOnStatusRenderer->setSpriteFrame(on);
            break;
        default:
            break;
    }
    updateFlippedX();
    updateFlippedY();
    updateRGBAToRenderer(_backGroundOnStatusRenderer);

    _backGroundOnStatusRendererAdaptDirty = true;
}

void TSRadioButton::loadTextureOffStatus(const std::string& off,TextureResType texType)
{
    if (off.empty())
    {
        return;
    }
    _backGroundOffStatusFileName = off;
    _backGroundOffStatusTexType = texType;
    switch (_backGroundOffStatusTexType)
    {
        case UI_TEX_TYPE_LOCAL:
            _backGroundOffStatusRenderer->setTexture(off);
            break;
        case UI_TEX_TYPE_PLIST:
            _backGroundOffStatusRenderer->setSpriteFrame(off);
            break;
        default:
            break;
    }
    updateFlippedX();
    updateFlippedY();
    updateRGBAToRenderer(_backGroundOffStatusRenderer);

    updateContentSizeWithTextureSize(calculateContentSize());
    _backGroundOffStatusRendererAdaptDirty = true;
}

void TSRadioButton::onTouchEnded(Touch *touch, Event *unusedEvent)
{
    _touchEndPos = touch->getLocation();
    if (_focus)
    {
        releaseUpEvent();
        if (_isSelected){
            setSelectedState(false);
            unSelectedEvent();
        }
        else
        {
            setSelectedState(true);
            selectedEvent();
        }
    }
    setFocused(false);
    Widget* widgetParent = getWidgetParent();
    if (widgetParent)
    {
        widgetParent->checkChildInfo(2,this,_touchEndPos);
    }
}

void TSRadioButton::onPressStateChangedToNormal()
{
    if (_isSelected)
    {
        _backGroundOffStatusRenderer->setVisible(false);
        _backGroundOnStatusRenderer->setVisible(true);
    } else {
        _backGroundOffStatusRenderer->setVisible(true);
        _backGroundOnStatusRenderer->setVisible(false);
    }
}

void TSRadioButton::onPressStateChangedToPressed()
{
    if (!_isSelected)
    {
        _backGroundOffStatusRenderer->setVisible(false);
        _backGroundOnStatusRenderer->setVisible(true);
    } else {
        _backGroundOffStatusRenderer->setVisible(true);
        _backGroundOnStatusRenderer->setVisible(false);
    }
}

void TSRadioButton::onPressStateChangedToDisabled()
{
    if (_isSelected)
    {
        _backGroundOffStatusRenderer->setVisible(false);
        _backGroundOnStatusRenderer->setVisible(true);
    } else {
        _backGroundOffStatusRenderer->setVisible(true);
        _backGroundOnStatusRenderer->setVisible(false);
    }
}

void TSRadioButton::setSelectedState(bool selected)
{
    if (selected == _isSelected)
    {
        return;
    }
    _isSelected = selected;
    _backGroundOnStatusRenderer->setVisible(_isSelected);
    _backGroundOffStatusRenderer->setVisible(!_isSelected);
}

bool TSRadioButton::getSelectedState()
{
    return _isSelected;
}

void TSRadioButton::selectedEvent()
{
    if (_radioButtonEventListener && _radioButtonEventSelector)
    {
        (_radioButtonEventListener->*_radioButtonEventSelector)(this,TSRADIOBUTTON_STATE_EVENT_SELECTED);
    }
}

void TSRadioButton::unSelectedEvent()
{
    if (_radioButtonEventListener && _radioButtonEventSelector)
    {
        (_radioButtonEventListener->*_radioButtonEventSelector)(this,TSRADIOBUTTON_STATE_EVENT_UNSELECTED);
    }
}

void TSRadioButton::addEventListenerRadioButton(Ref *target, SEL_TSSelectedStateEvent selector)
{
    _radioButtonEventListener = target;
    _radioButtonEventSelector = selector;
}

void TSRadioButton::updateFlippedX()
{
    _backGroundOnStatusRenderer->setFlippedX(_flippedX);
    _backGroundOffStatusRenderer->setFlippedX(_flippedX);
}

void TSRadioButton::updateFlippedY()
{
    _backGroundOnStatusRenderer->setFlippedY(_flippedY);
    _backGroundOffStatusRenderer->setFlippedY(_flippedY);
}

void TSRadioButton::onSizeChanged()
{
    Widget::onSizeChanged();
    _backGroundOnStatusRendererAdaptDirty = true;
    _backGroundOffStatusRendererAdaptDirty = true;
}

void TSRadioButton::adaptRenderers()
{
    if (_backGroundOnStatusRendererAdaptDirty)
    {
        backGroundOnStatusTextureScaleChangedWithSize();
        _backGroundOnStatusRendererAdaptDirty = false;
    }
    if (_backGroundOffStatusRendererAdaptDirty)
    {
        backGroundOffStatusTextureScaleChangedWithSize();
        _backGroundOffStatusRendererAdaptDirty = false;
    }
}

const Size& TSRadioButton::getVirtualRendererSize() const
{
    return _backGroundOffStatusRenderer->getContentSize();
}

Node* TSRadioButton::getVirtualRenderer()
{
    return _backGroundOffStatusRenderer;
}

void TSRadioButton::backGroundOnStatusTextureScaleChangedWithSize()
{
    if (_ignoreSize)
    {
        _backGroundOnStatusRenderer->setScale(1.0f);
    }
    else
    {
        Size textureSize = _backGroundOnStatusRenderer->getContentSize();
        if (textureSize.width <= 0.0f || textureSize.height <= 0.0f)
        {
            _backGroundOnStatusRenderer->setScale(1.0f);
            return;
        }
        float scaleX = _size.width / textureSize.width;
        float scaleY = _size.height / textureSize.height;
        _backGroundOnStatusRenderer->setScaleX(scaleX);
        _backGroundOnStatusRenderer->setScaleY(scaleY);
    }
    _backGroundOnStatusRenderer->setPosition(Point(_contentSize.width / 2, _contentSize.height / 2));
}

void TSRadioButton::backGroundOffStatusTextureScaleChangedWithSize()
{
    if (_ignoreSize)
    {
        _backGroundOffStatusRenderer->setScale(1.0f);
    }
    else
    {
        Size textureSize = _backGroundOffStatusRenderer->getContentSize();
        if (textureSize.width <= 0.0f || textureSize.height <= 0.0f)
        {
            _backGroundOffStatusRenderer->setScale(1.0f);
            return;
        }
        float scaleX = _size.width / textureSize.width;
        float scaleY = _size.height / textureSize.height;
        _backGroundOffStatusRenderer->setScaleX(scaleX);
        _backGroundOffStatusRenderer->setScaleY(scaleY);
    }
    _backGroundOffStatusRenderer->setPosition(Point(_contentSize.width / 2, _contentSize.height / 2));
}

std::string TSRadioButton::getDescription() const
{
    return "TSRadioButton";
}

void TSRadioButton::updateTextureColor()
{
    updateColorToRenderer(_backGroundOnStatusRenderer);
    updateColorToRenderer(_backGroundOffStatusRenderer);
}

void TSRadioButton::updateTextureOpacity()
{
    updateOpacityToRenderer(_backGroundOnStatusRenderer);
    updateOpacityToRenderer(_backGroundOffStatusRenderer);
}

void TSRadioButton::updateTextureRGBA()
{
    updateRGBAToRenderer(_backGroundOnStatusRenderer);
    updateRGBAToRenderer(_backGroundOffStatusRenderer);
}

Widget* TSRadioButton::createCloneInstance()
{
    return TSRadioButton::create();
}

void TSRadioButton::copySpecialProperties(Widget *widget)
{
    TSRadioButton* radioButton = dynamic_cast<TSRadioButton*>(widget);
    if (radioButton)
    {
        loadTextureOnStatus(radioButton->_backGroundOnStatusFileName, radioButton->_backGroundOnStatusTexType);
        loadTextureOffStatus(radioButton->_backGroundOffStatusFileName, radioButton->_backGroundOffStatusTexType);
        setSelectedState(radioButton->_isSelected);
    }
}

