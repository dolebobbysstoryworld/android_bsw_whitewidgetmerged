//
//  LoginLayer.cpp
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 16..
//
//

#include "LoginLayer.h"
#include "AccountLayerProtocol.h"
#include "Define.h"

USING_NS_CC;

#pragma mark - definition
using namespace ui;
using namespace extension;

#define TAG_EDTBX_ID                10001
#define TAG_EDTBX_PASSWORD          10002
#define TAG_BTN_LOGIN               10003
#define TAG_MENU_FGPASSWORD         10004
#define TAG_MENU_SIGNIN             10005
#define TAG_MENU_LOGINFB            10006
#define TAG_BTN_CLOSE               10007
#define TAG_BTN_SKIP                10008

#pragma mark - class init
Loginlayer::Loginlayer() :
_idEditBox(nullptr),
_pwEditBox(nullptr),
_loginButton(nullptr),
_isForgotPasswordStatus(false),
_skipLabel(nullptr),
_skipButton(nullptr) {
    
}

Loginlayer::~Loginlayer() {
    log("Loginlayer destructor call");
}

Loginlayer* Loginlayer::create()
{
    Loginlayer *layer = new Loginlayer();
    if (layer && layer->init())
    {
        layer->autorelease();
        return layer;
    }
    CC_SAFE_DELETE(layer);
    return nullptr;
}

void Loginlayer::onEnter() {
    Layer::onEnter();
}

void Loginlayer::onExit() {
    Layer::onExit();
}

bool Loginlayer::init()
{
    if ( !LayerColor::initWithColor(Color4B(0, 0, 0, 0), AccountLayerProtocol::getLayerSize().width, AccountLayerProtocol::getLayerSize().height) )
    {
        return false;
    }
	////////////////////////
    
    this->imageLayoutSetting();
    this->loginControlSetting();
    this->loginButtonSetting();
//    this->accountMenageControlSetting();
    this->accountManageControlAtMenuSetting();
    if (UserDefault::getInstance()->getIntegerForKey(UDKEY_INTEGER_USERNO, -1) <= 0) {
        this->skipControlSetting();
    }
    return true;
}

#pragma mark - UI layout Setting
void Loginlayer::characterImageLayoutSetting() {
    float width = Director::getInstance()->getOpenGLView()->getFrameSize().width;
    if ((width == 1024 || width == 2048) && IsIOS() == true) {//pad 계열만 표시
        Sprite *lionSprite = Sprite::create("full_popup_character_01.png");
        lionSprite->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        lionSprite->setPosition(ACCOUNTLAYER_POSITION_FULL_POPUP_CHARACTER_01);//Point(-143, 0)
        this->addChild(lionSprite);
        
        Sprite *racoonSprite = Sprite::create("full_popup_character_02.png");
        racoonSprite->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        racoonSprite->setPosition(ACCOUNTLAYER_POSITION_FULL_POPUP_CHARACTER_02);//Point(680, 472+210)
        this->addChild(racoonSprite);
    }
}

void Loginlayer::imageLayoutSetting() {
    Sprite *backgroundMap = Sprite::create("bg_map_full.png");
    backgroundMap->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    backgroundMap->setPosition(Point::ZERO);
    this->addChild(backgroundMap, 0);
    
    Sprite *backgroundFactor = Sprite::create("bg_map_factor.png");
    backgroundFactor->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    backgroundFactor->setPosition(Point::ZERO);
    this->addChild(backgroundFactor, 0);

    Sprite *backgroundSea = Sprite::create("bg_sea.png");
    backgroundSea->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    backgroundSea->setPosition(Point::ZERO);
    this->addChild(backgroundSea, 0);

    Sprite *logoMap = Sprite::create("login_logo.png");
    logoMap->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    logoMap->setPosition(ACCOUNTLAYER_POSITION_LOGIN_LOGO);//Point((828-346)/2, 1224-150-134)
    this->addChild(logoMap);
    
    this->characterImageLayoutSetting();
    
    Button *closeButton = Button::create("btn_cancel_normal.png", "btn_cancel_press.png");
    closeButton->setContentSize(ACCOUNTLAYER_SIZE_BTN_CANCEL_NORMAL);//Size(80, 80)
    closeButton->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    closeButton->setPosition(ACCOUNTLAYER_POSITION_BTN_CANCEL_NORMAL);//Point(692,1085)
    closeButton->setTag(TAG_BTN_CLOSE);
    closeButton->addTouchEventListener(this, toucheventselector(Loginlayer::touchEventForUIButton));
    this->addChild(closeButton);
}

void Loginlayer::idInputControlSetting(const cocos2d::Point &position) {
    std::string userID = "";
    if (UserDefault::getInstance()->getIntegerForKey(UDKEY_INTEGER_USERNO, -1) > 0) {
        userID = UserDefault::getInstance()->getStringForKey(UDKEY_STRING_LOGINID);
    }
    _idEditBox = TSIconEditBox::create("input_mail_icon.png", "ExampleID@dole.com", userID);
    _idEditBox->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    _idEditBox->setPosition(position);
    _idEditBox->setInputMode(EditBox::InputMode::EMAIL_ADDRESS);
    _idEditBox->setDelegate(this);
    this->addChild(_idEditBox);
}

void Loginlayer::loginControlSetting() {
    this->idInputControlSetting(LOGINLAYER_POSITION_CONTROLBUTTON_ID);//Point(160, 1224-(70+80+150+134))
    std::string password = "";
    if (UserDefault::getInstance()->getIntegerForKey(UDKEY_INTEGER_USERNO, -1) > 0) {
        password = UserDefault::getInstance()->getStringForKey(UDKEY_STRING_LOGINPW);
    }
    _pwEditBox = TSIconEditBox::create("input_password_icon.png", "Password", password);
    _pwEditBox->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    _pwEditBox->setPosition(LOGINLAYER_POSITION_CONTROLBUTTON_PW);//Point(160, 1224-(70+22+70+80+150+134))
    _pwEditBox->setInputFlag(EditBox::InputFlag::PASSWORD);
    _pwEditBox->setDelegate(this);
    this->addChild(_pwEditBox);
}

void Loginlayer::accountManageControlSetting() {
    //tuilise_todo : 투명 배경 추가하지 않으면 위치가 깨짐, 해결 방안 찾아야 함
    Button *findPasswordButton = Button::create("button_background_alpha.png");
    findPasswordButton->setTitleText("Forgot your password");
    findPasswordButton->setSize(LOGINLAYER_SIZE_BUTTON_ACCOUNTCONTROL);//Size(508,32)
//    findPasswordButton->setContentSize(Size(508,32));
    findPasswordButton->setTitleFontSize(LOGINLAYER_FONTSIZE_ACCOUNTCONTROL);//28
    findPasswordButton->setTitleColor(Color3B(12, 60, 96));
    findPasswordButton->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    findPasswordButton->setPosition(LOGINLAYER_POSITION_BUTTON_FORGOTPASSWORD);//Point(160, 122+32+44+32+44)
    this->addChild(findPasswordButton);

    Button *signinButton = Button::create("button_background_alpha.png");
    signinButton->setTitleText("Sign in");
    signinButton->setSize(LOGINLAYER_SIZE_BUTTON_ACCOUNTCONTROL);//Size(508,32)
//    signinButton->setContentSize(Size(508,32));
    signinButton->setTitleFontSize(LOGINLAYER_FONTSIZE_ACCOUNTCONTROL);//28
    signinButton->setTitleColor(Color3B(12, 60, 96));
    signinButton->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    signinButton->setPosition(LOGINLAYER_POSITION_BUTTON_SIGNUP);//Point(160, 122+32+44)
    signinButton->setTag(TAG_MENU_SIGNIN);
    signinButton->addTouchEventListener(this, toucheventselector(Loginlayer::touchEventForUIButton));
    this->addChild(signinButton);

    Button *facebookLoginButton = Button::create("button_background_alpha.png");
    facebookLoginButton->setTitleText("Login with facebook");
    facebookLoginButton->setSize(LOGINLAYER_SIZE_BUTTON_ACCOUNTCONTROL);//Size(508,32)
//    facebookLoginButton->setContentSize(Size(508,32));
    facebookLoginButton->setTitleFontSize(LOGINLAYER_FONTSIZE_ACCOUNTCONTROL);//28
    facebookLoginButton->setTitleColor(Color3B(12, 60, 96));
    facebookLoginButton->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    facebookLoginButton->setPosition(LOGINLAYER_POSITION_BUTTON_FACEBOOKLOGIN);//Point(160, 122)
    this->addChild(facebookLoginButton);
}

void Loginlayer::accountManageControlAtMenuSetting() {
    Color3B textColor = Color3B(0x0c, 0x3c, 0x60);
    //tuilise_todo : Label에 underline 지원이 안됨, 해결 방안 마련 또는 gui guide 수정 요청
    Label *findPasswordLabel = Label::createWithSystemFont("Forgot your password", FONT_NAME, LOGINLAYER_FONTSIZE_ACCOUNTCONTROL,
                                                           LOGINLAYER_SIZE_BUTTON_ACCOUNTCONTROL, TextHAlignment::CENTER, TextVAlignment::CENTER, true);
    findPasswordLabel->setColor(textColor);
    MenuItemLabel *findPasswordMenu =
    MenuItemLabel::create(findPasswordLabel, CC_CALLBACK_1(Loginlayer::menuItemCallback, this));
    findPasswordMenu->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    findPasswordMenu->setPosition(LOGINLAYER_POSITION_BUTTON_FORGOTPASSWORD);
    findPasswordMenu->setTag(TAG_MENU_FGPASSWORD);
    
    Label *signinLabel = Label::createWithSystemFont("Sign in", FONT_NAME, LOGINLAYER_FONTSIZE_ACCOUNTCONTROL,
                                                           LOGINLAYER_SIZE_BUTTON_ACCOUNTCONTROL, TextHAlignment::CENTER, TextVAlignment::CENTER, true);
    signinLabel->setColor(textColor);
    MenuItemLabel *signinMenu =
    MenuItemLabel::create(signinLabel, CC_CALLBACK_1(Loginlayer::menuItemCallback, this));
    signinMenu->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    signinMenu->setPosition(LOGINLAYER_POSITION_BUTTON_SIGNUP);
    signinMenu->setTag(TAG_MENU_SIGNIN);

    Label *facebookLoginLabel = Label::createWithSystemFont("Login with facebook", FONT_NAME, LOGINLAYER_FONTSIZE_ACCOUNTCONTROL,
                                                           LOGINLAYER_SIZE_BUTTON_ACCOUNTCONTROL, TextHAlignment::CENTER, TextVAlignment::CENTER, true);
    facebookLoginLabel->setColor(textColor);
    MenuItemLabel *facebookLoginMenu =
    MenuItemLabel::create(facebookLoginLabel, CC_CALLBACK_1(Loginlayer::menuItemCallback, this));
    facebookLoginMenu->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    facebookLoginMenu->setPosition(LOGINLAYER_POSITION_BUTTON_FACEBOOKLOGIN);
    facebookLoginMenu->setTag(TAG_MENU_LOGINFB);
    
    _accountMenu = Menu::create(findPasswordMenu, signinMenu, facebookLoginMenu, NULL);
    _accountMenu->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    _accountMenu->setPosition(cocos2d::Point::ZERO);
    this->addChild(_accountMenu, 2);
}

void Loginlayer::loginButtonSetting() {
    Scale9Sprite *normal = Scale9Sprite::create("button_normal_01.png",
                                                Rect(0,0,ACCOUNTLAYER_BUTTON_NORMAL_01_WIDTH,ACCOUNTLAYER_BUTTON_NORMAL_01_HEIGHT),
                                                Rect(ACCOUNTLAYER_BUTTON_NORMAL_01_INLEFT, ACCOUNTLAYER_BUTTON_NORMAL_01_INTOP,
                                                     ACCOUNTLAYER_BUTTON_NORMAL_01_INWIDTH, ACCOUNTLAYER_BUTTON_NORMAL_01_INHEIGHT));
    Scale9Sprite *press = Scale9Sprite::create("button_press_01.png",
                                               Rect(0,0,ACCOUNTLAYER_BUTTON_NORMAL_01_WIDTH,ACCOUNTLAYER_BUTTON_NORMAL_01_HEIGHT),
                                               Rect(ACCOUNTLAYER_BUTTON_NORMAL_01_INLEFT, ACCOUNTLAYER_BUTTON_NORMAL_01_INTOP,
                                                    ACCOUNTLAYER_BUTTON_NORMAL_01_INWIDTH, ACCOUNTLAYER_BUTTON_NORMAL_01_INHEIGHT));
    Scale9Sprite *dim = Scale9Sprite::create("button_dim_01.png",
                                             Rect(0,0,ACCOUNTLAYER_BUTTON_NORMAL_01_WIDTH,ACCOUNTLAYER_BUTTON_NORMAL_01_HEIGHT),
                                             Rect(ACCOUNTLAYER_BUTTON_NORMAL_01_INLEFT, ACCOUNTLAYER_BUTTON_NORMAL_01_INTOP,
                                                  ACCOUNTLAYER_BUTTON_NORMAL_01_INWIDTH, ACCOUNTLAYER_BUTTON_NORMAL_01_INHEIGHT));
    _loginButton = ControlButton::create();
    _loginButton->setBackgroundSpriteForState(normal, Control::State::NORMAL);
    _loginButton->setBackgroundSpriteForState(press, Control::State::HIGH_LIGHTED);
    _loginButton->setBackgroundSpriteForState(dim, Control::State::DISABLED);
    _loginButton->setTitleForState("Login", Control::State::NORMAL);
    _loginButton->setTitleTTFForState("Helvetica", Control::State::NORMAL);
    _loginButton->setTitleTTFSizeForState(LOGINLAYER_FONTSIZE_BUTTON_LOGIN, Control::State::NORMAL);
    _loginButton->setTitleColorForState(Color3B(0x79, 0x1b, 0x00), Control::State::NORMAL);
    _loginButton->setPreferredSize(LOGINLAYER_SIZE_BUTTON_LOGIN);//Size(508, 72)
    _loginButton->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    _loginButton->setPosition(LOGINLAYER_POSITION_BUTTON_LOGIN);//Point(160, 1224-(72+36+70+22+70+80+150+134))
    if ( ( this->checkingIDStringInvalidate( _idEditBox->getText() ) == true ) &&
        ( this->checkingPasswordStringInvalidate( _pwEditBox->getText() ) == true) ) {
        _loginButton->setEnabled(true);
    } else {
        _loginButton->setEnabled(false);
    }
    _loginButton->addTargetWithActionForControlEvents(this, cccontrol_selector(Loginlayer::eventForControlButton), Control::EventType::TOUCH_UP_INSIDE);
    this->addChild(_loginButton);
}

void Loginlayer::skipControlSetting() {
    _skipButton = Button::create("btn_skip_normal.png", "btn_skip_press.png");
    _skipButton->setContentSize(LOGINLAYER_SIZE_SKIPBUTTON);//Size(60, 60)
    _skipButton->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    _skipButton->setPosition(LOGINLAYER_POSITION_SKIPBUTTON);//Point(708-(06+06), 52)
    _skipButton->setTag(TAG_BTN_SKIP);
    _skipButton->addTouchEventListener(this, toucheventselector(Loginlayer::touchEventForUIButton));
    this->addChild(_skipButton);

//    _skipLabel = Label::createWithSystemFont("Skip", "Helvetica", 32);
//    
    _skipLabel = Label::createWithSystemFont("Skip", FONT_NAME, LOGINLAYER_FONTSIZE_SKIPLABEL,
                                             LOGINLAYER_SIZE_SKIPLABEL, TextHAlignment::CENTER, TextVAlignment::CENTER);//Size(70,60)
    _skipLabel->setColor(Color3B(0xff, 0xff, 0xff));
    _skipLabel->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    Size btnSize = _skipButton->getContentSize();
    Size titleSize = _skipLabel->getContentSize();
    _skipLabel->setPosition(Point(LOGINLAYER_POSITION_SKIPLABEL_X,
                                  LOGINLAYER_POSITION_SKIPLABEL_Y_MARGIN+(btnSize.height-titleSize.height)/2));//Point(708-(06+06+10+70), 52+(btnSize.height-titleSize.height)/2)
//    _skipLabel->setPosition(Point(708, 52));
//    _skipLabel->setVerticalAlignment(TextVAlignment::CENTER);
    this->addChild(_skipLabel);
}

void Loginlayer::forgotPasswordStatusUISetting() {
    if (_idEditBox) {
        _idEditBox->setVisible(false);
        this->removeChild(_idEditBox);
        _idEditBox = nullptr;
    }
    
    if (_skipLabel) {
        _skipLabel->setVisible(false);
    }
    if (_skipButton) {
        _skipButton->setVisible(false);
    }
    
    _accountMenu->setVisible(false);
//    this->idInputControlSetting(Point(160, 1224-(70+22+70+80+150+134)));
    
    const char *noticeText = "Please enter the email address used as the ID during sign up.\nA temporary password will be sent to the address.";
    Label *_forgotNoticeLabel =
    Label::createWithSystemFont(noticeText, FONT_NAME, LOGINLAYER_FONTSIZE_FORGOTNOTICELAYER,
                                LOGINLAYER_SIZE_FORGOTNOTICELABEL, TextHAlignment::CENTER, TextVAlignment::CENTER);//Size(508, 32+32+32+32)
    _forgotNoticeLabel->setColor(Color3B(0x4f, 0x2c, 0x00));
    _forgotNoticeLabel->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    _forgotNoticeLabel->setPosition(LOGINLAYER_POSITION_FORGOTNOTICELABEL);//Point(160, 1224-(134+150+28+32+32+32+32))
    this->addChild(_forgotNoticeLabel);
    
}

#pragma mark - Error Status Display
void Loginlayer::showIDStringError() {
    const char *idErrorComment = "Please enter the full email address,\nincluding after the @ sign";
    _idEditBox->showErrorComment(idErrorComment);
//    const char *idErrorComment = "This email address is already in use￼￼";
//    _idEditBox->showErrorComment(idErrorComment);
}

void Loginlayer::showPasswordStringError() {
    const char *passwordErrorComment = "￼Please enter a password with at least 6 characters";
    _pwEditBox->showErrorComment(passwordErrorComment);
}

#pragma mark - UI Controls Event Callback
void Loginlayer::touchEventForUIButton(Ref *pSender, TouchEventType type)//button class callback
{
    int tag = ((Node*)pSender)->getTag();
    switch (type) {
        case TOUCH_EVENT_ENDED: {
            log("Button Touch Up");
            switch (tag) {
                case TAG_BTN_CLOSE: {
                    AccountLayerProtocol *parent = dynamic_cast<AccountLayerProtocol*>(this->getParent());
                    parent->closeChildLayer();
                } break;
                case TAG_BTN_SKIP: {
                    _isForgotPasswordStatus = true;
                    AccountLayerProtocol *parent = dynamic_cast<AccountLayerProtocol*>(this->getParent());
                    parent->closeChildLayer();
                } break;
            }
        } break;
            
        case TOUCH_EVENT_BEGAN: { log("Button Touch Down"); } break;
        case TOUCH_EVENT_MOVED: { log("Button Touch Move"); } break;
        case TOUCH_EVENT_CANCELED: { log("Button Touch Cancelled"); } break;
        default: { log("Button unknown TouchEventType = %d", type); } break;
    }
}

void Loginlayer::menuItemCallback(Ref* pSender) {
    log("MenuItem click");
    int tag = ((Node*)pSender)->getTag();
    switch (tag) {
        case TAG_MENU_FGPASSWORD: {
            this->forgotPasswordStatusUISetting();
        } break;
        case TAG_MENU_SIGNIN: {
            AccountLayerProtocol *parent = dynamic_cast<AccountLayerProtocol*>(this->getParent());
            parent->nextPageLayer(2);
            this->removeFromParentAndCleanup(true);
        } break;
        case TAG_MENU_LOGINFB: {
            
        } break;
        default:
            break;
    }
}

void Loginlayer::eventForControlButton(Ref *pSender, Control::EventType type) {//ControlButton class callback
    switch (type) {
        case Control::EventType::TOUCH_UP_INSIDE: {
            _loginButton->setEnabled(false);//login button double tap 방지
            const char* idString = _idEditBox->getText();
            const char* pwString = _pwEditBox->getText();
            if (_isForgotPasswordStatus == false) {
                /////todo. normal login network process call
                log("normal login : id=%s | pw=%s", idString, pwString);
                this->getAuthKeyMobileProtocol(idString, pwString);
            } else {
                /////todo. Forgot Password network process call
                log("skip login : id=%s | pw=%s", idString, pwString);
            }
        } break;
        default:
            break;
    }
}

#pragma mark - EditBox Delegate
void Loginlayer::tsEditBoxReturn(extension::EditBox* editBox) {//EditBox Delegate
    if ( _isForgotPasswordStatus == false) {
        if ( ( this->checkingIDStringInvalidate( _idEditBox->getText() ) == true ) &&
             ( this->checkingPasswordStringInvalidate( _pwEditBox->getText() ) == true) ) {
            _loginButton->setEnabled(true);
            return;
        }
    } else {
        if ( this->checkingIDStringInvalidate( _idEditBox->getText() ) == true ) {
            _loginButton->setEnabled(true);
            return;
        }
    }

    _loginButton->setEnabled(false);
}

/*
////http://www.cplusplus.com/forum/general/65108/ Email Validation // tuilise comment : Old Spec????
////http://en.wikipedia.org/wiki/Email_address Email address format
////http://stackoverflow.com/questions/14913341/how-do-i-check-a-user-input-string-with-email-format other checking role
bool isCharacter(const char Character)
{
	return ( (Character >= 'a' && Character <= 'z') || (Character >= 'A' && Character <= 'Z'));
	//Checks if a Character is a Valid A-Z, a-z Character, based on the ascii value
}
bool isNumber(const char Character)
{
	return ( Character >= '0' && Character <= '9');
	//Checks if a Character is a Valid 0-9 Number, based on the ascii value
}
bool isValidEmailAddress(const char * EmailAddress)
{
	if(!EmailAddress) // If cannot read the Email Address...
		return 0;
	if(!isCharacter(EmailAddress[0])) // If the First character is not A-Z, a-z
		return 0;
	int AtOffset = -1;
	int DotOffset = -1;
	unsigned int Length = strlen(EmailAddress); // Length = StringLength (strlen) of EmailAddress
	for(unsigned int i = 0; i < Length; i++)
	{
		if(EmailAddress[i] == '@') // If one of the characters is @, store it's position in AtOffset
			AtOffset = (int)i;
		else if(EmailAddress[i] == '.') // Same, but with the dot
			DotOffset = (int)i;
	}
	if(AtOffset == -1 || DotOffset == -1) // If cannot find a Dot or a @
		return 0;
	if(AtOffset > DotOffset) // If the @ is after the Dot
		return 0;
	return !(DotOffset >= ((int)Length-1)); //Chech there is some other letters after the Dot
}
/////////////////////////////////////////////////////////////////
*/
#pragma mark - EditBox Utiltiy
bool Loginlayer::checkingIDStringInvalidate(const std::string &idString) {
    if ( idString.empty() ) {
        return false;
    }
    
    size_t result = idString.find("@", 1);
    if (result == -1) {
        this->showIDStringError();
        return false;
    }
    result = idString.find(".", result+2);
    if (result == -1) {
        this->showIDStringError();
        return false;
    }
//    return isValidEmailAddress(idString.c_str());
    return true;
}

bool Loginlayer::checkingPasswordStringInvalidate(const std::string &pwString) {
    if ( pwString.empty() ) {
        return false;
    }
    
    if ( pwString.length() < 6) {
        this->showPasswordStringError();
        return false;
    }
    return true;
}

void Loginlayer::getAuthKeyMobileProtocol(std::string userID, std::string password) {
//    DoleAPI* api = DoleAPI::getInstance();
//    api->getAuthKeyMobile(userID, password,this,//cocos2d::Ref *target,
//                         httpresponse_selector(Loginlayer::responseCallback)//cocos2d::network::SEL_HttpResponse pSelector
//                         );
}

void Loginlayer::responseCallback(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response )
{
    if (!response) return;
    
    printf("Tag: %s\n", response->getHttpRequest()->getTag());
    std::vector<char>*buffer = response->getResponseData();
    std::string str(buffer->begin(), buffer->end());
    
    printf("response Content: %s\n", str.c_str());
    
    if (strlen(str.c_str()) > 0) {
        rapidjson::Document ret;
        ret.Parse<0>(str.c_str());
        bool retValue = ret["Return"].GetBool();
        int retCode = ret["ReturnCode"].GetInt();
        printf("Parsed Data: retValue: %d, retCode: %d", retValue, retCode);
        AccountLayerProtocol *parent = dynamic_cast<AccountLayerProtocol*>(this->getParent());
        if (retValue) {
            std::string authKey = ret["AuthKey"].GetString();
            log("getAuthKeyMobile success\nAuthKey : %s\n", authKey.c_str());
            UserDefault::getInstance()->setStringForKey(UDKEY_STRING_AUTHKEY, authKey);
            int userNo = ret["UserNo"].GetInt();
            int oldUserNo = UserDefault::getInstance()->getIntegerForKey(UDKEY_INTEGER_USERNO, -1);
            if ( (oldUserNo == -1) || (userNo != oldUserNo) ) {
                log("Update User Number old : %d / current: %d\n", oldUserNo, userNo);
                UserDefault::getInstance()->setIntegerForKey(UDKEY_INTEGER_USERNO, userNo);
            }
            parent->loginDidEnded(true);
            parent->closeChildLayer();
        } else {
            //Error Display
            parent->loginDidEnded(false);
            __String *failMessgge = __String::createWithFormat("ProvinceInfo Fail : code : %d", retCode);
            PopupLayer *popup = PopupLayer::create(POPUP_TYPE_WARNING, failMessgge->getCString(), "Cancel", "OK");
            popup->setPosition(Point::ZERO);
            popup->setDelegate(this);
            this->addChild(popup);
        }
    }
}

void Loginlayer::popupButtonClicked(PopupLayer *popup, cocos2d::Ref *obj) {
    
}