//
//  SignupLayer.cpp
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 16..
//
//

#include "SignupLayer.h"

USING_NS_CC;
using namespace ui;

#define TAG_IEDT_ID         20001
#define TAG_IEDT_PW         20002
#define TAG_IEDT_CPW        20003
#define TAG_IEDT_BIRTH      20004
#define TAG_IEDT_CITY       20005
#define TAG_RADIO_MAIL      20006
#define TAG_RADIO_FEMAIL    20007
#define TAG_CHECK_AGREE     20008
#define TAG_CBTN_SIGNUP     20009
#define TAG_BTN_CLOSE       20010

using namespace extension;

SignupLayer::SignupLayer() {
    
}

SignupLayer::~SignupLayer() {}

SignupLayer* SignupLayer::create()
{
    SignupLayer *layer = new SignupLayer();
    if (layer && layer->init())
    {
        layer->autorelease();
        return layer;
    }
    CC_SAFE_DELETE(layer);
    return nullptr;
}

void SignupLayer::onEnter() {
    Layer::onEnter();
    this->getProvinceInfoProtocol();
}

void SignupLayer::onExit() {
    Layer::onExit();
}

bool SignupLayer::init() {
    if ( !LayerColor::initWithColor(Color4B(0, 0, 0, 0), AccountLayerProtocol::getLayerSize().width, AccountLayerProtocol::getLayerSize().height) ) return false;

    this->imageLayoutSetting();
    this->accountInfomationInputControlSetting();
    this->optionButtonSetting();
    this->signupButtonSetting();
    
    return true;
}

void SignupLayer::characterImageLayoutSetting() {
    float width = Director::getInstance()->getOpenGLView()->getFrameSize().width;
    if ((width == 1024 || width == 2048) && IsIOS() == true) {//pad 계열만 표시
        Sprite *lionSprite = Sprite::create("full_popup_character_01.png");
        lionSprite->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        lionSprite->setPosition(ACCOUNTLAYER_POSITION_FULL_POPUP_CHARACTER_01);//Point(-143, 0)
        this->addChild(lionSprite);
        
        Sprite *racoonSprite = Sprite::create("full_popup_character_02.png");
        racoonSprite->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        racoonSprite->setPosition(ACCOUNTLAYER_POSITION_FULL_POPUP_CHARACTER_02);//Point(680, 472+210)
        this->addChild(racoonSprite);
    }
}

void SignupLayer::imageLayoutSetting() {
    Sprite *backgroundMap = Sprite::create("bg_map_full.png");
    backgroundMap->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    backgroundMap->setPosition(Point::ZERO);
    this->addChild(backgroundMap, 0);
    
    Sprite *backgroundFactor = Sprite::create("bg_map_factor.png");
    backgroundFactor->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    backgroundFactor->setPosition(Point::ZERO);
    this->addChild(backgroundFactor, 0);
    
    this->characterImageLayoutSetting();
    
    Button *closeButton = Button::create("btn_cancel_normal.png", "btn_cancel_press.png");
    closeButton->setContentSize(ACCOUNTLAYER_SIZE_BTN_CANCEL_NORMAL);//Size(80, 80)
    closeButton->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    closeButton->setPosition(ACCOUNTLAYER_POSITION_BTN_CANCEL_NORMAL);//Point(692,1085)
    closeButton->setTag(TAG_BTN_CLOSE);
    closeButton->addTouchEventListener(this, toucheventselector(SignupLayer::touchEventForButton));
    this->addChild(closeButton);
    
    //    extension::ControlButton *closeButton = extension::ControlButton::create();
    //    extension::Scale9Sprite *normal = extension::Scale9Sprite::create("btn_cancel_normal.png");
    //    extension::Scale9Sprite *press = extension::Scale9Sprite::create("btn_cancel_press.png");
    //    closeButton->setBackgroundSpriteForState(normal, extension::Control::State::NORMAL);
    //    closeButton->setBackgroundSpriteForState(press, extension::Control::State::SELECTED);
    //    closeButton->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    //    closeButton->setPosition(Point(692,1085));
    //    this->addChild(closeButton);
    
}

void SignupLayer::accountInfomationInputControlSetting() {
    _idEditBox = TSIconEditBox::create("input_mail_icon.png", "ExampleID@dole.com", "");
    _idEditBox->setTag(TAG_IEDT_ID);
    _idEditBox->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    _idEditBox->setPosition(SIGNUPLAYER_POSITION_IDEDITBOX);//Point(160, 106+72+134+56+46+60+32+70+28+70+28+70+28+70+28)
    _idEditBox->setInputMode(EditBox::InputMode::EMAIL_ADDRESS);
    _idEditBox->setDelegate(this);
    _idEditBox->setEnabled(false);
    this->addChild(_idEditBox);

    _pwEditBox = TSIconEditBox::create("input_password_icon.png", "Password", "");
    _pwEditBox->setTag(TAG_IEDT_PW);
    _pwEditBox->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    _pwEditBox->setPosition(SIGNUPLAYER_POSITION_PWEDITBOX);//Point(160, 106+72+134+56+46+60+32+70+28+70+28+70+28)
    _pwEditBox->setInputFlag(EditBox::InputFlag::PASSWORD);
    _pwEditBox->setInputComment("at least 6 characters");
    _pwEditBox->setDelegate(this);
    _pwEditBox->setEnabled(false);
    this->addChild(_pwEditBox);

    _pwcEditBox = TSIconEditBox::create("input_password_icon.png", "Comfirm Password", "");
    _pwcEditBox->setTag(TAG_IEDT_CPW);
    _pwcEditBox->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    _pwcEditBox->setPosition(SIGNUPLAYER_POSITION_CPWEDITBOX);//Point(160, 106+72+134+56+46+60+32+70+28+70+28)
    _pwcEditBox->setInputFlag(EditBox::InputFlag::PASSWORD);
    _pwcEditBox->setDelegate(this);
    _pwcEditBox->setEnabled(false);
    this->addChild(_pwcEditBox);

    _birthEditBox = TSIconEditBox::create("input_birthday_icon.png", "Birth year", "1980-01-01");
    _birthEditBox->setTag(TAG_IEDT_BIRTH);
    _birthEditBox->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    _birthEditBox->setPosition(SIGNUPLAYER_POSITION_BIRTHEDITBOX);//Point(160, 106+72+134+56+46+60+32+70+28)
    _birthEditBox->setDelegate(this);
    _birthEditBox->setEnabled(false);
    this->addChild(_birthEditBox);

    _cityEditBox = TSIconEditBox::create("input_city_icon.png", "City", "");
    _cityEditBox->setTag(TAG_IEDT_CITY);
    _cityEditBox->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    _cityEditBox->setPosition(SIGNUPLAYER_POSITION_CITYEIDTBOX);//Point(160, 106+72+134+56+46+60+32)
    _cityEditBox->setDelegate(this);
    _cityEditBox->setEnabled(false);
    this->addChild(_cityEditBox);
}

void SignupLayer::optionButtonSetting() {
    _maleRadio = TSRadioButton::create("Male", "radio_on.png", "radio_off.png");
    _maleRadio->setTag(TAG_RADIO_MAIL);
    _maleRadio->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);//Point(160, 106+72+134+56+46)
    _maleRadio->setPosition(SIGNUPLAYER_POSITION_MAILRADIO);
    _maleRadio->addEventListenerRadioButton(this, radiobuttonselectedeventselector(SignupLayer::selectedStateEventForRadioButton));
    this->addChild(_maleRadio);

    _femaleRadio = TSRadioButton::create("Female", "radio_on.png", "radio_off.png");
    _femaleRadio->setTag(TAG_RADIO_FEMAIL);
//    _femaleRadio->setContentSize(Size(58,60));
    _femaleRadio->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    _femaleRadio->setPosition(SIGNUPLAYER_POSITION_FEMAILRADIO);//Point(160+(508/2), 106+72+134+56+46)
    _femaleRadio->addEventListenerRadioButton(this, radiobuttonselectedeventselector(SignupLayer::selectedStateEventForRadioButton));
    this->addChild(_femaleRadio);
    
    _termAgreeCheckBox = CheckBox::create("checkbox_off.png", "checkbox_off.png", "checkbox_on.png", "checkbox_off.png", "checkbox_on.png");
    _termAgreeCheckBox->setTag(TAG_CHECK_AGREE);
//    _termAgreeCheckBox->setSelectedState(true);
    _termAgreeCheckBox->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    _termAgreeCheckBox->setPosition(SIGNUPLAYER_POSITION_TERMSCHECKBOX);//Point(160, 106+72+134)
    _termAgreeCheckBox->addEventListenerCheckBox(this, checkboxselectedeventselector(SignupLayer::selectedStateEventForCheckButton));
    this->addChild(_termAgreeCheckBox);
    
    Label *agreementText =
    Label::createWithSystemFont("Agree to the Terms\n& Conditions and Privacy Policy", "Helvetica", SIGNUPLAYER_FONTSIZE_AGREEMENTTEXTLABEL);
    agreementText->setColor(Color3B(0x4f, 0x2c, 0x00));
    agreementText->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    agreementText->setPosition(SIGNUPLAYER_POSITION_AGREEMENTTEXTLABEL);//Point(160+54+18, 106+72+132)
    this->addChild(agreementText);
}

void SignupLayer::signupButtonSetting() {
    Scale9Sprite *normal = Scale9Sprite::create("button_normal_01.png",
                                                Rect(0,0,ACCOUNTLAYER_BUTTON_NORMAL_01_WIDTH,ACCOUNTLAYER_BUTTON_NORMAL_01_HEIGHT),
                                                Rect(ACCOUNTLAYER_BUTTON_NORMAL_01_INLEFT, ACCOUNTLAYER_BUTTON_NORMAL_01_INTOP,
                                                     ACCOUNTLAYER_BUTTON_NORMAL_01_INWIDTH, ACCOUNTLAYER_BUTTON_NORMAL_01_INHEIGHT));
    Scale9Sprite *press = Scale9Sprite::create("button_press_01.png",
                                               Rect(0,0,ACCOUNTLAYER_BUTTON_NORMAL_01_WIDTH,ACCOUNTLAYER_BUTTON_NORMAL_01_HEIGHT),
                                               Rect(ACCOUNTLAYER_BUTTON_NORMAL_01_INLEFT, ACCOUNTLAYER_BUTTON_NORMAL_01_INTOP,
                                                    ACCOUNTLAYER_BUTTON_NORMAL_01_INWIDTH, ACCOUNTLAYER_BUTTON_NORMAL_01_INHEIGHT));
    Scale9Sprite *dim = Scale9Sprite::create("button_dim_01.png",
                                             Rect(0,0,ACCOUNTLAYER_BUTTON_NORMAL_01_WIDTH,ACCOUNTLAYER_BUTTON_NORMAL_01_HEIGHT),
                                             Rect(ACCOUNTLAYER_BUTTON_NORMAL_01_INLEFT, ACCOUNTLAYER_BUTTON_NORMAL_01_INTOP,
                                                  ACCOUNTLAYER_BUTTON_NORMAL_01_INWIDTH, ACCOUNTLAYER_BUTTON_NORMAL_01_INHEIGHT));
    _signUpButton = ControlButton::create();
    _signUpButton->setTag(TAG_CBTN_SIGNUP);
    _signUpButton->setBackgroundSpriteForState(normal, Control::State::NORMAL);
    _signUpButton->setBackgroundSpriteForState(press, Control::State::HIGH_LIGHTED);
    _signUpButton->setBackgroundSpriteForState(dim, Control::State::DISABLED);
    _signUpButton->setTitleForState("Sign up", Control::State::NORMAL);
    _signUpButton->setTitleTTFForState("Helvetica", Control::State::NORMAL);
    _signUpButton->setTitleTTFSizeForState(SIGNUPLAYER_FONTSIZE_BUTTON_LOGIN, Control::State::NORMAL);
    _signUpButton->setTitleColorForState(Color3B(0x79, 0x1b, 0x00), Control::State::NORMAL);
    _signUpButton->setPreferredSize(SIGNUPLAYER_SIZE_SIGNUPBUTTON);//Size(508, 72)
    _signUpButton->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    _signUpButton->setPosition(SIGNUPLAYER_POSITION_SIGNUPBUTTON);//Point(160, 106)
    _signUpButton->addTargetWithActionForControlEvents(this, cccontrol_selector(SignupLayer::eventForControlButton), Control::EventType::TOUCH_UP_INSIDE);
    _signUpButton->setEnabled(false);
    this->addChild(_signUpButton);
}

void SignupLayer::showIDStringError() {
    const char *idErrorComment = "Please enter the full email address,\nincluding after the @ sign";
    _idEditBox->showErrorComment(idErrorComment);
    //    const char *idErrorComment = "This email address is already in use￼￼";
    //    _idEditBox->showErrorComment(idErrorComment);
}

void SignupLayer::showPasswordStringError() {
    const char *passwordErrorComment = "￼Please enter a password with at least 6 characters";
    _pwEditBox->showErrorComment(passwordErrorComment);
}

void SignupLayer::showConfirmPasswordStringError() {
    const char *confirmPasswordErrorComment = "￼The password is incorrect";
    _pwcEditBox->showErrorComment(confirmPasswordErrorComment);
}


bool SignupLayer::checkSignupButtonEnable(int tag) {
    bool errorDisplay = false;
    if (tag == TAG_IEDT_ID) {
        errorDisplay = true;
    }
    bool bResultID = this->checkingIDStringInvalidate(_idEditBox->getText(), errorDisplay);
    if ( bResultID == false) {
        return false;
    }
    
    errorDisplay = false;
    if (tag == TAG_IEDT_PW) {
        errorDisplay = true;
    }
    bool bResultPW = this->checkingPasswordStringInvalidate(_pwEditBox->getText(), errorDisplay);
    if ( bResultPW == false) {
        return false;
    }
    
    errorDisplay = false;
    if (tag == TAG_IEDT_CPW) {
        errorDisplay = true;
    }
    bool bResultCPW = this->checkingComfirmPasswordStringInvalidate(_pwEditBox->getText(), _pwcEditBox->getText(), errorDisplay);
    if ( bResultCPW == false) {
        return false;
    }
    
    if (strlen(_birthEditBox->getText()) <= 0) {
        return false;
    }
    
    if (strlen(_cityEditBox->getText()) <= 0) {
        return false;
    }
    
    if (_maleRadio->getSelectedState() == false && _femaleRadio->getSelectedState() == false) {
        return false;
    }
    
    if ( _termAgreeCheckBox->getSelectedState() == false ) {
        return false;
    }

    return true;
}

void SignupLayer::touchEventForButton(Ref *pSender, TouchEventType type) {
    switch (type) {
        case TOUCH_EVENT_ENDED: {
            log("Button Touch Up");
            AccountLayerProtocol *parent = dynamic_cast<AccountLayerProtocol*>(this->getParent());
            parent->closeChildLayer();
        } break;
            
        case TOUCH_EVENT_BEGAN: log("Button Touch Down"); break;
        case TOUCH_EVENT_MOVED: log("Button Touch Move"); break;
        case TOUCH_EVENT_CANCELED: log("Button Touch Cancelled"); break;
        default: break;
    }
}

void SignupLayer::eventForControlButton(Ref *pSender, Control::EventType type) {
    switch (type) {
        case Control::EventType::TOUCH_UP_INSIDE: {
            int gender = 99;
            if (_maleRadio->getSelectedState() == true) {
                gender = 1;
            } else if (_femaleRadio->getSelectedState() == true) {
                gender = 2;
            }
            AccountLayerProtocol *parent = dynamic_cast<AccountLayerProtocol*>(this->getParent());
            parent->setAccountInfo(_idEditBox->getText(), _pwEditBox->getText(),
                                   _birthEditBox->getText(), _cityEditBox->getText(), gender);
            parent->nextPageLayer(3);
            this->removeFromParentAndCleanup(true);
        } break;
        default:
            break;
    }
}

void SignupLayer::selectedStateEventForRadioButton(Ref *pSender, TSRadioButtonEventType eventType) {
    int tag = ((Node*)pSender)->getTag();
    switch (tag) {
        case TAG_RADIO_MAIL: {
            if (_maleRadio->getSelectedState() == true) {
                _femaleRadio->setSelectedState(false);
            }
        } break;
        case TAG_RADIO_FEMAIL: {
            if (_femaleRadio->getSelectedState() == true) {
                _maleRadio->setSelectedState(false);
            }
        } break;
        default:
            break;
    }
    _signUpButton->setEnabled(this->checkSignupButtonEnable(tag));
}

void SignupLayer::selectedStateEventForCheckButton(Ref *pSender, CheckBoxEventType eventType) {
    _signUpButton->setEnabled(this->checkSignupButtonEnable(((Node*)pSender)->getTag()));
}

void SignupLayer::tsEditBoxReturn(extension::EditBox* editBox) {
    _signUpButton->setEnabled(this->checkSignupButtonEnable(editBox->getTag()));
}

#pragma mark - EditBox Utiltiy
bool SignupLayer::checkingIDStringInvalidate(const std::string &idString, bool errorDisplay) {
    if ( idString.empty() ) {
        return false;
    }
    
    size_t result = idString.find("@", 1);
    if (result == -1) {
        this->showIDStringError();
        return false;
    }
    result = idString.find(".", result+2);
    if (result == -1) {
        if (errorDisplay) {
            this->showIDStringError();
        }
        return false;
    }
    //    return isValidEmailAddress(idString.c_str());
    return true;
}

bool SignupLayer::checkingPasswordStringInvalidate(const std::string &pwString, bool errorDisplay) {
    if ( pwString.empty() ) {
        return false;
    }
    
    if ( pwString.length() < 6) {
        if (errorDisplay) {
            this->showPasswordStringError();
        }
        return false;
    }
    return true;
}

bool SignupLayer::checkingComfirmPasswordStringInvalidate(const std::string &pwString, const std::string &cpwString, bool errorDisplay) {
    if ( pwString.empty() ) {
        return false;
    }

    if ( cpwString.empty() ) {
        return false;
    }

    size_t result = pwString.compare(cpwString);
    if (result == 0) {
        return true;
    }
    if (errorDisplay) {
        this->showConfirmPasswordStringError();
    }
    return false;
}

void SignupLayer::getProvinceInfoProtocol() {
//    DoleAPI* api = DoleAPI::getInstance();
//    api->getProvinceInfo(this,//cocos2d::Ref *target,
//                         httpresponse_selector(SignupLayer::responseCallback)//cocos2d::network::SEL_HttpResponse pSelector
//                         );
}

void SignupLayer::responseCallback(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response )
{
    if (!response) return;
    
    printf("Tag: %s\n", response->getHttpRequest()->getTag());
    std::vector<char>*buffer = response->getResponseData();
    std::string str(buffer->begin(), buffer->end());
    
    printf("Json: %s\n", str.c_str());
    
    if (strlen(str.c_str()) > 0) {
        rapidjson::Document ret;
        ret.Parse<0>(str.c_str());
        bool retValue = ret["Return"].GetBool();
        int retCode = ret["ReturnCode"].GetInt();
        rapidjson::Value& provincesArray = ret["Provinces"];
        printf("Parsed Data: retValue: %d, retCode: %d", retValue, retCode);
        if (retValue) {
            //signup info local saving
            log("ProvinceInfo success Provinces Count: %d City:%s\n", provincesArray.Size(), provincesArray[rapidjson::SizeType(0)].GetString());
            _idEditBox->setEnabled(true);
            _pwEditBox->setEnabled(true);
            _pwcEditBox->setEnabled(true);
            _birthEditBox->setEnabled(true);
            _cityEditBox->setEnabled(true);
            _cityEditBox->setText(provincesArray[rapidjson::SizeType(0)].GetString());
        } else {
            //Error Display
            __String *failMessgge = __String::createWithFormat("ProvinceInfo Fail : code : %d", retCode);
            PopupLayer *popup = PopupLayer::create(POPUP_TYPE_WARNING ,failMessgge->getCString(), "Cancel", "OK");
            popup->setDelegate(this);
            popup->setPosition(Point::ZERO);
            this->addChild(popup);
        }
    }
}

void SignupLayer::popupButtonClicked(PopupLayer *popup, cocos2d::Ref *obj) {
    
}