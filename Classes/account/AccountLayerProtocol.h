//
//  AccountLayerProtocol.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 20..
//
//

#ifndef ACCOUNTLAYERPROTOCOL_H
#define ACCOUNTLAYERPROTOCOL_H

#include "ResourceManager.h"

typedef struct {
    char idString[40];
    char pwString[20];
    char birthString[20];
    char cityString[40];
    int gender;
} DoleAccountInfomation ;

class AccountLayerProtocol
{
public:
    virtual ~AccountLayerProtocol() {}
    
    static inline cocos2d::Size getLayerSize() { return LOGINLAYER_BACKGROUND_SIZE; }//Size(828,1224);
    virtual void closeChildLayer() = 0;
    virtual void loginDidEnded(bool result) = 0;
    virtual void signupDidEnded(bool result) = 0;
    virtual bool getFirstRun() = 0;
    virtual void nextPageLayer(int page) = 0;
    virtual void setAccountInfo(const char* idString, const char* pwString, const char* birthdayString, const char* cityString, int gender) = 0;
    virtual DoleAccountInfomation *getAccountInfo() = 0;
};

class AccountLayerDelegate {
public:
    virtual ~AccountLayerDelegate() {}
//    virtual void loginDidFinished(bool result) = 0;
};

static inline bool IsIOS() {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    return true;
#else
    return false;
#endif
}

static inline bool IsAndroid() {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    return true;
#else
    return false;
#endif
}

/*
요청 리스트

 22page : input field와 sub text의 width가 정의되어 있지 않음
 63page : label button의 경우 배경 sprite가 있어야 하는 문제
 67page : skip시 password 표시(gui guide)? id 표시(ui guide)?

*/

#endif ///ACCOUNTLAYERPROTOCOL_H
