//
//  NameTexture2D.h
//  bobby
//
//  Created by Dongwook, Kim on 14. 5. 12..
//
//

#ifndef __bobby__NameTexture2D__
#define __bobby__NameTexture2D__

#include <iostream>
#include "cocos2d.h"

class NameTexture2D : public cocos2d::Texture2D
{
public:
    NameTexture2D() : Texture2D() {};
    void setTextureName(GLuint textureName);
};

#endif /* defined(__bobby__NameTexture2D__) */
