//
//  CharacterRecordingScene.cpp
//  bobby
//
//  Created by Dongwook, Kim on 14. 5. 26..
//
//

#include "CharacterRecordingScene.h"
#include <cocosbuilder/CocosBuilder.h>
#include "BookMapScene.h"
#include "Character.h"
#include "ResourceManager.h"
#include "LabelExtension.h"
#include "DimLayer.h"
#include "AppDelegate.h"
#include "guide/GuideRecoding.h"
#include "guide/GuideSetPosition.h"
#include "TextColorButton.h"
#include "SimpleAudioEngine.h"
#include "Util.h"
#include "Define.h"
#include "ToastLayer.h"
#include "HomeScene.h"
#include "TSDoleNetProgressLayer.h"
#include "BGMManager.h"

#include "external/json/document.h"
#include "external/json/prettywriter.h"
#include "external/json/stringbuffer.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
#include "VoiceRecorder.h"
#include "TSAccountViewController.h"
#include "TSDoleCoinViewController.h"
#include "AppController.h"

#import "UIViewController+ChangeRootView.h"
#import "TSAccountLoginViewController.h"
#import "TSDoleCoinInfoViewController.h"

#import "MixpanelHelper.h"

#endif
#endif

#define MAX_TIME_DURATION       60 * 1000 // 60 sec

#define MAX_CHARACTER_NUM       7

#define TAG_BTN_MENU    500002

#define MAX_PLAY_FPS            120.0

#define MENU_MUSIC      0
#define MENU_BG         1
#define MENU_ITEM       2

#define TAG_PROGRESS_LEFT   10
#define TAG_PROGRESS_CENTER 11
#define TAG_PROGRESS_RIGHT  12

#define TAG_TRASH_NORMAL        20
#define TAG_TRASH_LEFT_OPEN     21
#define TAG_TRASH_RIGHT_OPEN    22
#define TAG_TRASH_BG            23

#define TAG_POPUP_BACK_TO_SET_POSITION      100
#define TAG_POPUP_RERECORDING               101
#define TAG_POPUP_REUSE_ITEM                102

#define TAG_BACKGROUND_SCROLL_VIEW          200
#define TAG_CHARACTER_SCROLL_VIEW          201

#define TAG_FRAME_BG  300

#define TAG_CHARACTER_ITEM_NORMAL  500
#define TAG_CHARACTER_ITEM_SELECT  501

#define TAG_RECORDING_ICON_TEXT    1000

#define FRAME_BASE_TAG                      10000
#define CHARACTER_BASE_TAG                      20000

#define SCALE_FOR_EDGE_X      1.1
#define SCALE_FOR_EDGE_Y      1.1

#define FRAME_ANCHOR_Y                      0.95

#define BACKGROUND_ID       -1

#define TAP_ANIMATION_DURATION_IN_SEC       (0.16 + 0.2) // about 10 frames in 60fps and 0.2 redundancy

#define CHARACTER_LAYER_START       2

#define TAG_POPUP_NONE                      404040400
#define TAG_POPUP_PURCHASE_NEEDLOGIN        404040401
#define TAG_POPUP_NOTENOUGH_COIN            404040402
#define TAG_POPUP_UNLOCKITEM                404040403
#define TAG_NET_PURCHASE                    404040404
#define TAG_NET_INVENTORY                   404040405


const double PI = std::acos(-1);

Scene* CharacterRecording::createScene(__String* bookKey, int chapterIndex)
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    scene->setTag(TAG_RECORDING_SCENE);
    // 'layer' is an autorelease object
    auto layer = CharacterRecording::create(bookKey, chapterIndex);

    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

CharacterRecording* CharacterRecording::create(__String* bookKey, int chapterIndex)
{
    CharacterRecording *pRet = new CharacterRecording();
    pRet->bookKey = bookKey;
    pRet->chapterIndex = chapterIndex;
    pRet->viewMode = RecordingViewMode::SetPosition;
    pRet->setTag(TAG_RECORDING);
    
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

// on "init" you need to initialize your instance
bool CharacterRecording::init()
{
    std::chrono::milliseconds start = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch());
        
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    _protocolExecutePurchase = nullptr;
    _protocolInventoryList = nullptr;
#endif

    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
//    cocos2d::Point origin = Director::getInstance()->getVisibleOrigin();
    
    mainLayer = Layer::create();
    mainLayer->setContentSize(cocos2d::Size(visibleSize.width, visibleSize.height));
    this->addChild(mainLayer);
    
    touchNumber = 0;
    localCharacterId = 0;
    scrollViewToggleAnimation = false;
    lastUnlockBackgroundKey = nullptr;
    
    background = Sprite::create("set_position_bg.png");
    background->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    background->setPosition(cocos2d::Point(0, -RECORDING_BOTTOM_SCROLL_AREA_HEIGHT));
    mainLayer->addChild(background, 0);
    
    // TODO: load from user default
    auto defaultBackground = __String::create("B001");
    this->changeBackground(defaultBackground);
    
    auto soundKey = __String::create("S000"); //default -> None
    this->changeSound(soundKey);
    
    openBgItem = false;
    openCharacterItem = false;
    openSoundItem = false;

    // back button
    auto btnBack = MenuItemImage::create("btn_map_back_normal.png",
                                         "btn_map_back_press.png",
                                         CC_CALLBACK_1(CharacterRecording::btnBackCallback, this));
    btnBack->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
	btnBack->setPosition(cocos2d::Point(BOOKMAP_BTN_BACK));
    backMenu = Menu::create(btnBack, NULL);
    backMenu->setPosition(cocos2d::Point::ZERO);
    this->addChild(backMenu, 12);
    
    // trash item
    trashItem = this->createTrashItem();
    trashItem->setVisible(false);
    
    // remove area
    removeArea = cocos2d::Node::create();
    removeArea->setContentSize(RECORDING_REMOVE_AREA);
    removeArea->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_BOTTOM);
    removeArea->setPosition(cocos2d::Point(visibleSize.width/2, 0));
    mainLayer->addChild(removeArea, 20);
    
    // recoding button
    recordingMenu = this->createRecordingMenu();
    
    //done button
    doneMenu = this->createDoneMenu();
    
    // bottom menus
    bottomMenu = this->createBottomMenu();
    
    // complete menus
    completeMenu = this->createCompleteMenu();
    
    // progress bar
    this->createProgressBar();
    
    // recording icon
    recordingIcon = this->createRecordingIcon();
    recordingIcon->setVisible(false);
    mainLayer->addChild(recordingIcon, 100);
    
    // backgroundScrollView
    backgroundScrollView = BackgroundScrollView::create(BG_RECORD);
    backgroundScrollView->setPosition(cocos2d::Point(0, -RECORDING_BOTTOM_SCROLL_AREA_HEIGHT));
    backgroundScrollView->setDelegate(this);
    backgroundScrollView->setVisible(false);
    mainLayer->addChild(backgroundScrollView, 1);
    
//    backgroundScrollView->setSelectBackground(0);
    
    // characterScrollView
    characterScrollView = CharacterScrollView::create(CHAR_RECORD);
    characterScrollView->setPosition(cocos2d::Point(0, -RECORDING_BOTTOM_SCROLL_AREA_HEIGHT));
    characterScrollView->setDelegate(this);
    characterScrollView->setVisible(false);
    mainLayer->addChild(characterScrollView, 1);
    
    // soundScrollView
    soundScrollView = SoundScrollView::create();
    soundScrollView->setPosition(cocos2d::Point(0, -RECORDING_BOTTOM_SCROLL_AREA_HEIGHT));
    soundScrollView->setDelegate(this);
    soundScrollView->setVisible(false);
    mainLayer->addChild(soundScrollView, 1);
    
    // create botton Label
    createBgButton = ui::Button::create("btn_home_item_create.png", "home_background_frame_del_pressed.png");
    createBgButton->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//    float visibleY = HOME_CREATE_BTN_VISIBLE_RECT.height - HOME_CREATE_BTN_VISIBLE_RECT.width * std::tan(PI/12); // 15 deg.
//    createBgButton->setPosition(cocos2d::Point(visibleSize.width - HOME_CREATE_BTN_VISIBLE_RECT.width, -RECORDING_BOTTOM_SCROLL_AREA_HEIGHT + visibleY));
    createBgButton->setPosition(cocos2d::Point(visibleSize.width - HOME_CREATE_BTN_VISIBLE_RECT.width - HOME_CREATE_BG_BTN.x,-RECORDING_BOTTOM_SCROLL_AREA_HEIGHT+HOME_CREATE_BG_BTN.y));
    createBgButton->addTouchEventListener(this, (cocos2d::ui::SEL_TouchEvent)(&CharacterRecording::createButtonClicked));
    mainLayer->addChild(createBgButton, 15);
    
    createButtonLabel = Label::createWithSystemFont(Device::getResString(Res::Strings::CC_LIST_CHARACTER_MAKE), FONT_NAME_BOLD, HOME_CREATE_FONTSIZE, HOME_CREATE_TEXTSIZE, TextHAlignment::CENTER, TextVAlignment::CENTER);
//    createButtonLabel->setColor(Color3B(0xa2, 0x9a, 0x88));
    createButtonLabel->setColor(Color3B(0x8d, 0x33, 0x0f));
    createButtonLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    createButtonLabel->setPosition(HOME_CREATE_TEXT);
    createButtonLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    
    createButtonLabel->enableShadow(Color4B(254,177,149,255),cocos2d::Size(0,-1), 0);
//    createButtonLabel->enableShadow(Color4B(255,255,255,255),LABEL_SHADOW_CAMERA_SIZE, 0);
    createBgButton->addChild(createButtonLabel);
//    createBgButtom->setRotation(-15); // y값 width*sin(15)
    
    this->setCreateButtons(false);
    
    touchTargetMapping = __Dictionary::create();
    touchTargetMapping->retain();
    
    touchData = __Array::create();
    touchData->retain();
    
    playingTouches = __Dictionary::create();
    playingTouches->retain();
    
    // character setting
    characters = __Array::create();
    characters->retain();
    
    prevCharacter = NULL;
    currentSelectedCharacter = NULL;

    transparentLayer = nullptr;
    selectedLockedCharacter = nullptr;
    
    // caf name
    unsigned long int timestamp= time(NULL);
    voiceFileName = __String::createWithFormat("voice_%ld.aac", timestamp);
    voiceFileName->retain();
    
    bgmFileName = nullptr;
    
    currentProgressCharacter = NULL;
    
    recordedTime = 0;
    playedTime = 0;
    recording = false;
    playing = false;
    paused = false;
    
    lastTouchTimeStamp = 0;
    playTouchTimeStampIndex = 0;
    playTouchElapsedTime = 0;

    characterProperties = __Dictionary::create();
    characterProperties->retain();
    
    this->scheduleUpdate();
    
    std::chrono::milliseconds done = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch());
    
    isPlatformShown = false;

    log("total time : %lld ms", (done-start).count());

    bool loadedPrevData = false;
    auto currentIndex = this->chapterIndex;
    for (int index = currentIndex ; (index >= currentIndex-1) && index > 0 ;index--){
        bool result = this->loadPrevData(index);
        if (result) {
            if (index != this->chapterIndex){
                this->showReUsePopup();
            }
            loadedPrevData = true;
            break;
        }
    }
    
    if (!loadedPrevData) {
        auto delayOpen = CallFunc::create([&](){
            this->showCharacterScrollView();
            MenuItemImage* characterItem = (MenuItemImage*)bottomMenu->getChildByTag(MENU_ITEM);
            characterItem->setNormalImage(Sprite::create("btn_home_item_selected.png"));
            openCharacterItem = true;
        });
        auto squence = Sequence::create(DelayTime::create(0.5f), delayOpen, NULL);
        mainLayer->runAction(squence);
    }

    return true;
}

void CharacterRecording::reloadScrollViews()
{
    this->characterScrollView->reloadData();
    this->backgroundScrollView->reloadData();
}

void CharacterRecording::setCreateButtons(bool visible)
{
    createBgButton->setVisible(visible);
    createBgButton->setEnabled(visible);
}

CharacterRecording::~CharacterRecording()
{
    characters->removeAllObjects();
    characters->release();
    
    characterProperties->removeAllObjects();
    characterProperties->release();
    
    touchTargetMapping->removeAllObjects();
    touchTargetMapping->release();
    
    touchData->removeAllObjects();
    touchData->release();
    
    playingTouches->removeAllObjects();
    playingTouches->release();
}

void CharacterRecording::clearPrevData()
{
    for(int i = (int)characters->count()-1; i >= 0; i--){
        Character *character = (Character *)characters->getObjectAtIndex(i);
        character->removeFromParent();
    }

    characters->removeAllObjects();
    characterProperties->removeAllObjects();
    touchTargetMapping->removeAllObjects();
    touchData->removeAllObjects();
    playingTouches->removeAllObjects();
    localCharacterId = 0;
    
    // TODO: load from user default
    auto defaultBackground = __String::create("B001");
    this->changeBackground(defaultBackground);

    std::string bgKey = defaultBackground->getCString();
    int backgroundIndex = backgroundScrollView->getBackgroundIndex(bgKey);
    backgroundScrollView->setSelectBackground(backgroundIndex);
    
    auto soundKey = __String::create("S000"); //default -> None
    this->changeSound(soundKey);

    std::string selectedSoundKey = soundKey->getCString();
    int soundIndex = soundScrollView->getSoundIndex(selectedSoundKey);
    soundScrollView->setSelectSound(soundIndex);
}

bool CharacterRecording::loadPrevData(int chapterIndex)
{
    auto ud = UserDefault::getInstance();
    auto bookJsonString = ud->getStringForKey(this->bookKey->getCString());
    
    if (bookJsonString != "") {
        rapidjson::Document bookDoc;
        bookDoc.Parse<0>(bookJsonString.c_str());
        rapidjson::Value& chapters = bookDoc["chapters"];
        __String *chapterName = __String::createWithFormat("chapter%d", chapterIndex);
        
        rapidjson::Value& chapterData = chapters[chapterName->getCString()];
        if (!chapterData.IsNull()) {
            bool prevChapterSaved = chapterData.HasMember("char_datas");
            if ( (chapterIndex != this->chapterIndex) && !prevChapterSaved ) {
                return false;
            }
            
            bool hasPrevData = chapterData.HasMember("char_last_datas");
            if (!hasPrevData) {
                return false;
            }
            
            int minZOrder = 100;
            int maxZOrder = 0;
            
            __Dictionary *zOrderDic = __Dictionary::create();
            
            // get last status of prev chapter
            rapidjson::Value& charPropertyArray = chapterData["char_last_datas"];
            
            for (int index = 0; index < charPropertyArray.Size(); index++) {
                rapidjson::Value& charPropertyDic = charPropertyArray[rapidjson::SizeType(index)];
                
                __Dictionary* propertyDic = __Dictionary::create();

//                int charID = charPropertyDic["char_id"].GetInt();
                
                std::string charKey =  charPropertyDic["char_key"].GetString();
                
                __String *charKeyString = __String::create(charKey);
                
                float orgPosX = (float)charPropertyDic["pos_x"].GetDouble();
                __Float* posX = __Float::create(orgPosX);
                
                float orgPosY = (float)charPropertyDic["pos_y"].GetDouble();
                __Float* posY = __Float::create(orgPosY);
                
                __Float* scale = __Float::create((float)charPropertyDic["scale"].GetDouble());
                __Float* angle = __Float::create((float)charPropertyDic["angle"].GetDouble());
                __Integer* direction = __Integer::create(charPropertyDic["direction"].GetInt());
                __Integer* zOrder = __Integer::create(charPropertyDic["z_order"].GetInt());
                
                propertyDic->setObject(charKeyString, "char_key");
                propertyDic->setObject(posX, "pos_x");
                propertyDic->setObject(posY, "pos_y");
                propertyDic->setObject(scale, "scale");
                propertyDic->setObject(angle, "angle");
                propertyDic->setObject(direction, "direction");
                propertyDic->setObject(zOrder, "z_order");
                
                characterProperties->setObject(propertyDic, localCharacterId);
                
                Character *character = Character::create(charKey.c_str());
                character->lockInScreen();
                character->characterID = localCharacterId;
                character->setScale(scale->getValue());
                character->setPosition(posX->getValue(), posY->getValue());
                character->setRotation(angle->getValue());
                character->setDirection((CharacterDirection)direction->getValue());
                
                int zOrderValue = zOrder->getValue();
                
                minZOrder = MIN(minZOrder, zOrderValue);
                maxZOrder = MAX(maxZOrder, zOrderValue);
                
                mainLayer->addChild(character, CHARACTER_LAYER_START+zOrderValue);
                characters->addObject(character);
                
                zOrderDic->setObject(character, zOrderValue);
                localCharacterId++;
            }
            
            for (int i = minZOrder; i <= maxZOrder; i++) { //  sort by z order
                auto c = zOrderDic->objectForKey(i);
                if (c != NULL) {
                    Character *character = (Character *)c;
                    bringToFront(character);
                }
            }
            
            auto backgroundKeyString = chapterData["background"].GetString();
            this->selectedBackgroundKey = __String::create(backgroundKeyString);
            this->selectedBackgroundKey->retain();
            this->changeBackground(this->selectedBackgroundKey);
            
            std::string bgKey = selectedBackgroundKey->getCString();
            int backgroundIndex = backgroundScrollView->getBackgroundIndex(bgKey);
            backgroundScrollView->setSelectBackground(backgroundIndex);
            
            if(unlockTalkBox){ // temp code
                unlockTalkBox->removeFromParentAndCleanup(true);
                unlockTalkBox = nullptr;
            }
            
            auto soundKeyString = chapterData["sound"].GetString();
            this->selectedSoundKey = __String::create(soundKeyString);
            this->selectedSoundKey->retain();
            this->changeSound(this->selectedSoundKey);
            
            std::string soundKey = selectedSoundKey->getCString();
            int soundIndex = soundScrollView->getSoundIndex(soundKey);
            soundScrollView->setSelectSound(soundIndex);
            
            return true;
        }
    }
    
    return false;
}

void CharacterRecording::showDimLayer(bool show)
{
    if(show){
        if (dimLayer == nullptr) {
            cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
            dimLayer = LayerColor::create(Color4B(0x00,0x00,0x00,0xff * 0.2), visibleSize.width, visibleSize.height);
            mainLayer->addChild(dimLayer, 1);
        }
    } else {
        if (dimLayer != nullptr) {
            dimLayer->removeFromParent();
            dimLayer = nullptr;
        }
    }
}

Node *CharacterRecording::createTrashItem()
{
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();

    auto trashNode = cocos2d::Node::create();
    Sprite *normalSprite = Sprite::create("set_position_item_remove_normal.png");
    Sprite *leftOpenSprite = Sprite::create("set_position_item_remove_pressed_01.png");
    Sprite *rightOpenSprite = Sprite::create("set_position_item_remove_pressed_02.png");
    Sprite *trashBg = Sprite::create("set_position_item_remove_pressed_bg.png");

    auto trashSize = normalSprite->getContentSize();
    
    trashNode->setContentSize(trashSize);
    
    trashBg->setPosition(cocos2d::Point(trashSize.width/2, 0));
    trashBg->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_BOTTOM);
    trashBg->setTag(TAG_TRASH_BG);
    trashBg->setVisible(false);
    
    trashNode->addChild(trashBg);
    
    normalSprite->setPosition(cocos2d::Point(trashSize.width/2, 0));
    normalSprite->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_BOTTOM);
    normalSprite->setTag(TAG_TRASH_NORMAL);
    normalSprite->setVisible(true);
    trashNode->addChild(normalSprite);

    leftOpenSprite->setPosition(cocos2d::Point(trashSize.width/2, 0));
    leftOpenSprite->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_BOTTOM);
    leftOpenSprite->setTag(TAG_TRASH_LEFT_OPEN);
    leftOpenSprite->setVisible(false);
    trashNode->addChild(leftOpenSprite);

    rightOpenSprite->setPosition(cocos2d::Point(trashSize.width/2, 0));
    rightOpenSprite->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_BOTTOM);
    rightOpenSprite->setTag(TAG_TRASH_RIGHT_OPEN);
    rightOpenSprite->setVisible(false);

    trashNode->addChild(rightOpenSprite);
    
    trashNode->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_BOTTOM);
    trashNode->setPosition(cocos2d::Point(visibleSize.width/2, 0));
    mainLayer->addChild(trashNode, 20);
    
//    auto trashMenuItem = MenuItemImage::create("set_position_item_remove_normal.png",
//                                          "set_position_item_remove_pressed.png");
//    trashMenuItem->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
//	trashMenuItem->setPosition(cocos2d::Point(visibleSize.width, 0));
//    trashMenuItem->setVisible(false);
//    
//    auto trashMenu = Menu::create(trashMenuItem, NULL);
//    trashMenu->setPosition(cocos2d::Point::ZERO);
//    mainLayer->addChild(trashMenu, 20);
    return trashNode;
}

Menu *CharacterRecording::createRecordingMenu()
{
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();

    auto recordPress = this->createResizableButton("btn_set_record_press_left.png",
                                                   "btn_set_record_press_center.png",
                                                   "btn_set_record_press_right.png",
                                                   "btn_set_record_icon.png",
                                                   Device::getResString(Res::Strings::CC_RECORDING_BUTTON_TITLE_RECOD),//"Record",
                                                   RECORDING_RECORD_TEXT_FONT_SIZE,
                                                   RECORDING_RECORD_ICON,
                                                   RECORDING_RECORD_TEXT_LEFT_PADDING,
                                                   RECORDING_RECORD_TEXT_RIGHT_PADDING);
    
    auto recordNormal = this->createResizableButton("btn_set_record_normal_left.png",
                                                    "btn_set_record_normal_center.png",
                                                    "btn_set_record_normal_right.png",
                                                    "btn_set_record_icon.png",
                                                    Device::getResString(Res::Strings::CC_RECORDING_BUTTON_TITLE_RECOD),//"Record",
                                                    RECORDING_RECORD_TEXT_FONT_SIZE,
                                                    RECORDING_RECORD_ICON,
                                                    RECORDING_RECORD_TEXT_LEFT_PADDING,
                                                    RECORDING_RECORD_TEXT_RIGHT_PADDING);
    
    auto recordingMenuItem = MenuItemSprite::create(recordNormal,
                                                    recordPress,
                                                    CC_CALLBACK_1(CharacterRecording::recordingMenuCallback, this));
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    recordingMenuItem->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
	recordingMenuItem->setPosition(cocos2d::Point(visibleSize.width-RECORDING_RECORD_BUTTON.x, RECORDING_RECORD_BUTTON.y));
#else
    recordingMenuItem->setAnchorPoint(cocos2d::Point::ANCHOR_TOP_RIGHT);
	recordingMenuItem->setPosition(cocos2d::Point(visibleSize.width-RECORDING_RECORD_BUTTON.x, visibleSize.height - RECORDING_RECORD_BUTTON.y));
#endif
    
    auto recordingMenu = Menu::create(recordingMenuItem, NULL);
    recordingMenu->setPosition(cocos2d::Point::ZERO);
    mainLayer->addChild(recordingMenu, 12);
    
    return recordingMenu;
}

Menu *CharacterRecording::createDoneMenu()
{
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();

    auto doneNormal = this->createResizableButton("btn_set_record_done_normal_left.png",
                                                  "btn_set_record_done_normal_center.png",
                                                  "btn_set_record_done_normal_right.png",
                                                  "btn_set_record_done_icon.png",
                                                  Device::getResString(Res::Strings::CC_RECORDING_BUTTON_TITLE_DONE),//"Done",
                                                  RECORDING_DONE_TEXT_FONT_SIZE,
                                                  RECORDING_DONE_ICON,
                                                  RECORDING_DONE_TEXT_LEFT_PADDING,
                                                  RECORDING_DONE_TEXT_RIGHT_PADDING);
    
    auto donePress = this->createResizableButton("btn_set_record_done_press_left.png",
                                                 "btn_set_record_done_press_center.png",
                                                 "btn_set_record_done_press_right.png",
                                                 "btn_set_record_done_icon.png",
                                                 Device::getResString(Res::Strings::CC_RECORDING_BUTTON_TITLE_DONE),//"Done",
                                                 RECORDING_DONE_TEXT_FONT_SIZE,
                                                 RECORDING_DONE_ICON,
                                                 RECORDING_DONE_TEXT_LEFT_PADDING,
                                                 RECORDING_DONE_TEXT_RIGHT_PADDING);
    
    doneMenuItem = MenuItemSprite::create(doneNormal, donePress,
                                          CC_CALLBACK_1(CharacterRecording::doneMenuCallback, this));
    
    auto doneDisable = this->createResizableButton("btn_set_record_done_dim_left.png",
                                                   "btn_set_record_done_dim_center.png",
                                                   "btn_set_record_done_dim_right.png",
                                                   "btn_set_record_done_icon_dim.png",
                                                   Device::getResString(Res::Strings::CC_RECORDING_BUTTON_TITLE_DONE),//"Done",
                                                   RECORDING_DONE_TEXT_FONT_SIZE,
                                                   RECORDING_DONE_ICON,
                                                   RECORDING_DONE_TEXT_LEFT_PADDING,
                                                   RECORDING_DONE_TEXT_RIGHT_PADDING);
    disabledDoneMenuItem = MenuItemSprite::create(doneDisable, doneDisable);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    doneMenuItem->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
	doneMenuItem->setPosition(cocos2d::Point(visibleSize.width-RECORDING_DONE_BUTTON.x, RECORDING_DONE_BUTTON.y));
    disabledDoneMenuItem->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
	disabledDoneMenuItem->setPosition(cocos2d::Point(visibleSize.width-RECORDING_DONE_BUTTON.x, RECORDING_DONE_BUTTON.y));
#else
    doneMenuItem->setAnchorPoint(cocos2d::Point::ANCHOR_TOP_RIGHT);
	doneMenuItem->setPosition(cocos2d::Point(visibleSize.width-RECORDING_DONE_BUTTON.x, visibleSize.height - RECORDING_DONE_BUTTON.y));
    disabledDoneMenuItem->setAnchorPoint(cocos2d::Point::ANCHOR_TOP_RIGHT);
	disabledDoneMenuItem->setPosition(cocos2d::Point(visibleSize.width-RECORDING_DONE_BUTTON.x, visibleSize.height - RECORDING_DONE_BUTTON.y));
#endif
    doneMenuItem->setVisible(false);
    disabledDoneMenuItem->setVisible(true);
    disabledDoneMenuItem->setEnabled(true);

    auto doneMenu = Menu::create(doneMenuItem, disabledDoneMenuItem, NULL);
    doneMenu->setPosition(cocos2d::Point::ZERO);
    doneMenu->setVisible(false);
    
    this->addChild(doneMenu, 12);
    
    return doneMenu;
}

Menu *CharacterRecording::createBottomMenu()
{
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();

    auto musicMenu = MenuItemImage::create("btn_set_position_music_normal.png","btn_set_position_music_selected.png",CC_CALLBACK_1(CharacterRecording::itemButtonClicked, this));
    musicMenu->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    musicMenu->setPosition(cocos2d::Point::ZERO);
    
    auto backgroundMenu = MenuItemImage::create("btn_home_bg_normal.png","btn_home_bg_selected.png",CC_CALLBACK_1(CharacterRecording::itemButtonClicked, this));
    backgroundMenu->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    backgroundMenu->setPosition(cocos2d::Point(musicMenu->getContentSize().width+RECORDING_ITEM_PADDING, 0));
    
    auto itemMenu = MenuItemImage::create("btn_home_item_normal.png","btn_home_item_selected.png",CC_CALLBACK_1(CharacterRecording::itemButtonClicked, this));
    itemMenu->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    itemMenu->setPosition(cocos2d::Point((musicMenu->getContentSize().width+RECORDING_ITEM_PADDING)*2, 0));
    
    musicMenu->setTag(MENU_MUSIC);
    backgroundMenu->setTag(MENU_BG);
    itemMenu->setTag(MENU_ITEM);
    
    auto bottomMenu = Menu::create(musicMenu, backgroundMenu, itemMenu, NULL);
    bottomMenu->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
    bottomMenu->setPosition(cocos2d::Point(visibleSize.width-RECORDING_ITEM_RIGHT_PADDING - musicMenu->getContentSize().width*3 - RECORDING_ITEM_PADDING*2, RECORDING_ITEM_BOTTOM_PADDING));
    bottomMenu->setTag(TAG_BTN_MENU);
    mainLayer->addChild(bottomMenu, 12);
    return bottomMenu;
}

Menu *CharacterRecording::createCompleteMenu()
{
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();

    auto saveNormal = this->createResizableButton("btn_set_record_save_normal_left.png",
                                                  "btn_set_record_save_normal_center.png",
                                                  "btn_set_record_save_normal_right.png",
                                                  "btn_set_record_save_icon.png",
                                                  Device::getResString(Res::Strings::CC_RECORDING_BUTTON_TITLE_SAVE),//"Save",
                                                  RECORDING_SAVE_TEXT_FONT_SIZE,
                                                  RECORDING_SAVE_ICON,
                                                  RECORDING_SAVE_TEXT_LEFT_PADDING,
                                                  RECORDING_SAVE_TEXT_RIGHT_PADDING);
    
    auto savePress = this->createResizableButton("btn_set_record_save_press_left.png",
                                                 "btn_set_record_save_press_center.png",
                                                 "btn_set_record_save_press_right.png",
                                                 "btn_set_record_save_icon.png",
                                                 Device::getResString(Res::Strings::CC_RECORDING_BUTTON_TITLE_SAVE),//"Save",
                                                 RECORDING_SAVE_TEXT_FONT_SIZE,
                                                 RECORDING_SAVE_ICON,
                                                 RECORDING_SAVE_TEXT_LEFT_PADDING,
                                                 RECORDING_SAVE_TEXT_RIGHT_PADDING);
    
    auto saveMenuItem = MenuItemSprite::create(saveNormal,
                                               savePress,
                                               CC_CALLBACK_1(CharacterRecording::saveMenuCallback, this));
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    saveMenuItem->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
	saveMenuItem->setPosition(cocos2d::Point(visibleSize.width-RECORDING_SAVE_BUTTON.x, RECORDING_SAVE_BUTTON.y));
#else
    saveMenuItem->setAnchorPoint(cocos2d::Point::ANCHOR_TOP_RIGHT);
	saveMenuItem->setPosition(cocos2d::Point(visibleSize.width-RECORDING_SAVE_BUTTON.x, visibleSize.height - RECORDING_SAVE_BUTTON.y));
#endif
    
    auto rerecordNormal = this->createResizableButton("btn_set_record_normal_left.png",
                                                      "btn_set_record_normal_center.png",
                                                      "btn_set_record_normal_right.png",
                                                      "btn_set_rerecord_icon.png",
                                                      Device::getResString(Res::Strings::CC_RECORDING_BUTTON_TITLE_RERECORDING),//"Rerecord",
                                                      RECORDING_RERECORD_TEXT_FONT_SIZE,
                                                      RECORDING_RERECORD_ICON,
                                                      RECORDING_RERECORD_TEXT_LEFT_PADDING,
                                                      RECORDING_RERECORD_TEXT_RIGHT_PADDING);
    
    auto rerecordPress = this->createResizableButton("btn_set_record_press_left.png",
                                                     "btn_set_record_press_center.png",
                                                     "btn_set_record_press_right.png",
                                                     "btn_set_rerecord_icon.png",
                                                     Device::getResString(Res::Strings::CC_RECORDING_BUTTON_TITLE_RERECORDING),//"Rerecord",
                                                     RECORDING_RERECORD_TEXT_FONT_SIZE,
                                                     RECORDING_RERECORD_ICON,
                                                     RECORDING_RERECORD_TEXT_LEFT_PADDING,
                                                     RECORDING_RERECORD_TEXT_RIGHT_PADDING);
    
    auto rerecordingMenuItem = MenuItemSprite::create(rerecordNormal,
                                                      rerecordPress,
                                                      CC_CALLBACK_1(CharacterRecording::rerecordMenuCallback, this));
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    rerecordingMenuItem->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
	rerecordingMenuItem->setPosition(
			cocos2d::Point(visibleSize.width-RECORDING_SAVE_BUTTON.x - saveNormal->getContentSize().width - RECORDING_RERECORD_BUTTON_PADDING,
							RECORDING_SAVE_BUTTON.y));
#else
    rerecordingMenuItem->setAnchorPoint(cocos2d::Point::ANCHOR_TOP_RIGHT);
	rerecordingMenuItem->setPosition(
			cocos2d::Point(visibleSize.width-RECORDING_SAVE_BUTTON.x - saveNormal->getContentSize().width - RECORDING_RERECORD_BUTTON_PADDING,
							visibleSize.height - RECORDING_SAVE_BUTTON.y));
#endif
    
    playMenuItem = MenuItemImage::create("btn_set_record_play_normal.png", "btn_set_record_play_pressed.png",
                                         CC_CALLBACK_1(CharacterRecording::playMenuCallback, this));
    playMenuItem->setPosition(cocos2d::Point(visibleSize.width/2, visibleSize.height/2));
    
    auto pageTitle = this->createTitle(this->chapterIndex);
    pageTitle->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_BOTTOM);
    pageTitle->setPosition(cocos2d::Point(playMenuItem->getContentSize().width/2, MAP_PLAY_TITLE_BOTTOM_MARGIN));
    playMenuItem->addChild(pageTitle);
    
    auto completeMenu = Menu::create(rerecordingMenuItem, saveMenuItem, playMenuItem, NULL);
    completeMenu->setPosition(cocos2d::Point::ZERO);
    completeMenu->setVisible(false);
    mainLayer->addChild(completeMenu, 12);

    return completeMenu;
}

void CharacterRecording::createProgressBar()
{
    // progress bar bg
    progressBarBg = cocos2d::Node::create();
    progressBarBg->setContentSize(RECORDING_PROGRESS_BAR_SIZE);
    progressBarBg->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    progressBarBg->setPosition(RECORDING_PROGRESS_BAR);
    progressBarBg->setVisible(false);
    auto progressBarBgLeft = Sprite::create("recording_progress_bg_left.png");
    progressBarBgLeft->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    
    auto progressBarBgRight = Sprite::create("recording_progress_bg_right.png");
    
    auto centerBgWidth = RECORDING_PROGRESS_BAR_SIZE.width - progressBarBgLeft->getContentSize().width - progressBarBgRight->getContentSize().width;
    auto progressBarBgCenter = Sprite::create("recording_progress_bg_center.png", cocos2d::Rect(0, 0, centerBgWidth, RECORDING_PROGRESS_BAR_SIZE.height));
    progressBarBgCenter->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    progressBarBgCenter->setPosition(cocos2d::Point(progressBarBgLeft->getContentSize().width, 0));
    
    progressBarBgRight->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    progressBarBgRight->setPosition(cocos2d::Point(progressBarBgLeft->getContentSize().width + centerBgWidth, 0));
    
    progressBarBg->addChild(progressBarBgLeft);
    progressBarBg->addChild(progressBarBgRight);
    progressBarBg->addChild(progressBarBgCenter);
    
    mainLayer->addChild(progressBarBg, 12);
    
    // mark holder
    progressMarkHolder = cocos2d::Node::create();
    progressMarkHolder->setContentSize(RECORDING_PROGRESS_BAR_SIZE);
    progressMarkHolder->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    progressMarkHolder->setPosition(RECORDING_PROGRESS_BAR);
    mainLayer->addChild(progressMarkHolder, 13);
    
    // progress bar
    progressBar = cocos2d::Node::create();
    progressBar->setContentSize(RECORDING_PROGRESS_BAR_SIZE);
    progressBar->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    progressBar->setPosition(RECORDING_PROGRESS_BAR);
    progressBar->setVisible(false);
    auto progressBarLeft = Sprite::create("recording_progress_left.png");
    progressBarLeft->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    progressBarLeft->setTag(TAG_PROGRESS_LEFT);
    
    auto progressBarRight = Sprite::create("recording_progress_right.png");
    progressBarRight->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    progressBarRight->setPosition(cocos2d::Point(progressBarBgLeft->getContentSize().width, 0));
    progressBarRight->setTag(TAG_PROGRESS_RIGHT);
    
    progressBar->addChild(progressBarLeft);
    progressBar->addChild(progressBarRight);
    
    mainLayer->addChild(progressBar, 12);
    
    // play progress bar bg
    playProgressBarBg = cocos2d::Node::create();
    playProgressBarBg->setContentSize(RECORDING_PROGRESS_BAR_SIZE);
    playProgressBarBg->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarBg->setPosition(RECORDING_PROGRESS_BAR);
    playProgressBarBg->setVisible(false);
    auto playProgressBarBgLeft = Sprite::create("recording_play_progress_bg_left.png");
    playProgressBarBgLeft->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    
    auto playProgressBarBgRight = Sprite::create("recording_play_progress_bg_right.png");
    
    auto playCenterBgWidth = RECORDING_PROGRESS_BAR_SIZE.width - playProgressBarBgLeft->getContentSize().width - playProgressBarBgRight->getContentSize().width;
    auto playProgressBarBgCenter = Sprite::create("recording_play_progress_bg_center.png", cocos2d::Rect(0, 0, playCenterBgWidth, RECORDING_PROGRESS_BAR_SIZE.height));
    playProgressBarBgCenter->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarBgCenter->setPosition(cocos2d::Point(playProgressBarBgLeft->getContentSize().width, 0));
    
    playProgressBarBgRight->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarBgRight->setPosition(cocos2d::Point(playProgressBarBgLeft->getContentSize().width + playCenterBgWidth, 0));
    
    playProgressBarBg->addChild(playProgressBarBgLeft);
    playProgressBarBg->addChild(playProgressBarBgRight);
    playProgressBarBg->addChild(playProgressBarBgCenter);
    playProgressBarBg->setVisible(false);
    
    mainLayer->addChild(playProgressBarBg, 12);
    
    // play progress bar
    playProgressBar = cocos2d::Node::create();
    playProgressBar->setContentSize(RECORDING_PROGRESS_BAR_SIZE);
    playProgressBar->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBar->setPosition(RECORDING_PROGRESS_BAR);
    playProgressBar->setVisible(false);
    auto playProgressBarLeft = Sprite::create("recording_play_progress_left.png");
    playProgressBarLeft->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarLeft->setTag(TAG_PROGRESS_LEFT);
    
    auto playProgressBarRight = Sprite::create("recording_play_progress_right.png");
    playProgressBarRight->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarRight->setPosition(cocos2d::Point(playProgressBarLeft->getContentSize().width, 0));
    playProgressBarRight->setTag(TAG_PROGRESS_RIGHT);
    
    playProgressBar->addChild(playProgressBarLeft);
    playProgressBar->addChild(playProgressBarRight);
    playProgressBar->setVisible(false);
    
    mainLayer->addChild(playProgressBar, 12);
}


void CharacterRecording::setProgress(float value)
{
    if (value > 1.0f) {
        log("progress bar - reached to end");
        return;
    }
    
    Node *targetBar = NULL;
    std::string centerFileName;
    if (viewMode == RecordingViewMode::Recording) {
        targetBar = progressBar;
        centerFileName = "recording_progress_center.png";
    } else if (viewMode == RecordingViewMode::Playing){
        targetBar = playProgressBar;
        centerFileName = "recording_play_progress_center.png";
    } else {
        log("invalid view mode!");
        return;
    }
    
    auto center = targetBar->getChildByTag(TAG_PROGRESS_CENTER);
    
    auto progressBarLeft = targetBar->getChildByTag(TAG_PROGRESS_LEFT);
    auto progressBarRight = targetBar->getChildByTag(TAG_PROGRESS_RIGHT);

    auto progressWidth = RECORDING_PROGRESS_BAR_SIZE.width - progressBarLeft->getContentSize().width - progressBarRight->getContentSize().width;
    auto centerWidth = std::ceil(progressWidth * value);
    
    if (center) {
        center->removeFromParentAndCleanup(true);
    }

    if (centerWidth > 0) {
        auto newCenter = Sprite::create(centerFileName, cocos2d::Rect(0, 0, centerWidth, RECORDING_PROGRESS_BAR_SIZE.height));
        newCenter->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        newCenter->setPosition(cocos2d::Point(progressBarLeft->getContentSize().width, 0));
        newCenter->setTag(TAG_PROGRESS_CENTER);
        targetBar->addChild(newCenter);
    }
    progressBarRight->setPosition(cocos2d::Point(progressBarLeft->getContentSize().width + centerWidth, 0));
    
    if (viewMode == RecordingViewMode::Recording && currentProgressCharacter) {
        auto position = progressBarRight->getPosition();
        currentProgressCharacter->setPosition(cocos2d::Point(position.x+progressBarRight->getContentSize().width/2,
                                                             -RECORDING_PROGRESS_MARK_BOTTOM_MARGIN));
    }
}

void CharacterRecording::addMarkToProgress(Character *newChar)
{
    Sprite* newCharSprite;
    if (newChar) {        
        auto indicatorFileName = newChar->getIndicatorFileName();
        newCharSprite = Sprite::create(indicatorFileName);
    } else { // bg
        newCharSprite = Sprite::create("recording_progress_icon_bg.png");
    }
    
    // TODO: resolve sprite duplication
    currentProgressCharacter = newCharSprite;

    auto rightSprite = progressBar->getChildByTag(TAG_PROGRESS_RIGHT);
    auto position = rightSprite->getPosition();
    newCharSprite->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_BOTTOM);
    newCharSprite->setPosition(cocos2d::Point(position.x+rightSprite->getContentSize().width/2, -RECORDING_PROGRESS_MARK_BOTTOM_MARGIN));
    progressMarkHolder->addChild(newCharSprite, 13);
}

void CharacterRecording::clearMark()
{
    progressMarkHolder->removeAllChildren();
    currentProgressCharacter = NULL;
}

void CharacterRecording::clearRecordData()
{
    recordedTime = 0;
    lastTouchTimeStamp = 0;
    recordingTouchNum = 0;
    std::chrono::milliseconds zero(0);
    lastTouchTime = zero;
    touchData->removeAllObjects();
    
    doneMenuItem->setVisible(false);
    disabledDoneMenuItem->setVisible(true);
    disabledDoneMenuItem->setEnabled(true);

    this->clearMark();
    prevCharacter = NULL;
}

void CharacterRecording::lockMultiTouchAllCharacter()
{
    for(int i = 0; i < characters->count(); i++){
        Character *character = (Character *)characters->getObjectAtIndex(i);
        character->lockMultiTouch();
    }
}

void CharacterRecording::unlockMultiTouchAllCharacter()
{
    for(int i = 0; i < characters->count(); i++){
        Character *character = (Character *)characters->getObjectAtIndex(i);
        character->unlockMultiTouch();
    }
}

void CharacterRecording::setViewMode(RecordingViewMode viewMode)
{
    this->viewMode = viewMode;
    
    switch (viewMode) {
        case RecordingViewMode::SetPosition:
        {
            recordingMenu->setVisible(true);
            bottomMenu->setVisible(true);
            doneMenu->setVisible(false);
            completeMenu->setVisible(false);
            
            progressBarBg->setVisible(false);
            progressBar->setVisible(false);
            playProgressBar->setVisible(false);
            playProgressBarBg->setVisible(false);
            
            showRecordingIcon(false);
            this->unlockMultiTouchAllCharacter();
        }
            break;
        case RecordingViewMode::Recording:
        {
            recordingMenu->setVisible(false);
            bottomMenu->setVisible(false);
            doneMenu->setVisible(true);
            completeMenu->setVisible(false);

            progressBarBg->setVisible(true);
            progressBar->setVisible(false);
            playProgressBar->setVisible(false);
            playProgressBarBg->setVisible(false);
            
            this->lockMultiTouchAllCharacter();
#ifdef _GUIDE_ENABLE_
            GuideRecoding::create(this);
#endif
        }
            break;
        case RecordingViewMode::RecordingComplete:
        {
            recordingMenu->setVisible(false);
            bottomMenu->setVisible(false);
            doneMenu->setVisible(false);
            completeMenu->setVisible(true);
            playMenuItem->setVisible(true);

            progressBarBg->setVisible(true);
            progressBar->setVisible(true);
            playProgressBar->setVisible(false);
            playProgressBarBg->setVisible(false);
            
            showRecordingIcon(false);
        }
            break;
        case RecordingViewMode::Playing:
        {
            recordingMenu->setVisible(false);
            bottomMenu->setVisible(false);
            doneMenu->setVisible(false);
            playMenuItem->setVisible(false);

            playProgressBar->setVisible(true);
            playProgressBarBg->setVisible(true);
            progressBarBg->setVisible(false);
            progressBar->setVisible(false);
            
            showRecordingIcon(false);
        }
        default:
            break;
    }
}

cocos2d::Node * CharacterRecording::createResizableButton(const std::string& leftFileName, const std::string& centerFileName, const std::string& rightFileName,
                                                          const std::string& iconFileName, const std::string& text, float fontSize, cocos2d::Point iconPos,
                                                          float textLeftPadding, float textRightPadding)
{
    auto leftSprite = Sprite::create(leftFileName);
    leftSprite->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    leftSprite->setPosition(cocos2d::Point(0, 0));
    
    auto rightSprite = Sprite::create(rightFileName);
    rightSprite->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
    
    auto iconSprite = Sprite::create(iconFileName);
    iconSprite->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    iconSprite->setPosition(iconPos);
    
    auto textLabel = LabelExtension::create(text, FONT_NAME_BOLD, fontSize);
    textLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    auto textWidth = textLabel->getTextWidth();
    
    auto leftPadding = iconPos.x + iconSprite->getContentSize().width + textLeftPadding;
    textLabel->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_LEFT);
    textLabel->setPosition(cocos2d::Point(leftPadding, leftSprite->getContentSize().height/2));
    
    auto buttonTotalWidth = leftPadding + textWidth + textRightPadding;
    auto centerWidth = 0;
    
    if (buttonTotalWidth < (leftSprite->getContentSize().width + rightSprite->getContentSize().width)) {
        buttonTotalWidth = leftSprite->getContentSize().width + rightSprite->getContentSize().width;
    } else {
        centerWidth = buttonTotalWidth - leftSprite->getContentSize().width - rightSprite->getContentSize().width;
    }
    
    auto buttonSize = cocos2d::Size(buttonTotalWidth, leftSprite->getContentSize().height);
    rightSprite->setPosition(cocos2d::Point(buttonSize.width, 0));
    
    auto button = Node::create();
    
    if (centerWidth > 0) {
        auto centerSprite = Sprite::create(centerFileName, cocos2d::Rect(0, 0, centerWidth, leftSprite->getContentSize().height));
        centerSprite->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        centerSprite->setPosition(cocos2d::Point(leftSprite->getContentSize().width, 0));
        button->addChild(centerSprite);
    }
    
    button->setContentSize(buttonSize);
    button->addChild(leftSprite);
    button->addChild(rightSprite);
    button->addChild(iconSprite);
    button->addChild(textLabel);
    
    return button;
}

cocos2d::Node * CharacterRecording::createRecordingIcon()
{
    auto leftSprite = Sprite::create("btn_set_record_recording_normal_left.png");
    leftSprite->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    leftSprite->setPosition(cocos2d::Point(0, 0));
    
    auto rightSprite = Sprite::create("btn_set_record_recording_normal_right.png");
    rightSprite->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);

    auto textLabel = LabelExtension::create(Device::getResString(Res::Strings::CC_RECORDING_BUTTON_TITLE_RECODING), FONT_NAME_BOLD, RECORDING_RECORDING_TEXT_FONT_SIZE);
    textLabel->setColor(Color3B(255,255,255));
//    textLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    textLabel->setTag(TAG_RECORDING_ICON_TEXT);
    
    auto textWidth = textLabel->getTextWidth();
    
    auto leftPadding = RECORDING_RECORDING_TEXT_SIDE_PADDING;
    textLabel->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_LEFT);
    
    auto buttonTotalWidth = leftPadding + textWidth + RECORDING_RECORDING_TEXT_SIDE_PADDING;
    auto centerWidth = 0;
    
    if (buttonTotalWidth < (leftSprite->getContentSize().width + rightSprite->getContentSize().width)) {
        buttonTotalWidth = leftSprite->getContentSize().width + rightSprite->getContentSize().width;
    	textLabel->setPosition(cocos2d::Point((buttonTotalWidth-textWidth)/2, leftSprite->getContentSize().height/2));
    } else {
        centerWidth = buttonTotalWidth - leftSprite->getContentSize().width - rightSprite->getContentSize().width;
    	textLabel->setPosition(cocos2d::Point(leftPadding, leftSprite->getContentSize().height/2));
    }
    
    auto buttonSize = cocos2d::Size(buttonTotalWidth, leftSprite->getContentSize().height);
    rightSprite->setPosition(cocos2d::Point(buttonSize.width, 0));
    
    auto button = Node::create();
    
    if (centerWidth > 0) {
        auto centerSprite = Sprite::create("btn_set_record_recording_normal_center.png", cocos2d::Rect(0, 0, centerWidth, leftSprite->getContentSize().height));
        centerSprite->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        centerSprite->setPosition(cocos2d::Point(leftSprite->getContentSize().width, 0));
        button->addChild(centerSprite);
    }
    
    button->setContentSize(buttonSize);
    button->addChild(leftSprite);
    button->addChild(rightSprite);
    button->addChild(textLabel);
    
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    button->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
	button->setPosition(cocos2d::Point(visibleSize.width-RECORDING_RECORDING_ICON.x, RECORDING_RECORDING_ICON.y));
#else
    button->setAnchorPoint(cocos2d::Point::ANCHOR_TOP_RIGHT);
	button->setPosition(cocos2d::Point(visibleSize.width-RECORDING_RECORDING_ICON.x, visibleSize.height - RECORDING_RECORDING_ICON.y));
#endif
    
    float fadeDuration = 0.5f;
    auto fadeOutAnimation = FadeTo::create(fadeDuration, 255.0/2.0);
    auto fadeInAnimation = FadeTo::create(fadeDuration, 255.0);
    auto fadeSequence = Sequence::create(fadeOutAnimation, fadeInAnimation, NULL);
    auto repeatAnimation = RepeatForever::create(fadeSequence);
    
    textLabel->runAction(repeatAnimation);
    
    return button;
}

void CharacterRecording::saveCharacterProperty()
{
    auto childList = mainLayer->getChildren();
    auto childCount = childList.size();
    int localZOrder = CHARACTER_LAYER_START;
    for (int index = 0 ; index < childCount; index++) { // by z order
        auto child = childList.at(index);
        
        if (!child->isVisible()) continue;
        
        auto character = dynamic_cast<Character *>(child);
        if (character) {
            float posX = character->getPositionX();
            float posY = character->getPositionY();
            float angle = character->getRotation();
            float scale = character->getScale();
            CharacterDirection direction = character->getDirection();
            
            __String *charKey = __String::create(character->getCharKey());
            __Dictionary *propertyDic = __Dictionary::create();
            propertyDic->setObject(__Float::create(posX), "pos_x");
            propertyDic->setObject(__Float::create(posY), "pos_y");
            propertyDic->setObject(__Float::create(angle), "angle");
            propertyDic->setObject(__Float::create(scale), "scale");
            propertyDic->setObject(charKey, "char_key");
            propertyDic->setObject(__Integer::create((int)direction), "direction");
            propertyDic->setObject(__Integer::create((int)localZOrder), "z_order");
            
            localZOrder++;

            characterProperties->setObject(propertyDic, character->characterID);
        }
    }
}

void CharacterRecording::restoreCharacterProperty()
{
    for(int i = 0; i < characters->count(); i++){
        Character *character = (Character *)characters->getObjectAtIndex(i);
        __Dictionary *propertyDic = (__Dictionary *)characterProperties->objectForKey(character->characterID);
        float posX = ((__Float *)propertyDic->objectForKey("pos_x"))->getValue();
        float posY = ((__Float *)propertyDic->objectForKey("pos_y"))->getValue();
        float angle = ((__Float *)propertyDic->objectForKey("angle"))->getValue();
        float scale = ((__Float *)propertyDic->objectForKey("scale"))->getValue();
        int direction = ((__Integer *)propertyDic->objectForKey("direction"))->getValue();
        int localZOrder = ((__Integer *)propertyDic->objectForKey("z_order"))->getValue();

        character->clearTouch();
        character->setLocalZOrder(localZOrder);
        character->setPosition(posX, posY);
        character->setRotation(angle);
        character->setScale(scale);
        character->setDirection((CharacterDirection)direction);
    }
}

void CharacterRecording::saveRecordData(bool publish)
{
    auto ud = UserDefault::getInstance();

    if (this->bookKey) {
        auto bookJsonString = ud->getStringForKey(this->bookKey->getCString());
        if (bookJsonString != "") {
            rapidjson::Document bookDoc;
            bookDoc.Parse<0>(bookJsonString.c_str());
            
            // save last status
            rapidjson::Value characterLastDatas(rapidjson::kArrayType);
            rapidjson::Value charDic(rapidjson::kObjectType);
            
            auto childList = mainLayer->getChildren();
            auto childCount = childList.size();
            
            int localZOrder = 7;
            for (int index = childCount-1; index >= 0; index--) { // by z order
                auto child = childList.at(index);
                
                if (!child->isVisible()) continue;
                
                auto character = dynamic_cast<Character *>(child);
                
                if (character) {
                    float posX = character->getPositionX();
                    float posY = character->getPositionY();
                    float angle = character->getRotation();
                    float scale = character->getScale();
                    
                    int direction = (int)character->getDirection();
                    int zOrder = localZOrder;
                    
                    __String *charKey = __String::create(character->getCharKey());
                    
                    rapidjson::Value propertyDic(rapidjson::kObjectType);
                    propertyDic.AddMember("char_id", character->characterID, bookDoc.GetAllocator());
                    propertyDic.AddMember("char_key", charKey->getCString(), bookDoc.GetAllocator());
                    propertyDic.AddMember("pos_x", posX, bookDoc.GetAllocator());
                    propertyDic.AddMember("pos_y", posY, bookDoc.GetAllocator());
                    propertyDic.AddMember("angle", angle, bookDoc.GetAllocator());
                    propertyDic.AddMember("scale", scale, bookDoc.GetAllocator());
                    propertyDic.AddMember("direction", direction, bookDoc.GetAllocator());
                    propertyDic.AddMember("z_order", zOrder, bookDoc.GetAllocator());
                    
                    characterLastDatas.PushBack(propertyDic, bookDoc.GetAllocator());
                    localZOrder--;
                }
            }
            
            rapidjson::Value& chapters = bookDoc["chapters"];
            __String *chapterName = __String::createWithFormat("chapter%d", chapterIndex);
            rapidjson::Value& chapterData = chapters[chapterName->getCString()];
            if (chapterData.IsNull()) {
                chapterData.SetObject();
                chapterData.AddMember("background", selectedBackgroundKey->getCString(), bookDoc.GetAllocator());
                chapterData.AddMember("sound", selectedSoundKey->getCString(), bookDoc.GetAllocator());
                chapterData.AddMember("char_last_datas", characterLastDatas, bookDoc.GetAllocator());
            } else {
                chapterData["background"] = selectedBackgroundKey->getCString();
                chapterData["sound"] = selectedSoundKey->getCString();
                chapterData["char_last_datas"] = characterLastDatas;
            }
            
            if (publish) { // save status just before start recording
                // character property
                rapidjson::Value characterDatas(rapidjson::kArrayType);
                __Array *characterKeys = characterProperties->allKeys();
                rapidjson::Value charDic(rapidjson::kObjectType);
                
                if (characterKeys) {
                    for(int i = 0; i < characters->count(); i++){
                        auto character = (Character *)characters->getObjectAtIndex(i);
                        
                        rapidjson::Value propertyDic(rapidjson::kObjectType);
                        
                        __Dictionary *characterPropertyDic = (__Dictionary *)characterProperties->objectForKey(character->characterID);
                        
                        float posX = ((__Float *)characterPropertyDic->objectForKey("pos_x"))->getValue();
                        float posY = ((__Float *)characterPropertyDic->objectForKey("pos_y"))->getValue();
                        
                        float angle =((__Float *)characterPropertyDic->objectForKey("angle"))->getValue();
                        float scale = ((__Float *)characterPropertyDic->objectForKey("scale"))->getValue();
                        int direction = ((__Integer *)characterPropertyDic->objectForKey("direction"))->getValue();
                        int zOrder = ((__Integer *)characterPropertyDic->objectForKey("z_order"))->getValue();
                        
                        __String *charKey = (__String *)characterPropertyDic->objectForKey("char_key");
                        
                        propertyDic.AddMember("char_id", character->characterID, bookDoc.GetAllocator());
                        propertyDic.AddMember("char_key", charKey->getCString(), bookDoc.GetAllocator());
                        propertyDic.AddMember("pos_x", posX, bookDoc.GetAllocator());
                        propertyDic.AddMember("pos_y", posY, bookDoc.GetAllocator());
                        propertyDic.AddMember("angle", angle, bookDoc.GetAllocator());
                        propertyDic.AddMember("scale", scale, bookDoc.GetAllocator());
                        propertyDic.AddMember("direction", direction, bookDoc.GetAllocator());
                        propertyDic.AddMember("z_order", zOrder, bookDoc.GetAllocator());
                        
                        characterDatas.PushBack(propertyDic, bookDoc.GetAllocator());
                    }
                }
                chapterData.AddMember("char_datas", characterDatas, bookDoc.GetAllocator());
                
                // touch data
                rapidjson::Document timelineDocument;
                timelineDocument.SetObject();
                rapidjson::Document::AllocatorType& allocator = timelineDocument.GetAllocator();
                
                rapidjson::Value timelinesArray(rapidjson::kArrayType);
                
                for (int i = 0; i < touchData->count(); i++) {
                    rapidjson::Value touchInfoDic(rapidjson::kObjectType);
                    
                    __Dictionary *touchInfo = (__Dictionary *)touchData->getObjectAtIndex(i);
                    double timeStamp = ((__Double*)touchInfo->objectForKey("timestamp"))->getValue();
                    int touchID = ((__Integer*)touchInfo->objectForKey("touch_id"))->getValue();
                    int touchStatus = ((__Integer*)touchInfo->objectForKey("touch_status"))->getValue();
                    float locationX = ((__Float*)touchInfo->objectForKey("location_x"))->getValue();
                    float locationY = ((__Float*)touchInfo->objectForKey("location_y"))->getValue();
                    
                    touchInfoDic.AddMember("timestamp", timeStamp, allocator);
                    touchInfoDic.AddMember("touch_id", touchID, allocator);
                    touchInfoDic.AddMember("touch_status", touchStatus, allocator);
                    touchInfoDic.AddMember("location_x", locationX, allocator);
                    touchInfoDic.AddMember("location_y", locationY, allocator);
                    
                    timelinesArray.PushBack(touchInfoDic, allocator);
                }
                
                timelineDocument.AddMember("timelines", timelinesArray, allocator);
                
                rapidjson::StringBuffer strbuf;
                rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
                timelineDocument.Accept(writer);
                auto timelineJsonString = strbuf.GetString();
                
                unsigned long int timestamp= time(NULL);
                auto timelineKey = __String::createWithFormat("timeline_%ld", timestamp);
                ud->setStringForKey(timelineKey->getCString(), timelineJsonString);
                chapterData.AddMember("timeline_file", timelineKey->getCString(), bookDoc.GetAllocator());
                
                //voice
                chapterData.AddMember("voice_file", voiceFileName->getCString(), bookDoc.GetAllocator());
            }
            
            rapidjson::StringBuffer strbuf;
            rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
            bookDoc.Accept(writer);
            auto resultJsonString = strbuf.GetString();
            
            ud->setStringForKey(this->bookKey->getCString(), resultJsonString);
            ud->flush();
            
            // JP Added
            rapidjson::Value& chapData = bookDoc["chapters"];
            rapidjson::Value& chapter1Data = chapData["chapter1"];
            rapidjson::Value& chapter2Data = chapData["chapter2"];
            rapidjson::Value& chapter3Data = chapData["chapter3"];
            rapidjson::Value& chapter4Data = chapData["chapter4"];
            rapidjson::Value& chapter5Data = chapData["chapter5"];
            
            bool chapter1Exist = !chapter1Data.IsNull() && chapter1Data.HasMember("char_datas");
            bool chapter2Exist = !chapter2Data.IsNull() && chapter2Data.HasMember("char_datas");
            bool chapter3Exist = !chapter3Data.IsNull() && chapter3Data.HasMember("char_datas");
            bool chapter4Exist = !chapter4Data.IsNull() && chapter4Data.HasMember("char_datas");
            bool chapter5Exist = !chapter5Data.IsNull() && chapter5Data.HasMember("char_datas");
            
            if (chapter1Exist && chapter2Exist && chapter3Exist && chapter4Exist && chapter5Exist) {
                AppDelegate* app = (AppDelegate*)Application::getInstance();
                if(app != nullptr)
                {
                    app->mixpanelTrack("Successfully Completed Custom Storybook", "{\"Storybook\": \"Custom\"}");
                }
            }
            // End JP Addded
            
            cocos2d::log("data saved : %s", this->bookKey->getCString());
        }
    }
}

void CharacterRecording::finishPlaying()
{
    playTouchTimeStampIndex = 0;
    playTouchElapsedTime = 0;
    playedTime = 0;
    
    playing = false;
    paused = false;
    
    if (bgmFileName != nullptr)
        CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    
    this->setProgress(0.0);
    this->setViewMode(RecordingViewMode::RecordingComplete);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	AppDelegate* app = (AppDelegate*)Application::getInstance();
	if(app != nullptr) {
		app->changeVoicePlayStatus(ANDROID_PLAY_STOP, voiceFileName->getCString(), TAG_RECORDING);
	}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
    VoiceRecorder *voiceRecorder = [VoiceRecorder sharedRecorder];
    [voiceRecorder stop];
#endif
#endif
}

void CharacterRecording::markLastPseudoTouch()
{
    __Dictionary *touchInfo = __Dictionary::create();
    touchInfo->setObject(__Float::create(-1), "location_x");
    touchInfo->setObject(__Float::create(-1), "location_y");
    touchInfo->setObject(__Integer::create(0), "touch_id");
    touchInfo->setObject(__Integer::create((int)TouchStatus::Cancel), "touch_status");
    touchInfo->setObject(__Double::create(MAX_TIME_DURATION), "timestamp");
    
    touchData->addObject(touchInfo);
    
    log("reached to end");
}

void CharacterRecording::finishRecording()
{
    recording = false;

    for(int i = 0; i < characters->count(); i++){
        Character *character = (Character *)characters->getObjectAtIndex(i);
        character->clearTouch();
    }
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    AppDelegate* app = (AppDelegate*)Application::getInstance();
    if(app != nullptr) {
        app->changeVoiceRecordStatus(ANDROID_RECORD_STOP, nullptr, -1);
    }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
    float volume = UserDefault::getInstance()->getFloatForKey(UDKEY_FLOAT_VOLUME);
    CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(volume);
    
    VoiceRecorder *voiceRecorder = [VoiceRecorder sharedRecorder];
    [voiceRecorder stopRecord];
#endif
#endif
    // clear remaining touches
    touchNumber = 0;
    touches.clear();
    
    this->setProgress(1.0);
    this->setViewMode(RecordingViewMode::RecordingComplete);
}

void CharacterRecording::play()
{    
    if (bgmFileName != nullptr) {
        CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(bgmFileName->getCString());
    }
    this->setViewMode(RecordingViewMode::Playing);
    this->restoreCharacterProperty();
    playing = true;
    paused = false;
    
    playedTime = 0;
    this->playTouch();
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	AppDelegate* app = (AppDelegate*)Application::getInstance();
	if(app != nullptr) {
		app->changeVoicePlayStatus(ANDROID_PLAY_START, voiceFileName->getCString(), TAG_RECORDING);
	}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
    float volume = UserDefault::getInstance()->getFloatForKey(UDKEY_FLOAT_VOLUME);
    if (volume > 0.0f && voiceFileName){
        NSString *fileName = [NSString stringWithUTF8String:voiceFileName->getCString()];
        VoiceRecorder *voiceRecorder = [VoiceRecorder sharedRecorder];
        [voiceRecorder play:fileName];
    }
#endif
#endif
}

void CharacterRecording::pausePlay()
{
    playing = false;
    paused = true;
 
    if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()) {
        CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    }
    
    for (int index = 0; index < characters->count(); index++) {
        auto character = (Character *)characters->getObjectAtIndex(index);
        character->pause();
    }
    
    playMenuItem->setVisible(true);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	AppDelegate* app = (AppDelegate*)Application::getInstance();
	if(app != nullptr) {
		app->changeVoicePlayStatus(ANDROID_PLAY_PAUSE, voiceFileName->getCString(), TAG_RECORDING);
	}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
    VoiceRecorder *voiceRecorder = [VoiceRecorder sharedRecorder];
    [voiceRecorder pause];
#endif
#endif
}

void CharacterRecording::resumePlay()
{
    if (bgmFileName != nullptr)
        CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
    
    playing = true;
    paused = false;
    
    for (int index = 0; index < characters->count(); index++) {
        auto character = (Character *)characters->getObjectAtIndex(index);
        character->resume();
    }
    
    playMenuItem->setVisible(false);
     
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	AppDelegate* app = (AppDelegate*)Application::getInstance();
	if(app != nullptr) {
		app->changeVoicePlayStatus(ANDROID_PLAY_RESUME, voiceFileName->getCString(), TAG_RECORDING);
	}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
     VoiceRecorder *voiceRecorder = [VoiceRecorder sharedRecorder];
     [voiceRecorder resume];
#endif
#endif
}

void CharacterRecording::playMenuCallback(Ref* pSender)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Tap.wav", false);
    
    if (paused) {
        this->resumePlay();
    } else {
        this->play();
    }
    playMenuItem->setVisible(false);
}

void CharacterRecording::recordingMenuCallback(Ref* sender)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Tap.wav", false);

    if (this->viewMode == RecordingViewMode::SetPosition) {
        
        AppDelegate* app = (AppDelegate*)Application::getInstance();
        if(app != nullptr)
        {
            app->mixpanelTrack("Pressed Record Button", NULL);
        }
        
        this->saveCharacterProperty();
        this->clearRecordData();
        this->setViewMode(RecordingViewMode::Recording);
    }
}

void CharacterRecording::update(float delta)
{
    switch (viewMode) {
        case RecordingViewMode::SetPosition:
        {

        }
            break;
        case RecordingViewMode::Recording:
        {
            if (recording) {
                recordedTime += delta;
                float currentProgress = recordedTime / (((float)MAX_TIME_DURATION)/1000);
                
                if (currentProgress > 1.0f) {
                    this->markLastPseudoTouch();
                    this->finishRecording();
                } else {
                    this->setProgress(currentProgress);
                }
            }
        }
            break;
        case RecordingViewMode::Playing:
        {
            if (playing) {
                playedTime += delta;
                float currentProgress = playedTime / (((float)MAX_TIME_DURATION)/1000);
                this->setProgress(currentProgress);
            }
        }
            break;
        case RecordingViewMode::RecordingComplete:
        {

        }
            break;
        default:
            break;
    }
}

bool CharacterRecording::onTouchBegan(cocos2d::Touch* touch, cocos2d::Event *event)
{
    if (this->isScrollViewVisible()){
        auto closingArea = mainLayer->getContentSize().height - RECORDING_BOTTOM_SCROLL_VIEW_HEIGHT;
        auto touchPoint = touch->getLocationInView();
        
        log("touch at : %f, area : %f", touchPoint.y, closingArea);
        if (touchPoint.y <= closingArea && !scrollViewToggleAnimation){
            return true;
        }
        return false;
    }
    
    if (viewMode == RecordingViewMode::RecordingComplete) {
        return false;
    } else if (viewMode == RecordingViewMode::Playing){
        if (event != NULL) { // real touch
            if (playing) {
                this->pausePlay();
//            } else {
//                this->resumePlay();
            }
            return false;
        }
    } else if (viewMode == RecordingViewMode::Recording) {
        this->recordTouch(touch, TouchStatus::Down);
        if(recording) {
            this->showRecordingIcon(true);
        }
    }
    
    auto childList = mainLayer->getChildren();
    auto childCount = childList.size();
    
    for (int index = childCount-1; index >= 0; index--) { // by z order
        auto child = childList.at(index);
        
        if (!child->isVisible()) continue;
        
        auto characterNode = dynamic_cast<Character *>(child);
        
        if (characterNode) {
            cocos2d::Point locationInNode = child->convertToNodeSpace(touch->getLocation());

//            clock_t start = clock();
//            bool result = characterNode->pixelMaskContainsPointInChild(characterNode, locationInNode); // TODO: implements this func
            bool result = characterNode->getBoundingBox().containsPoint(touch->getLocation()); // temp code

//            clock_t end = clock();
//            int elapsed = double(end - start) / CLOCKS_PER_SEC * 1000;
//            log("time elpased total check : %dms", elapsed);
            
            if (result) {
                touchNumber++;
                touches.pushBack(touch);
                this->bringToFront(characterNode);
                cocos2d::log("sprite onTouchBegan... x = %f, y = %f", locationInNode.x, locationInNode.y);

                currentSelectedCharacter = characterNode;

                int touchID = touch->getID();
                touchTargetMapping->setObject(characterNode, touchID);
                
                characterNode->onTouchBegan(touch, event);
                
                if (viewMode == RecordingViewMode::Recording && (prevCharacter != currentSelectedCharacter) ) {
                    prevCharacter = characterNode;
                    this->addMarkToProgress(characterNode);
                }

                if (viewMode == RecordingViewMode::SetPosition) {
                    backMenu->setVisible(false);
                    bottomMenu->setVisible(false);
                    recordingMenu->setVisible(false);
                    trashItem->setVisible(true);
                }
                return true;
            }
        }
    }
    
    if (viewMode == RecordingViewMode::Recording && (prevCharacter != NULL || progressMarkHolder->getChildrenCount() == 0) ) {
        prevCharacter = NULL;
        this->addMarkToProgress(NULL);
    }
    
    return true;
}

void CharacterRecording::onTouchMoved(cocos2d::Touch* touch, cocos2d::Event *event)
{
    if (isBackgroundScrollViewVisible || viewMode == RecordingViewMode::RecordingComplete ) { //|| viewMode == RecordingViewMode::Playing ) {
        // no need to move a character
        return;
    }
    
    if (viewMode == RecordingViewMode::Recording) {
    	if(recording) {
            this->recordTouch(touch, TouchStatus::Move);
        }
    }
    
    int touchID = touch->getID();
    Ref* target = touchTargetMapping->objectForKey(touchID);
    if (target != NULL) {
        Character *character = (Character*)target;
        character->onTouchMoved(touch, event);
        
        if (viewMode == RecordingViewMode::SetPosition) {
            cocos2d::Rect charRect = character->getBoundingBox();
            auto centerPoint = cocos2d::Point(charRect.origin.x + charRect.size.width/2, charRect.origin.y + charRect.size.height/2);
            
            auto trashNormal = trashItem->getChildByTag(TAG_TRASH_NORMAL);
            auto trashLeftOpen = trashItem->getChildByTag(TAG_TRASH_LEFT_OPEN);
            auto trashRightOpen = trashItem->getChildByTag(TAG_TRASH_RIGHT_OPEN);
            auto trashBg = trashItem->getChildByTag(TAG_TRASH_BG);
            
            if (trashItem->getBoundingBox().containsPoint(centerPoint) || removeArea->getBoundingBox().containsPoint(centerPoint) ){

                cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
                
                trashNormal->setVisible(false);
                trashBg->setVisible(true);
                
                if (centerPoint.x < visibleSize.width/2) { // open left
                    trashLeftOpen->setVisible(true);
                    trashRightOpen->setVisible(false);
                } else { // open right
                    trashLeftOpen->setVisible(false);
                    trashRightOpen->setVisible(true);
                }
                this->showDimLayer(true);
            } else {
                trashNormal->setVisible(true);
                trashLeftOpen->setVisible(false);
                trashRightOpen->setVisible(false);
                trashBg->setVisible(false);

                this->showDimLayer(false);
            }
        }
    }
}

void CharacterRecording::onTouchEnded(cocos2d::Touch* touch, cocos2d::Event *event)
{
    if (this->isScrollViewVisible()){

        if (isBackgroundScrollViewVisible || isCharacterScrollViewVisible || isSoundScrollViewVisible) {
            hideBottomScrollView();
        }
        
        if (openCharacterItem) {
            MenuItemImage* characterItem = (MenuItemImage*)bottomMenu->getChildByTag(MENU_ITEM);
            characterItem->setNormalImage(Sprite::create("btn_home_item_normal.png"));
            characterItem->setEnabled(true);
            openCharacterItem = false;
        } else if (openBgItem) {
            MenuItemImage* bgItem = (MenuItemImage*)bottomMenu->getChildByTag(MENU_BG);
            bgItem->setNormalImage(Sprite::create("btn_home_bg_normal.png"));
            bgItem->setEnabled(true);
            openBgItem = false;
        } else if (openSoundItem) {
            MenuItemImage* soundItem = (MenuItemImage*)bottomMenu->getChildByTag(MENU_MUSIC);
            soundItem->setNormalImage(Sprite::create("btn_set_position_music_normal.png"));
            soundItem->setEnabled(true);
            openSoundItem = false;
        }
        return;
    }
    
    if (viewMode == RecordingViewMode::Recording) {
    	if(recording) {
        this->recordTouch(touch, TouchStatus::Up);
    }
    }
    
    int touchID = touch->getID();
    Ref* target = touchTargetMapping->objectForKey(touchID);
    if (target != NULL) {
        Character *character = (Character*)target;
        character->onTouchEnded(touch, event);
        touchTargetMapping->removeObjectForKey(touchID);
        
        if (viewMode == RecordingViewMode::Recording && event != NULL && character->getPressStatus() == CharacterPressStatus::Tap) { // real touch
            // trigger pseudo touch for guarantee tap animation time
            Touch *pseudoTouch = new Touch();
            pseudoTouch->autorelease();
            cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
            pseudoTouch->setTouchInfo(-1, -visibleSize.width, -visibleSize.height);
            this->recordTouch(pseudoTouch, TouchStatus::Down);
            DelayTime *delayTime = DelayTime::create(TAP_ANIMATION_DURATION_IN_SEC);
            auto touchUpTirrger = CallFunc::create([&](){
                Touch *pseudoTouch = new Touch();
                pseudoTouch->autorelease();
                cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
                pseudoTouch->setTouchInfo(-1, -visibleSize.width, -visibleSize.height);
                this->recordTouch(pseudoTouch, TouchStatus::Up);
            });
            auto touchUpAction = Sequence::create(delayTime, touchUpTirrger, NULL);
            this->runAction(touchUpAction);
        }
        
        if (viewMode == RecordingViewMode::SetPosition) {
            cocos2d::Rect charRect = character->getBoundingBox();
            auto centerPoint = cocos2d::Point(charRect.origin.x + charRect.size.width/2, charRect.origin.y + charRect.size.height/2);
            if (trashItem->getBoundingBox().containsPoint(centerPoint) || removeArea->getBoundingBox().containsPoint(centerPoint) ){
                character->clearTouch();
                
                std::string charKey = character->getCharKey();

                characterProperties->removeObjectForKey(character->characterID);
                
                auto trashNormal = trashItem->getChildByTag(TAG_TRASH_NORMAL);
                auto trashLeftOpen = trashItem->getChildByTag(TAG_TRASH_LEFT_OPEN);
                auto trashRightOpen = trashItem->getChildByTag(TAG_TRASH_RIGHT_OPEN);
                auto trashBg = trashItem->getChildByTag(TAG_TRASH_BG);
                
                trashNormal->setVisible(true);
                trashLeftOpen->setVisible(false);
                trashRightOpen->setVisible(false);
                trashBg->setVisible(false);
                
                if (character == selectedLockedCharacter) {
                    selectedLockedCharacter = nullptr;
                }
                
                this->showDimLayer(false);
                characters->removeObject(character);
                character->removeFromParentAndCleanup(true);
            }
        }
    }
    
    touchNumber--;
    touches.eraseObject(touch);
    
    if (touches.size() == 0) {
        if (viewMode == RecordingViewMode::SetPosition) {
            backMenu->setVisible(true);
            bottomMenu->setVisible(true);
            recordingMenu->setVisible(true);
            trashItem->setVisible(false);
            this->showDimLayer(false);
        } else if (viewMode == RecordingViewMode::Recording) {
            this->showRecordingIcon(false);
        }
        
        touchTargetMapping->removeAllObjects();
        currentSelectedCharacter = NULL;
    }

    log("sprite onTouchesEnded...");
}

void CharacterRecording::onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event  *event)
{
    if (viewMode == RecordingViewMode::Recording) {
        this->recordTouch(touch, TouchStatus::Cancel);
    }
    
    int touchID = touch->getID();
    Ref* target = touchTargetMapping->objectForKey(touchID);
    if (target != NULL) {
        Character *character = (Character*)target;
        character->onTouchCancelled(touch, event);
    }
    
    touchNumber--;
    touches.eraseObject(touch);
    
    if (touches.size() == 0) {
        if (viewMode == RecordingViewMode::SetPosition) {
            backMenu->setVisible(true);
            bottomMenu->setVisible(true);
            recordingMenu->setVisible(true);
            trashItem->setVisible(false);
            this->showDimLayer(false);
        } else if (viewMode == RecordingViewMode::Recording) {
			if(recording) {
            this->showRecordingIcon(false);
        }
		}
        
        touchTargetMapping->removeAllObjects();
        currentSelectedCharacter = NULL;
    }
    
    log("sprite onTouchCancelled...");
}

void CharacterRecording::createButtonClicked(Ref* obj, ui::TouchEventType type)
{
    switch (type) {
        case ui::TOUCH_EVENT_ENDED: {
            log("create bg clicked");
            if (cameraScene) return;
            
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
            AppDelegate* app = (AppDelegate*)Application::getInstance();
            if(!isPlatformShown) {
                if(openBgItem) {
			app->showPlatformView(ANDROID_TAKE_PICTURE, TAG_RECORDING);
                } else {
			app->showPlatformView(ANDROID_CREATE_CHARACTER, TAG_RECORDING);
                }
            }
            isPlatformShown = true;
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#ifndef MACOS
            auto director = Director::getInstance();
            if (openBgItem) {
                cameraScene = CustomCamera::createWithViewType(camera_type_bg);
            } else {
                cameraScene = CustomCamera::createWithViewType(camera_type_character);
            }
            CustomCamera *cameraLayer = (CustomCamera*)cameraScene->getChildByTag(TAG_CAMERA);
            cameraLayer->setDelegate(this);
            director->pushScene(cameraScene);
#endif
#endif
        } break;
        case ui::TOUCH_EVENT_BEGAN: break;
        case ui::TOUCH_EVENT_MOVED: break;
        case ui::TOUCH_EVENT_CANCELED: break;
        default: break;
    }
}

void CharacterRecording::bringToFront(Sprite* pSender)
{
    if(touchNumber > 1) return;
    
    for (int index = 0; index < characters->count(); index++) {
        Character *character = (Character *)characters->getObjectAtIndex(index);
        character->setLocalZOrder(CHARACTER_LAYER_START);
    }

    pSender->setLocalZOrder(CHARACTER_LAYER_START+1);
}

void CharacterRecording::scrollViewDidScroll(ScrollView *view)
{
    int tag = view->getTag();
    if (tag == TAG_BACKGROUND_SCROLL_VIEW){
        auto oldOffset = backgroundViewScrollOffset.x;
        
        if ( !view->isDragging()){ // scroll did end
            cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
            auto endOffset = visibleSize.width - view->getContentSize().width;

            if ( !((oldOffset == 0 && view->getContentOffset().x == 0) || (oldOffset == endOffset && view->getContentOffset().x == endOffset)) ){
                auto frameCount = 10;
                float rotationDuration = 0.1f;
                int direction = 0;
                
                auto container = view->getChildren().at(0);
                for (int index = 0; index < frameCount ; index++) {
                    Sprite *frame = (Sprite *)(container->getChildByTag(FRAME_BASE_TAG+index));
                    auto frameBg = (Sprite *)frame->getChildByTag(TAG_FRAME_BG);
                    changePositionAtAnchor(frameBg, cocos2d::Point(0.5, FRAME_ANCHOR_Y) );

                    if((oldOffset-view->getContentOffset().x) < 0) {
                        direction = -1;
                    } else {
                        direction = 1;
                    }
                    
                    float rotationAngle = direction * 5.0f;
                    
                    auto rotateAction = RotateTo::create(rotationDuration, rotationAngle);
                    auto rotateReverseAction = RotateTo::create(rotationDuration, 0);
                    auto frameSequence = Sequence::create(rotateAction, rotateReverseAction, NULL);
                    
                    if (frame == NULL) continue;
                    frameBg->runAction(frameSequence->clone());
                }
            }
            backgroundViewScrollOffset = view->getContentOffset();
        }
    } else {}
}

void CharacterRecording::scrollViewDidZoom(ScrollView *view)
{
    log("scrollViewDidZoom");
}

void CharacterRecording::addCharacterToScreen(__String* charKey)
{
    auto ud = UserDefault::getInstance();
    auto charString = ud->getStringForKey(charKey->getCString());
    rapidjson::Document characterInfo;
    characterInfo.Parse<0>(charString.c_str());
    if (!characterInfo.HasMember("ccbi") && !characterInfo.HasMember("base_char")){
        log("error - no ccbi ! %s",charKey->getCString());
        return;
    }
    
    Character* newCharacter = nullptr;
    
    if (charKey) {
        std::chrono::milliseconds before = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch());
        
        newCharacter = Character::create(charKey->getCString());
        newCharacter->setPosition(mainLayer->getContentSize().width/2,mainLayer->getContentSize().height/2);
        float defaultScale = Util::getCharacterDefulatScale();
        newCharacter->lockInScreen();
        newCharacter->setScale(defaultScale);
        newCharacter->characterID = localCharacterId;
        localCharacterId++;
        newCharacter->setDirection(CharacterDirection::Front);
        mainLayer->addChild(newCharacter, CHARACTER_LAYER_START);
        characters->addObject(newCharacter);
        bringToFront(newCharacter);
        
        std::chrono::milliseconds after = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch());
        log("character loading time : %lld ms", (after-before).count());
    }
    

    if (!characterInfo.HasMember("base_char")) { // not custom character
        bool isLock = characterInfo["locked"].GetBool();
        
        if (isLock) {
            if (newCharacter != nullptr) {
                removeSelectedLockCharacter();
                
                selectedLockedCharacter = newCharacter;
                
                int expiredDay = 0;

                if (characterInfo.HasMember("expired_hour")) {
                    auto expiredMin = characterInfo["expired_hour"].GetInt();
                    expiredDay = expiredMin/60/24;
                }
                
                showUnlockTalkBox(UnlockItemType::ItemTypeCharacter, expiredDay);
            }
        } else {
            removeSelectedLockCharacter();
            bool free = characterInfo["free"].GetBool();
            if (!free) {
                if (characterInfo.HasMember("expiration_date")) { // expiration_date = registered date
                    if (characterInfo.HasMember("expired_hour")) {
                        const char *registerTimeChar = characterInfo["expiration_date"].GetString();
                        auto expiredMin = characterInfo["expired_hour"].GetInt();

                        auto leftTime = Util::getItemLeftTime(registerTimeChar,expiredMin);
                        if(leftTime < 0) {
                            int expiredDay = expiredMin/60/24;
                            showUnlockTalkBox(UnlockItemType::ItemTypeCharacter, expiredDay);
                            characterScrollView->reloadData();
                        }
                    }
                }
            }
            
            AppDelegate* app = (AppDelegate*)Application::getInstance();
            if(app != nullptr)
            {
                char dictStr[128];
                sprintf(dictStr, "{\"Character\":\"%s\"}", charKey->getCString());
                app->mixpanelTrack("Used Character in Story", dictStr);
            }
        }
    } else {
        removeSelectedLockCharacter();
        
        AppDelegate* app = (AppDelegate*)Application::getInstance();
        if(app != nullptr)
        {
            char dictStr[128];
            sprintf(dictStr, "{\"Character\":\"%s\"}", "Custom");
            app->mixpanelTrack("Used Character in Story", dictStr);
        }
    }
}

void CharacterRecording::removeSelectedLockBackground()
{
    if (selectedLockedBackgroundKey != nullptr) {
        
        selectedLockedBackgroundKey = nullptr;
        if (lastUnlockBackgroundKey != nullptr) {
            std::string key = lastUnlockBackgroundKey->getCString();
            int backgroundIndex = backgroundScrollView->getBackgroundIndex(key);
            backgroundScrollView->setSelectBackground(backgroundIndex);
            this->changeBackground(lastUnlockBackgroundKey);
        }
        
        if (unlockTalkBox != nullptr) {
            unlockTalkBox->removeFromParent();
            unlockTalkBox = nullptr;
        }
    }
}

void CharacterRecording::removeSelectedLockCharacter()
{
    if (selectedLockedCharacter != nullptr) {
        characters->removeObject(selectedLockedCharacter);
        selectedLockedCharacter->removeFromParent();
        selectedLockedCharacter = nullptr;
        
        if (unlockTalkBox != nullptr) {
            unlockTalkBox->removeFromParent();
            unlockTalkBox = nullptr;
        }
    }
}

void CharacterRecording::btnBackCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
//	AppDelegate* app = (AppDelegate*)Application::getInstance();
//	if(app != nullptr) {
//		const char* bgmFileName = nullptr;
//		auto soundString = UserDefault::getInstance()->getStringForKey(selectedSoundKey->getCString());
//		rapidjson::Document soundInfo;
//		soundInfo.Parse<0>(soundString.c_str());
//		if (soundInfo.HasMember("sound_filename")) {
//			bgmFileName = soundInfo["sound_filename"].GetString();
//		}
//		app->changeVoiceRecordStatus(ANDROID_RECORD_SAVE, bgmFileName, -1);
//	}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
    VoiceRecorder *voiceRecorder = [VoiceRecorder sharedRecorder];
    [voiceRecorder stopRecord];
    
    float volume = UserDefault::getInstance()->getFloatForKey(UDKEY_FLOAT_VOLUME);
    CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(volume);
#endif
#endif
    
    if (pSender != nullptr) {
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Tap.wav", false);
    }
    
    switch (viewMode) {
        case RecordingViewMode::SetPosition:
        {
            removeSelectedLockCharacter();
            removeSelectedLockBackground();
            
            CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
            
            this->clearVoiceRecorder();
//            this->removeAllChildren();
            this->saveRecordData(false);
            auto director = Director::getInstance();
            director->popSceneWithTransition([](Scene *scene) {
                return TransitionProgressOutIn::create(0.4f, scene);
            });
        }
            break;
        case RecordingViewMode::Recording:
        case RecordingViewMode::RecordingComplete:
        {
            if (touchData->count() > 0) {
                Director::getInstance()->pause();
                PopupLayer *modal;
                if (Application::getInstance()->getCurrentLanguage() != LanguageType::JAPANESE) {
                    modal = PopupLayer::create(POPUP_TYPE_WARNING,
                                               Device::getResString(Res::Strings::CC_RECORDING_POPUP_WARNING_WITHOUT_SAVING),//"Do you want to leave without saving? All recorded contents will disappear.",
                                               Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL),//"Cancel",
                                               Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK));//"OK");
                } else {
                    modal = PopupLayer::create(POPUP_TYPE_WARNING,
                                               Device::getResString(Res::Strings::CC_RECORDING_POPUP_WARNING_WITHOUT_SAVING),//"Do you want to leave without saving? All recorded contents will disappear.",
                                               Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_NO),//"Cancel",
                                               Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_YES));//"OK");
                }
                modal->setTag(TAG_POPUP_BACK_TO_SET_POSITION);
                modal->setDelegate(this);
                this->addChild(modal, 1000);
            } else {
                this->deleteVoiceFile();
                this->setViewMode(RecordingViewMode::SetPosition);
            }
        }
            break;
        case RecordingViewMode::Playing:
        {
//            if(playing){
                this->finishPlaying();
                this->restoreCharacterProperty();
//            }
        }
            break;
        default:
            break;
    }
}

void CharacterRecording::deleteVoiceFile()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	AppDelegate* app = (AppDelegate*)Application::getInstance();
	if(app != nullptr) {
		app->changeVoiceRecordStatus(ANDROID_RECORD_DELETE, voiceFileName->getCString(), TAG_RECORDING);
	}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
    VoiceRecorder *voiceRecorder = [VoiceRecorder sharedRecorder];
    [voiceRecorder deleteRecord];
#endif
#endif
}

void CharacterRecording::clearVoiceRecorder()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
    VoiceRecorder *voiceRecorder = [VoiceRecorder sharedRecorder];
    [voiceRecorder clear];
#endif
#endif
}

bool CharacterRecording::isScrollViewVisible()
{
    float positionY = mainLayer->getPosition().y;
    float scrollViewHeight = RECORDING_BOTTOM_SCROLL_AREA_HEIGHT;
    if (positionY < scrollViewHeight) {
        return false;
    } else {
        return true;
    }
}

void CharacterRecording::onSelectCharacter(CharacterScrollView* view, Node* selectedItem, __String* characterKey, int index)
{
    if(unlockTalkBox){
        unlockTalkBox->removeFromParentAndCleanup(true);
        unlockTalkBox = nullptr;
    }
    
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Tap.wav", false);
    
    if (characters->count() == MAX_CHARACTER_NUM) {
        auto toastLayer = ToastLayer::create(Device::getResString(Res::Strings::CC_SETPOSITION_POPUP_WARNING_ITEM_FULL), 2);
        toastLayer->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        toastLayer->setPosition(cocos2d::Point::ZERO);
        this->addChild(toastLayer);
        log("Could not add more character!");
        return;
    }
    
    this->addCharacterToScreen(characterKey);
}

void CharacterRecording::onSelectBackground(BackgroundScrollView* view, Node* selectedItem, __String* backgroundKey, int index)
{
    if(unlockTalkBox){
        unlockTalkBox->removeFromParentAndCleanup(true);
        unlockTalkBox = nullptr;
    }
    
//    Sprite *frame = (Sprite *)selectedItem;
//    auto index = frame->getTag() % FRAME_BASE_TAG;
    log("frame pressed : %d", index);
    
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Tap.wav", false);

    this->changeBackground(backgroundKey);
//    backgroundScrollView->setSelectBackground(index);
    
}

void CharacterRecording::onSelectSound(SoundScrollView* view, Node* selectedItem, __String* soundKey, int index)
{
    log("frame pressed : %d", index);
    
    this->changeSound(soundKey);
}

void CharacterRecording::changeSound(__String *soundKey)
{
    log("changeSound -recordingScene");
    __String *cloneKey = soundKey->clone();
    cloneKey->retain();
    selectedSoundKey = cloneKey;
    
    if (bgmFileName != nullptr){
        bgmFileName->release();
        bgmFileName = nullptr;
    }
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();

    auto ud = UserDefault::getInstance();
    auto soundString = ud->getStringForKey(cloneKey->getCString());
    rapidjson::Document soundInfo;
    soundInfo.Parse<0>(soundString.c_str());
    if (soundInfo.HasMember("sound_filename")) {
        auto soundFile = soundInfo["sound_filename"].GetString();
        if (soundFile) {
            bgmFileName = __String::create(soundFile);
            bgmFileName->retain();
        }
    }
    
    if (isSoundScrollViewVisible && bgmFileName != nullptr) {
        CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(bgmFileName->getCString());
        
        AppDelegate* app = (AppDelegate*)Application::getInstance();
        if(app != nullptr)
        {
            char dictStr[128];
            sprintf(dictStr, "{\"Music\":\"%s\"}", bgmFileName->getCString());
            app->mixpanelTrack("Used Music in Story", dictStr);
        }
        
    } else {
        // Sound: "None"
        AppDelegate* app = (AppDelegate*)Application::getInstance();
        if(app != nullptr)
        {
            char dictStr[128];
            sprintf(dictStr, "{\"Music\":\"%s\"}", "None");
            app->mixpanelTrack("Used Music in Story", dictStr);
        }
    }
}

void CharacterRecording::changeBackground(__String *backgroundKey)
{
    __String *cloneKey = backgroundKey->clone();
    cloneKey->retain();
    
    selectedBackgroundKey = cloneKey;
    auto ud = UserDefault::getInstance();
    auto backgroundString = ud->getStringForKey(selectedBackgroundKey->getCString());
    rapidjson::Document backgroundInfo;
    backgroundInfo.Parse<0>(backgroundString.c_str());
    auto isCustom = backgroundInfo["custom"].GetBool();
    
    const char *selectFileName = nullptr;
    __String *bgPhotoImage = nullptr;
    if (isCustom) {
        auto bgPhoto = (char *)backgroundInfo["main_photo_file"].GetString();
        bgPhotoImage = __String::create(bgPhoto);
        auto borderBg = "home_background_select_custom.png";
        selectFileName = borderBg;
    } else {
        selectFileName = backgroundInfo["select"].GetString();
    }

    if (selectedFrame != NULL) {
        selectedFrame->removeFromParent();
    }
    
    selectedFrame = Sprite::create(selectFileName);
    
    auto scaleFactor = Util::getCharacterScaleFactor();

    auto hasAni = backgroundInfo.HasMember("ani_ccbi");
    if (hasAni) {
        auto bgID = backgroundInfo["bg_id"].GetInt();
        cocos2d::Point aniItemPos;
        auto aniCcbi = backgroundInfo["ani_ccbi"].GetString();
        
        switch (bgID) {
            case 1:// B001
                aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI01;
                break;
            case 2:// B002
                aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI02;
                break;
            case 4:// B004
                aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI04;
                break;
            case 6:// B006
                aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI06;
                break;
//            case 7:// B007
//                aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI07;
//                break;
            case 8:// B008
                aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI08;
                break;
            case 9:// B009
                aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI09;
                break;
//            case 10:// B010
//                aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI10;
//                break;
            case 11:// B011
                aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI11;
                break;
            case 12:// B012
                aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI12;
                break;
            default:
                break;
        }

        cocosbuilder::NodeLoaderLibrary *nodeLoaderLibrary = cocosbuilder::NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
        cocosbuilder::CCBReader *ccbReader = new cocosbuilder::CCBReader(nodeLoaderLibrary);
        Node *ccbiObj = ccbReader->readNodeGraphFromFile(aniCcbi, this);
        auto ccbiSprite = (Sprite*)ccbiObj;
        ccbiSprite->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        
        ccbiSprite->setPosition(cocos2d::Point(aniItemPos.x/scaleFactor, aniItemPos.y/scaleFactor));
        ccbReader->release();
        
        if (ccbiSprite) {
            selectedFrame->addChild(ccbiSprite);
        }
    }

    selectedFrame->setScale(scaleFactor);
    
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    auto frameSize = selectedFrame->getContentSize() * Util::getCharacterScaleFactor();
    
    selectedFrame->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    selectedFrame->setPosition((visibleSize.width - frameSize.width)/2, (visibleSize.height-frameSize.height)/2);
    mainLayer->addChild(selectedFrame, 1);
    
    if (bgPhotoImage != nullptr) {
        std::string photoType("bg");
        std::string path = Util::getMainPhotoPath(photoType);
        path.append(bgPhotoImage->getCString());
        if (cocos2d::FileUtils::getInstance()->isFileExist(path) ){
            
            cocos2d::Data imageData = cocos2d::FileUtils::getInstance()->getDataFromFile(path.c_str());
            unsigned long nSize = imageData.getSize();
            
            cocos2d::Image *image = new cocos2d::Image();
            image->initWithImageData(imageData.getBytes(), nSize);
            
            cocos2d::Texture2D *texture = new cocos2d::Texture2D();
            texture->initWithImage(image);
            
            auto picture = Sprite::createWithTexture(texture);
            picture->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
            float scaleFactor = Util::getCharacterScaleFactor();
            picture->setPosition(cocos2d::Point(HOME_CUSTOM_BG_BIG.x/scaleFactor,HOME_CUSTOM_BG_BIG.y/scaleFactor));
            selectedFrame->addChild(picture);
        } else {
            log("error! no file : %s", path.c_str());
        }
    }
    
    bool lockedItem = false;
    
    if (!isCustom) {
        if (backgroundInfo["locked"].GetBool()) {
            int expiredDay = 0;
            
            if (backgroundInfo.HasMember("expired_hour")) {
                auto expiredMin = backgroundInfo["expired_hour"].GetInt();
                expiredDay = expiredMin/60/24;
            }
            
            showUnlockTalkBox(UnlockItemType::ItemTypeBG, expiredDay);
            selectedLockedBackgroundKey = selectedBackgroundKey;
            lockedItem = true;
        }
    }
    
    if (!lockedItem) {
        lastUnlockBackgroundKey = selectedBackgroundKey;
        selectedLockedBackgroundKey = nullptr;
        closeUnlockTalkbox();
        
        AppDelegate* app = (AppDelegate*)Application::getInstance();
        if(app != nullptr) {
            if (!isCustom) {
                char dictStr[128];
                sprintf(dictStr, "{\"Background\":\"%s\"}", selectedBackgroundKey->getCString());
                app->mixpanelTrack("Used Background in Story", dictStr);
            } else {
                char dictStr[128];
                sprintf(dictStr, "{\"Background\":\"%s\"}", "Custom");
                app->mixpanelTrack("Used Background in Story", dictStr);
            }
        }
    }
}

Sprite* CharacterRecording::getCcbiSprite(const char* ccbiName, cocos2d::Point position)
{
    cocosbuilder::NodeLoaderLibrary *nodeLoaderLibrary = cocosbuilder::NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
    cocosbuilder::CCBReader *ccbReader = new cocosbuilder::CCBReader(nodeLoaderLibrary);
    Node *ccbiObj = ccbReader->readNodeGraphFromFile(ccbiName, this);
    auto ccbiSprite = (Sprite*)ccbiObj;
    ccbiSprite->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    ccbiSprite->setPosition(position);
    ccbReader->release();
    return ccbiSprite;
}

void CharacterRecording::scrollViewButtonEnable(bool backgroundButtonEnable, bool isMusicScroll)
{
    if (isMusicScroll) {
        __Array *chardeleteButtons = characterScrollView->deleteButtons;
        auto size1 = chardeleteButtons->count();
        for(int index = 0 ; index < size1 ; index++){
            ui::Button *deleteBtn = (ui::Button*)chardeleteButtons->getObjectAtIndex(index);
            deleteBtn->setEnabled(false);
        }
        __Array *deleteButtons = backgroundScrollView->deleteButtons;
        auto size2 = deleteButtons->count();
        for(int index = 0 ; index < size2 ; index++){
            ui::Button *deleteBtn = (ui::Button*)deleteButtons->getObjectAtIndex(index);
            deleteBtn->setEnabled(false);
        }
    } else {
        __Array *chardeleteButtons = characterScrollView->deleteButtons;
        auto size1 = chardeleteButtons->count();
        for(int index = 0 ; index < size1 ; index++){
            ui::Button *deleteBtn = (ui::Button*)chardeleteButtons->getObjectAtIndex(index);
            deleteBtn->setEnabled(!backgroundButtonEnable);
        }
        __Array *deleteButtons = backgroundScrollView->deleteButtons;
        auto size2 = deleteButtons->count();
        for(int index = 0 ; index < size2 ; index++){
            ui::Button *deleteBtn = (ui::Button*)deleteButtons->getObjectAtIndex(index);
            deleteBtn->setEnabled(backgroundButtonEnable);
        }
    }
    
}

void CharacterRecording::showSoundScrollView()
{
    if (scrollViewToggleAnimation) return;
    
    characterScrollView->setVisible(false);
    isCharacterScrollViewVisible = false;
    backgroundScrollView->setVisible(false);
    isBackgroundScrollViewVisible = false;
    
    this->scrollViewButtonEnable(false, true);
    float scrollDuration = 0.2;
    if (!isSoundScrollViewVisible) {
        if(this->isScrollViewVisible()){
            removeSelectedLockCharacter();
            removeSelectedLockBackground();
            soundScrollView->setVisible(!soundScrollView->isVisible());
            isSoundScrollViewVisible = !isSoundScrollViewVisible;
#ifdef _GUIDE_ENABLE_
            GuideSetPosition::create(this, GuideSetPosition::GuideMode::Music);
#endif
        } else {
            scrollViewToggleAnimation = true;
            soundScrollView->setVisible(true);
            auto action = EaseInOut::create(MoveTo::create(scrollDuration, cocos2d::Point(0, RECORDING_BOTTOM_SCROLL_AREA_HEIGHT)), 0.8f);
            auto actionCallback = CallFunc::create([&](){
                scrollViewToggleAnimation = false;
#ifdef _GUIDE_ENABLE_
                GuideSetPosition::create(this, GuideSetPosition::GuideMode::Music);
#endif
            });
            auto squence = Sequence::create(action, actionCallback, NULL);
            mainLayer->runAction(squence);
            isSoundScrollViewVisible = true;
            
            CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
            if (bgmFileName != nullptr) {
                CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(bgmFileName->getCString());
            }
        }
        this->setCreateButtons(false);
    } else {
//        this->hideBottomScrollView();
        soundScrollView->scrollToFirstItem();

//        soundScrollView->scrollToFirstItem();
//        auto action = EaseInOut::create(MoveTo::create(scrollDuration, cocos2d::Point::ZERO), 0.8f);
//        auto actionCallback = CallFunc::create([&](){
//            soundScrollView->setVisible(false);
//            this->setCreateButtons(false);
//            scrollViewToggleAnimation = false;
//            if(unlockTalkBox){
//                unlockTalkBox->removeFromParentAndCleanup(true);
//                unlockTalkBox = nullptr;
//            }
//        });
//        auto squence = Sequence::create(action, actionCallback, NULL);
//        mainLayer->runAction(squence);
//        isSoundScrollViewVisible = false;
    }
}

void CharacterRecording::showBackgroundScrollView()
{
    if (scrollViewToggleAnimation) return;
    
    characterScrollView->setVisible(false);
    isCharacterScrollViewVisible = false;
    soundScrollView->setVisible(false);
    isSoundScrollViewVisible = false;
    
    float scrollDuration = 0.2;
    if (!isBackgroundScrollViewVisible) {
        if(this->isScrollViewVisible()){
            CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
            removeSelectedLockCharacter();
            
            backgroundScrollView->setVisible(!backgroundScrollView->isVisible());
            isBackgroundScrollViewVisible = !isBackgroundScrollViewVisible;
#ifdef _GUIDE_ENABLE_
            GuideSetPosition::create(this, GuideSetPosition::GuideMode::Background);
#endif
        } else {
            scrollViewToggleAnimation = true;
            backgroundScrollView->setVisible(true);
            auto action = EaseInOut::create(MoveTo::create(scrollDuration, cocos2d::Point(0, RECORDING_BOTTOM_SCROLL_AREA_HEIGHT)), 0.8f);
            auto actionCallback = CallFunc::create([&](){
                scrollViewToggleAnimation = false;
#ifdef _GUIDE_ENABLE_
                GuideSetPosition::create(this, GuideSetPosition::GuideMode::Background);
#endif
            });
            auto squence = Sequence::create(action, actionCallback, NULL);
            mainLayer->runAction(squence);
            isBackgroundScrollViewVisible = true;
        }
        this->setCreateButtons(true);
        this->scrollViewButtonEnable(true, false);
    } else {
//        this->hideBottomScrollView();
        backgroundScrollView->scrollToFirstItem();

//        backgroundScrollView->scrollToFirstItem();
//        auto action = EaseInOut::create(MoveTo::create(scrollDuration, cocos2d::Point::ZERO), 0.8f);
//        auto actionCallback = CallFunc::create([&](){
//            backgroundScrollView->setVisible(false);
//            this->setCreateButtons(false);
//            scrollViewToggleAnimation = false;
//            if(unlockTalkBox){
//                unlockTalkBox->removeFromParentAndCleanup(true);
//                unlockTalkBox = nullptr;
//            }
//        });
//        auto squence = Sequence::create(action, actionCallback, NULL);
//        mainLayer->runAction(squence);
//        isBackgroundScrollViewVisible = false;
//        this->scrollViewButtonEnable(false, false);
    }
}

void CharacterRecording::showCharacterScrollView()
{
    if (scrollViewToggleAnimation) return;
    
    backgroundScrollView->setVisible(false);
    isBackgroundScrollViewVisible = false;
    soundScrollView->setVisible(false);
    isSoundScrollViewVisible = false;
    
    float scrollDuration = 0.2;
    if (!isCharacterScrollViewVisible) {
        if(this->isScrollViewVisible()){
            removeSelectedLockBackground();
            CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
            
            characterScrollView->setVisible(!characterScrollView->isVisible());
            isCharacterScrollViewVisible = !isCharacterScrollViewVisible;
#ifdef _GUIDE_ENABLE_
            GuideSetPosition::create(this, GuideSetPosition::GuideMode::Character);
#endif
        } else {
            scrollViewToggleAnimation = true;
            characterScrollView->setVisible(true);
            auto action = EaseInOut::create(MoveTo::create(scrollDuration, cocos2d::Point(0, RECORDING_BOTTOM_SCROLL_AREA_HEIGHT)), 0.8f);
            auto actionCallback = CallFunc::create([&](){
                scrollViewToggleAnimation = false;
#ifdef _GUIDE_ENABLE_
                GuideSetPosition::create(this, GuideSetPosition::GuideMode::Character);
#endif
            });
            auto squence = Sequence::create(action, actionCallback, NULL);
            mainLayer->runAction(squence);
            isCharacterScrollViewVisible = true;
        }
        this->setCreateButtons(true);
        this->scrollViewButtonEnable(false, false);
    } else {
//        this->hideBottomScrollView();
        characterScrollView->scrollToFirstItem();

//        characterScrollView->scrollToFirstItem();
//        auto action = EaseInOut::create(MoveTo::create(scrollDuration, cocos2d::Point::ZERO), 0.8f);
//        auto actionCallback = CallFunc::create([&](){
//            characterScrollView->setVisible(false);
//            this->setCreateButtons(false);
//            scrollViewToggleAnimation = false;
//            if(unlockTalkBox){
//                unlockTalkBox->removeFromParentAndCleanup(true);
//                unlockTalkBox = nullptr;
//            }
//        });
//        auto squence = Sequence::create(action, actionCallback, NULL);
//        mainLayer->runAction(squence);
//        isCharacterScrollViewVisible = false;
//        this->scrollViewButtonEnable(true, false);
    }
}

void CharacterRecording::hideBottomScrollView()
{
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    scrollViewToggleAnimation = true;
    float scrollDuration = 0.2;
    auto action = EaseInOut::create(MoveTo::create(scrollDuration, cocos2d::Point::ZERO), 0.8f);
    auto actionCallback = CallFunc::create([&](){
        if (characterScrollView->isVisible() == true && characters->count() > 0) {
#ifdef _GUIDE_ENABLE_
            GuideSetPosition::create(this, GuideSetPosition::GuideMode::Delete);
#endif
        }
        backgroundScrollView->setVisible(false);
        characterScrollView->setVisible(false);
        soundScrollView->setVisible(false);
        this->setCreateButtons(false);
        scrollViewToggleAnimation = false;
        
        if (selectedLockedCharacter != nullptr || selectedLockedBackgroundKey != nullptr) {
            showTransparentLayer();
        }
    });
    auto squence = Sequence::create(action, actionCallback, NULL);
    mainLayer->runAction(squence);
    isCharacterScrollViewVisible = false;
    isBackgroundScrollViewVisible = false;
    isSoundScrollViewVisible = false;
    
    this->scrollViewButtonEnable(true, false);
}

void CharacterRecording::showTransparentLayer()
{
    if(transparentLayer == nullptr) {
        log("show transparent layer");

        transparentLayer = Layer::create();
        cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
        transparentLayer->setContentSize(visibleSize);
        
        auto listener = EventListenerTouchOneByOne::create();
        listener->setSwallowTouches(true);
        listener->onTouchBegan = [&](Touch* touch, Event* event) {
            if (unlockTalkBox != nullptr) {
                cocos2d::Point touchInNode = unlockTalkBox->convertToNodeSpace(touch->getLocation());
                
                auto boundingRect = unlockTalkBox->getBoundingBox();
                auto visibleRect = cocos2d::Rect(0, 0, boundingRect.size.width / unlockTalkBox->getScale(), boundingRect.size.height / unlockTalkBox->getScale());
                bool result = visibleRect.containsPoint(touchInNode);
                return !result;
            }
            return true;
        };
        
        listener->onTouchMoved = [&](Touch* touch, Event* event) {
            
        };
        
        listener->onTouchEnded = [&](Touch* touch, Event* event) {
            removeTransparentLayer();
        };
        
        listener->onTouchCancelled = [&](Touch* touch, Event* event) {
            removeTransparentLayer();
         };
        
        _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, transparentLayer);
        this->addChild(transparentLayer, 14);
    }
}

void CharacterRecording::removeTransparentLayer()
{
    if (transparentLayer != nullptr){
        log("removed transparent layer");
        _eventDispatcher->removeEventListenersForTarget(transparentLayer);
        transparentLayer->removeFromParent();
        transparentLayer = nullptr;
        
        removeSelectedLockCharacter();
        removeSelectedLockBackground();

        if (unlockTalkBox != nullptr) {
            unlockTalkBox->removeFromParent();
            unlockTalkBox = nullptr;
        }
    }
}

void CharacterRecording::showRecordingIcon(bool show)
{
    recordingIcon->setVisible(show);
    doneMenuItem->setVisible(!show);
}

void CharacterRecording::itemButtonClicked(Ref* obj)
{
    auto button = (MenuItemImage*)obj;
    int tag = button->getTag();
 
    if (transparentLayer != nullptr) {
        return;
    }
    
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Tap.wav", false);
    
    if (tag == MENU_BG) {
        if (!scrollViewToggleAnimation) {
            log("bg item button clicked!");
            createButtonLabel->setString(Device::getResString(Res::Strings::CC_LIST_BACKGROUND_MAKE));

            button->setEnabled(false);
            auto afterCallBack = CallFunc::create(CC_CALLBACK_0(CharacterRecording::showBackgroundScrollView, this));
            auto toggleActionFinished = CallFunc::create([=](){
                button->setEnabled(true);
            });
            auto actions = Sequence::create(afterCallBack, toggleActionFinished, NULL);
            button->runAction(actions);
            
            Sprite *img;
            if (openBgItem) {
//                img = Sprite::create("btn_home_bg_normal.png");
            } else {
                img = Sprite::create("btn_home_bg_selected.png");
                openBgItem = true;
                button->setNormalImage(img);
            }
//            openBgItem = !openBgItem;
//            button->setNormalImage(img);
            
            if (openCharacterItem) {
                MenuItemImage* characterItem = (MenuItemImage*)bottomMenu->getChildByTag(MENU_ITEM);
                characterItem->setNormalImage(Sprite::create("btn_home_item_normal.png"));
                characterItem->setEnabled(true);
                openCharacterItem = false;
            } else if (openSoundItem) {
                MenuItemImage* soundItem = (MenuItemImage*)bottomMenu->getChildByTag(MENU_MUSIC);
                soundItem->setNormalImage(Sprite::create("btn_set_position_music_normal.png"));
                soundItem->setEnabled(true);
                openSoundItem = false;
            }
        }
    } else if (tag == MENU_ITEM) {
        if (!scrollViewToggleAnimation) {
            log("item button clicked!");
        createButtonLabel->setString(Device::getResString(Res::Strings::CC_LIST_CHARACTER_MAKE));

            button->setEnabled(false);
            auto afterCallBack = CallFunc::create(CC_CALLBACK_0(CharacterRecording::showCharacterScrollView, this));
            auto toggleActionFinished = CallFunc::create([=](){
                button->setEnabled(true);
            });
            auto actions = Sequence::create(afterCallBack, toggleActionFinished, NULL);
            button->runAction(actions);
            
            Sprite *img;
            if (openCharacterItem) {
//                img = Sprite::create("btn_home_item_normal.png");
            } else {
                img = Sprite::create("btn_home_item_selected.png");
                button->setNormalImage(img);
                openCharacterItem = true;
            }
//            openCharacterItem = !openCharacterItem;
//            button->setNormalImage(img);
            
            if (openBgItem) {
                MenuItemImage* bgItem = (MenuItemImage*)bottomMenu->getChildByTag(MENU_BG);
                bgItem->setNormalImage(Sprite::create("btn_home_bg_normal.png"));
                bgItem->setEnabled(true);
                openBgItem = false;
            } else if (openSoundItem) {
                MenuItemImage* soundItem = (MenuItemImage*)bottomMenu->getChildByTag(MENU_MUSIC);
                soundItem->setNormalImage(Sprite::create("btn_set_position_music_normal.png"));
                soundItem->setEnabled(true);
                openSoundItem = false;
            }
        }
    } else if (tag == MENU_MUSIC) {
        if (!scrollViewToggleAnimation) {
            log("bg item button clicked!");
            this->setCreateButtons(false);
            
            button->setEnabled(false);
            auto afterCallBack = CallFunc::create(CC_CALLBACK_0(CharacterRecording::showSoundScrollView, this));
            auto toggleActionFinished = CallFunc::create([=](){
                button->setEnabled(true);
            });
            auto actions = Sequence::create(afterCallBack, toggleActionFinished, NULL);
            button->runAction(actions);
            
            Sprite *img;
            if (openSoundItem) {
//                img = Sprite::create("btn_set_position_music_normal.png");
            } else {
                img = Sprite::create("btn_set_position_music_selected.png");
                button->setNormalImage(img);
                openSoundItem = true;
            }
//            openSoundItem = !openSoundItem;
//            button->setNormalImage(img);
            
            if (openCharacterItem) {
                MenuItemImage* characterItem = (MenuItemImage*)bottomMenu->getChildByTag(MENU_ITEM);
                characterItem->setNormalImage(Sprite::create("btn_home_item_normal.png"));
                characterItem->setEnabled(true);
                openCharacterItem = false;
            } else if (openBgItem) {
                MenuItemImage* bgItem = (MenuItemImage*)bottomMenu->getChildByTag(MENU_BG);
                bgItem->setNormalImage(Sprite::create("btn_home_bg_normal.png"));
                bgItem->setEnabled(true);
                openBgItem = false;
            }
        }
    }
}

void CharacterRecording::showReUsePopup()
{
//    Director::getInstance()->pause();
    PopupLayer *modal;
//    if (Application::getInstance()->getCurrentLanguage() != LanguageType::JAPANESE) {
//        modal = PopupLayer::create(POPUP_TYPE_WARNING,
//                                               Device::getResString(Res::Strings::CC_SETPOSITION_POPUP_NOTICE_AUTOSELECT_LASTPAGE), // "Do you want to use the item\nyou selected on the last page?";
//                                               Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL),//"Cancel",
//                                               Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK));//"OK");
//    } else {
        modal = PopupLayer::create(POPUP_TYPE_WARNING,
                                               Device::getResString(Res::Strings::CC_SETPOSITION_POPUP_NOTICE_AUTOSELECT_LASTPAGE), // "Do you want to use the item\nyou selected on the last page?";
                                               Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_NO),//"No",
                                               Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_YES));//"Yes");
//    }
    modal->setTag(TAG_POPUP_REUSE_ITEM);
    modal->setDelegate(this);
    this->addChild(modal, 1000);
}

void CharacterRecording::showUnlockTalkBox(UnlockItemType type, int period)
{
    if (unlockTalkBox == nullptr) {
        closeUnlockTalkbox();
    }
    
    // create text label
    std::string originString;
    if (type == UnlockItemType::ItemTypeCharacter) {
        if(period == 0) {
            originString = Device::getResString(Res::Strings::CC_EXPANDEDITEM_NEED_UNLOCK_COIN_CHA);// "Use Dole Coins to unlock\nthe special item."
        } else {
            auto foramtString = Device::getResString(Res::Strings::CC_EXPANDEDITEM_NEED_UNLOCK_COIN_LIMITED_PERIOD); // "Once unlocked, you will be able to use \nthe item for %d days. "
            auto result = __String::createWithFormat(foramtString, period);
            originString = result->getCString();
        }
    } else {
        if(period == 0) {
            originString = Device::getResString(Res::Strings::CC_EXPANDEDBG_NEED_UNLOCK_COIN); //"Use Dole Coins to unlock\nthe special background"
        } else {
            auto foramtString = Device::getResString(Res::Strings::CC_EXPANDEDBG_NEED_UNLOCK_COIN_LIMITED_PERIOD); // "Once unlocked, you will be able to use \nthe background for %d days. "
            auto result = __String::createWithFormat(foramtString, period);
            originString = result->getCString();
        }
    }
    
    std::stringstream stringStream(originString);
    std::string line;
    std::vector<std::string> lines;
    while (std::getline(stringStream, line, '\n')) { // split string by '\n'
        lines.push_back(line);
    }
    
    int lineCount = (int)lines.size();
    float maxWidth = 0;
    Vector<std::string>::iterator itr;
    for(itr = lines.begin(); itr != lines.end(); itr++)
    {
        auto textWidth = Device::getTextWidth(itr->c_str(), FONT_NAME, RECORDING_TALK_BOX_TEXT_FONT_SIZE);
        maxWidth = MAX(maxWidth, textWidth);
    }
    
    float textLimit = 0;
    float leftMargin = RECORDING_TALK_BOX_TEXT_LEFT_MARGIN;
    float bottomMargin = RECORDING_TALK_BOX_TEXT_BOTTOM_MARGIN;
    float topMargin = RECORDING_TALK_BOX_TEXT_TOP_MARGIN;
    if (type == UnlockItemType::ItemTypeCharacter) {
//            textLimit = (lineCount > 2) ? RECORDING_TALK_BOX_CHARACTER_MAX_WIDTH_LONG : RECORDING_TALK_BOX_CHARACTER_MAX_WIDTH_SHORT;
        textLimit = RECORDING_TALK_BOX_CHARACTER_MAX_WIDTH_LONG; // short width is not enough size.

    } else {
        textLimit = (lineCount > 2) ? RECORDING_TALK_BOX_BACKGROUND_MAX_WIDTH_LONG : RECORDING_TALK_BOX_BACKGROUND_MAX_WIDTH_SHORT;
    }

    float textWidth = MIN(maxWidth, textLimit);
    float textHeight = 0;
#ifdef MACOS
    textHeight = 58;
#endif
    
    auto textLabel = LabelExtension::create(originString, FONT_NAME, RECORDING_TALK_BOX_TEXT_FONT_SIZE, cocos2d::Size(textWidth, textHeight),
                                            TextHAlignment::LEFT, TextVAlignment::BOTTOM);
    
    textLabel->setColor(Color3B(0xff,0xff,0xff));
    textLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    textLabel->setPosition(cocos2d::Point(leftMargin, bottomMargin));
    textLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    
    float textAreaHeight = textLabel->getContentSize().height;
    
    // create unlock button
    std::string unlockString = Device::getResString(Res::Strings::CC_EXPANDEDITEM_POPUP_COIN_BUTTON_TITLE_UNLOCK); // "Unlock"
//        auto unlockStringWidth = Device::getTextWidth(itr->c_str(), FONT_NAME, RECORDING_TALK_BOX_UNLOCK_BUTTON_FONT_SIZE);
    
    auto unlockLabel = LabelExtension::create(unlockString, FONT_NAME_BOLD, RECORDING_TALK_BOX_UNLOCK_BUTTON_FONT_SIZE,
                                              cocos2d::Size(RECORDING_TALK_BOX_UNLOCK_BUTTON_TEXT_MAX_WIDTH - RECORDING_TALK_BOX_UNLOCK_BUTTON_TEXT_SIDE_MARGIN * 2, 0),
                                              TextHAlignment::CENTER, TextVAlignment::CENTER);
    unlockLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    
    cocos2d::Rect okInset = cocos2d::Rect(RECORDING_CLEAR_TALK_BOX_OK_BUTTON_INSET.x, RECORDING_CLEAR_TALK_BOX_OK_BUTTON_INSET.y, 1, 1);
    auto unlockNormalSprite = Scale9Sprite::create(okInset, "set_position_talk_box_btn_ok_normal.png");
    auto unlockPressedSprite = Scale9Sprite::create(okInset, "set_position_talk_box_btn_ok_pressed.png");
    
    float talkboxHeight = textAreaHeight + topMargin + bottomMargin;
    
    float unlockButtonHeight = talkboxHeight - RECORDING_TALK_BOX_UNLOCK_BUTTON_TOP_MARGIN - RECORDING_TALK_BOX_UNLOCK_BUTTON_BOTTOM_MARGIN;
    
    unlockNormalSprite->setContentSize(cocos2d::Size(RECORDING_TALK_BOX_UNLOCK_BUTTON_TEXT_MAX_WIDTH, unlockButtonHeight));
    unlockPressedSprite->setContentSize(cocos2d::Size(RECORDING_TALK_BOX_UNLOCK_BUTTON_TEXT_MAX_WIDTH, unlockButtonHeight));
    
    auto okItem = MenuItemSprite::create(unlockNormalSprite, unlockPressedSprite,
                                         CC_CALLBACK_1(CharacterRecording::unlockTalkBoxCallback, this));

    float talkboxWidth = leftMargin + textWidth + RECORDING_TALK_BOX_UNLOCK_BUTTON_LEFT_MARGIN + RECORDING_TALK_BOX_UNLOCK_BUTTON_TEXT_MAX_WIDTH + RECORDING_TALK_BOX_UNLOCK_BUTTON_BOTTOM_MARGIN;
    
    unlockLabel->setPosition(okItem->getContentSize().width/2, okItem->getContentSize().height/2);
    okItem->addChild(unlockLabel);
    okItem->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
    okItem->setPosition(cocos2d::Point(talkboxWidth-RECORDING_TALK_BOX_UNLOCK_BUTTON_RIGHT_MARGIN, RECORDING_TALK_BOX_UNLOCK_BUTTON_BOTTOM_MARGIN));
    
    cocos2d::Rect talkBoxInset = cocos2d::Rect(RECORDING_UNLOCK_TALK_BOX_INSET.x, RECORDING_UNLOCK_TALK_BOX_INSET.y,
                                               RECORDING_UNLOCK_TALK_BOX_INSET_SIZE.width, RECORDING_UNLOCK_TALK_BOX_INSET_SIZE.height);
    unlockTalkBox = Scale9Sprite::create(talkBoxInset, "set_position_lock_bg_talk_box_bg.png");
    unlockTalkBox->setContentSize(cocos2d::Size(talkboxWidth, talkboxHeight));
    
    auto talkBoxMenu = Menu::create(okItem, NULL);
    talkBoxMenu->setPosition(cocos2d::Point::ZERO);
    unlockTalkBox->addChild(talkBoxMenu);
    unlockTalkBox->addChild(textLabel);


    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	unlockTalkBox->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_BOTTOM);
	unlockTalkBox->setPosition(cocos2d::Point(visibleSize.width/2,
			(visibleSize.height - RECORDING_UNLOCK_TALK_BOX_SIZE.height) / 2 - RECORDING_BOTTOM_SCROLL_AREA_HEIGHT));
#else
    unlockTalkBox->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
    unlockTalkBox->setPosition(cocos2d::Point(visibleSize.width/2, visibleSize.height/2 - RECORDING_BOTTOM_SCROLL_AREA_HEIGHT));
#endif
    mainLayer->addChild(unlockTalkBox, 15);
}

void CharacterRecording::closeUnlockTalkbox()
{
    if(unlockTalkBox){
        unlockTalkBox->removeFromParentAndCleanup(true);
        unlockTalkBox = nullptr;
    }
}

void CharacterRecording::unlockTalkBoxCallback(Ref* pSender)
{
    if(unlockTalkBox){
        if (pSender == nullptr) {
            return;
        }
        
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Tap.wav", false);
        // TODO: start purchase process
        this->checkExecutePurchase();
    }
}

void CharacterRecording::cancelExecutePurchase(bool netfail) {
    TSDoleNetProgressLayer *progressLayer = (TSDoleNetProgressLayer *)this->getChildByTag(TAG_NET_PROGRESS);
    if (progressLayer) {
        if(netfail) {
            progressLayer->activityStop(Device::getResString(Res::Strings::CM_NET_WARNING_NETWORK_UNSTABLE));
        } else {
            progressLayer->activityStop(NULL);
        }
    }
    this->onFinishedPurchaseProcess(false, NULL);
}

void CharacterRecording::checkExecutePurchase() {
    std::string key;
    if (selectedLockedCharacter) {
        key = selectedLockedCharacter->getCharKey();
    } else if (selectedLockedBackgroundKey) {
        key = selectedLockedBackgroundKey->getCString();
    }
    if (key.length() <= 0) {
        log("not selected purchase item... check please...");
        return;
    }
    
    std::string authKey = UserDefault::getInstance()->getStringForKey(UDKEY_STRING_AUTHKEY);
    if (authKey.length() <= 0) {
        PopupLayer *needLogin;
        if (Application::getInstance()->getCurrentLanguage() != LanguageType::JAPANESE) {
            needLogin = PopupLayer::create(POPUP_TYPE_WARNING, Device::getResString(Res::Strings::CC_EXPANDEDITEM_POPUP_NEED_LOGIN),
                                           Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL),
                                           Device::getResString(Res::Strings::CC_HOME_POPUP_NEED_LOGIN_BUTTON_TITLE_LOGIN));//"Log in to use Dole Coin.", "Cancel", "Log in"
        } else {
            needLogin = PopupLayer::create(POPUP_TYPE_WARNING, Device::getResString(Res::Strings::CC_EXPANDEDITEM_POPUP_NEED_LOGIN),
                                            "もどる", "ログインする");
        }
        needLogin->setTag(TAG_POPUP_PURCHASE_NEEDLOGIN);
        needLogin->setDelegate(this);
        this->addChild(needLogin, 1000);
        return;
    }

    __Array *products = _homeLayer->getProducts();
    ssize_t count = (products ? products->count() : 0);
    if (count == 0) {
        log("product list empty...");
        return;
    }
    
    DoleProduct* product = nullptr;
    for (ssize_t index = 0; index<count; index++) {
        product = (DoleProduct*)products->getObjectAtIndex(index);
        const char *code = product->getProductCode();
        if (product && strcmp(code, key.c_str()) == 0 ) {
            break;
        }
    }

    if (!product) {
        log("product empty...");
#if COCOS2D_DEBUG
        PopupLayer *errorCoin = PopupLayer::create(POPUP_TYPE_WARNING, "For DEBUG\nProduct Infomation not exist.",
                                                   Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK));
        errorCoin->setTag(TAG_POPUP_NONE);
        this->addChild(errorCoin, 1000);
#endif
        return;
    }

    int salePrice = product->getSalePrice();
    if (salePrice > _homeLayer->getDoleCoin()) {
        
        PopupLayer *errorCoin = PopupLayer::create(POPUP_TYPE_WARNING, Device::getResString(Res::Strings::CC_EXPANDEDITEM_POPUP_NOT_ENOUGH_COIN),
                                                   Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL),
                                                   Device::getResString(Res::Strings::CC_EXPANDEDITEM_POPUP_BUTTON_TITLE_EARN_COIN));
        errorCoin->setTag(TAG_POPUP_NOTENOUGH_COIN);
        errorCoin->setDelegate(this);
        this->addChild(errorCoin, 1000);
        return;
    }
    
    __String *message = __String::createWithFormat(Device::getResString(Res::Strings::CC_EXPANDEDITEM_POPUP_WARNING_USE_UNLOCK_COIN), salePrice);//"Unlock with %d Dole Coin.\nThis action cannot be undone"
    PopupLayer *errorCoin = PopupLayer::create(POPUP_TYPE_WARNING, message->getCString(),
                                               Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL),//"Cancel",
                                               Device::getResString(Res::Strings::CC_EXPANDEDITEM_POPUP_COIN_BUTTON_TITLE_UNLOCK));// "Unlock"
    errorCoin->setTag(TAG_POPUP_UNLOCKITEM);
    errorCoin->setDelegate(this);
    TSDoleNetProgressLayer *progressLayer = (TSDoleNetProgressLayer *)this->getChildByTag(TAG_NET_PROGRESS);
    if (progressLayer) {
        progressLayer->addChild(errorCoin);
    } else {
        this->addChild(errorCoin, 1000);
    }
}

void CharacterRecording::doExecutePurchase() {
    std::string key;
    if (selectedLockedCharacter) {
        key = selectedLockedCharacter->getCharKey();
    } else if (selectedLockedBackgroundKey) {
        key = selectedLockedBackgroundKey->getCString();
    }
    if (key.length() <= 0) {
        log("not selected purchase item... check please...");
        return;
    }
    __Array *products = _homeLayer->getProducts();
    ssize_t count = (products ? products->count() : 0);
    if (count == 0) {
        return;
    }
    
    DoleProduct* product = nullptr;
    for (ssize_t index = 0; index<count; index++) {
        product = (DoleProduct*)products->getObjectAtIndex(index);
        const char *code = product->getProductCode();
        if (product && strcmp(code, key.c_str()) == 0 ) {
            break;
        }
    }
    
    if (!product) {
#if COCOS2D_DEBUG
        PopupLayer *errorCoin = PopupLayer::create(POPUP_TYPE_WARNING, "For DEBUG\nProduct Infomation not exist.",
                                                   Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK));
        errorCoin->setTag(TAG_POPUP_NONE);
        this->addChild(errorCoin, 1000);
#endif
        return;
    }
    
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	if(_protocolExecutePurchase != nullptr) {
		if(_protocolExecutePurchase->getStatus() == TSProtocolStatus::Finished) {
			CC_SAFE_RELEASE_NULL(_protocolExecutePurchase);
		} else {
			_protocolExecutePurchase->setStatus(TSProtocolStatus::Canceled);
		}
	}
//#else
//    CC_SAFE_RELEASE_NULL(_protocolExecutePurchase);
//#endif
    _protocolExecutePurchase = new TSDoleExecutePurchase();
    _protocolExecutePurchase->setDelegate(this);
    _protocolExecutePurchase->setTag(TAG_NET_PURCHASE);
    _protocolExecutePurchase->init(product);
    _protocolExecutePurchase->request();
}

void CharacterRecording::doleCheckInventoryListUpdate() {
    if (_homeLayer->getProducts()) {
        this->doleGetInventoryListProtocol();
    } else {
        auto sequence = Sequence::create(DelayTime::create(0.5f), CallFunc::create(CC_CALLBACK_0(CharacterRecording::doleCheckInventoryListUpdate, this)), NULL);
        this->runAction(sequence);
    }
}

void CharacterRecording::doleGetInventoryListProtocol() {
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	if(_protocolInventoryList != nullptr) {
		if(_protocolInventoryList->getStatus() == TSProtocolStatus::Finished) {
			CC_SAFE_RELEASE_NULL(_protocolInventoryList);
		} else {
			_protocolInventoryList->setStatus(TSProtocolStatus::Canceled);
		}
	}
//#else
//    CC_SAFE_RELEASE_NULL(_protocolInventoryList);
//#endif
    _protocolInventoryList = new TSDoleGetInventoryList();
    _protocolInventoryList->setDelegate(this);
    _protocolInventoryList->setTag(TAG_NET_INVENTORY);
    _protocolInventoryList->init(UserDefault::getInstance()->getStringForKey(UDKEY_STRING_AUTHKEY), _homeLayer->getProducts());
    _protocolInventoryList->request();
}

void CharacterRecording::checkRecall(int type) {
    switch (type) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    	case ANDROID_UNLOCK_ITEM_WITH_LOGIN: {
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
        case TSRecallCharRECUnlockItem: {
#endif
//            this->checkExecutePurchase();
            _homeLayer->setChaRecLayer(this);
            TSDoleNetProgressLayer *layer = (TSDoleNetProgressLayer*)this->getChildByTag(TAG_NET_PROGRESS);
            if (layer == nullptr) {
                TSDoleNetProgressLayer *progressLayer = TSDoleNetProgressLayer::create();
                progressLayer->activityStart();
                this->addChild(progressLayer);
            }
        } break;
        default:
            break;
    }
}

void CharacterRecording::onRequestStarted(TSDoleProtocol *protocol, TSNetResult result) {
    switch (protocol->getTag()) {
        case TAG_NET_PURCHASE: {
            TSDoleNetProgressLayer *layer = (TSDoleNetProgressLayer*)this->getChildByTag(TAG_NET_PROGRESS);
            if (layer == nullptr) {
                TSDoleNetProgressLayer *progressLayer = TSDoleNetProgressLayer::create();
                progressLayer->activityStart();
                this->addChild(progressLayer);
            }
        } break;
        default:
            break;
    }
}

void CharacterRecording::onResponseEnded(TSDoleProtocol *protocol, TSNetResult result) {
    switch (result) {
        case TSNetResult::Success : {
            switch (protocol->getTag()) {
                case TAG_NET_PURCHASE: {
                    _homeLayer->doleCoinGetBalanceProtocol();
                    this->doleGetInventoryListProtocol();
                    
                        #if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
                    
                            std::string key;
                            if (selectedLockedCharacter) {
                                key = selectedLockedCharacter->getCharKey();
                                AppDelegate* app = (AppDelegate*)Application::getInstance();
                                app->purchasedCharacter(key.c_str());
                                
                            } else if (selectedLockedBackgroundKey) {
                                key = selectedLockedBackgroundKey->getCString();
                                AppDelegate* app = (AppDelegate*)Application::getInstance();
                                app->purchasedBackground(key.c_str());
                            }
                    
                        #elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
                        #   ifndef MACOS
                        #   endif
                        #endif
                    
                    return;
                } break;
                case TAG_NET_INVENTORY: {
                    TSDoleNetProgressLayer *progressLayer = (TSDoleNetProgressLayer *)this->getChildByTag(TAG_NET_PROGRESS);
                    if (progressLayer) {
                        progressLayer->activityStop(Device::getResString(Res::Strings::CC_HOME_POPUP_UNLOCK_ITEM_MESSAGE));//"Congratulations!\nYou received the item.");
                    }
                    _homeLayer->updateProductJsonInfomation();
                    this->onFinishedPurchaseProcess(true, NULL);
                } break;
                default:
                    break;
            }
        } break;
        case TSNetResult::ErrorTimeout: {
            switch (protocol->getTag()) {
                case TAG_NET_INVENTORY:
                case TAG_NET_PURCHASE: {
                    TSDoleNetProgressLayer *progressLayer = (TSDoleNetProgressLayer *)this->getChildByTag(TAG_NET_PROGRESS);
                    if (progressLayer) {
                        progressLayer->activityStop(Device::getResString(Res::Strings::CM_NET_WARNING_NETWORK_UNSTABLE));
                    }
                    this->onFinishedPurchaseProcess(false, NULL);
                } break;
                default:
                    TSNetToastLayer::show(this);
                    break;
            }
        } break;
        default:
            TSNetToastLayer::show(this);
            break;

    }
}

void CharacterRecording::onProtocolFailed(TSDoleProtocol *protocol, TSNetResult result) {
    switch (protocol->getTag()) {
        case TAG_NET_INVENTORY:
        case TAG_NET_PURCHASE: {
            TSDoleNetProgressLayer *progressLayer = (TSDoleNetProgressLayer *)this->getChildByTag(TAG_NET_PROGRESS);
            if (progressLayer) {
                progressLayer->activityStop(Device::getResString(Res::Strings::CM_NET_WARNING_NETWORK_UNSTABLE));
            }
        } break;
        default:
            TSNetToastLayer::show(this);
            break;
    }
}

//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
void CharacterRecording::onRequestCanceled(TSDoleProtocol *protocol) {
	CC_SAFE_RELEASE_NULL(protocol);
}
//#endif

void CharacterRecording::onFinishedPurchaseProcess(bool success, char* item)
{
    if(success) {
        selectedLockedCharacter = nullptr;
        selectedLockedBackgroundKey = nullptr;
        
        backgroundScrollView->reloadData();
        characterScrollView->reloadData();
    }
    removeTransparentLayer();
    closeUnlockTalkbox();
}


void CharacterRecording::doneMenuCallback(Ref* pSender)
{
    if (pSender != nullptr) {
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Tap.wav", false);
    }
    
    this->setViewMode(RecordingViewMode::RecordingComplete);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	AppDelegate* app = (AppDelegate*)Application::getInstance();
	if(app != nullptr) {
		const char* bgmFileName = nullptr;
		auto soundString = UserDefault::getInstance()->getStringForKey(selectedSoundKey->getCString());
		rapidjson::Document soundInfo;
		soundInfo.Parse<0>(soundString.c_str());
		if (soundInfo.HasMember("sound_filename")) {
			bgmFileName = soundInfo["sound_filename"].GetString();
		}
		app->changeVoiceRecordStatus(ANDROID_RECORD_SAVE, bgmFileName, -1);
	}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
    VoiceRecorder *voiceRecorder = [VoiceRecorder sharedRecorder];
    [voiceRecorder stopRecord];
    
    float volume = UserDefault::getInstance()->getFloatForKey(UDKEY_FLOAT_VOLUME);
    CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(volume);
#endif
#endif
    
}

void CharacterRecording::rerecordMenuCallback(Ref* pSender)
{
    if (pSender != nullptr) {
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Tap.wav", false);
    }
    
    Director::getInstance()->pause();
    PopupLayer *modal;
    if (Application::getInstance()->getCurrentLanguage() != LanguageType::JAPANESE) {
        modal = PopupLayer::create(POPUP_TYPE_WARNING,
                                   Device::getResString(Res::Strings::CC_RECORDING_POPUP_WARNING_RERECORDING),//"Delete all recorded contents and start new recording.",
                                   Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL),//"Cancel",
                                   Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK));//"OK");
    } else {
        modal = PopupLayer::create(POPUP_TYPE_WARNING,
                                   Device::getResString(Res::Strings::CC_RECORDING_POPUP_WARNING_RERECORDING),//"Delete all recorded contents and start new recording.",
                                   Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_NO),//"Cancel",
                                   Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_YES));//"OK");
    }
    modal->setTag(TAG_POPUP_RERECORDING);
    modal->setDelegate(this);
    this->addChild(modal, 1000);
    
    if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()) {
        CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    }
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	AppDelegate* app = (AppDelegate*)Application::getInstance();
	if(app != nullptr) {
		app->changeVoicePlayStatus(ANDROID_PLAY_PAUSE, voiceFileName->getCString(), TAG_RECORDING);
	}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
    VoiceRecorder *voiceRecorder = [VoiceRecorder sharedRecorder];
    [voiceRecorder pause];
#endif
#endif
}

void CharacterRecording::saveMenuCallback(Ref* pSender)
{
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    
    AppDelegate* app = (AppDelegate*)Application::getInstance();
    app->createStorySuccess();
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#   ifndef MACOS
    [MixpanelHelper logCreateStorysuccess];
#   endif
#endif
    
    if (bgmFileName != nullptr)
        CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    
    if (pSender != nullptr) {
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Tap.wav", false);
    }
    
    playing = false;
    this->saveRecordData(true);
    this->clearVoiceRecorder();

//    this->removeAllChildren();
    auto director = Director::getInstance();
    director->popSceneWithTransition([](Scene *scene) {
        return TransitionProgressOutIn::create(0.4f, scene);
    });
}


void CharacterRecording::popupButtonClicked(PopupLayer *popup, cocos2d::Ref *obj)
{
    Director::getInstance()->resume();
    auto clickedButton = (MenuItem*)obj;
    int tag = clickedButton->getTag();
    int popupTag = popup->getTag();
    
    switch (popupTag) {
        case TAG_POPUP_BACK_TO_SET_POSITION: {
            if (tag == 0) {
                log("cancel back to set position");
            } else {
                this->deleteVoiceFile();
                this->clearRecordData();
                this->restoreCharacterProperty();
                this->setProgress(0.0);
                this->setViewMode(RecordingViewMode::SetPosition);
            }
        } break;
        case TAG_POPUP_RERECORDING: {
            if (paused) {
                this->resumePlay();
            }
            if (tag == 0) {
                log("cancel rerecording");
                if (bgmFileName != nullptr)
                    CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
            } else {
                if (bgmFileName != nullptr)
                    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
                
                this->unschedule(schedule_selector(CharacterRecording::onPlayTouch));
                
                while (playTouchTimeStampIndex < touchData->count()) {
                    __Dictionary *touchInfo = (__Dictionary *)touchData->getObjectAtIndex(playTouchTimeStampIndex);
//                    __Double *timeStamp = (__Double *)touchInfo->objectForKey("timestamp");
//                    if (timeStamp->getValue() < playTouchElapsedTime) {
                        __Integer *touchID = (__Integer *)touchInfo->objectForKey("touch_id");
                        __Integer *touchStatus = (__Integer *)touchInfo->objectForKey("touch_status");
                        
                        __Float *posX = (__Float *)touchInfo->objectForKey("location_x");
                        __Float *posY = (__Float *)touchInfo->objectForKey("location_y");
                        
                        Touch *touch = (Touch *)playingTouches->objectForKey(touchID->getValue());
                        if (touch == NULL) {
                            touch = new Touch();
                            touch->autorelease();
                            playingTouches->setObject(touch, touchID->getValue());
                        }
                        touch->setTouchInfo(touchID->getValue(), posX->getValue(), posY->getValue());
                        
                        TouchStatus status = (TouchStatus)touchStatus->getValue();
                        
//                        log("play touch(%f, %f) ! type: %d", posX->getValue(), posY->getValue(), touchStatus->getValue());
                        
                        switch (status) {
                            case TouchStatus::Down:
                                this->onTouchBegan(touch, nullptr);
                                break;
                            case TouchStatus::Up:
                            {
                                this->onTouchEnded(touch, nullptr);
                                playingTouches->removeObjectForKey(touchID->getValue());
                            }
                                break;
                            case TouchStatus::Move:
                                this->onTouchMoved(touch, nullptr);
                                break;
                            case TouchStatus::Cancel:
                            {
                                this->onTouchCancelled(touch, nullptr);
                                playingTouches->removeObjectForKey(touchID->getValue());
                            }
                                break;
                            default:
                                break;
                        }
                        playTouchTimeStampIndex++;
//                    } else {
//                        break;
//                    }
                }
                
                playTouchTimeStampIndex = 0;
                playTouchElapsedTime = 0;
                playedTime = 0;
                
                playing = false;
                paused = false;
                
                touchNumber = 0;
                touches.clear();
                
                this->deleteVoiceFile();
                this->clearRecordData();
                this->restoreCharacterProperty();
                this->setProgress(0.0);
                progressBar->setVisible(false);
                this->setViewMode(RecordingViewMode::Recording);
            }
        } break;
        case TAG_POPUP_REUSE_ITEM: {
            if (tag == 0) { // cancel
                this->clearPrevData();
                this->showCharacterScrollView();
                MenuItemImage* characterItem = (MenuItemImage*)bottomMenu->getChildByTag(MENU_ITEM);
                characterItem->setNormalImage(Sprite::create("btn_home_item_selected.png"));
                openCharacterItem = true;
            } else { // ok
                // close pop up
            }
        } break;
        case TAG_POPUP_PURCHASE_NEEDLOGIN: {
            if (tag == 0) {
                log("cancelButton clicked!");
                this->cancelExecutePurchase(false);
            } else {
                log("okButton clicked!!");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
                AppDelegate* app = (AppDelegate*)Application::getInstance();
                if(app != nullptr) {
                    app->showPlatformView(ANDROID_LOGIN, TAG_RECORDING);
                }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#ifdef UI_TYPE_IOS8
                TSAccountLoginViewController *loginVC = [[TSAccountLoginViewController alloc] init];
                loginVC.originLayer = TSOriginLayerCharacterRecording;
                loginVC.recallType = TSRecallCharRECUnlockItem;
                [UIViewController replaceRootViewControlller:loginVC];
//                [loginVC release];
#else//UI_TYPE_IOS8
                UIViewController *viewController = nil;
                if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                    viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
                } else {
                    viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
                }
                NSLog(@"viewController screen frame : %@",NSStringFromCGRect(viewController.view.frame));
                TSAccountViewController *accountViewController = [[TSAccountViewController alloc] initWithFirstRun:false];
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                    accountViewController.modalPresentationStyle = UIModalPresentationFormSheet;
                }
                accountViewController.originLayer = TSOriginLayerCharacterRecording;
                accountViewController.recallType = TSRecallCharRECUnlockItem;
                if(IS_IOS8 && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                    accountViewController.preferredContentSize = CGSizeMake(1024, 768);
                }
                [viewController presentViewController:accountViewController animated:YES completion:nil];
#endif//UI_TYPE_IOS8
#endif
            }
        } break;
        case TAG_POPUP_NOTENOUGH_COIN: {
            if (tag == 0) {
                log("cancelButton clicked!");
                this->cancelExecutePurchase(false);
            } else {
                log("okButton clicked!!");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
                AppDelegate* app = (AppDelegate*)Application::getInstance();
                if(app != nullptr) {
                    app->showPlatformView(ANDROID_DOLE_COIN, TAG_RECORDING_SCENE);
                }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#ifdef UI_TYPE_IOS8
                TSDoleCoinInfoViewController *doleCoinVC = [[TSDoleCoinInfoViewController alloc] init];
                doleCoinVC.originLayer = TSOriginLayerCharacterRecording;
                [UIViewController replaceRootViewControlller:doleCoinVC];
//                [doleCoinVC release];
#else//UI_TYPE_IOS8
                BOOL isAnimated = YES;
//                if (animated == false) {
//                    isAnimated = NO;
//                }
                UIViewController *viewController = nil;
                if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                    viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
                } else {
                    viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
                }
                NSLog(@"viewController screen frame : %@",NSStringFromCGRect(viewController.view.frame));
                TSDoleCoinViewController *doleCoinViewController = [[TSDoleCoinViewController alloc] init];
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                    doleCoinViewController.modalPresentationStyle = UIModalPresentationFormSheet;
                }
                doleCoinViewController.originLayer = TSOriginLayerCharacterRecording;
                if(IS_IOS8 && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                    doleCoinViewController.preferredContentSize = CGSizeMake(1024, 768);
                }
                [viewController presentViewController:doleCoinViewController animated:isAnimated completion:nil];
#endif//UI_TYPE_IOS8
#endif
            }
        } break;
        case TAG_POPUP_UNLOCKITEM: {
            if (tag == 2) {
                this->doExecutePurchase();
            } else if (tag == 0) {
                log("cancelButton clicked!");
                this->cancelExecutePurchase(false);
            } else {
                log("okButton clicked!!");
            }
            
        } break;
        default: {
            if (tag == 0) {
                log("cancelButton clicked!");
            } else {
                log("okButton clicked!!");
            }
        } break;
    }
}

void CharacterRecording::onEnter()
{
    Layer::onEnter();
    TSBGMManager::getInstance()->stop();

    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(CharacterRecording::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(CharacterRecording::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(CharacterRecording::onTouchEnded, this);
    listener->onTouchCancelled = CC_CALLBACK_2(CharacterRecording::onTouchCancelled, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    auto keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyReleased = CC_CALLBACK_2(CharacterRecording::onKeyReleased, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(keyboardListener, this);
}

void CharacterRecording::onExit()
{
    _eventDispatcher->removeEventListenersForTarget(this);
    Layer::onExit();
}

void CharacterRecording::recordTouch(cocos2d::Touch *touch, TouchStatus status)
{
    if (touchData->count() == 0) {
        doneMenuItem->setVisible(true);
        disabledDoneMenuItem->setVisible(false);
        disabledDoneMenuItem->setEnabled(false);

        progressBar->setVisible(true);
        
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    	AppDelegate* app = (AppDelegate*)Application::getInstance();
    	if(app != nullptr) {
    		app->changeVoiceRecordStatus(ANDROID_RECORD_START, voiceFileName->getCString(), -1);
    	}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
        NSString *fileName = [NSString stringWithUTF8String: voiceFileName->getCString()];
        VoiceRecorder *voiceRecorder = [VoiceRecorder sharedRecorder];
        [voiceRecorder startRecord:fileName];
        
        CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(DEFAULT_VOLUME_BACKGROUND_SOUND);
#endif
#endif
    }
    
    std::chrono::milliseconds now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch());

    if (status == TouchStatus::Down) {
        if (recordingTouchNum == 0) {
            lastTouchTime = now;
            recording = true;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        	AppDelegate* app = (AppDelegate*)Application::getInstance();
        	if(app != nullptr) {
        		app->changeVoiceRecordStatus(ANDROID_RECORD_RESUME, nullptr, -1);
        	}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
            VoiceRecorder *voiceRecorder = [VoiceRecorder sharedRecorder];
            [voiceRecorder resumeRecord];
#endif
#endif
        }
        recordingTouchNum++;
    }
    
    std::chrono::duration<double> diff = now - lastTouchTime;
//    log("diff : %f", diff.count());
    double diffInMili = ((double)diff.count() * 1000.0);
    
    if (recordingTouchNum > 0) {
        int touchID = touch->getID();
        cocos2d::Point location = touch->getLocationInView();
        
        accumulatedTouchTimeStamp = lastTouchTimeStamp+diffInMili;
        
        __Dictionary *touchInfo = __Dictionary::create();
        touchInfo->setObject(__Float::create(location.x), "location_x");
        touchInfo->setObject(__Float::create(location.y), "location_y");
        touchInfo->setObject(__Integer::create(touchID), "touch_id");
        touchInfo->setObject(__Integer::create((int)status), "touch_status");
        touchInfo->setObject(__Double::create(accumulatedTouchTimeStamp), "timestamp");
        
        touchData->addObject(touchInfo);
//        log("write touch(%f, %f) ! acc time : %f", location.x, location.y, accumulatedTouchTimeStamp);
    }

    if (status == TouchStatus::Up){
        recordingTouchNum--;
        if (recordingTouchNum == 0) {
            lastTouchTimeStamp = accumulatedTouchTimeStamp;
            recording = false;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        	AppDelegate* app = (AppDelegate*)Application::getInstance();
        	if(app != nullptr) {
        		app->changeVoiceRecordStatus(ANDROID_RECORD_PAUSE, nullptr, -1);
        	}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
            VoiceRecorder *voiceRecorder = [VoiceRecorder sharedRecorder];
            [voiceRecorder pauseRecord];
#endif
#endif
        }
    }
}

void CharacterRecording::playTouch()
{
    this->schedule(schedule_selector(CharacterRecording::onPlayTouch), (1.0/MAX_PLAY_FPS));
}

void CharacterRecording::onPlayTouch(float df)
{
    if (!playing) {
        return;
    }
    
//    log ("onPlayTouch : %f", df);
    double elapsedMs = df * 1000;
    playTouchElapsedTime += elapsedMs;
    
    if (playTouchTimeStampIndex == 0) {
        playProgressBar->setVisible(true);
    }
    
    while (playTouchTimeStampIndex < touchData->count()) {
        __Dictionary *touchInfo = (__Dictionary *)touchData->getObjectAtIndex(playTouchTimeStampIndex);
        __Double *timeStamp = (__Double *)touchInfo->objectForKey("timestamp");
        if (timeStamp->getValue() < playTouchElapsedTime) {
            __Integer *touchID = (__Integer *)touchInfo->objectForKey("touch_id");
            __Integer *touchStatus = (__Integer *)touchInfo->objectForKey("touch_status");

            __Float *posX = (__Float *)touchInfo->objectForKey("location_x");
            __Float *posY = (__Float *)touchInfo->objectForKey("location_y");
            
            Touch *touch = (Touch *)playingTouches->objectForKey(touchID->getValue());
            if (touch == NULL) {
                touch = new Touch();
                touch->autorelease();
                playingTouches->setObject(touch, touchID->getValue());
            }
            touch->setTouchInfo(touchID->getValue(), posX->getValue(), posY->getValue());
            
            TouchStatus status = (TouchStatus)touchStatus->getValue();
            
//            log("play touch(%f, %f) ! type: %d", posX->getValue(), posY->getValue(), touchStatus->getValue());
            
            switch (status) {
                case TouchStatus::Down:
                    this->onTouchBegan(touch, nullptr);
                    break;
                case TouchStatus::Up:
                {
                    this->onTouchEnded(touch, nullptr);
                    playingTouches->removeObjectForKey(touchID->getValue());
                }
                    break;
                case TouchStatus::Move:
                    this->onTouchMoved(touch, nullptr);
                    break;
                case TouchStatus::Cancel:
                {
                    this->onTouchCancelled(touch, nullptr);
                    playingTouches->removeObjectForKey(touchID->getValue());
                }
                    break;
                default:
                    break;
            }
            playTouchTimeStampIndex++;
        } else {
            break;
        }
    }
    
    if ( playTouchTimeStampIndex >= touchData->count() ) {
//        __Dictionary *touchInfo = (__Dictionary *)touchData->getLastObject();
//        __Double *lastTimeStamp = (__Double *)touchInfo->objectForKey("timestamp");
//        log ("touchPlay end! duration : %f", lastTimeStamp->getValue());
        this->unschedule(schedule_selector(CharacterRecording::onPlayTouch));
        this->finishPlaying();
    }
}

void CharacterRecording::onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	if(keyCode == cocos2d::EventKeyboard::KeyCode::KEY_BACKSPACE) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		if(getChildByTag(TAG_GUIDE_LAYER) != NULL) {
			removeChildByTag(TAG_GUIDE_LAYER);
		} else if(getChildByTag(TAG_POPUP_BACK_TO_SET_POSITION) != NULL) {
			removeChildByTag(TAG_POPUP_BACK_TO_SET_POSITION);
		} else if(getChildByTag(TAG_POPUP_RERECORDING) != NULL) {
			removeChildByTag(TAG_POPUP_RERECORDING);
		} else if(isPlatformShown) {
			AppDelegate* app = (AppDelegate*)Application::getInstance();
			isPlatformShown = !app->hidePlatformView();
		} else if(openBgItem) {
			hideBottomScrollView();

            MenuItemImage* bgItem = (MenuItemImage*)bottomMenu->getChildByTag(MENU_BG);
            bgItem->setNormalImage(Sprite::create("btn_home_bg_normal.png"));
            bgItem->setEnabled(true);
                openBgItem = !openBgItem;
		} else if(openCharacterItem) {
			hideBottomScrollView();
				
            MenuItemImage* characterItem = (MenuItemImage*)bottomMenu->getChildByTag(MENU_ITEM);
            characterItem->setNormalImage(Sprite::create("btn_home_item_normal.png"));
            characterItem->setEnabled(true);
            openCharacterItem = !openCharacterItem;
		} else if(openSoundItem) {
			hideBottomScrollView();

            MenuItemImage* soundItem = (MenuItemImage*)bottomMenu->getChildByTag(MENU_MUSIC);
            soundItem->setNormalImage(Sprite::create("btn_set_position_music_normal.png"));
            soundItem->setEnabled(true);
            openSoundItem = !openSoundItem;
		} else {
			btnBackCallback(nullptr);
		}
#endif
	}
}

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
void CharacterRecording::updateCustomCharacater(int index, const char* path)
{
	std::string characterKey = Util::saveCustomCharacterData(index, path);
	characterPhotoSaved(characterKey);

}

void CharacterRecording::updateCustomBackground(const char* path)
{
	std::string bgKey = Util::saveCustomBackgroundData(path);
	bgPhotoSaved(bgKey);
}

void CharacterRecording::checkExecutePurchaseWithLogin()
{
	if(_homeLayer != nullptr) {
		_homeLayer->updateDoleCoinAndroid();
	}

	checkRecall(ANDROID_UNLOCK_ITEM_WITH_LOGIN);

	if(_homeLayer != nullptr) {
		_homeLayer->doleGetInventoryListProtocol();
	}
}
#endif

void CharacterRecording::characterPhotoSaved(std::string characterKey)
{
    characterScrollView->addCharacter(characterKey);
    if (_delegate != NULL) {
        _delegate->characterPhotoSavedInRecordScene(characterKey);
    }
}

void CharacterRecording::bgPhotoSaved(std::string bgKey)
{
    backgroundScrollView->addBackground(bgKey);
    if (_delegate != NULL) {
        _delegate->bgPhotoSavedInRecordScene(bgKey);
    }
}

void CharacterRecording::cameraScenePopped()
{
    cameraScene = nullptr;
}

Label *CharacterRecording::createTitle(int chapterIndex)
{
    // TODO: change text design
    __String *chapterTitle = NULL;
    
    switch (chapterIndex) {
        case 1:
            chapterTitle = __String::create(Device::getResString(Res::Strings::CC_PLAY_PAGE_TITLE_INTRO));//"Intro"
            break;
        case 2:
        case 3:
        case 4:
            chapterTitle = __String::createWithFormat(Device::getResString(Res::Strings::CC_PLAY_PAGE_TITLE_PAGE_N), chapterIndex);//"Page %d"
            break;
        case 5:
            chapterTitle = __String::create(Device::getResString(Res::Strings::CC_PLAY_PAGE_TITLE_FINAL));//"Final"
            break;
        default:
            return NULL;
    }
    
    auto pageTitle = cocos2d::Label::createWithSystemFont(chapterTitle->getCString(), FONT_NAME_BOLD, MAP_PLAY_TITLE_TEXT_SIZE);
    pageTitle->setDimensions(MAP_PLAY_TITLE_SIZE.width, MAP_PLAY_TITLE_SIZE.height);
    pageTitle->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::CENTER);
    pageTitle->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    pageTitle->setColor(cocos2d::Color3B(255, 255, 255));
    
    //    pageTitle->enableOutline(Color4B(255,255,255,255), MAP_PLAY_TITLE_TEXT_STORKE_WIDTH);
    //    pageTitle->enableShadow(Color4B(0,0,0,255*0.8), LABEL_SHADOW_DEFAULT_SIZE); //LABEL_SHADOW_PLAY_SIZE
    //    this->addChild(pageTitle, 15);
    
    return pageTitle;
}
