//
//  LabelExtension.cpp
//  bobby
//
//  Created by oasis on 2014. 4. 9..
//
//

#include "LabelExtension.h"
#include "utf8.h"

USING_NS_CC;

LabelExtension* LabelExtension::create(const std::string& text, const std::string& fontName,
		float fontSize, const Size& dimensions /* = Size::ZERO */, TextHAlignment hAlignment /* = TextHAlignment::LEFT */,
		TextVAlignment vAlignment /* = TextVAlignment::TOP */, bool underline /* = false */, bool includeFontPadding /* = true */)
{
    auto ret = new LabelExtension(nullptr,hAlignment,vAlignment);
    
    if (ret)
    {
        do
        {
            if (FileUtils::getInstance()->isFileExist(fontName))
            {
                TTFConfig ttfConfig(fontName.c_str(),fontSize,GlyphCollection::DYNAMIC);
                if (ret->setTTFConfig(ttfConfig))
                {
                    break;
                }
            }
            
            FontDefinition fontDef;
            fontDef._fontName = fontName;
            fontDef._fontSize = fontSize;
            fontDef._dimensions = dimensions;
            fontDef._alignment = hAlignment;
            fontDef._vertAlignment = vAlignment;
            fontDef._underline = underline;
            fontDef._includeFontPadding = includeFontPadding;
            
            ret->setFontDefinition(fontDef);
        } while (0);
        
        ret->setDimensions(dimensions.width,dimensions.height);
        ret->setString(text);
        ret->autorelease();
    }
    
    return ret;
}

void LabelExtension::sizeToFitWidth(float width)
{
    float fontSize = this->getSystemFontSize();
    float fontAdjustmentStep = 1.0f;

    while (this->getContentSize().width > width)
    {
        fontSize -= fontAdjustmentStep;
        this->setSystemFontSize(fontSize);
        
        if (fontSize<5) {
            this->setWidth(137);
            break;
        }
    }
    
}

void LabelExtension::ellipsis(const char* fontName, int fontSize)
{
    int orgStrWidth = Device::getTextWidth(this->getString().c_str(), fontName, fontSize);
    int contentWidth = this->getContentSize().width;
    if (orgStrWidth > contentWidth) {
        std::vector<std::string> results;
        std::string strTmp = this->getString().c_str();
        
        char* str = (char*)strTmp.c_str();
        char* str_i = str;
        char* end = str+strlen(str)+1;
        
        unsigned char symbol[5] = {0,0,0,0,0};
        do {
            memset(symbol, 0, 5);
            uint32_t code = utf8::next(str_i,end);
            if (code == 0) {
                continue;
            }
            utf8::append(code, symbol);
            std::stringstream s;
            s << symbol;
            results.push_back((char*)symbol);
            
        } while(str_i < end);
        
        int ellipsisWidth = Device::getTextWidth("..", fontName, fontSize);
        std::string appendstr;
        std::string resultStr;
        for (int i=0;i <results.size(); i++) {
            auto currentWord = results.at(i);
            appendstr.append(results.at(i));
            int stringWidth = Device::getTextWidth(appendstr.c_str(), fontName, fontSize);
            
            if (stringWidth < (contentWidth-ellipsisWidth)) {
                resultStr.append(results.at(i));
            } else {
                resultStr.append("..");
                log("resultStr : %s", resultStr.c_str());
                this->setString(resultStr);
                break;
            }
        }
    }
}

void LabelExtension::addEllipsis()
{
    log("content size width : %f",getContentSize().width);

    auto text = this->getString();
    auto ellipsis = "...";
    Device::TextAlign align = Device::TextAlign::LEFT; // center vertical, left horizontal

    if (TextVAlignment::TOP == _fontDefinition._vertAlignment)
	{
		align = (TextHAlignment::CENTER == _fontDefinition._alignment) ? Device::TextAlign::TOP
		: (TextHAlignment::LEFT == _fontDefinition._alignment) ? Device::TextAlign::TOP_LEFT : Device::TextAlign::TOP_RIGHT;
	}
	else if (TextVAlignment::CENTER == _fontDefinition._vertAlignment)
	{
		align = (TextHAlignment::CENTER == _fontDefinition._alignment) ? Device::TextAlign::CENTER
		: (TextHAlignment::LEFT == _fontDefinition._alignment) ? Device::TextAlign::LEFT : Device::TextAlign::RIGHT;
	}
	else if (TextVAlignment::BOTTOM == _fontDefinition._vertAlignment)
	{
		align = (TextHAlignment::CENTER == _fontDefinition._alignment) ? Device::TextAlign::BOTTOM
		: (TextHAlignment::LEFT == _fontDefinition._alignment) ? Device::TextAlign::BOTTOM_LEFT : Device::TextAlign::BOTTOM_RIGHT;
	}
	else
	{
		CCASSERT(false, "Not supported alignment format!");
		return;
	}

    int index = Device::getEllipsisIndexForText(text.c_str(), ellipsis, _fontDefinition, align);
    if(index >= 0) {
    	text = text.substr(0, index);
    	text = text.append(ellipsis);
    	this->setString(text);
    }
}

void LabelExtension::addEllipsisWidthDigitNum(int digit)
{
    auto text = this->getString();
    if (text.length() > digit) {
        text = text.substr(0, digit);
        text = text.append("...");
        this->setString(text);
    }
}

int LabelExtension::getTextWidth()
{
    auto text = this->getString();
    int width = Device::getTextWidth(text.c_str(), _fontDefinition._fontName.c_str(), _fontDefinition._fontSize);
    return width;
}

