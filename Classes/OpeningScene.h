//
//  OpeningScene.h
//  bobby
//
//  Created by Dongwook, Kim on 14. 7. 10..
//
//

#ifndef __bobby__OpeningScene__
#define __bobby__OpeningScene__

#include <iostream>
#include "cocos2d.h"
#include <cocosbuilder/CocosBuilder.h>
#include <chrono>

USING_NS_CC;

class Opening;

class OpeningDelegate
{
public:
    virtual ~OpeningDelegate() {}
    virtual void onOpeningFinished(Opening* opening) = 0;
};

class Opening : public LayerColor, cocosbuilder::CCBAnimationManagerDelegate
{
public:
    static Opening* createOpening(__String *title, __String *author);
    bool init(__String *title, __String *author);
    void playBackgroundMusic();
    
    OpeningDelegate* getDelegate() { return _delegate; }
    void setDelegate(OpeningDelegate* pDelegate) { _delegate = pDelegate; }
    void completedAnimationSequenceNamed(const char *name);
    
    void initAnimation();
    void startAnimation();
    void pauseAnimation();
    void resumeAnimation();

    bool isAnimationStarted() { return animationStarted; };
    bool isAnimationPaused() { return animationPaused; };
    
    float getTotalDuration();
private:
    Node *createTitleNode(__String *title, __String *author);

    OpeningDelegate *_delegate;

    Node *titleNode;
    
    Sprite *curtain;
    Sprite *bgSprite;
    __String *title;
    __String *author;
    
    float curtainAnimationDuration;
    
    Action *bgAction;
    Action *curtainAction;
    Action *finishAction;

    void finishedOpening();
    
    void stopAction();
    void pauseAllChildren(Node *node);
    void resumeAllChildren(Node *node);

    void runAnimationDurationOffset(float durationOffset);
    
    std::chrono::milliseconds animationStartTime;
    double elapsedAnimationTime;
    double accElapsedAnimationTime;

    bool animationStarted;
    bool animationPaused;
};


#endif /* defined(__bobby__OpeningScene__) */
