//
//  TableViewExtension.h
//  bobby
//
//  Created by Dongwook, Kim on 14. 4. 10..
//
//

#ifndef __bobby__TableViewExtension__
#define __bobby__TableViewExtension__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class TableViewExtensionDelegate
{
public:
    virtual void tableCellTouched(TableView* table, TableViewCell* cell, Touch *pTouch, Event *pEvent) = 0;
};

class TableViewExtension : public TableView
{
public:
    TableViewExtension():TableView(){}
    
    static TableViewExtension* create(TableViewDataSource* dataSource, cocos2d::Size size);
    
    virtual void onTouchEnded (Touch *pTouch, Event *pEvent) override;
    TableViewExtensionDelegate* getExtensionDelegate() { return _tableViewExtensionDelegate; }
    void setExtensionDelegate(TableViewExtensionDelegate* pDelegate) { _tableViewExtensionDelegate = pDelegate; }

protected:
    TableViewExtensionDelegate *_tableViewExtensionDelegate;
};


#endif /* defined(__bobby__TableViewExtension__) */
