//
//  PopupLayer.cpp
//  Popup
//
//  Created by oasis on 2014. 5. 9..
//
//

#include "PopupLayer.h"
#include "Define.h"
#include "ResourceManager.h"
#include "Util.h"

USING_NS_CC;

#define TAG_LABEL_CELLTITLE             1010101010
#define TAG_CANCEL_BUTTON             	0
#define TAG_CONTENT_LAYER             	1212121212
#define TAG_MENU_BUTTON             	1313131313

PopupLayer::PopupLayer():_delegate(nullptr),_pendingClose(false),_preventClose(false) {}

PopupLayer::~PopupLayer()
{
    if (popupType == POPUP_TYPE_SHARED || popupType == POPUP_TYPE_YEARS) listArr->release();
}

PopupLayer* PopupLayer::create(popup_type type, const std::string &message, const std::string &btn1Name, const std::string &btn2Name)
{
    PopupLayer *layer = new PopupLayer();
    if (layer && layer->initWithMessage(type, message, btn1Name, btn2Name))
    {
        layer->autorelease();
        return layer;
    }
    CC_SAFE_DELETE(layer);
    return nullptr;
}
//#ifdef MACOS
//int PopupLayer::getMessageLabelHeight(const char* message, const char* fontName, int fontSize, int width)
//{
//    __String secondLineTitle = message;
//    auto titleArr = secondLineTitle.componentsSeparatedByString(" ");
//    
//    int lineNum = 0;
//    __String lineStr = "";
//    for (int i = 0; i < titleArr->count(); i++) {
//        auto currentWord =((__String *)titleArr->getObjectAtIndex(i))->getCString();
//        lineStr.append(currentWord);
//        lineStr.append(" ");
//        
//        int labelWidth = Device::getTextWidth(lineStr.getCString(), fontName, fontSize);
//        if (labelWidth > width) {
//            lineNum++;
//            log("seconLine : %s", lineStr.getCString());
//            lineStr = "";
//            
//            if (i == titleArr->count() - 1) {
//                int lastWordWidth = Device::getTextWidth(currentWord, fontName, fontSize);
//                int lastLineNum = (lastWordWidth%width == 0)?lastWordWidth/width:lastWordWidth/width+1;
//                lineNum = lineNum + lastLineNum;
//                
//            }
//        }
//        
//    }
//    return lineNum + 1;
//}
//#endif

bool PopupLayer::initWithMessage(popup_type type, const std::string &message, const std::string &btn1Name, const std::string &btn2Name)
{
    if ( !LayerColor::initWithColor(Color4B(0, 0, 0, 50)) )
    {
        return false;
    }
	////////////////////////
    
    popupType = type;
	cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();

    auto contentsLayer = Layer::create();
    contentsLayer->setTag(TAG_CONTENT_LAYER);
    cocos2d::extension::Scale9Sprite *bottomBg = NULL;
    if (popupType == POPUP_TYPE_WARNING || popupType == POPUP_TYPE_COINS) {
        auto topBg = Sprite::create("popup_no_title.png");
        topBg->setAnchorPoint(Point(0,0));
        
        bottomBg = this->getBottomBg();
        
        Sprite *iconImage = NULL;
        if (popupType == POPUP_TYPE_WARNING) {
            iconImage = Sprite::create("popup_warning.png");
        } else {
            iconImage = Sprite::create("popup_coin.png");
        }
        iconImage->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        
        float messageLabelHeight = 0;
#ifdef MACOS
        //    int lineNum = this->getMessageLabelHeight(message.c_str(), FONT_NAME, 34, topBg->getContentSize().width - 200);
        //    log("lineNum %i", lineNum);
        //    messageLabelHeight = 43*lineNum;
        messageLabelHeight = 75;
#endif
        // size의 y가 설정되어 있지 않으면 mac에서 제대로 표시가 되지 않음... 적절한 수치로 변경 필요 (jmjeong)
        Label *messageLabel = Label::createWithSystemFont(message, FONT_NAME, POPUP_MESSEGE_FONTSIZE,Size(topBg->getContentSize().width - (18+18), messageLabelHeight),TextHAlignment::CENTER, TextVAlignment::TOP);
        messageLabel->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        messageLabel->setColor(Color3B(0x78,0x78,0x78));
        messageLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
        //    log("messageLabelHeight : %f", messageLabel->getContentSize().height);
        //
        //    log("getnumLine : %d", messageLabel->getStringNumLines());
        
        Sprite *background = this->getMiddleBackground(messageLabel->getContentSize().height + POPUP_TOP_PADDING - topBg->getContentSize().height + POPUP_MIDDLE_PADDING + iconImage->getContentSize().height + POPUP_BOTTOM_PADDING, Point(0,bottomBg->getContentSize().height));
        
        contentsLayer->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        contentsLayer->setContentSize(Size(topBg->getContentSize().width,background->getContentSize().height+topBg->getContentSize().height+bottomBg->getContentSize().height));
        contentsLayer->setPosition(Point(visibleSize.width/2-contentsLayer->getContentSize().width/2,visibleSize.height/2-contentsLayer->getContentSize().height/2));
        this->addChild(contentsLayer);
        
        topBg->setPosition(Point(0,contentsLayer->getContentSize().height-topBg->getContentSize().height));
        iconImage->setPosition(Point((contentsLayer->getContentSize().width/2)-(iconImage->getContentSize().width/2),contentsLayer->getContentSize().height - POPUP_TOP_PADDING - iconImage->getContentSize().height));
        messageLabel->setPosition(Point(contentsLayer->getContentSize().width/2 - messageLabel->getContentSize().width/2, contentsLayer->getContentSize().height- POPUP_TOP_PADDING - iconImage->getContentSize().height - POPUP_MIDDLE_PADDING - messageLabel->getContentSize().height));
        
        contentsLayer->addChild(background, 0);
        contentsLayer->addChild(topBg);
        contentsLayer->addChild(bottomBg);
        contentsLayer->addChild(iconImage);
        contentsLayer->addChild(messageLabel);
        
    } else if (popupType == POPUP_TYPE_SHARED) {
        auto *topBg = this->getTopBg();
        
        auto popName = this->getTopLabel(Device::getResString(Res::Strings::CC_JOURNEYMAP_POPUP_TITLE_SHARE_LIST));//"Share"
        topBg->addChild(popName);
        
        bottomBg = this->getBottomBg();
        
        listArr = __Array::create();
        listArr->retain();
        listArr->addObject(__String::create(Device::getResString(Res::Strings::CC_JOURNEYMAP_POPUP_SHARELIST_FACEBOOK)));//"Facebook"
        listArr->addObject(__String::create(Device::getResString(Res::Strings::CC_JOURNEYMAP_POPUP_SHARELIST_YOUTUBE)));//"Youtube"
//        listArr->addObject(__String::create("Vimeo"));
        
        TableView* tableView = TableView::create(this, POPUP_SHARED_TABLEVIEW_SIZE);
        tableView->setAnchorPoint(Point(0.5f, 0.5f));
        tableView->setPosition(Point(0,bottomBg->getContentSize().height));
        tableView->setDirection(ScrollView::Direction::VERTICAL);
        
        tableView->setDelegate(this);
        tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
        
        tableView->reloadData();
        contentsLayer->addChild(tableView, 5);
//        this->setTouchEnabled(true);
        tableView->setBounceable(false);
        
        Sprite *background = this->getMiddleBackground(POPUP_SHARED_MIDDLE_HEIGHT, Point(0,bottomBg->getContentSize().height));
        
        contentsLayer->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        contentsLayer->setContentSize(Size(topBg->getContentSize().width,background->getContentSize().height+topBg->getContentSize().height+bottomBg->getContentSize().height));
        contentsLayer->setPosition(Point(visibleSize.width/2-contentsLayer->getContentSize().width/2,visibleSize.height/2-contentsLayer->getContentSize().height/2));
        this->addChild(contentsLayer);
        
        topBg->setPosition(Point(0,contentsLayer->getContentSize().height-topBg->getContentSize().height));
        contentsLayer->addChild(background, 0);
        contentsLayer->addChild(topBg);
        contentsLayer->addChild(bottomBg);
        
    } else if (popupType == POPUP_TYPE_YEARS) {
        auto *topBg = this->getTopBg();
        
        auto popName = this->getTopLabel("Set birth year");
        topBg->addChild(popName);
        
        bottomBg = this->getBottomBg();
        
        listArr = __Array::create();
        listArr->retain();
        
        for (int i=1900; i < 2100; i++) {
            auto year = __String::createWithFormat("%02d", i);
            year->retain();
            listArr->addObject(year);
            year->release();
        }
        
        TableView* tableView = TableView::create(this, POPUP_SHARED_TABLEVIEW_SIZE);
        tableView->setAnchorPoint(Point(0.5f, 0.5f));
        tableView->setPosition(Point(0,bottomBg->getContentSize().height));
        tableView->setDirection(ScrollView::Direction::VERTICAL);
        
        tableView->setDelegate(this);
        tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
        
        tableView->reloadData();
        contentsLayer->addChild(tableView, 5);
        //        this->setTouchEnabled(true);
        
        Sprite *background = this->getMiddleBackground(POPUP_YEAR_BACKGROUND_HEIGHT, Point(0,bottomBg->getContentSize().height));
        
        contentsLayer->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        contentsLayer->setContentSize(Size(topBg->getContentSize().width,background->getContentSize().height+topBg->getContentSize().height+bottomBg->getContentSize().height));
        contentsLayer->setPosition(Point(visibleSize.width/2-contentsLayer->getContentSize().width/2,visibleSize.height/2-contentsLayer->getContentSize().height/2));
        this->addChild(contentsLayer, TAG_CONTENT_LAYER);
        
        topBg->setPosition(Point(0,contentsLayer->getContentSize().height-topBg->getContentSize().height));
        contentsLayer->addChild(background, 0);
        contentsLayer->addChild(topBg);
        contentsLayer->addChild(bottomBg);
        
        auto pickerLine01 = this->getPickerLine(POPUP_YEAR_PICKER_LINE_01);
        auto pickerLine02 = this->getPickerLine(POPUP_YEAR_PICKER_LINE_02);
        contentsLayer->addChild(pickerLine01,6);
        contentsLayer->addChild(pickerLine02,6);
    }
    
    Menu *buttonMenu = NULL;
    MenuItemSprite *btn1 = NULL;
    MenuItemSprite *btn2 = NULL;
    if (btn1Name == "" && btn2Name == NO_BUTTON_NAME) {
        log("you should wrote one button name at least!");
    } else if (btn1Name == "" || btn2Name == NO_BUTTON_NAME) {
        if (btn1Name == NO_BUTTON_NAME) {
            btn2 = this->getButton(btn2Name, POPUP_ONE_BUTTON_SIZE);
        } else if (btn2Name == NO_BUTTON_NAME) {
            btn1 = this->getButton(btn1Name, POPUP_ONE_BUTTON_SIZE);
        }
    } else {
        btn1 = this->getButton(btn1Name, POPUP_TWO_BUTTON_SIZE);
        btn2 = this->getButton(btn2Name, POPUP_TWO_BUTTON_SIZE);
    }
    
    buttonMenu = Menu::create(btn1, btn2, NULL);
    buttonMenu->alignItemsHorizontallyWithPadding(POPUP_BUTTON_PADDING);
    buttonMenu->setPosition(Point(bottomBg->getContentSize().width/2,bottomBg->getContentSize().height/2));
    buttonMenu->setTag(TAG_MENU_BUTTON);
    contentsLayer->addChild(buttonMenu);
    
    return true;
}

LayerColor* PopupLayer::getPickerLine(Point position)
{
    auto pickerLine = LayerColor::create(Color4B(0x6f,0xd4,0xdd,255), POPUP_YEAR_PICKER_LINE_SIZE.width, POPUP_YEAR_PICKER_LINE_SIZE.height);
    pickerLine->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    pickerLine->setPosition(position);
    return pickerLine;
}

MenuItemSprite* PopupLayer::getButton(const std::string& buttonName, float width)
{
    __String *norBtnImgName = NULL;
    __String *pressBtnImgName = NULL;
    int tag = 0;
    if (buttonName == Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL)
        || buttonName == Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_NO)
        || buttonName == "もどる") {//"Cancel"
        norBtnImgName = __String::create("popup_cancel_btn_normal.png");
        pressBtnImgName = __String::create("popup_cancel_btn_press.png");
        tag = TAG_CANCEL_BUTTON;
    } else if (buttonName == Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK)//"OK"
               || buttonName == Device::getResString(Res::Strings::PF_SIGNUP_BUTTON_TITLE_SET)//"Set
               || buttonName == Device::getResString(Res::Strings::CC_HOME_POPUP_NEED_LOGIN_BUTTON_TITLE_LOGIN)
               || buttonName == Device::getResString(Res::Strings::CC_EXPANDEDITEM_POPUP_BUTTON_TITLE_EARN_COIN)
               || buttonName == Device::getResString(Res::Strings::CC_EXPANDEDITEM_POPUP_COIN_BUTTON_TITLE_UNLOCK)
               || buttonName == Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_YES)//"Set
               || buttonName == "ログインする") {
        norBtnImgName = __String::create("popup_ok_btn_normal.png");
        pressBtnImgName = __String::create("popup_ok_btn_press.png");
        if (buttonName == Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK)
            || buttonName == Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_YES) ) {//"OK"
            tag = 1;
        } else {
            tag = 2;
        }
    } else {
        norBtnImgName = __String::create("popup_cancel_btn_normal.png");
        pressBtnImgName = __String::create("popup_cancel_btn_press.png");
    }
    
    const std::string &name1 = norBtnImgName->getCString();
    const std::string &name2 = pressBtnImgName->getCString();
    cocos2d::extension::Scale9Sprite *scale1 = cocos2d::extension::Scale9Sprite::create(name1, Rect(0,0,POPUP_BUTTON_IMAGE_SIZE.width,POPUP_BUTTON_IMAGE_SIZE.height),Rect(POPUP_BUTTON_CORNER_SIZE.width,POPUP_BUTTON_CORNER_SIZE.height,POPUP_BUTTON_CORNER_SIZE.width,POPUP_BUTTON_CORNER_SIZE.height));
    scale1->setContentSize(Size(width,scale1->getContentSize().height));
    cocos2d::extension::Scale9Sprite *scale2 = cocos2d::extension::Scale9Sprite::create(name2, Rect(0,0,POPUP_BUTTON_IMAGE_SIZE.width,POPUP_BUTTON_IMAGE_SIZE.height),Rect(POPUP_BUTTON_CORNER_SIZE.width,POPUP_BUTTON_CORNER_SIZE.height,POPUP_BUTTON_CORNER_SIZE.width,POPUP_BUTTON_CORNER_SIZE.height));
    scale2->setContentSize(Size(width,scale2->getContentSize().height));
    
    std::string firstLine, secondLine;
    Util::splitTitleString(buttonName, ' ', firstLine, secondLine, POPUP_BUTTON_TWOLINE_LABEL_WIDTH, FONT_NAME, POPUP_BUTTON_FONTSIZE);
    
    Label *buttonLabel;
    if (strcmp(secondLine.c_str(), "") == 0) {
        buttonLabel = Label::createWithSystemFont(buttonName, FONT_NAME, POPUP_BUTTON_FONTSIZE,cocos2d::Size::ZERO,TextHAlignment::CENTER, TextVAlignment::CENTER);
    } else {
        buttonLabel = Label::createWithSystemFont(buttonName, FONT_NAME, POPUP_BUTTON_TWOLINE_FONTSIZE ,cocos2d::Size(POPUP_BUTTON_TWOLINE_LABEL_WIDTH,0),TextHAlignment::CENTER, TextVAlignment::CENTER);
    }
    
    auto button = MenuItemSprite::create(scale1, scale2, CC_CALLBACK_1(PopupLayer::buttonClicked, this));
    buttonLabel->setColor(Color3B(0xff,0xff,0xff));
    buttonLabel->setAnchorPoint(Point(0.5,0.5));
    buttonLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    buttonLabel->setPosition(Point(button->getContentSize().width/2, button->getContentSize().height/2));
    button->addChild(buttonLabel);
    button->setTag(tag);
    
    return button;
}

Label* PopupLayer::getTopLabel(const std::string &name)
{
    Label *popName = Label::createWithSystemFont(name, FONT_NAME, POPUP_TITLE_FONTSIZE, POPUP_TITLE_LABEL_SIZE,TextHAlignment::LEFT, TextVAlignment::CENTER);
    popName->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    popName->setColor(Color3B(0xff,0xff,0xff));
    popName->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    popName->setPosition(POPUP_TITLE);
    return popName;
}

cocos2d::extension::Scale9Sprite* PopupLayer::getBottomBg()
{
    auto bottomBg = cocos2d::extension::Scale9Sprite::create("popup_bottom.png",
    		Rect(0, 0, POPUP_BOTTOMBG_SIZE.width, POPUP_BOTTOMBG_SIZE.height),
    		Rect(POPUP_BOTTOMBG_CORNER_SIZE.width, POPUP_BOTTOMBG_CORNER_SIZE.height, POPUP_BOTTOMBG_CORNER_SIZE.width, POPUP_BOTTOMBG_CORNER_SIZE.height));
    bottomBg->setContentSize(Size(POPUP_WIDTH,bottomBg->getContentSize().height));
    bottomBg->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    bottomBg->setPosition(Point::ZERO);
    return bottomBg;
}

cocos2d::extension::Scale9Sprite* PopupLayer::getTopBg()
{
    auto *topBg = cocos2d::extension::Scale9Sprite::create("popup_top.png", Rect(0,0,POPUP_LIST_TOPBG_SIZE.width,POPUP_LIST_TOPBG_SIZE.height),Rect(POPUP_LIST_TOPBG_CORNER_SIZE.width,POPUP_LIST_TOPBG_CORNER_SIZE.height,POPUP_LIST_TOPBG_CORNER_SIZE.width,POPUP_LIST_TOPBG_CORNER_SIZE.height));
    topBg->setContentSize(Size(POPUP_WIDTH,topBg->getContentSize().height));
    topBg->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    topBg->setPosition(Point::ZERO);
    return topBg;
}

Sprite* PopupLayer::getMiddleBackground(float height, Point bgPosition)
{
    Texture2D *texture = Director::getInstance()->getTextureCache()->addImage("popup_middle.png");
    const Texture2D::TexParams &tp = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE};
    texture->setTexParameters(tp);
    Sprite *background = Sprite::createWithTexture(texture, Rect(0, 0, texture->getContentSize().width, height));
    background->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    background->setPosition(bgPosition);
    return background;
}

void PopupLayer::buttonClicked(Ref *pSender)
{
    auto clickedButton = (MenuItem*)pSender;
    int i = clickedButton->getTag();
    bool skipClose = _preventClose;
    
    if (i == 0) {
        
    } else if (i == 1) {
    
    } else {
        log("unknown tag");
    }
    
	if (_delegate != NULL) {
        _delegate->popupButtonClicked(this, clickedButton);
    }
    
	if(!skipClose) {
		this->removeFromParentAndCleanup(true);
	}
}

void PopupLayer::onEnter()
{
    Layer::onEnter();
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [=](Touch* touch, Event* event) {return true;};
    listener->onTouchMoved = [=](Touch* touch, Event* event){};
    listener->onTouchEnded = [=](Touch* touch, Event* event) {
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    	if(_pendingClose) {
    		this->removeFromParentAndCleanup(true);
    	}
//#endif
    };
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}


void PopupLayer::onExit()
{
    _eventDispatcher->removeEventListenersForTarget(this);
    Layer::onExit();
}

#pragma mark tableView
Size PopupLayer::tableCellSizeForIndex(cocos2d::extension::TableView *table, ssize_t idx)
{
    if (popupType == POPUP_TYPE_SHARED) {
        return POPUP_SHARED_CELL_SIZE;
    } else if (popupType == POPUP_TYPE_YEARS) {
        return POPUP_YEAR_CELL_SIZE;
    }
    return Size(0,0);
}

TableViewCell* PopupLayer::tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx)
{
    TableViewCell* cell = table->dequeueCell();
    if (!cell) {
        cell = new TableViewCell();
        cell->autorelease();
    }
    
    __String* string = (__String*)listArr->getObjectAtIndex(idx);
    cell->removeAllChildrenWithCleanup(true);
    if (popupType == POPUP_TYPE_SHARED) {
        const char *sharedImgName = NULL;
        switch (idx) {
            case 0:
            {
                sharedImgName = "popup_share_facebook.png";
                break;
            }
            case 1:
            {
                sharedImgName = "popup_share_youtube.png";
                break;
            }
            case 2:
            {
                sharedImgName = "popup_share_vimeo.png";
                break;
            }
            default:
                break;
        }
        const std::string& imgName = sharedImgName;
        Sprite* sharedImg = Sprite::create(imgName);
        sharedImg->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        sharedImg->setPosition(POPUP_SHARED_ICON);
        sharedImg->setContentSize(POPUP_SHARED_ICON_SIZE);
        cell->addChild(sharedImg);
        
        Label *label = Label::createWithSystemFont(string->getCString(), "Helvetica", POPUP_SHARED_CELL_FONTSIZE, POPUP_SHARED_CELL_LABEL_SIZE,TextHAlignment::LEFT, TextVAlignment::CENTER);
        label->setColor(Color3B(0x78,0x78,0x78));
        label->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        label->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
        label->setPosition(POPUP_SHARED_CELL_LABEL);
        label->setTag(TAG_LABEL_CELLTITLE);
        cell->addChild(label);
    } else if (popupType == POPUP_TYPE_YEARS) {
        Label *label = Label::createWithSystemFont(string->getCString(), "Helvetica", POPUP_SHARED_CELL_FONTSIZE, POPUP_YEAR_CELL_LABEL_SIZE,TextHAlignment::CENTER, TextVAlignment::CENTER);
        label->setColor(Color3B(0x60,0x60,0x60));
        label->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        label->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
        label->setPosition(POPUP_YEAR_CELL_LABEL);
        label->setTag(TAG_LABEL_CELLTITLE);
        cell->addChild(label);
    }
    
    return cell;
    
}

ssize_t PopupLayer::numberOfCellsInTableView(cocos2d::extension::TableView *table)
{
    return listArr->count();
}

void PopupLayer::tableCellTouched(cocos2d::extension::TableView *table, cocos2d::extension::TableViewCell *cell)
{
    log("cell touched");
    if (_delegate) {
        Label *label = (Label *)cell->getChildByTag(TAG_LABEL_CELLTITLE);
        if (label) {
            log("selected cell title = %s", label->getString().c_str());
            _delegate->popupListClicked(this, label->getString().c_str());
        }
    }

//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    _pendingClose = true;
//#else
//	this->removeFromParentAndCleanup(true);
//#endif
}

void PopupLayer::scrollViewDidScroll(cocos2d::extension::ScrollView *view)
{
    
}

void PopupLayer::scrollViewDidZoom(cocos2d::extension::ScrollView *view)
{
    
}
