//
//  JsonFormat.h
//  bobby
//
//  Created by Dongwook, Kim on 14. 4. 17..
//
//

#ifndef bobby_JsonFormat_h
#define bobby_JsonFormat_h

// meta.json

{
    "last_update" : 1397809321 // unix time stamp
}


// books.json

{
    "book1" : {
        "title" : "Bobby's Circus show",
        "author" : "Dole",
        "background" : "home_book_story_01.png",
        "background_image" : "home_book_story_bg_01.png",
        "editable"   : false,
        "default_title" : true,
        "default_author" : true,
        "font_color" : [
                        255,
                        235,
                        9
                        ],
        "editable" : false,
        "chapters" : {
            "chapter1" : {
                "movie_file" : "scene_01_0630.mp4",
                "background" : "B001",
                "sound"      : "S001",
                "timeline_file" : "timeline_1397809321",
                "char_datas" : [
                                {
                                    "char_id":1,
                                    "char_key":"C010",
                                    "pos_x":10,
                                    "pos_y":10,
                                    "angle":0,
                                    "scale":1.0,
                                    "direction":1
                                },
                                {
                                    "char_id":2,
                                    "char_key":"C013",
                                    "pos_x":10,
                                    "pos_y":10,
                                    "angle":0,
                                    "scale":1.0,
                                    "direction":1
                                }
                                ],
                "char_last_datas" : [
                                {
                                    "char_id":1,
                                    "char_key":"C010",
                                    "pos_x":10,
                                    "pos_y":10,
                                    "angle":0,
                                    "scale":1.0,
                                    "direction":1
                                },
                                {
                                    "char_id":2,
                                    "char_key":"C013",
                                    "pos_x":10,
                                    "pos_y":10,
                                    "angle":0,
                                    "scale":1.0,
                                    "direction":1
                                }
                                ]
            },
            "chapter2" : {
               ...
            },
            "chapter3" : {
               ...
            },
            "chapter4" : {
               ...
            },
            "chapter5" : {
               ...
            }
        }
    },
    "book2" : {
        ...
    },
    "book3" : {
        ...
    },
    ...
}

// resources.json

{
    "character" : {
        "C010" : {
            "side" : "home_book_story_character_01.png",
            "charname" : "lion",
            "ccbi" : "lion.ccbi",
            "bookmap_ccbi" : "map_created_page_character_01.ccbi",
            "item_normal" : "home_character_01_normal.png",
            "item_selected" : "home_character_01_select.png",
            "custom_normal" : "home_character_custom_01_normal.png",
            "custom_select" : "home_character_custom_01_select.png",
            "custom_face" : "camera_character_frame_01.png",
            "indicator" : "recording_progress_icon_character_01.png",
            "flip_on_move" : true,
            "move_sound" : "item_02_drag.wav", // optional
            "tap_sound" : "item_02_drag.wav", // optional
            "editable" : false,
            "locked" : true,
            "price" : 600,
            "free" : false,
            "keep_days" : 7
        },
        "I003" : {
            "side" : "home_book_story_character_01.png",
            "charname" : "ball",
            "ccbi" : "ball",
            "bookmap_ccbi" : "map_created_page_character_01.ccbi",
            "item_normal" : "home_item_01_normal.png",
            "item_selected" : "home_item_01_select.png",
            "indicator" : "recording_progress_icon_item_01.png",
            "flip_on_move" : true,
            "move_sound_all_direction" : true,
            "move_sound" : "item_01_tap.wav",
            "tap_sound" : "item_01_drag.wav",
            "editable" : false,
            "locked" : true,
            "price" : 500,
            "free" : false,
            "keep_days" : 0
        },
        ...
    },
    "custom_character" : {
        "C010_custom" : {
            "custom_id" : 10,
            "side" : "home_book_story_character_create_01.png",
            "charname" : "lion_custom",
            "ccbi" : "lion_custom.ccbi",
            "bookmap_ccbi" : "map_created_page_character_create_01.ccbi",
            "item_normal" : "home_character_custom_01_normal.png",
            "item_selected" : "home_character_custom_01_select.png",
            "indicator" : "recording_progress_icon_character_01.png",
            "flip_on_move" : true,
            "move_sound" : "Move.wav",
            "editable" : true,
            "locked" : true,
            "price" : 600,
            "free" : false,
            "keep_days" : 7
        },
        ...
    },
    "background" : {
        "B001" : {
            "bg_id" : 1,
            "name" : "Circus",
            "filename" : "home_background_frame_01.png",
            "select" : "home_background_select_01.png",
            "map_ccbi" : "map_bg_01.ccbi",
            "ani_ccbi": "home_background_select_01_ani.ccbi",
            "custom" : false,
            "locked" : false,
            "price" : 0,
            "free" : true,
            "keep_days" : 0
        },
        ...
    },
    "sound" : {
        "S001" : {
            "name" : "Jolly",
            "filename" : "set_position_music_jolly.png",
            "sound_filename" : "01_dole_jolly.mp3",
            "font_color" : [
                            255,
                            88,
                            25
                            ],
            "status" : 1
        },
        ...
    }
}

// order.json
    
{
    "book_order" : ["book2", "book1"],
    "character_order" : ["C001","C004","C005","C002","C003","C006","C007","C008","C009","C010","C011","C012","C013"],
    "custom_character_order" : ["C001_custom","C004_custom","C005_custom","C002_custom","C003_custom","C006_custom","C007_custom",
                                "C008_custom","C009_custom","C010_custom","C011_custom","C012_custom"],
    "background_order" : ["B001","B002","B003","B004","B001","B002","B003","B004","B001","B002"],
    "sound_order" : ["S001","S002","S003","S004","S005","S001","S002","S003","S004","S005"]
}
    
// timeline (recording touch)
{
    "timelines" : [
                   {
                        "timestamp" : 1,
                        "touch_id" : 1,
                        "touch_status" : 1,
                        "location_x" : 10,
                        "location_y" : 20
                   },
                   {
                        "timestamp" : 2,
                        "touch_id" : 1,
                        "touch_status" : 1,
                        "location_x" : 10,
                        "location_y" : 20
                   }
                  ],
    "zoom_scale_x":1.1,
    "zoom_scale_y":1.1
}
    
// user character
"custom_char_key" :
{
    "base_char" : "C010_custom",
    "main_photo_file" : "main01.png",
    "thumbnail_photo_file" : "thumb01.png"
}
    
// user background
"custom_bg_key" :
{
    "custom" : true,
    "main_photo_file" : "main01.png",
    "thumbnail_photo_file" : "thumb01.png"
}
    
    
#endif

