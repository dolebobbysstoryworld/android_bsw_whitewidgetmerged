//
//  LabelExtension.h
//  bobby
//
//  Created by oasis on 2014. 4. 9..
//
//

#ifndef __bobby__LabelExtension__
#define __bobby__LabelExtension__

#include "cocos2d.h"
USING_NS_CC;

class LabelExtension : public cocos2d::Label
{
public:
    LabelExtension(FontAtlas *atlas, TextHAlignment hAlignment,TextVAlignment vAlignment) : Label(atlas,hAlignment,vAlignment)
    {}
    ~LabelExtension(){}
    static LabelExtension* create(const std::string& text, const std::string& fontName, float fontSize,
                                         const cocos2d::Size& dimensions = cocos2d::Size::ZERO, TextHAlignment hAlignment = TextHAlignment::LEFT,
                                         TextVAlignment vAlignment = TextVAlignment::TOP, bool underline = false, bool includeFontPadding = true);
    static LabelExtension* createWithTTF(const TTFConfig& ttfConfig, const std::string& text, TextHAlignment alignment = TextHAlignment::LEFT, int lineWidth = 0);

    virtual void sizeToFitWidth(float width);
    virtual void addEllipsis();
    virtual int getTextWidth();
    void addEllipsisWidthDigitNum(int digit);
    void ellipsis(const char* fontName, int fontSize);
};
#endif /* defined(__bobby__LabelExtension__) */
