//
//  ScrollViewExtension.cpp
//  bobby
//
//  Created by Dongwook, Kim on 14. 4. 14..
//
//

#include "ScrollViewExtension.h"

#define SCROLL_DEACCEL_DIST  1.0f
#define BOUNCE_DURATION      0.15f

ScrollViewExtension* ScrollViewExtension::create(Size size, Node* container/* = NULL*/)
{
    ScrollViewExtension* pRet = new ScrollViewExtension();
    if (pRet && pRet->initWithViewSize(size, container))
    {
        pRet->autorelease();
        pRet->setDelegate(pRet);
    }
    else
    {
        CC_SAFE_DELETE(pRet);
    }
    return pRet;
}

ScrollViewExtension* ScrollViewExtension::create()
{
    ScrollViewExtension* pRet = new ScrollViewExtension();
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        pRet->setDelegate(pRet);
    }
    else
    {
        CC_SAFE_DELETE(pRet);
    }
    return pRet;
}

bool ScrollViewExtension::onTouchBegan(Touch *touch, Event *event)
{
    if (!this->isVisible() || !this->getParent()->isVisible()) return false;
    
    if (isAnimate) return false;
    
    auto ret = ScrollView::onTouchBegan(touch, event);
    auto childList = this->getContainer()->getChildren();
    for (auto child : childList )
    {
        bool isTouchable = (child->getLocalZOrder() == TOUCHABLE_LAYER || child->getLocalZOrder() == TOUCHABLE_LAYER_WITH_DIM)?true:false;
        if (!isTouchable || !child->isVisible()) continue;

        Point touchLocation = touch->getLocation();
        Point local = this->getContainer()->convertToNodeSpace(touchLocation);
        Rect bb = child->getBoundingBox();
        
        if ( bb.containsPoint(local) )
        {
            _selectedItem = child;
//            break;
        }
    }
    
//    log("touch began on scroll view extension");
    
    return ret;
}


void ScrollViewExtension::onTouchMoved(Touch *touch, Event *event)
{
    if (isAnimate) return;
    
    ScrollView::onTouchMoved(touch, event);
    if (abs(touch->getStartLocation().x - touch->getLocation().x) > SELECT_THRESHOLD) {
        _selectedItem = nullptr;
    }
    isMoving = true;
//    log("touch move on scroll view extension");
}

void ScrollViewExtension::onTouchEnded(Touch *touch, Event *event)
{
    if (isAnimate) return;
    
    bool itemSelected = false;
    ScrollView::onTouchEnded(touch, event);
    if (_selectedItem) {
        Point touchLocation = touch->getLocation();
        Point local = this->getContainer()->convertToNodeSpace(touchLocation);
        Rect bb = _selectedItem->getBoundingBox();
        if (bb.containsPoint( local ) && _scrollViewExtensionDelegate){
            _scrollViewExtensionDelegate->onSelectItem(this, _selectedItem, touch, event);
            itemSelected = true;
        } 
    }
    
    if (isScrollEnd() && !itemSelected && isMoving) {
        isMoving = false;
        
        if (_scrollViewExtensionDelegate != nullptr) {
            _scrollViewExtensionDelegate->scrollViewDidScrollEnd(this);
        }
    }
//    log("touch ended on scroll view extension");
}

void ScrollViewExtension::onTouchCancelled(Touch *touch, Event *event)
{
    ScrollView::onTouchCancelled(touch, event);
    _selectedItem = nullptr;
}

void ScrollViewExtension::scrollViewDidScroll(ScrollView* view)
{
    if (_scrollViewExtensionDelegate != nullptr) {
        _scrollViewExtensionDelegate->scrollViewDidScroll(this);
    }
    
    if (isScrollEnd() && isMoving) {
        isMoving = false;
        
        if (_scrollViewExtensionDelegate != nullptr) {
            _scrollViewExtensionDelegate->scrollViewDidScrollEnd(this);
        }
    }
}

void ScrollViewExtension::scrollViewDidZoom(ScrollView* view)
{
    if (_scrollViewExtensionDelegate != nullptr) {
        _scrollViewExtensionDelegate->scrollViewDidZoom(this);
    }
}

bool ScrollViewExtension::isScrollEnd()
{
    if(_touches.size() != 2 && !this->isDragging() && (fabsf(_scrollDistance.x) <= SCROLL_DEACCEL_DIST && fabsf(_scrollDistance.y) <= SCROLL_DEACCEL_DIST)) {
        return true;
    }
    
    return false;
}
