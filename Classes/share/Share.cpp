#include "Share.h"
#include <cctype>
#include <iomanip>
#include <sstream>
#include "AuthWebView.h"
#include "Define.h"


using namespace rapidjson;
using namespace std;


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
extern "C" {
    JNIEXPORT void JNICALL
    Java_com_dole_bobbysstoryworld_BobbysStoryWorldActivity_retrieveAuthKeyFromRedirection(
                                                                                     JNIEnv *env, jobject jobj,
                                                                                     jlong jniHandle,
                                                                                     //jint shareType,
                                                                                     jstring redirectionUrl) {
        const char* redirectionUrlChar = env->GetStringUTFChars(redirectionUrl, 0);
        
        // Restore Client instance here
        Share::Client* client = reinterpret_cast<Share::Client*>(jniHandle);
        Share::retrieveAuthKey(client, redirectionUrlChar); //static_cast<Share::ShareType>(shareType), redirectionUrlChar);
        env->ReleaseStringUTFChars(redirectionUrl, redirectionUrlChar);
    }
    
    JNIEXPORT void JNICALL
    Java_com_dole_bobbysstoryworld_BobbysStoryWorldActivity_startClientNextAction(
                                                                            JNIEnv *env, jobject jobj,
                                                                            jlong jniHandle) {
        // Restore Client instance and execute next action
        Share::Client* client = reinterpret_cast<Share::Client*>(jniHandle);
        Share::sendAPIRequest(client->getNextActionClient());
        client->setNextActionClient(NULL);
    }
}
#endif


namespace Share {
    
    size_t progressCallback(void* clientp, double dltotal, double dlnow, double ultotal, double ulnow)
    {
        
        Client* client = static_cast<Client*>(clientp);
        if(client > 0) {
            
            if(client->isCancelled()) {
                return -1;
            }
            
            OnProgressCallback* callback = client->getOnProgressCallback();
            if(callback != NULL) {
                callback->onProgressCallback(client, ultotal, ulnow);
            }
        }
        
        CCLOG("Share::progressCallback dltotal : %f, dlnow : %f, ultotal : %f, ulnow : %f ||| p = %p", dltotal, dlnow, ultotal, ulnow, clientp);
        return 0;
    }
    
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
    
    void tokenizer(const string& str,
                   vector<string>& tokens,
                   const string& delimiters = " ")  {
        string::size_type lastPos = str.find_first_not_of(delimiters, 0);
        string::size_type pos     = str.find_first_of(delimiters, lastPos);
        
        while (string::npos != pos || string::npos != lastPos) {
            tokens.push_back(str.substr(lastPos, pos - lastPos));
            lastPos = str.find_first_not_of(delimiters, pos);
            pos = str.find_first_of(delimiters, lastPos);
        }
    }
    
    string url_encode(const string &value) {
        ostringstream escaped;
        escaped.fill('0');
        escaped << hex;
        
        for (string::const_iterator i = value.begin(), n = value.end(); i != n; ++i) {
            string::value_type c = (*i);
            if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~') {
                escaped << c;
            }
            else if (c == ' ')  {
                escaped << '+';
            }
            else {
                escaped << '%' << setw(2) << ((int) c) << setw(0);
            }
        }
        
        return escaped.str();
    }
    
    string createJSONRequestString(KeyValuePair request) {
        Document doc;
        doc.SetObject();
        
        // Create JSON doc
        for (auto itr = request.begin(); itr != request.end(); itr++) {
            string key = itr->first;
            doc.AddMember((itr->first).c_str(), (itr->second).c_str(), doc.GetAllocator());
        }
        
        // Convert JSON document to string
        GenericStringBuffer< UTF8<> > buffer;
        Writer< GenericStringBuffer< UTF8<> > > writer(buffer);
        doc.Accept(writer);
        
        return buffer.GetString();
    }
    
    string createEncodedRequestString(KeyValuePair request) {
        string requestParamStr;
        
        for (auto itr = request.begin(); itr != request.end();) {
            string key = itr->first;
            string value = itr->second;
            requestParamStr += key + "=" + value;
            
            if(++itr != request.end())
                requestParamStr += "&";
        }
        
        return requestParamStr;
    }
    
    void getValueFromMap(KeyJSONValueMap& resultMap, string key, rapidjson::Value& returnValue) {
        KeyJSONValueMap::const_iterator pos = resultMap.find(key); // Temp string
        if (pos != resultMap.end()) {
            returnValue = pos->second;
            //rapidjson::Value& v = pos->second;
        }
    }
    
    /**
     * TODO NEED Template
     */
    string getStringFromMap(KeyJSONValueMap& resultMap, string key) {
        
        KeyJSONValueMap::const_iterator pos = resultMap.find(key); // Temp string
        if (pos != resultMap.end()) {
            rapidjson::Value& v = pos->second;
            if(v.IsString()) {
                return v.GetString();
            }
        }
        return "";
    }
    
    static const char* kTypeNames[] = { "Null", "False", "True", "Object", "Array", "String", "Number" };
    
    void parseResultAndCreateMap(rapidjson::Value& rootValue, KeyJSONValueMap& resultMap) {
        for (rapidjson::Value::ConstMemberIterator itr = rootValue.MemberonBegin(); itr != rootValue.MemberonEnd(); ++itr) {
            CCLOG("Share::parseResultAndCreateMap Member [%s] - type is [%s]\n", itr->name.GetString(), kTypeNames[itr->value.GetType()]);
            string key = itr->name.GetString();
            rapidjson::Value &v = rootValue[key.c_str()];
            if(v.IsObject()) {
                parseResultAndCreateMap(v, resultMap);
            } else if(v.IsArray()) {
                //TODO loop
                for (SizeType i = 0; i < v.Size(); i++) {
                    rapidjson::Value& itemV = v[i];
                    if(itemV.IsObject())
                        parseResultAndCreateMap(itemV, resultMap);
                    else
                        resultMap.insert(std::pair<string, rapidjson::Value&>(key, itemV));
                }
            } else {
                resultMap.insert(std::pair<string, rapidjson::Value&>(key, v));
                
                if(v.IsString()) {
                    CCLOG("Share::parseResultAndCreateMap [%s] => [%s]", key.c_str(), v.GetString());
                }
                
                if(v.IsNumber()) {
                    CCLOG("Share::parseResultAndCreateMap [%s] => [%d]", key.c_str(), v.GetInt());
                }
            }
        }
    }
    
    string YoutubeAuthClient::checkError(KeyJSONValueMap& resultMap) {
        return Share::getStringFromMap(resultMap, "error");
    }
    
    void YoutubeAuthClient::onPostResult(vector<string> resultHeader, KeyJSONValueMap& resultMap) {
        
        std::string prefix(Youtube::PREFIX);
        switch(m_apiType) {
            case ApiType::AUTH: {
                
            } break;
            case ApiType::GET_ACCESS_TOKEN: {
                AuthWebView::getInstance()->removeWebView();
                string accessKey = Share::getStringFromMap(resultMap, Youtube::TAG_ACCESS_TOKEN);
                string refreshKey = Share::getStringFromMap(resultMap, Youtube::TAG_REFRESH_TOKEN);
                string tokenType = Share::getStringFromMap(resultMap, Youtube::TAG_TOKEN_TYPE);
                
                // Prefix necessary
                UserDefault::getInstance()->setStringForKey((prefix + Youtube::TAG_ACCESS_TOKEN).c_str(), accessKey);
                UserDefault::getInstance()->setStringForKey((prefix + Youtube::TAG_REFRESH_TOKEN).c_str(), refreshKey);
                UserDefault::getInstance()->setStringForKey((prefix + Youtube::TAG_TOKEN_TYPE).c_str(), tokenType);
                
            } break;
            case ApiType::REFRESH_TOKEN: {
                string accessKey = Share::getStringFromMap(resultMap, Youtube::TAG_ACCESS_TOKEN);
                string tokenType = Share::getStringFromMap(resultMap, Youtube::TAG_TOKEN_TYPE);
                
                // Prefix necessary
                UserDefault::getInstance()->setStringForKey((prefix + Youtube::TAG_ACCESS_TOKEN).c_str(), accessKey);
                UserDefault::getInstance()->setStringForKey((prefix + Youtube::TAG_TOKEN_TYPE).c_str(), tokenType);
            } break;
            default: // Start upload isn't a case here
                break;
        }
    }
    
    void YoutubeUploadClient::setUploadState(UploadState uploadState) {
        m_uploadState = uploadState;
    }
    
    void YoutubeUploadClient::setUploadFilePath(string uploadFilePath) {
        m_uploadFilePath = uploadFilePath;
    }
    
    void YoutubeUploadClient::setUploadFileSize(off_t fileSize) {
        m_uploadFileSizeStr = to_string(fileSize);
        CCLOG("Share::YoutubeUploadClient::setUploadFileSize file size str = %s", m_uploadFileSizeStr.c_str());
    }
    
    string YoutubeUploadClient::getUploadFileSizeStr() {
        return m_uploadFileSizeStr;
    }
    
    string checkErrorReason(KeyJSONValueMap& resultMap) {
        return Share::getStringFromMap(resultMap, "reason");
    }
    
    string YoutubeUploadClient::checkError(KeyJSONValueMap& resultMap) {
        rapidjson::Value errorValue;
        getValueFromMap(resultMap, "errors", errorValue);
        if(errorValue.IsNull())
            return "";
        
        if(errorValue.IsObject()) {
            return errorValue["message"].GetString();
        }
    }
    
    void YoutubeUploadClient::onPostResult(vector<string> resultHeader, KeyJSONValueMap& resultMap) {
        
        // Update states
        switch(m_uploadState) {
            case INIT_STARTED: {
                m_uploadState = UploadState::INIT_COMPLETED;
            } break;
            case INIT_COMPLETED: {
                m_uploadState = UploadState::MEDIA_IN_PROGRESS;
            } break;
            case MEDIA_IN_PROGRESS: {
                m_uploadState = UploadState::MEDIA_COMPLETED;
            } break;
            case MEDIA_COMPLETED: //TODO
            default:
                return;
        }
        
        if(m_uploadState == INIT_COMPLETED) {
            // Parse header and find "Location: "
            const string tagLocation = "Location: ";
            string locationUrl = "";
            for(auto header : resultHeader) {
                string::size_type pos = header.find(tagLocation);
                if(pos != string::npos) { // found
                    locationUrl = header.substr(pos + tagLocation.length(), header.length());
                    break;
                }
            }
            
            CCLOG("Share::onPostResult location Url: %s", locationUrl.c_str());
            CCLOG("Share::onPostResult file full path : %s", m_uploadFilePath.c_str());
            
            cocos2d::network::HttpRequest *request = new cocos2d::network::HttpRequest();
            request->setUrl(locationUrl.c_str());
            request->setRequestType(cocos2d::network::HttpRequest::Type::PUT_UPLOAD);
            
            std::string prefix(Youtube::PREFIX);
            
            UserDefault* UD = UserDefault::getInstance();
            string authStr("Authorization: ");
            authStr.append(UD->getStringForKey((prefix + Youtube::TAG_TOKEN_TYPE).c_str())).append(" ");
            authStr.append(UD->getStringForKey((prefix + Youtube::TAG_ACCESS_TOKEN).c_str()));
            
            string contentLenStr("Content-Length: ");
            contentLenStr.append(getUploadFileSizeStr());
            
            vector<string> headers;
            headers.push_back(authStr.c_str()); //TODO
            headers.push_back(contentLenStr.c_str()); //TODO
            headers.push_back("Content-Type: video/*");
            request->setHeaders(headers);
            
#if COCOS2D_DEBUG > 0
            for(auto header : headers) {
                CCLOG("Share::onPostResult file upload request header : %s", header.c_str());
            }
#endif
            
            FILE *fp = fopen(m_uploadFilePath.c_str(), "rb");
            request->setRequestFile(fp);
            request->setRequestFileSize(atoi(getUploadFileSizeStr().c_str()));
            
            request->setProgressRef(this);
            request->setProgressCallback(Share::progressCallback);
            
            request->setResponseCallback(getResponseCallback());
            request->setTag(getTag().c_str());
            
            cocos2d::network::HttpClient::getInstance()->send(request);
            request->release();
        }
    }
    
    void Client::setOnResultCallback(OnResultCallback* callback) {
        m_OnResultCallback = callback;
    }
    
    void Client::setOnProgressCallback(OnProgressCallback* callback) {
        m_OnProgressCallback = callback;
    }
    
    void Client::responseCallback(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response ) {
        if (!response) return;
        
        CCLOG("Share:: response TAG = %s", response->getHttpRequest()->getTag());
        vector<char>*buffer = response->getResponseData();
        vector<char>*headerBuffer = response->getResponseHeader();
        string result(buffer->begin(), buffer->end());
        string headerResult(headerBuffer->begin(), headerBuffer->end());
        
        vector<string> headerTokens;
        tokenizer(headerResult, headerTokens, "\n");
        
        const int errorCode = response->getResponseCode();
        
        /** Only for logging **/
        CCLOG("Share::Client::responseCallback result = %s | code = %ld", result.c_str(), errorCode);
        CCLOG("Share::Client::responseCallback header = %s", headerResult.c_str());
        for(auto token : headerTokens) {
            CCLOG("Share:: header token = %s", token.c_str());
        }
        /** End of log **/
        
        KeyJSONValueMap resultMap;
        Document* jsonDoc = NULL;
        
        string errorMsg = "";
        string errorReason = "";
        
        if(!result.empty()) {
            // parse and make map result
            jsonDoc = new Document();
            if(jsonDoc->Parse<0>(result.c_str()).HasParseError())
                throw ParseError("Json parse failed");
            
            parseResultAndCreateMap(*jsonDoc, resultMap);
            CCLOG("Share::Client::responseCallback after parse");
        }
        
        
        if(!result.empty()) {
            errorMsg = checkError(resultMap);
            errorReason = checkErrorReason(resultMap);
        }
        
        if(errorCode != 200 || !errorMsg.empty()) {
            CCLOG("Share::Client::responseCallback error msg : %s", errorMsg.c_str());
            
            if(errorCode == 401) {
                switch(getApiType()) {
                    case START_UPLOAD: {
                        if(errorReason == "youtubeSignupRequired") {
                            // Show webview for creating channel
                            YoutubeAuthClient* client = new YoutubeAuthClient(this);
                            client->setApiType(ApiType::CREATE_CHANNEL);
                            
                            string apiUrl = client->getApi();
                            string redirectUrl = client->getRedirectUrl();
                            
                            AuthWebView* webView = AuthWebView::getInstance();
                            webView->showWebView(client,
                                                 this->getShareType(),
                                                 apiUrl.c_str(),
                                                 redirectUrl.c_str(),
                                                 0, 0,
                                                 -1, -1);
                            goto release_api_result;
                        } else if(errorReason == "authError") {
                            CCLOG("Share::Client::responseCallback token expired. Start refreshing");
                            Share::refreshYoutubeTokens(this);
                            goto release_api_result;
                        }
                    }
                    default:
                        break;
                }
            }
            
            if(m_OnResultCallback != NULL)
                m_OnResultCallback->onFailedCallback(this, errorCode, resultMap);
            
        } else {
            CCLOG("Share::Client::responseCallback no error");
            if(m_OnResultCallback != NULL) {
                m_OnResultCallback->onSuccessCallback(this, 0, resultMap);
            }
            
            CCLOG("Share::Client::responseCallback before onPostResult");
            onPostResult(headerTokens, resultMap);
            
            Client* nextAction = getNextActionClient();
            if(nextAction != NULL &&
               nextAction->getApiType() == ApiType::START_UPLOAD) {
                CCLOG("Share::Client::responseCallback after onPostResult and request API of kept client ");
                Share::updateYoutubeUploadAuth(static_cast<YoutubeUploadClient*>(nextAction));
                sendAPIRequest(nextAction);
                this->setNextActionClient(NULL);
            }
        }
        
    release_api_result:
        if(jsonDoc != NULL) {
            CCLOG("Share:: release jsonDoc");
            delete jsonDoc;
        }
    }
    
    void Client::cancel() {
        m_IsCancelled = true;
        if(m_useWebView) {
            AuthWebView* webView = AuthWebView::getInstance();
            if(webView) {
                webView->removeWebView();
            }
        }
        
        if(m_OnResultCallback != NULL) {
            CCLOG("Share:: client cancelled");
            m_OnResultCallback->onCancelledCallback(this);
        }
    }
    
    void sendAPIRequest(Client* client) {
        CCLOG("Share::sendAPIRequest");
        RequestEncodeType requestType = client->getRequestEncodeType();
        KeyValuePair* requestMap = client->getRequestMap();
        KeyValuePair* extraHeader = client->getExtraHeader();
        string requestExtraData = client->getRequestExtraData();
        MethodType methodType = client->getHttpRequestMethodType();
        bool useWebView = client->getIsUseWebView();
        
        string apiUrl = string(client->getApi());
        string redirectUrl = client->getRedirectUrl();
        
        string contentType = "";
        string requestData = "";
        bool isExtraRequestDataExist = (!requestExtraData.empty());
        
        if(requestType == RequestEncodeType::url_encoded && requestMap != nullptr) {
            contentType = "Content-Type: application/x-www-form-urlencoded";
            string encodedStr = createEncodedRequestString(*requestMap);
            delete requestMap;
            
            if(isExtraRequestDataExist) {
                apiUrl.append("?");
                apiUrl.append(encodedStr);
                requestData.append(requestExtraData);
            } else {
                requestData.append(encodedStr); //TODO Just copy?
            }
        }
        else if(requestType == RequestEncodeType::json) {
            contentType = "Content-Type: application/json; charset=utf-8";
            
            if(isExtraRequestDataExist) {
                apiUrl.append("?");
                apiUrl.append(createEncodedRequestString(*requestMap));
                requestData.append(requestExtraData);
            } else {
                requestData.append(createJSONRequestString(*requestMap));
            }
        }
        
        if(!useWebView) {
            cocos2d::network::HttpRequest *request = new cocos2d::network::HttpRequest();
            
            const char* requestParam = requestData.c_str();
            CCLOG("Share:: requestParam = %s", requestParam);
            request->setRequestData(requestParam, strlen(requestParam));
            
            CCLOG("Share:: apiUrl : %s", apiUrl.c_str());
            
            request->setUrl(apiUrl.c_str());
            request->setRequestType(methodType);
            request->setResponseCallback(client->getResponseCallback());
            request->setTag(client->getTag().c_str()); //TODO What is this?
            
            vector<string> headers;
            headers.push_back(contentType);
            if(extraHeader != NULL) {
                for (auto itr = extraHeader->begin(); itr != extraHeader->end(); ++itr) {
                    string headerStr = itr->first + ": " + itr->second;
                    CCLOG("Share:: header str : %s", headerStr.c_str());
                    headers.push_back(headerStr);
                }
            }
            request->setHeaders(headers);
            
            cocos2d::network::HttpClient::getInstance()->send(request);
            request->release();
            
        } else {
            apiUrl += "?";
            apiUrl += requestData;
            
            CCLOG("Share:: api = %s", apiUrl.c_str());
            CCLOG("Share:: red = %s", redirectUrl.c_str());
            
            // Show webview
            AuthWebView* webView = AuthWebView::getInstance();
            webView->showWebView(client,
                                 client->getShareType(),
                                 apiUrl.c_str(),
                                 redirectUrl.c_str(),
                                 0, 0,
                                 -1, -1);
        }
    }
    
    
    void createYoutubeAuthAction(Client* client) {
        CCLOG("Share::createYoutubeAuthAction");
        KeyValuePair* requestParamMap = new KeyValuePair();
        requestParamMap->insert(make_pair("client_id", Youtube::APP_ID));
        requestParamMap->insert(make_pair("redirect_uri", url_encode(Youtube::REDIRECT_URL)));
        requestParamMap->insert(make_pair("response_type", "code"));
        requestParamMap->insert(make_pair("scope", url_encode(Youtube::SCOPE_ACCOUNT+ " " + Youtube::SCOPE_UPLOAD)));
        requestParamMap->insert(make_pair("access_type", "offline"));
        
        client->setApiType(ApiType::AUTH);
        client->setRequestEncodeType(RequestEncodeType::url_encoded);
        client->setRequestMap(requestParamMap);
        client->setExtraHeader(NULL);
        client->setRequestExtraData(string());
        client->setHttpRequestMethodType(MethodType::GET);
        client->setIsUseWebView(true);
    }
    
    void startAuthorizeYoutube(Client* client) {
        CCLOG("Share:: startAuthorize");
        createYoutubeAuthAction(client);
        
        sendAPIRequest(client);
    }
    
    void retrieveAuthKey(Client* client, const char* redirectUrl) {
        // Parse code and get authkey!
        string redirectUrlStr(redirectUrl);
        string code= "";
        
        const string pattern("code=");
        size_t num_start = redirectUrlStr.find(pattern) + pattern.size();
        
        if(num_start != string::npos && redirectUrlStr.length() > 0) {
            code = redirectUrlStr.substr(num_start, string::npos);
        }
        
        CCLOG("Share:: code = %s", code.c_str());
        KeyValuePair* requestParamMap = new KeyValuePair();
        requestParamMap->insert(make_pair("code", url_encode(code)));
        requestParamMap->insert(make_pair("client_id", url_encode(Youtube::APP_ID)));
        requestParamMap->insert(make_pair("client_secret", url_encode(Youtube::APP_KEY)));
        requestParamMap->insert(make_pair("redirect_uri", url_encode(Youtube::REDIRECT_URL)));
        requestParamMap->insert(make_pair("grant_type", url_encode("authorization_code")));
        
        client->setApiType(ApiType::GET_ACCESS_TOKEN);
        client->setRequestEncodeType(RequestEncodeType::url_encoded);
        client->setRequestMap(requestParamMap);
        client->setExtraHeader(NULL);
        client->setRequestExtraData(string());
        client->setHttpRequestMethodType(MethodType::POST);
        client->setIsUseWebView(false);
        
        sendAPIRequest(client);
    }
    
    void refreshYoutubeTokens(Client* client) {
        
        std::string prefix(Youtube::PREFIX);
        string refreshToken = UserDefault::getInstance()->getStringForKey((prefix + Youtube::TAG_REFRESH_TOKEN).c_str(), "");
        
        //TODO Check empty str
        
        KeyValuePair* requestParamMap = new KeyValuePair();
        requestParamMap->insert(make_pair("client_id", Youtube::APP_ID));
        requestParamMap->insert(make_pair("client_secret", url_encode(Youtube::APP_KEY)));
        requestParamMap->insert(make_pair("refresh_token", refreshToken));
        requestParamMap->insert(make_pair("grant_type", "refresh_token"));
        
        client->setApiType(ApiType::REFRESH_TOKEN);
        client->setRequestEncodeType(RequestEncodeType::url_encoded);
        client->setRequestMap(requestParamMap);
        client->setExtraHeader(NULL);
        client->setRequestExtraData(string());
        client->setHttpRequestMethodType(MethodType::POST);
        client->setIsUseWebView(false);
        
        sendAPIRequest(client);
    }
    
    void updateYoutubeUploadAuth(YoutubeUploadClient* client) {
        KeyValuePair* extraHeader = client->getExtraHeader();
        if(extraHeader == nullptr)
            return;
        
        string prefix(Youtube::PREFIX);
        string authStr("Bearer ");
        authStr.append(UserDefault::getInstance()->getStringForKey((prefix + Youtube::TAG_ACCESS_TOKEN).c_str()));
        
        (*extraHeader)["authorization"] = authStr.c_str();
        //        extraHeader->insert(make_pair("authorization", authStr.c_str()));
    }
    
    void startYoutubeUpload(
                            YoutubeUploadClient* client,
                            string uploadFilePath,
                            string title,
                            string desc,
                            list<string> tagList
                            ) {
        
        CCLOG("Share::startYoutubeUpload");
        // Generate start upload request json
        Document doc;
        doc.SetObject();
        Document::AllocatorType& allocator = doc.GetAllocator();
        
        rapidjson::Value snippetValue(kObjectType);
        snippetValue.AddMember("title", title.c_str(), doc.GetAllocator());
        snippetValue.AddMember("description", desc.c_str(), allocator);
        
        rapidjson::Value tags(kArrayType);
        for(auto it = tagList.begin(); it != tagList.end() ; ++it) {
            tags.PushBack((*it).c_str(), allocator);
        }
        snippetValue.AddMember("tags", tags, allocator);
        snippetValue.AddMember("categoryId", 1, allocator);
        doc.AddMember("snippet", snippetValue, doc.GetAllocator());
        
        rapidjson::Value status(kObjectType);
        status.AddMember("privacyStatus", "public", allocator);
        status.AddMember("embeddable", true, allocator);
        status.AddMember("license", "youtube", allocator);
        doc.AddMember("status", status, allocator);
        
        // Convert JSON document to string
        GenericStringBuffer< UTF8<> > buffer;
        Writer< GenericStringBuffer< UTF8<> > > writer(buffer);
        doc.Accept(writer);
        string jsonStr = buffer.GetString();
        
        KeyValuePair* requestParamMap = new KeyValuePair();
        requestParamMap->insert(make_pair("uploadType", "resumable"));
        requestParamMap->insert(make_pair("part", "snippet,status,contentDetails"));
        
        CCLOG("Share:: upload header");
        
        string prefix(Youtube::PREFIX);
        string authStr("Bearer ");
        authStr.append(UserDefault::getInstance()->getStringForKey((prefix + Youtube::TAG_ACCESS_TOKEN).c_str()));
        
        string contentLengthStr = Share::to_string(jsonStr.length());
        
        // Open file!
        off_t file_size;
        struct stat stbuf;
        int fd = open(uploadFilePath.c_str(), O_RDONLY);
        if(fd == -1) {
            CCLOG("Share::startYoutubeUpload file open failed");
            return; // Handle error?
        }
        
        if((fstat(fd, &stbuf) != 0) || (!S_ISREG(stbuf.st_mode))) {
            CCLOG("Share::startYoutubeUpload file stat failed");
            return; // Handle error?
        }
        
        file_size = stbuf.st_size;
        CCLOG("Share::startYoutubeUpload file size : %ld", (long)file_size);
        close(fd);
        
        client->setUploadFileSize(file_size);
        client->setUploadState(UploadState::INIT_STARTED);
        client->setUploadFilePath(uploadFilePath);
        
        KeyValuePair* extraHeader = new KeyValuePair();
        extraHeader->insert(make_pair("authorization", authStr.c_str()));
        extraHeader->insert(make_pair("content-length", contentLengthStr.c_str()));
        extraHeader->insert(make_pair("x-Upload-Content-Length", (client->getUploadFileSizeStr()).c_str())); //TODO Need real file size
        extraHeader->insert(make_pair("x-Upload-Content-Type", "video/*"));
        
        client->setApiType(ApiType::START_UPLOAD);
        client->setRequestEncodeType(RequestEncodeType::json);
        client->setRequestMap(requestParamMap);
        client->setExtraHeader(extraHeader);
        client->setRequestExtraData(jsonStr);
        client->setHttpRequestMethodType(MethodType::POST);
        client->setIsUseWebView(false);
        
        // Check authentication and if auth_token exist
        string accessKeyTag = prefix + Youtube::TAG_TOKEN_TYPE;
        string accessKey = UserDefault::getInstance()->getStringForKey(accessKeyTag.c_str());
        if(accessKey == "") {
            auto authClient = new YoutubeAuthClient(client);
            createYoutubeAuthAction(authClient);
            startAuthorizeYoutube(authClient);
            return;
        }
        
        // Request start upload
        sendAPIRequest(client);
    }
}
