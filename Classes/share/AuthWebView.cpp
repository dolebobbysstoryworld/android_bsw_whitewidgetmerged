
#include "AuthWebView.h"

AuthWebView* AuthWebView::s_Instance;

// Android
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "AppDelegate.h"
#include <platform/android/jni/JniHelper.h>

#include <android/log.h>

const char* package_name = "com/dole/bobbysstoryworld/BobbysStoryWorldActivity";

void AuthWebView::showWebView(Share::Client* client,
                              Share::ShareType shareType,
                              const char* url,
                              const char* redirectUrl,
                              float x, float y,
                              float width, float height) {
    setClient(client);
    
    JniMethodInfo info;
    if(JniHelper::getMethodInfo(info,
                                package_name,
                                "displayWebView",
                                "(JILjava/lang/String;Ljava/lang/String;IIII)V")) {
        jlong jhandle = (jlong) (intptr_t) client; //reinterpret_cast<jlong>(&client);
        jint jshareType = (int)shareType;
        jstring jurl = info.env->NewStringUTF(url);
        jstring jredirectUrl = info.env->NewStringUTF(redirectUrl);
        jint jX = (int)x;
        jint jY = (int)y;
        jint jWidth = (int)width;
        jint jHeight = (int)height;
        
        info.env->CallVoidMethod(thisObj, info.methodID, jhandle, jshareType, jurl, jredirectUrl, jX, jY, jWidth, jHeight);
        info.env->DeleteLocalRef(info.classID);
    }
}

void AuthWebView::removeWebView() {
    JniMethodInfo info;
    if(JniHelper::getMethodInfo(info, package_name, "removeWebView", "()V")) {
        info.env->CallVoidMethod(thisObj, info.methodID);
    }
}

#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
//#include "AuthWebView_iOS.h"
//
//static AuthWebView_iOS* m_webViewiOS;
//
//void AuthWebView::showWebView(
//                            Share::Client* client,
//                            Share::ShareType shareType,
//                            const char* url,
//                            const char* redirectUrl,
//                            float x, float y,
//                            float width, float height)
//{
//
//    setClient(client);
//    CCLOG("hsw_dbg auth web view ========================= this = %p", this);
////    m_webViewiOS = [[AuthWebView_iOS alloc] initWithCaller:client];
//    m_webViewiOS = [[AuthWebView_iOS alloc] initWithClient:client];
//    [m_webViewiOS showWebView_url:url x:x y:y width:width height:height];
//}
//
//void AuthWebView::removeWebView()
//{
//    [m_webViewiOS removeWebView];
//    m_webViewiOS = nullptr;
//}

#include "TSYouTubeAuthViewController.h"
#include "AppController.h"

#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

static TSYouTubeAuthViewController *m_authViewController;

void AuthWebView::showWebView(
                              Share::Client* client,
                              Share::ShareType shareType,
                              const char* url,
                              const char* redirectUrl,
                              float x, float y,
                              float width, float height)
{
    
    setClient(client);
    CCLOG("hsw_dbg auth web view ========================= this = %p", this);
    //    m_webViewiOS = [[AuthWebView_iOS alloc] initWithCaller:client];
    UIViewController *viewController = nil;
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
    } else {
        viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
    }
    m_authViewController = [[TSYouTubeAuthViewController alloc] init];
    [m_authViewController setClient:client];
    [viewController presentViewController:m_authViewController animated:YES completion:nil];
    [m_authViewController requrstURL:url];
}

void AuthWebView::removeWebView()
{
    [m_authViewController closeActionForEndAuth];
    m_authViewController = nullptr;
}
#endif
