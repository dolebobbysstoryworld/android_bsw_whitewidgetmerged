#include "cocos2d.h"
#include "Share.h"
#import "AuthWebView_iOS.h"
#include <CCGLView.h>

#import "UIImage+PathExtention.h"

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

using namespace Share;

@implementation AuthWebView_iOS

//- (id)initWithCaller:(AuthWebView *)caller {
//    NSLog(@"hsw_dbg init with caller1 : %p ", caller);
//    self = [super init];
//    if( !self )
//        return nil;
//    NSLog(@"hsw_dbg init with caller client : %p ", caller->getClient());
//    self->m_caller = caller;
//    
//    NSLog(@"hsw_dbg init with caller client get : %p ", m_caller->getClient());
//    
//    return self;
//}

- (id)initWithClient:(Share::Client *)client {
    self = [super init];
    if( !self )
        return nil;
    self->m_client = client;
    
    NSLog(@"hsw_dbg init with caller client get : %p ", self->m_client);
    
    return self;
}

- (void)showWebView_url:(const char*)url x:(float)x y:(float)y width:(float)width height:(float)height
{

    NSString *request = [NSString stringWithUTF8String:url];
    
    if (!m_webview)
    {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        NSLog(@"hsw_dbg width = %f, height = %f ", width, height);
        if(width == -1) {
            width = screenRect.size.width;
        }
        
        if(height == -1) {
            height = screenRect.size.height;
        }
        NSLog(@"hsw_dbg width = %f, height = %f ", height, width); //screenRect.size.width, screenRect.size.height);
        
        webViewFrame = [[UIView alloc] initWithFrame:CGRectMake(x, y, height, width)];
        
        UIImage* bannerButtom = [UIImage imageNamedEx:@"banner_bottom.png" ignore:YES];
        
        UIImageView *toolbarView = [[UIImageView alloc] initWithImage:bannerButtom];
        toolbarView.frame = CGRectMake(0, width-bannerButtom.size.height, height, bannerButtom.size.height);
        
        UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *closeButtonImage = [UIImage imageNamedEx:@"banner_close_btn.png" ignore:YES];
        closeButton.frame = CGRectMake(height-closeButtonImage.size.width-(IS_IPAD?(44/2):(28/2)),
                                       toolbarView.frame.size.height-closeButtonImage.size.height-(IS_IPAD?(25/2):(14/2)),
                                       closeButtonImage.size.width, closeButtonImage.size.height);
        [closeButton setBackgroundImage:closeButtonImage forState:UIControlStateNormal];
        [closeButton setTitle:NSLocalizedString(@"CM_POPUP_BUTTON_TITLE_CLOSE", @"Close") forState:UIControlStateNormal];
        closeButton.titleLabel.textColor = [UIColor whiteColor];
        closeButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:IS_IPAD?(50/2):(30/2)];
        [closeButton addTarget:self action:@selector(selectorCloseButton:) forControlEvents:UIControlEventTouchUpInside];
        [toolbarView addSubview:closeButton];
        toolbarView.userInteractionEnabled = YES;
        
        float bottomAlphaSpace = (IS_IPAD)?30/2:22/2;
        m_webview = [[UIWebView alloc] initWithFrame:CGRectMake(x, y, height, width-toolbarView.frame.size.height+bottomAlphaSpace)];
        [m_webview setDelegate:self];
        
        m_webview.backgroundColor = [UIColor clearColor];
        m_webview.opaque = YES;
        [webViewFrame addSubview:m_webview];
        [webViewFrame addSubview:toolbarView];
        
        UIApplication* clientApp = [UIApplication sharedApplication];
        UIWindow* topWindow = [clientApp keyWindow];
        if (!topWindow)
        {
            topWindow = [[clientApp windows] objectAtIndex:0];
        }

//        NSLog(@"topWindow Frame = %@", NSStringFromCGRect(topWindow.frame));
        webViewFrame.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
//        CGAffineTransform rotate = CGAffineTransformMakeRotation(1.57079633);
        webViewFrame.center = topWindow.center;
        [webViewFrame setTransform:CGAffineTransformMakeRotation((M_PI * (90) / 180.0))];
        
        [topWindow addSubview:webViewFrame];
        
        [m_webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:request]
                                                cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                            timeoutInterval:60]];
    }

}

- (IBAction)selectorCloseButton:(id)sender {
    [self removeWebView];
    m_client->cancel();
}

- (void)removeWebView
{
    [m_webview removeFromSuperview];
    m_webview = NULL;
    [webViewFrame removeFromSuperview];
    webViewFrame = nil;
}

#pragma mark - Optional UIWebViewDelegate delegate methods
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"Redirecting to URL: %@", request.URL.absoluteString);
    if(m_client->getShareType() == ShareType::youtube) {
        YoutubeAuthClient* client = static_cast<YoutubeAuthClient*>(m_client);
        NSString* requestUrlStr = request.URL.absoluteString;
        NSString* redirectUrl = [NSString stringWithCString:client->getRedirectUrl().c_str()
                                                   encoding:[NSString defaultCStringEncoding]];
        if ([requestUrlStr hasPrefix:redirectUrl]) {
            // Restore Client instance here
            NSLog(@"Client from caller client = %p", client);
            if(client->getRedirectUrl() == Share::Youtube::REDIRECT_URL) {
                if([requestUrlStr hasSuffix:[NSString stringWithUTF8String:Share::Youtube::REDIRECT_ERROR.c_str()]]) {
                    m_client->cancel();
                    return NO;
                }
                
                Share::retrieveAuthKey(client, [requestUrlStr UTF8String]);
            } else {
                Share::sendAPIRequest(client->getNextActionClient());
                client->setNextActionClient(NULL);
            }
            
            [self removeWebView];
            
            return YES;
        }
    }
    
    return YES;
}

- (void) webViewDidStartLoad:(UIWebView *)webView
{
    NSURL *url = [webView.request mainDocumentURL];
    NSLog(@"The start URL is: %@", url);
}

- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    NSURL *url = [webView.request mainDocumentURL];
    NSLog(@"The finish URL is: %@", url);
}

- (void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSURL *url = [webView.request mainDocumentURL];
    NSLog(@"The load failed URL is: %@", url);
    [self removeWebView];
}

@end
