#import <Foundation/Foundation.h>
#include "AuthWebView.h"
#include "Share.h"

@interface AuthWebView_iOS : NSObject <UIWebViewDelegate>
{
    UIWebView* m_webview;
    UIView* webViewFrame;
    Share::Client* m_client;
}

- (id) initWithClient:(Share::Client*)caller;
- (void)showWebView_url:(const char*)url x:(float)x y:(float)y width:(float)width height:(float)height;
- (void)removeWebView;

@end
