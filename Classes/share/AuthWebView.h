#ifndef __CC_WEBVIEW_H_
#define __CC_WEBVIEW_H_

#include "cocos2d.h"
#include "Share.h"


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#include <jni.h>

extern JNIEnv* thisEnv;
extern jobject thisObj;

#endif

class AuthWebView {
 private:
    static AuthWebView* s_Instance;
    AuthWebView() {}
    void operator=(AuthWebView const&);

    Share::Client* m_client;

 public:
    ~AuthWebView() {}
    
    static AuthWebView* getInstance() {
        if(!s_Instance)
            s_Instance = new AuthWebView();
        return s_Instance;
    }

    inline void setClient(Share::Client* client) {
        m_client = client;
    }
    
    inline Share::Client* getClient() { return m_client; }
    
    void showWebView(Share::Client* client,
		     Share::ShareType shareType,
		     const char* url,
		     const char* redirectUrl,
		     float x, float y,
		     float width, float height);
    
    void removeWebView();

};

#endif
