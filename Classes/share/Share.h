#ifndef __bobby_Share__
#define __bobby_Share__

#include <string>
#include <map>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "cocos2d.h"
#include "extensions/cocos-ext.h"

#include "external/json/document.h"
#include "external/json/prettywriter.h"
#include "external/json/stringbuffer.h"


#include "network/HttpClient.h"
#include "network/HttpRequest.h"
#include "network/HttpResponse.h"


USING_NS_CC;
USING_NS_CC_EXT;

namespace Share {
    const int DEFAULT_ERROR_CODE = -1;
    const std::string JSON_BODY = "json_body";
    
    class Client;
    
    typedef std::map<std::string, std::string> KeyValuePair;
    typedef std::map<std::string, rapidjson::Value&>  KeyJSONValueMap;
    typedef cocos2d::network::HttpRequest::Type MethodType;
    
    class OnResultCallback {
    public:
        virtual void onSuccessCallback(Client* client, int errorCode, KeyJSONValueMap& resultMap) = 0;
        virtual void onFailedCallback(Client* client, int errorCode, KeyJSONValueMap& resultMap) = 0;
        virtual void onCancelledCallback(Client* client) = 0;
    };
    
    class OnProgressCallback {
    public:
    	virtual void onProgressCallback(Client* client, double uploadTotal, double uploadedNow) = 0;
    };
    
    enum RequestEncodeType {
        json,
        url_encoded
    };
    
    enum ShareType {
        youtube = 1,
        //        vimeo = 2
    };
    
    enum ApiType {
        AUTH,
        CREATE_CHANNEL, //Added for create channel
        GET_ACCESS_TOKEN,
        REFRESH_TOKEN,
        START_UPLOAD,
        END_UPLOAD
    };
    
    enum UploadState {
        NOT_STARTED,
        INIT_STARTED,
        INIT_COMPLETED,
        MEDIA_IN_PROGRESS,
        MEDIA_COMPLETED
    };
    
    namespace Youtube {
        const std::string TAG = "youtube";
        const std::string PREFIX = TAG + "_";
        
        const std::string TAG_ACCESS_TOKEN = "access_token";
        const std::string TAG_REFRESH_TOKEN = "refresh_token";
        const std::string TAG_TOKEN_TYPE = "token_type";
        const std::string TAG_START_UPLOAD = "start_upload";
        const std::string TAG_UPLOAD = "upload";
        
        const std::string APP_ID = "268846144701-grp03j1di1sv0vo6v1cg0m8epb7e2hvc.apps.googleusercontent.com"; //"905134153781-f4kt0rk4r0hktndsqhiarm1qkesme16h.apps.googleusercontent.com";
        const std::string APP_KEY = "K6_2Ch1OO4rZvKehoOTWkBnQ"; //"P0dZk6oxAlPMYlBRzwBVYygb";
        
        const std::string REDIRECT_URL = "http://localhost/oauth2callback";
        const std::string REDIRECT_ERROR = "error=access_denied";
        const std::string SCOPE_ACCOUNT = "https://www.googleapis.com/auth/youtube";
        const std::string SCOPE_UPLOAD = "https://www.googleapis.com/auth/youtube.upload";
        const std::string UPLOAD_CONTENT_TYPE = "video/*";
        
        const std::string CHANNEL_CREATION_URL = "https://m.youtube.com/create_channel?chromeless=1&next=/channel_creation_done";
        const std::string CHANNEL_CREATION_DONE_URL = "https://m.youtube.com/channel_creation_done";
        
        const std::string API_AUTH = "https://accounts.google.com/o/oauth2/auth";
        const std::string API_TOKEN = "https://accounts.google.com/o/oauth2/token";
        const std::string API_REFRESH_TOKEN = API_TOKEN;
        const std::string API_GET_CATEGORY_ID = "https://www.googleapis.com/youtube/v3/videoCategories";
        const std::string API_START_UPLOAD = "https://www.googleapis.com/upload/youtube/v3/videos"; //"https://gdata.youtube.com/feeds/api/users/default/uploads";
    };
    /*
     namespace Vimeo {
     const std::string APP_ID = "";
     const std::string APP_KEY = "";
     
     const std::string AUTHORIZE_URL = "https://api.vimeo.com/oauth/authorize";
     const std::string REQUEST_TOKEN_URL = "https://api.vimeo.com/oauth/request_token";
     
     const std::string ACCESS_TOKEN_URL = "https://api.vimeo.com/oauth/access_token";
     
     const std::string REDIRECT_URL = "http://localhost/oauth2callback";
     const std::string REDIRECT_ERROR = "http://localhost/oauth2callback#error=access_denied";
     };
     */
    // End of constants
    
    // Errors
    class ParseError : public std::runtime_error {
    public:
        ParseError(const std::string msg)
        : std::runtime_error(msg)
        {}
    };
    
    class MissingKeyError : public std::runtime_error {
    public:
        MissingKeyError(const std::string msg)
        : std::runtime_error(msg)
        {}
    };
    
    class Client {
    public:
        Client(Client* nextActionClient) {
            if(nextActionClient)
                m_nextActionClient = nextActionClient;
            
            m_responseCallback = std::bind(
                                           &Client::responseCallback,
                                           this,
                                           std::placeholders::_1,
                                           std::placeholders::_2);
        }
        
        ~Client() {
            if(m_requestMap != NULL)
                delete m_requestMap;
            
            if(m_extraHeader != NULL)
                delete m_extraHeader;
            
            if(m_nextActionClient != NULL)
                delete m_nextActionClient;
        }
        
        void setOnResultCallback(OnResultCallback* resultCallback);
        void setOnProgressCallback(OnProgressCallback* progressCallback);
        inline OnProgressCallback* getOnProgressCallback() { return m_OnProgressCallback; }
        
        inline void setNextActionClient(Client* nextActionClient) { m_nextActionClient = nextActionClient; }
        inline Client* getNextActionClient() { return m_nextActionClient; }
        
        ShareType getShareType() { return m_shareType; }
        
        inline void setApiType(ApiType apiType) {
            m_apiType = apiType;
        }
        
        inline ApiType getApiType() { return m_apiType; }
        
        virtual std::string getApi() { return ""; };
        virtual std::string getRedirectUrl() { return ""; };
        virtual std::string getTag() { return ""; };
        virtual void onPostResult(std::vector<std::string> resultHeaders, KeyJSONValueMap& resultMap) {};
        
        virtual std::string checkError(KeyJSONValueMap& resultMap) { return ""; };
        
        cocos2d::network::ccHttpRequestCallback getResponseCallback() { return m_responseCallback; }
        
        void responseCallback(
                              cocos2d::network::HttpClient *sender,
                              cocos2d::network::HttpResponse *response);
        
        inline void setRequestEncodeType(RequestEncodeType requestType) { m_requestType = requestType; }
        inline RequestEncodeType getRequestEncodeType() { return m_requestType; }
        
        inline void setRequestMap(KeyValuePair* requestMap) {
            m_requestMap = requestMap;
        }
        inline KeyValuePair* getRequestMap() { return m_requestMap; }
        
        inline void setExtraHeader(KeyValuePair* extraHeader) { m_extraHeader = extraHeader; }
        inline KeyValuePair* getExtraHeader() { return m_extraHeader; }
        
        inline void setRequestExtraData(std::string requestExtraData) { m_requestExtraData = requestExtraData; }
        inline std::string getRequestExtraData() { return m_requestExtraData; }
        
        inline void setHttpRequestMethodType(MethodType methodType) { m_methodType = methodType; }
        inline MethodType getHttpRequestMethodType() { return m_methodType; }
        
        inline void setIsUseWebView(bool useWebView) { m_useWebView = useWebView; }
        inline bool getIsUseWebView() { return m_useWebView; }
        
        void cancel();
        
        inline bool isCancelled() {
            return m_IsCancelled;
        }
        
    protected:
        ApiType m_apiType;
        ShareType m_shareType;
        
    private:
        Client* m_nextActionClient = NULL;
        cocos2d::network::ccHttpRequestCallback m_responseCallback;
        
        RequestEncodeType m_requestType;
        KeyValuePair* m_requestMap = NULL;
        KeyValuePair* m_extraHeader = NULL;
        std::string m_requestExtraData;
        MethodType m_methodType;
        bool m_useWebView;
        bool m_IsCancelled = false;
        
        OnResultCallback* m_OnResultCallback = NULL;
        OnProgressCallback* m_OnProgressCallback = NULL;
    };
    
    class YoutubeAuthClient : public Client {
    public:
        YoutubeAuthClient() : YoutubeAuthClient(NULL) {}
        
        YoutubeAuthClient(Client* nextActionClient) : Client(nextActionClient) {
            m_shareType = ShareType::youtube;
        }
        
        std::string getApi() {
            switch(m_apiType) {
                case AUTH:
                    return Youtube::API_AUTH;
                    
                case GET_ACCESS_TOKEN:
                case REFRESH_TOKEN:
                    return Youtube::API_TOKEN;
                case CREATE_CHANNEL:
                    return Youtube::CHANNEL_CREATION_URL;
                    
                default:
                    return NULL;
            }
        }
        
        std::string getRedirectUrl() {
            switch(m_apiType) {
                case GET_ACCESS_TOKEN:
                case AUTH:
                case REFRESH_TOKEN:
                    return Youtube::REDIRECT_URL;
                    
                case CREATE_CHANNEL:
                    return Youtube::CHANNEL_CREATION_DONE_URL;
                    
                default:
                    return NULL;
            }
            return Youtube::REDIRECT_URL;
        }
        
        std::string getTag() {
            return Youtube::TAG;
        }
        
        std::string checkError(KeyJSONValueMap& resultMap);
        void onPostResult(std::vector<std::string> resultHeader, KeyJSONValueMap& resultMap);
        
    };
    
    class YoutubeUploadClient : public Client {
        
    public:
        YoutubeUploadClient() : YoutubeUploadClient(NULL) {}
        YoutubeUploadClient(Client* nextActionClient) : Client(nextActionClient) {}
        
        std::string getApi() { return Youtube::API_START_UPLOAD; }
        std::string getRedirectUrl() { return m_redirectUrl; }
        std::string getTag() { return Youtube::TAG_START_UPLOAD; } //TODO "Youtube_Upload"
        std::string checkError(KeyJSONValueMap& resultMap);
        void onPostResult(std::vector<std::string> resultHeaders, KeyJSONValueMap& resultMap);
        
        void setRedirectUrl(std::string url) {
            m_redirectUrl = url;
        }
        
        void setUploadState(UploadState uploadState);
        
        void setUploadFilePath(std::string uploadFilePath);
        
        void setUploadFileSize(off_t fileSizeNumber);
        
        std::string getUploadFileSizeStr();
        
        inline UploadState getUploadState() { return m_uploadState; }
        UploadState m_uploadState = UploadState::NOT_STARTED;
    private:
        std::string m_redirectUrl;
        //UploadState m_uploadState = UploadState::NOT_STARTED;
        std::string m_uploadFilePath;
        std::string m_uploadFileSizeStr = "";
    };
    
    
    /**
     * Common methods
     */
    void sendAPIRequest(Client* client);
    std::string createJSONRequestString(KeyValuePair request);
    
    std::string createEncodedRequestString(KeyValuePair request);
    
    //std::string getValueFromMap(KeyJSONValueMap& resultMap);
    
    void storeOrReplaceTokens(Client* client);
    
    void parseResultAndCreateMap(std::string jsonResult, KeyJSONValueMap& resultMap);
    
    /**
     * Youtube auth start
     */
    void startAuthorizeYoutube(Client* client);
    void retrieveAuthKey(Client* client, const char* redirectUrl);
    
    /**
     * Youtube refresh token
     */
    void refreshYoutubeTokens(Client* client);
    
    /**
     * Start youtube uplaod
     */
    void startYoutubeUpload(YoutubeUploadClient* client,
                            std::string uploadFilePath,
                            std::string title,
                            std::string desc,
                            std::list<std::string> tagList);
    
    void updateYoutubeUploadAuth(YoutubeUploadClient* client);
    
}

#endif
