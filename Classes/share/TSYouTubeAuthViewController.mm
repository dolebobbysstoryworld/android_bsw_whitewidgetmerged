//
//  TSYouTubeAuthViewController.m
//  bobby
//
//  Created by Lee mu hyeon on 2014. 9. 5..
//
//

#import "TSYouTubeAuthViewController.h"
#import "ResolutionManager.h"
#import "UIImage+PathExtention.h"
#include "Share.h"

#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

using namespace Share;

@interface TSYouTubeAuthViewController () <UIWebViewDelegate> {
    Share::Client* m_client;
}

@property (nonatomic, strong) UIWebView *webView;

@end

@implementation TSYouTubeAuthViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)setClient:(Share::Client *)caller {
    self->m_client = caller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view
    NSLog(@"frame : %@", NSStringFromCGRect(self.view.frame));
    if (!IS_IOS8) {
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.height, self.view.frame.size.width);
    }
    
    CGRect bottomRect;
//    CGRect checkBoxRect;
//    CGRect doNotShowRect;
    CGRect closeButtonRect;
    
    if (IS_IPAD) {
        bottomRect = CGRectMake(0, self.view.frame.size.height - 180/2, self.view.frame.size.width, 180/2);
//        checkBoxRect = CGRectMake(44/2, (180-(86+32))/2, 86/2, 86/2);
//        doNotShowRect = CGRectMake((44+86+32)/2, (180-(50+49))/2, 496/2, 49/2);
        closeButtonRect = CGRectMake(self.view.frame.size.width-44/2-(313)/2, (180-(99+25))/2, 313/2, 99/2);
    } else {
//        if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
            bottomRect = CGRectMake(0, self.view.frame.size.height - 110/2, self.view.frame.size.width, 110/2);
//        } else {
//            bottomRect = CGRectMake(0, self.view.frame.size.width - 110/2, self.view.frame.size.height, 110/2);
//        }

//        if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
            closeButtonRect = CGRectMake(self.view.frame.size.width-28/2-(186)/2, (110-(59+14))/2, 186/2, 59/2);
//        } else {
//            closeButtonRect = CGRectMake(self.view.frame.size.height-28/2-(186)/2, (110-(59+14))/2, 186/2, 59/2);
//        }

        
        bottomRect = CGRectMake(0, self.view.frame.size.height - 110/2, self.view.frame.size.width, 110/2);
//        checkBoxRect = CGRectMake(28/2, (110-(51+19))/2, 51/2, 51/2);
//        doNotShowRect = CGRectMake((28+51+18)/2, (110-(30+30))/2, 286/2, 30/2);
        closeButtonRect = CGRectMake(self.view.frame.size.width-28/2-(186)/2, (110-(59+14))/2, 186/2, 59/2);
    }
    
    float bottomAlphaSpace = (IS_IPAD)?30/2:22/2;
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-bottomRect.size.height+bottomAlphaSpace)];
    self.webView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    self.webView.delegate = self;
    [self.view addSubview:self.webView];
    
    UIView *bottom = [[UIView alloc] initWithFrame:bottomRect];
    bottom.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamedEx:@"banner_bottom.png" ignore:YES]];
    [self.view addSubview:bottom];
    
    UIButton *closeButton = [[UIButton alloc] initWithFrame:closeButtonRect];
    [closeButton setBackgroundImage:[UIImage imageNamedEx:@"banner_close_btn.png" ignore:YES] forState:UIControlStateNormal];
    [closeButton setTitle:NSLocalizedString(@"CM_POPUP_BUTTON_TITLE_CLOSE", "Close") forState:UIControlStateNormal];
    closeButton.titleLabel.textColor = [UIColor whiteColor];
    closeButton.titleLabel.font = [UIFont fontWithName:@"helvetica-bold" size:(IS_IPAD)?50/2:30/2];
    [closeButton addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
    [bottom addSubview:closeButton];
}

- (void)requrstURL:(const char *)url {
    NSString *request = [NSString stringWithUTF8String:url];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:request]
                                               cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                           timeoutInterval:60]];
}

- (void)closeActionForEndAuth {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)closeAction:(id)sender {
    m_client->cancel();
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSLog(@"Redirecting to URL: %@", request.URL.absoluteString);
    if(m_client->getShareType() == ShareType::youtube) {
        YoutubeAuthClient* client = static_cast<YoutubeAuthClient*>(m_client);
        NSString* requestUrlStr = request.URL.absoluteString;
        NSString* redirectUrl = [NSString stringWithCString:client->getRedirectUrl().c_str()
                                                   encoding:[NSString defaultCStringEncoding]];
        if ([requestUrlStr hasPrefix:redirectUrl]) {
            // Restore Client instance here
            NSLog(@"Client from caller client = %p", client);
            if(client->getRedirectUrl() == Share::Youtube::REDIRECT_URL) {
                if([requestUrlStr hasSuffix:[NSString stringWithUTF8String:Share::Youtube::REDIRECT_ERROR.c_str()]]) {
                    m_client->cancel();
                    return NO;
                }
                
                Share::retrieveAuthKey(client, [requestUrlStr UTF8String]);
            } else {
                Share::sendAPIRequest(client->getNextActionClient());
                client->setNextActionClient(NULL);
            }
            
            [self closeActionForEndAuth];
            
            return YES;
        }
    }
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURL *url = [webView.request mainDocumentURL];
    NSLog(@"The start URL is: %@", url);
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSURL *url = [webView.request mainDocumentURL];
    NSLog(@"The finish URL is: %@", url);
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSURL *url = [webView.request mainDocumentURL];
    NSLog(@"The load failed URL is: %@", url);
    [self closeActionForEndAuth];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
