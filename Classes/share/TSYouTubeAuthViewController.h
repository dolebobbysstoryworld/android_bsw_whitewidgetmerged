//
//  TSYouTubeAuthViewController.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 9. 5..
//
//

#import <UIKit/UIKit.h>
#include "AuthWebView.h"
#include "Share.h"

@interface TSYouTubeAuthViewController : UIViewController

- (void)setClient:(Share::Client*)caller;
- (void)requrstURL:(const char*)url;
- (void)closeActionForEndAuth;

@end
