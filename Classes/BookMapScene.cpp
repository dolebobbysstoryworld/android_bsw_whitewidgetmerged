//
//  BookMapScene.cpp
//  bobby
//
//  Created by Jaemok Jeong on 4/8/14.
//
//

#include "BookMapScene.h"
#include "HomeScene.h"
#include <cocosbuilder/CocosBuilder.h>
#include "ResourceManager.h"
#include "Define.h"
#include "ChapterPlayScene.h"
#include "Util.h"
#include "AppDelegate.h"
#include "SimpleAudioEngine.h"
#include "guide/GuideMap.h"
#include "TSDoleNetProgressLayer.h"
#include "HomeScene.h"
#include "JeVideoScene.h"
#include "BGMManager.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
#include "VoiceRecorder.h"
#include "TSAccountViewController.h"
#include "TSSettingViewController.h"
#include "TSPopupViewController.h"
#include "TSFacebookVideoUploadManager.h"
#include "AppController.h"


#import "UIViewController+ChangeRootView.h"
#import "TSAccountLoginViewController.h"
#import "TSSettingMainViewController.h"
#import "TSShareCommentViewController.h"

#import "MixpanelHelper.h"

#endif
#endif

using namespace ui;

#define FEATHER_NORMAL_FILE_NAME "btn_empty_page_normal.ccbi"
#define FEATHER_PRESS_FILE_NAME "btn_empty_page_press.ccbi"

#define TAG_CHARACTER_01 1
#define TAG_CHARACTER_02 2
#define TAG_CHARACTER_03 3
#define TAG_CHARACTER_04 4
#define TAG_CHARACTER_05 5
#define TAG_PLAY_BUTTON  6

#define TAG_DELETE_01 11
#define TAG_DELETE_02 12
#define TAG_DELETE_03 13
#define TAG_DELETE_04 14
#define TAG_DELETE_05 15

#define TAG_CHAPTER_01 101
#define TAG_CHAPTER_02 102
#define TAG_CHAPTER_03 103
#define TAG_CHAPTER_04 104
#define TAG_CHAPTER_05 105

#define TAG_MENU_FEATHER_01 201
#define TAG_MENU_FEATHER_02 202
#define TAG_MENU_FEATHER_03 203
#define TAG_MENU_FEATHER_04 204
#define TAG_MENU_FEATHER_05 205

#define TAG_MENU_CHARACTER_01 301
#define TAG_MENU_CHARACTER_02 302
#define TAG_MENU_CHARACTER_03 303
#define TAG_MENU_CHARACTER_04 304
#define TAG_MENU_CHARACTER_05 305

#define POPUP_TEST 0
#define DELETE_CHAPTER_POPUP 1
#define DELETE_BTN_TAG      10

#define KEYBOARD_NOT_SHOWN -1
#define TITLE_AREA 0 
#define AUTHOR_AREA 1
#define SHARE_POPUP_YOUTUBE 1
#define SHARE_POPUP_FACEBOOK 0

#define TAG_DIMLAYER 50

#define TAG_POPUP_SHAREPOPUP        202020201
#define TAG_POPUP_SHARELIST         202020202
#define TAG_POPUP_SHAREERROR        202020203
#define TAG_POPUP_NEEDSHARELOGIN    202020204
#define TAG_POPUP_FACEBOOK_PERMISSION_ERROR 202020205
#define TAG_POPUP_DLDEVIC_NEEDLOGIN 202020206
#define TAG_POPUP_DLDEVIC           202020207

#define TAG_PROGRESS_LAYER           102310

#define ZORDER_FEAHTER 30
#define ZORDER_ITEM 10
#define ZORDER_BG_ITEM 20

#define DELAY_START_ANIMATION 0.3f
#define CUSTOM_MAP_BG "map_bg_create.ccbi"
#define CUSTOM_MAP_BG_NO_CHARACTER "map_bg_none_create.ccbi"


Scene* BookMap::createScene(__String* bookKey, bool addMode)
{
    // Mixpanel
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    
    AppDelegate* app = (AppDelegate*)Application::getInstance();
//    if(bookKey->compare("book1") == 0)
//    {
//        app->showBookA();
//    }
//    else if(bookKey->compare("book2") == 0)
//    {
//        app->showBookB();
//    }
//    else if((bookKey->compare("new_book_key") == 0))
//    {
//        app->createdCustomBook();
//    }
    
//    app->showBook(bookKey->getCString());

    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#   ifndef MACOS
    
    if(bookKey->compare("book1") == 0)
    {
        [MixpanelHelper logOpenStorybookSampleA];
    }
    else if(bookKey->compare("book2") == 0)
    {
        [MixpanelHelper logOpenStorybookSampleB];
    }
    else if((bookKey->compare("new_book_key") == 0))
    {
        [MixpanelHelper logCreateCustomStorybookStart];
    }
    
#   endif
#endif

    
    
    
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = BookMap::create(bookKey, addMode);
    layer->setTag(TAG_BOOKMAP);
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

BookMap* BookMap::create(__String* bookKey, bool addMode)
{
    BookMap *pRet = new BookMap();
    bookKey->retain();
    pRet->bookKey = bookKey;
    pRet->addMode = addMode;
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

// on "init" you need to initialize your instance
bool BookMap::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LayerColor::initWithColor( Color4B(0xba,0xea,0xed,0x00)) )
    {
        return false;
    }
    
    fromhome = true;
    finishedDownload = false;
    readyForShare = false;
    _delegate = nullptr;
    textFieldStatus = KEYBOARD_NOT_SHOWN;
    _shareTarget = ShareTarget::None;
#ifdef MACOS
    cancelEdit = false;
#endif
    // Size visibleSize = Director::getInstance()->getVisibleSize();
   
    // dole logo
    auto bgMap = Sprite::create("bg_map.png");
    bgMap->setAnchorPoint(cocos2d::Point(0,0));
    bgMap->setPosition(BOOKMAP_BG_MAP);
    this->addChild(bgMap, 0);
    
    auto btnBack = MenuItemImage::create("btn_map_back_normal.png",
                                         "btn_map_back_press.png",
                                          CC_CALLBACK_1(BookMap::btnBackCallback, this));
    btnBack->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
	btnBack->setPosition(cocos2d::Point::ZERO);
    
    auto backMenu = Menu::create(btnBack, NULL);
    backMenu->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    backMenu->setPosition(cocos2d::Point(BOOKMAP_BTN_BACK));
    this->addChild(backMenu, 2);
    
    auto menuItem = MenuItemImage::create("btn_home_menu_normal.png",
                                          "btn_home_menu_press.png",
                                          CC_CALLBACK_1(BookMap::menuCallback, this));
    menuItem->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
	menuItem->setPosition(BOOKMAP_MENU_BTN);
    
    auto menu = Menu::create(menuItem, NULL);
    menu->setPosition(cocos2d::Point::ZERO);
    this->addChild(menu, 2);
    
//    cocos2d::Point origin = Director::getInstance()->getVisibleOrigin();
//    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    
    //map_title
    auto bookTitle = "";
    auto authorTitle = "";
    if (this->bookKey) {
        auto ud = UserDefault::getInstance();
        auto bookJsonString = ud->getStringForKey(this->bookKey->getCString());
        if (bookJsonString != "") {
            this->bookJson.Parse<0>(bookJsonString.c_str());
            bookTitle = bookJson["title"].GetString();
            if (bookJson["editable"].GetBool() == false) {
                if (strcmp(bookTitle, "The Banana Brothers") == 0) {
                    bookTitle = (char*)Device::getResString(Res::Strings::CC_PRLOAD_BOOKTITLE_1);
                } else if (strcmp(bookTitle, "Bob's Journey") == 0) {
                    bookTitle = (char*)Device::getResString(Res::Strings::CC_PRLOAD_BOOKTITLE_2);
                }
            }
            authorTitle = bookJson["author"].GetString();
        }
    }
    
    std::string title(bookTitle);
    std::string author(authorTitle);
    titleString = title;
    authorString = author;
    
    log("authorString : %s",authorString.c_str());
    
    this->setTitleAndNameUI(bookTitle,authorTitle);
    
//    auto titleBg = Sprite::create("map_title_bg.png");
//    titleBg->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//    titleBg->setPosition(BOOKMAP_TITLE_BG);
//    this->addChild(titleBg, 2);
//    
//    auto bookTitle = "";
//    if (this->bookKey) {
//        auto ud = UserDefault::getInstance();
//        auto bookJsonString = ud->getStringForKey(this->bookKey->getCString());
//        if (bookJsonString != "") {
//            this->bookJson.Parse<0>(bookJsonString.c_str());
//            bookTitle = bookJson["title"].GetString();
//        }
//    }
//    
//    titleLabel = Label::createWithSystemFont(bookTitle, FONT_NAME_BOLD, BOOKMAP_TITLE_LABEL_FONTSIZE, BOOKMAP_TITLE_LABEL_SIZE, TextHAlignment::CENTER, TextVAlignment::CENTER);
//    
//    auto titleLabelButton = MenuItemSprite::create(titleLabel, titleLabel, CC_CALLBACK_1(BookMap::titleCallback, this));
//    titleLabelButton->setAnchorPoint(Point(0,0));
//    titleLabelButton->setPosition(BOOKMAP_TITLE_LABEL);
//    
//    auto titleMenu = Menu::create(titleLabelButton, NULL);
//    titleMenu->setPosition(Point::ZERO);
//    titleBg->addChild(titleMenu, 4);
//
//    titleLabel->setColor(Color3B(0x79,0x34,0x0a));
//    titleLabel->enableShadow(Color4B(0xff,0xff,0xff,153),Size(0,-0.5), 0);
//    
//    Label *subLabel = Label::createWithSystemFont("by Dolekey", FONT_NAME, BOOKMAP_SUBTITLE_LABEL_FONTSIZE, BOOKMAP_SUBTITLE_LABEL_SIZE, TextHAlignment::CENTER, TextVAlignment::CENTER);
//    subLabel->setColor(Color3B(255,235,9));
//    subLabel->setAnchorPoint(Point::ZERO);
//    subLabel->setPosition(BOOKMAP_SUBTITLE_LABEL);
//    subLabel->enableShadow(Color4B(0xa2,0x5c,0x32,153),Size(0,-0.5), 0);
//    titleBg->addChild(subLabel,4);
    
    chapter1SpritesArray = __Array::create();
    chapter1SpritesArray->retain();
    
    chapter2SpritesArray = __Array::create();
    chapter2SpritesArray->retain();

    chapter3SpritesArray = __Array::create();
    chapter3SpritesArray->retain();

    chapter4SpritesArray = __Array::create();
    chapter4SpritesArray->retain();

    chapter5SpritesArray = __Array::create();
    chapter5SpritesArray->retain();

    this->checkChapterExist();
    
    auto ud = UserDefault::getInstance();
    auto bookJsonString = ud->getStringForKey(bookKey->getCString());
    rapidjson::Document bookJson;
    bookJson.Parse<0>(bookJsonString.c_str());
    
    __Array *charKeys = __Array::create();
    __Array *bgMapKeys = __Array::create();
    __Array *characterFileNames = __Array::create();
    if (bookJson.HasMember("chapters") && !bookJson["chapters"].IsNull()) {
        rapidjson::Value& chapters = bookJson["chapters"];
        
        for (int index = 0; index < 5; index++) {
            __String* chapterName = __String::createWithFormat("chapter%d", index+1);
            rapidjson::Value& chapterDatas = chapters[chapterName->getCString()];
            if (!chapterDatas.IsNull() && chapterDatas.HasMember("char_datas")) {
                rapidjson::Value& charDatas = chapterDatas["char_datas"];
                if ( charDatas.IsArray() && charDatas.Size() > 0 ){//!charDatas.IsNull() ){
                    rapidjson::Value& dataDic = charDatas[rapidjson::SizeType(0)];
                    std::string charKey = dataDic["char_key"].GetString();
                    __String *charKeyString = __String::create(charKey);
                    
                    if (charKeys->getIndexOfObject(charKeyString) == CC_INVALID_INDEX) {
                        charKeys->addObject(charKeyString);
                    } else {
                        auto charKeyString = __String::create("C000");
                        charKeys->addObject(charKeyString);
                    }
                } else {
                    auto charKeyString = __String::create("C000");
                    charKeys->addObject(charKeyString);
                }
            } else {
                auto charKeyString = __String::create("C000");
                charKeys->addObject(charKeyString);
            }
            
            if (!chapterDatas.IsNull() && chapterDatas.HasMember("background")) {
                auto backgroundKey = chapterDatas["background"].GetString();
                __String *bgKeyString = __String::create(backgroundKey);
                bgKeyString->retain();
                bgMapKeys->addObject(bgKeyString);
            } else {
                __String *bgKeyString = __String::create("B001"); //default
                bgKeyString->retain();
                bgMapKeys->addObject(bgKeyString);
            }
            
        }
        
        for (int i=0; i < charKeys->count(); i++) {
            auto charKey = (__String *)charKeys->getObjectAtIndex(i);
            auto charJsonString = ud->getStringForKey(charKey->getCString());
            if(charJsonString.length() > 0){
                rapidjson::Document charJson;
                charJson.Parse<0>(charJsonString.c_str());
                __String *charFileName;
                if (charJson.HasMember("base_char")) { // custom character
                    auto baseChar = charJson["base_char"].GetString();
                    auto baseCharacterJsonString = ud->getStringForKey(baseChar);
                    rapidjson::Document baseCharacterInfo;
                    baseCharacterInfo.Parse<0>(baseCharacterJsonString.c_str());
                    charFileName = __String::create(baseCharacterInfo["bookmap_ccbi"].GetString());
                } else {
                    charFileName = __String::create(charJson["bookmap_ccbi"].GetString());
                }
                characterFileNames->addObject(charFileName);
//                log("characterFileName : %s", charFileName->getCString());
            } else {
                characterFileNames->addObject(__String::create("map_created_page_character_01.ccbi"));
//                log("characterFileName : %s", ((__String*)characterFileNames->getObjectAtIndex(i))->getCString());
            }
            
        }
    }    
    
    auto char1string = (__String*)characterFileNames->getObjectAtIndex(0);
    std::string char1str(char1string->getCString());
    const char *char1Name = strcpy((char*)malloc(char1str.length()+1), char1str.c_str());
    auto char2string = (__String*)characterFileNames->getObjectAtIndex(1);
    std::string char2str(char2string->getCString());
    const char *char2Name = strcpy((char*)malloc(char2str.length()+1), char2str.c_str());
    auto char3string = (__String*)characterFileNames->getObjectAtIndex(2);
    std::string char3str(char3string->getCString());
    const char *char3Name = strcpy((char*)malloc(char3str.length()+1), char3str.c_str());
    auto char4string = (__String*)characterFileNames->getObjectAtIndex(3);
    std::string char4str(char4string->getCString());
    const char *char4Name = strcpy((char*)malloc(char4str.length()+1), char4str.c_str());
    auto char5string = (__String*)characterFileNames->getObjectAtIndex(4);
    std::string char5str(char5string->getCString());
    const char *char5Name = strcpy((char*)malloc(char5str.length()+1), char5str.c_str());
    
    auto charKey1 = (__String*)charKeys->getObjectAtIndex(0);
    std::string charKey1str(charKey1->getCString());
    const char *charKey1Name = strcpy((char*)malloc(charKey1str.length()+1), charKey1str.c_str());
    auto charKey2 = (__String*)charKeys->getObjectAtIndex(1);
    std::string charKey2str(charKey2->getCString());
    const char *charKey2Name = strcpy((char*)malloc(charKey2str.length()+1), charKey2str.c_str());
    auto charKey3 = (__String*)charKeys->getObjectAtIndex(2);
    std::string charKey3str(charKey3->getCString());
    const char *charKey3Name = strcpy((char*)malloc(charKey3str.length()+1), charKey3str.c_str());
    auto charKey4 = (__String*)charKeys->getObjectAtIndex(3);
    std::string charKey4str(charKey4->getCString());
    const char *charKey4Name = strcpy((char*)malloc(charKey4str.length()+1), charKey4str.c_str());
    auto charKey5 = (__String*)charKeys->getObjectAtIndex(4);
    std::string charKey5str(charKey5->getCString());
    const char *charKey5Name = strcpy((char*)malloc(charKey5str.length()+1), charKey5str.c_str());
    
    auto sequence1 = chapter1Exist? CallFunc::create(CC_CALLBACK_0(BookMap::runCharacterSequenceAnimation, this,charKey1Name, char1Name,BOOKMAP_CHARACTER01, TAG_CHARACTER_01, this->getMapccbiName(bgMapKeys, 0, strcmp(charKey1Name, "C000")!=0), BOOKMAP_ICON_BG01, "btn_map_delete.ccbi", BOOKMAP_DELETE_BTN01, TAG_DELETE_01, TAG_CHAPTER_01)) :
    CallFunc::create(CC_CALLBACK_0(BookMap::addFeatherMenuAndRun, this,BOOKMAP_FEATHER01, TAG_CHARACTER_01, TAG_CHAPTER_01));
    
    auto sequence2 = chapter2Exist? CallFunc::create(CC_CALLBACK_0(BookMap::runCharacterSequenceAnimation, this, charKey2Name,char2Name,BOOKMAP_CHARACTER02, TAG_CHARACTER_02, this->getMapccbiName(bgMapKeys, 1,strcmp(charKey2Name, "C000")!=0), BOOKMAP_ICON_BG02, "btn_map_delete.ccbi", BOOKMAP_DELETE_BTN02, TAG_DELETE_02, TAG_CHAPTER_02)) :
    CallFunc::create(CC_CALLBACK_0(BookMap::addFeatherMenuAndRun, this,BOOKMAP_FEATHER02, TAG_CHARACTER_02, TAG_CHAPTER_02));
    
    auto sequence3 = chapter3Exist? CallFunc::create(CC_CALLBACK_0(BookMap::runCharacterSequenceAnimation, this, charKey3Name,char3Name,BOOKMAP_CHARACTER03, TAG_CHARACTER_03, this->getMapccbiName(bgMapKeys, 2,strcmp(charKey3Name, "C000")!=0), BOOKMAP_ICON_BG03, "btn_map_delete.ccbi", BOOKMAP_DELETE_BTN03, TAG_DELETE_03, TAG_CHAPTER_03)) :
    CallFunc::create(CC_CALLBACK_0(BookMap::addFeatherMenuAndRun, this,BOOKMAP_FEATHER03, TAG_CHARACTER_03, TAG_CHAPTER_03));
    
    auto sequence4 = chapter4Exist? CallFunc::create(CC_CALLBACK_0(BookMap::runCharacterSequenceAnimation, this, charKey4Name,char4Name,BOOKMAP_CHARACTER04, TAG_CHARACTER_04, this->getMapccbiName(bgMapKeys, 3,strcmp(charKey4Name, "C000")!=0), BOOKMAP_ICON_BG04, "btn_map_delete.ccbi", BOOKMAP_DELETE_BTN04, TAG_DELETE_04, TAG_CHAPTER_04)) :
    CallFunc::create(CC_CALLBACK_0(BookMap::addFeatherMenuAndRun, this,BOOKMAP_FEATHER04, TAG_CHARACTER_04, TAG_CHAPTER_04));
    
    auto sequence5 = chapter5Exist? CallFunc::create(CC_CALLBACK_0(BookMap::runCharacterSequenceAnimation, this, charKey5Name,char5Name,BOOKMAP_CHARACTER05, TAG_CHARACTER_05, this->getMapccbiName(bgMapKeys, 4,strcmp(charKey5Name, "C000")!=0), BOOKMAP_ICON_BG05, "btn_map_delete.ccbi", BOOKMAP_DELETE_BTN05, TAG_DELETE_05, TAG_CHAPTER_05)) :
    CallFunc::create(CC_CALLBACK_0(BookMap::addFeatherMenuAndRun, this,BOOKMAP_FEATHER05, TAG_CHARACTER_05, TAG_CHAPTER_05));
    
    auto characterAllAction = Sequence::create(DelayTime::create(DELAY_START_ANIMATION),
                                               sequence1,
                                               DelayTime::create(0.1f),
                                               sequence2,
                                               DelayTime::create(0.1f),
                                               sequence3,
                                               DelayTime::create(0.1f),
                                               sequence4,
                                               DelayTime::create(0.1f),
                                               sequence5, NULL);
    runAction(characterAllAction);

    auto playBtn = this->getCcbiSprite("btn_map_all_play.ccbi", cocos2d::Point::ZERO);
    Sprite *ship = (Sprite*)playBtn->getChildByTag(100);
    LanguageType currentLanguageType = Application::getInstance()->getCurrentLanguage();
    switch (currentLanguageType) {
        case LanguageType::KOREAN:
        {
            if(ship) ship->setTexture("btn_map_play_normal_ko.png");
            break;
        }
        case LanguageType::JAPANESE: {
            if(ship) ship->setTexture("btn_map_play_normal_jp.png");
            break;
        }
        case LanguageType::ENGLISH:
        default: {
            if(ship) ship->setTexture("btn_map_play_normal.png");
            break;
        }
    }
    
    if (playBtn != NULL) {
        playMenu = this->addMenu(playBtn, playBtn, BOOKMAP_PLAY_BTN, TAG_PLAY_BUTTON, true);
        this->setPlayMenuVisible();
    }
    
////    characterView
//    auto characterAllAction = Sequence::create(
//                                               CallFunc::create(CC_CALLBACK_0(BookMap::runCharacterSequenceAnimation, this, "map_created_page_character_01.ccbi",BOOKMAP_CHARACTER01, TAG_CHARACTER_01, "map_bg_01.ccbi", BOOKMAP_ICON_BG01, "btn_map_delete.ccbi", BOOKMAP_DELETE_BTN01, TAG_DELETE_01, TAG_CHAPTER_01)),
//                                               DelayTime::create(0.1f),
//                                               CallFunc::create(CC_CALLBACK_0(BookMap::runCharacterSequenceAnimation, this, "map_created_page_character_02.ccbi",BOOKMAP_CHARACTER02, TAG_CHARACTER_02, "map_bg_01.ccbi", BOOKMAP_ICON_BG02, "btn_map_delete.ccbi", BOOKMAP_DELETE_BTN02, TAG_DELETE_02, TAG_CHAPTER_02)),
//                                               DelayTime::create(0.1f),
//                                               CallFunc::create(CC_CALLBACK_0(BookMap::runCharacterSequenceAnimation, this, "map_created_page_character_03.ccbi",BOOKMAP_CHARACTER03, TAG_CHARACTER_03, "map_bg_03.ccbi", BOOKMAP_ICON_BG03, "btn_map_delete.ccbi", BOOKMAP_DELETE_BTN03, TAG_DELETE_03, TAG_CHAPTER_03)),
//                                               DelayTime::create(0.1f),
//                                               CallFunc::create(CC_CALLBACK_0(BookMap::runCharacterSequenceAnimation, this, "map_created_page_character_04.ccbi",BOOKMAP_CHARACTER04, TAG_CHARACTER_04, "map_bg_04.ccbi", BOOKMAP_ICON_BG04, "btn_map_delete.ccbi", BOOKMAP_DELETE_BTN04, TAG_DELETE_04, TAG_CHAPTER_04)),
//                                               DelayTime::create(0.1f),
//                                               CallFunc::create(CC_CALLBACK_0(BookMap::runCharacterSequenceAnimation, this, "map_created_page_character_05.ccbi",BOOKMAP_CHARACTER05, TAG_CHARACTER_05, "map_bg_05.ccbi", BOOKMAP_ICON_BG05, "btn_map_delete.ccbi", BOOKMAP_DELETE_BTN05, TAG_DELETE_05, TAG_CHAPTER_05)),
//                                               DelayTime::create(0.1f),
//                                               NULL);
//    runAction(characterAllAction);
    
//    featherView
    
//    auto featherAllAction = Sequence::create(
//                                             CallFunc::create(CC_CALLBACK_0(BookMap::addFeatherMenuAndRun, this,BOOKMAP_FEATHER01, TAG_CHARACTER_01)),
//                                             DelayTime::create(0.1f),
//                                             CallFunc::create(CC_CALLBACK_0(BookMap::addFeatherMenuAndRun, this,BOOKMAP_FEATHER02, TAG_CHARACTER_02)),
//                                             DelayTime::create(0.1f),
//                                             CallFunc::create(CC_CALLBACK_0(BookMap::addFeatherMenuAndRun, this,BOOKMAP_FEATHER03, TAG_CHARACTER_03)),
//                                             DelayTime::create(0.1f),
//                                             CallFunc::create(CC_CALLBACK_0(BookMap::addFeatherMenuAndRun, this,BOOKMAP_FEATHER04, TAG_CHARACTER_04)),
//                                             DelayTime::create(0.1f),
//                                             CallFunc::create(CC_CALLBACK_0(BookMap::addFeatherMenuAndRun, this,BOOKMAP_FEATHER05, TAG_CHARACTER_05)),
//                                             NULL);
//    runAction(featherAllAction);
    
    auto intro = Sprite::create("map_title_page_01.png");
    intro->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    intro->setPosition(BOOKMAP_INTRO);
    this->addChild(intro, 2);
    
    auto introLabel = this->getFlagLabel(Device::getResString(Res::Strings::CC_JOURNEYMAP_BUTTON_INTRO), BOOKMAP_FLAG_SIZE_TYPE01, BOOKMAP_FLAG_POS_TYPE01, 1);// "Intro"
    intro->addChild(introLabel);
    
    auto final = Sprite::create("map_title_page_05.png");
    final->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    final->setPosition(BOOKMAP_FINAL);
    this->addChild(final, 2);
    
    auto finalLabel = this->getFlagLabel(Device::getResString(Res::Strings::CC_JOURNEYMAP_BUTTON_FINAL), BOOKMAP_FLAG_SIZE_TYPE01, BOOKMAP_FLAG_POS_TYPE01, 5);//"Final"
    final->addChild(finalLabel);
    
    auto scene1 = Sprite::create("map_title_page_02.png");
    scene1->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    scene1->setPosition(BOOKMAP_SCENE_2);
    this->addChild(scene1, 2);
    
    auto scene1Label = this->getFlagLabel("2", BOOKMAP_FLAG_SIZE_TYPE02, BOOKMAP_FLAG_POS_TYPE02, 2);
    scene1->addChild(scene1Label);
    
    auto scene2 = Sprite::create("map_title_page_03.png");
    scene2->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    scene2->setPosition(BOOKMAP_SCENE_3);
    this->addChild(scene2, 2);
    
    auto scene2Label = this->getFlagLabel("3", BOOKMAP_FLAG_SIZE_TYPE02, BOOKMAP_FLAG_POS_TYPE02, 3);
    scene2->addChild(scene2Label);
    
    auto scene3 = Sprite::create("map_title_page_04.png");
    scene3->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    scene3->setPosition(BOOKMAP_SCENE_4);
    this->addChild(scene3, 2);
    
    auto scene3Label = this->getFlagLabel("4", BOOKMAP_FLAG_SIZE_TYPE02, BOOKMAP_FLAG_POS_TYPE02, 4);
    scene3->addChild(scene3Label);
    
    isPlatformShown = false;
    textEdited = false;

    if (this->bookJson["editable"].GetBool()) {
        GuideMap::create(this, GuideMap::Type::Select);
    }
    
    return true;
}

const char* BookMap::getMapccbiName(__Array *bgMapKeys,int index, bool characterExist)
{
    auto ud = UserDefault::getInstance();
    auto bookJsonString = ud->getStringForKey(bookKey->getCString());
    rapidjson::Document bookJson;
    bookJson.Parse<0>(bookJsonString.c_str());
    
    auto bgKeyString = (__String*)bgMapKeys->getObjectAtIndex(index);
    auto backgroundString = ud->getStringForKey(bgKeyString->getCString());
    rapidjson::Document backgroundInfo;
    backgroundInfo.Parse<0>(backgroundString.c_str());
    
    const char *mapCcbiNameString = nullptr;
    if (backgroundInfo["custom"].GetBool()) {
        if (characterExist) {
            auto *customMapCcbi = CUSTOM_MAP_BG;
            mapCcbiNameString = customMapCcbi;
        } else {
            auto *customMapCcbi = CUSTOM_MAP_BG_NO_CHARACTER;
            mapCcbiNameString = customMapCcbi;
        }
    } else {
        if (characterExist) {
            auto mapCcbiName = backgroundInfo["map_ccbi"].GetString();
            std::string mapCcbiNameStr(mapCcbiName);
            mapCcbiNameString = strcpy((char*)malloc(mapCcbiNameStr.length()+1), mapCcbiNameStr.c_str());
        } else {
            auto mapCcbiName = backgroundInfo["map_ccbi_no_character"].GetString();
            std::string mapCcbiNameStr(mapCcbiName);
            mapCcbiNameString = strcpy((char*)malloc(mapCcbiNameStr.length()+1), mapCcbiNameStr.c_str());
        }
    }
    return mapCcbiNameString;
}

void BookMap::setTitleAndNameUI(const std::string &title, const std::string &nameTitle)
{
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    auto leftBg = Sprite::create("map_title_bg_left.png");
    leftBg->setAnchorPoint(cocos2d::Point(0,0));
    
    std::string stripTitle = title;
    while(std::isspace(*stripTitle.rbegin()))
        stripTitle.erase(stripTitle.length()-1);
    
    auto titleLabel = Label::createWithSystemFont(stripTitle, FONT_NAME_BOLD, BOOKMAP_TITLE_LABEL_FONTSIZE, cocos2d::Size(1,BOOKMAP_SINGLE_TITLE_LABEL_SIZE.height), TextHAlignment::CENTER, TextVAlignment::CENTER);
    titleLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    
    log("stripTitle : %s count: %lu", stripTitle.c_str(), stripTitle.length());
    
    int titleStringWidth = Device::getTextWidth(stripTitle.c_str(), FONT_NAME_BOLD, BOOKMAP_TITLE_LABEL_FONTSIZE);
    LabelExtension* subTitleLabel = NULL;
    
    bool isDoubleLine = (titleStringWidth > BOOKMAP_DOUBLE_TITLE_LABEL_SIZE.width)?true:false;
    
    if (isDoubleLine) {
        titleLabel->setWidth(BOOKMAP_DOUBLE_TITLE_LABEL_SIZE.width);
        titleLabel->setPosition(BOOKMAP_DOUBLE_TITLE_LABEL_01);
        
        std::string bookTitleMain, bookTitleSub;
        Util::splitTitleString(stripTitle, ' ', bookTitleMain, bookTitleSub, BOOKMAP_DOUBLE_TITLE_LABEL_SIZE.width, FONT_NAME_BOLD, BOOKMAP_TITLE_LABEL_FONTSIZE);

        log("bookTitleMain : %s, sub : %s, subcount : %lu", bookTitleMain.c_str(), bookTitleSub.c_str(), bookTitleSub.length());
        
        titleLabel->setString(bookTitleMain);
        
        subTitleLabel = LabelExtension::create(bookTitleSub, FONT_NAME_BOLD, BOOKMAP_TITLE_LABEL_FONTSIZE, BOOKMAP_DOUBLE_TITLE_LABEL_SIZE, TextHAlignment::CENTER, TextVAlignment::CENTER);
        subTitleLabel->ellipsis(FONT_NAME_BOLD, BOOKMAP_TITLE_LABEL_FONTSIZE);
        subTitleLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        subTitleLabel->setColor(Color3B(0x88,0x47,0x01));
        subTitleLabel->setPosition(BOOKMAP_DOUBLE_TITLE_LABEL_02);
        subTitleLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
        subTitleLabel->enableShadow(Color4B(255,255,255,255.0f*0.6f), cocos2d::Size(0,-1), 0);
    } else {
        if (titleStringWidth < BOOKMAP_SINGLE_TITLE_LABEL_SIZE.width) {
            titleLabel->setWidth(BOOKMAP_SINGLE_TITLE_LABEL_SIZE.width);
        } else {
        	titleLabel->setWidth(titleStringWidth);
        }
        titleLabel->setHorizontalAlignment(TextHAlignment::CENTER);
//        log("titleLabel width : %f", titleLabel->getContentSize().width);
        titleLabel->setPosition(cocos2d::Point(BOOKMAP_SINGLE_TITLE_LABEL));
    }
    titleLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    titleLabel->setColor(Color3B(0x88,0x47,0x01));
    titleLabel->enableShadow(Color4B(255,255,255,255.0f*0.6f), cocos2d::Size(0,-1), 0);
    
    //bg
    Texture2D *texture = Director::getInstance()->getTextureCache()->addImage("map_title_bg_center.png");
    const Texture2D::TexParams &tp = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE};
    texture->setTexParameters(tp);
    float bgWidth = titleLabel->getContentSize().width-((leftBg->getContentSize().width-BOOKMAP_SINGLE_TITLE_LABEL.x)*2);
    if (titleLabel->getString().length() <=0 && isDoubleLine) {
        bgWidth = subTitleLabel->getContentSize().width-((leftBg->getContentSize().width-BOOKMAP_SINGLE_TITLE_LABEL.x)*2);
    }
    auto background = Sprite::createWithTexture(texture, cocos2d::Rect(0, 0, bgWidth, texture->getContentSize().height));
    background->setAnchorPoint(cocos2d::Point(0,0));
	background->setPosition(cocos2d::Point(leftBg->getContentSize().width,0));
    
    //name label
    std::string stripName = nameTitle;
    while(std::isspace(*stripName.rbegin()))
        stripName.erase(stripName.length()-1);
    
    log("stripName : %s count: %lu", stripName.c_str(), stripName.length());
    
    auto nameLabel = Label::createWithSystemFont(stripName, FONT_NAME, BOOKMAP_NAME_LABEL_FONTSIZE, cocos2d::Size(1,BOOKMAP_SINGLE_NAME_LABEL_SIZE.height), TextHAlignment::CENTER, TextVAlignment::CENTER);
    nameLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    
    int nameStringWidth = Device::getTextWidth(stripName.c_str(), FONT_NAME, BOOKMAP_NAME_LABEL_FONTSIZE);
    LabelExtension* lastNameLabel = NULL;
    
    bool isNameDoubleLine = (nameStringWidth > BOOKMAP_DOUBLE_NAME_LABEL_SIZE.width)?true:false;
    
    if (isNameDoubleLine) {
        nameLabel->setWidth(BOOKMAP_DOUBLE_NAME_LABEL_SIZE.width);
        if (titleLabel->getString().length() <=0 && isDoubleLine) {
            nameLabel->setPosition(cocos2d::Point(leftBg->getContentSize().width+subTitleLabel->getContentSize().width+BOOKMAP_SINGLE_NAME_LABEL.x-(leftBg->getContentSize().width-BOOKMAP_SINGLE_TITLE_LABEL.x), BOOKMAP_DOUBLE_NAME_LABEL_01.y));
        } else {
            nameLabel->setPosition(cocos2d::Point(leftBg->getContentSize().width+titleLabel->getContentSize().width+BOOKMAP_SINGLE_NAME_LABEL.x-(leftBg->getContentSize().width-BOOKMAP_SINGLE_TITLE_LABEL.x), BOOKMAP_DOUBLE_NAME_LABEL_01.y));
        }
        
        std::string authorMain, authorSub;
        Util::splitTitleString(stripName, ' ', authorMain, authorSub, BOOKMAP_DOUBLE_NAME_LABEL_SIZE.width, FONT_NAME, BOOKMAP_NAME_LABEL_FONTSIZE);
        
        nameLabel->setString(authorMain);
        
        lastNameLabel = LabelExtension::create(authorSub, FONT_NAME, BOOKMAP_NAME_LABEL_FONTSIZE, BOOKMAP_DOUBLE_NAME_LABEL_SIZE, TextHAlignment::CENTER, TextVAlignment::CENTER);
        lastNameLabel->ellipsis(FONT_NAME, BOOKMAP_NAME_LABEL_FONTSIZE);
        lastNameLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        lastNameLabel->setColor(Color3B(0x9c,0x52,0x00));
        lastNameLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
        
        if (titleLabel->getString().length() <=0 && isDoubleLine) {
            lastNameLabel->setPosition(cocos2d::Point(leftBg->getContentSize().width+subTitleLabel->getContentSize().width+BOOKMAP_SINGLE_NAME_LABEL.x-(leftBg->getContentSize().width-BOOKMAP_SINGLE_TITLE_LABEL.x), BOOKMAP_DOUBLE_NAME_LABEL_02.y));
        } else {
            lastNameLabel->setPosition(cocos2d::Point(leftBg->getContentSize().width+titleLabel->getContentSize().width+BOOKMAP_SINGLE_NAME_LABEL.x-(leftBg->getContentSize().width-BOOKMAP_SINGLE_TITLE_LABEL.x), BOOKMAP_DOUBLE_NAME_LABEL_02.y));
        }
        lastNameLabel->enableShadow(Color4B(255,255,255,255.0f*0.6f), cocos2d::Size(0,-1), 0);
    } else {
        if (nameLabel->getContentSize().width < BOOKMAP_SINGLE_NAME_LABEL_SIZE.width) {
            nameLabel->setWidth(BOOKMAP_SINGLE_NAME_LABEL_SIZE.width);
            nameLabel->setHorizontalAlignment(TextHAlignment::CENTER);
        }
        if (titleLabel->getString().length() <=0 && isDoubleLine) {
            nameLabel->setPosition(cocos2d::Point(leftBg->getContentSize().width+subTitleLabel->getContentSize().width+BOOKMAP_SINGLE_NAME_LABEL.x-(leftBg->getContentSize().width-BOOKMAP_SINGLE_TITLE_LABEL.x), BOOKMAP_SINGLE_NAME_LABEL.y));
        } else {
            nameLabel->setPosition(cocos2d::Point(leftBg->getContentSize().width+titleLabel->getContentSize().width+BOOKMAP_SINGLE_NAME_LABEL.x-(leftBg->getContentSize().width-BOOKMAP_SINGLE_TITLE_LABEL.x), BOOKMAP_SINGLE_NAME_LABEL.y));
        }
        
    }
    nameLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    nameLabel->setColor(Color3B(0x9c,0x52,0x00));
    nameLabel->enableShadow(Color4B(255,255,255,255.0f*0.6f), cocos2d::Size(0,-1), 0);
    
    auto rightBg = Sprite::create("map_title_bg_right.png");
    rightBg->setAnchorPoint(cocos2d::Point(0,0));
    rightBg->setPosition(cocos2d::Point(leftBg->getContentSize().width+background->getContentSize().width,0));
    
    contentsSprite = Sprite::create();
    contentsSprite->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_BOTTOM);
    contentsSprite->setContentSize(cocos2d::Size(leftBg->getContentSize().width+rightBg->getContentSize().width+background->getContentSize().width, leftBg->getContentSize().height));
	contentsSprite->setPosition(cocos2d::Point(visibleSize.width/2,BOOKMAP_TITLE_Y));
	this->addChild(contentsSprite, 5);
    
    contentsSprite->addChild(leftBg);
    contentsSprite->addChild(background);
    contentsSprite->addChild(rightBg);
    contentsSprite->addChild(titleLabel);
    contentsSprite->addChild(nameLabel);
    if (isDoubleLine) contentsSprite->addChild(subTitleLabel);
        
    if (isNameDoubleLine) contentsSprite->addChild(lastNameLabel);
}

void BookMap::popupCallback(Ref* pSender)
{
    //    Director::getInstance()->pause();
    //    auto dimlayer = TSDimLayer::create();
    //    this->addChild(dimlayer);
    //    return;
    //
    Director::getInstance()->pause();
    PopupLayer *modal;
    if (Application::getInstance()->getCurrentLanguage() != LanguageType::JAPANESE) {
        modal = PopupLayer::create(POPUP_TYPE_WARNING,
                                   Device::getResString(Res::Strings::CC_JOURNEYMAP_POPUP_WARNING_DELETE_PAGE),//"Delete Chapter?",
                                   Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL),//"Cancel",
                                   Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK));//"OK");
    } else {
        modal = PopupLayer::create(POPUP_TYPE_WARNING,
                                   Device::getResString(Res::Strings::CC_JOURNEYMAP_POPUP_WARNING_DELETE_PAGE),//"Delete Chapter?",
                                   Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_NO),//"Cancel",
                                   Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_YES));//"OK");
    }
    modal->setTag(DELETE_CHAPTER_POPUP);
    modal->setDelegate(this);
    this->addChild(modal, 1000);
}

void BookMap::popupButtonClicked(PopupLayer *popup, cocos2d::Ref *obj)
{
    Director::getInstance()->resume();
    auto clickedButton = (MenuItem*)obj;
    int tag = clickedButton->getTag();
    
    if (popup->getTag() == DELETE_CHAPTER_POPUP) {
        if (tag == 0) {
            log("delete cancelButton clicked!!");
        } else {
            log("delete okButton clicked!!");
            
            AppDelegate* app = (AppDelegate*)Application::getInstance();
            if(app != nullptr)
            {
                app->mixpanelTrack("Deleted Custom Story", NULL);
            }
            
            auto ud = UserDefault::getInstance();
            
            rapidjson::Value& chapters = bookJson["chapters"];
            rapidjson::Value nullValue(rapidjson::kNullType);
            
            __String *chapterName = NULL;
            
            switch (removingChapter) {
                case 1:
                {
                    chapterName = __String::create("chapter1");
                    break;
                }
                case 2:
                {
                    chapterName = __String::create("chapter2");
                    break;
                }
                case 3:
                {
                    chapterName = __String::create("chapter3");
                    break;
                }
                case 4:
                {
                    chapterName = __String::create("chapter4");
                    break;
                }
                case 5:
                {
                    chapterName = __String::create("chapter5");
                    break;
                }
                default:
                    return;
            }
            
            rapidjson::Value& chapterData = chapters[chapterName->getCString()];
            if (!chapterData["timeline_file"].IsNull()) {
                auto oldtimeline = chapterData["timeline_file"].GetString();
                ud->setStringForKey(oldtimeline, "");
            }
            
            if (!chapterData["voice_file"].IsNull()) {
                auto voiceFileName = chapterData["voice_file"].GetString();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
                NSString *fileName = [NSString stringWithUTF8String:voiceFileName];
                VoiceRecorder *voiceRecorder = [VoiceRecorder sharedRecorder];
                [voiceRecorder removeFile:fileName];
#endif
#endif
            }
            
            chapterData = nullValue;
            
            rapidjson::StringBuffer strbuf;
            rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
            this->bookJson.Accept(writer);
            auto resultJsonString = strbuf.GetString();
            
            ud->setStringForKey(this->bookKey->getCString(), resultJsonString);
            ud->flush();
            
            this->reloadChapterData();
            log("delete action");
            this->setPlayMenuVisible();
        }
    } else if ( popup->getTag() == TAG_POPUP_SHAREPOPUP ) {
        if (tag == 0) {
            log("cancelButton clicked!");
        } else {
            //    start recording for share
            auto director = Director::getInstance();
            auto playAndRecordScene = PlayAndRecord::createScene(this->bookKey, PlayAndRecordType::RecordForShare);
            auto playAndRecordLayer = (PlayAndRecord *)playAndRecordScene->getChildByTag(TAG_PLAY_AND_RECORDING);
            playAndRecordLayer->setDelegate(this);
            auto transition = TransitionProgressInOut::create(0.4f, playAndRecordScene);
            director->pushScene(transition);
        }
    } else if ( popup->getTag() == TAG_POPUP_NEEDSHARELOGIN ) {
        if (tag == 0) {
            log("cancelButton clicked!");
        } else {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
            AppDelegate* app = (AppDelegate*)Application::getInstance();
            if(app != nullptr) {
                app->setPendingAction(ANDROID_SHARE);
                app->showPlatformView(ANDROID_LOGIN, TAG_BOOKMAP);
            }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#ifdef UI_TYPE_IOS8
            TSAccountLoginViewController *loginVC = [[TSAccountLoginViewController alloc] init];
            loginVC.originLayer = TSOriginLayerBookMap;
            loginVC.recallType = TSRecallBookMapSharePopup;
            [UIViewController replaceRootViewControlller:loginVC];
//            [loginVC release];
#else//UI_TYPE_IOS8
            UIViewController *viewController = nil;
            if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
            } else {
                viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
            }
            NSLog(@"viewController screen frame : %@",NSStringFromCGRect(viewController.view.frame));
            TSAccountViewController *accountViewController = [[TSAccountViewController alloc] initWithFirstRun:false];
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                accountViewController.modalPresentationStyle = UIModalPresentationFormSheet;
            }
            accountViewController.originLayer = TSOriginLayerBookMap;
            accountViewController.recallType = TSRecallBookMapSharePopup;
            if(IS_IOS8 && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                accountViewController.preferredContentSize = CGSizeMake(1024, 768);
            }
            [viewController presentViewController:accountViewController animated:YES completion:nil];
#endif//UI_TYPE_IOS8
#endif
        }
        
    } else if ( popup->getTag() == TAG_POPUP_DLDEVIC_NEEDLOGIN ) {
        if (tag == 0) {
            log("cancelButton clicked!");
        } else {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
            AppDelegate* app = (AppDelegate*)Application::getInstance();
            if(app != nullptr) {
                app->setPendingAction(ANDROID_DOWNLOAD_TO_DEVICE);
                app->showPlatformView(ANDROID_LOGIN, TAG_BOOKMAP);
            }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#ifdef UI_TYPE_IOS8
            TSAccountLoginViewController *loginVC = [[TSAccountLoginViewController alloc] init];
            loginVC.originLayer = TSOriginLayerBookMap;
            loginVC.recallType = TSRecallBookMapDLDevicePopup;
            [UIViewController replaceRootViewControlller:loginVC];
//            [loginVC release];
#else//UI_TYPE_IOS8
            UIViewController *viewController = nil;
            if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
            } else {
                viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
            }
            NSLog(@"viewController screen frame : %@",NSStringFromCGRect(viewController.view.frame));
            TSAccountViewController *accountViewController = [[TSAccountViewController alloc] initWithFirstRun:false];
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                accountViewController.modalPresentationStyle = UIModalPresentationFormSheet;
            }
            accountViewController.originLayer = TSOriginLayerBookMap;
            accountViewController.recallType = TSRecallBookMapDLDevicePopup;
            if(IS_IOS8 && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                accountViewController.preferredContentSize = CGSizeMake(1024, 768);
            }
            [viewController presentViewController:accountViewController animated:YES completion:nil];
#endif//UI_TYPE_IOS8
#endif
        }
    } else if ( popup->getTag() == TAG_POPUP_DLDEVIC ) {
        if (tag == 0) {
            log("cancelButton clicked!");
        } else {
            auto director = Director::getInstance();
            auto playAndRecordScene = PlayAndRecord::createScene(this->bookKey, PlayAndRecordType::RecordForDownload);
            auto playAndRecordLayer = (PlayAndRecord *)playAndRecordScene->getChildByTag(TAG_PLAY_AND_RECORDING);
            playAndRecordLayer->setDelegate(this);
            auto transition = TransitionProgressInOut::create(0.4f, playAndRecordScene);
            director->pushScene(transition);
        }
    } else if ( popup->getTag() == TAG_POPUP_SHARELIST ) {
        if (tag == 0) {
            log("cancelButton clicked!");
            bool result = Device::deleteFile(movieFilePath.c_str());
            if (result) {
                log("movie file delete success");
            } else {
                log("movie file delete fail");
            }
        }
    } else {
        if (tag == 0) {
            log("cancelButton clicked!");
        } else {
            log("okButton clicked!!");
        }
    }
}

void BookMap::onEnterTransitionDidFinish() {
    if(readyForShare == true) {
        readyForShare = false;
        if (!movieFilePath.empty()){
            // TODO: upload movie
            log("upload file : %s", movieFilePath.c_str());
            this->showShareCommentView();
        }
    }
}

void BookMap::onEnter()
{
    TSBGMManager::getInstance()->play();
    LayerColor::onEnter();
    log("enter");
    blockBackKey = false;
    if (!fromhome) {
        this->reloadChapterData();
    } else {
        fromhome = false;
    }
    
    if (finishedDownload){
        finishedDownload = false;
        auto toastLayer = ToastLayer::create(Device::getResString(Res::Strings::CC_JOURNEYMAP__DOWNLOAD_DEVICE_COMPLETE), 2);
        toastLayer->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        toastLayer->setPosition(cocos2d::Point::ZERO);
        this->addChild(toastLayer, 1000);
    } else if (readyForShare) {
//        readyForShare = false;
//        if (!movieFilePath.empty()){
//            // TODO: upload movie
//            log("upload file : %s", movieFilePath.c_str());
////            PopupLayer *share = PopupLayer::create(POPUP_TYPE_SHARED,
////                                                   Device::getResString(Res::Strings::CC_JOURNEYMAP_POPUP_TITLE_SHARE_LIST),//"Share",
////                                                   Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL));//"cancel");
////            share->setDelegate(this);
////            share->setTag(TAG_POPUP_SHARELIST);
////            this->addChild(share, 1000);
//            auto sequence = Sequence::create(DelayTime::create(0.2f), CallFunc::create(CC_CALLBACK_0(BookMap::showShareCommentView, this)), NULL);
//            this->runAction(sequence);
////            this->showShareCommentView();
//        }
    }
    
    this->setPlayMenuVisible();
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(false);
    
    listener->onTouchBegan = [=](Touch* touch, Event* event) {
        if (textFieldStatus != KEYBOARD_NOT_SHOWN) {
#ifdef MACOS
            cancelEdit = true;
            if (textFieldStatus == TITLE_AREA) {
//                titleTextField->detachWithIME();
                titleTextField->onExit();
            } else {
//                authorTextField->detachWithIME();
                authorTextField->onExit();
            }
            this->removeInputTitleSpriteWithChildren();
#endif
        }
        return true;
    };
    
    listener->onTouchMoved = [=](Touch* touch, Event* event){};
    
    listener->onTouchEnded = [=](Touch* touch, Event* event){
        if (menuListLayer) {
            if (menuListLayer->menuListVisible) {
                menuListLayer->menuListControl(false);
            }
        }
        
        if (inputTitleSprite) return;
        
        bool editable = bookJson["editable"].GetBool();
        if (!editable) return;
        
        auto target = event->getCurrentTarget();
        cocos2d::Point locationInNode = target->convertToNodeSpace(touch->getLocation());
        cocos2d::Rect titleRect = cocos2d::Rect(contentsSprite->getPosition().x - contentsSprite->getContentSize().width/2,contentsSprite->getPosition().y, contentsSprite->getContentSize().width-BOOKMAP_AUTHOR_TOUCH_WIDTH, contentsSprite->getContentSize().height);
        cocos2d::Rect authorRect = cocos2d::Rect(contentsSprite->getPosition().x - contentsSprite->getContentSize().width/2+ contentsSprite->getContentSize().width-BOOKMAP_AUTHOR_TOUCH_WIDTH,contentsSprite->getPosition().y, BOOKMAP_AUTHOR_TOUCH_WIDTH, contentsSprite->getContentSize().height);
        
        if (textFieldStatus == KEYBOARD_NOT_SHOWN) {
	        if (titleRect.containsPoint(locationInNode)) {
	            log("touched");
	            this->titleCallback(TITLE_AREA);
	            this->addDimLayer();
	        } else if (authorRect.containsPoint(locationInNode)) {
	            log("author");
	            this->titleCallback(AUTHOR_AREA);
	            this->addDimLayer();
	        }
	    }
    };
    
    listener->onTouchCancelled = [=](Touch* touch, Event* event){ };
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
   
    auto keyListener = EventListenerKeyboard::create();
    keyListener->onKeyReleased = CC_CALLBACK_2(BookMap::onKeyReleased, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(keyListener, this);
}

void BookMap::addDimLayer()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    auto dimLayer = LayerColor::create(cocos2d::Color4B(0,0,0,255.0f*0.6f));
    dimLayer->setContentSize(cocos2d::Size(visibleSize.width, visibleSize.height));
    dimLayer->setTag(TAG_DIMLAYER);
    this->addChild(dimLayer, 50);
#endif
}

void BookMap::removeDimLayer()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
    auto dimLayer = this->getChildByTag(TAG_DIMLAYER);
    if (dimLayer)
        dimLayer->removeFromParentAndCleanup(true);
#endif
}

void BookMap::onExit()
{
//    _eventDispatcher->removeAllEventListeners();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	_eventDispatcher->removeEventListenersForTarget(this);
#endif

    Layer::onExit();
}

void BookMap::checkChapterExist()
{
    if (this->bookKey) {
        auto ud = UserDefault::getInstance();
        auto bookJsonString = ud->getStringForKey(this->bookKey->getCString());
        if (bookJsonString != "") {
            this->bookJson.Parse<0>(bookJsonString.c_str());
            rapidjson::Value& chapterData = bookJson["chapters"];
            
            rapidjson::Value& chapter1Data = chapterData["chapter1"];
            rapidjson::Value& chapter2Data = chapterData["chapter2"];
            rapidjson::Value& chapter3Data = chapterData["chapter3"];
            rapidjson::Value& chapter4Data = chapterData["chapter4"];
            rapidjson::Value& chapter5Data = chapterData["chapter5"];

            chapter1Exist = !chapter1Data.IsNull() && chapter1Data.HasMember("char_datas");
            chapter2Exist = !chapter2Data.IsNull() && chapter2Data.HasMember("char_datas");
            chapter3Exist = !chapter3Data.IsNull() && chapter3Data.HasMember("char_datas");
            chapter4Exist = !chapter4Data.IsNull() && chapter4Data.HasMember("char_datas");
            chapter5Exist = !chapter5Data.IsNull() && chapter5Data.HasMember("char_datas");
        }
    }
}

void BookMap::reloadChapterData()
{
    if (this->bookKey) {
        auto ud = UserDefault::getInstance();
        auto bookJsonString = ud->getStringForKey(this->bookKey->getCString());
//        log("bookJsonString :%s", bookJsonString.c_str());
        if (bookJsonString != "") {
            this->bookJson.Parse<0>(bookJsonString.c_str());
            rapidjson::Value& chapterData = bookJson["chapters"];
            
            rapidjson::Value& chapter1Data = chapterData["chapter1"];
            rapidjson::Value& chapter2Data = chapterData["chapter2"];
            rapidjson::Value& chapter3Data = chapterData["chapter3"];
            rapidjson::Value& chapter4Data = chapterData["chapter4"];
            rapidjson::Value& chapter5Data = chapterData["chapter5"];
            
            // TODO: resolve memory issue (malloc, retain)
            
            __Array *charKeys = __Array::create();
            __Array *bgMapKeys = __Array::create();
            __Array *characterFileNames = __Array::create();
            if (bookJson.HasMember("chapters") && !bookJson["chapters"].IsNull()) {
                rapidjson::Value& chapters = bookJson["chapters"];
                
                for (int index = 0; index < 5; index++) {
                    __String* chapterName = __String::createWithFormat("chapter%d", index+1);
                    rapidjson::Value& chapterDatas = chapters[chapterName->getCString()];
                    if ( !chapterDatas.IsNull() && chapterDatas.HasMember("char_datas") ) {
                        rapidjson::Value& charDatas = chapterDatas["char_datas"];
                        if ( charDatas.IsArray() && charDatas.Size() > 0 ){//!charDatas.IsNull() ){
                            rapidjson::Value& dataDic = charDatas[rapidjson::SizeType(0)];
                            std::string charKey = dataDic["char_key"].GetString();
                            __String *charKeyString = __String::create(charKey);
                            charKeyString->retain();
                            if (charKeys->getIndexOfObject(charKeyString) == CC_INVALID_INDEX) {
                                charKeys->addObject(charKeyString);
                            } else {
                                auto charKeyString = __String::create("C000");
                                charKeyString->retain();
                                charKeys->addObject(charKeyString);
                            }
                        } else {
                            auto charKeyString = __String::create("C000");
                            charKeys->addObject(charKeyString);
                        }
                    } else {
                        auto charKeyString = __String::create("C000");
                        charKeyString->retain();
                        charKeys->addObject(charKeyString);
                    }

                    if (!chapterDatas.IsNull() && chapterDatas.HasMember("background")) {
                        auto backgroundKey = chapterDatas["background"].GetString();
                        __String *bgKeyString = __String::create(backgroundKey);
                        bgKeyString->retain();
                        bgMapKeys->addObject(bgKeyString);
                    } else {
                        __String *bgKeyString = __String::create("B001"); //default
                        bgKeyString->retain();
                        bgMapKeys->addObject(bgKeyString);
                    }

                }
                
                for (int i=0; i < charKeys->count(); i++) {
                    auto charKey = (__String *)charKeys->getObjectAtIndex(i);
                    auto charJsonString = ud->getStringForKey(charKey->getCString());
                    if(charJsonString.length() > 0){
                        rapidjson::Document charJson;
                        charJson.Parse<0>(charJsonString.c_str());
                        __String *charFileName;
                        if (charJson.HasMember("base_char")) { // custom character
                            auto baseChar = charJson["base_char"].GetString();
                            auto baseCharacterJsonString = ud->getStringForKey(baseChar);
                            rapidjson::Document baseCharacterInfo;
                            baseCharacterInfo.Parse<0>(baseCharacterJsonString.c_str());
                            charFileName = __String::create(baseCharacterInfo["bookmap_ccbi"].GetString());
                        } else {
                            charFileName = __String::create(charJson["bookmap_ccbi"].GetString());
                        }
                        characterFileNames->addObject(charFileName);
                    } else {
                        characterFileNames->addObject(__String::create("map_created_page_character_01.ccbi"));
                    }
                }
            }
            
            bool jsonDataChanged = false;
            
            chapter1TempDataExist = !chapter1Data.IsNull() && !chapter1Data.HasMember("char_datas");
            
            bool chapter1ExistNow = !chapter1Data.IsNull() && chapter1Data.HasMember("char_datas");
            if (chapter1Exist != chapter1ExistNow) {
                chapter1Exist = chapter1ExistNow;

                __Array* targetArray = chapter1SpritesArray;
                auto size = targetArray->count();
                for(int index = 0 ; index < size ; index++){
                    Sprite* sprite = (Sprite*)targetArray->getObjectAtIndex(index);
                    sprite->removeFromParentAndCleanup(true);
                }
                targetArray->removeAllObjects();
                
                if(chapter1Exist) {
                	auto childFeather = getChildByTag(TAG_MENU_FEATHER_01);
                	if(childFeather != nullptr)
                		removeChild(childFeather, true);
                } else {
                	auto childCharacter = getChildByTag(TAG_MENU_CHARACTER_01);
                	if(childCharacter != nullptr)
                		removeChild(childCharacter, true);
                }
                
                auto charString = (__String*)characterFileNames->getObjectAtIndex(0);
                std::string charStr(charString->getCString());
                const char *charName = strcpy((char*)malloc(charStr.length()+1), charStr.c_str());
                log("char : %s", charName);
                
                auto charKey = (__String*)charKeys->getObjectAtIndex(0);
                std::string charKeyStr(charKey->getCString());
                const char *charKeyName = strcpy((char*)malloc(charKeyStr.length()+1), charKeyStr.c_str());
                log("charKeyName : %s", charKeyName);
//                auto bgKeyString = (__String*)bgMapKeys->getObjectAtIndex(0);
//                auto backgroundString = ud->getStringForKey(bgKeyString->getCString());
//                rapidjson::Document backgroundInfo;
//                backgroundInfo.Parse<0>(backgroundString.c_str());
                
//                const char *mapCcbiNameString = nullptr;
//                if (backgroundInfo["custom"].GetBool()) {
//                    auto *customMapCcbi = CUSTOM_MAP_BG;
//                    mapCcbiNameString = customMapCcbi;
//                } else {
//                    auto mapCcbiName = backgroundInfo["map_ccbi"].GetString();
//                    std::string mapCcbiNameStr(mapCcbiName);
//                    mapCcbiNameString = strcpy((char*)malloc(mapCcbiNameStr.length()+1), mapCcbiNameStr.c_str());
//                }
                
                auto sequence1 = chapter1Exist? CallFunc::create(CC_CALLBACK_0(BookMap::runCharacterSequenceAnimation, this, charKeyName, charName,BOOKMAP_CHARACTER01, TAG_CHARACTER_01, this->getMapccbiName(bgMapKeys, 0, strcmp(charKeyName, "C000")!=0), BOOKMAP_ICON_BG01, "btn_map_delete.ccbi", BOOKMAP_DELETE_BTN01, TAG_DELETE_01, TAG_CHAPTER_01)) :
                CallFunc::create(CC_CALLBACK_0(BookMap::addFeatherMenuAndRun, this,BOOKMAP_FEATHER01, TAG_CHARACTER_01, TAG_CHAPTER_01));
                
                auto sequence1Action = Sequence::create(DelayTime::create(DELAY_START_ANIMATION),
                                                           sequence1,
                                                           NULL);
                runAction(sequence1Action);
            }
            
            chapter2TempDataExist = !chapter2Data.IsNull() && !chapter2Data.HasMember("char_datas");

            bool chapter2ExistNow = !chapter2Data.IsNull() && chapter2Data.HasMember("char_datas");

            if (chapter2Exist != chapter2ExistNow) {
                chapter2Exist = chapter2ExistNow;
                
                __Array* targetArray = chapter2SpritesArray;
                auto size = targetArray->count();
                for(int index = 0 ; index < size ; index++){
                    Sprite* sprite = (Sprite*)targetArray->getObjectAtIndex(index);
                    sprite->removeFromParentAndCleanup(true);
                }
                targetArray->removeAllObjects();
                
                if(chapter2Exist) {
                	auto childFeather = getChildByTag(TAG_MENU_FEATHER_02);
                	if(childFeather != nullptr)
                		removeChild(childFeather, true);
                } else {
                	auto childCharacter = getChildByTag(TAG_MENU_CHARACTER_02);
                	if(childCharacter != nullptr)
                		removeChild(childCharacter, true);
                }

                auto charString = (__String*)characterFileNames->getObjectAtIndex(1);
                std::string charStr(charString->getCString());
                const char *charName = strcpy((char*)malloc(charStr.length()+1), charStr.c_str());
                
                auto charKey = (__String*)charKeys->getObjectAtIndex(1);
                std::string charKeyStr(charKey->getCString());
                const char *charKeyName = strcpy((char*)malloc(charKeyStr.length()+1), charKeyStr.c_str());
                
//                auto bgKeyString = (__String*)bgMapKeys->getObjectAtIndex(1);
//                auto backgroundString = ud->getStringForKey(bgKeyString->getCString());
//                rapidjson::Document backgroundInfo;
//                backgroundInfo.Parse<0>(backgroundString.c_str());
//                const char *mapCcbiNameString = nullptr;
//                if (backgroundInfo["custom"].GetBool()) {
//                    auto *customMapCcbi = CUSTOM_MAP_BG;
//                    mapCcbiNameString = customMapCcbi;
//                } else {
//                    auto mapCcbiName = backgroundInfo["map_ccbi"].GetString();
//                    std::string mapCcbiNameStr(mapCcbiName);
//                    mapCcbiNameString = strcpy((char*)malloc(mapCcbiNameStr.length()+1), mapCcbiNameStr.c_str());
//                }
                
                auto sequence2 = chapter2Exist? CallFunc::create(CC_CALLBACK_0(BookMap::runCharacterSequenceAnimation, this, charKeyName,charName,BOOKMAP_CHARACTER02, TAG_CHARACTER_02, this->getMapccbiName(bgMapKeys, 1, strcmp(charKeyName, "C000")!=0), BOOKMAP_ICON_BG02, "btn_map_delete.ccbi", BOOKMAP_DELETE_BTN02, TAG_DELETE_02, TAG_CHAPTER_02)) :
                CallFunc::create(CC_CALLBACK_0(BookMap::addFeatherMenuAndRun, this,BOOKMAP_FEATHER02, TAG_CHARACTER_02, TAG_CHAPTER_02));
                
                auto sequence2Action = Sequence::create(DelayTime::create(DELAY_START_ANIMATION),
                                                        sequence2,
                                                        NULL);
                runAction(sequence2Action);
                
                if ( !chapter2ExistNow ) {
                    rapidjson::Value nullValue(rapidjson::kNullType);
                    chapterData["chapter2"] = nullValue;
                    jsonDataChanged = true;
                }
            }
            
            chapter3TempDataExist = !chapter3Data.IsNull() && !chapter3Data.HasMember("char_datas");
            
            bool chapter3ExistNow = !chapter3Data.IsNull() && chapter3Data.HasMember("char_datas");
            
            if (chapter3Exist != chapter3ExistNow) {
                chapter3Exist = chapter3ExistNow;

                __Array* targetArray = chapter3SpritesArray;
                auto size = targetArray->count();
                for(int index = 0 ; index < size ; index++){
                    Sprite* sprite = (Sprite*)targetArray->getObjectAtIndex(index);
                    sprite->removeFromParentAndCleanup(true);
                }
                targetArray->removeAllObjects();
                
                if(chapter3Exist) {
                	auto childFeather = getChildByTag(TAG_MENU_FEATHER_03);
                	if(childFeather != nullptr)
                		removeChild(childFeather, true);
                } else {
                	auto childCharacter = getChildByTag(TAG_MENU_CHARACTER_03);
                	if(childCharacter != nullptr)
                		removeChild(childCharacter, true);
                }

                auto charString = (__String*)characterFileNames->getObjectAtIndex(2);
                std::string charStr(charString->getCString());
                const char *charName = strcpy((char*)malloc(charStr.length()+1), charStr.c_str());
                
                auto charKey = (__String*)charKeys->getObjectAtIndex(2);
                std::string charKeyStr(charKey->getCString());
                const char *charKeyName = strcpy((char*)malloc(charKeyStr.length()+1), charKeyStr.c_str());
                
//                auto bgKeyString = (__String*)bgMapKeys->getObjectAtIndex(2);
//                auto backgroundString = ud->getStringForKey(bgKeyString->getCString());
//                rapidjson::Document backgroundInfo;
//                backgroundInfo.Parse<0>(backgroundString.c_str());
//                const char *mapCcbiNameString = nullptr;
//                if (backgroundInfo["custom"].GetBool()) {
//                    auto *customMapCcbi = CUSTOM_MAP_BG;
//                    mapCcbiNameString = customMapCcbi;
//                } else {
//                    auto mapCcbiName = backgroundInfo["map_ccbi"].GetString();
//                    std::string mapCcbiNameStr(mapCcbiName);
//                    mapCcbiNameString = strcpy((char*)malloc(mapCcbiNameStr.length()+1), mapCcbiNameStr.c_str());
//                }
                
                auto sequence3 = chapter3Exist? CallFunc::create(CC_CALLBACK_0(BookMap::runCharacterSequenceAnimation, this, charKeyName,charName,BOOKMAP_CHARACTER03, TAG_CHARACTER_03, this->getMapccbiName(bgMapKeys, 2, strcmp(charKeyName, "C000")!=0), BOOKMAP_ICON_BG03, "btn_map_delete.ccbi", BOOKMAP_DELETE_BTN03, TAG_DELETE_03, TAG_CHAPTER_03)) :
                CallFunc::create(CC_CALLBACK_0(BookMap::addFeatherMenuAndRun, this,BOOKMAP_FEATHER03, TAG_CHARACTER_03, TAG_CHAPTER_03));
                
                auto sequence3Action = Sequence::create(DelayTime::create(DELAY_START_ANIMATION),
                                                        sequence3,
                                                        NULL);
                runAction(sequence3Action);
                
                if ( !chapter3ExistNow ) {
                    rapidjson::Value nullValue(rapidjson::kNullType);
                    chapterData["chapter3"] = nullValue;
                    jsonDataChanged = true;
                }
            }
            
            chapter4TempDataExist = !chapter4Data.IsNull() && !chapter4Data.HasMember("char_datas");
            
            bool chapter4ExistNow = !chapter4Data.IsNull() && chapter4Data.HasMember("char_datas");
            
            if (chapter4Exist != chapter4ExistNow) {
                chapter4Exist = chapter4ExistNow;

                __Array* targetArray = chapter4SpritesArray;
                auto size = targetArray->count();
                for(int index = 0 ; index < size ; index++){
                    Sprite* sprite = (Sprite*)targetArray->getObjectAtIndex(index);
                    sprite->removeFromParentAndCleanup(true);
                }
                targetArray->removeAllObjects();

                if(chapter4Exist) {
                	auto childFeather = getChildByTag(TAG_MENU_FEATHER_04);
                	if(childFeather != nullptr)
                		removeChild(childFeather, true);
                } else {
                	auto childCharacter = getChildByTag(TAG_MENU_CHARACTER_04);
                	if(childCharacter != nullptr)
                		removeChild(childCharacter, true);
                }

                auto charString = (__String*)characterFileNames->getObjectAtIndex(3);
                std::string charStr(charString->getCString());
                const char *charName = strcpy((char*)malloc(charStr.length()+1), charStr.c_str());
                
                auto charKey = (__String*)charKeys->getObjectAtIndex(3);
                std::string charKeyStr(charKey->getCString());
                const char *charKeyName = strcpy((char*)malloc(charKeyStr.length()+1), charKeyStr.c_str());
                
//                auto bgKeyString = (__String*)bgMapKeys->getObjectAtIndex(3);
//                auto backgroundString = ud->getStringForKey(bgKeyString->getCString());
//                rapidjson::Document backgroundInfo;
//                backgroundInfo.Parse<0>(backgroundString.c_str());
//                const char *mapCcbiNameString = nullptr;
//                if (backgroundInfo["custom"].GetBool()) {
//                    auto *customMapCcbi = CUSTOM_MAP_BG;
//                    mapCcbiNameString = customMapCcbi;
//                } else {
//                    auto mapCcbiName = backgroundInfo["map_ccbi"].GetString();
//                    std::string mapCcbiNameStr(mapCcbiName);
//                    mapCcbiNameString = strcpy((char*)malloc(mapCcbiNameStr.length()+1), mapCcbiNameStr.c_str());
//                }
                
                auto sequence4 = chapter4Exist? CallFunc::create(CC_CALLBACK_0(BookMap::runCharacterSequenceAnimation, this, charKeyName,charName,BOOKMAP_CHARACTER04, TAG_CHARACTER_04, this->getMapccbiName(bgMapKeys, 3, strcmp(charKeyName, "C000")!=0), BOOKMAP_ICON_BG04, "btn_map_delete.ccbi", BOOKMAP_DELETE_BTN04, TAG_DELETE_04, TAG_CHAPTER_04)) :
                CallFunc::create(CC_CALLBACK_0(BookMap::addFeatherMenuAndRun, this,BOOKMAP_FEATHER04, TAG_CHARACTER_04, TAG_CHAPTER_04));
                
                auto sequence4Action = Sequence::create(DelayTime::create(DELAY_START_ANIMATION),
                                                        sequence4,
                                                        NULL);
                runAction(sequence4Action);
                
                if ( !chapter4ExistNow ) {
                    rapidjson::Value nullValue(rapidjson::kNullType);
                    chapterData["chapter4"] = nullValue;
                    jsonDataChanged = true;
                }
            }
            
            chapter5TempDataExist = !chapter5Data.IsNull() && !chapter5Data.HasMember("char_datas");
            
            bool chapter5ExistNow = !chapter5Data.IsNull() && chapter5Data.HasMember("char_datas");
            
            if (chapter5Exist != chapter5ExistNow) {
                chapter5Exist = chapter5ExistNow;
                
                __Array* targetArray = chapter5SpritesArray;
                auto size = targetArray->count();
                for(int index = 0 ; index < size ; index++){
                    Sprite* sprite = (Sprite*)targetArray->getObjectAtIndex(index);
                    sprite->removeFromParentAndCleanup(true);
                }
                targetArray->removeAllObjects();
                
                if(chapter5Exist) {
                	auto childFeather = getChildByTag(TAG_MENU_FEATHER_05);
                	if(childFeather != nullptr)
                		removeChild(childFeather, true);
                } else {
                	auto childCharacter = getChildByTag(TAG_MENU_CHARACTER_05);
                	if(childCharacter != nullptr)
                		removeChild(childCharacter, true);
                }

                auto charString = (__String*)characterFileNames->getObjectAtIndex(4);
                std::string charStr(charString->getCString());
                const char *charName = strcpy((char*)malloc(charStr.length()+1), charStr.c_str());
                
                auto charKey = (__String*)charKeys->getObjectAtIndex(4);
                std::string charKeyStr(charKey->getCString());
                const char *charKeyName = strcpy((char*)malloc(charKeyStr.length()+1), charKeyStr.c_str());
                
//                auto bgKeyString = (__String*)bgMapKeys->getObjectAtIndex(4);
//                auto backgroundString = ud->getStringForKey(bgKeyString->getCString());
//                rapidjson::Document backgroundInfo;
//                backgroundInfo.Parse<0>(backgroundString.c_str());
//                const char *mapCcbiNameString = nullptr;
//                if (backgroundInfo["custom"].GetBool()) {
//                    auto *customMapCcbi = CUSTOM_MAP_BG;
//                    mapCcbiNameString = customMapCcbi;
//                } else {
//                    auto mapCcbiName = backgroundInfo["map_ccbi"].GetString();
//                    std::string mapCcbiNameStr(mapCcbiName);
//                    mapCcbiNameString = strcpy((char*)malloc(mapCcbiNameStr.length()+1), mapCcbiNameStr.c_str());
//                }
                
                auto sequence5 = chapter5Exist? CallFunc::create(CC_CALLBACK_0(BookMap::runCharacterSequenceAnimation, this, charKeyName,charName,BOOKMAP_CHARACTER05, TAG_CHARACTER_05, this->getMapccbiName(bgMapKeys, 4, strcmp(charKeyName, "C000")!=0), BOOKMAP_ICON_BG05, "btn_map_delete.ccbi", BOOKMAP_DELETE_BTN05, TAG_DELETE_05, TAG_CHAPTER_05)) :
                CallFunc::create(CC_CALLBACK_0(BookMap::addFeatherMenuAndRun, this,BOOKMAP_FEATHER05, TAG_CHARACTER_05, TAG_CHAPTER_05));

                auto sequence5Action = Sequence::create(DelayTime::create(DELAY_START_ANIMATION),
                                                        sequence5,
                                                        NULL);
                runAction(sequence5Action);
                
                if ( !chapter5ExistNow ) {
                    rapidjson::Value nullValue(rapidjson::kNullType);
                    chapterData["chapter5"] = nullValue;
                    jsonDataChanged = true;
                }
            }
        }
    }
}

void BookMap::setPlayMenuVisible()
{
    int existChapterNum = 0;
    if (chapter1Exist) existChapterNum++;
    if (chapter2Exist) existChapterNum++;
    if (chapter3Exist) existChapterNum++;
    if (chapter4Exist) existChapterNum++;
    if (chapter5Exist) existChapterNum++;
    
    if (existChapterNum > 0) {
        playMenu->setVisible(true);
#ifdef _GUIDE_ENABLE_
        GuideMap::create(this, GuideMap::Type::Play);
#endif
    } else {
        playMenu->setVisible(false);
    }
}

void BookMap::runCharacterSequenceAnimation(const char *characterKey, const char *characterCcbiName, cocos2d::Point characterPos, int characterTag, const char*iconBgCcbiName, cocos2d::Point iconBgPos, const char *delBtnCcbiName, cocos2d::Point deletePos, int deleteBtnTag, int chapterTag)
{
//    if (!characterCcbiName) return;
 
//    log("characterCCBINAME : %s", characterCcbiName);
    
    bool editable = bookJson["editable"].GetBool();
    auto deleteButtonCreator = editable?CallFunc::create(CC_CALLBACK_0(BookMap::addIconMenu,this,characterKey, delBtnCcbiName, deletePos, deleteBtnTag, chapterTag)):NULL;
    
    Sequence *action;
    if (strcmp(characterKey, "C000")==0) {
//        auto bgStr = __String::create(iconBgCcbiName);
//        std::string bgstd(iconBgCcbiName);
//        bgstd.substr(bgstd.length()-7, bgstd.length()-5);
//        log("bgstd : %s", bgstd.c_str());
        
//        __String::createWithFormat("map_bg_none_%02d.ccbi", productName)
        action = Sequence::create(
                                  CallFunc::create(CC_CALLBACK_0(BookMap::addIconMenu,this, characterKey, iconBgCcbiName, characterPos, characterTag, chapterTag)),
                                  DelayTime::create(0.1f),
                                  deleteButtonCreator, NULL);
    } else {
        action = Sequence::create(
                                  CallFunc::create(CC_CALLBACK_0(BookMap::addIconMenu,this,characterKey, characterCcbiName, characterPos,characterTag, chapterTag)),
                                  DelayTime::create(0.1f),
                                  CallFunc::create(CC_CALLBACK_0(BookMap::addItemBg, this,iconBgCcbiName, iconBgPos, chapterTag)),
                                  DelayTime::create(0.1f),
                                  deleteButtonCreator, NULL);
    }
    
    runAction(action);
}

void BookMap::addIconMenu(const char *characterKey, const char *ccbiName, cocos2d::Point position, int tag, int chapterTag)
{
//    log("character key : %s", characterKey);
    auto spriteObj = this->getCcbiSprite(ccbiName, cocos2d::Point::ZERO);
    Menu* characterMenu = this->addMenu(spriteObj, spriteObj, position, tag, true);
    if(characterMenu != nullptr)
    {
    	characterMenu->setTag(TAG_MENU_CHARACTER_01 + tag - 1);
        characterMenu->setLocalZOrder(ZORDER_ITEM);
    }
    auto arr = this->getChapterSpriteArray(chapterTag);
    arr->addObject(spriteObj);
    
    if (strcmp(characterKey, "C000") == 0) return;
    
    if (tag == TAG_CHARACTER_01 || tag == TAG_CHARACTER_02 || tag == TAG_CHARACTER_03 || tag == TAG_CHARACTER_04 || tag == TAG_CHARACTER_05) {
        if (characterKey) {
            auto ud = UserDefault::getInstance();
            auto charJsonString = ud->getStringForKey(characterKey);
            rapidjson::Document characterInfo;
            characterInfo.Parse<0>(charJsonString.c_str());
            __String *thumbnailImage = nullptr;

            if (characterInfo.HasMember("base_char") ){ // custom character
                auto baseChar = characterInfo["base_char"].GetString();
                auto baseCharacterJsonString = ud->getStringForKey(baseChar);
                
                rapidjson::Document baseCharacterInfo;
                baseCharacterInfo.Parse<0>(baseCharacterJsonString.c_str());
                
                auto thumbnail = (char *)characterInfo["thumbnail_photo_file"].GetString();
                thumbnailImage = __String::create(thumbnail);
                
                if (thumbnailImage != nullptr) {
                    std::string photoType("character");
                    std::string path = Util::getThumbnailPhotoPath(photoType);
                    path.append(thumbnailImage->getCString());
                    if (cocos2d::FileUtils::getInstance()->isFileExist(path) ){
                        Sprite *face = (Sprite*)spriteObj->getChildByTag(100);
                        if (face) {
                            face->setTexture(Director::getInstance()->getTextureCache()->addImage(path));
                        }
                    } else {
                        log("error! no file : %s", path.c_str());
                    }
                }
            }
        }
        
        
    }
    
}

void BookMap::addItemBg(const char* ccbiName, cocos2d::Point position, int chapterTag)
{
    auto ud = UserDefault::getInstance();
    auto bookJsonString = ud->getStringForKey(bookKey->getCString());
    rapidjson::Document bookJson;
    bookJson.Parse<0>(bookJsonString.c_str());
    
    Sprite *itemBg;
    if (!this->bookJson["editable"].GetBool()) {
        if (strcmp(ccbiName, "map_bg_07.ccbi") == 0) {
            itemBg = this->getCcbiSprite("map_bg_preload_07.ccbi", position);
        } else if (strcmp(ccbiName, "map_bg_10.ccbi") == 0) {
            itemBg = this->getCcbiSprite("map_bg_preload_10.ccbi", position);
        } else {
            itemBg = this->getCcbiSprite(ccbiName, position);
        }
    } else {
        itemBg = this->getCcbiSprite(ccbiName, position);
    }

    this->addChild(itemBg, 4);
    itemBg->setLocalZOrder(ZORDER_BG_ITEM);
    
    auto arr = this->getChapterSpriteArray(chapterTag);
    arr->addObject(itemBg);
}

//void BookMap::runFeatherSequenceAnimation(Point position)
//{
//    auto action1 = Sequence::create(CallFunc::create(CC_CALLBACK_0(BookMap::addFeatherMenuAndRun, this, BOOKMAP_FEATHER01, 1)), NULL);
//    runAction(action1);
//}

void BookMap::addFeatherMenuAndRun(cocos2d::Point position, int tag, int chapterTag)
{
    auto normal = this->getCcbiSprite(FEATHER_NORMAL_FILE_NAME, cocos2d::Point::ZERO);
    auto press = this->getCcbiSprite(FEATHER_PRESS_FILE_NAME, cocos2d::Point::ZERO);
    __String *sequenceName = NULL;
    if (tag == 1) {
        featherNormal01 = normal;
        featherPress01 = press;
        sequenceName = __String::createWithFormat("show%02d", tag);
    } else if (tag == 2) {
        featherNormal02 = normal;
        featherPress02 = press;
        sequenceName = __String::createWithFormat("show%02d", tag);
    } else if (tag == 3) {
        featherNormal03 = normal;
        featherPress03 = press;
        sequenceName = __String::createWithFormat("show%02d", tag);
    } else if (tag == 4) {
        featherNormal04 = normal;
        featherPress04 = press;
        sequenceName = __String::createWithFormat("show%02d", tag);
    } else if (tag == 5) {
        featherNormal05 = normal;
        featherPress05 = press;
        sequenceName = __String::createWithFormat("show%02d", tag);
    }
    
    auto arr = this->getChapterSpriteArray(chapterTag);
    arr->addObject(normal);
    arr->addObject(press);

    Menu* feather = this->addMenu(normal, press, position, tag, true);
    if(feather != nullptr)
    {
    	feather->setTag(TAG_MENU_FEATHER_01 + tag - 1);
        feather->setLocalZOrder(ZORDER_FEAHTER);
    }
    this->runAnimationWithName(normal, sequenceName->getCString());
}

Menu* BookMap::addMenu(Sprite* normal, Sprite* press, cocos2d::Point position, int tag, bool visible)
{
    auto menuItem = MenuItemSprite::create(normal,press,CC_CALLBACK_1(BookMap::bookButtonCallback, this));
    menuItem->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    menuItem->setPosition(cocos2d::Point::ZERO);
    menuItem->setTag(tag);
    auto menu = Menu::create(menuItem, NULL);
    menu->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    menu->setPosition(position);
    menu->setVisible(visible);
    this->addChild(menu, 3);

    return menu;
}

__Array* BookMap::getChapterSpriteArray(int chapterTag)
{
    switch (chapterTag) {
        case TAG_CHAPTER_01:
            return chapter1SpritesArray;
        case TAG_CHAPTER_02:
            return chapter2SpritesArray;
        case TAG_CHAPTER_03:
            return chapter3SpritesArray;
        case TAG_CHAPTER_04:
            return chapter4SpritesArray;
        case TAG_CHAPTER_05:
            return chapter5SpritesArray;
        default:
            return NULL;
    }
}


void BookMap::runAnimationWithName(Sprite* object, const char* name)
{
    cocosbuilder::CCBAnimationManager *manager = (cocosbuilder::CCBAnimationManager*)object->getUserObject();
    manager->setDelegate(this);
    manager->runAnimationsForSequenceNamed(name);
}

Sprite* BookMap::getCcbiSprite(const char* ccbiName, cocos2d::Point position)
{
    cocosbuilder::NodeLoaderLibrary *nodeLoaderLibrary = cocosbuilder::NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
    cocosbuilder::CCBReader *ccbReader = new cocosbuilder::CCBReader(nodeLoaderLibrary);
    Node *ccbiObj = ccbReader->readNodeGraphFromFile(ccbiName, this);
    auto ccbiSprite = (Sprite*)ccbiObj;
    ccbiSprite->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    ccbiSprite->setPosition(position);
    ccbReader->release();
    return ccbiSprite;
}

Label* BookMap::getFlagLabel(const char* flagName, cocos2d::Size labelSize, cocos2d::Point position, int pos)
{
    float fontSize = 0.0f;
    switch (pos) {
        case 1: {
            fontSize = BOOKMAP_FLAG_FONTSIZE_1;
        } break;
        case 5: {
            fontSize = BOOKMAP_FLAG_FONTSIZE_5;
        } break;
        default:
            fontSize = BOOKMAP_FLAG_FONTSIZE_N;
            break;
    }
    auto flagLabel = Label::createWithSystemFont(flagName, FONT_NAME_BOLD, fontSize, labelSize, TextHAlignment::CENTER, TextVAlignment::CENTER);
    flagLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    flagLabel->setPosition(position);
    flagLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    flagLabel->setColor(Color3B(255,255,255));
    return flagLabel;
}

void BookMap::completedAnimationSequenceNamed(const char *name)
{
    if (strcmp(name, "bounce") == 0) return;
    
    Sprite *featherNormal = NULL;
    Sprite *featherPress = NULL;
    log("name %s",name);
    if (strcmp(name, "show01") == 0) {
        featherNormal = featherNormal01;
        featherPress = featherPress01;
    } else if (strcmp(name, "show02") == 0) {
        featherNormal = featherNormal02;
        featherPress = featherPress02;
    } else if (strcmp(name, "show03") == 0) {
        featherNormal = featherNormal03;
        featherPress = featherPress03;
    } else if (strcmp(name, "show04") == 0) {
        featherNormal = featherNormal04;
        featherPress = featherPress04;
    } else if (strcmp(name, "show05") == 0) {
        featherNormal = featherNormal05;
        featherPress = featherPress05;
    }
    cocosbuilder::CCBAnimationManager *manager = (cocosbuilder::CCBAnimationManager*)featherNormal->getUserObject();
    manager->runAnimationsForSequenceNamed("bounce");
    cocosbuilder::CCBAnimationManager *manager1 = (cocosbuilder::CCBAnimationManager*)featherPress->getUserObject();
    manager1->runAnimationsForSequenceNamed("bounce");
}

void BookMap::bookButtonCallback(Ref* sender)
{
    if (menuListLayer) {
        if (menuListLayer->menuListVisible) {
            menuListLayer->menuListControl(false);
        }
    }
    
    log("bookButtoncallback");
    
    if (sender != nullptr) {
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Tap.wav", false);
    }
    
    this->setEnabledForMenu(false);
    
    auto menu = (Menu*)sender;
    int tag = menu->getTag();
    float changingTime = 0.4f;
    if (tag == TAG_CHARACTER_01 || tag == TAG_CHARACTER_02 || tag == TAG_CHARACTER_03 || tag == TAG_CHARACTER_04 || tag == TAG_CHARACTER_05) {
        auto director = Director::getInstance();
        
        bool exist = false;
        if (tag == TAG_CHARACTER_01) {
            exist = chapter1Exist;
        } else if (tag == TAG_CHARACTER_02) {
            exist = chapter2Exist;
        } else if (tag == TAG_CHARACTER_03) {
            exist = chapter3Exist;
        } else if (tag == TAG_CHARACTER_04) {
            exist = chapter4Exist;
        } else if (tag == TAG_CHARACTER_05) {
            exist = chapter5Exist;
        }

        if (exist) {
            
            AppDelegate* app = (AppDelegate*)Application::getInstance();
            if(app != nullptr)
            {
                char dictStr[128];
                
                if (bookKey->compare("book1") == 0)
                {
                    auto trackString = __String::createWithFormat("Played Storybook Sample A Video %d", tag);
                    sprintf(dictStr, "{\"Storybook\":\"Sample A\", \"Video\":\"Video %d\"}", tag);
                    app->mixpanelTrack(trackString->getCString(), dictStr);
                }
                else if (bookKey->compare("book2") == 0)
                {
                    auto trackString = __String::createWithFormat("Played Storybook Sample B Video %d", tag);
                    sprintf(dictStr, "{\"Storybook\":\"Sample B\", \"Video\":\"Video %d\"}", tag);
                    app->mixpanelTrack(trackString->getCString(), dictStr);
                } else
                {
                    sprintf(dictStr, "{\"Storybook\":\"Custom\", \"Video\":\"Video %d\"}", tag);
                    app->mixpanelTrack("Played Custom Video", dictStr);
                }
            }
            
            auto editable = bookJson["editable"].GetBool();
            if (editable) {
        		blockBackKey = true;
                auto chapterPlayScene = ChapterPlay::createScene(this->bookKey, tag);
                auto transition = TransitionProgressInOut::create(changingTime, chapterPlayScene);
                director->pushScene(transition);
            } else {
                rapidjson::Value& chapters = bookJson["chapters"];
                int chapterIndex = tag;
                auto chapterName = __String::createWithFormat("chapter%d", chapterIndex);
                rapidjson::Value& currentChapter = chapters[chapterName->getCString()];
                if (currentChapter.HasMember("movie_file") ){
                    auto movieFileName = currentChapter["movie_file"].GetString();
                    log("movieFileName : %s", movieFileName);
                    std::string movieName(movieFileName);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
                    AppDelegate* app = (AppDelegate*)Application::getInstance();
                    if(!isPlatformShown) {
                        app->showPlatformVideoView(movieName.c_str(), TAG_BOOKMAP);
                    }
                    isPlatformShown = true;
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
                    __Array *movieArray = __Array::create();
                    movieArray->retain();
                    auto movieStr = __String::create(movieFileName);
                    movieArray->addObject(movieStr);
                    
                    auto director = Director::getInstance();
                    auto video = JeVideo::createScene(__String::create(""),__String::create(""),movieArray, PLAY_TYPE_ONE);
                    auto transition = TransitionProgressInOut::create(0.4f, video);
                    director->pushScene(transition);
                    
                    movieArray->removeAllObjects();
                    
                    [MixpanelHelper logPlayStorybookSampleVideoWithIndex:tag];
#endif
#endif
                }
            }
        } else {
            auto characterRecordingScene = CharacterRecording::createScene(this->bookKey, tag);
            auto recordLayer = (CharacterRecording*)characterRecordingScene->getChildByTag(TAG_RECORDING);
            recordLayer->setDelegate(this);
            recordLayer->setHome(_homeLayer);
            auto transition = TransitionProgressInOut::create(changingTime, characterRecordingScene);
            director->pushScene(transition);
            
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
            
            int finalCounter = 0;
            
            if(chapter1Exist)
                finalCounter++;
            
            if(chapter2Exist)
                finalCounter++;
            
            if(chapter3Exist)
                finalCounter++;
            
            if(chapter4Exist)
                finalCounter++;
            
            if(chapter5Exist)
                finalCounter++;
            
            AppDelegate* app = (AppDelegate*)Application::getInstance();
            app->createStoryStart(tag, finalCounter);
            
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#   ifndef MACOS
            
            int finalCounter = 0;
            
            if(chapter1Exist)
                finalCounter++;
            
            if(chapter2Exist)
                finalCounter++;
            
            if(chapter3Exist)
                finalCounter++;
            
            if(chapter4Exist)
                finalCounter++;
            
            if(chapter5Exist)
                finalCounter++;
            
            
            [MixpanelHelper logCreateStoryStartWithIndex:tag isFinal:(finalCounter >= 4)];
#   endif
#endif
            
        }
    } else if(tag == TAG_PLAY_BUTTON) {
        AppDelegate* app = (AppDelegate*)Application::getInstance();
        auto editable = bookJson["editable"].GetBool();
        if (editable) {
            bool exist = chapter1Exist || chapter2Exist || chapter3Exist || chapter4Exist || chapter5Exist;
            if (exist) {
    			blockBackKey = true;
                auto director = Director::getInstance();
                auto playAndRecordScene = PlayAndRecord::createScene(this->bookKey, PlayAndRecordType::PlayOnly);
                auto playAndRecordLayer = (PlayAndRecord *)playAndRecordScene->getChildByTag(TAG_PLAY_AND_RECORDING);
                playAndRecordLayer->setDelegate(this);
                auto transition = TransitionProgressInOut::create(0.4f, playAndRecordScene);
                director->pushScene(transition);
                
                if(app != nullptr)
                {
                    char dictStr[128];
                    
                    sprintf(dictStr, "{\"Storybook\":\"Custom\", \"Video\":\"All\"}");
                        app->mixpanelTrack("Played All Custom Videos", dictStr);
                }
                
                
            } else {
                // show popup??
            }
        } else {
            
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#   ifndef MACOS
            [MixpanelHelper logPlayStorybookSampleVideoAll];
#   endif
#endif
            
            auto movieArray = __Array::create();
            movieArray->retain();
            
            rapidjson::Value& chapters = bookJson["chapters"];
            for (int i=0; i < 5; i++) {
                auto chapterName = __String::createWithFormat("chapter%d", i+1);
                rapidjson::Value& currentChapter = chapters[chapterName->getCString()];
                if (currentChapter.HasMember("movie_file") ){
                    auto movieFileName = currentChapter["movie_file"].GetString();
                    auto movieStr = __String::create(movieFileName);
                    movieArray->addObject(movieStr);
                }
                
                if (currentChapter.HasMember("trans_movie_file") ){
                    auto movieFileName = currentChapter["trans_movie_file"].GetString();
                    auto movieStr = __String::create(movieFileName);
                    movieArray->addObject(movieStr);
                }
            }
            
            __String *book = nullptr;
            __String *author = nullptr;
            if (this->bookKey) {
                auto ud = UserDefault::getInstance();
                auto bookJsonString = ud->getStringForKey(this->bookKey->getCString());
                if (bookJsonString != "") {
                    rapidjson::Document bookJson;
                    bookJson.Parse<0>(bookJsonString.c_str());
                    auto bookTitle = bookJson["title"].GetString();
//                    if (Application::getInstance()->getCurrentLanguage() == LanguageType::JAPANESE) {
                        if (bookJson["editable"].GetBool() == false) {
                            if (strcmp(bookTitle, "The Banana Brothers") == 0) {
                                bookTitle = (char*)Device::getResString(Res::Strings::CC_PRLOAD_BOOKTITLE_1);
                            } else if (strcmp(bookTitle, "Bob's Journey") == 0) {
                                bookTitle = (char*)Device::getResString(Res::Strings::CC_PRLOAD_BOOKTITLE_2);
                            }
                        }
//                    }

                    auto authorTitle = bookJson["author"].GetString();
                    book = __String::create(bookTitle);
                    author = __String::create(authorTitle);
                }
            }
            
    		blockBackKey = true;
            auto director = Director::getInstance();
            auto video = JeVideo::createScene(book, author, movieArray, PLAY_TYPE_ALL);
            auto transition = TransitionProgressInOut::create(0.4f, video);
            director->pushScene(transition);
            
            if(app != nullptr)
            {
                char dictStr[128];
                
                if (bookKey->compare("book1") == 0)
                {
                    sprintf(dictStr, "{\"Storybook\":\"Sample A\", \"Video\":\"All\"}");
                    app->mixpanelTrack("Played All Sample A Videos", dictStr);
                }
                else if (bookKey->compare("book2") == 0)
                {
                    sprintf(dictStr, "{\"Storybook\":\"Sample B\", \"Video\":\"All\"}");
                    app->mixpanelTrack("Played All Sample B Videos", dictStr);
                }
            }
        }
    } else {
        switch (tag) {
            case TAG_DELETE_01:
            {
                removingChapter = 1;
                break;
            }
            case TAG_DELETE_02:
            {
                removingChapter = 2;
                break;
            }
            case TAG_DELETE_03:
            {
                removingChapter = 3;
                break;
            }
            case TAG_DELETE_04:
            {
                removingChapter = 4;
                break;
            }
            case TAG_DELETE_05:
            {
                removingChapter = 5;
                break;
            }
            default:
                return;
        }
        
        Director::getInstance()->pause();
        PopupLayer *modal;
        if (Application::getInstance()->getCurrentLanguage() != LanguageType::JAPANESE) {
            modal = PopupLayer::create(POPUP_TYPE_WARNING,
                                       Device::getResString(Res::Strings::CC_JOURNEYMAP_POPUP_WARNING_DELETE_PAGE),
                                       Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL),//"Cancel",
                                       Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK));//"OK");
        } else {
            modal = PopupLayer::create(POPUP_TYPE_WARNING,
                                       Device::getResString(Res::Strings::CC_JOURNEYMAP_POPUP_WARNING_DELETE_PAGE),
                                       Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_NO),//"Cancel",
                                       Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_YES));//"OK");
        }
        modal->setTag(DELETE_CHAPTER_POPUP);
        modal->setDelegate(this);
        this->addChild(modal, 1000);
    }
    auto menuEnabledSequence = Sequence::create(DelayTime::create(changingTime), CallFunc::create(CC_CALLBACK_0(BookMap::setEnabledForMenu, this, true)), NULL);
    this->runAction(menuEnabledSequence);
}

void BookMap::setEnabledForMenu(bool isEnabled)
{
    const auto& children = this->getChildren();
    for (const auto& child : children)
    {
        Menu *childSprite = dynamic_cast<Menu*>(child);
        if (childSprite) {
            childSprite->setEnabled(isEnabled);
        }
    }
}

void BookMap::runFadeToAction(Sprite* object)
{
    object->setOpacity(0);
    auto fadeToAction = FadeTo::create(1.0f, 255);
    object->runAction(fadeToAction);
}

Sprite* BookMap::addBooksObject(const std::string& imageName, cocos2d::Point location)
{
    auto booksObject = Sprite::create(imageName);
    booksObject->setAnchorPoint(cocos2d::Point(0,0));
    booksObject->setPosition(cocos2d::Point(location.x,location.y-booksObject->getContentSize().height));
    return booksObject;
}

Node * BookMap::addWrite(cocos2d::Point location)
{
    cocosbuilder::NodeLoaderLibrary *nodeLoaderLibrary = cocosbuilder::NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
    cocosbuilder::CCBReader *ccbReaderWrite = new cocosbuilder::CCBReader(nodeLoaderLibrary);
    Node *write = ccbReaderWrite->readNodeGraphFromFile("write.ccbi", this);
    write->setPosition(location);
    ccbReaderWrite->release();
    return write;
}

void BookMap::btnBackCallback(Ref* pSender)
{
    if (pSender != nullptr) {
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Tap.wav", false);
    }
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    if(blockBackKey) {
		return;
	}
	if(isPlatformShown) {
		AppDelegate* app = (AppDelegate*)Application::getInstance();
		isPlatformShown = !app->hidePlatformView();
		TSBGMManager::getInstance()->play();
		return;
	}
#endif

    if (_delegate != NULL) {
        if(addMode){
            bool isAdded = chapter1Exist || chapter2Exist || chapter3Exist || chapter4Exist || chapter5Exist ||
                           chapter1TempDataExist || chapter2TempDataExist|| chapter3TempDataExist || chapter4TempDataExist || chapter5TempDataExist ||
                           textEdited;
            _delegate->onExitBookMap(this->bookKey, isAdded);
        } else {
            _delegate->updateCurrentBook(this->bookKey);
        }
    }
    this->bookKey->release();
    
    auto director = Director::getInstance();
    // auto homeScene = Home::createScene();
    // director->replaceScene(homeScene);

    director->popSceneWithTransition([](Scene *scene) {
        return TransitionProgressOutIn::create(0.4f, scene);
    });
    
    this->cleanup();
}

void BookMap::titleCallback(int area)
{
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();

    log("titleTextField : %s",this->bookJson["title"].GetString());
    
    auto leftBg = Sprite::create("map_title_bg_left.png");
    leftBg->setAnchorPoint(cocos2d::Point(0,0));
    
    cocos2d::extension::Scale9Sprite *editBoxBg1 = cocos2d::extension::Scale9Sprite::create("clear_bg.png", cocos2d::Rect(0,0,POPUP_BUTTON_IMAGE_SIZE.width,POPUP_BUTTON_IMAGE_SIZE.height),cocos2d::Rect(10,10,100,70));
    editBoxBg1->setContentSize(cocos2d::Size(BOOKMAP_DOUBLE_TITLE_LABEL_SIZE.width, BOOKMAP_DOUBLE_TITLE_LABEL_SIZE.height+16));
    
    cocos2d::extension::Scale9Sprite *editBoxBg2 = cocos2d::extension::Scale9Sprite::create("clear_bg.png", cocos2d::Rect(0,0,POPUP_BUTTON_IMAGE_SIZE.width,POPUP_BUTTON_IMAGE_SIZE.height),cocos2d::Rect(10,10,100,70));
    editBoxBg2->setContentSize(cocos2d::Size(BOOKMAP_DOUBLE_NAME_LABEL_SIZE.width, BOOKMAP_DOUBLE_NAME_LABEL_SIZE.height+16));
    
//    int splitNum = this->getSplitStringStartIndex(titleString.c_str(), FONT_NAME_BOLD, BOOKMAP_TITLE_LABEL_FONTSIZE, BOOKMAP_DOUBLE_TITLE_LABEL_SIZE.width);
//    if (splitNum <= 1) {
//        splitNum = this->getSplitStringWithoutWhitespace(titleString.c_str(), FONT_NAME_BOLD, BOOKMAP_TITLE_LABEL_FONTSIZE, BOOKMAP_DOUBLE_TITLE_LABEL_SIZE.width);
//    }
    
//    int splitNum = 0;
//    std::string firstStr = titleString;
//    int firstTextWidth = Device::getTextWidth(firstStr.c_str(), FONT_NAME_BOLD, BOOKMAP_TITLE_LABEL_FONTSIZE);
//    firstStr = firstStr.substr(0, splitNum-2);
//    if (firstTextWidth >= BOOKMAP_DOUBLE_TITLE_LABEL_SIZE.width) {
//        firstStr = firstStr.append("...");
//    }
    
    int titleMaxLength = 0;
    LanguageType currentLanguageType = Application::getInstance()->getCurrentLanguage();
    switch (currentLanguageType) {
        case LanguageType::KOREAN:
        case LanguageType::JAPANESE: {
            titleMaxLength = 30;
        } break;
        case LanguageType::ENGLISH:
        default: {
            titleMaxLength = 60;
        } break;
    }
    
    std::string bookTitleMain, bookTitleSub;
    Util::splitTitleString(titleString, ' ', bookTitleMain, bookTitleSub, BOOKMAP_DOUBLE_TITLE_LABEL_SIZE.width, FONT_NAME_BOLD, BOOKMAP_TITLE_LABEL_FONTSIZE);
    
//    titleTextField = TextFieldTTF::textFieldWithPlaceHolder(titleString, FONT_NAME_BOLD, BOOKMAP_TITLE_LABEL_FONTSIZE);
    titleTextField = EditBox::create(cocos2d::Size(BOOKMAP_DOUBLE_TITLE_LABEL_SIZE.width, BOOKMAP_DOUBLE_TITLE_LABEL_SIZE.height+16), editBoxBg1);
//    titleTextField->setPlaceHolder(bookTitleMain.c_str());
//    titleTextField->setPlaceholderFont(FONT_NAME_BOLD, BOOKMAP_TITLE_LABEL_FONTSIZE);
//    titleTextField->setPlaceholderFontColor(Color3B(0x88,0x47,0x01));
    titleTextField->setMaxLength(titleMaxLength);
    if (this->bookJson["default_title"].GetBool()) {
        titleTextField->setPlaceHolder(titleString.c_str());
        titleTextField->setPlaceholderFont(FONT_NAME_BOLD, BOOKMAP_TITLE_LABEL_FONTSIZE);
        titleTextField->setPlaceholderFontColor(Color3B(0x88,0x47,0x01));
    } else {
        titleTextField->setText(titleString.c_str());
    }
//    titleTextField->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
//    titleTextField->setDimensions(BOOKMAP_DOUBLE_TITLE_LABEL_SIZE.width, BOOKMAP_DOUBLE_TITLE_LABEL_SIZE.height);
//    titleTextField->setContentSize(BOOKMAP_DOUBLE_TITLE_LABEL_SIZE);
    titleTextField->setPosition(cocos2d::Point(BOOKMAP_DOUBLE_TITLE_LABEL_01.x, leftBg->getContentSize().height/2 - titleTextField->getContentSize().height/2));
    titleTextField->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    titleTextField->setColor(Color3B(0x88,0x47,0x01));
    titleTextField->setFont(FONT_NAME_BOLD, BOOKMAP_TITLE_LABEL_FONTSIZE);
    titleTextField->setFontColor(Color3B(0x88,0x47,0x01));
    titleTextField->setDelegate(this);
//    titleTextField->enableShadow(Color4B(255,255,255,255.0f*0.6f), cocos2d::Size(0,-0.5), 0);
    titleTextField->setAnchorPoint(cocos2d::Point::ZERO);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
    //bg
    Texture2D *texture = Director::getInstance()->getTextureCache()->addImage("map_title_bg_center.png");
    const Texture2D::TexParams &tp = {GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE};
    texture->setTexParameters(tp);
    auto bgWidth = titleTextField->getContentSize().width-((leftBg->getContentSize().width-BOOKMAP_SINGLE_TITLE_LABEL.x)*2);

	auto background = Sprite::createWithTexture(texture, cocos2d::Rect(0, 0, bgWidth, texture->getContentSize().height));
	background->setAnchorPoint(cocos2d::Point(0,0));
	background->setPosition(cocos2d::Point(leftBg->getContentSize().width,0));
#endif
    
//    int splitNumSecond = this->getSplitStringStartIndex(authorString.c_str(), FONT_NAME, BOOKMAP_NAME_LABEL_FONTSIZE, BOOKMAP_DOUBLE_NAME_LABEL_SIZE.width);
//    if (splitNumSecond <= 1) {
//        splitNumSecond = this->getSplitStringWithoutWhitespace(authorString.c_str(), FONT_NAME, BOOKMAP_NAME_LABEL_FONTSIZE, BOOKMAP_DOUBLE_NAME_LABEL_SIZE.width);
//    }
    
//    int splitNumSecond = 0;
//    
//    std::string secondStr = authorString;
//    int secondTextWidth = Device::getTextWidth(secondStr.c_str(), FONT_NAME, BOOKMAP_NAME_LABEL_FONTSIZE);
//    secondStr = secondStr.substr(0, splitNumSecond-2);
//    
//    if (secondTextWidth >= BOOKMAP_DOUBLE_NAME_LABEL_SIZE.width) {
//        secondStr = secondStr.append("...");
//    }
//
    std::string authorMain, authorSub;
    Util::splitTitleString(authorString, ' ', authorMain, authorSub, BOOKMAP_DOUBLE_NAME_LABEL_SIZE.width, FONT_NAME, BOOKMAP_NAME_LABEL_FONTSIZE);
    
    authorTextField = EditBox::create(cocos2d::Size(BOOKMAP_DOUBLE_NAME_LABEL_SIZE.width, BOOKMAP_DOUBLE_NAME_LABEL_SIZE.height+16), editBoxBg2);
//    authorTextField = TextFieldTTF::textFieldWithPlaceHolder(authorString, FONT_NAME, BOOKMAP_NAME_LABEL_FONTSIZE);
//    authorTextField->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
//    authorTextField->setDimensions(BOOKMAP_DOUBLE_NAME_LABEL_SIZE.width, BOOKMAP_DOUBLE_NAME_LABEL_SIZE.height);
//    authorTextField->setContentSize(BOOKMAP_DOUBLE_NAME_LABEL_SIZE);
    authorTextField->setPosition( cocos2d::Point(leftBg->getContentSize().width+titleTextField->getContentSize().width+BOOKMAP_SINGLE_NAME_LABEL.x-(leftBg->getContentSize().width-BOOKMAP_SINGLE_TITLE_LABEL.x), leftBg->getContentSize().height/2 - authorTextField->getContentSize().height/2) );
    authorTextField->setMaxLength(titleMaxLength);
//    authorTextField->setPlaceHolder(authorMain.c_str());
//    authorTextField->setPlaceholderFont(FONT_NAME, BOOKMAP_NAME_LABEL_FONTSIZE);
//    authorTextField->setPlaceholderFontColor(Color3B(0x9c,0x52,0x00));
    if (this->bookJson["default_author"].GetBool()) {
        authorTextField->setPlaceHolder(authorString.c_str());
        authorTextField->setPlaceholderFont(FONT_NAME, BOOKMAP_NAME_LABEL_FONTSIZE);
        authorTextField->setPlaceholderFontColor(Color3B(0x9c,0x52,0x00));
    } else {
        authorTextField->setText(authorString.c_str());
    }
    authorTextField->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    authorTextField->setFont(FONT_NAME, BOOKMAP_NAME_LABEL_FONTSIZE);
    authorTextField->setFontColor(Color3B(0x9c,0x52,0x00));
    authorTextField->setColor(Color3B(0x9c,0x52,0x00));
    authorTextField->setAnchorPoint(cocos2d::Point::ZERO);
    authorTextField->setDelegate(this);
//    authorTextField->enableShadow(Color4B(255,255,255,255.0f*0.6f), cocos2d::Size(0,-0.5), 0);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
    auto rightBg = Sprite::create("map_title_bg_right.png");
    rightBg->setAnchorPoint(cocos2d::Point(0,0));
    rightBg->setPosition(cocos2d::Point(leftBg->getContentSize().width+background->getContentSize().width,0));
    
    inputTitleSprite =  Sprite::create();
    inputTitleSprite->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_BOTTOM);
    inputTitleSprite->setContentSize(cocos2d::Size(leftBg->getContentSize().width+rightBg->getContentSize().width+background->getContentSize().width, leftBg->getContentSize().height));
	inputTitleSprite->setPosition(cocos2d::Point(visibleSize.width/2, visibleSize.height-BOOKMAP_EDIT_BOX_Y-inputTitleSprite->getContentSize().height));
	this->addChild(inputTitleSprite, TAG_DIMLAYER+1);
    
    inputTitleSprite->addChild(leftBg);
    inputTitleSprite->addChild(background);
    inputTitleSprite->addChild(rightBg);
    inputTitleSprite->addChild(titleTextField);
    inputTitleSprite->addChild(authorTextField);
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    const char* title = titleTextField->getText();
    const char* author = authorTextField->getText();
    titleTextField->setDialogMode(EditBox::EditBoxMode::TITLE);
    titleTextField->setParam(TITLE_AREA);
    authorTextField->setDialogMode(EditBox::EditBoxMode::TITLE);
    authorTextField->setParam(AUTHOR_AREA);
	AppDelegate* app = (AppDelegate*)Application::getInstance();
	if(app != nullptr) {
		app->setEditTextContents(title, author, TAG_BOOKMAP);
	}
	
	titleTextField->setText(authorString.c_str());
	titleTextField->setPlaceHolder(titleString.c_str());
	authorTextField->setText(authorString.c_str());
	authorTextField->setPlaceHolder(titleString.c_str());
	
#endif

    if (area == TITLE_AREA) {
        titleTextField->touchDownAction(titleTextField, Control::EventType::TOUCH_UP_INSIDE);
//        titleTextField->attachWithIME();
        textFieldStatus = TITLE_AREA;
    } else {
        authorTextField->touchDownAction(authorTextField, Control::EventType::TOUCH_UP_INSIDE);
//        authorTextField->attachWithIME();
        textFieldStatus = AUTHOR_AREA;
    }
}

void BookMap::keyboardWillShow (IMEKeyboardNotificationInfo &info) {}
void BookMap::keyboardDidShow (IMEKeyboardNotificationInfo &info) {}
void BookMap::keyboardWillHide (IMEKeyboardNotificationInfo &info)
{
    
#ifdef MACOS
    if (cancelEdit) {
        cancelEdit = !cancelEdit;
        return;
    }
#endif
    this->setAndsaveTitle();
    if (this->titleTextField && this->authorTextField) {
        auto fadeOutFunction = CallFunc::create([=](){
            if (inputTitleSprite != nullptr) {
                const auto& children = inputTitleSprite->getChildren();
                for (const auto& child : children)
                {
                    auto fadeOut = FadeTo::create(0.5f, 0.0f);
                    child->runAction(fadeOut);
                }
            }
        });
        
        auto sequence = Sequence::create(fadeOutFunction, CallFunc::create(CC_CALLBACK_0(BookMap::removeInputTitleSpriteWithChildren, this)), NULL);
        this->runAction(sequence);
        
//        this->removeInputTitleSpriteWithChildren();
    }
    this->removeDimLayer();
}

void BookMap::keyboardDidHide (IMEKeyboardNotificationInfo &info) {
//    if (this->titleTextField && this->authorTextField) {
//        auto fadeOutFunction = CallFunc::create([=](){
//            if (inputTitleSprite != nullptr) {
//                const auto& children = inputTitleSprite->getChildren();
//                for (const auto& child : children)
//                {
//                    auto fadeOut = FadeTo::create(0.5f, 0.0f);
//                    child->runAction(fadeOut);
//                }
//            }
//        });
//        
//        auto sequence = Sequence::create(fadeOutFunction, CallFunc::create(CC_CALLBACK_0(BookMap::removeInputTitleSpriteWithChildren, this)), NULL);
//        this->runAction(sequence);
//        
////        this->removeInputTitleSpriteWithChildren();
//    }
}

void BookMap::setAndsaveTitle()
{
    if (this->titleTextField && this->authorTextField) {
        if (textFieldStatus == TITLE_AREA) {
            __String text(this->titleTextField->getText());
            //        log("text.length() L %i", text.length());
            if (text.length() > 0) {
                contentsSprite->removeAllChildrenWithCleanup(true);
                this->setTitleAndNameUI(this->titleTextField->getText(), authorString);
                this->saveToJason();
                textEdited = true;
            }
        } else {
            __String text(this->authorTextField->getText());
            if (text.length() > 0) {
                contentsSprite->removeAllChildrenWithCleanup(true);
                this->setTitleAndNameUI(titleString,this->authorTextField->getText());
                this->saveToJason();
            }
        }
    }
}

void BookMap::removeInputTitleSpriteWithChildren()
{
    if (titleTextField) {
        this->titleTextField->removeFromParentAndCleanup(true);
        this->titleTextField = NULL;
    }
    
    if (authorTextField) {
        this->authorTextField->removeFromParentAndCleanup(true);
        this->authorTextField = NULL;
    }
    
    if (inputTitleSprite) {
        this->inputTitleSprite->removeFromParentAndCleanup(true);
        this->inputTitleSprite = NULL;
    }
    
    textFieldStatus = KEYBOARD_NOT_SHOWN;
}

void BookMap::saveToJason()
{
    if (this->bookKey) {
        if (textFieldStatus == TITLE_AREA) {
            this->bookJson["title"].SetString( this->titleTextField->getText());
            this->bookJson["author"].SetString(authorString.c_str());
            this->bookJson["default_title"].SetBool(false);
            titleString = this->titleTextField->getText();
            
        } else {
            this->bookJson["author"].SetString( this->authorTextField->getText());
            this->bookJson["title"].SetString(titleString.c_str());
            this->bookJson["default_author"].SetBool(false);
            authorString = this->authorTextField->getText();
        }
        
        rapidjson::StringBuffer strbuf;
        rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
        this->bookJson.Accept(writer);
        auto jsonString = strbuf.GetString();
        auto ud = UserDefault::getInstance();
        ud->setStringForKey(this->bookKey->getCString(), jsonString);
        ud->flush();
    }
}

void BookMap::menuCallback(Ref* pSender)
{
    if (pSender != nullptr) {
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Tap.wav", false);
    }
    
    if (menuListLayer != nullptr && menuListLayer->menuListVisible) {
    	menuListLayer->menuListControl(false);
        return;
    }
    
    int existChapterNum = 0;
    if (chapter1Exist) existChapterNum++;
    if (chapter2Exist) existChapterNum++;
    if (chapter3Exist) existChapterNum++;
    if (chapter4Exist) existChapterNum++;
    if (chapter5Exist) existChapterNum++;
    
    auto editable = bookJson["editable"].GetBool();
    if (editable) {
        if (existChapterNum > 0) {
            menuListLayer = MenuListLayer::create(LIST_TYPE_02);
            menuListLayer->setDelegate(this);
            this->addChild(menuListLayer, 1000);
        } else {
            menuListLayer = MenuListLayer::create(LIST_TYPE_01);
            menuListLayer->setDelegate(this);
            this->addChild(menuListLayer, 1000);
        }
    } else {
        menuListLayer = MenuListLayer::create(LIST_TYPE_01);
        menuListLayer->setDelegate(this);
        this->addChild(menuListLayer, 1000);
    }
    
    menuListLayer->menuListControl(!menuListLayer->menuListVisible);
}

void BookMap::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif
    
    Director::getInstance()->end();
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void BookMap::onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	if(keyCode == cocos2d::EventKeyboard::KeyCode::KEY_BACKSPACE) {
		if(getChildByTag(TAG_GUIDE_LAYER) != NULL) {
			auto guide = (GuideLayer*)getChildByTag(TAG_GUIDE_LAYER);
			guide->onTouchEnded(NULL, event);
		} else if(getChildByTag(TAG_POPUP_SHAREPOPUP) != NULL) {
			removeChildByTag(TAG_POPUP_SHAREPOPUP);
		} else if(getChildByTag(TAG_POPUP_SHARELIST) != NULL) {
			removeChildByTag(TAG_POPUP_SHARELIST);
		} else if(getChildByTag(TAG_POPUP_SHAREERROR) != NULL) {
			removeChildByTag(TAG_POPUP_SHAREERROR);
		} else if(getChildByTag(TAG_POPUP_NEEDSHARELOGIN) != NULL) {
			removeChildByTag(TAG_POPUP_NEEDSHARELOGIN);
		} else if(getChildByTag(TAG_POPUP_FACEBOOK_PERMISSION_ERROR) != NULL) {
			removeChildByTag(TAG_POPUP_FACEBOOK_PERMISSION_ERROR);
		} else if(getChildByTag(TAG_POPUP_DLDEVIC_NEEDLOGIN) != NULL) {
			removeChildByTag(TAG_POPUP_DLDEVIC_NEEDLOGIN);
		} else if(getChildByTag(TAG_POPUP_DLDEVIC_NEEDLOGIN) != NULL) {
			removeChildByTag(TAG_POPUP_DLDEVIC_NEEDLOGIN);
		} else if(getChildByTag(DELETE_CHAPTER_POPUP) != NULL) {
    		Director::getInstance()->resume();
			removeChildByTag(DELETE_CHAPTER_POPUP);
		} else if(menuListLayer && menuListLayer->menuListVisible) {
            menuListLayer->menuListControl(false);
        } else {
            btnBackCallback(nullptr);
        }
    }
    
#ifdef MACOS
    if (keyCode == EventKeyboard::KeyCode::KEY_KP_ENTER) {
        if (cancelEdit) {
            cancelEdit = !cancelEdit;
            return;
        }
        
        this->setAndsaveTitle();
        this->removeDimLayer();
    }
#endif
}

#pragma mark - EditBoxDelegate

void BookMap::editBoxEditingDidBegin(EditBox* editBox) {
//    log("editBox %p DidBegin !", editBox);
}
void BookMap::editBoxEditingDidEnd(EditBox* editBox) {
//    log("editBox %p DidEnd !", editBox);
}
void BookMap::editBoxTextChanged(EditBox* editBox, const std::string& text) {
//    log("editBox %p TextChanged, text: %s ", editBox, text.c_str());
}
void BookMap::editBoxReturn(EditBox* editBox) {
//    log("editBox  was returned !");
    
    if (titleTextField == editBox) {
//        log("title EditBox return !");
    } else if (authorTextField == editBox) {
//        log("author EditBox return !");
    }
}

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
void BookMap::updateEditBoxText(const std::string& title, const std::string& author)
{
    if (this->titleTextField && this->authorTextField) {
        contentsSprite->removeAllChildrenWithCleanup(true);
    	textFieldStatus = KEYBOARD_NOT_SHOWN;
        
		if(this->bookKey) {
			if(title.length() != 0) {
				this->bookJson["title"].SetString(title.c_str());
	        	this->bookJson["default_title"].SetBool(false);
				titleString = title;
	        }
	        
	        if(author.length() != 0) {
				this->bookJson["author"].SetString(author.c_str());
		        this->bookJson["default_author"].SetBool(false);
				authorString = author;
		    }
	
			rapidjson::StringBuffer strbuf;
			rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
			this->bookJson.Accept(writer);
			auto jsonString = strbuf.GetString();
			auto ud = UserDefault::getInstance();
			ud->setStringForKey(this->bookKey->getCString(), jsonString);
			ud->flush();
		}
        this->setTitleAndNameUI(titleString, authorString);
	}
}

void BookMap::clearEditMode()
{
    textFieldStatus = KEYBOARD_NOT_SHOWN;
}

void BookMap::onFacebookActionResult(int result, int type)
{
	switch(type) {
		case 1:
			switch(result) {
				case 0: // success
//					showShareEditText(SHARE_POPUP_FACEBOOK);
					this->showVideoShareConfirmPopup();
					break;

				case -1: // failed
				default:
	                PopupLayer *sharePopup = PopupLayer::create(POPUP_TYPE_WARNING,
	                		"Denied\nPermission Error\npublish_actions",//"After converting to video format.\nstart to share.",
							"OK");//"OK");
					sharePopup->setDelegate(this);
					sharePopup->setTag(TAG_POPUP_FACEBOOK_PERMISSION_ERROR);
					this->addChild(sharePopup, 999999);
					break;
			}
			break;
	}
}

void BookMap::showShareEditText()
{
	int shareType = -1;
	switch(_shareTarget) {
		case ShareTarget::Youtube:
			shareType = SHARE_POPUP_YOUTUBE;
			break;

		case ShareTarget::Facebook:
			shareType = SHARE_POPUP_FACEBOOK;
			break;
	}

	const char* title = titleString.c_str();
	const char* author = authorString.c_str();

	cocos2d::extension::Scale9Sprite *blankBg = cocos2d::extension::Scale9Sprite::create("clear_bg.png", cocos2d::Rect(0,0,0,0),cocos2d::Rect(0,0,0,0));
	blankBg->setContentSize(cocos2d::Size(0, 0));

	auto popup = EditBox::create(cocos2d::Size(0, 0), blankBg);
	popup->setPosition(cocos2d::Point::ZERO);
	popup->setDelegate(this);
	popup->setAnchorPoint(cocos2d::Point::ZERO);
	popup->setDialogMode(EditBox::EditBoxMode::COMMENT);
	popup->setParam(shareType);

	AppDelegate* app = (AppDelegate*)Application::getInstance();
	if(app != nullptr) {
		app->setEditTextContents(title, author, TAG_BOOKMAP);
	}

	popup->touchDownAction(popup, Control::EventType::TOUCH_UP_INSIDE);
}

void BookMap::onFacebookProgressUpdate(double progress)
{
	TSDoleNetProgressLayer* progressLayer = (TSDoleNetProgressLayer*)getChildByTag(TAG_PROGRESS_LAYER);
	if(progressLayer != nullptr) {
		progressLayer->showProgress(progress);
	}
}

void BookMap::cancelYoutubeShare()
{
	if(client != nullptr) {
		client->cancel();
		client = nullptr;
	}
}

void BookMap::doleGetInventoryListProtocol()
{
	if(_homeLayer != nullptr) {
		_homeLayer->doleGetInventoryListProtocol();
	}
}
#endif

void BookMap::characterPhotoSavedInRecordScene(std::string characterKey)
{
    if (_delegate != NULL) {
        _delegate->characterPhotoSavedInRecord(characterKey);
    }
}

void BookMap::bgPhotoSavedInRecordScene(std::string bgKey)
{
    if (_delegate != NULL) {
        _delegate->bgPhotoSavedInRecord(bgKey);
    }
}

void BookMap::showVideoShareConfirmPopup() {
    PopupLayer *sharePopup = PopupLayer::create(POPUP_TYPE_WARNING,
                                                Device::getResString(Res::Strings::CC_JOURNEYMAP_POPUP_NOTICE_SHARE),//"After converting to video format.\nstart to share.",
                                                Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL),//"cancel",
                                                Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK));//"OK");
    sharePopup->setDelegate(this);
    sharePopup->setTag(TAG_POPUP_SHAREPOPUP);
    this->addChild(sharePopup, 999999);
}

void BookMap::selectVideoShareTarget() {
    PopupLayer *share = PopupLayer::create(POPUP_TYPE_SHARED,
                                           Device::getResString(Res::Strings::CC_JOURNEYMAP_POPUP_TITLE_SHARE_LIST),//"Share",
                                           Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL));//"cancel");
    share->setDelegate(this);
    share->setTag(TAG_POPUP_SHARELIST);
    this->addChild(share, 1000);
    
}

void BookMap::videoDownloadToDevice() {
    std::string authKey = UserDefault::getInstance()->getStringForKey(UDKEY_STRING_AUTHKEY);
    if (authKey.length() > 0) {
        PopupLayer *sharePopup = PopupLayer::create(POPUP_TYPE_WARNING,
                                                    Device::getResString(Res::Strings::CC_JOURNEYMAP_POPUP_NOTICCE_DOWNLOADDEVICE),
                                                    Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL),//"cancel",
                                                    Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK));//"OK");
        sharePopup->setDelegate(this);
        sharePopup->setTag(TAG_POPUP_DLDEVIC);
        this->addChild(sharePopup, 999999);
    } else {
        PopupLayer *sharePopup;
        if (Application::getInstance()->getCurrentLanguage() != LanguageType::JAPANESE) {
            sharePopup = PopupLayer::create(POPUP_TYPE_WARNING,
                                            Device::getResString(Res::Strings::PF_DOLECOIN_NEED_LOGIN),
                                            Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL),//"cancel",
                                            Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK));//"OK");
        } else {
            sharePopup = PopupLayer::create(POPUP_TYPE_WARNING,
                                            Device::getResString(Res::Strings::PF_DOLECOIN_NEED_LOGIN),
                                            "もどる", "ログインする");//"OK");
        }
        sharePopup->setDelegate(this);
        sharePopup->setTag(TAG_POPUP_DLDEVIC_NEEDLOGIN);
        this->addChild(sharePopup, 999999);
    }
}

void BookMap::listButtonClicked(int buttonTag)
{
    switch (buttonTag) {
        case BUTTON_TYPE_SETTINGS: {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
            AppDelegate* app = (AppDelegate*)Application::getInstance();
            if(app != nullptr) {
            	app->showPlatformView(ANDROID_SETTING, TAG_BOOKMAP);
            }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
            //    std::string authKey = UserDefault::getInstance()->getStringForKey(UDKEY_STRING_AUTHKEY);
            //    if (authKey.length() > 0 ) {// login 되어 있음.
            //        this->runSettingView();
            //    } else {
            //        this->runAccountView();
            //    }
            this->runSettingView();//내부 View 에서 분기가 이루어 진다.
#endif
          break;
        }
        case BUTTON_TYPE_DOWNLOAD: {
            this->videoDownloadToDevice();
          break;
        }
        case BUTTON_TYPE_SHARE: {
            std::string authKey = UserDefault::getInstance()->getStringForKey(UDKEY_STRING_AUTHKEY);
            if (authKey.length() > 0) {
//            	showVideoShareConfirmPopup();
                this->selectVideoShareTarget();
            } else {
                PopupLayer *sharePopup;
                if (Application::getInstance()->getCurrentLanguage() != LanguageType::JAPANESE) {
                    sharePopup = PopupLayer::create(POPUP_TYPE_WARNING,
                                                    Device::getResString(Res::Strings::PF_DOLECOIN_NEED_LOGIN),
                                                    Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL),//"cancel",
                                                    Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK));//"OK");
                } else {
                    sharePopup = PopupLayer::create(POPUP_TYPE_WARNING,
                                                    Device::getResString(Res::Strings::PF_DOLECOIN_NEED_LOGIN),
                                                    "もどる", "ログインする");
                }
                sharePopup->setDelegate(this);
                sharePopup->setTag(TAG_POPUP_NEEDSHARELOGIN);
                this->addChild(sharePopup, 999999);
            }
          break;
        }
        default:
            break;
    }
}

void BookMap::startAuthorizedForVideoUpload() {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    switch (_shareTarget) {
        case ShareTarget::Youtube: {
            client = new Share::YoutubeAuthClient();
            
            CCLOG("hsw_dbg youtube auth client =============> %p", client);
            client->setOnResultCallback(this);
            Share::startAuthorizeYoutube(client);
        } break;
        case ShareTarget::Facebook: {
            AppDelegate* app = (AppDelegate*)Application::getInstance();
            if(app != nullptr) {
                app->checkFacebookPermission();
            }
        } break;
        default:
            break;
    }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    switch (_shareTarget) {
        case ShareTarget::Youtube: {
            CCLOG("hsw_dbg onAuthClicked called");
            
            client = new Share::YoutubeAuthClient();
            
            CCLOG("hsw_dbg youtube auth client =============> %p", client);
            client->setOnResultCallback(this);
            Share::startAuthorizeYoutube(client);
        } break;
        case ShareTarget::Facebook: {
#ifdef __FBUPLODTESTMODE_
            Share::KeyJSONValueMap resultMap;
            this->onSuccessCallback(NULL, 0, resultMap);
#else
            [[TSFacebookVideoUploadManager sharedObject] prepareShareOnFaceBook:(void*)this];
#endif
        } break;
        default:
            break;
    }
#endif
}

void BookMap::showShareCommentView() {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    	showShareEditText();
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    UIViewController *viewController = nil;
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
    } else {
        viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
    }
    NSLog(@"viewController screen frame : %@",NSStringFromCGRect(viewController.view.frame));
    
    const char *bookTitle = titleString.c_str();
    const char *bookAuther = authorString.c_str();
    
    NSString *bookTitleString = [NSString stringWithCString:bookTitle encoding:NSUTF8StringEncoding];
    NSString *bookAutherString = [NSString stringWithCString:bookAuther encoding:NSUTF8StringEncoding];
    
    NSDictionary *userData;
    switch (_shareTarget) {
        case ShareTarget::Youtube: {
            userData =
            @{kTSShareUserDataKeyTitle: bookTitleString,
              kTSShareUserDataKeyAuthor: bookAutherString,
              kTSShareUserDataKeyTarget: kTSShareUserDataKeyTargetYouTube };
            
        } break;
        case ShareTarget::Facebook: {
            userData =
            @{kTSShareUserDataKeyTitle: bookTitleString,
              kTSShareUserDataKeyAuthor: bookAutherString,
              kTSShareUserDataKeyTarget: kTSShareUserDataKeyTargetFacebook };
            
        } break;
        default:
            break;
    }

#ifdef UI_TYPE_IOS8
    TSShareCommentViewController *commentVC = [[TSShareCommentViewController alloc] initWithData:userData];
    [UIViewController replaceRootViewControlller:commentVC];
#else
    TSPopupViewController *popupViewController =
    [[TSPopupViewController alloc] initWithPopupMode:TSPopupTypeShareComment userData:userData];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        popupViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    if(IS_IOS8 && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        popupViewController.preferredContentSize = CGSizeMake(1024, 768);
    }
    [viewController presentViewController:popupViewController animated:NO completion:nil];
    //    [popupViewController release];
#endif
#endif
}

void BookMap::popupListClicked(PopupLayer *popup, std::string selectedTitle) {
    log("selected title : %s", selectedTitle.c_str());
    if (strcmp(selectedTitle.c_str(), Device::getResString(Res::Strings::CC_JOURNEYMAP_POPUP_SHARELIST_YOUTUBE)) == 0) {//"Youtube"
        _shareTarget = ShareTarget::Youtube;
    } else if (strcmp(selectedTitle.c_str(), Device::getResString(Res::Strings::CC_JOURNEYMAP_POPUP_SHARELIST_FACEBOOK)) == 0) {//"Facebook"
        _shareTarget = ShareTarget::Facebook;
    }
    this->startAuthorizedForVideoUpload();
}

void BookMap::onCancelledCallback(Share::Client* client) {
    //TODO Cancelled
    CCLOG("BookMap::onCancelledCallback called");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (_shareTarget == ShareTarget::Facebook) {
        [TSFacebookVideoUploadManager releaseSharedObject];
    }
#endif
    _shareTarget = ShareTarget::None;
//    this->selectVideoShareTarget();
}

void BookMap::onSuccessCallback(Share::Client* client, int errorCode, Share::KeyJSONValueMap& resultMap) {
    CCLOG("hsw_dbg onUploadClicked called");
    
    if (errorCode == 0) {
        this->showVideoShareConfirmPopup();
    } else {
        log("인증 실패_%d!!!", errorCode);
    }
}

void BookMap::onFailedCallback(Share::Client* client, int errorCode, Share::KeyJSONValueMap& resultMap) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (_shareTarget == ShareTarget::Facebook) {
        [TSFacebookVideoUploadManager releaseSharedObject];
    }
#endif
    _shareTarget = ShareTarget::None;
    if (!movieFilePath.empty()) {
        bool result = Device::deleteFile(movieFilePath.c_str());
        if (result) {
            log("movie file delete success");
        } else {
            log("movie file delete fail");
        }
    }
    PopupLayer *sharePopup =
    PopupLayer::create(POPUP_TYPE_WARNING,
                       Device::getResString(Res::Strings::CC_JOURNEYMAP_POPUP_ERROR_SHARE),//"Failed to share on social network.\nPlease check network connection or social network status and try again.",
                       Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK));//"OK");
    sharePopup->setDelegate(this);
    sharePopup->setTag(TAG_POPUP_SHAREERROR);
    this->addChild(sharePopup, 999999);
}

void BookMap::onShareCommentResult(bool isOK, const char* comment) {
    if (isOK) {
        TSDoleNetProgressLayer *progressLayer = TSDoleNetProgressLayer::create();
        switch (_shareTarget) {
            case ShareTarget::Youtube: {
                __String *youtubeVideoTitle = nullptr;
                switch (Application::getInstance()->getCurrentLanguage()) {
                    case LanguageType::KOREAN: {
                        youtubeVideoTitle = __String::createWithFormat(Device::getResString(Res::Strings::PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_YOUTUBE),
                                                                       authorString.c_str(), titleString.c_str());
                    } break;
                    case LanguageType::JAPANESE: {
                        youtubeVideoTitle = __String::createWithFormat(Device::getResString(Res::Strings::PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_YOUTUBE),
                                                                       authorString.c_str(), titleString.c_str());
                    } break;
                    case LanguageType::ENGLISH:
                    default: {
                        youtubeVideoTitle = __String::createWithFormat(Device::getResString(Res::Strings::PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_YOUTUBE),
                                                                       titleString.c_str(), authorString.c_str());
                        break;
                    }
                }
                progressLayer->startYouTubeShare(movieFilePath.c_str(), youtubeVideoTitle->getCString(), comment, _homeLayer);
            } break;
            case ShareTarget::Facebook: {
                progressLayer->startFacebookShare(movieFilePath.c_str(), titleString.c_str(), comment, _homeLayer);
            } break;
            default:
                break;
        }
        this->addChild(progressLayer, 999999);
    } else {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        if (_shareTarget == ShareTarget::Facebook) {
            [TSFacebookVideoUploadManager releaseSharedObject];
        }
#endif
        _shareTarget = ShareTarget::None;
        log("comment canceled!!!!!!!!!!!");// TODO:tuilise comment 입력 취소에 대한 결과 뭐 해야 하남?
        bool result = Device::deleteFile(movieFilePath.c_str());
        if (result) {
            log("movie file delete success");
        } else {
            log("movie file delete fail");
        }
    }
}

void BookMap::onShareCommentResult2(int shareType, bool isOK, const char* comment) {
    if (isOK) {
    	TSDoleNetProgressLayer *progressLayer = nullptr;
    	switch(shareType) {
    		case SHARE_POPUP_FACEBOOK:
    	        progressLayer = TSDoleNetProgressLayer::create();
    	        progressLayer->startFacebookShare(movieFilePath.c_str(), titleString.c_str(), comment, _homeLayer);
    			break;

    		case SHARE_POPUP_YOUTUBE:
    	        __String *youtubeVideoTitle = nullptr;
    	        switch (Application::getInstance()->getCurrentLanguage()) {
    	            case LanguageType::KOREAN:
    	            case LanguageType::JAPANESE: {
    	                youtubeVideoTitle = __String::createWithFormat(Device::getResString(Res::Strings::PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_YOUTUBE),
    	                                                               authorString.c_str(), titleString.c_str());
    	            } break;
    	            case LanguageType::ENGLISH:
    	            default: {
    	                youtubeVideoTitle = __String::createWithFormat(Device::getResString(Res::Strings::PF_SHARECOMMNET_POPUP_DEFAULTCOMMENT_YOUTUBE),
    	                                                               titleString.c_str(), authorString.c_str());
    	                break;
    	            }
    	        }

    	        progressLayer = TSDoleNetProgressLayer::create();
    	        progressLayer->startYouTubeShare(movieFilePath.c_str(), youtubeVideoTitle->getCString(), comment, _homeLayer);
    			break;
    	}
    	if(progressLayer != nullptr)
    		this->addChild(progressLayer, 999999, TAG_PROGRESS_LAYER);
    } else {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        if (_shareTarget == ShareTarget::Facebook) {
            [TSFacebookVideoUploadManager releaseSharedObject];
        }
#endif
        _shareTarget = ShareTarget::None;
        log("comment canceled!!!!!!!!!!!");// TODO:tuilise comment 입력 취소에 대한 결과 뭐 해야 하남?
        bool result = Device::deleteFile(movieFilePath.c_str());
        if (result) {
            log("movie file delete success");
        } else {
            log("movie file delete fail");
        }
    }
}

void BookMap::onFacebookShareComplete() {
	TSDoleNetProgressLayer* progressLayer = (TSDoleNetProgressLayer*)getChildByTag(TAG_PROGRESS_LAYER);
	if(progressLayer != nullptr) {
		progressLayer->onPostUploadVideo();
	}
}

void BookMap::runSettingView() {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#ifdef UI_TYPE_IOS8
    TSSettingMainViewController *settingMainVC = [[TSSettingMainViewController alloc] init];
    settingMainVC.originLayer = TSOriginLayerBookMap;
    [UIViewController replaceRootViewControlller:settingMainVC];
//    [settingMainVC release];
#else//UI_TYPE_IOS8
    UIViewController *viewController = nil;
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
    } else {
        viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
    }
    NSLog(@"viewController screen frame : %@",NSStringFromCGRect(viewController.view.frame));
    TSSettingViewController *settingViewController = [[TSSettingViewController alloc] init];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        settingViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    settingViewController.originLayer = TSOriginLayerBookMap;
    if(IS_IOS8 && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        settingViewController.preferredContentSize = CGSizeMake(1024, 768);
    }
    [viewController presentViewController:settingViewController animated:YES completion:nil];
//    [settingViewController release];
#endif//UI_TYPE_IOS8
#endif
}

void BookMap::playAndRecordFinished(PlayAndRecordType type, bool success, std::string fileName)
{
    switch (type) {
        case PlayAndRecordType::PlayOnly:
            
            break;
        case PlayAndRecordType::RecordForDownload:
        {
            if (success) {
                finishedDownload = true;
            }
        }
            break;
        case PlayAndRecordType::RecordForShare:
            if (success) {
                readyForShare = true;
                movieFilePath = fileName;
            }
            break;
        default:
            break;
    }
}

void BookMap::checkRecall(int type) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    switch (type) {
    	case ANDROID_SHARE:
        {
        	this->scheduleOnce(SEL_SCHEDULE(&BookMap::selectVideoShareTarget), 0.0f);
        } break;
        case ANDROID_DOWNLOAD_TO_DEVICE:
        {
        	this->scheduleOnce(SEL_SCHEDULE(&BookMap::videoDownloadToDevice), 0.0f);
        } break;
        default:
            break;
    }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
    switch (type) {
        case TSRecallBookMapSharePopup:
        {
//        	showVideoShareConfirmPopup();
            this->selectVideoShareTarget();
        } break;
        case TSRecallBookMapDLDevicePopup:
        {
            this->videoDownloadToDevice();
        } break;
        default:
            break;
    }
#endif
}
