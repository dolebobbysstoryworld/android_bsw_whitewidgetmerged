//
//  ToastLayer.h
//  bobby
//
//  Created by oasis on 2014. 6. 13..
//
//

#ifndef __bobby__ToastLayer__
#define __bobby__ToastLayer__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class ToastLayer;
class ToastLayerDelegate {
public:
    virtual ~ToastLayerDelegate() {}
    virtual void onToastRemoved(ToastLayer *toast) = 0;
};

class ToastLayer : public cocos2d::LayerColor
{
public:
    ToastLayer();
    virtual ~ToastLayer();
    static ToastLayer* create(const std::string &message, float seconds);
    bool initWithMessage(const std::string &message, float seconds);
    virtual void onEnter();
    virtual void onExit();
    void setDelegate(ToastLayerDelegate * pDelegate) { _delegate = pDelegate; }
    
private:
    Label *text;
    void removeAction(float dt);
    void removeToastLayer();
    void fadeAction(int fadeType, Node* obj, float duration);
    float removeDuration;
    ToastLayerDelegate *_delegate;
};


#endif /* defined(__bobby__ToastLayer__) */
