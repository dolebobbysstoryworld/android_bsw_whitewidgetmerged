//
//  PlayAndRecordScene.h
//  bobby
//
//  Created by Dongwook, Kim on 14. 5. 21..
//
//

#ifndef __bobby__PlayAndRecordScene__
#define __bobby__PlayAndRecordScene__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "Character.h"
#include "ChapterPlayer.h"
#include "OpeningScene.h"
#include "EndingScene.h"
#include "PopupLayer.h"
#include "Define.h"

#include "external/json/document.h"
#include "external/json/prettywriter.h"
#include "external/json/stringbuffer.h"

USING_NS_CC;
USING_NS_CC_EXT;

typedef enum
{
    PlayOnly,
    RecordForShare,
    RecordForDownload
} PlayAndRecordType;

class PlayAndRecordDelegate
{
public:
    virtual ~PlayAndRecordDelegate() {}
    virtual void playAndRecordFinished(PlayAndRecordType type, bool success, std::string fileName) = 0;
};

class PlayAndRecord : public cocos2d::LayerColor, ChapterPlayerDelegate, OpeningDelegate, EndingDelegate, PopupLayerDelegate
{
    
public:
    static cocos2d::Scene* createScene(__String* bookKey, PlayAndRecordType type);
    static PlayAndRecord* create(__String* bookKey, PlayAndRecordType type);

    virtual bool init();
    ~PlayAndRecord();
    
    virtual void chapterPlayFinished(int chapterIndex);
    virtual void onTransitionFinished(int chapterIndex);

    virtual void onEnter();
    virtual void onExit();
    
    virtual void onOpeningFinished(Opening* opening);
    virtual void onEndingFinished(Ending* ending);

    void popupButtonClicked(PopupLayer *popup, cocos2d::Ref *obj);
    
    void btnBackCallback(Ref* pSender);
    void reZorder(Sprite* pSender);

    PlayAndRecordType getType() {return _type; };
    
    void setDelegate(PlayAndRecordDelegate* delegate) { _delegate = delegate; };
    
    void playAndRecordFinished(bool success, std::string fileName);
    
    void startAudioRecord();
    void doneRecording();
    bool isChapterDataExist(int chapterIndex);
    
    void showEnding();
    
    bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event *event);
    void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event *event);
    void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event *event);
    void onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event *event);

    void createProgressBar();
    void setProgress(float value);
    void setProgress(Node *progressBar, float value);

    void playMenuCallback(Ref* pSender);
    void cancelMenuCallback(Ref* pSender);
    virtual void update(float delta);
    
    void showUI(bool show);
    void triggerHideUI();
    void cancelHideUI();
    
    void startPlay();
    void pausePlay();
    void resumePlay();

    void startRecording();
    void pauseRecording();
    void resumeRecording();

//    CREATE_FUNC(PlayAndRecord);
    
    __String* bookKey;

    virtual void onEnterTransitionDidFinish();
    
    void onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    void changeRecordStatus(bool record);
#endif

private:
    void createCocosRecorder();
    
    void createPlayProgressBar();
    void setPlayProgress(float value);
    cocos2d::Node *playProgressBar;
    cocos2d::Node *playProgressBarHolder;
    
    Label *createTitleLabel();
    __String *getTitle(int chapterIndex);

    Label *titleLabel;
    Label *titleLabel2;
    
    cocos2d::Action *hideUIAction;
    
    PlayAndRecordDelegate *_delegate;
    PlayAndRecordType _type;
    ChapterPlayer *player;

    bool onOpeningEnding;
    bool onTransition;
    bool recording;
    int _chapterIndex;
    
    bool visibleUI;

    bool playing;
    bool paused;
    
    Opening *openingLayer;
    Ending *endingLayer;
    
    Menu* menu;
    Menu* controlMenu;
    
    Node *playProgressBarBg;
//    Node *playProgressBarHolder;
    
    Node *playProgressBarParent;
    Node *playProgressBar1;
    Node *playProgressBar2;
    
    float totalDuration;
    float playedTime;
    
    cocos2d::Node * createResizableButton(const std::string& leftFileName, const std::string& centerFileName, const std::string& rightFileName,
                                             const std::string& iconFileName, const std::string& text, float fontSize, cocos2d::Point iconPos,
                                          float textLeftPadding, float textRightPadding);
    
    cocos2d::MenuItemImage *playMenuItem;
    cocos2d::MenuItemImage *pauseMenuItem;
    
    cocos2d::Sprite *selectedFrame;

    TransitionProgressOutIn *transition;
    float getTotalDuration();

    __String** getAudioFileList();
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    RenderTexture *flipRenderTexture;
#endif
    
};

#endif /* defined(__bobby__PlayAndRecordScene__) */
