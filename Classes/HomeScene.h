#ifndef __Home_SCENE_H__
#define __Home_SCENE_H__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ScrollViewExtension.h"
#include "PopupLayer.h"
#include "DoleAPI.h"
#include "ui/CocosGUI.h"
#include "TextColorButton.h"
#include "DimLayer.h"
#include "BookMapScene.h"
#include "CustomCameraScene.h"
#include "CharacterScrollView.h"
#include "BackgroundScrollView.h"
#include "MenuListLayer.h"
#include <cocosbuilder/CocosBuilder.h>

#include "TSDoleProtocol.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CharacterRecording;

class Home : public cocos2d::LayerColor, public ScrollViewExtensionDelegate, PopupLayerDelegate, TextColorButtonDelegate, DimLayerDelegate, BookMapDelegate, CustomCameraDelegate, CharacterScrollViewDelegate, BackgroundScrollViewDelegate, MenuListLayerDelegate, TSDoleProtocolDelegate, cocosbuilder::CCBAnimationManagerDelegate
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    ~Home();
    
    void initCloud(cocos2d::Size visibleSize);
    void initMenu(cocos2d::Size visibleSize);
    void initItems(cocos2d::Size visibleSize);
    void initBookScrollView(cocos2d::Size visibleSize);
    
    void showGuide();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    void menuCallback(cocos2d::Ref* pSender);
    void photoCallback(Ref* pSender);
//    void backgroundButtonCallback(Ref* pSender);
    void framePressedCallback(Ref* pSender);
    void characterPressedCallback(Ref* pSender);
    void facebookCallback(Ref* pSender);
    void toastCallback(Ref* pSender);
    void popupCallback(Ref* pSender);
    void popupButtonClicked(PopupLayer *popup, cocos2d::Ref *obj);

    void qrCodeCallback(Ref* pSender);
    
    void itemButtonClicked(Ref* obj);
    
    // scrollview extension delegate
    void scrollViewDidScroll(ScrollViewExtension* view);
    void scrollViewDidZoom(ScrollViewExtension* view);
    void scrollViewDidScrollEnd(ScrollViewExtension* view);
    void onSelectItem(ScrollViewExtension* view, Node* selectedItem, Touch *pTouch, Event *pEvent);
    
    // character scrollview delegate
    void onSelectCharacter(CharacterScrollView* view, Node* selectedItem, __String* characterKey, int index);
    
    // character scrollview delegate
    void onSelectBackground(BackgroundScrollView* view, Node* selectedItem, __String* backgroundKey, int index);
    
//    int splitTitleString(const std::string &s, char delim, std::string &firstLine, std::string &secondLine);
    std::string addNewBook();
    void removeBook(__String* bookKey);
    void runAddNewBookAnimation(__String* bookKey);
    void runRemoveBookAnimation(__String* bookKey);

    Sprite* createSideCharacter(__String* characterKey);
    
    Node* getBookCharacter(const char *ccbiName, cocos2d::Point position, float scale);
    
    void scrollToFirst(ScrollView *scrollView);
    void hideBottomScrollView();
    void showBackgroundScrollView();
    void showCharacterScrollView();
    bool isScrollViewVisible();

    virtual void onEnter();
    virtual void onExit();
    // implement the "static create()" method manually
    CREATE_FUNC(Home);
    

//    ScrollViewExtension* createBackgroundScrollView();
    ScrollViewExtension* bookScrollView;

    BackgroundScrollView *backgroundScrollView;
    CharacterScrollView* characterScrollView;
    
    cocos2d::Point scrollOffset;

    __Dictionary *booksSpriteDic;
    __String *selectedBookKey;
    Layer *bgLayer;
    __Array *booksKeyArray;

    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
    
    virtual void textColorButtonPressed(TextColorButton* button);
    virtual void textColorButtonNormal(TextColorButton* button);
    
    virtual void dimLayerRemoved();
    
    void runBookSideCharacterAnimation(__String *bookKey, float delay, bool isUpdate);
    void showBookSideCharacter(float currentOffset);
    void hideBookSideCharacter(float oldOffset, float newOffset);

    void updateCurrentBook(__String *bookKey);
    void onExitBookMap(__String *bookKey, bool added);

    void characterPhotoSaved(std::string characterKey);
    void bgPhotoSaved(std::string bgKey);
    void cameraScenePopped();
    
    void characterPhotoSavedInRecord(std::string characterKey);
    void bgPhotoSavedInRecord(std::string bgKey);

    void listButtonClicked(int buttonTag);
    void completedAnimationSequenceNamed(const char *name);
    
#pragma mark - Account & Dole Coin
private:
    TSDoleGetReNewAuthKey *_protocolReNewAuthKey;
    TSDoleGetBalance *_protocolGetBalance;
    TSDoleGetCacheSyncVersion *_protocolCacheVersion;
    TSDoleGetInventoryList *_protocolInventoryList;
    TSDoleGetEventNoticeList *_protocolNoticeList;
    TSDoleGetAppVersion *_protocolAppVersion;
    TSDoleExecutePurchase *_protocolExecutePurchase;
    
public:
    void onRequestStarted(TSDoleProtocol *protocol, TSNetResult result);
    void onResponseEnded(TSDoleProtocol *protocol, TSNetResult result);
    void onProtocolFailed(TSDoleProtocol *protocol, TSNetResult result);
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    void onRequestCanceled(TSDoleProtocol *protocol);
//#endif
    void onHttpRequestCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);
    void DoleAccountInitialized();
    void doleUpdateAuthKey();
    void doleCoinGetBalanceProtocol();
    void doleCacheSyncVersionProtocol();
    void doleGetProductProtocol();
    void doleCheckInventoryListUpdate();
    void doleGetInventoryListProtocol();
    void doleExecutePurchase();
    void doleExecuteCancel();
    void doleNoticeListProtocol();
    
    void runDoleCoinView(bool animated = true);
    void runAccountView(bool firstActive = false);
    void runSettingView();
    void runPasswordView();
    
    void doleCoinInit();
    void touchEventForUIButton(Ref *pSender, ui::TouchEventType type);
    void updateDoleCoin(int balance);
    void renewDoleCoin(int balance);

    bool loadProductSalesInfomation();
    void updateProductJsonInfomation();
    inline void setSelectedProduct(const char* name) { _seletedProductName = name; }
    inline const char* getSelectedProductName() { return _seletedProductName.c_str(); }

    void getUpdateVersion();
    void showPromotionPopup();
    void reloadScrollViews();
    
protected:
    std::string _seletedProductName;
    int _selectedProductType; // -1 nil; 0 char; 1 bg
    __Array *_productList;
    int _doleCoinBalance;
    
public:
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    void updateCustomCharacater(int index, const char* path);
    void updateCustomBackground(const char* path);
    void pendingShowGuide();
    void updateDoleCoinAndroid();
    void renewDoleCoinAndroid(int balance);
#endif
    void deleteCharItemInRecordScene(Ref *obj);
    void deleteBgItemInRecordScene(Ref *obj);
#pragma mark - class member variable
private:
    bool isBookAnimation;
    bool isAddingBookAnimation;
    bool isScrollByAdd;
    bool movingScene;
    
    bool scrollViewToggleAnimation;
    
    ui::Button *selDeleteBtn;
    
    cocos2d::Menu* bottomMenu;

    bool isBackgroundScrollViewVisible;
    bool isCharacterScrollViewVisible;

    bool isPlatformShown;
    bool isCharacterSelected;
    
    void bgItemScrollCoinClicked(Ref* pSender);
    void bgItemCoinClicked(Ref *pSender, ui::TouchEventType type);
    void createButtonClicked(Ref* obj, ui::TouchEventType type);
    
    bool openBgItem;
    bool openCharacterItem;
    bool bgItemClicked;
    
    bool reachedToEnd;
    
    Label *createButtonLabel;
    void setEnableButtons(bool enable);
    Scene *cameraScene;
    
//    ui::Button *deleteButton;
    ui::Button *createBgButton;
    
    Sprite* getCcbiSprite(const char* ccbiName, cocos2d::Point position);
    
    void setCreateDeleteButtons(bool visible);
    
    MenuListLayer *menuListLayer;
    void scrollViewButtonEnable(bool backgroundButtonEnable);
    
    void preloadSoundEffects();
    std::vector<std::string> splitByDelim(const std::string &s, char delim);

    std::string appURL;
    bool isBookBgForCustom(const char* bookBgName);
    __String* saveNewBookBgtoJson(rapidjson::Document &bookJson, const char* bookKey, bool isCustomBg, const char* chapterBgKey);
    __String* setDefaultBookBg(rapidjson::Document &bookJson, const char* bookKey);
    
    Layer *mainLayer;
    void fadeInAfterSplash();
    
    void deleteButtonClicked(Ref *obj, ui::TouchEventType type);
    void savePromotionFirstInitDate();
    
    long getLimitTime();
    
//    void itemDeleteButtonClicked(Ref *obj, ui::TouchEventType type);
//    void deleteModeChange();
//    bool isItemDeleteMode;
    
    CharacterRecording *_charecLayer;
    int _isPurchasReady;
public:
    void checkRecall(int type);
    __Array* getProducts() { return _productList; }
    int getDoleCoin() { return _doleCoinBalance; }
    void setChaRecLayer(CharacterRecording *layer) { _charecLayer = layer; }
};

#endif // __Home_SCENE_H__
