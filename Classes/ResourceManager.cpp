//
//  ResourceManager.cpp
//  bobby
//
//  Created by Lee mu hyeon on 2014. 4. 15..
//
//

#include "ResourceManager.h"

USING_NS_CC;
using namespace std;

static Resource iPhoneResource      	= { cocos2d::Size(960, 640),    "iphone35",     ResolutionType::iphone35,   	cocos2d::Size(960, 640)     };
static Resource iPhoneWideResource  	= { cocos2d::Size(1136, 640),   "iphone4",      ResolutionType::iphone4,    	cocos2d::Size(1136, 640)    };
static Resource iPadResource        	= { cocos2d::Size(1024, 768),   "ipad",         ResolutionType::ipad,       	cocos2d::Size(1024, 768)    };
static Resource iPadHDResource      	= { cocos2d::Size(2048, 1536),  "ipadhd",       ResolutionType::ipadhd,     	cocos2d::Size(2048, 1536)   };
static Resource androidFullHDResource	= { cocos2d::Size(1920, 1080),  "androidfullhd",ResolutionType::androidfullhd,  cocos2d::Size(1920, 1080)   };
static Resource androidHDResource		= { cocos2d::Size(1280, 720),   "androidhd",    ResolutionType::androidhd,  	cocos2d::Size(1280, 720)    };
static Resource guideResource			= { cocos2d::Size(640, 360),    "guide",        ResolutionType::androidfullhd,  cocos2d::Size(640, 360)     };

static ResourceManager *s_pResourceMgr;

void changePositionAtAnchor(Node *node, Point anchor) {
    Point currentPosition = node->getPosition();
    Point currentAnchor = node->getAnchorPoint();
    Size objectSize = node->getContentSize();
    Point originPosition = Point(currentPosition.x - (objectSize.width * currentAnchor.x), currentPosition.y - (objectSize.height * currentAnchor.y));
    node->setAnchorPoint(anchor);
    node->setPosition(Point(originPosition.x + (objectSize.width * anchor.x), originPosition.y + (objectSize.height * anchor.y)));
}

ResourceManager::ResourceManager()
{
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
        glview = GLView::create("Bobby Adventure");
        glview->setFrameSize(1024, 768);
//        glview->setFrameSize(1920,1080);
        director->setOpenGLView(glview);
    }
    
	Size frameSize = glview->getFrameSize();
    log("frameSize(%d,%d)", (int)frameSize.width, (int)frameSize.height);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    if(frameSize.width == androidFullHDResource.size.width)
    	this->currentResource = &androidFullHDResource;
    else
    	this->currentResource = &androidHDResource;
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
    if (frameSize.width == iPhoneWideResource.size.width)
        this->currentResource = &iPhoneWideResource;
    else if (frameSize.width == iPadHDResource.size.width)
        this->currentResource = &iPadHDResource;
    else if (frameSize.width == iPadResource.size.width)
        this->currentResource = &iPadResource;
    else if (frameSize.width == iPhoneResource.size.width)
        this->currentResource = &iPhoneResource;
	else
        this->currentResource = &guideResource;
#endif
    
    this->currentType = this->currentResource->type;
    this->currentLanguageType = Application::getInstance()->getCurrentLanguage();
    
    vector<string> searchPath;
    searchPath.push_back("common");
    if (this->currentType==ResolutionType::iphone4) {
        searchPath.push_back("iphone");
    } else if (this->currentType==ResolutionType::iphone35) {
        searchPath.push_back("iphone35");
        searchPath.push_back("iphone");
    }
    
    searchPath.push_back(this->currentResource->directory);
    log("searchPath %s", this->currentResource->directory);
    FileUtils::getInstance()->setSearchPaths(searchPath);
    
    glview->setDesignResolutionSize(this->currentResource->designResolution.width,
                                    this->currentResource->designResolution.height,
                                    ResolutionPolicy::FIXED_WIDTH);
    
    float scaleFromHeight = this->currentResource->size.height / this->currentResource->designResolution.height;
    float scaleFromWidth = this->currentResource->size.width / this->currentResource->designResolution.width;
    director->setContentScaleFactor( MIN(scaleFromHeight, scaleFromWidth) );
    
    float scaleFactor = director->getContentScaleFactor();
    log("contents scale factor : %f", scaleFactor);
}

ResourceManager::~ResourceManager() {
    
}

ResourceManager* ResourceManager::getInstance() {
    if (! s_pResourceMgr) {
        s_pResourceMgr = new ResourceManager();
    }
    return s_pResourceMgr;
}

// POS_C_FUNC_START  **do not delete this line**

cocos2d::Point ResourceManager::home_test_menu_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((16+130+884-100),534);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((16+130+884-100)*2/3,534*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((2048-50-186-200),(1310));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((2048-50-186-200))/2,((1310))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((16+130+884-100),534);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((16+130+884)-176-100,534);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_test_menu_facebook() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((16+130+884-200),534);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((16+130+884-200)*2/3,534*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((2048-50-186-400),(1310));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((2048-50-186-400))/2,((1310))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((16+130+884-200),534);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((16+130+884)-176-200,534);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_test_menu_popup() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((16+130+884-300),534);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((16+130+884-300)*2/3,534*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((2048-50-186-600),(1310));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((2048-50-186-600))/2,((1310))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((16+130+884-300),534);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((16+130+884)-176-300,534);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_test_menu_qrcode() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((16+130+884-400),534);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((16+130+884-400)*2/3,534*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((2048-50-186-800),(1310));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((2048-50-186-800))/2,((1310))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((16+130+884-400),534);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((16+130+884)-176-400,534);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_test_menu_toast() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((16+130+884-500),534);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((16+130+884-500)*2/3,534*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((2048-50-186-1000),(1310));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((2048-50-186-1000))/2,((1310))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((16+130+884-500),534);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((16+130+884)-176-500,534);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_logo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(33+160+38,950);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((33+160+38)*2/3,950*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(50+220+54,1350);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((50+220+54)/2,(1350)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(16+106+11,558);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(16+106+11,558);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_logo_dole() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(33,950);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(33*2/3,950*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(50,1350);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((50)/2,(1350)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(16,558);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(16,558);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_logo_divider() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(211,1080-68-45);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(211*2/3,(1080-68-45)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(50+220+25,1350+126+60-96-64);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((50+220+25)/2,(1350+126+60-96-64)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(16+106+5,558+66+16-39-27);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(16+106+5,558+66+16-39-27);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_menu_btn() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((1920-148-30),(1080-150-30));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((1920-148-30)*2/3,(1080-150-30)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((2048-50-186),(1310));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((2048-50-186))/2,((1310))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((16+130+884),534);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((16+130+884)-176,534);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::home_main_layer_hide_height() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (340);
        }
        case ResolutionType::androidhd: {
            return (340*2/3);
        }
        case ResolutionType::ipadhd: {
            return (484);
        }
        case ResolutionType::ipad: {
            return (484)/2;
        }
        case ResolutionType::iphone4: {
            return (204);
        }
        case ResolutionType::iphone35: {
            return (204);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::home_bg() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(0,-340);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(0,-340*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(0,-484);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((0)/2,(-484)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(0,-204);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(0,-204);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_bg_home_cloud_01() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(555,760);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(555*2/3,760*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(80,1080);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((80)/2,(1080)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(40,448);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(40,448);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_bg_home_cloud_02() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(555+234+392,918);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((555+234+392)*2/3,918*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(80+330+560,1306);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((80+330+560)/2,(1306)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((40+140+232),544);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((40+140+232),544);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_coin() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(40,80);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(40*2/3,80*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(50,114);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((50)/2,(114)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(20,48);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(20,48);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_x() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(176+20,137);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((176+20)*2/3,137*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((242+30),196);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((242+30))/2,(196)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(113,82);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(113,82);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_coin_num() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((176+20+42+16),(124+34));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((176+20+42+16)*2/3,(124+34)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((242+30+56+26),178);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((242+30+56+26))/2,(178)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(113+25+11,74);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(113+25+11,74);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::home_coin_before_login_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(40,(68+20));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(40*2/3,(68+20)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(52,94);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((52)/2,(94)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(24,44);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(24,44);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::home_coin_num_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(258,(68+20));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(258*2/3,(68+20)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(334,94);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((334)/2,(94)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(140,42);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(140,42);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::home_coin_num_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (64);
        }
        case ResolutionType::androidhd: {
            return (64*2/3);
        }
        case ResolutionType::ipadhd: {
            return (90);
        }
        case ResolutionType::ipad: {
            return (90)/2;
        }
        case ResolutionType::iphone4: {
            return (38);
        }
        case ResolutionType::iphone35: {
            return (38);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::home_item_menu() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(1510,80);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(1510*2/3,80*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(1498,116);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((1498)/2,(116)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(888,48);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(888-176,48);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_item_btn() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((170+40),0);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((170+40)*2/3,0);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((230+40),0);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((230+40))/2,(0)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((102+24),0);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((102+24),0);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::home_item_scroll_height() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (340);
        }
        case ResolutionType::androidhd: {
            return (340*2/3);
        }
        case ResolutionType::ipadhd: {
            return (484);
        }
        case ResolutionType::ipad: {
            return (484)/2;
        }
        case ResolutionType::iphone4: {
            return (204);
        }
        case ResolutionType::iphone35: {
            return (204);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::home_new_book() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(220,52);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(220*2/3,52*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(200,64);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((200)/2,(64)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(130,32);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(130,32);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_new_book_layer() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(220-14,52-9);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((220-14)*2/3,(52-9)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((200-40),(64-12));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((200-40))/2,((64-12))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((130-8-8),(32-12));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((130-8-8),(32-12));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::home_book_layer_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(458+14+14,598+9);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size((458+14+14)*2/3,(598+9)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size((652+40),(850+12));
        }
        case ResolutionType::ipad: {
            return cocos2d::Size(((652+40))/2,((850+12))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size((274+8+8),(356+6));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size((274+8+8),(356+6));
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::home_book_bg() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(14,9);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(14*2/3,9*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(20,12);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((20)/2,(12)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(8,6);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(8,6);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_book_character_01() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(457,(223+181));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((457)*2/3,(223+181)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(652-4,344+232);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((652-4)/2,(344+232)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(272-2,120+104);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(272-2,120+104);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_book_character_02() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(457,223);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((457)*2/3,223*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(652-4,344);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((652-4)/2,(344)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(272-2,120);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(272-2,120);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_book_delete_button() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(14,18);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(14*2/3,18*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(20,24);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((20)/2,(24)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(8,10);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(8,10);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_book_preload_author() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(214,18);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((214)*2/3,(18)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((306),(24));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((306))/2,((24))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((128),(10));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((128),(10));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_book_author() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((62),(9+18-8));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((62)*2/3,(9+18-8)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((94),(12+24));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((94))/2,((12+24))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((40),(10+6));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((40),(10+6));
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::home_book_author_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (36);
                } break;
                case LanguageType::KOREAN: {
                    return (32);
                } break;
                case LanguageType::JAPANESE: {
                    return (32);
                } break;
            }
        }
        case ResolutionType::androidhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (36*2/3);
                } break;
                case LanguageType::KOREAN: {
                    return (32*2/3);
                } break;
                case LanguageType::JAPANESE: {
                    return (32*2/3);
                } break;
            }
        }
        case ResolutionType::ipadhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (50);
                } break;
                case LanguageType::KOREAN: {
                    return (46);
                } break;
                case LanguageType::JAPANESE: {
                    return (46);
                } break;
            }
        }
        case ResolutionType::ipad: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (50)/2;
                } break;
                case LanguageType::KOREAN: {
                    return (46)/2;
                } break;
                case LanguageType::JAPANESE: {
                    return (46)/2;
                } break;
            }
        }
        case ResolutionType::iphone4: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (22);
                } break;
                case LanguageType::KOREAN: {
                    return (18);
                } break;
                case LanguageType::JAPANESE: {
                    return (18);
                } break;
            }
        }
        case ResolutionType::iphone35: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (22);
                } break;
                case LanguageType::KOREAN: {
                    return (18);
                } break;
                case LanguageType::JAPANESE: {
                    return (18);
                } break;
            }
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::home_book_author_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(368,(40+16));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(368*2/3,(40+16)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(518,54);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((518)/2,(54)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(218,24);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(218,24);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::home_book_start_y() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return ((52-9));
        }
        case ResolutionType::androidhd: {
            return ((52-9)*2/3);
        }
        case ResolutionType::ipadhd: {
            return ((64-12));
        }
        case ResolutionType::ipad: {
            return ((64-12))/2;
        }
        case ResolutionType::iphone4: {
            return ((30-6));
        }
        case ResolutionType::iphone35: {
            return ((30-6));
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::home_book_list_view() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(0,207-52);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(0,(207-52)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(0,(288)-(64));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((0)/2,((288)-(64))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(0,(122-30));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(0,(122-30));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::home_book_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(14+458+14,598+9);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size((14+458+14)*2/3,(598+9)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size((652+40),(850+20));
        }
        case ResolutionType::ipad: {
            return cocos2d::Size(((652+40))/2,((850+20))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(274+8+8,356+6);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(274+8+8,356+6);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::home_book_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (260-14-14);
        }
        case ResolutionType::androidhd: {
            return ((260-14-14)*2/3);
        }
        case ResolutionType::ipadhd: {
            return ((200-40));
        }
        case ResolutionType::ipad: {
            return ((200-40))/2;
        }
        case ResolutionType::iphone4: {
            return ((154-8-8));
        }
        case ResolutionType::iphone35: {
            return ((154-8-8));
        }
    }
    return 0.0f;
}

float ResourceManager::home_book_scroll_hieght() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (598+52);
        }
        case ResolutionType::androidhd: {
            return ((598+52)*2/3);
        }
        case ResolutionType::ipadhd: {
            return ((850+64));
        }
        case ResolutionType::ipad: {
            return ((850+64))/2;
        }
        case ResolutionType::iphone4: {
            return ((356+30));
        }
        case ResolutionType::iphone35: {
            return ((356+30));
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::home_new_book_text() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(62,43);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(62*2/3,43*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(84,70);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((84)/2,(70)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(38,32);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(38,32);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_new_book_jp_text_first() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(62,(63+54));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(62*2/3,(63+54)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(84,82+64);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((84)/2,(82+64)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(38,38+32);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(38,38+32);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_new_book_jp_text_second() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(62,63);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(62*2/3,63*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(84,82);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((84)/2,(82)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(38,38);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(38,38);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::home_new_book_title_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (92);
                } break;
                case LanguageType::KOREAN: {
                    return (84);
                } break;
                case LanguageType::JAPANESE: {
                    return (50);
                } break;
            }
        }
        case ResolutionType::androidhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (92*2/3);
                } break;
                case LanguageType::KOREAN: {
                    return (84*2/3);
                } break;
                case LanguageType::JAPANESE: {
                    return (50*2/3);
                } break;
            }
        }
        case ResolutionType::ipadhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (110);
                } break;
                case LanguageType::KOREAN: {
                    return (96);
                } break;
                case LanguageType::JAPANESE: {
                    return ((92-4));
                } break;
            }
        }
        case ResolutionType::ipad: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (110)/2;
                } break;
                case LanguageType::KOREAN: {
                    return (96)/2;
                } break;
                case LanguageType::JAPANESE: {
                    return ((92-4))/2;
                } break;
            }
        }
        case ResolutionType::iphone4: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (54);
                } break;
                case LanguageType::KOREAN: {
                    return (48);
                } break;
                case LanguageType::JAPANESE: {
                    return (40);
                } break;
            }
        }
        case ResolutionType::iphone35: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (54);
                } break;
                case LanguageType::KOREAN: {
                    return (48);
                } break;
                case LanguageType::JAPANESE: {
                    return (40);
                } break;
            }
        }
    }
    return 0.0f;
}

float ResourceManager::home_new_book_jp_title_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (92);
                } break;
                case LanguageType::KOREAN: {
                    return (84);
                } break;
                case LanguageType::JAPANESE: {
                    return (50);
                } break;
            }
        }
        case ResolutionType::androidhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (92*2/3);
                } break;
                case LanguageType::KOREAN: {
                    return (84*2/3);
                } break;
                case LanguageType::JAPANESE: {
                    return (50*2/3);
                } break;
            }
        }
        case ResolutionType::ipadhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (110);
                } break;
                case LanguageType::KOREAN: {
                    return (96);
                } break;
                case LanguageType::JAPANESE: {
                    return (60);
                } break;
            }
        }
        case ResolutionType::ipad: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (110)/2;
                } break;
                case LanguageType::KOREAN: {
                    return (96)/2;
                } break;
                case LanguageType::JAPANESE: {
                    return (60)/2;
                } break;
            }
        }
        case ResolutionType::iphone4: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (54);
                } break;
                case LanguageType::KOREAN: {
                    return (48);
                } break;
                case LanguageType::JAPANESE: {
                    return (28);
                } break;
            }
        }
        case ResolutionType::iphone35: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (54);
                } break;
                case LanguageType::KOREAN: {
                    return (48);
                } break;
                case LanguageType::JAPANESE: {
                    return (28);
                } break;
            }
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::home_new_book_text_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(280,148);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(280*2/3,148*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(356,114);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((356)/2,(114)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(166,56);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(166,56);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::home_new_book_jp_text_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(280,54);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(280*2/3,54*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(356,64);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((356)/2,(64)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(166,32);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(166,32);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::home_new_book_layer_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size((14+370+14), 485+9);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size((14+370+14)*2/3,(485+9)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size((20+480+20), 626+12);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size(((20+480+20))/2,( 626+12)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size((8+220+8), 288+6);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size((8+220+8), 288+6);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::home_book_title() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(62,394+4+72+6);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(62*2/3,(394+4+72+6)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(94,(550+20+100+10));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((94)/2,((550+20+100+10))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(40,(234+6+42+2));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(40,(234+6+42+2));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_book_title_a_line() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(62,394+4+72+6-(92-50));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(62*2/3,(394+4+72+6-(92-50))*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(94,(550+20+100+10-(125-70)));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((94)/2,((550+20+100+10-(125-70)))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(40,(234+6+42+2-(58-28)));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(40,(234+6+42+2-(58-28)));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_pre_book_title() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((62-28/2+4),(394+4+72+6));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((62-28/2+4)*2/3,(394+4+72+6)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(94-30,(550+20+100+10));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((94-30)/2,((550+20+100+10))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(40-12,(234+6+42+2));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(40-12,(234+6+42+2));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::home_book_title_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(368,92);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(368*2/3,92*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(518,100);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((518)/2,(100)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(218,42);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(218,42);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::home_pre_book_title_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size((368+28-2-4),92);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size((368+28-2-4)*2/3,92*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(518+60,100);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((518+60)/2,(100)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(218+24,42);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(218+24,42);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::home_book_title_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (68);
                } break;
                case LanguageType::KOREAN: {
                    return (62);
                } break;
                case LanguageType::JAPANESE: {
                    return (62-8);
                } break;
            }
        }
        case ResolutionType::androidhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (68*2/3);
                } break;
                case LanguageType::KOREAN: {
                    return (62*2/3);
                } break;
                case LanguageType::JAPANESE: {
                    return ((62-8)*2/3);
                } break;
            }
        }
        case ResolutionType::ipadhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (96);
                } break;
                case LanguageType::KOREAN: {
                    return (92);
                } break;
                case LanguageType::JAPANESE: {
                    return (92-14);
                } break;
            }
        }
        case ResolutionType::ipad: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (96)/2;
                } break;
                case LanguageType::KOREAN: {
                    return (92)/2;
                } break;
                case LanguageType::JAPANESE: {
                    return (92-14)/2;
                } break;
            }
        }
        case ResolutionType::iphone4: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (40);
                } break;
                case LanguageType::KOREAN: {
                    return (34);
                } break;
                case LanguageType::JAPANESE: {
                    return (34-4);
                } break;
            }
        }
        case ResolutionType::iphone35: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (40);
                } break;
                case LanguageType::KOREAN: {
                    return (34);
                } break;
                case LanguageType::JAPANESE: {
                    return (34-4);
                } break;
            }
        }
    }
    return 0.0f;
}

float ResourceManager::home_book_title_pre_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (68);
                } break;
                case LanguageType::KOREAN: {
                    return (62);
                } break;
                case LanguageType::JAPANESE: {
                    return (57);
                } break;
            }
        }
        case ResolutionType::androidhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (68*2/3);
                } break;
                case LanguageType::KOREAN: {
                    return (62*2/3);
                } break;
                case LanguageType::JAPANESE: {
                    return ((57)*2/3);
                } break;
            }
        }
        case ResolutionType::ipadhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (96);
                } break;
                case LanguageType::KOREAN: {
                    return (92);
                } break;
                case LanguageType::JAPANESE: {
                    return (92);
                } break;
            }
        }
        case ResolutionType::ipad: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (96)/2;
                } break;
                case LanguageType::KOREAN: {
                    return (92)/2;
                } break;
                case LanguageType::JAPANESE: {
                    return (92)/2;
                } break;
            }
        }
        case ResolutionType::iphone4: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (40);
                } break;
                case LanguageType::KOREAN: {
                    return (34);
                } break;
                case LanguageType::JAPANESE: {
                    return (34);
                } break;
            }
        }
        case ResolutionType::iphone35: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (40);
                } break;
                case LanguageType::KOREAN: {
                    return (34);
                } break;
                case LanguageType::JAPANESE: {
                    return (34);
                } break;
            }
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::home_book_title_sub() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(62,394+4);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(62*2/3,(394+4)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(94,(550+20));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((94)/2,((550+20))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(40,(234+6));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(40,(234+6));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_background_scroll_view() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(0,-340);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(0,-340*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(0,(-484));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((0)/2,((-484))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(0,(-204));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(0,(-204));
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::home_background_scroll_view_height() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (370);
        }
        case ResolutionType::androidhd: {
            return (370*2/3);
        }
        case ResolutionType::ipadhd: {
            return (336+180);
        }
        case ResolutionType::ipad: {
            return (336+180)/2;
        }
        case ResolutionType::iphone4: {
            return (138+84);
        }
        case ResolutionType::iphone35: {
            return (138+84);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::home_background_frame_type_01() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(0,0);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(0,0);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(0,0);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((0)/2,(0)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(0,0);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(0,0);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_background_frame_type_02() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(0,42);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(0,42*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(0,50);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((0)/2,(50)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(0,25);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(0,25);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::home_background_scroll_view_startpadding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (61);
        }
        case ResolutionType::androidhd: {
            return (61*2/3);
        }
        case ResolutionType::ipadhd: {
            return (88);
        }
        case ResolutionType::ipad: {
            return (88)/2;
        }
        case ResolutionType::iphone4: {
            return (36);
        }
        case ResolutionType::iphone35: {
            return (36);
        }
    }
    return 0.0f;
}

float ResourceManager::home_background_scroll_coin_fontsize_01() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (52);
        }
        case ResolutionType::androidhd: {
            return (52*2/3);
        }
        case ResolutionType::ipadhd: {
            return (76);
        }
        case ResolutionType::ipad: {
            return (76)/2;
        }
        case ResolutionType::iphone4: {
            return (30);
        }
        case ResolutionType::iphone35: {
            return (30);
        }
    }
    return 0.0f;
}

float ResourceManager::home_background_scroll_coin_fontsize_02() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (40);
        }
        case ResolutionType::androidhd: {
            return (40*2/3);
        }
        case ResolutionType::ipadhd: {
            return (58);
        }
        case ResolutionType::ipad: {
            return (58)/2;
        }
        case ResolutionType::iphone4: {
            return (24);
        }
        case ResolutionType::iphone35: {
            return (24);
        }
    }
    return 0.0f;
}

float ResourceManager::home_background_scroll_coin_fontsize_03() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (35);
        }
        case ResolutionType::androidhd: {
            return (35*2/3);
        }
        case ResolutionType::ipadhd: {
            return (50);
        }
        case ResolutionType::ipad: {
            return (50)/2;
        }
        case ResolutionType::iphone4: {
            return (21);
        }
        case ResolutionType::iphone35: {
            return (21);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::home_background_scroll_coin_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(63,90);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(63*2/3,90*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(92,50+32);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((92)/2,(50+32)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(36,25+12);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(36,25+12);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::home_background_scroll_coin() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((28-92),(222-74-74-14-93));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((28-92)*2/3,(222-74-74-14-93)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((-130+34),(-34));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((-130+34))/2,((-34))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((-54+11),(-11));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((-54+11),(-11));
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::home_background_scroll_end_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (112);
        }
        case ResolutionType::androidhd: {
            return (112*2/3);
        }
        case ResolutionType::ipadhd: {
            return (112);
        }
        case ResolutionType::ipad: {
            return (112)/2;
        }
        case ResolutionType::iphone4: {
            return (68);
        }
        case ResolutionType::iphone35: {
            return (68);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::home_background_line_start() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(16,213);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(16*2/3,213*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(0,336);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((0)/2,(336)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(0,138);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(0,138);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_background_check() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(184,(279-67-79));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(184*2/3,(279-67-79)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(536/2-120/2,194);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((536/2-120/2)/2,(194)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(258/2-50/2,80);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(258/2-50/2,80);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_background_coin_text() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(29,34);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(29*2/3,34*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(56,102-32/2);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((56)/2,(102-32/2)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(20,40-12/2);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(20,40-12/2);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::home_bg_select_coin_fontsize_01() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (112);
        }
        case ResolutionType::androidhd: {
            return (112*2/3);
        }
        case ResolutionType::ipadhd: {
            return (162);
        }
        case ResolutionType::ipad: {
            return (162)/2;
        }
        case ResolutionType::iphone4: {
            return (66);
        }
        case ResolutionType::iphone35: {
            return (66);
        }
    }
    return 0.0f;
}

float ResourceManager::home_bg_select_coin_fontsize_02() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (92);
        }
        case ResolutionType::androidhd: {
            return (92*2/3);
        }
        case ResolutionType::ipadhd: {
            return (134);
        }
        case ResolutionType::ipad: {
            return (134)/2;
        }
        case ResolutionType::iphone4: {
            return (54);
        }
        case ResolutionType::iphone35: {
            return (54);
        }
    }
    return 0.0f;
}

float ResourceManager::home_bg_select_coin_fontsize_03() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (78);
        }
        case ResolutionType::androidhd: {
            return (78*2/3);
        }
        case ResolutionType::ipadhd: {
            return (112);
        }
        case ResolutionType::ipad: {
            return (112)/2;
        }
        case ResolutionType::iphone4: {
            return (46);
        }
        case ResolutionType::iphone35: {
            return (46);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::home_bg_select_coin_margin() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((35),18);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((35)*2/3,18*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(68,22);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((68)/2,(22)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(22,10);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(22,10);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_background_unlock_talk_box() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(648,217);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(648*2/3,217*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(60+646+60,92+50+18+50+60);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((60+646+60)/2,(92+50+18+50+60)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(30+330+30,28+28+4+28+46);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(30+330+30,28+28+4+28+46);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::home_item_talk_box_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(648,217);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(648*2/3,217*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(60+646+60,92+50+18+50+60);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((60+646+60)/2,(92+50+18+50+60)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(30+330+30,28+28+4+28+46);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(30+330+30,28+28+4+28+46);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::home_item_talkbox_inset_right_tail() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(52,33);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(52*2/3,33*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(370/3,268/3);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((370/3)/2,(268/3)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(184/3,134/3);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(184/3,134/3);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::home_item_talkbox_inset_right_tail_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(1,1);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(1,1);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(20,50);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((20)/2,(50)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(1,20);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(1,20);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::home_item_talkbox_inset_left_tail() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(112,51);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(112*2/3,51*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(370/2,268/2);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((370/2)/2,(268/2)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(184/2,134/2);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(184/2,134/2);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::home_item_talkbox_inset_left_tail_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(1,1);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(1,1);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(20,50);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((20)/2,(50)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(1,20);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(1,20);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::home_item_select_coin_margin() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(12,-28);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(12*2/3,-28*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(52,-14);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((52)/2,(-14)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(14,-4);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(14,-4);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::home_item_select_text_area_width() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (408);
        }
        case ResolutionType::androidhd: {
            return (408*2/3);
        }
        case ResolutionType::ipadhd: {
            return (482);
        }
        case ResolutionType::ipad: {
            return (482)/2;
        }
        case ResolutionType::iphone4: {
            return (250);
        }
        case ResolutionType::iphone35: {
            return (250);
        }
    }
    return 0.0f;
}

float ResourceManager::home_item_select_text_bg_mid_height() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (217);
        }
        case ResolutionType::androidhd: {
            return (217*2/3);
        }
        case ResolutionType::ipadhd: {
            return (280);
        }
        case ResolutionType::ipad: {
            return (280)/2;
        }
        case ResolutionType::iphone4: {
            return (128);
        }
        case ResolutionType::iphone35: {
            return (128);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::home_item_talkbox() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(114,1);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(114*2/3,1*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(92,4);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((92)/2,(4)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(70,2);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(70,2);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::home_bg_select_coin_text_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(200,207);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(200*2/3,207*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(200,166+22);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((200)/2,(166+22)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(84,70+12);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(84,70+12);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::home_bottom_scroll_view_frame_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(438,279);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(438*2/3,279*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(536,408);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((536)/2,(408)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(258,172);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(258,172);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::home_bottom_scroll_view_frame_start_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (61);
        }
        case ResolutionType::androidhd: {
            return (61*2/3);
        }
        case ResolutionType::ipadhd: {
            return (88);
        }
        case ResolutionType::ipad: {
            return (88)/2;
        }
        case ResolutionType::iphone4: {
            return (36);
        }
        case ResolutionType::iphone35: {
            return (36);
        }
    }
    return 0.0f;
}

float ResourceManager::home_bottom_scroll_view_frame_bottom_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (42);
        }
        case ResolutionType::androidhd: {
            return (42*2/3);
        }
        case ResolutionType::ipadhd: {
            return (50);
        }
        case ResolutionType::ipad: {
            return (50)/2;
        }
        case ResolutionType::iphone4: {
            return (25);
        }
        case ResolutionType::iphone35: {
            return (25);
        }
    }
    return 0.0f;
}

float ResourceManager::home_bottom_scroll_view_character_item_start_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (61);
        }
        case ResolutionType::androidhd: {
            return (61*2/3);
        }
        case ResolutionType::ipadhd: {
            return (90);
        }
        case ResolutionType::ipad: {
            return (90)/2;
        }
        case ResolutionType::iphone4: {
            return (36);
        }
        case ResolutionType::iphone35: {
            return (36);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::home_bottom_scroll_view_character_item_holder_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(292,324);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(292*2/3,324*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(358,408);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((358)/2,(408)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(172,188);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(172,188);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::home_bottom_scroll_view_character_item_bottom_padding1() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (286-324);
        }
        case ResolutionType::androidhd: {
            return ((286-324)*2/3);
        }
        case ResolutionType::ipadhd: {
            return (0);
        }
        case ResolutionType::ipad: {
            return (0)/2;
        }
        case ResolutionType::iphone4: {
            return (172-188);
        }
        case ResolutionType::iphone35: {
            return (172-188);
        }
    }
    return 0.0f;
}

float ResourceManager::home_bottom_scroll_view_character_item_bottom_padding2() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (309-324);
        }
        case ResolutionType::androidhd: {
            return ((309-324)*2/3);
        }
        case ResolutionType::ipadhd: {
            return (26);
        }
        case ResolutionType::ipad: {
            return (26)/2;
        }
        case ResolutionType::iphone4: {
            return (186-188);
        }
        case ResolutionType::iphone35: {
            return (186-188);
        }
    }
    return 0.0f;
}

float ResourceManager::home_bottom_scroll_view_character_item_bottom_padding3() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (1);
        }
        case ResolutionType::androidhd: {
            return (1*2/3);
        }
        case ResolutionType::ipadhd: {
            return (56);
        }
        case ResolutionType::ipad: {
            return (56)/2;
        }
        case ResolutionType::iphone4: {
            return (8);
        }
        case ResolutionType::iphone35: {
            return (8);
        }
    }
    return 0.0f;
}

float ResourceManager::home_bottom_scroll_view_music_item_bottom_padding1() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (286-324);
        }
        case ResolutionType::androidhd: {
            return ((286-324)*2/3);
        }
        case ResolutionType::ipadhd: {
            return (0);
        }
        case ResolutionType::ipad: {
            return (0)/2;
        }
        case ResolutionType::iphone4: {
            return (0);
        }
        case ResolutionType::iphone35: {
            return (0);
        }
    }
    return 0.0f;
}

float ResourceManager::home_bottom_scroll_view_music_item_bottom_padding2() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (309-324);
        }
        case ResolutionType::androidhd: {
            return ((309-324)*2/3);
        }
        case ResolutionType::ipadhd: {
            return (26);
        }
        case ResolutionType::ipad: {
            return (26)/2;
        }
        case ResolutionType::iphone4: {
            return (14);
        }
        case ResolutionType::iphone35: {
            return (14);
        }
    }
    return 0.0f;
}

float ResourceManager::home_bottom_scroll_view_music_item_bottom_padding3() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (1);
        }
        case ResolutionType::androidhd: {
            return (1*2/3);
        }
        case ResolutionType::ipadhd: {
            return (56);
        }
        case ResolutionType::ipad: {
            return (56)/2;
        }
        case ResolutionType::iphone4: {
            return (24);
        }
        case ResolutionType::iphone35: {
            return (24);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::home_bottom_scroll_view_music_item_holder_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(292,324);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(292*2/3,324*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(358,408);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((358)/2,(408)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(172,172);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(172,172);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::home_bottom_scroll_view_music_pin_top_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (16);
        }
        case ResolutionType::androidhd: {
            return (16*2/3);
        }
        case ResolutionType::ipadhd: {
            return (32);
        }
        case ResolutionType::ipad: {
            return (32)/2;
        }
        case ResolutionType::iphone4: {
            return (10);
        }
        case ResolutionType::iphone35: {
            return (10);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::home_bottom_scroll_view_icon_in_character() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(30,10);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(30*2/3,10*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(78,-38);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((78)/2,(-38)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(20,-2);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(20,-2);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::home_bottom_scroll_view_character_pin_top_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (16);
        }
        case ResolutionType::androidhd: {
            return (16*2/3);
        }
        case ResolutionType::ipadhd: {
            return (32);
        }
        case ResolutionType::ipad: {
            return (32)/2;
        }
        case ResolutionType::iphone4: {
            return (10);
        }
        case ResolutionType::iphone35: {
            return (10);
        }
    }
    return 0.0f;
}

float ResourceManager::home_bottom_scroll_view_center_icon_bottom_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (279-67-79);
        }
        case ResolutionType::androidhd: {
            return ((279-67-79)*2/3);
        }
        case ResolutionType::ipadhd: {
            return (194);
        }
        case ResolutionType::ipad: {
            return (194)/2;
        }
        case ResolutionType::iphone4: {
            return (80);
        }
        case ResolutionType::iphone35: {
            return (80);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::home_bottom_scroll_view_coin() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(322,3);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(322*2/3,3*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(344,0);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((344)/2,(0)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(190,0);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(190,0);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::home_bottom_scroll_view_coin_visible_height() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (27+39+27);
        }
        case ResolutionType::androidhd: {
            return ((27+39+27)*2/3);
        }
        case ResolutionType::ipadhd: {
            return (48+50+48);
        }
        case ResolutionType::ipad: {
            return (48+50+48)/2;
        }
        case ResolutionType::iphone4: {
            return (15+25+15);
        }
        case ResolutionType::iphone35: {
            return (15+25+15);
        }
    }
    return 0.0f;
}

float ResourceManager::home_bottom_scroll_view_coin_font_size_digit1() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (52);
        }
        case ResolutionType::androidhd: {
            return (52*2/3);
        }
        case ResolutionType::ipadhd: {
            return (76);
        }
        case ResolutionType::ipad: {
            return (76)/2;
        }
        case ResolutionType::iphone4: {
            return (30);
        }
        case ResolutionType::iphone35: {
            return (30);
        }
    }
    return 0.0f;
}

float ResourceManager::home_bottom_scroll_view_coin_font_size_digit2() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (40);
        }
        case ResolutionType::androidhd: {
            return (40*2/3);
        }
        case ResolutionType::ipadhd: {
            return (58);
        }
        case ResolutionType::ipad: {
            return (58)/2;
        }
        case ResolutionType::iphone4: {
            return (24);
        }
        case ResolutionType::iphone35: {
            return (24);
        }
    }
    return 0.0f;
}

float ResourceManager::home_bottom_scroll_view_coin_font_size_digit3() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (35);
        }
        case ResolutionType::androidhd: {
            return (35*2/3);
        }
        case ResolutionType::ipadhd: {
            return (50);
        }
        case ResolutionType::ipad: {
            return (50)/2;
        }
        case ResolutionType::iphone4: {
            return (21);
        }
        case ResolutionType::iphone35: {
            return (21);
        }
    }
    return 0.0f;
}

float ResourceManager::home_bottom_scroll_view_pin_top_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (23);
        }
        case ResolutionType::androidhd: {
            return (23*2/3);
        }
        case ResolutionType::ipadhd: {
            return (34);
        }
        case ResolutionType::ipad: {
            return (34)/2;
        }
        case ResolutionType::iphone4: {
            return (12);
        }
        case ResolutionType::iphone35: {
            return (12);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::home_bottom_line_start() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(16,213);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(16*2/3,213*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(0,336);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((0)/2,(336)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(0,138);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(0,138);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::home_bottom_line_left() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(426,157);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(426*2/3,157*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(582,180);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((582)/2,(180)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(262,84);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(262,84);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::home_bottom_line_mid() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(875,157);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(875*2/3,157*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(1072,180);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((1072)/2,(180)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(516,84);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(516,84);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::home_bottom_line_right() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(603,157);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(603*2/3,157*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(690,180);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((690)/2,(180)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(358,84);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(358,84);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::home_talk_box() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((61),(207+18+1-34));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((61)*2/3,(207+18+1-34)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(74,(284));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((74)/2,((284))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(34,115);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(34,115);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::home_talk_box_purchase_bottom_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (241-207);
        }
        case ResolutionType::androidhd: {
            return ((241-207)*2/3);
        }
        case ResolutionType::ipadhd: {
            return (332-284);
        }
        case ResolutionType::ipad: {
            return (332-284)/2;
        }
        case ResolutionType::iphone4: {
            return (136-115);
        }
        case ResolutionType::iphone35: {
            return (136-115);
        }
    }
    return 0.0f;
}

float ResourceManager::home_talk_box_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (39);
        }
        case ResolutionType::androidhd: {
            return (39*2/3);
        }
        case ResolutionType::ipadhd: {
            return (44);
        }
        case ResolutionType::ipad: {
            return (44)/2;
        }
        case ResolutionType::iphone4: {
            return (22);
        }
        case ResolutionType::iphone35: {
            return (22);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::home_talk_box_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(646,50);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(646*2/3,50*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(646,50);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((646)/2,(50)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(330,28);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(330,28);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::home_talk_box_label_01() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(55,(75+43+12));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(55*2/3,(75+43+12)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(60,92+50+16);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((60)/2,(92+50+16)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(30,46+28+4);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(30,46+28+4);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_talk_box_label_02() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(55,(75));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(55*2/3,(75)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(60,92);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((60)/2,(92)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(30,46);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(30,46);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::home_talk_box_max_width_short() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return ((538+10));
        }
        case ResolutionType::androidhd: {
            return ((538+10)*2/3);
        }
        case ResolutionType::ipadhd: {
            return (646);
        }
        case ResolutionType::ipad: {
            return (646)/2;
        }
        case ResolutionType::iphone4: {
            return (250);
        }
        case ResolutionType::iphone35: {
            return (250);
        }
    }
    return 0.0f;
}

float ResourceManager::home_talk_box_max_width_long() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return ((538+10));
        }
        case ResolutionType::androidhd: {
            return ((538+10)*2/3);
        }
        case ResolutionType::ipadhd: {
            return (568);
        }
        case ResolutionType::ipad: {
            return (568)/2;
        }
        case ResolutionType::iphone4: {
            return (296);
        }
        case ResolutionType::iphone35: {
            return (296);
        }
    }
    return 0.0f;
}

float ResourceManager::home_talk_box_bottom_margin() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (75);
        }
        case ResolutionType::androidhd: {
            return (75*2/3);
        }
        case ResolutionType::ipadhd: {
            return (92);
        }
        case ResolutionType::ipad: {
            return (92)/2;
        }
        case ResolutionType::iphone4: {
            return (40);
        }
        case ResolutionType::iphone35: {
            return (40);
        }
    }
    return 0.0f;
}

float ResourceManager::home_talk_box_side_margin() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (55);
        }
        case ResolutionType::androidhd: {
            return (55*2/3);
        }
        case ResolutionType::ipadhd: {
            return (60);
        }
        case ResolutionType::ipad: {
            return (60)/2;
        }
        case ResolutionType::iphone4: {
            return (30);
        }
        case ResolutionType::iphone35: {
            return (30);
        }
    }
    return 0.0f;
}

float ResourceManager::home_talk_box_top_margin() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (44);
        }
        case ResolutionType::androidhd: {
            return (44*2/3);
        }
        case ResolutionType::ipadhd: {
            return (60);
        }
        case ResolutionType::ipad: {
            return (60)/2;
        }
        case ResolutionType::iphone4: {
            return (28);
        }
        case ResolutionType::iphone35: {
            return (28);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::home_frame_sprite_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size((18+378+18),(74+74+14+93));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size((18+378+18)*2/3,(74+74+14+93)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size((14+436+14),((38-(332-304))+304));
        }
        case ResolutionType::ipad: {
            return cocos2d::Size(((14+436+14))/2,(((38-(332-304))+304))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size((10+232+10),((18-(138-125))+125));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size((10+232+10),((18-(138-125))+125));
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::home_delete_btn_type01() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((112/2-55),(112/2-52));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((112/2-55)*2/3,(112/2-52)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((136/2-66),(154/2-70));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((136/2-66))/2,((154/2-70))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((66/2-32),(66/2-30));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((66/2-32),(66/2-30));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_framebg() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(18,(26-(222-208)));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(18*2/3,(26-(222-208))*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(14,(38-(332-304)));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((14)/2,((38-(332-304)))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(10,(18-(138-125)));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(10,(18-(138-125)));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_create_btn() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(1920,(191-340));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(1920*2/3,(191-340)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((1612+436),(-484+300));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((1612+436))/2,((-484+300))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((942+194),(-204+135));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((942-176+194),(-204+135));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::home_create_btn_visible_rect() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(218,280);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(218*2/3,280*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(288,364);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((288)/2,(364)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(128,166);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(128,166);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::home_create_bg_btn() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(23,91);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(23*2/3,91*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(38,152);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((38)/2,(152)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(16,56);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(16,56);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::home_create_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (32);
                } break;
                case LanguageType::KOREAN: {
                    return (30);
                } break;
                case LanguageType::JAPANESE: {
                    return (21);
                } break;
            }
        }
        case ResolutionType::androidhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (32*2/3);
                } break;
                case LanguageType::KOREAN: {
                    return (30*2/3);
                } break;
                case LanguageType::JAPANESE: {
                    return (21*2/3);
                } break;
            }
        }
        case ResolutionType::ipadhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return ((38-4));
                } break;
                case LanguageType::KOREAN: {
                    return ((38-4));
                } break;
                case LanguageType::JAPANESE: {
                    return ((32-4));
                } break;
            }
        }
        case ResolutionType::ipad: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return ((38-4))/2;
                } break;
                case LanguageType::KOREAN: {
                    return ((38-4))/2;
                } break;
                case LanguageType::JAPANESE: {
                    return ((32-4))/2;
                } break;
            }
        }
        case ResolutionType::iphone4: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return ((18-2));
                } break;
                case LanguageType::KOREAN: {
                    return ((18-2));
                } break;
                case LanguageType::JAPANESE: {
                    return ((14-2));
                } break;
            }
        }
        case ResolutionType::iphone35: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return ((18-2));
                } break;
                case LanguageType::KOREAN: {
                    return ((18-2));
                } break;
                case LanguageType::JAPANESE: {
                    return ((14-2));
                } break;
            }
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::home_create_textsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(186,95);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(186*2/3,95*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(204,42+42+10);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((204)/2,(42+42+10)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(100,22+22+10);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(100,22+22+10);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::home_create_text() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(16,65);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(16*2/3,65*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(42,94);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((42)/2,(94)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(14,42);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(14,42);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::home_delete_btn_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (10);
        }
        case ResolutionType::androidhd: {
            return (10*2/3);
        }
        case ResolutionType::ipadhd: {
            return (10);
        }
        case ResolutionType::ipad: {
            return (10)/2;
        }
        case ResolutionType::iphone4: {
            return (6);
        }
        case ResolutionType::iphone35: {
            return (6);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::home_delete_btn_glow_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(332,380);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(332*2/3,380*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(332,380);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((332)/2,(380)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(160,176);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(160,176);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::home_background_layer_shadow() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (40);
        }
        case ResolutionType::androidhd: {
            return (40*2/3);
        }
        case ResolutionType::ipadhd: {
            return (34);
        }
        case ResolutionType::ipad: {
            return (34)/2;
        }
        case ResolutionType::iphone4: {
            return (18);
        }
        case ResolutionType::iphone35: {
            return (18);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::home_background_layer_coin() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((94+40),((1080-1017)/2+40-18));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((94+40)*2/3,((1080-1017)/2+40-18)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(240,302-240);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((240)/2,(302-240)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(100,126-100);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(100,126-100);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::home_background_layer_coin_text_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (112);
        }
        case ResolutionType::androidhd: {
            return (112*2/3);
        }
        case ResolutionType::ipadhd: {
            return (162);
        }
        case ResolutionType::ipad: {
            return (162)/2;
        }
        case ResolutionType::iphone4: {
            return (66);
        }
        case ResolutionType::iphone35: {
            return (66);
        }
    }
    return 0.0f;
}

float ResourceManager::home_custom_bg_bottom_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (78);
        }
        case ResolutionType::androidhd: {
            return (78*2/3);
        }
        case ResolutionType::ipadhd: {
            return (116);
        }
        case ResolutionType::ipad: {
            return (116)/2;
        }
        case ResolutionType::iphone4: {
            return (49);
        }
        case ResolutionType::iphone35: {
            return (49);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::home_custom_bg_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(346,194);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(346*2/3,194*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(368,276);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((368)/2,(276)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(204,115);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(204,115);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::home_background_item_bg_ani01() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(416,840);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((138*2),(279*2));
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(366,1188);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((366)/2,(1188)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(248,502);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(178,502);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_background_item_bg_ani02() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((247+1),(445+1));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((247+2)*2/3,(445+2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(60,628);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((60)/2,(628)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(144,266);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(72,266);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_background_item_bg_ani04() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(650,58);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(434,38);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(666,64);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((666)/2,(64)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(394,36);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(324,36);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_background_item_bg_ani06() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(628,712);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(628*2/3,712*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(616,1010);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((616)/2,(1010)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(378,422);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(296,422);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_background_item_bg_ani07() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(34,58);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(34*2/3,58*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(60,64);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((60)/2,(64)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(22,36);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(23,84);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_background_item_bg_ani08() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(598,216);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(598*2/3,216*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(560,326);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((560)/2,(326)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(177*2,64*2);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(282,128);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_background_item_bg_ani09() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(34,132);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(34*2/3,132*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(60,176);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((60)/2,(176)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(22,84);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(26,84);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_background_item_bg_ani10() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(256,78);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(256*2/3,78*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(90,90);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((90)/2,(90)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(146,44);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(75,44);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_background_item_bg_ani11() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(34,164);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(34*2/3,164*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(60,292);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((60)/2,(292)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(22,94);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(26,94);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_background_item_bg_ani12() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(246,416);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(246*2/3,416*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(60,634);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((60)/2,(634)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(132,266);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(60,266);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_custom_bg_big() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(17*2,29*2);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(17*2*2/3,29*2*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(28*2,32*2);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((28*2)/2,(32*2)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(10*2,17*2);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(12*2,17*2);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::home_custom_bg_book_thumb() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(76,96);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(76*2/3,96*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(100,174);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((100)/2,(174)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(44,58);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(44,58);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::home_custom_bg_width_inbook() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (335);
        }
        case ResolutionType::androidhd: {
            return (335*2/3);
        }
        case ResolutionType::ipadhd: {
            return (478);
        }
        case ResolutionType::ipad: {
            return (478)/2;
        }
        case ResolutionType::iphone4: {
            return (198);
        }
        case ResolutionType::iphone35: {
            return (198);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::home_setting_bg_image_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(226,140);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(151,94);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(300,184);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((300)/2,(184)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(140,100);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(140,100);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::home_setting_bg_capinsets() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(108,60);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(72,42);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(52,66);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((52)/2,(66)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(32,34);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(32,34);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::home_setting_bg_capinsets_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size((220-108*2),(140-60*2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size((147-72*2),(94-42*2));
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(110,70);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((110)/2,(70)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(38,38);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(38,38);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::home_setting_bg_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(374,172);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(374*2/3,172*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(426,196);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((426)/2,(196)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(228,100);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(228,100);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::home_setting_bg() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(12,(400+499));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(12*2/3,(400+499)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(30,(834+472));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((30)/2,((834+472))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(6,(294+236));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(6,(294+236));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::home_setting_list_press_image_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(61,42);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(41,28);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(138,138);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((138)/2,(138)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(68,68);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(68,68);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::home_setting_list_press_capInsets() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(24,20);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(18,13);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(32,32);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((32)/2,(32)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(12,12);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(12,12);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::home_setting_list_press_capInsets_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size((61-24-24),(42-20-20));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size((41-18-18),(28-13-13));
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size((138-32-32),(138-32-32));
        }
        case ResolutionType::ipad: {
            return cocos2d::Size(((138-32-32))/2,((138-32-32))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size((68-12-12),(68-12-12));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size((68-12-12),(68-12-12));
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::home_setting_list_press() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(18,23);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(18*2/3,23*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(22,22);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((22)/2,(22)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(14,14);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(14,14);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::home_setting_list_press_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size((338-12*2-1),(114-23/2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size((338-12*2-1)*2/3,(114-23/2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size((382-32),(138-32/2+4));
        }
        case ResolutionType::ipad: {
            return cocos2d::Size(((382-32))/2,((138-32/2+4))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size((200-12-6),(68-6));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size((200-12-6),(68-6));
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::home_setting_icon() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((18+26),(23+26));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((18+26)*2/3,(23+26)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((22+30),(22+28));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((22+30))/2,((22+28))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((14+14),(14+18-4));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((14+14),(14+18-4));
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::home_setting_label_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (48);
                } break;
                case LanguageType::KOREAN: {
                    return (44);
                } break;
                case LanguageType::JAPANESE: {
                    return (44);
                } break;
            }
        }
        case ResolutionType::androidhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (48*2/3);
                } break;
                case LanguageType::KOREAN: {
                    return (44*2/3);
                } break;
                case LanguageType::JAPANESE: {
                    return (44*2/3);
                } break;
            }
        }
        case ResolutionType::ipadhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (58);
                } break;
                case LanguageType::KOREAN: {
                    return (54);
                } break;
                case LanguageType::JAPANESE: {
                    return (54);
                } break;
            }
        }
        case ResolutionType::ipad: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (58)/2;
                } break;
                case LanguageType::KOREAN: {
                    return (54)/2;
                } break;
                case LanguageType::JAPANESE: {
                    return (54)/2;
                } break;
            }
        }
        case ResolutionType::iphone4: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (28);
                } break;
                case LanguageType::KOREAN: {
                    return (24);
                } break;
                case LanguageType::JAPANESE: {
                    return (24);
                } break;
            }
        }
        case ResolutionType::iphone35: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (28);
                } break;
                case LanguageType::KOREAN: {
                    return (24);
                } break;
                case LanguageType::JAPANESE: {
                    return (24);
                } break;
            }
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::home_setting_label() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((18+26+62+32),(23+26));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((18+26+62+32)*2/3,(23+26)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((22+30+78+34),(22+28+4));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((22+30+78+34))/2,((22+28+4))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((14+14+38+18),(14+18));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((14+14+38+18),(14+18));
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::home_dim_limit_time_fontsize_01() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (74);
        }
        case ResolutionType::androidhd: {
            return (74*2/3);
        }
        case ResolutionType::ipadhd: {
            return (110);
        }
        case ResolutionType::ipad: {
            return (110)/2;
        }
        case ResolutionType::iphone4: {
            return (46);
        }
        case ResolutionType::iphone35: {
            return (46);
        }
    }
    return 0.0f;
}

float ResourceManager::home_dim_limit_time_fontsize_02() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (82);
        }
        case ResolutionType::androidhd: {
            return (82*2/3);
        }
        case ResolutionType::ipadhd: {
            return (120);
        }
        case ResolutionType::ipad: {
            return (120)/2;
        }
        case ResolutionType::iphone4: {
            return (50);
        }
        case ResolutionType::iphone35: {
            return (50);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::home_dim_limit_time_lable_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(163,(82+28*2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(163*2/3,(82+28*2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(200,120);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((200)/2,(120)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(84,56);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(84,56);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::home_dim_limit_time_y() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (50);
        }
        case ResolutionType::androidhd: {
            return (50*2/3);
        }
        case ResolutionType::ipadhd: {
            return (72+40);
        }
        case ResolutionType::ipad: {
            return (72+40)/2;
        }
        case ResolutionType::iphone4: {
            return (30+18);
        }
        case ResolutionType::iphone35: {
            return (30+18);
        }
    }
    return 0.0f;
}

float ResourceManager::home_dim_limit_name_label_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (24);
        }
        case ResolutionType::androidhd: {
            return (24*2/3);
        }
        case ResolutionType::ipadhd: {
            return (36);
        }
        case ResolutionType::ipad: {
            return (36)/2;
        }
        case ResolutionType::iphone4: {
            return (14);
        }
        case ResolutionType::iphone35: {
            return (14);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::home_dim_limit_name_label_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(163,(28*2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(163*2/3,(28*2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(200,40);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((200)/2,(40)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(84,18);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(84,18);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::home_dim_limit_name_label_y() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return ((50-28/2));
        }
        case ResolutionType::androidhd: {
            return ((50-28/2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return (72);
        }
        case ResolutionType::ipad: {
            return (72)/2;
        }
        case ResolutionType::iphone4: {
            return (30);
        }
        case ResolutionType::iphone35: {
            return (30);
        }
    }
    return 0.0f;
}

float ResourceManager::home_dim_limit_char_talkbox_y() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (92);
        }
        case ResolutionType::androidhd: {
            return (92);
        }
        case ResolutionType::ipadhd: {
            return (92);
        }
        case ResolutionType::ipad: {
            return (92)/2;
        }
        case ResolutionType::iphone4: {
            return (70);
        }
        case ResolutionType::iphone35: {
            return (70);
        }
    }
    return 0.0f;
}

float ResourceManager::home_frame_limit_fontsize_01() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (38);
        }
        case ResolutionType::androidhd: {
            return (38*2/3);
        }
        case ResolutionType::ipadhd: {
            return (56);
        }
        case ResolutionType::ipad: {
            return (56)/2;
        }
        case ResolutionType::iphone4: {
            return (22);
        }
        case ResolutionType::iphone35: {
            return (22);
        }
    }
    return 0.0f;
}

float ResourceManager::home_frame_limit_fontsize_02() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (45);
        }
        case ResolutionType::androidhd: {
            return (45*2/3);
        }
        case ResolutionType::ipadhd: {
            return (67);
        }
        case ResolutionType::ipad: {
            return (67)/2;
        }
        case ResolutionType::iphone4: {
            return (28);
        }
        case ResolutionType::iphone35: {
            return (28);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::home_frame_limit_label_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(82,82);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(82*2/3,82*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(92,70);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((92)/2,(70)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(36,32);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(36,32);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::home_frame_limit_label() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(18,28);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(18*2/3,28*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(56,84);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((56)/2,(84)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(20,30);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(20,30);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_bg_map() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(0,0);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(0,0);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(0,0);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((0)/2,(0)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(0,0);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(0,0);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_btn_back() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(30,900);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(30*2/3,900*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(50,1310);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((50)/2,(1310)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(20,532);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(20,532);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_title_label() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(32,30+48+2);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(32*2/3,(30+48+2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(60,(43+66+5));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((60)/2,((43+66+5))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(26,(4+32+4));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(26,(4+32+4));
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::bookmap_title_label_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (46);
                } break;
                case LanguageType::KOREAN: {
                    return (42);
                } break;
                case LanguageType::JAPANESE: {
                    return (42);
                } break;
            }
        }
        case ResolutionType::androidhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (46*2/3);
                } break;
                case LanguageType::KOREAN: {
                    return (42*2/3);
                } break;
                case LanguageType::JAPANESE: {
                    return (42*2/3);
                } break;
            }
        }
        case ResolutionType::ipadhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (56);
                } break;
                case LanguageType::KOREAN: {
                    return (52);
                } break;
                case LanguageType::JAPANESE: {
                    return (52);
                } break;
            }
        }
        case ResolutionType::ipad: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (56)/2;
                } break;
                case LanguageType::KOREAN: {
                    return (52)/2;
                } break;
                case LanguageType::JAPANESE: {
                    return (52)/2;
                } break;
            }
        }
        case ResolutionType::iphone4: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (30);
                } break;
                case LanguageType::KOREAN: {
                    return (26);
                } break;
                case LanguageType::JAPANESE: {
                    return (26);
                } break;
            }
        }
        case ResolutionType::iphone35: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (30);
                } break;
                case LanguageType::KOREAN: {
                    return (26);
                } break;
                case LanguageType::JAPANESE: {
                    return (26);
                } break;
            }
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::bookmap_single_title_label_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(420,64);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(420*2/3,64*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(560,60);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((560)/2,(60)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(250,34);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(250,34);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::bookmap_double_title_label_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(720,64);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(720*2/3,64*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(760,60);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((760)/2,(60)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(370,34);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(370,34);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::bookmap_single_title_label() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(50,(160-54)/2);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(50*2/3,(160-54)/2*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(48,(184-60)/2);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((48)/2,((184-60)/2)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(35,(98-34)/2);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(35,(98-34)/2);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_double_title_label_01() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(50,(160-54-54)/2+54);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(50*2/3,((160-54-54)/2+54)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(48,(184-60-60)/2+60);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((48)/2,((184-60-60)/2+60)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(35,(98-34-34)/2+34);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(35,(98-34-34)/2+34);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_double_title_label_02() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(50,(160-54-54)/2);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(50*2/3,(160-54-54)/2*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(48,(184-60-60)/2);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((48)/2,((184-60-60)/2)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(35,(98-34-34)/2);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(35,(98-34-34)/2);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::bookmap_name_label_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (40);
                } break;
                case LanguageType::KOREAN: {
                    return (36);
                } break;
                case LanguageType::JAPANESE: {
                    return (36);
                } break;
            }
        }
        case ResolutionType::androidhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (40*2/3);
                } break;
                case LanguageType::KOREAN: {
                    return (36*2/3);
                } break;
                case LanguageType::JAPANESE: {
                    return (36*2/3);
                } break;
            }
        }
        case ResolutionType::ipadhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (46);
                } break;
                case LanguageType::KOREAN: {
                    return (42);
                } break;
                case LanguageType::JAPANESE: {
                    return (42);
                } break;
            }
        }
        case ResolutionType::ipad: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (46)/2;
                } break;
                case LanguageType::KOREAN: {
                    return (42)/2;
                } break;
                case LanguageType::JAPANESE: {
                    return (42)/2;
                } break;
            }
        }
        case ResolutionType::iphone4: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (28);
                } break;
                case LanguageType::KOREAN: {
                    return (24);
                } break;
                case LanguageType::JAPANESE: {
                    return (24);
                } break;
            }
        }
        case ResolutionType::iphone35: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (28);
                } break;
                case LanguageType::KOREAN: {
                    return (24);
                } break;
                case LanguageType::JAPANESE: {
                    return (24);
                } break;
            }
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::bookmap_single_name_label_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(206,60);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(206*2/3,60*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(236,60);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((236)/2,(60)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(174,32);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(174,32);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::bookmap_double_name_label_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(206,60);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(206*2/3,60*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(236,50);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((236)/2,(50)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(174,32);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(174,32);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::bookmap_single_name_label() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(94,(160-50)/2);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(94*2/3,(160-50)/2*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(104,(184-60)/2);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((104)/2,((184-60)/2)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(64,(98-32)/2);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(64,(98-32)/2);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_double_name_label_01() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(94,(160-50-50)/2+50);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(94*2/3,((160-50-50)/2+50)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(104,(184-60-60)/2+60);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((104)/2,((184-60-60)/2+60)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(64,(98-32-32)/2+32);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(64,(98-32-32)/2+32);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_double_name_label_02() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(94,(160-50-50)/2);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(94*2/3,(160-50-50)/2*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(104,(184-60-60)/2);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((104)/2,((184-60-60)/2)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(64,(98-32-32)/2);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(64,(98-32-32)/2);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_subtitle_label() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(32,31);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(32*2/3,31*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(60,(43));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((60)/2,((43))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(26,14);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(26,14);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::bookmap_subtitle_label_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (40);
        }
        case ResolutionType::androidhd: {
            return (40*2/3);
        }
        case ResolutionType::ipadhd: {
            return (60);
        }
        case ResolutionType::ipad: {
            return (60)/2;
        }
        case ResolutionType::iphone4: {
            return (28);
        }
        case ResolutionType::iphone35: {
            return (28);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::bookmap_subtitle_label_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(562,48);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(562*2/3,48*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(768,66);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((768)/2,(66)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(320,32);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(320,32);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::bookmap_feather01() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(476,(440+78));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(476*2/3,(440+78)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(336,736);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((336)/2,(736)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(279,306);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((279-(176/2)),306);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_feather02() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(673,258);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(673*2/3,258*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(616,366);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((616)/2,(366)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(397,152);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((397-(176/2)),152);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_feather03() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((673+138+187),(308+60));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((673+138+187)*2/3,(308+60)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((616+196+266),524);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((616+196+266))/2,(524)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((397+94+104),218);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((397+94+104-(176/2)),218);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_feather04() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((673+138+187+138+32),(308+208+52+60));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((673+138+187+138+32)*2/3,(308+208+52+60)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((616+196+266+196+46),(524+296+74));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((616+196+266+196+46))/2,((524+296+74))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((397+94+104+94),(218+145+9));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((397+94+104+94-(176/2)),(218+145+9));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_feather05() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(476+138+762,464);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((476+138+762)*2/3,464*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((336+196+1084),660);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((336+196+1084))/2,(660)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((279+94+438),275);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((279+94+438-(176/2)),275);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_menu_btn() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((30+148+1564),900);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((30+148+1564)*2/3,900*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((50+186+1576),1310);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((50+186+1576))/2,(1310)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((20+90+916),532);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((20+90+916-176),532);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_play_btn() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(1598,0);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(1598*2/3,0);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((1666-84+40),0);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((1666-84+40))/2,(0)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(938,0);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((938-176),0);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_title_bg() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((1920-626)/2,31);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((1920-626)/2*2/3,31*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(580,46);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((580)/2,(46)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(382,18);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((382-(176/2)),18);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_intro() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(591,518);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(591*2/3,518*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(498,736);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((498)/2,(736)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(355,306);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((355-(176/2)),306);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_final() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(591+146+752,464);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((591+146+752)*2/3,464*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((498+208+1072),660);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((498+208+1072))/2,(660)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((358+96+432),276);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((358+96+432-(176/2)),276);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_scene_2() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(543,258);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(543*2/3,258*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(430,366);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((430)/2,(366)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((322),152);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((322-(176/2)),152);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_scene_3() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((543+86+238),368);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((543+86+238)*2/3,368*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((430+120+340),524);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((430+120+340))/2,(524)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((322+56+146),218);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((322+56+146-(176/2)),218);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_scene_4() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((543+86+238+86+85),(368+80+180));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((543+86+238+86+85)*2/3,(368+80+180)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((430+120+340+120+124),(524+112+258));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((430+120+340+120+124))/2,((524+112+258))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((322+56+146+56+32),(218+52+102));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((322+56+146+56+32-(176/2)),(218+52+102));
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::bookmap_flag_fontsize_1() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (42);
                } break;
                case LanguageType::KOREAN: {
                    return (34);
                } break;
                case LanguageType::JAPANESE: {
                    return (18);
                } break;
            }
        }
        case ResolutionType::androidhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (42*2/3);
                } break;
                case LanguageType::KOREAN: {
                    return (34*2/3);
                } break;
                case LanguageType::JAPANESE: {
                    return (18*2/3);
                } break;
            }
        }
        case ResolutionType::ipadhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (50);
                } break;
                case LanguageType::KOREAN: {
                    return (46);
                } break;
                case LanguageType::JAPANESE: {
                    return (32);
                } break;
            }
        }
        case ResolutionType::ipad: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (50)/2;
                } break;
                case LanguageType::KOREAN: {
                    return (46)/2;
                } break;
                case LanguageType::JAPANESE: {
                    return (32)/2;
                } break;
            }
        }
        case ResolutionType::iphone4: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return ((26-2));
                } break;
                case LanguageType::KOREAN: {
                    return (24);
                } break;
                case LanguageType::JAPANESE: {
                    return (14);
                } break;
            }
        }
        case ResolutionType::iphone35: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return ((26-2));
                } break;
                case LanguageType::KOREAN: {
                    return (24);
                } break;
                case LanguageType::JAPANESE: {
                    return (14);
                } break;
            }
        }
    }
    return 0.0f;
}

float ResourceManager::bookmap_flag_fontsize_n() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (38);
        }
        case ResolutionType::androidhd: {
            return (38*2/3);
        }
        case ResolutionType::ipadhd: {
            return (50);
        }
        case ResolutionType::ipad: {
            return (50)/2;
        }
        case ResolutionType::iphone4: {
            return (26);
        }
        case ResolutionType::iphone35: {
            return (26);
        }
    }
    return 0.0f;
}

float ResourceManager::bookmap_flag_fontsize_5() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (34);
                } break;
                case LanguageType::KOREAN: {
                    return (28);
                } break;
                case LanguageType::JAPANESE: {
                    return (18);
                } break;
            }
        }
        case ResolutionType::androidhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (34*2/3);
                } break;
                case LanguageType::KOREAN: {
                    return (28*2/3);
                } break;
                case LanguageType::JAPANESE: {
                    return (18*2/3);
                } break;
            }
        }
        case ResolutionType::ipadhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return ((50-6));
                } break;
                case LanguageType::KOREAN: {
                    return (42);
                } break;
                case LanguageType::JAPANESE: {
                    return (26);
                } break;
            }
        }
        case ResolutionType::ipad: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return ((50-6))/2;
                } break;
                case LanguageType::KOREAN: {
                    return (42)/2;
                } break;
                case LanguageType::JAPANESE: {
                    return (26)/2;
                } break;
            }
        }
        case ResolutionType::iphone4: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return ((26-3-4));
                } break;
                case LanguageType::KOREAN: {
                    return (20);
                } break;
                case LanguageType::JAPANESE: {
                    return ((12-1));
                } break;
            }
        }
        case ResolutionType::iphone35: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return ((26-3-4));
                } break;
                case LanguageType::KOREAN: {
                    return (20);
                } break;
                case LanguageType::JAPANESE: {
                    return ((12-1));
                } break;
            }
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::bookmap_flag_pos_type01() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(27,65);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(27*2/3,65*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(46,106);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((46)/2,(106)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(22,48);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(22,48);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_flag_pos_type02() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(30,30);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(30*2/3,30*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(46,44);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((46)/2,(44)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(22,16);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(22,16);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::bookmap_flag_size_type01() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(102,63);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(102*2/3,63*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(130,54);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((130)/2,(54)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(56,30);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(56,30);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::bookmap_flag_size_type02() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(40,46);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(40*2/3,46*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(54,52);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((54)/2,(52)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(24,29);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(24,29);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::bookmap_character01() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(415,518);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(415*2/3,518*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(250,736);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((250)/2,(736)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(241,306);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((241-(176/2)),306);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_character02() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(613,258);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(613*2/3,258*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(530,366);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((530)/2,(366)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((360),152);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((360-(176/2)),152);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_character03() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(938,368);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(938*2/3,368*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(990,524);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((990)/2,(524)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((562),218);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((562-(176/2)),218);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_character04() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((613+176+319),(368+260));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((613+176+319)*2/3,(368+260)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((530+248+456),(524+370));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((530+248+456))/2,((524+370))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((360+114+176),(152+170+50));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((360+114+176-(176/2)),(152+170+50));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_character05() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((415+176+722),464);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((415+176+722)*2/3,464*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((250+248+1032),660);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((250+248+1032))/2,(660)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((241+114+417),276);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((241+114+417-(176/2)),276);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_delete_btn01() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(415-74,518);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((415-74)*2/3,518*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((250-104),736);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((250-104))/2,(736)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((244-50),306);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((244-50-(176/2)),306);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_delete_btn02() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(613-144,258);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((613-144)*2/3,258*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((530-204),366);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((530-204))/2,(366)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((360-88),152);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((360-88-(176/2)),152);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_delete_btn03() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(938-144,368);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((938-144)*2/3,368*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((990-204),524);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((990-204))/2,(524)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((562-88),218);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((562-88-(176/2)),218);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_delete_btn04() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((613+176+319-144),(368+260));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((613+176+319-144)*2/3,(368+260)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((530+248+456-120-104),(524+370));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((530+248+456-120-104))/2,((524+370))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((360+114+176-88),(152+170+50));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((360+114+176-88-(176/2)),(152+170+50));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_delete_btn05() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((415+176+722-74),464);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((415+176+722-74)*2/3,464*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((250+248+1032-104),660);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((250+248+1032-104))/2,(660)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((244+114+414-50),276);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((244+114+414-50-(176/2)),276);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_icon_bg01() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(415-74+171,518-47);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((415-74+171)*2/3,(518-47)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((250+242-104),(736-66));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((250+242-104))/2,((736-66))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((244-50+110),(306-26));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((244-50+110-(176/2)),(306-26));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_icon_bg02() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(613-144+74+85+82,258-47);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((613-144+74+85+82)*2/3,(258-47)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((530+248+40-150),(366-66));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((530+248+40-150))/2,((366-66))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((360-88+50+56+42),(152-26));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((360-88+50+56+42-(176/2)),(152-26));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_icon_bg03() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(938-144+74+85+82,368-47);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((938-144+74+85+82)*2/3,(368-47)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((990+248+40-150),(524-66));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((990+248+40-150))/2,((524-66))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((562-88+50+56+42),(218-26));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((562-88+50+56+42-(176/2)),(218-26));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_icon_bg04() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((613+176+319-144+74+85+82),(368+260-47));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((613+176+319-144+74+85+82)*2/3,(368+260-47)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((530+248+456+248+40-150),(524+370-66));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((530+248+456+248+40-150))/2,((524+370-66))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((360+114+176-88+50+56+42),(152+170+50-26));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((360+114+176-88+50+56+42-(176/2)),(152+170+50-26));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_icon_bg05() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((415+176+722-74+171),464-47);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((415+176+722-74+171)*2/3,(464-47)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((250+248+1032+242-104),(660-66));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((250+248+1032+242-104))/2,((660-66))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((244+114+414-50+110),(276-26));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((244+114+414-50+110-(176/2)),(276-26));
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::bookmap_author_touch_width() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return ((104/2+236+48));
        }
        case ResolutionType::androidhd: {
            return ((104/2+236+48)*2/3);
        }
        case ResolutionType::ipadhd: {
            return ((104/2+236+48));
        }
        case ResolutionType::ipad: {
            return ((104/2+236+48))/2;
        }
        case ResolutionType::iphone4: {
            return ((64/2+174+35));
        }
        case ResolutionType::iphone35: {
            return ((64/2+174+35));
        }
    }
    return 0.0f;
}

float ResourceManager::bookmap_edit_box_Y() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (323);
        }
        case ResolutionType::androidhd: {
            return (323*2/3);
        }
        case ResolutionType::ipadhd: {
            return (323);
        }
        case ResolutionType::ipad: {
            return (323)/2;
        }
        case ResolutionType::iphone4: {
            return (109);
        }
        case ResolutionType::iphone35: {
            return (109);
        }
    }
    return 0.0f;
}

float ResourceManager::home_new_book_start_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (220-14);
        }
        case ResolutionType::androidhd: {
            return ((220-14)*2/3);
        }
        case ResolutionType::ipadhd: {
            return (200-20);
        }
        case ResolutionType::ipad: {
            return (200-20)/2;
        }
        case ResolutionType::iphone4: {
            return (130-8);
        }
        case ResolutionType::iphone35: {
            return (130-8);
        }
    }
    return 0.0f;
}

float ResourceManager::bookmap_title_y() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (46);
        }
        case ResolutionType::androidhd: {
            return (46*2/3);
        }
        case ResolutionType::ipadhd: {
            return (46);
        }
        case ResolutionType::ipad: {
            return (46)/2;
        }
        case ResolutionType::iphone4: {
            return (18);
        }
        case ResolutionType::iphone35: {
            return (18);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::bookmap_menulist_bg_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(616,400);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(616*2/3,400*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(740,472);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((740)/2,(472)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(372,236);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(372,236);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::bookmap_menulist_press_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size((580-12*2*2),(114-23/2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size((580-12*2*2)*2/3,(114-23/2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size((696-32-32),(138-32/2+4));
        }
        case ResolutionType::ipad: {
            return cocos2d::Size(((696-32-32))/2,((138-32/2+4))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size((344-12-12-12/2),(68-12/2));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size((344-12-12-12/2),(68-12/2));
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::bookmap_menulist_label_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(580,114);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(580*2/3,114*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(696,138);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((696)/2,(138)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(344,68);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(344,68);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::bookmap_menulist_button_height() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (114);
        }
        case ResolutionType::androidhd: {
            return (114*2/3);
        }
        case ResolutionType::ipadhd: {
            return (138);
        }
        case ResolutionType::ipad: {
            return (138)/2;
        }
        case ResolutionType::iphone4: {
            return (68);
        }
        case ResolutionType::iphone35: {
            return (68);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::bookmap_menulist_mid_label() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((26+62+32),(26));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((26+62+32)*2/3,(26)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((30+78+34),(28));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((30+78+34))/2,((28))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((14+38+18),(18));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((14+38+18),(18));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::bookmap_menulist_mid_icon() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((26),(26));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((26)*2/3,(26)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((30),(28));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((30))/2,((28))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((14),(18));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((14),(18));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::camera_btn_back() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(8,292);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(8*2/3,292*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(50,1310);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((50)/2,(1310)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(20,532);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(20,532);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::LoginLayer_background_Size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(1080,1920);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(1080*2/3,1920*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(828,1224);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((828)/2,(1224)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(640,1136);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(640,960);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::AccountLayer_Position_full_popup_character_01() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(-143,0);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((-143)*2/3,0);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(-143,0);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((-143)/2,(0)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(-143,0);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(-143,0);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::AccountLayer_Position_full_popup_character_02() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(680,(472+210));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(680*2/3,(472+210)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(680,(472+210));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((680)/2,((472+210))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(680,(472+210));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(680,(472+210));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::AccountLayer_Position_login_logo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(((1080-590)/2),(1920-300-100));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(((1080-590)/2)*2/3,(1920-300-100)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(((828-346)/2),(1224-150-134));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((((828-346)/2))/2,((1224-150-134))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(((828-346)/2),(1224-150-134));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(((828-346)/2),(1224-150-134));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::AccountLayer_Size_btn_cancel_normal() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(198,192);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(198*2/3,192*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(80,80);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((80)/2,(80)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(80,80);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(80,80);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::AccountLayer_Position_btn_cancel_normal() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((1080-198),(1920-192));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((1080-198)*2/3,(1920-192)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(692,1085);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((692)/2,(1085)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(692,1085);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(692,1085);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::LoginLayer_Position_ControlButton_ID() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(112,(1920-100-300-118-120));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(112*2/3,(1920-100-300-118-120)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,(1224-(70+80+150+134)));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,((1224-(70+80+150+134)))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,(1224-(70+80+150+134)));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,(1224-(70+80+150+134)));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::LoginLayer_Position_ControlButton_PW() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(112,(1920-100-300-118-120-34-120));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(112*2/3,(1920-100-300-118-120-34-120)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,(1224-(70+22+70+80+150+134)));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,((1224-(70+22+70+80+150+134)))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,(1224-(70+22+70+80+150+134)));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,(1224-(70+22+70+80+150+134)));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::LoginLayer_Size_Button_AccountControl() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(856,60);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(856*2/3,60*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(508,32);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((508)/2,(32)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(508,32);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(508,32);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::LoginLayer_FontSize_AccountControl() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (46);
        }
        case ResolutionType::androidhd: {
            return (46*2/3);
        }
        case ResolutionType::ipadhd: {
            return (28);
        }
        case ResolutionType::ipad: {
            return (28)/2;
        }
        case ResolutionType::iphone4: {
            return (28);
        }
        case ResolutionType::iphone35: {
            return (28);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::LoginLayer_Position_Button_ForgotPassword() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(112,(162+50+76+50+76));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(112*2/3,(162+50+76+50+76)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,(122+32+44+32+44));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,((122+32+44+32+44))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,(122+32+44+32+44));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,(122+32+44+32+44));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::LoginLayer_Position_Button_Signup() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(112,(162+50+76));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(112*2/3,(162+50+76)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,(122+32+44));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,((122+32+44))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,(122+32+44));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,(122+32+44));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::LoginLayer_Position_Button_FacebookLogin() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(112,162);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(112*2/3,162*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,122);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,(122)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,122);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,122);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::AccountLayer_button_normal_01_Width() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (152);
        }
        case ResolutionType::androidhd: {
            return (152*2/3);
        }
        case ResolutionType::ipadhd: {
            return (98);
        }
        case ResolutionType::ipad: {
            return (98)/2;
        }
        case ResolutionType::iphone4: {
            return (98);
        }
        case ResolutionType::iphone35: {
            return (98);
        }
    }
    return 0.0f;
}

float ResourceManager::AccountLayer_button_normal_01_Height() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (125);
        }
        case ResolutionType::androidhd: {
            return (125*2/3);
        }
        case ResolutionType::ipadhd: {
            return (72);
        }
        case ResolutionType::ipad: {
            return (72)/2;
        }
        case ResolutionType::iphone4: {
            return (72);
        }
        case ResolutionType::iphone35: {
            return (72);
        }
    }
    return 0.0f;
}

float ResourceManager::AccountLayer_button_normal_01_InLeft() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (59);
        }
        case ResolutionType::androidhd: {
            return (59*2/3);
        }
        case ResolutionType::ipadhd: {
            return (34);
        }
        case ResolutionType::ipad: {
            return (34)/2;
        }
        case ResolutionType::iphone4: {
            return (34);
        }
        case ResolutionType::iphone35: {
            return (34);
        }
    }
    return 0.0f;
}

float ResourceManager::AccountLayer_button_normal_01_InTop() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (55);
        }
        case ResolutionType::androidhd: {
            return (55*2/3);
        }
        case ResolutionType::ipadhd: {
            return (35);
        }
        case ResolutionType::ipad: {
            return (35)/2;
        }
        case ResolutionType::iphone4: {
            return (35);
        }
        case ResolutionType::iphone35: {
            return (35);
        }
    }
    return 0.0f;
}

float ResourceManager::AccountLayer_button_normal_01_inWidth() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (34);
        }
        case ResolutionType::androidhd: {
            return (34*2/3);
        }
        case ResolutionType::ipadhd: {
            return (30);
        }
        case ResolutionType::ipad: {
            return (30)/2;
        }
        case ResolutionType::iphone4: {
            return (30);
        }
        case ResolutionType::iphone35: {
            return (30);
        }
    }
    return 0.0f;
}

float ResourceManager::AccountLayer_button_normal_01_inHeight() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (15);
        }
        case ResolutionType::androidhd: {
            return (15*2/3);
        }
        case ResolutionType::ipadhd: {
            return (2);
        }
        case ResolutionType::ipad: {
            return (2)/2;
        }
        case ResolutionType::iphone4: {
            return (2);
        }
        case ResolutionType::iphone35: {
            return (2);
        }
    }
    return 0.0f;
}

float ResourceManager::LoginLayer_FontSize_Button_Login() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (56);
        }
        case ResolutionType::androidhd: {
            return (56*2/3);
        }
        case ResolutionType::ipadhd: {
            return (34);
        }
        case ResolutionType::ipad: {
            return (34)/2;
        }
        case ResolutionType::iphone4: {
            return (34);
        }
        case ResolutionType::iphone35: {
            return (34);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::LoginLayer_Size_Button_Login() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(856,125);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(856*2/3,125*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(508,72);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((508)/2,(72)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(508,72);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(508,72);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::LoginLayer_Position_Button_Login() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(112,(1920-100-300-118-120-34-120-60-125));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(112*2/3,(1920-100-300-118-120-34-120-60-125)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,(1224-(72+36+70+22+70+80+150+134)));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,((1224-(72+36+70+22+70+80+150+134)))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,(1224-(72+36+70+22+70+80+150+134)));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,(1224-(72+36+70+22+70+80+150+134)));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::LoginLayer_Size_SkipButton() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(90,90);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(90*2/3,90*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(60,60);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((60)/2,(60)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(60,60);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(60,60);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::LoginLayer_Position_SkipButton() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((1080-44-90),36);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((1080-44-90)*2/3,36*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((708-(06+06)),52);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((708-(06+06)))/2,(52)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((708-(06+06)),52);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((708-(06+06)),52);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::LoginLayer_Size_SkipLabel() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(120,90);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(120*2/3,90*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(70,60);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((70)/2,(60)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(70,60);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(70,60);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::LoginLayer_Position_SkipLabel_x() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return ((1080-(44+90+20+120)));
        }
        case ResolutionType::androidhd: {
            return ((1080-(44+90+20+120))*2/3);
        }
        case ResolutionType::ipadhd: {
            return ((708-(06+06+10+70)));
        }
        case ResolutionType::ipad: {
            return ((708-(06+06+10+70)))/2;
        }
        case ResolutionType::iphone4: {
            return ((708-(06+06+10+70)));
        }
        case ResolutionType::iphone35: {
            return ((708-(06+06+10+70)));
        }
    }
    return 0.0f;
}

float ResourceManager::LoginLayer_Position_SkipLabel_y_Margin() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (36);
        }
        case ResolutionType::androidhd: {
            return (36*2/3);
        }
        case ResolutionType::ipadhd: {
            return (52);
        }
        case ResolutionType::ipad: {
            return (52)/2;
        }
        case ResolutionType::iphone4: {
            return (52);
        }
        case ResolutionType::iphone35: {
            return (52);
        }
    }
    return 0.0f;
}

float ResourceManager::LoginLayer_FontSize_SkipLabel() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (54);
        }
        case ResolutionType::androidhd: {
            return (54*2/3);
        }
        case ResolutionType::ipadhd: {
            return (32);
        }
        case ResolutionType::ipad: {
            return (32)/2;
        }
        case ResolutionType::iphone4: {
            return (32);
        }
        case ResolutionType::iphone35: {
            return (32);
        }
    }
    return 0.0f;
}

float ResourceManager::LoginLayer_FontSize_ForgotNoticeLayer() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (46);
        }
        case ResolutionType::androidhd: {
            return (46*2/3);
        }
        case ResolutionType::ipadhd: {
            return (28);
        }
        case ResolutionType::ipad: {
            return (28)/2;
        }
        case ResolutionType::iphone4: {
            return (28);
        }
        case ResolutionType::iphone35: {
            return (28);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::LoginLayer_Size_ForgotNoticeLabel() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(508,(32+32+32+32));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(508*2/3,(32+32+32+32)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(508,(32+32+32+32));
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((508)/2,((32+32+32+32))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(508,(32+32+32+32));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(508,(32+32+32+32));
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::LoginLayer_Position_ForgotNoticeLabel() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(160,(1224-(134+150+28+32+32+32+32)));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(160*2/3,(1224-(134+150+28+32+32+32+32))*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,(1224-(134+150+28+32+32+32+32)));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,((1224-(134+150+28+32+32+32+32)))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,(1224-(134+150+28+32+32+32+32)));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,(1224-(134+150+28+32+32+32+32)));
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::AccountLayer_FontSize_RadioLabel() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (34);
        }
        case ResolutionType::androidhd: {
            return (34*2/3);
        }
        case ResolutionType::ipadhd: {
            return (34);
        }
        case ResolutionType::ipad: {
            return (34)/2;
        }
        case ResolutionType::iphone4: {
            return (34);
        }
        case ResolutionType::iphone35: {
            return (34);
        }
    }
    return 0.0f;
}

float ResourceManager::TSIconEditBox_Size_Width() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (856);
        }
        case ResolutionType::androidhd: {
            return (856*2/3);
        }
        case ResolutionType::ipadhd: {
            return (510);
        }
        case ResolutionType::ipad: {
            return (510)/2;
        }
        case ResolutionType::iphone4: {
            return (510);
        }
        case ResolutionType::iphone35: {
            return (510);
        }
    }
    return 0.0f;
}

float ResourceManager::TSIconEditBox_Size_Height() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (120);
        }
        case ResolutionType::androidhd: {
            return (120*2/3);
        }
        case ResolutionType::ipadhd: {
            return (70);
        }
        case ResolutionType::ipad: {
            return (70)/2;
        }
        case ResolutionType::iphone4: {
            return (70);
        }
        case ResolutionType::iphone35: {
            return (70);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::TSIconEditBox_Position_Icon() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(34,((120-86)/2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(34*2/3,((120-86)/2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(20,9);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((20)/2,(9)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(20,9);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(20,9);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::TSIconEditBox_Size_textFiled() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size((856-34-86-32-44),54);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size((856-34-86-32-44)*2/3,54*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(392,34);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((392)/2,(34)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(392,34);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(392,34);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::TSIconEditBox_Position_textField() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((34+86+32),33);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((34+86+32)*2/3,33*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((20+52+20),18);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((20+52+20))/2,(18)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((20+52+20),18);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((20+52+20),18);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::TSIconEditBox_FontSize_textField() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (44);
        }
        case ResolutionType::androidhd: {
            return (44*2/3);
        }
        case ResolutionType::ipadhd: {
            return (30);
        }
        case ResolutionType::ipad: {
            return (30)/2;
        }
        case ResolutionType::iphone4: {
            return (30);
        }
        case ResolutionType::iphone35: {
            return (30);
        }
    }
    return 0.0f;
}

float ResourceManager::TSIconEditBox_FontSize_ErrorComment() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (44);
        }
        case ResolutionType::androidhd: {
            return (44*2/3);
        }
        case ResolutionType::ipadhd: {
            return (26);
        }
        case ResolutionType::ipad: {
            return (26)/2;
        }
        case ResolutionType::iphone4: {
            return (26);
        }
        case ResolutionType::iphone35: {
            return (26);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::TSIconEditBox_Size_ErrorComment() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size((856-44-44),60);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size((856-44-44)*2/3,60*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(458,60);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((458)/2,(60)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(458,60);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(458,60);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::TSIconEditBox_Position_InputComment1_x() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return ((20+52+20+128+12));
        }
        case ResolutionType::androidhd: {
            return ((20+52+20+128+12)*2/3);
        }
        case ResolutionType::ipadhd: {
            return ((20+52+20+128+12));
        }
        case ResolutionType::ipad: {
            return ((20+52+20+128+12))/2;
        }
        case ResolutionType::iphone4: {
            return ((20+52+20+128+12));
        }
        case ResolutionType::iphone35: {
            return ((20+52+20+128+12));
        }
    }
    return 0.0f;
}

float ResourceManager::TSIconEditBox_Position_InputComment2_x() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return ((20+52+20+128*2+12));
        }
        case ResolutionType::androidhd: {
            return ((20+52+20+128*2+12)*2/3);
        }
        case ResolutionType::ipadhd: {
            return ((20+52+20+128*2+12));
        }
        case ResolutionType::ipad: {
            return ((20+52+20+128*2+12))/2;
        }
        case ResolutionType::iphone4: {
            return ((20+52+20+128*2+12));
        }
        case ResolutionType::iphone35: {
            return ((20+52+20+128*2+12));
        }
    }
    return 0.0f;
}

float ResourceManager::TSIconEditBox_Position_InputComment_y() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (((120-40*2)/2));
        }
        case ResolutionType::androidhd: {
            return (((120-40*2)/2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return (((70-26*2)/2));
        }
        case ResolutionType::ipad: {
            return (((70-26*2)/2))/2;
        }
        case ResolutionType::iphone4: {
            return (((70-26*2)/2));
        }
        case ResolutionType::iphone35: {
            return (((70-26*2)/2));
        }
    }
    return 0.0f;
}

float ResourceManager::TSIconEditBox_Size_InputComment_Label_WidthDelta() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (126);
        }
        case ResolutionType::androidhd: {
            return (126*2/3);
        }
        case ResolutionType::ipadhd: {
            return (126);
        }
        case ResolutionType::ipad: {
            return (126)/2;
        }
        case ResolutionType::iphone4: {
            return (126);
        }
        case ResolutionType::iphone35: {
            return (126);
        }
    }
    return 0.0f;
}

float ResourceManager::TSIconEditBox_Size_InputComment_Label_Height() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (52);
        }
        case ResolutionType::androidhd: {
            return (52*2/3);
        }
        case ResolutionType::ipadhd: {
            return (52);
        }
        case ResolutionType::ipad: {
            return (52)/2;
        }
        case ResolutionType::iphone4: {
            return (52);
        }
        case ResolutionType::iphone35: {
            return (52);
        }
    }
    return 0.0f;
}

float ResourceManager::TSIconEditBox_FontSize_InputComment() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (22);
        }
        case ResolutionType::androidhd: {
            return (22*2/3);
        }
        case ResolutionType::ipadhd: {
            return (22);
        }
        case ResolutionType::ipad: {
            return (22)/2;
        }
        case ResolutionType::iphone4: {
            return (22);
        }
        case ResolutionType::iphone35: {
            return (22);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::SignupLayer_Position_IdEditBox() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(160,(106+72+134+56+46+60+32+70+28+70+28+70+28+70+28));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(160*2/3,(106+72+134+56+46+60+32+70+28+70+28+70+28+70+28)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,(106+72+134+56+46+60+32+70+28+70+28+70+28+70+28));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,((106+72+134+56+46+60+32+70+28+70+28+70+28+70+28))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,(106+72+134+56+46+60+32+70+28+70+28+70+28+70+28));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,(106+72+134+56+46+60+32+70+28+70+28+70+28+70+28));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::SignupLayer_Position_PWEditBox() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(160,(106+72+134+56+46+60+32+70+28+70+28+70+28));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(160*2/3,(106+72+134+56+46+60+32+70+28+70+28+70+28)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,(106+72+134+56+46+60+32+70+28+70+28+70+28));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,((106+72+134+56+46+60+32+70+28+70+28+70+28))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,(106+72+134+56+46+60+32+70+28+70+28+70+28));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,(106+72+134+56+46+60+32+70+28+70+28+70+28));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::SignupLayer_Position_CPWEditBox() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(160,(106+72+134+56+46+60+32+70+28+70+28));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(160*2/3,(106+72+134+56+46+60+32+70+28+70+28)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,(106+72+134+56+46+60+32+70+28+70+28));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,((106+72+134+56+46+60+32+70+28+70+28))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,(106+72+134+56+46+60+32+70+28+70+28));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,(106+72+134+56+46+60+32+70+28+70+28));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::SignupLayer_Position_BirthEditBox() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(160,(106+72+134+56+46+60+32+70+28));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(160*2/3,(106+72+134+56+46+60+32+70+28)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,(106+72+134+56+46+60+32+70+28));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,((106+72+134+56+46+60+32+70+28))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,(106+72+134+56+46+60+32+70+28));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,(106+72+134+56+46+60+32+70+28));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::SignupLayer_Position_CityEidtBox() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(160,(106+72+134+56+46+60+32));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(160*2/3,(106+72+134+56+46+60+32)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,(106+72+134+56+46+60+32));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,((106+72+134+56+46+60+32))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,(106+72+134+56+46+60+32));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,(106+72+134+56+46+60+32));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::SignupLayer_Position_MailRadio() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(160,(106+72+134+56+46));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(160*2/3,(106+72+134+56+46)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,(106+72+134+56+46));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,((106+72+134+56+46))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,(106+72+134+56+46));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,(106+72+134+56+46));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::SignupLayer_Position_FemailRadio() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((160+(508/2)),(106+72+134+56+46));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((160+(508/2))*2/3,(106+72+134+56+46)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((160+(508/2)),(106+72+134+56+46));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((160+(508/2)))/2,((106+72+134+56+46))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((160+(508/2)),(106+72+134+56+46));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((160+(508/2)),(106+72+134+56+46));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::SignupLayer_Position_TermsCheckBox() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(160,(106+72+134));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(160*2/3,(106+72+134)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,(106+72+134));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,((106+72+134))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,(106+72+134));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,(106+72+134));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::SignupLayer_Position_AgreementTextLabel() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((160+54+18),(106+72+132));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((160+54+18)*2/3,(106+72+132)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((160+54+18),(106+72+132));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((160+54+18))/2,((106+72+132))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((160+54+18),(106+72+132));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((160+54+18),(106+72+132));
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::SignupLayer_FontSize_AgreementTextLabel() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (26);
        }
        case ResolutionType::androidhd: {
            return (26*2/3);
        }
        case ResolutionType::ipadhd: {
            return (26);
        }
        case ResolutionType::ipad: {
            return (26)/2;
        }
        case ResolutionType::iphone4: {
            return (26);
        }
        case ResolutionType::iphone35: {
            return (26);
        }
    }
    return 0.0f;
}

float ResourceManager::SignupLayer_FontSize_Button_Login() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (34);
        }
        case ResolutionType::androidhd: {
            return (34*2/3);
        }
        case ResolutionType::ipadhd: {
            return (34);
        }
        case ResolutionType::ipad: {
            return (34)/2;
        }
        case ResolutionType::iphone4: {
            return (34);
        }
        case ResolutionType::iphone35: {
            return (34);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::SignupLayer_Size_SignupButton() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(508,72);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(508*2/3,72*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(508,72);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((508)/2,(72)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(508,72);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(508,72);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::SignupLayer_Position_SignupButton() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(160,106);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(160*2/3,106*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,106);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,(106)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,106);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,106);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::SignTermsLayer_Position_TermTextTop() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(160,(1224-(102+20)));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(160*2/3,(1224-(102+20))*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,(1224-(102+20)));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,((1224-(102+20)))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,(1224-(102+20)));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,(1224-(102+20)));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::SignTermsLayer_Position_TermTextBottom() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(160,(1224-(102+20+318+20)));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(160*2/3,(1224-(102+20+318+20))*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,(1224-(102+20+318+20)));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,((1224-(102+20+318+20)))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,(1224-(102+20+318+20)));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,(1224-(102+20+318+20)));
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::SignTermsLayer_Delta_TermTextMid_Width() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (510);
        }
        case ResolutionType::androidhd: {
            return (510*2/3);
        }
        case ResolutionType::ipadhd: {
            return (510);
        }
        case ResolutionType::ipad: {
            return (510)/2;
        }
        case ResolutionType::iphone4: {
            return (510);
        }
        case ResolutionType::iphone35: {
            return (510);
        }
    }
    return 0.0f;
}

float ResourceManager::SignTermsLayer_Delta_TermTextMid_Height() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (358);
        }
        case ResolutionType::androidhd: {
            return (358*2/3);
        }
        case ResolutionType::ipadhd: {
            return (358);
        }
        case ResolutionType::ipad: {
            return (358)/2;
        }
        case ResolutionType::iphone4: {
            return (358);
        }
        case ResolutionType::iphone35: {
            return (358);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::SignTermsLayer_Position_TermTextMid() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(160,(1224-(102+20+318)));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(160*2/3,(1224-(102+20+318))*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,(1224-(102+20+318)));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,((1224-(102+20+318)))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,(1224-(102+20+318)));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,(1224-(102+20+318)));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::SignTermsLayer_Position_TermText_Title() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((160+20),(1224-(106+22+34)));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((160+20)*2/3,(1224-(106+22+34))*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((160+20),(1224-(106+22+34)));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((160+20))/2,((1224-(106+22+34)))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((160+20),(1224-(106+22+34)));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((160+20),(1224-(106+22+34)));
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::SignTermsLayer_FontSize_TermText_Title() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (28);
        }
        case ResolutionType::androidhd: {
            return (28*2/3);
        }
        case ResolutionType::ipadhd: {
            return (28);
        }
        case ResolutionType::ipad: {
            return (28)/2;
        }
        case ResolutionType::iphone4: {
            return (28);
        }
        case ResolutionType::iphone35: {
            return (28);
        }
    }
    return 0.0f;
}

float ResourceManager::SignTermsLayer_FontSize_TermText_Text_Label() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (24);
        }
        case ResolutionType::androidhd: {
            return (24*2/3);
        }
        case ResolutionType::ipadhd: {
            return (24);
        }
        case ResolutionType::ipad: {
            return (24)/2;
        }
        case ResolutionType::iphone4: {
            return (24);
        }
        case ResolutionType::iphone35: {
            return (24);
        }
    }
    return 0.0f;
}

float ResourceManager::SignTermsLayer_Size_TermText_Text_Label_Width() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (460);
        }
        case ResolutionType::androidhd: {
            return (460*2/3);
        }
        case ResolutionType::ipadhd: {
            return (460);
        }
        case ResolutionType::ipad: {
            return (460)/2;
        }
        case ResolutionType::iphone4: {
            return (460);
        }
        case ResolutionType::iphone35: {
            return (460);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::SignTermsLayer_Size_TermText_ScrollView() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(470,(385-(22+34+21+2+18)));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(470*2/3,(385-(22+34+21+2+18))*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(470,(385-(22+34+21+2+18)));
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((470)/2,((385-(22+34+21+2+18)))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(470,(385-(22+34+21+2+18)));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(470,(385-(22+34+21+2+18)));
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::SignTermsLayer_Position_TermText_ScrollView() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((160+20),(1224-(102+358)));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((160+20)*2/3,(1224-(102+358))*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((160+20),(1224-(102+358)));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((160+20))/2,((1224-(102+358)))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((160+20),(1224-(102+358)));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((160+20),(1224-(102+358)));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::SignTermsLayer_Position_PolicyTextTop() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(160,(106+72+36+56+18+358-20));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(160*2/3,(106+72+36+56+18+358-20)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,(106+72+36+56+18+358-20));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,((106+72+36+56+18+358-20))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,(106+72+36+56+18+358-20));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,(106+72+36+56+18+358-20));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::SignTermsLayer_Position_PolicyTextBottom() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(160,(106+72+36+56+18));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(160*2/3,(106+72+36+56+18)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,(106+72+36+56+18));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,((106+72+36+56+18))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,(106+72+36+56+18));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,(106+72+36+56+18));
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::SignTermsLayer_Delta_PolicyTextMid_Width() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (510);
        }
        case ResolutionType::androidhd: {
            return (510*2/3);
        }
        case ResolutionType::ipadhd: {
            return (510);
        }
        case ResolutionType::ipad: {
            return (510)/2;
        }
        case ResolutionType::iphone4: {
            return (510);
        }
        case ResolutionType::iphone35: {
            return (510);
        }
    }
    return 0.0f;
}

float ResourceManager::SignTermsLayer_Delta_PolicyTextMid_Height() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (358);
        }
        case ResolutionType::androidhd: {
            return (358*2/3);
        }
        case ResolutionType::ipadhd: {
            return (358);
        }
        case ResolutionType::ipad: {
            return (358)/2;
        }
        case ResolutionType::iphone4: {
            return (358);
        }
        case ResolutionType::iphone35: {
            return (358);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::SignTermsLayer_Position_PolicyTextMid() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(160,(106+72+36+56+18+20));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(160*2/3,(106+72+36+56+18+20)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,(106+72+36+56+18+20));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,((106+72+36+56+18+20))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,(106+72+36+56+18+20));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,(106+72+36+56+18+20));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::SignTermsLayer_Position_PolicyText_Title() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(160+20,(106+72+36+56+18+358-(22+34)));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((160+20)*2/3,(106+72+36+56+18+358-(22+34))*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160+20,(106+72+36+56+18+358-(22+34)));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160+20)/2,((106+72+36+56+18+358-(22+34)))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160+20,(106+72+36+56+18+358-(22+34)));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160+20,(106+72+36+56+18+358-(22+34)));
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::SignTermsLayer_FontSize_PolicyText_Title() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (28);
        }
        case ResolutionType::androidhd: {
            return (28*2/3);
        }
        case ResolutionType::ipadhd: {
            return (28);
        }
        case ResolutionType::ipad: {
            return (28)/2;
        }
        case ResolutionType::iphone4: {
            return (28);
        }
        case ResolutionType::iphone35: {
            return (28);
        }
    }
    return 0.0f;
}

float ResourceManager::SignTermsLayer_FontSize_PolicyText_Text_Label() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (24);
        }
        case ResolutionType::androidhd: {
            return (24*2/3);
        }
        case ResolutionType::ipadhd: {
            return (24);
        }
        case ResolutionType::ipad: {
            return (24)/2;
        }
        case ResolutionType::iphone4: {
            return (24);
        }
        case ResolutionType::iphone35: {
            return (24);
        }
    }
    return 0.0f;
}

float ResourceManager::SignTermsLayer_Size_PolicyText_Text_Label_Width() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (460);
        }
        case ResolutionType::androidhd: {
            return (460*2/3);
        }
        case ResolutionType::ipadhd: {
            return (460);
        }
        case ResolutionType::ipad: {
            return (460)/2;
        }
        case ResolutionType::iphone4: {
            return (460);
        }
        case ResolutionType::iphone35: {
            return (460);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::SignTermsLayer_Size_PolicyText_ScrollView() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(470,(385-(22+34+21+2+18)));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(470*2/3,(385-(22+34+21+2+18))*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(470,(385-(22+34+21+2+18)));
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((470)/2,((385-(22+34+21+2+18)))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(470,(385-(22+34+21+2+18)));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(470,(385-(22+34+21+2+18)));
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::SignTermsLayer_Position_PolicyText_ScrollView() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((160+20),(106+72+36+56+18));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((160+20)*2/3,(106+72+36+56+18)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((160+20),(106+72+36+56+18));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((160+20))/2,((106+72+36+56+18))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((160+20),(106+72+36+56+18));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((160+20),(106+72+36+56+18));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::SignTermsLayer_Position_PCheckBox() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(160,(1224-(56+18+358+102)));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(160*2/3,(1224-(56+18+358+102))*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,(1224-(56+18+358+102)));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,((1224-(56+18+358+102)))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,(1224-(56+18+358+102)));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,(1224-(56+18+358+102)));
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::SignTermsLayer_Position_TermsCheckBox_Label_x() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return ((160+54+18));
        }
        case ResolutionType::androidhd: {
            return ((160+54+18)*2/3);
        }
        case ResolutionType::ipadhd: {
            return ((160+54+18));
        }
        case ResolutionType::ipad: {
            return ((160+54+18))/2;
        }
        case ResolutionType::iphone4: {
            return ((160+54+18));
        }
        case ResolutionType::iphone35: {
            return ((160+54+18));
        }
    }
    return 0.0f;
}

float ResourceManager::SignTermsLayer_Position_TermsCheckBox_Label_y() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return ((1224-(56+18+358+102)));
        }
        case ResolutionType::androidhd: {
            return ((1224-(56+18+358+102))*2/3);
        }
        case ResolutionType::ipadhd: {
            return ((1224-(56+18+358+102)));
        }
        case ResolutionType::ipad: {
            return ((1224-(56+18+358+102)))/2;
        }
        case ResolutionType::iphone4: {
            return ((1224-(56+18+358+102)));
        }
        case ResolutionType::iphone35: {
            return ((1224-(56+18+358+102)));
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::SignTermsLayer_Position_PrivacyCheckBox() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(160,(106+72+36));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(160*2/3,(106+72+36)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,(106+72+36));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,((106+72+36))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,(106+72+36));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,(106+72+36));
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::SignTermsLayer_Position_PrivacyCheckBox_Label_x() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return ((160+54+18));
        }
        case ResolutionType::androidhd: {
            return ((160+54+18)*2/3);
        }
        case ResolutionType::ipadhd: {
            return ((160+54+18));
        }
        case ResolutionType::ipad: {
            return ((160+54+18))/2;
        }
        case ResolutionType::iphone4: {
            return ((160+54+18));
        }
        case ResolutionType::iphone35: {
            return ((160+54+18));
        }
    }
    return 0.0f;
}

float ResourceManager::SignTermsLayer_Position_PrivacyCheckBox_Label_y() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return ((106+72+36));
        }
        case ResolutionType::androidhd: {
            return ((106+72+36)*2/3);
        }
        case ResolutionType::ipadhd: {
            return ((106+72+36));
        }
        case ResolutionType::ipad: {
            return ((106+72+36))/2;
        }
        case ResolutionType::iphone4: {
            return ((106+72+36));
        }
        case ResolutionType::iphone35: {
            return ((106+72+36));
        }
    }
    return 0.0f;
}

float ResourceManager::SignTermsLayer_FontSize_Cancel_Button() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (34);
        }
        case ResolutionType::androidhd: {
            return (34*2/3);
        }
        case ResolutionType::ipadhd: {
            return (34);
        }
        case ResolutionType::ipad: {
            return (34)/2;
        }
        case ResolutionType::iphone4: {
            return (34);
        }
        case ResolutionType::iphone35: {
            return (34);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::SignTermsLayer_Size_Cancel_Button() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(244,72);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(244*2/3,72*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(244,72);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((244)/2,(72)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(244,72);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(244,72);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::SignTermsLayer_Position_Cancel_Button() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(160,106);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(160*2/3,106*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(160,106);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((160)/2,(106)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(160,106);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(160,106);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::SignTermsLayer_FontSize_Next_Button() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (34);
        }
        case ResolutionType::androidhd: {
            return (34*2/3);
        }
        case ResolutionType::ipadhd: {
            return (34);
        }
        case ResolutionType::ipad: {
            return (34)/2;
        }
        case ResolutionType::iphone4: {
            return (34);
        }
        case ResolutionType::iphone35: {
            return (34);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::SignTermsLayer_Size_Next_Button() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(244,72);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(244*2/3,72*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(244,72);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((244)/2,(72)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(244,72);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(244,72);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::SignTermsLayer_Position_Next_Button() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((160+244+20),106);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((160+244+20)*2/3,106*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((160+244+20),106);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((160+244+20))/2,(106)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((160+244+20),106);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((160+244+20),106);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::popup_bottombg_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(55,150);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(55*2/3,150*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(100,90);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((100)/2,(90)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(100,90);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(100,90);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::popup_bottombg_corner_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(18,50);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(18*2/3,50*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(32,34);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((32)/2,(34)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(32,34);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(32,34);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::popup_top_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (49);
        }
        case ResolutionType::androidhd: {
            return (49*2/3);
        }
        case ResolutionType::ipadhd: {
            return (28);
        }
        case ResolutionType::ipad: {
            return (28)/2;
        }
        case ResolutionType::iphone4: {
            return (28);
        }
        case ResolutionType::iphone35: {
            return (28);
        }
    }
    return 0.0f;
}

float ResourceManager::popup_middle_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (28);
        }
        case ResolutionType::androidhd: {
            return (28*2/3);
        }
        case ResolutionType::ipadhd: {
            return (16);
        }
        case ResolutionType::ipad: {
            return (16)/2;
        }
        case ResolutionType::iphone4: {
            return (16);
        }
        case ResolutionType::iphone35: {
            return (16);
        }
    }
    return 0.0f;
}

float ResourceManager::popup_bottom_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (43);
        }
        case ResolutionType::androidhd: {
            return (43*2/3);
        }
        case ResolutionType::ipadhd: {
            return (24);
        }
        case ResolutionType::ipad: {
            return (24)/2;
        }
        case ResolutionType::iphone4: {
            return (24);
        }
        case ResolutionType::iphone35: {
            return (24);
        }
    }
    return 0.0f;
}

float ResourceManager::popup_one_button_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (888);
        }
        case ResolutionType::androidhd: {
            return (888*2/3);
        }
        case ResolutionType::ipadhd: {
            return (524);
        }
        case ResolutionType::ipad: {
            return (524)/2;
        }
        case ResolutionType::iphone4: {
            return (524);
        }
        case ResolutionType::iphone35: {
            return (524);
        }
    }
    return 0.0f;
}

float ResourceManager::popup_two_button_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (437);
        }
        case ResolutionType::androidhd: {
            return (437*2/3);
        }
        case ResolutionType::ipadhd: {
            return (258);
        }
        case ResolutionType::ipad: {
            return (258)/2;
        }
        case ResolutionType::iphone4: {
            return (258);
        }
        case ResolutionType::iphone35: {
            return (258);
        }
    }
    return 0.0f;
}

float ResourceManager::popup_button_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (14);
        }
        case ResolutionType::androidhd: {
            return (14*2/3);
        }
        case ResolutionType::ipadhd: {
            return (8);
        }
        case ResolutionType::ipad: {
            return (8)/2;
        }
        case ResolutionType::iphone4: {
            return (8);
        }
        case ResolutionType::iphone35: {
            return (8);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::popup_button_image_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(152,120);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(152*2/3,120*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(100,70);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((100)/2,(70)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(100,70);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(100,70);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::popup_button_corner_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(10,10);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(10*2/3,10*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(10,10);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((10)/2,(10)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(10,10);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(10,10);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::popup_messege_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (57);
        }
        case ResolutionType::androidhd: {
            return (57*2/3);
        }
        case ResolutionType::ipadhd: {
            return (34);
        }
        case ResolutionType::ipad: {
            return (34)/2;
        }
        case ResolutionType::iphone4: {
            return (34);
        }
        case ResolutionType::iphone35: {
            return (34);
        }
    }
    return 0.0f;
}

float ResourceManager::popup_button_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (57);
                } break;
                case LanguageType::KOREAN: {
                    return (54);
                } break;
                case LanguageType::JAPANESE: {
                    return (54);
                } break;
            }
        }
        case ResolutionType::androidhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (57*2/3);
                } break;
                case LanguageType::KOREAN: {
                    return (54*2/3);
                } break;
                case LanguageType::JAPANESE: {
                    return (54*2/3);
                } break;
            }
        }
        case ResolutionType::ipadhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (34);
                } break;
                case LanguageType::KOREAN: {
                    return (34);
                } break;
                case LanguageType::JAPANESE: {
                    return (34);
                } break;
            }
        }
        case ResolutionType::ipad: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (34)/2;
                } break;
                case LanguageType::KOREAN: {
                    return (34)/2;
                } break;
                case LanguageType::JAPANESE: {
                    return (34)/2;
                } break;
            }
        }
        case ResolutionType::iphone4: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (34);
                } break;
                case LanguageType::KOREAN: {
                    return (34);
                } break;
                case LanguageType::JAPANESE: {
                    return (34);
                } break;
            }
        }
        case ResolutionType::iphone35: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (34);
                } break;
                case LanguageType::KOREAN: {
                    return (34);
                } break;
                case LanguageType::JAPANESE: {
                    return (34);
                } break;
            }
        }
    }
    return 0.0f;
}

float ResourceManager::popup_button_twoline_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (42);
                } break;
                case LanguageType::KOREAN: {
                    return (40);
                } break;
                case LanguageType::JAPANESE: {
                    return (40);
                } break;
            }
        }
        case ResolutionType::androidhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (44*2/3);
                } break;
                case LanguageType::KOREAN: {
                    return (44*2/3);
                } break;
                case LanguageType::JAPANESE: {
                    return (44*2/3);
                } break;
            }
        }
        case ResolutionType::ipadhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (24);
                } break;
                case LanguageType::KOREAN: {
                    return (24);
                } break;
                case LanguageType::JAPANESE: {
                    return (24);
                } break;
            }
        }
        case ResolutionType::ipad: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (24)/2;
                } break;
                case LanguageType::KOREAN: {
                    return (24)/2;
                } break;
                case LanguageType::JAPANESE: {
                    return (24)/2;
                } break;
            }
        }
        case ResolutionType::iphone4: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (24);
                } break;
                case LanguageType::KOREAN: {
                    return (24);
                } break;
                case LanguageType::JAPANESE: {
                    return (24);
                } break;
            }
        }
        case ResolutionType::iphone35: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (24);
                } break;
                case LanguageType::KOREAN: {
                    return (24);
                } break;
                case LanguageType::JAPANESE: {
                    return (24);
                } break;
            }
        }
    }
    return 0.0f;
}

float ResourceManager::popup_button_twoline_label_width() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return ((437-(48*2)));
        }
        case ResolutionType::androidhd: {
            return ((437-(48*2))*2/3);
        }
        case ResolutionType::ipadhd: {
            return (258-(28*2));
        }
        case ResolutionType::ipad: {
            return (258-(28*2))/2;
        }
        case ResolutionType::iphone4: {
            return (258-(28*2));
        }
        case ResolutionType::iphone35: {
            return (258-(28*2));
        }
    }
    return 0.0f;
}

float ResourceManager::popup_button_2line_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (44);
        }
        case ResolutionType::androidhd: {
            return (44*2/3);
        }
        case ResolutionType::ipadhd: {
            return (24);
        }
        case ResolutionType::ipad: {
            return (24)/2;
        }
        case ResolutionType::iphone4: {
            return (24);
        }
        case ResolutionType::iphone35: {
            return (24);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::popup_list_topbg_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(152,142);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(152*2/3,142*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(100,84);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((100)/2,(84)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(100,84);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(100,84);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::popup_list_topbg_corner_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(32,34);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(32*2/3,34*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(32,34);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((32)/2,(34)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(32,34);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(32,34);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::popup_width() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (918);
        }
        case ResolutionType::androidhd: {
            return (918*2/3);
        }
        case ResolutionType::ipadhd: {
            return (544);
        }
        case ResolutionType::ipad: {
            return (544)/2;
        }
        case ResolutionType::iphone4: {
            return (544);
        }
        case ResolutionType::iphone35: {
            return (544);
        }
    }
    return 0.0f;
}

float ResourceManager::popup_title_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (57);
        }
        case ResolutionType::androidhd: {
            return (57*2/3);
        }
        case ResolutionType::ipadhd: {
            return (37);
        }
        case ResolutionType::ipad: {
            return (37)/2;
        }
        case ResolutionType::iphone4: {
            return (37);
        }
        case ResolutionType::iphone35: {
            return (37);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::popup_title_label_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(826,88);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(826*2/3,88*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(492,42);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((492)/2,(42)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(492,42);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(492,42);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::popup_shared_cell_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(918,154);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(918*2/3,154*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(492,92);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((492)/2,(92)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(492,92);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(492,92);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::popup_title() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(46,(148-88)/2);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(46*2/3,(148-88)/2*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(26,(84-42)/2);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((26)/2,((84-42)/2)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(26,(84-42)/2);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(26,(84-42)/2);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::popup_shared_tableview_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(918,(154*2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(918*2/3,(154*2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(492,92+92);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((492)/2,(92+92)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(492,92+92);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(492,92+92);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::popup_shared_middle_height() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return ((154*2));
        }
        case ResolutionType::androidhd: {
            return ((154*2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return (92+92);
        }
        case ResolutionType::ipad: {
            return (92+92)/2;
        }
        case ResolutionType::iphone4: {
            return (92+92);
        }
        case ResolutionType::iphone35: {
            return (92+92);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::popup_shared_icon() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(46,13);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(46*2/3,13*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(26,8);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((26)/2,(8)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(26,8);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(26,8);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::popup_shared_icon_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(128,128);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(128*2/3,128*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(76,76);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((76)/2,(76)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(76,76);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(76,76);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::popup_shared_cell_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (57);
        }
        case ResolutionType::androidhd: {
            return (57*2/3);
        }
        case ResolutionType::ipadhd: {
            return (34);
        }
        case ResolutionType::ipad: {
            return (34)/2;
        }
        case ResolutionType::iphone4: {
            return (34);
        }
        case ResolutionType::iphone35: {
            return (34);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::popup_shared_cell_label_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(570,94);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(570*2/3,94*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(394,62);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((394)/2,(62)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(394,62);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(394,62);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::popup_shared_cell_label() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((46+128+36),(154-94)/2);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((46+128+36)*2/3,(154-94)/2*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(26+76+22,(76+8+8-62)/2);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((26+76+22)/2,((76+8+8-62)/2)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(26+76+22,(76+8+8-62)/2);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(26+76+22,(76+8+8-62)/2);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::popup_year_background_height() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (90+90+90+3+3);
        }
        case ResolutionType::androidhd: {
            return ((90+90+90+3+3)*2/3);
        }
        case ResolutionType::ipadhd: {
            return (90+90+90+3+3);
        }
        case ResolutionType::ipad: {
            return (90+90+90+3+3)/2;
        }
        case ResolutionType::iphone4: {
            return (90+90+90+3+3);
        }
        case ResolutionType::iphone35: {
            return (90+90+90+3+3);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::popup_year_cell_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(492,90);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(492*2/3,90*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(492,90);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((492)/2,(90)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(492,90);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(492,90);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::popup_year_cell_label_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(464,38);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(464*2/3,38*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(464,38);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((464)/2,(38)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(464,38);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(464,38);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::popup_year_cell_label() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(40,26);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(40*2/3,26*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(40,26);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((40)/2,(26)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(40,26);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(40,26);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::popup_year_picker_line_01() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(40,90+90);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(40*2/3,(90+90)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(40,90+90);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((40)/2,(90+90)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(40,90+90);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(40,90+90);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::popup_year_picker_line_02() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(40,90+90+3+90);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(40*2/3,(90+90+3+90)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(40,90+90+3+90);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((40)/2,(90+90+3+90)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(40,90+90+3+90);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(40,90+90+3+90);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::popup_year_picker_line_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(464,3);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(464*2/3,3*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(464,3);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((464)/2,(3)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(464,3);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(464,3);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::recording_bg_frame() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((1920-1688)/2,53);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((1920-1688)/2*2/3,53*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(100,80);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((100)/2,(80)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(65,30);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(51,30);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::recording_item_right_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (30);
        }
        case ResolutionType::androidhd: {
            return (30*2/3);
        }
        case ResolutionType::ipadhd: {
            return (50);
        }
        case ResolutionType::ipad: {
            return (50)/2;
        }
        case ResolutionType::iphone4: {
            return (20);
        }
        case ResolutionType::iphone35: {
            return (20);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_item_bottom_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (131-188);
        }
        case ResolutionType::androidhd: {
            return ((131-188)*2/3);
        }
        case ResolutionType::ipadhd: {
            return (146-204);
        }
        case ResolutionType::ipad: {
            return (146-204)/2;
        }
        case ResolutionType::iphone4: {
            return (80-114);
        }
        case ResolutionType::iphone35: {
            return (80-114);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_item_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (40);
        }
        case ResolutionType::androidhd: {
            return (40*2/3);
        }
        case ResolutionType::ipadhd: {
            return (40);
        }
        case ResolutionType::ipad: {
            return (40)/2;
        }
        case ResolutionType::iphone4: {
            return (24);
        }
        case ResolutionType::iphone35: {
            return (24);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_bottom_scroll_view_frame_start_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (61);
        }
        case ResolutionType::androidhd: {
            return (61*2/3);
        }
        case ResolutionType::ipadhd: {
            return (88);
        }
        case ResolutionType::ipad: {
            return (88)/2;
        }
        case ResolutionType::iphone4: {
            return (36);
        }
        case ResolutionType::iphone35: {
            return (36);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_bottom_scroll_view_frame_bottom_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (42);
        }
        case ResolutionType::androidhd: {
            return (42*2/3);
        }
        case ResolutionType::ipadhd: {
            return (50);
        }
        case ResolutionType::ipad: {
            return (50)/2;
        }
        case ResolutionType::iphone4: {
            return (25);
        }
        case ResolutionType::iphone35: {
            return (25);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_bottom_scroll_view_character_item_start_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (61);
        }
        case ResolutionType::androidhd: {
            return (61*2/3);
        }
        case ResolutionType::ipadhd: {
            return (90);
        }
        case ResolutionType::ipad: {
            return (90)/2;
        }
        case ResolutionType::iphone4: {
            return (36);
        }
        case ResolutionType::iphone35: {
            return (36);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::recording_bottom_scroll_view_character_item_holder_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(292,324);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(292*2/3,324*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(358,408);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((358)/2,(408)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(172,188);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(172,188);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::recording_bottom_scroll_view_character_item_bottom_padding1() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (286-324);
        }
        case ResolutionType::androidhd: {
            return ((286-324)*2/3);
        }
        case ResolutionType::ipadhd: {
            return (0);
        }
        case ResolutionType::ipad: {
            return (0)/2;
        }
        case ResolutionType::iphone4: {
            return (172-188);
        }
        case ResolutionType::iphone35: {
            return (172-188);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_bottom_scroll_view_character_item_bottom_padding2() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (309-324);
        }
        case ResolutionType::androidhd: {
            return ((309-324)*2/3);
        }
        case ResolutionType::ipadhd: {
            return (26);
        }
        case ResolutionType::ipad: {
            return (26)/2;
        }
        case ResolutionType::iphone4: {
            return (186-188);
        }
        case ResolutionType::iphone35: {
            return (186-188);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_bottom_scroll_view_character_item_bottom_padding3() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (1);
        }
        case ResolutionType::androidhd: {
            return (1*2/3);
        }
        case ResolutionType::ipadhd: {
            return (56);
        }
        case ResolutionType::ipad: {
            return (56)/2;
        }
        case ResolutionType::iphone4: {
            return (8);
        }
        case ResolutionType::iphone35: {
            return (8);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::recording_bottom_scroll_view_icon_in_character() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(30,10);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(30*2/3,10*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(78,-38);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((78)/2,(-38)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(20,-2);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(20,-2);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::recording_bottom_scroll_view_character_pin_top_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (16);
        }
        case ResolutionType::androidhd: {
            return (16*2/3);
        }
        case ResolutionType::ipadhd: {
            return (32);
        }
        case ResolutionType::ipad: {
            return (32)/2;
        }
        case ResolutionType::iphone4: {
            return (10);
        }
        case ResolutionType::iphone35: {
            return (10);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_bottom_scroll_view_center_icon_bottom_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (279-67-79);
        }
        case ResolutionType::androidhd: {
            return ((279-67-79)*2/3);
        }
        case ResolutionType::ipadhd: {
            return (194);
        }
        case ResolutionType::ipad: {
            return (194)/2;
        }
        case ResolutionType::iphone4: {
            return (80);
        }
        case ResolutionType::iphone35: {
            return (80);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::recording_bottom_scroll_view_coin() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(322,3);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(322*2/3,3*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(344,0);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((344)/2,(0)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(190,0);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(190,0);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::recording_bottom_scroll_view_coin_visible_height() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (27+39+27);
        }
        case ResolutionType::androidhd: {
            return ((27+39+27)*2/3);
        }
        case ResolutionType::ipadhd: {
            return (48+50+48);
        }
        case ResolutionType::ipad: {
            return (48+50+48)/2;
        }
        case ResolutionType::iphone4: {
            return (15+25+15);
        }
        case ResolutionType::iphone35: {
            return (15+25+15);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_bottom_scroll_view_coin_font_size_digit1() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (52);
        }
        case ResolutionType::androidhd: {
            return (52*2/3);
        }
        case ResolutionType::ipadhd: {
            return (76);
        }
        case ResolutionType::ipad: {
            return (76)/2;
        }
        case ResolutionType::iphone4: {
            return (30);
        }
        case ResolutionType::iphone35: {
            return (30);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_bottom_scroll_view_coin_font_size_digit2() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (40);
        }
        case ResolutionType::androidhd: {
            return (40*2/3);
        }
        case ResolutionType::ipadhd: {
            return (58);
        }
        case ResolutionType::ipad: {
            return (58)/2;
        }
        case ResolutionType::iphone4: {
            return (24);
        }
        case ResolutionType::iphone35: {
            return (24);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_bottom_scroll_view_coin_font_size_digit3() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (35);
        }
        case ResolutionType::androidhd: {
            return (35*2/3);
        }
        case ResolutionType::ipadhd: {
            return (50);
        }
        case ResolutionType::ipad: {
            return (50)/2;
        }
        case ResolutionType::iphone4: {
            return (21);
        }
        case ResolutionType::iphone35: {
            return (21);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_bottom_scroll_area_height() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (448);
        }
        case ResolutionType::androidhd: {
            return (448*2/3);
        }
        case ResolutionType::ipadhd: {
            return (612);
        }
        case ResolutionType::ipad: {
            return (612)/2;
        }
        case ResolutionType::iphone4: {
            return (266);
        }
        case ResolutionType::iphone35: {
            return (266);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_bottom_scroll_view_height() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (157+213);
        }
        case ResolutionType::androidhd: {
            return ((157+213)*2/3);
        }
        case ResolutionType::ipadhd: {
            return (180+336);
        }
        case ResolutionType::ipad: {
            return (180+336)/2;
        }
        case ResolutionType::iphone4: {
            return (84+138);
        }
        case ResolutionType::iphone35: {
            return (84+138);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_bottom_scroll_view_pin_bottom_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (244);
        }
        case ResolutionType::androidhd: {
            return (244*2/3);
        }
        case ResolutionType::ipadhd: {
            return (356);
        }
        case ResolutionType::ipad: {
            return (356)/2;
        }
        case ResolutionType::iphone4: {
            return (150);
        }
        case ResolutionType::iphone35: {
            return (150);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::recording_bottom_line_start() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(16,213);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(16*2/3,213*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(0,336);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((0)/2,(336)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(0,138);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(0,138);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::recording_bottom_line_left() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(426,157);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(426*2/3,157*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(582,180);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((582)/2,(180)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(262,84);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(262,84);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::recording_bottom_line_mid() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(875,157);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(875*2/3,157*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(1072,180);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((1072)/2,(180)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(516,84);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(516,84);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::recording_bottom_line_right() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(603,157);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(603*2/3,157*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(690,180);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((690)/2,(180)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(358,84);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(358,84);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::recording_bg_create_btn_visible_rect() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(292,191);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(292*2/3,191*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(436,300);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((436)/2,(300)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(194,135);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(194,135);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::recording_record_icon() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(44,29);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(44*2/3,29*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(48,30);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((48)/2,(30)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(22,19);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(22,19);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::recording_record_text_font_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (46);
        }
        case ResolutionType::androidhd: {
            return (46*2/3);
        }
        case ResolutionType::ipadhd: {
            return (54);
        }
        case ResolutionType::ipad: {
            return (54)/2;
        }
        case ResolutionType::iphone4: {
            return (27);
        }
        case ResolutionType::iphone35: {
            return (27);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_record_text_left_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (30);
        }
        case ResolutionType::androidhd: {
            return (30*2/3);
        }
        case ResolutionType::ipadhd: {
            return (14);
        }
        case ResolutionType::ipad: {
            return (14)/2;
        }
        case ResolutionType::iphone4: {
            return (14);
        }
        case ResolutionType::iphone35: {
            return (14);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_record_text_right_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (44);
        }
        case ResolutionType::androidhd: {
            return (44*2/3);
        }
        case ResolutionType::ipadhd: {
            return (56);
        }
        case ResolutionType::ipad: {
            return (56)/2;
        }
        case ResolutionType::iphone4: {
            return (24);
        }
        case ResolutionType::iphone35: {
            return (24);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::recording_record_button() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(30,929);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(30*2/3,929*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(50,60);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((50)/2,(60)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(20,18);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(20,18);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::recording_done_icon() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(44,29);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(44*2/3,29*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(48,30);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((48)/2,(30)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(22,19);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(22,19);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::recording_done_text_font_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (46);
        }
        case ResolutionType::androidhd: {
            return (46*2/3);
        }
        case ResolutionType::ipadhd: {
            return (54);
        }
        case ResolutionType::ipad: {
            return (54)/2;
        }
        case ResolutionType::iphone4: {
            return (27);
        }
        case ResolutionType::iphone35: {
            return (27);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_done_text_left_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (30);
        }
        case ResolutionType::androidhd: {
            return (30*2/3);
        }
        case ResolutionType::ipadhd: {
            return (14);
        }
        case ResolutionType::ipad: {
            return (14)/2;
        }
        case ResolutionType::iphone4: {
            return (14);
        }
        case ResolutionType::iphone35: {
            return (14);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_done_text_right_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (44);
        }
        case ResolutionType::androidhd: {
            return (44*2/3);
        }
        case ResolutionType::ipadhd: {
            return (56);
        }
        case ResolutionType::ipad: {
            return (56)/2;
        }
        case ResolutionType::iphone4: {
            return (24);
        }
        case ResolutionType::iphone35: {
            return (24);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::recording_done_button() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(30,929);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(30*2/3,929*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(50,60);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((50)/2,(60)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(20,18);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(20,18);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::recording_rerecord_text_font_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (46);
        }
        case ResolutionType::androidhd: {
            return (46*2/3);
        }
        case ResolutionType::ipadhd: {
            return (54);
        }
        case ResolutionType::ipad: {
            return (54)/2;
        }
        case ResolutionType::iphone4: {
            return (27);
        }
        case ResolutionType::iphone35: {
            return (27);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_rerecord_text_left_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (30);
        }
        case ResolutionType::androidhd: {
            return (30*2/3);
        }
        case ResolutionType::ipadhd: {
            return (14);
        }
        case ResolutionType::ipad: {
            return (14)/2;
        }
        case ResolutionType::iphone4: {
            return (14);
        }
        case ResolutionType::iphone35: {
            return (14);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_rerecord_text_right_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (44);
        }
        case ResolutionType::androidhd: {
            return (44*2/3);
        }
        case ResolutionType::ipadhd: {
            return (56);
        }
        case ResolutionType::ipad: {
            return (56)/2;
        }
        case ResolutionType::iphone4: {
            return (24);
        }
        case ResolutionType::iphone35: {
            return (24);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::recording_rerecord_icon() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(44,29);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(44*2/3,29*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(48,30);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((48)/2,(30)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(22,19);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(22,19);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::recording_rerecord_button_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (25);
        }
        case ResolutionType::androidhd: {
            return (25*2/3);
        }
        case ResolutionType::ipadhd: {
            return (30);
        }
        case ResolutionType::ipad: {
            return (30)/2;
        }
        case ResolutionType::iphone4: {
            return (16);
        }
        case ResolutionType::iphone35: {
            return (16);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_save_text_font_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (46);
        }
        case ResolutionType::androidhd: {
            return (46*2/3);
        }
        case ResolutionType::ipadhd: {
            return (54);
        }
        case ResolutionType::ipad: {
            return (54)/2;
        }
        case ResolutionType::iphone4: {
            return (27);
        }
        case ResolutionType::iphone35: {
            return (27);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_save_text_left_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (30);
        }
        case ResolutionType::androidhd: {
            return (30*2/3);
        }
        case ResolutionType::ipadhd: {
            return (14);
        }
        case ResolutionType::ipad: {
            return (14)/2;
        }
        case ResolutionType::iphone4: {
            return (14);
        }
        case ResolutionType::iphone35: {
            return (14);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_save_text_right_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (44);
        }
        case ResolutionType::androidhd: {
            return (44*2/3);
        }
        case ResolutionType::ipadhd: {
            return (56);
        }
        case ResolutionType::ipad: {
            return (56)/2;
        }
        case ResolutionType::iphone4: {
            return (24);
        }
        case ResolutionType::iphone35: {
            return (24);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::recording_save_icon() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(44,29);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(44*2/3,29*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(48,30);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((48)/2,(30)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(22,19);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(22,19);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::recording_save_button() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(30,929);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(30*2/3,929*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(50,60);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((50)/2,(60)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(20,18);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(20,18);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::recording_progress_bar() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(30,19);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(30*2/3,19*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(50,26);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((50)/2,(26)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(20,10);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(20,10);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::recording_recording_text_font_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (46);
        }
        case ResolutionType::androidhd: {
            return (46*2/3);
        }
        case ResolutionType::ipadhd: {
            return (54);
        }
        case ResolutionType::ipad: {
            return (54)/2;
        }
        case ResolutionType::iphone4: {
            return (27);
        }
        case ResolutionType::iphone35: {
            return (27);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_recording_text_side_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (26);
        }
        case ResolutionType::androidhd: {
            return (26*2/3);
        }
        case ResolutionType::ipadhd: {
            return (40);
        }
        case ResolutionType::ipad: {
            return (40)/2;
        }
        case ResolutionType::iphone4: {
            return (20);
        }
        case ResolutionType::iphone35: {
            return (20);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::recording_recording_icon() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(30,929);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(30*2/3,929*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(50,60);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((50)/2,(60)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(20,18);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(20,18);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::recording_remove_area() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(616,80);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(616*2/3,80*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(850,116);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((850)/2,(116)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(366,48);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(366,48);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::recording_progress_bar_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(1860,38);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(1860*2/3,38*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(1948,56);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((1948)/2,(56)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(1096,24);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(920,24);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::recording_progress_mark_bottom_margin() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (10);
        }
        case ResolutionType::androidhd: {
            return (10*2/3);
        }
        case ResolutionType::ipadhd: {
            return (16);
        }
        case ResolutionType::ipad: {
            return (16)/2;
        }
        case ResolutionType::iphone4: {
            return (7);
        }
        case ResolutionType::iphone35: {
            return (7);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::recording_clear_talk_box_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(55+316+24+135+27,23+139+55);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size((55+316+24+135+27)*2/3,(23+139+55)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(68+380+40+166+30,60+50+16+50+92);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((68+380+40+166+30)/2,(60+50+16+50+92)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(30+198+14+82+16,28+28+4+28+46);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(30+198+14+82+16,28+28+4+28+46);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::recording_clear_talk_box() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(9,123);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(9*2/3,123*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(20,166);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((20)/2,(166)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(4,68);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(4,68);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::recording_clear_talk_box_inset() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(50,50);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(50*2/3,50*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(330/2,268/2);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((330/2)/2,(268/2)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(170/2,134/2);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(170/2,134/2);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::recording_clear_talk_box_ok_button_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(135,139);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(135*2/3,139*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(166,174);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((166)/2,(174)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(82,84);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(82,84);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::recording_clear_talk_box_ok_button_inset() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(67,69);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(67*2/3,69*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(82,76);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((82)/2,(76)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(40,40);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(40,40);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::recording_unlock_talk_box_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(78+553+24+195+27,23+139+31);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size((78+553+24+195+27)*2/3,(23+139+31)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(104+634+40+242+30,30+174+32);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((104+634+40+242+30)/2,(30+174+32)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(40+332+14+118+16,16+86+16);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(40+332+14+118+16,16+86+16);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::recording_unlock_talk_box_inset() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(176/2,170/2);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(176/2*2/3,170/2*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(250/2,236/2);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((250/2)/2,(236/2)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(116/2,118/2);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(116/2,118/2);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::recording_unlock_talk_box_inset_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(10,10);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(10*2/3,10*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(30,30);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((30)/2,(30)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(10,10);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(10,10);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::recording_unlock_talk_box_unlock_button_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(195,139);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(195*2/3,139*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(242,174);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((242)/2,(174)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(118,86);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(118,86);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::recording_talk_box_unlock_button() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(27,31);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(27*2/3,31*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(30,32);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((30)/2,(32)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(16,16);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(16,16);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::recording_talk_box_text_font_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (39);
        }
        case ResolutionType::androidhd: {
            return (39*2/3);
        }
        case ResolutionType::ipadhd: {
            return (46);
        }
        case ResolutionType::ipad: {
            return (46)/2;
        }
        case ResolutionType::iphone4: {
            return (24);
        }
        case ResolutionType::iphone35: {
            return (24);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_talk_box_background_max_width_long() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (553);
        }
        case ResolutionType::androidhd: {
            return (553*2/3);
        }
        case ResolutionType::ipadhd: {
            return (792);
        }
        case ResolutionType::ipad: {
            return (792)/2;
        }
        case ResolutionType::iphone4: {
            return (412);
        }
        case ResolutionType::iphone35: {
            return (412);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_talk_box_background_max_width_short() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (553);
        }
        case ResolutionType::androidhd: {
            return (553*2/3);
        }
        case ResolutionType::ipadhd: {
            return (643);
        }
        case ResolutionType::ipad: {
            return (643)/2;
        }
        case ResolutionType::iphone4: {
            return (332);
        }
        case ResolutionType::iphone35: {
            return (332);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_talk_box_character_max_width_long() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (553);
        }
        case ResolutionType::androidhd: {
            return (553*2/3);
        }
        case ResolutionType::ipadhd: {
            return (792);
        }
        case ResolutionType::ipad: {
            return (792)/2;
        }
        case ResolutionType::iphone4: {
            return (412);
        }
        case ResolutionType::iphone35: {
            return (412);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_talk_box_character_max_width_short() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (553);
        }
        case ResolutionType::androidhd: {
            return (553*2/3);
        }
        case ResolutionType::ipadhd: {
            return (482);
        }
        case ResolutionType::ipad: {
            return (482)/2;
        }
        case ResolutionType::iphone4: {
            return (250);
        }
        case ResolutionType::iphone35: {
            return (250);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_talk_box_text_left_margin() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (42);
        }
        case ResolutionType::androidhd: {
            return (42*2/3);
        }
        case ResolutionType::ipadhd: {
            return (62);
        }
        case ResolutionType::ipad: {
            return (62)/2;
        }
        case ResolutionType::iphone4: {
            return (30);
        }
        case ResolutionType::iphone35: {
            return (30);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_talk_box_text_bottom_margin() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (51);
        }
        case ResolutionType::androidhd: {
            return (51*2/3);
        }
        case ResolutionType::ipadhd: {
            return (60);
        }
        case ResolutionType::ipad: {
            return (60)/2;
        }
        case ResolutionType::iphone4: {
            return (29);
        }
        case ResolutionType::iphone35: {
            return (29);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_talk_box_text_top_margin() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (44);
        }
        case ResolutionType::androidhd: {
            return (44*2/3);
        }
        case ResolutionType::ipadhd: {
            return (60);
        }
        case ResolutionType::ipad: {
            return (60)/2;
        }
        case ResolutionType::iphone4: {
            return (29);
        }
        case ResolutionType::iphone35: {
            return (29);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_talk_box_unlock_button_text_max_width() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (195);
        }
        case ResolutionType::androidhd: {
            return (195*2/3);
        }
        case ResolutionType::ipadhd: {
            return (242);
        }
        case ResolutionType::ipad: {
            return (242)/2;
        }
        case ResolutionType::iphone4: {
            return (118);
        }
        case ResolutionType::iphone35: {
            return (118);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_talk_box_unlock_button_text_side_margin() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (6);
        }
        case ResolutionType::androidhd: {
            return (6*2/3);
        }
        case ResolutionType::ipadhd: {
            return (12);
        }
        case ResolutionType::ipad: {
            return (12)/2;
        }
        case ResolutionType::iphone4: {
            return (4);
        }
        case ResolutionType::iphone35: {
            return (4);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_talk_box_unlock_button_right_margin() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (27);
        }
        case ResolutionType::androidhd: {
            return (27*2/3);
        }
        case ResolutionType::ipadhd: {
            return (30);
        }
        case ResolutionType::ipad: {
            return (30)/2;
        }
        case ResolutionType::iphone4: {
            return (16);
        }
        case ResolutionType::iphone35: {
            return (16);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_talk_box_unlock_button_left_margin() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (24);
        }
        case ResolutionType::androidhd: {
            return (24*2/3);
        }
        case ResolutionType::ipadhd: {
            return (40);
        }
        case ResolutionType::ipad: {
            return (40)/2;
        }
        case ResolutionType::iphone4: {
            return (14);
        }
        case ResolutionType::iphone35: {
            return (14);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_talk_box_unlock_button_bottom_margin() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (31);
        }
        case ResolutionType::androidhd: {
            return (31*2/3);
        }
        case ResolutionType::ipadhd: {
            return (32);
        }
        case ResolutionType::ipad: {
            return (32)/2;
        }
        case ResolutionType::iphone4: {
            return (16);
        }
        case ResolutionType::iphone35: {
            return (16);
        }
    }
    return 0.0f;
}

float ResourceManager::recording_talk_box_unlock_button_top_margin() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (23);
        }
        case ResolutionType::androidhd: {
            return (23*2/3);
        }
        case ResolutionType::ipadhd: {
            return (30);
        }
        case ResolutionType::ipad: {
            return (30)/2;
        }
        case ResolutionType::iphone4: {
            return (16);
        }
        case ResolutionType::iphone35: {
            return (16);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::recording_talk_box_text_label_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(587,43+12+43);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(587*2/3,(43+12+43)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(690,50+16+50);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((690)/2,(50+16+50)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(360,28+4+28);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(360,28+4+28);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::recording_clear_talk_box_top_label() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(55,75+43+12);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(55*2/3,(75+43+12)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(68,92+50+16);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((68)/2,(92+50+16)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(30,46+28+4);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(30,46+28+4);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::recording_clear_talk_box_second_label() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(55,75);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(55*2/3,75*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(68,92);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((68)/2,(92)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(30,46);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(30,46);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::recording_unlock_talk_box_top_label() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(78,51+43+12);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(78*2/3,(51+43+12)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(104,60+50+18);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((104)/2,(60+50+18)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(40,30+28+4);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(40,30+28+4);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::recording_unlock_talk_box_second_label() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(78,51);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(78*2/3,51*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(104,60);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((104)/2,(60)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(40,30);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(40,30);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::recording_talk_box_ok_button() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(24,55);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(24*2/3,55*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(40,64);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((40)/2,(64)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(14,34);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(14,34);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::recording_talk_box_unlock_button_font_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (50);
                } break;
                case LanguageType::KOREAN: {
                    return (40);
                } break;
                case LanguageType::JAPANESE: {
                    return (32);
                } break;
            }
        }
        case ResolutionType::androidhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (50*2/3);
                } break;
                case LanguageType::KOREAN: {
                    return (40*2/3);
                } break;
                case LanguageType::JAPANESE: {
                    return (32*2/3);
                } break;
            }
        }
        case ResolutionType::ipadhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (60);
                } break;
                case LanguageType::KOREAN: {
                    return (48);
                } break;
                case LanguageType::JAPANESE: {
                    return (40);
                } break;
            }
        }
        case ResolutionType::ipad: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (60)/2;
                } break;
                case LanguageType::KOREAN: {
                    return (48)/2;
                } break;
                case LanguageType::JAPANESE: {
                    return (40)/2;
                } break;
            }
        }
        case ResolutionType::iphone4: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (29);
                } break;
                case LanguageType::KOREAN: {
                    return (22);
                } break;
                case LanguageType::JAPANESE: {
                    return (18);
                } break;
            }
        }
        case ResolutionType::iphone35: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (29);
                } break;
                case LanguageType::KOREAN: {
                    return (22);
                } break;
                case LanguageType::JAPANESE: {
                    return (18);
                } break;
            }
        }
    }
    return 0.0f;
}

float ResourceManager::recording_sound_scroll_label_font_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (42);
        }
        case ResolutionType::androidhd: {
            return (42*2/3);
        }
        case ResolutionType::ipadhd: {
            return (52);
        }
        case ResolutionType::ipad: {
            return (52)/2;
        }
        case ResolutionType::iphone4: {
            return (24);
        }
        case ResolutionType::iphone35: {
            return (24);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::recording_sound_scroll_label_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(292,(46+24+24));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(292*2/3,(46+24+24)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(298,54);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((298)/2,(54)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(152,28);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(152,28);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::recording_sound_scroll_label_shodow() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(0,-2);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(0,-2);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(0,-2);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((0)/2,(-2)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(0,-1);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(0,-1);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::recording_sound_scroll_label() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(0,(324-286));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(0,(324-286)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point((358-298)/2,90);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point(((358-298)/2)/2,(90)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point((172-152)/2,12);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point((172-152)/2,12);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::share_cancel_text_font_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (46);
        }
        case ResolutionType::androidhd: {
            return (46*2/3);
        }
        case ResolutionType::ipadhd: {
            return (54);
        }
        case ResolutionType::ipad: {
            return (54)/2;
        }
        case ResolutionType::iphone4: {
            return (27);
        }
        case ResolutionType::iphone35: {
            return (27);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::share_cancel_icon() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(44,29);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(44*2/3,29*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(48,30);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((48)/2,(30)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(22,19);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(22,19);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::share_cancel_text_left_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (30);
        }
        case ResolutionType::androidhd: {
            return (30*2/3);
        }
        case ResolutionType::ipadhd: {
            return (4);
        }
        case ResolutionType::ipad: {
            return (4)/2;
        }
        case ResolutionType::iphone4: {
            return (14);
        }
        case ResolutionType::iphone35: {
            return (14);
        }
    }
    return 0.0f;
}

float ResourceManager::share_cancel_text_right_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (44);
        }
        case ResolutionType::androidhd: {
            return (44*2/3);
        }
        case ResolutionType::ipadhd: {
            return (46);
        }
        case ResolutionType::ipad: {
            return (46)/2;
        }
        case ResolutionType::iphone4: {
            return (24);
        }
        case ResolutionType::iphone35: {
            return (24);
        }
    }
    return 0.0f;
}

cocos2d::Point ResourceManager::share_cancel_button() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(30,929);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(30*2/3,929*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(50,60);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((50)/2,(60)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(20,18);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(20,18);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::map_play_title_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(175,58);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(175*2/3,58*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(256,78);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((256)/2,(78)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(104,36);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(104,36);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::map_play_title_bottom_margin() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (53);
        }
        case ResolutionType::androidhd: {
            return (53*2/3);
        }
        case ResolutionType::ipadhd: {
            return (90);
        }
        case ResolutionType::ipad: {
            return (90)/2;
        }
        case ResolutionType::iphone4: {
            return (54);
        }
        case ResolutionType::iphone35: {
            return (54);
        }
    }
    return 0.0f;
}

float ResourceManager::map_play_title_top_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (60);
        }
        case ResolutionType::androidhd: {
            return (60*2/3);
        }
        case ResolutionType::ipadhd: {
            return (76);
        }
        case ResolutionType::ipad: {
            return (76)/2;
        }
        case ResolutionType::iphone4: {
            return (36);
        }
        case ResolutionType::iphone35: {
            return (36);
        }
    }
    return 0.0f;
}

float ResourceManager::map_play_title_text_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (54);
                } break;
                case LanguageType::KOREAN: {
                    return (44);
                } break;
                case LanguageType::JAPANESE: {
                    return (44);
                } break;
            }
        }
        case ResolutionType::androidhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (54*2/3);
                } break;
                case LanguageType::KOREAN: {
                    return (44*2/3);
                } break;
                case LanguageType::JAPANESE: {
                    return (44*2/3);
                } break;
            }
        }
        case ResolutionType::ipadhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (74);
                } break;
                case LanguageType::KOREAN: {
                    return (60);
                } break;
                case LanguageType::JAPANESE: {
                    return (50);
                } break;
            }
        }
        case ResolutionType::ipad: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (74)/2;
                } break;
                case LanguageType::KOREAN: {
                    return (60)/2;
                } break;
                case LanguageType::JAPANESE: {
                    return (50)/2;
                } break;
            }
        }
        case ResolutionType::iphone4: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (32);
                } break;
                case LanguageType::KOREAN: {
                    return (26);
                } break;
                case LanguageType::JAPANESE: {
                    return (20);
                } break;
            }
        }
        case ResolutionType::iphone35: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (32);
                } break;
                case LanguageType::KOREAN: {
                    return (26);
                } break;
                case LanguageType::JAPANESE: {
                    return (20);
                } break;
            }
        }
    }
    return 0.0f;
}

float ResourceManager::map_play_title_text_storke_width() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (6);
        }
        case ResolutionType::androidhd: {
            return (6*2/3);
        }
        case ResolutionType::ipadhd: {
            return (8);
        }
        case ResolutionType::ipad: {
            return (8)/2;
        }
        case ResolutionType::iphone4: {
            return (4);
        }
        case ResolutionType::iphone35: {
            return (4);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::toast_bg_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(30,30);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(20,20);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(124,124);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((124)/2,(124)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(72,72);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(72,72);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::toast_bg_corner_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(11,11);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(8,8);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(12,12);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((12)/2,(12)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(8,8);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(8,8);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::toast_bg_corner_inset() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(8,8);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(4,4);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(12,12);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((12)/2,(12)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(8,8);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(8,8);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::toast_content_size_type_01() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(646,124);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(646*2/3,124*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(646,124);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((646)/2,(124)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(336+20+20,72);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(336+20+20,72);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::toast_content_size_type_02() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(524+38+38,0);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size((524+38+38)*2/3,0);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(524+38+38,0);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((524+38+38)/2,(0)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(314+20+20,0);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(314+20+20,0);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::toast_type_02_extra_width_ja() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (200);
        }
        case ResolutionType::androidhd: {
            return (200*2/3);
        }
        case ResolutionType::ipadhd: {
            return (0);
        }
        case ResolutionType::ipad: {
            return (0)/2;
        }
        case ResolutionType::iphone4: {
            return (0);
        }
        case ResolutionType::iphone35: {
            return (0);
        }
    }
    return 0.0f;
}

float ResourceManager::toast_message_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (48);
        }
        case ResolutionType::androidhd: {
            return (48*2/3);
        }
        case ResolutionType::ipadhd: {
            return (48);
        }
        case ResolutionType::ipad: {
            return (48)/2;
        }
        case ResolutionType::iphone4: {
            return (28);
        }
        case ResolutionType::iphone35: {
            return (28);
        }
    }
    return 0.0f;
}

float ResourceManager::toast_message_ja_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (48);
        }
        case ResolutionType::androidhd: {
            return (48*2/3);
        }
        case ResolutionType::ipadhd: {
            return ((48-16));
        }
        case ResolutionType::ipad: {
            return ((48-16))/2;
        }
        case ResolutionType::iphone4: {
            return ((28-10));
        }
        case ResolutionType::iphone35: {
            return ((28-10));
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::toast_label_size_type_01() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size((370+34+34),(52+36*2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size((370+34+34)*2/3,(52+36*2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(570,52);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((570)/2,(52)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(336,32);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(336,32);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::toast_label_size_type_02() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size((524+34+34),(188));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size((524+34+34)*2/3,(188)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(524,52);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((524)/2,(52)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(314,32);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(314,32);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::toast_bg_padding_top_bottom() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (0);
        }
        case ResolutionType::androidhd: {
            return (0);
        }
        case ResolutionType::ipadhd: {
            return ((124-52));
        }
        case ResolutionType::ipad: {
            return ((124-52))/2;
        }
        case ResolutionType::iphone4: {
            return ((72-32));
        }
        case ResolutionType::iphone35: {
            return ((72-32));
        }
    }
    return 0.0f;
}

float ResourceManager::toast_bg_padding_left() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (34);
        }
        case ResolutionType::androidhd: {
            return (34*2/3);
        }
        case ResolutionType::ipadhd: {
            return (38);
        }
        case ResolutionType::ipad: {
            return (38)/2;
        }
        case ResolutionType::iphone4: {
            return (20);
        }
        case ResolutionType::iphone35: {
            return (20);
        }
    }
    return 0.0f;
}

float ResourceManager::toast_bg_y() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (32);
        }
        case ResolutionType::androidhd: {
            return (32*2/3);
        }
        case ResolutionType::ipadhd: {
            return (32);
        }
        case ResolutionType::ipad: {
            return (32)/2;
        }
        case ResolutionType::iphone4: {
            return (70);
        }
        case ResolutionType::iphone35: {
            return (70);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::label_shadow_default_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(0,-6);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(0,-6*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(0,-6);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((0)/2,(-6)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(0,-2);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(0,-2);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::label_shadow_author_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(0,-4);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(0,-4*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(0,-4);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((0)/2,(-4)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(0,-2);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(0,-2);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::label_shadow_play_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(0,-16);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(0,-16*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(0,-16);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((0)/2,(-16)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(0,-5);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(0,-5);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::label_shadow_camera_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(6,-5.196);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(6*2/3,-5.196*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(6,-5.196);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((6)/2,(-5.196)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(2,-1.732);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(2,-1.732);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c001_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(62,187);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(62*2/3,187*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(76,227);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((76)/2,(227)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(36,107);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(36,107);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c002_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(53,188);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(53*2/3,188*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(64,224);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((64)/2,(224)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(31,110);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(31,110);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c003_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(48,184);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(48*2/3,184*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(56,218);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((56)/2,(218)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(29,107);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(29,107);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c004_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(67,161);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(67*2/3,161*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(80,192);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((80)/2,(192)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(38,93);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(38,93);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c005_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(51,133);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(51*2/3,133*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(62,159);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((62)/2,(159)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(30,78);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(30,78);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c006_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(80,163);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(80*2/3,163*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(95,195);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((95)/2,(195)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(45,97);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(45,97);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c007_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(51,138);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(51*2/3,138*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(60,164);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((60)/2,(164)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(28,78);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(28,78);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c008_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(72,126);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(72*2/3,126*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(87,150);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((87)/2,(150)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(42,73);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(42,73);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c009_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(55,138);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(55*2/3,138*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(65,164);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((65)/2,(164)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(32,79);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(32,79);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c010_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(49,151);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(49*2/3,151*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(61,179);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((61)/2,(179)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(31,88);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(31,88);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c011_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(60,142);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(60*2/3,142*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(71,169);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((71)/2,(169)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(35,82);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(35,82);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c012_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(60,151);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(60*2/3,151*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(70,180);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((70)/2,(180)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(34,87);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(34,87);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c013_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(41,174);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(41*2/3,174*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(48,208);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((48)/2,(208)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(24,98);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(24,98);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c014_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(47,175);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(47*2/3,175*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(56,211);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((56)/2,(211)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(28,101);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(28,101);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c001_side_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((18+82/2),(73+82/2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((18+82/2)*2/3,(73+82/2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(24+(114/2),105+(114/2));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((24+(114/2))/2,(105+(114/2))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(10+(49/2),59+(49/2));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(10+(49/2),59+(49/2));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c002_side_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((15+82/2),(73+82/2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((15+82/2)*2/3,(73+82/2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(21+(114/2),107+(114/2));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((21+(114/2))/2,(107+(114/2))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(10+(48/2),61+(48/2));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(10+(48/2),61+(48/2));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c003_side_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((26+78/2),(71+78/2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((26+78/2)*2/3,(71+78/2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(36+(108/2),103+(108/2));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((36+(108/2))/2,(103+(108/2))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(16+(46/2),59+(46/2));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(16+(46/2),59+(46/2));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c004_side_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((24+74/2),(70+74/2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((24+74/2)*2/3,(70+74/2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(33+(102/2),102+(102/2));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((33+(102/2))/2,(102+(102/2))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(15+(43/2),59+(43/2));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(15+(43/2),59+(43/2));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c005_side_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((106/2-16),(57+106/2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((106/2-16)*2/3,(57+106/2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(-23+(147/2),85+(147/2));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((-23+(147/2))/2,(85+(147/2))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(-7+(61/2),53+(61/2));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(-7+(61/2),53+(61/2));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c006_side_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((82/2-3),(54+82/2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((82/2-3)*2/3,(54+82/2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(-3+(113/2),80+(113/2));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((-3+(113/2))/2,(80+(113/2))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(0+(48/2),50+(48/2));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(0+(48/2),50+(48/2));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c007_side_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((0+100/2),(54+100/2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((0+100/2)*2/3,(54+100/2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(1+(137/2),80+(137/2));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((1+(137/2))/2,(80+(137/2))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(2+(57/2),51+(57/2));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(2+(57/2),51+(57/2));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c008_side_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((1+88/2),(50+88/2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((1+88/2)*2/3,(50+88/2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(2+(122/2),73+(122/2));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((2+(122/2))/2,(73+(122/2))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(1+(51/2),47+(51/2));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(1+(51/2),47+(51/2));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c009_side_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((98/2-11),(62+98/2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((98/2-11)*2/3,(62+98/2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(-16+(136/2),91+(136/2));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((-16+(136/2))/2,(91+(136/2))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(-5+(57/2),56+(57/2));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(-5+(57/2),56+(57/2));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c010_side_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((5+87/2),(58+87/2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((5+87/2)*2/3,(58+87/2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(7+(121/2),85+(121/2));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((7+(121/2))/2,(85+(121/2))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(4+(50/2),54+(50/2));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(4+(50/2),54+(50/2));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c011_side_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((89/2-4),(53+89/2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((89/2-4)*2/3,(53+89/2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(1+(124/2),81+(124/2));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((1+(124/2))/2,(81+(124/2))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(-1+(52/2),50+(52/2));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(-1+(52/2),50+(52/2));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c012_side_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((2+87/2),(63+87/2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((2+87/2)*2/3,(63+87/2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(3+(120/2),94+(120/2));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((3+(120/2))/2,(94+(120/2))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(0+(50/2),57+(50/2));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(0+(50/2),57+(50/2));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c013_side_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((13+90/2),(66+90/2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((13+90/2)*2/3,(66+90/2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(18+(125/2),96+(125/2));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((18+(125/2))/2,(96+(125/2))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(8+(52/2),57+(52/2));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(8+(52/2),57+(52/2));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::custom_character_c014_side_photo() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((1+86/2),(55+86/2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((1+86/2)*2/3,(55+86/2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(0+(120/2),78+(120/2));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((0+(120/2))/2,(78+(120/2))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(0+(50/2),49+(50/2));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(0+(50/2),49+(50/2));
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Size ResourceManager::custom_character_c001_side_photo_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(82,82);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(82*2/3,82*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(114,114);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((114)/2,(114)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(49,49);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(49,49);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::custom_character_c002_side_photo_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(82,82);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(82*2/3,82*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(114,114);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((114)/2,(114)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(48,48);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(48,48);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::custom_character_c003_side_photo_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(78,78);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(78*2/3,78*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(108,108);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((108)/2,(108)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(46,46);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(46,46);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::custom_character_c004_side_photo_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(74,74);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(74*2/3,74*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(102,102);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((102)/2,(102)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(43,43);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(43,43);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::custom_character_c005_side_photo_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(106,106);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(106*2/3,106*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(147,147);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((147)/2,(147)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(61,61);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(61,61);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::custom_character_c006_side_photo_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(82,82);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(82*2/3,82*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(113,113);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((113)/2,(113)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(48,48);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(48,48);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::custom_character_c007_side_photo_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(100,100);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(100*2/3,100*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(137,137);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((137)/2,(137)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(57,57);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(57,57);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::custom_character_c008_side_photo_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(88,88);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(88*2/3,88*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(122,122);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((122)/2,(122)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(51,51);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(51,51);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::custom_character_c009_side_photo_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(98,98);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(98*2/3,98*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(136,136);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((136)/2,(136)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(57,57);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(57,57);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::custom_character_c010_side_photo_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(87,87);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(87*2/3,87*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(121,121);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((121)/2,(121)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(50,50);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(50,50);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::custom_character_c011_side_photo_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(89,89);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(89*2/3,89*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(124,124);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((124)/2,(124)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(52,52);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(52,52);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::custom_character_c012_side_photo_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(87,87);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(87*2/3,87*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(120,120);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((120)/2,(120)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(50,50);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(50,50);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::custom_character_c013_side_photo_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(90,90);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(90*2/3,90*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(125,125);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((125)/2,(125)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(52,52);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(52,52);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::custom_character_c014_side_photo_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(86,86);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(86*2/3,86*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(120,120);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((120)/2,(120)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(50,50);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(50,50);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::opening_single_title_label_font_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (120);
                } break;
                case LanguageType::KOREAN: {
                    return (118);
                } break;
                case LanguageType::JAPANESE: {
                    return (118);
                } break;
            }
        }
        case ResolutionType::androidhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (120*2/3);
                } break;
                case LanguageType::KOREAN: {
                    return (118*2/3);
                } break;
                case LanguageType::JAPANESE: {
                    return (118*2/3);
                } break;
            }
        }
        case ResolutionType::ipadhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (170);
                } break;
                case LanguageType::KOREAN: {
                    return (166);
                } break;
                case LanguageType::JAPANESE: {
                    return (166);
                } break;
            }
        }
        case ResolutionType::ipad: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (170)/2;
                } break;
                case LanguageType::KOREAN: {
                    return (166)/2;
                } break;
                case LanguageType::JAPANESE: {
                    return (166)/2;
                } break;
            }
        }
        case ResolutionType::iphone4: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (72);
                } break;
                case LanguageType::KOREAN: {
                    return (70);
                } break;
                case LanguageType::JAPANESE: {
                    return (70);
                } break;
            }
        }
        case ResolutionType::iphone35: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (72);
                } break;
                case LanguageType::KOREAN: {
                    return (70);
                } break;
                case LanguageType::JAPANESE: {
                    return (70);
                } break;
            }
        }
    }
    return 0.0f;
}

float ResourceManager::opening_double_title_label_font_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (108);
                } break;
                case LanguageType::KOREAN: {
                    return (100);
                } break;
                case LanguageType::JAPANESE: {
                    return (100);
                } break;
            }
        }
        case ResolutionType::androidhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (108*2/3);
                } break;
                case LanguageType::KOREAN: {
                    return (100*2/3);
                } break;
                case LanguageType::JAPANESE: {
                    return (100*2/3);
                } break;
            }
        }
        case ResolutionType::ipadhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (154);
                } break;
                case LanguageType::KOREAN: {
                    return (146);
                } break;
                case LanguageType::JAPANESE: {
                    return (146);
                } break;
            }
        }
        case ResolutionType::ipad: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (154)/2;
                } break;
                case LanguageType::KOREAN: {
                    return (146)/2;
                } break;
                case LanguageType::JAPANESE: {
                    return (146)/2;
                } break;
            }
        }
        case ResolutionType::iphone4: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (64);
                } break;
                case LanguageType::KOREAN: {
                    return (56);
                } break;
                case LanguageType::JAPANESE: {
                    return (56);
                } break;
            }
        }
        case ResolutionType::iphone35: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (64);
                } break;
                case LanguageType::KOREAN: {
                    return (56);
                } break;
                case LanguageType::JAPANESE: {
                    return (56);
                } break;
            }
        }
    }
    return 0.0f;
}

float ResourceManager::opening_author_label_font_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (70);
                } break;
                case LanguageType::KOREAN: {
                    return (62);
                } break;
                case LanguageType::JAPANESE: {
                    return (62);
                } break;
            }
        }
        case ResolutionType::androidhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (70*2/3);
                } break;
                case LanguageType::KOREAN: {
                    return (62*2/3);
                } break;
                case LanguageType::JAPANESE: {
                    return (62*2/3);
                } break;
            }
        }
        case ResolutionType::ipadhd: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (100);
                } break;
                case LanguageType::KOREAN: {
                    return (92);
                } break;
                case LanguageType::JAPANESE: {
                    return (92);
                } break;
            }
        }
        case ResolutionType::ipad: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (100)/2;
                } break;
                case LanguageType::KOREAN: {
                    return (92)/2;
                } break;
                case LanguageType::JAPANESE: {
                    return (92)/2;
                } break;
            }
        }
        case ResolutionType::iphone4: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (42);
                } break;
                case LanguageType::KOREAN: {
                    return (34);
                } break;
                case LanguageType::JAPANESE: {
                    return (34);
                } break;
            }
        }
        case ResolutionType::iphone35: {
            switch (currentLanguageType) {
                case LanguageType::ENGLISH:
                default: {
                    return (42);
                } break;
                case LanguageType::KOREAN: {
                    return (34);
                } break;
                case LanguageType::JAPANESE: {
                    return (34);
                } break;
            }
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::opening_single_title_label_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(1368,(130+30*2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(1368*2/3,(130+30*2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(1402,190);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((1402)/2,(190)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(668,90);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(668,90);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Size ResourceManager::opening_double_title_label_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(1368,(113+30*2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(1368*2/3,(113+30*2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(1402,165);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((1402)/2,(165)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(668,70);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(668,70);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::opening_single_title_label() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(150,(198-30+15));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(150*2/3,(198-30+15)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(234,280);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((234)/2,(280)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(94,112);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(94,112);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::opening_double_title_label_01() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(150,(125+113-30));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(150*2/3,(125+113-30)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(234,170+165);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((234)/2,(170+165)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(94,72+70);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(94,72+70);
        }
    }
    return cocos2d::Point::ZERO;
}

cocos2d::Point ResourceManager::opening_double_title_label_02() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point(150,(125-30));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point(150*2/3,(125-30)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(234,170);
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((234)/2,(170)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(94,72);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(94,72);
        }
    }
    return cocos2d::Point::ZERO;
}

float ResourceManager::opening_title_label_shadow_distance() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (5);
        }
        case ResolutionType::androidhd: {
            return (5*2/3);
        }
        case ResolutionType::ipadhd: {
            return (6);
        }
        case ResolutionType::ipad: {
            return (6)/2;
        }
        case ResolutionType::iphone4: {
            return (4);
        }
        case ResolutionType::iphone35: {
            return (4);
        }
    }
    return 0.0f;
}

float ResourceManager::opening_author_label_side_margin() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (254);
        }
        case ResolutionType::androidhd: {
            return (254*2/3);
        }
        case ResolutionType::ipadhd: {
            return (540);
        }
        case ResolutionType::ipad: {
            return (540)/2;
        }
        case ResolutionType::iphone4: {
            return (222);
        }
        case ResolutionType::iphone35: {
            return (222);
        }
    }
    return 0.0f;
}

float ResourceManager::opening_author_label_shadow_distance() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (3);
        }
        case ResolutionType::androidhd: {
            return (3*2/3);
        }
        case ResolutionType::ipadhd: {
            return (4);
        }
        case ResolutionType::ipad: {
            return (4)/2;
        }
        case ResolutionType::iphone4: {
            return (2);
        }
        case ResolutionType::iphone35: {
            return (2);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::opening_author_label_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(1230,(74+20));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(1230*2/3,(74+20)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(1286,104);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((1286)/2,(104)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(588,46);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(588,46);
        }
    }
    return cocos2d::Size::ZERO;
}

float ResourceManager::opening_author_label_top_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return ((80+10));
        }
        case ResolutionType::androidhd: {
            return ((80+10)*2/3);
        }
        case ResolutionType::ipadhd: {
            return (126);
        }
        case ResolutionType::ipad: {
            return (126)/2;
        }
        case ResolutionType::iphone4: {
            return (54);
        }
        case ResolutionType::iphone35: {
            return (54);
        }
    }
    return 0.0f;
}

float ResourceManager::opening_author_label_star_bottom_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return ((22+10));
        }
        case ResolutionType::androidhd: {
            return ((22+10)*2/3);
        }
        case ResolutionType::ipadhd: {
            return (26);
        }
        case ResolutionType::ipad: {
            return (26)/2;
        }
        case ResolutionType::iphone4: {
            return (14);
        }
        case ResolutionType::iphone35: {
            return (14);
        }
    }
    return 0.0f;
}

float ResourceManager::opening_author_label_star_side_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (20);
        }
        case ResolutionType::androidhd: {
            return (20*2/3);
        }
        case ResolutionType::ipadhd: {
            return (16);
        }
        case ResolutionType::ipad: {
            return (16)/2;
        }
        case ResolutionType::iphone4: {
            return (4);
        }
        case ResolutionType::iphone35: {
            return (4);
        }
    }
    return 0.0f;
}

float ResourceManager::opening_title_text_max_width() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (1514);
        }
        case ResolutionType::androidhd: {
            return (1514*2/3);
        }
        case ResolutionType::ipadhd: {
            return (1500);
        }
        case ResolutionType::ipad: {
            return (1500)/2;
        }
        case ResolutionType::iphone4: {
            return (876);
        }
        case ResolutionType::iphone35: {
            return (720);
        }
    }
    return 0.0f;
}

float ResourceManager::opening_author_text_max_width() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (1306);
        }
        case ResolutionType::androidhd: {
            return (1306*2/3);
        }
        case ResolutionType::ipadhd: {
            return (888);
        }
        case ResolutionType::ipad: {
            return (888)/2;
        }
        case ResolutionType::iphone4: {
            return (620);
        }
        case ResolutionType::iphone35: {
            return (464);
        }
    }
    return 0.0f;
}

float ResourceManager::opening_title_center_min_width() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (344);
        }
        case ResolutionType::androidhd: {
            return (344*2/3);
        }
        case ResolutionType::ipadhd: {
            return (488);
        }
        case ResolutionType::ipad: {
            return (488)/2;
        }
        case ResolutionType::iphone4: {
            return (208);
        }
        case ResolutionType::iphone35: {
            return (208);
        }
    }
    return 0.0f;
}

float ResourceManager::opening_title_center_max_width() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (1118);
        }
        case ResolutionType::androidhd: {
            return (1118*2/3);
        }
        case ResolutionType::ipadhd: {
            return (976);
        }
        case ResolutionType::ipad: {
            return (976)/2;
        }
        case ResolutionType::iphone4: {
            return (676);
        }
        case ResolutionType::iphone35: {
            return (520);
        }
    }
    return 0.0f;
}

float ResourceManager::opening_title_node_top_padding() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (349);
        }
        case ResolutionType::androidhd: {
            return (349*2/3);
        }
        case ResolutionType::ipadhd: {
            return (492);
        }
        case ResolutionType::ipad: {
            return (492)/2;
        }
        case ResolutionType::iphone4: {
            return (204);
        }
        case ResolutionType::iphone35: {
            return (204);
        }
    }
    return 0.0f;
}

float ResourceManager::guide_label_nanum_fontSize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (76);
        }
        case ResolutionType::androidhd: {
            return (76*2/3);
        }
        case ResolutionType::ipadhd: {
            return (96);
        }
        case ResolutionType::ipad: {
            return (96)/2;
        }
        case ResolutionType::iphone4: {
            return (44);
        }
        case ResolutionType::iphone35: {
            return (44);
        }
    }
    return 0.0f;
}

float ResourceManager::guide_label_normal_fontSize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (44);
        }
        case ResolutionType::androidhd: {
            return (44*2/3);
        }
        case ResolutionType::ipadhd: {
            return (54);
        }
        case ResolutionType::ipad: {
            return (54)/2;
        }
        case ResolutionType::iphone4: {
            return (24);
        }
        case ResolutionType::iphone35: {
            return (24);
        }
    }
    return 0.0f;
}

float ResourceManager::guide_label_big_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (80);
        }
        case ResolutionType::androidhd: {
            return (80*2/3);
        }
        case ResolutionType::ipadhd: {
            return (96);
        }
        case ResolutionType::ipad: {
            return (96)/2;
        }
        case ResolutionType::iphone4: {
            return (48);
        }
        case ResolutionType::iphone35: {
            return (48);
        }
    }
    return 0.0f;
}

float ResourceManager::guide_label_jp_big_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (50);
        }
        case ResolutionType::androidhd: {
            return (50*2/3);
        }
        case ResolutionType::ipadhd: {
            return (60);
        }
        case ResolutionType::ipad: {
            return (60)/2;
        }
        case ResolutionType::iphone4: {
            return (30);
        }
        case ResolutionType::iphone35: {
            return (30);
        }
    }
    return 0.0f;
}

float ResourceManager::guide_label_small_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (36);
        }
        case ResolutionType::androidhd: {
            return (36*2/3);
        }
        case ResolutionType::ipadhd: {
            return (36);
        }
        case ResolutionType::ipad: {
            return (36)/2;
        }
        case ResolutionType::iphone4: {
            return (24);
        }
        case ResolutionType::iphone35: {
            return (24);
        }
    }
    return 0.0f;
}

float ResourceManager::guide_label_jp_small_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (36);
        }
        case ResolutionType::androidhd: {
            return (36*2/3);
        }
        case ResolutionType::ipadhd: {
            return (36);
        }
        case ResolutionType::ipad: {
            return (36)/2;
        }
        case ResolutionType::iphone4: {
            return (24);
        }
        case ResolutionType::iphone35: {
            return (24);
        }
    }
    return 0.0f;
}

float ResourceManager::progress_label_fontsize() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return (44);
        }
        case ResolutionType::androidhd: {
            return (44);
        }
        case ResolutionType::ipadhd: {
            return (62);
        }
        case ResolutionType::ipad: {
            return (62)/2;
        }
        case ResolutionType::iphone4: {
            return (26);
        }
        case ResolutionType::iphone35: {
            return (26);
        }
    }
    return 0.0f;
}

cocos2d::Size ResourceManager::progress_label_size() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Size(340,48);
        }
        case ResolutionType::androidhd: {
            return cocos2d::Size(340,48);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Size(492,66);
        }
        case ResolutionType::ipad: {
            return cocos2d::Size((492)/2,(66)/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Size(206,30);
        }
        case ResolutionType::iphone35: {
            return cocos2d::Size(206,30);
        }
    }
    return cocos2d::Size::ZERO;
}

cocos2d::Point ResourceManager::progress_label_position() {
    switch (currentType) {
        case ResolutionType::androidfullhd: {
            return cocos2d::Point((1920/2),((1080-176)/2-26-48/2));
        }
        case ResolutionType::androidhd: {
            return cocos2d::Point((1920/2)*2/3,((1080-176)/2-26-48/2)*2/3);
        }
        case ResolutionType::ipadhd: {
            return cocos2d::Point(0,(1536/2-(252/2+38+66)));
        }
        case ResolutionType::ipad: {
            return cocos2d::Point((0)/2,((1536/2-(252/2+38+66)))/2);
        }
        case ResolutionType::iphone4: {
            return cocos2d::Point(0,(640/2-(104/2+14+30)));
        }
        case ResolutionType::iphone35: {
            return cocos2d::Point(0,(640/2-(104/2+14+30)));
        }
    }
    return cocos2d::Point::ZERO;
}
// POS_C_FUNC_END  **do not delete this line**
