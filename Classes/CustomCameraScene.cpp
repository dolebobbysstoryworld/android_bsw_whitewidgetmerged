//
//  CustomCameraScene.cpp
//  bobby
//
//  Created by oasis on 2014. 4. 14..
//
//

#include "CustomCameraScene.h"
#include "HomeScene.h"
#include "ResourceManager.h"
#include "Define.h"

#include "external/json/document.h"
#include "external/json/prettywriter.h"
#include "external/json/stringbuffer.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#import "CamUIViewController.h"
#import "CharacterCamViewController.h"
#import "AppController.h"

#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#endif



//Scene* CustomCamera::createScene()
//{
//    // 'scene' is an autorelease object
//    auto scene = Scene::create();
//    
//    // 'layer' is an autorelease object
//    auto layer = CameraScene::create();
//    
//    // add layer as a child to scene
//    scene->addChild(layer);
//    
//    // return the scene
//    return scene;
//}

Scene* CustomCamera::createWithViewType(camera_type type)
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = CustomCamera::create();
    layer->setTag(TAG_CAMERA);
    if (layer && layer->initWithViewType(type)) {
        // add layer as a child to scene
        scene->addChild(layer);
    }
    // return the scene
    return scene;
}

bool CustomCamera::initWithViewType(camera_type type)
{
    if ( !LayerColor::initWithColor( Color4B(255,255,255,255)) )
    {
        return false;
    }
    
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    // cocos2d::Point origin = Director::getInstance()->getVisibleOrigin();
    
    Texture2D *texture = Director::getInstance()->getTextureCache()->addImage("camera_frame.png");
    const Texture2D::TexParams &tp = {GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_REPEAT};
    texture->setTexParameters(tp);
    auto background = Sprite::createWithTexture(texture, cocos2d::Rect(0, 0, visibleSize.width, visibleSize.height));
    background->setAnchorPoint(cocos2d::Point(0,0));
	background->setPosition(cocos2d::Point::ZERO);
    this->addChild(background);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
    UIViewController *viewController = nil;
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
    } else {
        viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
    }
    
//     NSLog(@"viewController screen frame : %@",NSStringFromCGRect(viewController.view.frame));
    if (type == camera_type_bg) {
        CamUIViewController *camUIViewController = [[CamUIViewController alloc] init];
//        [viewController presentViewController:camUIViewController animated:NO completion:nil];
        [viewController.view addSubview:camUIViewController.view];
        [camUIViewController release];
    } else {
        CharacterCamViewController *characterCamViewController = [[CharacterCamViewController alloc] init];
//        [viewController presentViewController:characterCamViewController animated:NO completion:nil];
        [viewController.view addSubview:characterCamViewController.view];
        [characterCamViewController release];
    }
#endif
#endif
    
//    auto btnBack = MenuItemImage::create("btn_map_back_normal.png",
//                                         "btn_map_back_press.png",
//                                         CC_CALLBACK_1(CameraScene::btnBackCallback, this));
//    btnBack->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//	btnBack->setPosition(cocos2d::Point::ZERO);
//    
//    auto menu = Menu::create(btnBack, NULL);
//    menu->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//    menu->setPosition(cocos2d::Point(CAMERA_BTN_BACK));
//    this->addChild(menu, 100);
    
    return true;
}

//// on "init" you need to initialize your instance
//bool CameraScene::init()
//{
//    //////////////////////////////
//    // 1. super init first
//    if ( !LayerColor::initWithColor( Color4B(255,255,255,255)) )
//    {
//        return false;
//    }
//    
//    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
//    // cocos2d::Point origin = Director::getInstance()->getVisibleOrigin();
//    
//    Texture2D *texture = Director::getInstance()->getTextureCache()->addImage("camera_frame.png");
//    const Texture2D::TexParams &tp = {GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_REPEAT};
//    texture->setTexParameters(tp);
//    auto background = Sprite::createWithTexture(texture, cocos2d::Rect(0, 0, visibleSize.width, visibleSize.height));
//    background->setAnchorPoint(cocos2d::Point(0,0));
//	background->setPosition(cocos2d::Point::ZERO);
//    this->addChild(background);
//    
//    auto btnBack = MenuItemImage::create("btn_map_back_normal.png",
//                                         "btn_map_back_press.png",
//                                         CC_CALLBACK_1(CameraScene::btnBackCallback, this));
//    btnBack->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//	btnBack->setPosition(cocos2d::Point::ZERO);
//
//    auto menu = Menu::create(btnBack, NULL);
//    menu->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//    menu->setPosition(cocos2d::Point(CAMERA_BTN_BACK));
//    this->addChild(menu, 100);
//    
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
//#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
//#ifndef MACOS
//    UIViewController *viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
//    NSLog(@"viewController screen frame : %@",NSStringFromCGRect(viewController.view.frame));
//    CamUIViewController *camUIViewController = [[CamUIViewController alloc] init];
//
//    [viewController.view addSubview:camUIViewController.view];
//#endif
//#endif
//
//    return true;
//}

void CustomCamera::btnBackCallback()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
    UIViewController *viewController = nil;
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
    } else {
        viewController = (UIViewController *)((AppController *)[UIApplication sharedApplication].delegate).viewController;
    }
    for (UIView *subView in [viewController.view subviews]) {
        [subView removeFromSuperview];
        subView = nil;
    }
#endif
#endif
    auto director = Director::getInstance();
    director->popScene();
//    auto homeScene = Home::createScene();
//    director->replaceScene(homeScene);
//    this->cleanup();
    if (_delegate != NULL) {
        _delegate->cameraScenePopped();
    }
}

void CustomCamera::customCharacterPhotoSaved(std::string baseCharacterKey, std::string mainPhotoName, std::string thumbnailPhotoName)
{
    unsigned long int timestamp= time(NULL);
    auto customCharacterKey = __String::createWithFormat("user_char_%ld", timestamp);
    auto customCharacterString = __String::createWithFormat("{\"base_char\" : \"%s\", \"main_photo_file\" : \"%s\", \"thumbnail_photo_file\" : \"%s\" }",
                                                            baseCharacterKey.c_str(), mainPhotoName.c_str(), thumbnailPhotoName.c_str());
    
    auto ud = UserDefault::getInstance();
    auto orderJsonString = ud->getStringForKey(ORDER_KEY, "");
    rapidjson::Document orderDocument;
    rapidjson::Document::AllocatorType& allocator = orderDocument.GetAllocator();
    orderDocument.Parse<0>(orderJsonString.c_str());
    rapidjson::Value& bookOrder = orderDocument["character_order"];

    // add to user order
    rapidjson::Value charKey;
    charKey.SetString(customCharacterKey->getCString());
    bookOrder.PushBack(charKey, allocator);
    
    rapidjson::StringBuffer strbuf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
    orderDocument.Accept(writer);
    auto resultOrderJsonString = strbuf.GetString();
    
    // save to user default
    ud->setStringForKey(ORDER_KEY, resultOrderJsonString);
    ud->setStringForKey(customCharacterKey->getCString(), customCharacterString->getCString());
    ud->flush();
    
    if (_delegate != NULL) {
        _delegate->characterPhotoSaved(customCharacterKey->getCString());
    }
}

void CustomCamera::customBgPhotoSaved(std::string baseBgKey, std::string mainPhotoName, std::string thumbnailPhotoName)
{
    unsigned long int timestamp= time(NULL);
    auto customBgKey = __String::createWithFormat("user_bg_%ld", timestamp);
    auto customBgString = __String::createWithFormat("{\"custom\" : true, \"main_photo_file\" : \"%s\", \"thumbnail_photo_file\" : \"%s\" }",
                                                            mainPhotoName.c_str(), thumbnailPhotoName.c_str());
    
    auto ud = UserDefault::getInstance();
    auto orderJsonString = ud->getStringForKey(ORDER_KEY, "");
    rapidjson::Document orderDocument;
    rapidjson::Document::AllocatorType& allocator = orderDocument.GetAllocator();
    orderDocument.Parse<0>(orderJsonString.c_str());
    rapidjson::Value& bookOrder = orderDocument["background_order"];
    
    // add to user order
    rapidjson::Value bgKey;
    bgKey.SetString(customBgKey->getCString());
    bookOrder.PushBack(bgKey, allocator);
    
    rapidjson::StringBuffer strbuf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
    orderDocument.Accept(writer);
    auto resultOrderJsonString = strbuf.GetString();
    
    // save to user default
    ud->setStringForKey(ORDER_KEY, resultOrderJsonString);
    ud->setStringForKey(customBgKey->getCString(), customBgString->getCString());
    ud->flush();
    
    if (_delegate != NULL) {
        _delegate->bgPhotoSaved(customBgKey->getCString());
    }
}
