//
//  Character.h
//  PixelCollision
//
//  Created by oasis on 2014. 5. 8..
//
//

#ifndef __PixelCollision__Character__
#define __PixelCollision__Character__

#include "cocos2d.h"
#include "bitarray.h"
#include <cocosbuilder/CocosBuilder.h>

USING_NS_CC;

#define DIRECTION_CHANGE_THRESHOLD          3.0
#define TAP_THRESHOLD                       10.0
#define LONG_PRESS_THRESHOLD_SEC            0.6
#define MOVE_STOP_THRESHOLD_SEC             0.5

enum class CharacterTag
{
    TagFrontHead            = 1,
    TagFrontBody            = 2,
    TagFrontLeftArm         = 3,
    TagFrontRightArm        = 4,
    TagFrontLeftLeg         = 5,
    TagFrontRightLeg        = 6,
    TagFrontTail            = 7,

    TagSideHead             = 8,
    TagSideBody             = 9,
    TagSideLeftArm          = 10,
    TagSideRightArm         = 11,
    TagSideLeftLeg          = 12,
    TagSideRightLeg         = 13,
    TagSideTail             = 14,
    
    TagFrontItem1           = 15,
    TagFrontItem2           = 16,
    
    TagSideItem1            = 17,
    TagSideItem2            = 18
};

enum class CharacterDirection {
    Left,
    Front,
    Right
};

enum class CharacterPressStatus {
    None,
    Pressed,
    Tap,
    LongPressed
};

enum class TouchStatus {
    Down,
    Move,
    Up,
    Cancel
};

class Character : public cocos2d::Sprite, cocosbuilder::CCBAnimationManagerDelegate
{
public:
    int touchNumber;
    int characterID;
    
    
    bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event  *event);
    void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event  *event);
    void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event  *event);
    void onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event  *event);
    
    static Character* create(const char* charKey);
    static Character* create(const char* charKey, uint8_t alphaThreshold);
    
//    static Character* create(const char* ccbiFileName, const std::string& filename);
//    static Character* create(const char* ccbiFileName, const std::string& filename, uint8_t alphaThreshold);
//    
//    static Character* createWithCcbi(const char* ccbiFileName, const std::string& charName);
//    static Character* createWithCcbi(const char* ccbiFileName, const std::string& charName, uint8_t alphaThreshold);
    
	bool pixelMaskBitAt(cocos2d::Point point);
    bool pixelMaskContainsPoint(cocos2d::Point point);
    bool pixelMaskContainsPointInChild(cocos2d::Node *node, cocos2d::Point point);

    void completedAnimationSequenceNamed(const char *name);

    void showExpansionEffect(bool show);

    virtual bool containsPoint(cocos2d::Point point);
#ifndef MACOS
    virtual bool containsTouch(cocos2d::Touch* touch);
#endif

    virtual void setPosition(const cocos2d::Point& position);
    virtual void setPosition(float x, float y);

    void clearTouch();
    
    void lockMultiTouch() { isMultiTouchLocked = true; };
    void unlockMultiTouch() { isMultiTouchLocked = false; };

    void runAnimationByDirection();

    void lockInScreen() { lockedInScreen = true; };
    void unlockInScreen() { lockedInScreen = false; };
    
    void setDirection(CharacterDirection direction);
    CharacterDirection getDirection() { return currentDirection; };

    void setPressStatus(CharacterPressStatus pressStatus);
    CharacterPressStatus getPressStatus() { return pressStatus; };

    bool isCustom() { return _isCustom; } ;

    void enableExpansionEffect(bool enable) { isEnableExpansionEffect = enable; };
    
    void pause();
    void resume();
    bool isPaused() { return paused; };
    std::string getCharName() { return charName; };
    std::string getCharKey() { return charKey; };
    std::string getIndicatorFileName() { return indicatorFileName; };

    cocos2d::Node *characterObj;
private:
    float lastDistance;
    Vector<Touch*> touches;
    bool secondTouchBegin;
    uint8_t alphaThreshold;
    std::string charName;
    std::string charKey;
    std::string indicatorFileName;
    std::string moveSoundFileName;
    std::string tapSoundFileName;

    bool isEnableExpansionEffect;
    
    bool _isCustom;
    std::string photoFilePath;
    
    std::string pausedAnimation;
    bool pausedExpansion;
    
    CharacterDirection currentDirection;
    CharacterPressStatus pressStatus;
    
    float scaleStartDistance;
    
    bool paused;
    bool isMoving;
    bool isExpansionEffectIsOn;
    bool moveByRealTouch;
    bool moveSoundAllDirection;
    bool flipOnMove;
    
    bool isMultiTouchLocked;
    bool moveSoundPlaying;
    float oldScale;
    unsigned int soundID;
    
    bool lockedInScreen;
    
    cocos2d::Action *longPressTriggerAction;
    cocos2d::Action *stopMoveTriggerAction;
    cocos2d::Action *expansionAction;

#if USE_BITARRAY
	bit_array_t* pixelMask;
#else
	unsigned char* pixelMask;
#endif
	int pixelMaskWidth;
	int pixelMaskHeight;
	int pixelMaskSize;
	float pixelMaskResolutionFactor;
    
//    int isPowerOfTwo(unsigned int x);
    float distanceBetweenPoints(cocos2d::Point p1, cocos2d::Point p2);

    std::string getResourceName(const std::string& characterName, int tag);
    std::string getItemResourceName(const std::string& characterName, int tag);
    
    bool initWithCCbi(const char* ccbiFileName, const char* charKey, const std::string& charName, const std::string& photoFile, const std::string& indicator,
                      bool isCustom, bool flipOnMove, bool moveSoundAllDirection, const std::string& moveSoundFile, const std::string& tapSoundFile, GLubyte alphaThreshold);

    bool initWithFile(const std::string& filename);
    void initWithImage(cocos2d::Image *image, GLubyte alphaThreshold);

//    Sprite* initWithFile(const std::string& filename, GLubyte alphaThreshold);
//    Sprite* initWithFile(const char* ccbiFileName, const std::string& filename, GLubyte alphaThreshold);
    
    bool isPlayingEffect();
    void playEffect();
    void stopEffect();
    
    void snapRotation();
    
    void pauseAllChild(Node *node);
    void resumeAllChild(Node *node);

    void startLongPressTrigger();
    void cancelLongPressTrigger();
    void longPressed();
    void pressed();
    
    void startHorizontalMove();
    void startVerticalMove();
    void stopMove();
};

#endif /* defined(__PixelCollision__Character__) */
