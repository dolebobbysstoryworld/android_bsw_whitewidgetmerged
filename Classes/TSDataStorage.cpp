//
//  TSDataStorage.cpp
//  bobby
//
//  Created by Lee mu hyeon on 2014. 8. 25..
//
//

#include "TSDataStorage.h"
#include "Define.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "AppDelegate.h"
#endif

USING_NS_CC;

#define TAG_NET_RENEWAUTH           8760001


static TSDataStorage *g_dataStorage = nullptr;

TSDataStorage *TSDataStorage::getInstance() {
    if(g_dataStorage) {
        return g_dataStorage;
    }
    g_dataStorage = new TSDataStorage();
    return g_dataStorage;
}

TSDataStorage::TSDataStorage() {
    _reNewAuthKey = new TSDoleGetReNewAuthKey();
    _reNewAuthKey->setDelegate(this);
    _reNewAuthKey->setTag(TAG_NET_RENEWAUTH);
    _reNewAuthKey->init(UserDefault::getInstance()->getIntegerForKey(UDKEY_INTEGER_USERNO), UserDefault::getInstance()->getStringForKey(UDKEY_STRING_AUTHKEY));
    _reNewAuthKey->request();
}

TSDataStorage::~TSDataStorage() {
    
}

void TSDataStorage::onRequestStarted(TSDoleProtocol *protocol, TSNetResult result) {
    
}

void TSDataStorage::onResponseEnded(TSDoleProtocol *protocol, TSNetResult result) {
    switch (result) {
        case TSNetResult::Success : {
            switch (protocol->getTag()) {
                case TAG_NET_RENEWAUTH: {
                    //AuthKey가 갱신 되었으니 Coin을 얻고 아이템 진열 버전 정보를 가져온다.
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
                    AppDelegate* app = (AppDelegate*)Application::getInstance();
                    app->updateAuthKey(UserDefault::getInstance()->getStringForKey(UDKEY_STRING_AUTHKEY).c_str());
#endif
//                    this->doleCoinGetBalanceProtocol();
//                    this->doleCacheSyncVersionProtocol();//TODO: tuilise:동시 호출이 문제가 없는지 확인 필요. shared instance 구조에서 class instance 구조로 나중에 변
//                    this->getUpdateVersion();
                } break;
                default:
                    break;
            }
        } break;
        case TSNetResult::ErrorTimeout: {
            switch (protocol->getTag()) {
                default:
//                    TSNetToastLayer::show(this);
                    break;
            }
        } break;
        default:
            break;
    }

}

void TSDataStorage::onProtocolFailed(TSDoleProtocol *protocol, TSNetResult result) {
    
}

//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
void TSDataStorage::onRequestCanceled(TSDoleProtocol *protocol) {

}
//#endif