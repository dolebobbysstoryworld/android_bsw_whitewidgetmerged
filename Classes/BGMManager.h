//
//  BGMManager.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 9. 24..
//
//

#ifndef _BGMMANAGER_H_
#define _BGMMANAGER_H_

#include "cocos2d.h"

NS_CC_BEGIN

class TSBGMManager : public Ref {
protected:
    TSBGMManager();
    virtual ~TSBGMManager() {} ;

public:
    static TSBGMManager *getInstance();
    
    void play();
    void stop();
    void pause();
    void resume();
    void setMute(bool mute);

private:
    bool _isPlaying;
    bool _isPaused;
    bool _isMute;
};

NS_CC_END

#endif /* defined(_BGMMANAGER_H_) */
