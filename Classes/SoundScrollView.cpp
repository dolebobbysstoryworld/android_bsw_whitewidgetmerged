//
//  SoundScrollView.cpp
//  bobby
//
//  Created by oasis on 2014. 7. 23..
//
//

#include "SoundScrollView.h"
#include "ResourceManager.h"
#include "Define.h"
#include "Util.h"

#include "external/json/document.h"
#include "external/json/prettywriter.h"
#include "external/json/stringbuffer.h"

#define SOUND_BASE_TAG                  20000
#define LINE_BASE_TAG                       30000

#define TAG_FRAME_SOUND            300
#define TAG_SOUND_SELECT_COIN      400

#define SOUND_ANCHOR_Y                  0.95
#define DELETE_SOUND_POPUP           0

#define MAX_ROTATION        5.0f

#define WHITESPACE_CHAR_DEFAULT     0

using namespace rapidjson;

bool SoundScrollView::init()
{
    if (Layer::init()) {
        
        linesCount = 0;
        
        currentSoundAngle = 0.0f;
        scrollViewPressed = false;
        reachedToEnd = false;
        
        soundList = __Array::create();
        soundList->retain();
        
//        deleteButtons = __Array::create();
//        deleteButtons->retain();
        
        items = __Dictionary::create();
        items->retain();
        
        pins = __Array::create();
        pins->retain();
        
        this->scrollView = this->createScrollView();
        this->loadJsonToScrollView(this->scrollView);
        
        this->setAnchorPoint(this->scrollView->getAnchorPoint());
        this->setContentSize(this->scrollView->getViewSize());
        this->setPosition(this->scrollView->getPosition());
        this->addChild(this->scrollView, 0);
        
        this->setSelectSound(0);
        
        scrollOffset = cocos2d::Point::ZERO;
        // TODO: create menus
        return true;
    }
    return false;
}

ScrollViewExtension *SoundScrollView::createScrollView()
{
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    
    ScrollViewExtension *newScrollView = ScrollViewExtension::create(cocos2d::Size(visibleSize.width, HOME_BACKGROUND_SCROLL_VIEW_HEIGHT), nullptr);
    newScrollView->setBounceable(false);
    newScrollView->setAnchorPoint(cocos2d::Point(0, 0));
    newScrollView->setTag(TAG_SCROLLVIEW);
    newScrollView->setExtensionDelegate(this);
    newScrollView->setBounceable(true);

    return newScrollView;
}

//void SoundScrollView::soundItemCoinClicked(Ref* pSender)
//{
//    log("coin clicked");
//}

void SoundScrollView::rotateSounds(float duration, float angle, bool animation)
{
    auto container = scrollView->getChildren().at(0);
    auto soundCount = soundList->count();
    currentSoundAngle = angle;
    
    for (int index = 0; index < soundCount ; index++) {
        Sprite *frame = (Sprite *)(container->getChildByTag(SOUND_BASE_TAG+index));
        
        if (frame == NULL) continue;
        
        changePositionAtAnchor(frame, cocos2d::Point(0.5, SOUND_ANCHOR_Y));
        
        if (animation) {
            auto rotateAction = RotateTo::create(duration, angle);
            auto rotateReverseAction = RotateTo::create(duration, 0);
            auto frameSequence = Sequence::create(rotateAction, rotateReverseAction, NULL);
            frame->runAction(frameSequence->clone());
        } else {
            frame->setRotation(angle);
        }
    }
}

void SoundScrollView::scrollViewDidScroll(ScrollViewExtension* view)
{
    auto oldOffset = scrollOffset.x;
    scrollOffset = view->getContentOffset();
    
    float rotationDuration = 0.1f;
    int direction = 0;
    
    if ( view->getContentOffset().x == view->minContainerOffset().x ){
        if (oldOffset == view->minContainerOffset().x){
            if (view->isDragging()){
                if (!reachedToEnd) {
                    reachedToEnd = true;
                    return;
                }
                direction = -1;
            } else {
                reachedToEnd = false;
            }
        }
    } else if (view->getContentOffset().x == view->maxContainerOffset().x) {
        if (oldOffset == view->maxContainerOffset().x){
            if (view->isDragging()){
                if (!reachedToEnd) {
                    reachedToEnd = true;
                    return;
                }
                direction = 1;
            } else {
                reachedToEnd = false;
            }
        }
        isScrollingToFirst = false;
    } else {
        if((oldOffset-view->getContentOffset().x) < 0) {
            direction = 1;
        } else {
            direction = -1;
        }
    }
    
    if (view->isDragging() && !scrollViewPressed) {
        scrollViewPressed = true;
        currentSoundAngle = 0; // clear old angle
    }
    
    float nextAngle = currentSoundAngle;
    if (direction == 1) {
        if (currentSoundAngle < MAX_ROTATION) {
            nextAngle = currentSoundAngle + 0.5f;
        }
    } else {
        if (currentSoundAngle > -MAX_ROTATION) {
            nextAngle = currentSoundAngle - 0.5f;
        }
    }
    
    if (view->isDragging()){
        this->rotateSounds(rotationDuration, nextAngle, false);
    } else {
        this->rotateSounds(rotationDuration, nextAngle, true);
        scrollViewPressed = false;
    }
}

void SoundScrollView::scrollViewDidZoom(ScrollViewExtension* view)
{
    
}

void SoundScrollView::scrollViewDidScrollEnd(ScrollViewExtension* view)
{
    isScrollingToFirst = false;
}

//void SoundScrollView::popupButtonClicked(PopupLayer *popup, cocos2d::Ref *obj)
//{
//    Director::getInstance()->resume();
//    auto clickedButton = (MenuItem*)obj;
//    int tag = clickedButton->getTag();
//    
//    if (popup->getTag() == DELETE_SOUND_POPUP) {
//        if (tag == 0) {
//            log("delete cancelButton clicked!!");
//        } else {
//            log("delete okButton clicked!!");
//            float scaleDuration = 0.2f;
//            float shrinkRatio = 0.97;
//            auto scaleAction = ScaleTo::create(scaleDuration, shrinkRatio);
//            auto reverse = ScaleTo::create(scaleDuration, 1.0f);
//            
//            auto scaleCallback = CallFunc::create([&](){
//                log("selectedCharKey popup :%s", selectedSoundItemKey->getCString());
//                this->runRemoveSoundAnimation(selectedSoundItemKey);
//                auto runningScene = Director::getInstance()->getRunningScene();
//                if (runningScene->getTag() == TAG_RECORDING_SCENE) {
//                    __NotificationCenter::getInstance()->postNotification("deleteSoundItemItemInRecordScene");
//                }
//            });
//            auto scaleSequence = Sequence::create(scaleAction, reverse, scaleCallback, NULL);
//            selDeleteSoundItem->runAction(scaleSequence);
//        }
//    } else {
//        if (tag == 0) {
//            log("cancelButton clicked!");
//        } else {
//            log("okButton clicked!!");
//        }
//    }
//}

void SoundScrollView::onSelectItem(ScrollViewExtension* view, Node* selectedItem, Touch *pTouch, Event *pEvent)
{
    log("onselectitem");
    auto sound = (Sprite *)selectedItem;
    log("getTag : %i", sound->getTag());
    float scaleDuration = 0.05f;
    float shrinkRatio = 0.97;
    auto scaleAction = ScaleTo::create(scaleDuration, shrinkRatio);
    auto reverse = ScaleTo::create(scaleDuration, 1.0f);
    auto afterCallBack = CallFuncN::create(CC_CALLBACK_1(SoundScrollView::soundItemPressedCallback, this));
    auto actions = Sequence::create(scaleAction, reverse, afterCallBack, NULL);
    sound->runAction(actions);
}

void SoundScrollView::soundItemPressedCallback(Ref* pSender)
{
    Sprite *soundItem = (Sprite *)pSender;
    auto index = soundItem->getTag() % SOUND_BASE_TAG;
    log("soundItem pressed : %d", index);
    auto soundItemKey = (__String*)soundList->getObjectAtIndex(index);
    this->setSelectSound(index);
    
    if (_scrollViewDelegate) {
        _scrollViewDelegate->onSelectSound(this, soundItem, soundItemKey, index);
    }
}

void SoundScrollView::setSelectSound(int index)
{
    if (frameSelectedBorder != NULL) {
        frameSelectedBorder->removeFromParentAndCleanup(false);
    }
    
    auto soundItemKey = (__String*)soundList->getObjectAtIndex(index);
    auto ud = UserDefault::getInstance();
    auto soundJsonString = ud->getStringForKey(soundItemKey->getCString());
    rapidjson::Document sound;
    sound.Parse<0>(soundJsonString.c_str());
    
    auto soundSelectFileName = sound["filename_select"].GetString();
    
    frameSelectedBorder = Sprite::create(soundSelectFileName);
    frameSelectedBorder->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    frameSelectedBorder->setPosition(cocos2d::Point::ZERO);
    
//    if (frameSelectedBorder == NULL) {
//        frameSelectedBorder = Sprite::create("background_frame_select.png");
//        frameSelectedBorder->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//        frameSelectedBorder->setPosition(cocos2d::Point::ZERO);
//        
//        auto frameCheck = Sprite::create("background_frame_check.png");
//        frameCheck->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//        frameCheck->setPosition(cocos2d::Point((frameSelectedBorder->getContentSize().width - frameCheck->getContentSize().width)/2,
//                                               RECORDING_BOTTOM_SCROLL_VIEW_CENTER_ICON_BOTTOM_PADDING));
//        frameSelectedBorder->addChild(frameCheck);
//        frameSelectedBorder->retain();
//    } else {
//        frameSelectedBorder->removeFromParentAndCleanup(false);
//    }
    
    auto targetFrame = scrollView->getContainer()->getChildByTag(SOUND_BASE_TAG + index);
    auto frameSoundItem = targetFrame->getChildByTag(TAG_FRAME_SOUND);
    frameSoundItem->addChild(frameSelectedBorder, 1);
}

int SoundScrollView::getSoundIndex(std::string& soundItemKey)
{
    int result = CC_INVALID_INDEX;
    for (int index = 0; index <soundList->count(); index++){
        auto sKey = (__String *)soundList->getObjectAtIndex(index);
        if (soundItemKey.compare(sKey->getCString())== 0) {
            result = index;
            break;
        }
    }
    return result;
}

const char *SoundScrollView::loadLocalizedSoundName(std::string soundItemKey) {
    if (strcmp(soundItemKey.c_str(), "S000") == 0 ) {
        return Device::getResString(Res::Strings::CC_LIST_MUSIC_TITLE_NONE);
    } else if (strcmp(soundItemKey.c_str(), "S001") == 0 ) {
        return Device::getResString(Res::Strings::CC_LIST_MUSIC_TITLE_JOLLY);
    } else if (strcmp(soundItemKey.c_str(), "S002") == 0 ) {
        return Device::getResString(Res::Strings::CC_LIST_MUSIC_TITLE_DREAM);
    } else if (strcmp(soundItemKey.c_str(), "S003") == 0 ) {
        return Device::getResString(Res::Strings::CC_LIST_MUSIC_TITLE_BRAVE);
    } else if (strcmp(soundItemKey.c_str(), "S004") == 0 ) {
        return Device::getResString(Res::Strings::CC_LIST_MUSIC_TITLE_SHINE);
    } else if (strcmp(soundItemKey.c_str(), "S005") == 0 ) {
        return Device::getResString(Res::Strings::CC_LIST_MUSIC_TITLE_BRISK);
    } else if (strcmp(soundItemKey.c_str(), "S006") == 0 ) {
        return Device::getResString(Res::Strings::CC_LIST_MUSIC_TITLE_ANGRY);
    } else if (strcmp(soundItemKey.c_str(), "S007") == 0 ) {
        return Device::getResString(Res::Strings::CC_LIST_MUSIC_TITLE_SLEEPY);
    } else if (strcmp(soundItemKey.c_str(), "S008") == 0 ) {
        return Device::getResString(Res::Strings::CC_LIST_MUSIC_TITLE_ADVENTURE);
    } else if (strcmp(soundItemKey.c_str(), "S009") == 0 ) {
        return Device::getResString(Res::Strings::CC_LIST_MUSIC_TITLE_CRUISE);
    } else if (strcmp(soundItemKey.c_str(), "S010") == 0 ) {
        return Device::getResString(Res::Strings::CC_LIST_MUSIC_TITLE_JUNGLE);
    }
    return "sound";
}

cocos2d::Node* SoundScrollView::createSoundForScrollView(std::string soundItemKey, int index)
{
    auto ud = UserDefault::getInstance();
    auto soundJsonString = ud->getStringForKey(soundItemKey.c_str());
    rapidjson::Document sound;
    sound.Parse<0>(soundJsonString.c_str());
    
    auto frame = Sprite::create();
    auto itemContentSize = HOME_BOTTOM_SCROLL_VIEW_MUSIC_ITEM_HOLDER_SIZE;
    frame->setContentSize(itemContentSize);
    frame->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    frame->setColor(Color3B(222,211,11));
    
    auto soundFileName = sound["filename"].GetString();
    
    auto frameSoundItem = Sprite::create(soundFileName);
    frameSoundItem->setPosition(cocos2d::Point(itemContentSize.width/2, itemContentSize.height));
    frameSoundItem->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_TOP);
    frameSoundItem->setTag(TAG_FRAME_SOUND);
    
    auto frameContentsSize = frameSoundItem->getContentSize();
    frame->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    frame->setContentSize(frameContentsSize);
    
    float bottomPadding = 0;
    if((index%3 == 0)) {
        bottomPadding = HOME_BOTTOM_SCROLL_VIEW_MUSIC_ITEM_BOTTOM_PADDING1;
    } else if((index%3 == 1)) {
        bottomPadding = HOME_BOTTOM_SCROLL_VIEW_MUSIC_ITEM_BOTTOM_PADDING2;
    } else if ((index%3 == 2)){
        bottomPadding = HOME_BOTTOM_SCROLL_VIEW_MUSIC_ITEM_BOTTOM_PADDING3;
    }
    
    cocos2d::Point currentPoint = cocos2d::Point(HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_START_PADDING + frameContentsSize.width*index, bottomPadding);
    frame->setPosition(currentPoint);
    frame->addChild(frameSoundItem);
    
    auto soundName = this->loadLocalizedSoundName(soundItemKey);//sound["name"].GetString();
    rapidjson::Value& labelColor = sound["font_color"];
    
    auto soundLabel = Label::createWithSystemFont(soundName, FONT_NAME, RECORDING_SOUND_SCROLL_LABEL_FONT_SIZE,RECORDING_SOUND_SCROLL_LABEL_SIZE,TextHAlignment::CENTER, TextVAlignment::CENTER);
    soundLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    soundLabel->setColor(Color3B(labelColor[SizeType(0)].GetInt(),
                                labelColor[SizeType(1)].GetInt(),
                                labelColor[SizeType(2)].GetInt()));
    soundLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    soundLabel->enableShadow(Color4B(142,111,72,255),RECORDING_SOUND_SCROLL_LABEL_SHODOW, 0);
    soundLabel->setPosition(RECORDING_SOUND_SCROLL_LABEL);
    frame->addChild(soundLabel);
    
    auto soundKey = soundItemKey.c_str();
    
    auto soundKeyStr = __String::create(soundKey);
//    log("charKey  - %s", soundKeyStr->getCString());
    auto soundKeyClone = soundKeyStr->clone();
    soundKeyClone->retain();
    frame->setUserData(soundKeyClone);
    soundList->addObject(__String::create(soundKey));
    return frame;
//    
//    bool isLock = sound["locked"].GetBool();
//    
//    if (isLock) {
//        int price = sound["price"].GetInt();
//        auto lock = Sprite::create("background_frame_lock.png");
//        lock->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//        lock->setPosition(cocos2d::Point(frameSoundItem->getContentSize().width/2-lock->getContentSize().width/2, RECORDING_BOTTOM_SCROLL_VIEW_CENTER_ICON_BOTTOM_PADDING));
//        frameSoundItem->addChild(lock, 1);
//        
//        auto coinBtn = cocos2d::ui::Button::create("background_frame_dolecoin.png", "background_frame_dolecoin.png");
//        coinBtn->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//        coinBtn->setPosition(RECORDING_BOTTOM_SCROLL_VIEW_COIN);
//        coinBtn->addTouchEventListener(this, (cocos2d::ui::SEL_TouchEvent)(&SoundScrollView::soundItemCoinClicked));
//        frameSoundItem->addChild(coinBtn, 2);
//        
//        std::stringstream ss;
//        ss << price;
//        
//        auto coinText = ss.str();
//        auto coinFontSize = coinText.length() == 1 ? RECORDING_BOTTOM_SCROLL_VIEW_COIN_FONT_SIZE_DIGIT1 :
//        coinText.length() == 2 ? RECORDING_BOTTOM_SCROLL_VIEW_COIN_FONT_SIZE_DIGIT2 : RECORDING_BOTTOM_SCROLL_VIEW_COIN_FONT_SIZE_DIGIT3;
//        auto coinNum = Label::createWithSystemFont(coinText, FONT_NAME_BOLD, coinFontSize);
//        coinNum->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::CENTER);
//        coinNum->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
//        coinNum->setColor(Color3B(0xff,0x54,0x11));
//        coinNum->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
//        coinNum->enableShadow(Color4B(255,255,255,255.0f*0.51f),cocos2d::Size(1.0,-0.866), 0);
//        coinNum->setPosition(cocos2d::Point(coinBtn->getContentSize().width/2,
//                                            coinBtn->getContentSize().height - RECORDING_BOTTOM_SCROLL_VIEW_COIN_VISIBLE_HEIGHT+RECORDING_BOTTOM_SCROLL_VIEW_COIN_VISIBLE_HEIGHT/2));
//        coinBtn->addChild(coinNum);
//    }
//    
//    if (!sound["custom"].GetBool()){
//
//        auto soundFileName = sound["filename"].GetString();
//
//        auto frameSoundItem = Sprite::create(soundFileName);
//        frameSoundItem->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//        frameSoundItem->setPosition(cocos2d::Point::ZERO);
//        frameSoundItem->setTag(TAG_FRAME_SOUND);
//        
//        auto frameContentsSize = frameSoundItem->getContentSize();
//        frame->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//        frame->setContentSize(frameContentsSize);
//        
//        float bottomPadding = 0;
//        if((index%2 == 1)) {
//            bottomPadding = RECORDING_BOTTOM_SCROLL_VIEW_FRAME_BOTTOM_PADDING;
//        }
//        
//        cocos2d::Point currentPoint = cocos2d::Point(HOME_BACKGROUND_SCROLL_VIEW_STARTPADDING + frameContentsSize.width*index, bottomPadding);
//        frame->setPosition(currentPoint);
//        frame->addChild(frameSoundItem);
//        
//        auto soundKey = soundItemKey.c_str();
//        
//        auto soundKeyStr = __String::create(soundKey);
//        log("charKey  - %s", soundKeyStr->getCString());
//        auto soundKeyClone = soundKeyStr->clone();
//        soundKeyClone->retain();
//        frame->setUserData(soundKeyClone);
//        soundList->addObject(__String::create(soundKey));
//        
//        bool isLock = sound["locked"].GetBool();
//        
//        if (isLock) {
//            int price = sound["price"].GetInt();
//            auto lock = Sprite::create("background_frame_lock.png");
//            lock->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//            lock->setPosition(cocos2d::Point(frameSoundItem->getContentSize().width/2-lock->getContentSize().width/2, RECORDING_BOTTOM_SCROLL_VIEW_CENTER_ICON_BOTTOM_PADDING));
//            frameSoundItem->addChild(lock, 1);
//            
//            auto coinBtn = cocos2d::ui::Button::create("background_frame_dolecoin.png", "background_frame_dolecoin.png");
//            coinBtn->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//            coinBtn->setPosition(RECORDING_BOTTOM_SCROLL_VIEW_COIN);
//            coinBtn->addTouchEventListener(this, (cocos2d::ui::SEL_TouchEvent)(&SoundScrollView::soundItemCoinClicked));
//            frameSoundItem->addChild(coinBtn, 2);
//            
//            std::stringstream ss;
//            ss << price;
//            
//            auto coinText = ss.str();
//            auto coinFontSize = coinText.length() == 1 ? RECORDING_BOTTOM_SCROLL_VIEW_COIN_FONT_SIZE_DIGIT1 :
//            coinText.length() == 2 ? RECORDING_BOTTOM_SCROLL_VIEW_COIN_FONT_SIZE_DIGIT2 : RECORDING_BOTTOM_SCROLL_VIEW_COIN_FONT_SIZE_DIGIT3;
//            auto coinNum = Label::createWithSystemFont(coinText, FONT_NAME_BOLD, coinFontSize);
//            coinNum->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::CENTER);
//            coinNum->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
//            coinNum->setColor(Color3B(0xff,0x54,0x11));
//            coinNum->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
//            coinNum->enableShadow(Color4B(255,255,255,255.0f*0.51f),cocos2d::Size(1.0,-0.866), 0);
//            coinNum->setPosition(cocos2d::Point(coinBtn->getContentSize().width/2,
//                                                coinBtn->getContentSize().height - RECORDING_BOTTOM_SCROLL_VIEW_COIN_VISIBLE_HEIGHT+RECORDING_BOTTOM_SCROLL_VIEW_COIN_VISIBLE_HEIGHT/2));
//            coinBtn->addChild(coinNum);
//        }
//    } else {
//        auto thumbnail = (char *)sound["thumbnail_photo_file"].GetString();
//        auto thumbnailImage = __String::create(thumbnail);
//        
//        if (thumbnailImage != nullptr) {
//            std::string photoType("soundItem");
//            std::string path = Util::getThumbnailPhotoPath(photoType);
//            path.append(thumbnailImage->getCString());
//            log("path : %s", path.c_str());
//            if (cocos2d::FileUtils::getInstance()->isFileExist(path) ){
//                cocos2d::Data imageData = cocos2d::FileUtils::getInstance()->getDataFromFile(path.c_str());
//                unsigned long nSize = imageData.getSize();
//                
//                cocos2d::Image *image = new cocos2d::Image();
//                image->initWithImageData(imageData.getBytes(), nSize);
//                
//                cocos2d::Texture2D *texture = new cocos2d::Texture2D();
//                texture->initWithImage(image);
//                
//                cocos2d::Point imageStartPos = cocos2d::Point(0,0);
//                
//                auto customPhoto = Sprite::createWithTexture(texture);
//                customPhoto->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//                customPhoto->setPosition(cocos2d::Point(HOME_BOTTOM_SCROLL_VIEW_FRAME_SIZE.width/2-HOME_CUSTOM_BG_SIZE.width/2, HOME_CUSTOM_BG_BOTTOM_PADDING));
//                
//                auto frameSoundItem = Sprite::create("home_background_frame_custom.png");
//                frameSoundItem->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//                frameSoundItem->setPosition(cocos2d::Point::ZERO);
//                frameSoundItem->setTag(TAG_FRAME_SOUND);
//                
//                frameSoundItem->addChild(customPhoto);
//                
//                auto frameContentsSize = frameSoundItem->getContentSize();
//                frame->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//                frame->setContentSize(frameContentsSize);
//                
//                float bottomPadding = 0;
//                if((index%2 == 1)) {
//                    bottomPadding = RECORDING_BOTTOM_SCROLL_VIEW_FRAME_BOTTOM_PADDING;
//                }
//                
//                cocos2d::Point currentPoint = cocos2d::Point(HOME_BACKGROUND_SCROLL_VIEW_STARTPADDING + frameContentsSize.width*index, bottomPadding);
//                frame->setPosition(currentPoint);
//                frame->addChild(frameSoundItem);
//                
//                auto soundKey = soundItemKey.c_str();
//                
//                auto soundKeyStr = __String::create(soundKey);
//                log("charKey  - %s", soundKeyStr->getCString());
//                auto soundKeyClone = soundKeyStr->clone();
//                soundKeyClone->retain();
//                frame->setUserData(soundKeyClone);
//                soundList->addObject(__String::create(soundKey));
//                
////                auto deleteButton = cocos2d::ui::Button::create("btn_home_book_story_delete_normal.png", "btn_home_book_story_delete_normal.png");
////                deleteButton->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
////                deleteButton->setPosition(cocos2d::Point(HOME_BOTTOM_SCROLL_VIEW_FRAME_SIZE.width - (HOME_BOTTOM_SCROLL_VIEW_FRAME_SIZE.width-HOME_CUSTOM_BG_SIZE.width)/2 -deleteButton->getContentSize().width/2, HOME_CUSTOM_BG_BOTTOM_PADDING-deleteButton->getContentSize().height/2));
////                deleteButton->addTouchEventListener(this, (cocos2d::ui::SEL_TouchEvent)(&SoundScrollView::deleteButtonClicked));
////                deleteButton->setUserData(soundList->getObjectAtIndex(index));
////                frame->addChild(deleteButton, 2);
////                deleteButtons->addObject(deleteButton);
//                
//            } else {
//                log("error! no file : %s", path.c_str());
//            }
//        }
//        
//    }
}

//void SoundScrollView::deleteButtonClicked(Ref *obj, ui::TouchEventType type)
//{
//    switch (type) {
//        case ui::TOUCH_EVENT_ENDED: {
//            auto selDelButton = ((ui::Button*)obj);
//            selectedSoundItemKey = (__String *)selDelButton->getUserData();
//            auto holderNode = (Node *)selDelButton->getParent();
//            selDeleteSoundItem = holderNode;
//            
//            Director::getInstance()->pause();
//            PopupLayer *modal = PopupLayer::create(POPUP_TYPE_WARNING, "Selected background will be deleted. Deleted records can't be restored.", "Cancel", "OK");
//            
//            modal->setTag(DELETE_SOUND_POPUP);
//            modal->setDelegate(this);
//            auto runningScene = Director::getInstance()->getRunningScene();
//            runningScene->addChild(modal, 1000);
//        } break;
//        case ui::TOUCH_EVENT_BEGAN: break;
//        case ui::TOUCH_EVENT_MOVED: break;
//        case ui::TOUCH_EVENT_CANCELED: break;
//        default: break;
//    }
//}

//bool SoundScrollView::addSound(std::string soundItemKey)
//{
//    log("add soundItem %s", soundItemKey.c_str());
//    int index = soundList->count();
//    auto item = createSoundForScrollView(soundItemKey, index);
//    scrollView->addChild(item, TOUCHABLE_LAYER, SOUND_BASE_TAG+index);
//    item->setScale(0.0f);
//    
//    auto spriteName = __String::createWithFormat("home_background_frame_pin_%02d.png", index%4+1);
//    auto pin = Sprite::create(spriteName->getCString());
//    pin->setAnchorPoint(cocos2d::Point::ANCHOR_TOP_LEFT);
//    cocos2d::Point pos = cocos2d::Point(item->getPositionX() + (HOME_BOTTOM_SCROLL_VIEW_FRAME_SIZE.width - pin->getContentSize().width)/2,
//                                        HOME_BOTTOM_SCROLL_VIEW_FRAME_SIZE.height + item->getPositionY() + HOME_BOTTOM_SCROLL_VIEW_PIN_TOP_PADDING);
//    pin->setPosition(pos);
//    scrollView->addChild(pin, TOUCHABLE_LAYER+1);
//    
//    __String *soundItemKeyClone = __String::create(soundItemKey)->clone();
//    soundItemKeyClone->retain();
//    
//    log("soundList : %zi", soundList->count());
//    items->setObject(item, soundItemKeyClone->getCString());
//    log("items :%i", items->count());
//    pins->addObject(pin);
//    
//    float moveDuration = 0.25f;
//    this->scrollView->setTouchEnabled(false);
//    
//    auto newCharacterCallback = CallFunc::create([&](){
//        this->scrollView->setTouchEnabled(true);
//    });
//    
//    // scale animation
//    auto startDelay = DelayTime::create(moveDuration);
//    auto scaleTo = ScaleTo::create(moveDuration, 1.0f);
//    auto appearAction = Sequence::create(startDelay, scaleTo, newCharacterCallback, NULL);
//    item->runAction(appearAction);
//    
//    this->adjustLine();
//    
//    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
//    cocos2d::Point scrollOffset = scrollView->getContentOffset();
//    this->scrollView->setContentOffsetInDuration(cocos2d::Point(-(scrollView->getContentSize().width-visibleSize.width), scrollOffset.y), 0.25f);
//    
//    return true;
//}

//bool SoundScrollView::removeSound(__String *soundItemKey)
//{
//    log("soundItemKey : %s", soundItemKey->getCString());
//    auto ud = UserDefault::getInstance();
//    
//    auto orderJsonString = ud->getStringForKey(ORDER_KEY, "");
//    rapidjson::Document orderDocument;
//    orderDocument.Parse<0>(orderJsonString.c_str());
//    rapidjson::Value& itemOrder = orderDocument["background_order"];
//    rapidjson::Value newOrder(rapidjson::kArrayType);
//    rapidjson::Document::AllocatorType& allocator = orderDocument.GetAllocator();
//    
//    for (rapidjson::SizeType i = 0; i < itemOrder.Size(); i++) {
//        auto soundKey = itemOrder[i].GetString();
//        if (soundItemKey->compare(soundKey) == 0) {
//            log("delete item");
//            continue; // pass
//        }
//        log("item %s  del: %s", soundKey, soundItemKey->getCString());
//        newOrder.PushBack(soundKey, allocator);
//    }
//    
//    orderDocument["background_order"] = newOrder;
//    rapidjson::StringBuffer strbuf;
//    rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
//    orderDocument.Accept(writer);
//    auto resultOrderJsonString = strbuf.GetString();
//    ud->setStringForKey(ORDER_KEY, resultOrderJsonString);
//    ud->flush();
//    
//    return true;
//}

//void SoundScrollView::runRemoveSoundAnimation(__String* soundItemKey)
//{
//    log("removeSoundAnimation");
//    std::string keyStr(soundItemKey->getCString());
//    auto soundIndex = this->getSoundIndex(keyStr);
//    
//    this->removeSound(soundItemKey);
//    
//    if (soundIndex == CC_INVALID_INDEX) {
//        log("not found");
//    } else {
//        cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
//        auto soundItemToBeDelete = (Node*)items->objectForKey(soundItemKey->getCString());
//        
//        float moveDuration = 0.25f;
//        auto scaleTo = ScaleTo::create(moveDuration, 0.5f);
//        auto placeTo = Place::create( cocos2d::Point(-visibleSize.width*2, -visibleSize.height*2));
//        auto removeCallback = CallFunc::create([&, visibleSize, soundIndex, moveDuration](){
//            
//            cocos2d::Point scrollOffset = scrollView->getContentOffset();
//            auto moveByPoint = cocos2d::Point(-HOME_BOTTOM_SCROLL_VIEW_FRAME_SIZE.width,0);
//            this->scrollView->setContentOffsetInDuration(cocos2d::Point(scrollOffset.x - moveByPoint.x, scrollOffset.y), moveDuration);
//            
//            auto moveBy = MoveBy::create(moveDuration, moveByPoint);
//            auto currentSoundItemCount = soundList->count();
//            
//            int nextIndex = currentSoundItemCount;
//            bool processNearSoundItem = false;
//            
//            for (int index = soundIndex; index < currentSoundItemCount; index++) {
//                float bottomPadding = 0;
//                if((index%2 == 0)) {
//                    bottomPadding = RECORDING_BOTTOM_SCROLL_VIEW_FRAME_BOTTOM_PADDING;
//                } else {
//                    bottomPadding = -RECORDING_BOTTOM_SCROLL_VIEW_FRAME_BOTTOM_PADDING;
//                }
//                
//                moveByPoint = cocos2d::Point(-HOME_BOTTOM_SCROLL_VIEW_FRAME_SIZE.width,bottomPadding);
//                moveBy = nullptr;
//                moveBy = MoveBy::create(moveDuration, moveByPoint);
//                
//                auto soundItemKey = (__String*)soundList->getObjectAtIndex(index);
//                auto holder = (Node*)items->objectForKey(soundItemKey->getCString());
//                holder->setTag(SOUND_BASE_TAG + index-1);
//                auto boundingRect = holder->getBoundingBox();
//                auto originXInScreen = boundingRect.origin.x + scrollOffset.x;
//                
//                if (index == currentSoundItemCount) {
//                    auto lastAnimationBookCallback = CallFunc::create([&](){
//                        this->scrollView->setTouchEnabled(true);
//                    });
//                    auto lastBookAnimationSequence = Sequence::create(moveBy->clone(), lastAnimationBookCallback, NULL);
//                    holder->runAction(lastBookAnimationSequence);
//                } else {
//                    holder->runAction(moveBy->clone());
//                }
//            }
//            
//            for (int index = nextIndex; index < currentSoundItemCount; index++) {
//                auto soundItemKey = (__String *)soundList->getObjectAtIndex(index);
//                auto holder = (Node*)items->objectForKey(soundItemKey->getCString());
//                auto originPosition = holder->getPosition();
//                holder->setPosition( cocos2d::Point(originPosition.x+moveByPoint.x, originPosition.y+moveByPoint.y));
//            }
//        });
//        
//        auto doneCallback = CallFunc::create([=](){
//            auto selDeletePin = (Sprite*)pins->getLastObject();
//            selDeletePin->removeFromParentAndCleanup(true);
//            selDeleteSoundItem->removeFromParentAndCleanup(true);
//            items->removeObjectForKey(selectedSoundItemKey->getCString());
//            pins->removeLastObject();
//            std::string soundItemStr(selectedSoundItemKey->getCString());
//            soundList->removeObjectAtIndex(this->getSoundIndex(soundItemStr));
//            log("after items->count : %i %zi %zi", items->count(), pins->count(), soundList->count());
//            this->adjustLine();
//        });
//        
//        auto disappearAction = Sequence::create(scaleTo, placeTo, removeCallback, doneCallback, NULL);
//        soundItemToBeDelete->runAction(disappearAction);
//    }
//}

//void SoundScrollView::adjustLine()
//{
//    std::vector<std::string> lineResources;
//    lineResources.push_back("set_background_frame_line_left.png");
//    lineResources.push_back("set_background_frame_line_mid.png");
//    lineResources.push_back("set_background_frame_line_right.png");
//    
//    auto soundCount = soundList->count();
//    auto soundContentSize = HOME_BOTTOM_SCROLL_VIEW_FRAME_SIZE;
//    int whiteSpace = WHITESPACE_CHAR_DEFAULT;
//    auto scrollViewWidth = HOME_BACKGROUND_SCROLL_VIEW_STARTPADDING + soundContentSize.width*(soundCount + whiteSpace);
//    
//    auto targetMidLineNum = std::ceil( (scrollViewWidth - HOME_BOTTOM_LINE_LEFT.width - HOME_BOTTOM_LINE_RIGHT.width) / HOME_BOTTOM_LINE_MID.width );
//    auto currentMidLineNum = linesCount-2;
//    if (targetMidLineNum < currentMidLineNum) { // remove line
//        auto moveByPoint = cocos2d::Point(-HOME_BOTTOM_LINE_MID.width+(HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_HOLDER_SIZE.width*2),0);
//        auto toRemoveCount = currentMidLineNum - targetMidLineNum;
//        
//        auto container = scrollView->getChildren().at(0);
//        Sprite *lastLine = (Sprite *)(container->getChildByTag(LINE_BASE_TAG+(linesCount-1)));
//        auto listLineCurrentPosition = lastLine->getPosition();
//        lastLine->setPosition(listLineCurrentPosition - cocos2d::Point(toRemoveCount * HOME_BOTTOM_LINE_MID.width, 0) );
//        
//        for (int index = currentMidLineNum; index > currentMidLineNum - toRemoveCount; index--) {
//            Sprite *line = (Sprite *)(container->getChildByTag(LINE_BASE_TAG+index));
//            line->removeFromParent();
//        }
//        
//        Sprite *lastLineForNewTag = (Sprite *)(container->getChildByTag(LINE_BASE_TAG+(linesCount-1)));
//        lastLineForNewTag->setTag(LINE_BASE_TAG+(linesCount-2));
//        
//        auto currentContentSize = scrollView->getContentSize();
//        scrollView->setContentSize(currentContentSize - cocos2d::Size(toRemoveCount * HOME_BOTTOM_LINE_MID.width, 0));
//        
//        linesCount -= toRemoveCount;
//        
//        cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
//        cocos2d::Point scrollOffset = scrollView->getContentOffset();
//        if (scrollOffset.x < -(scrollView->getContentSize().width-visibleSize.width+soundContentSize.width)) {
//            this->scrollView->setContentOffsetInDuration(cocos2d::Point(-(scrollView->getContentSize().width-visibleSize.width+soundContentSize.width), scrollOffset.y), 0.25f);
//        }
//        
//    } else if (targetMidLineNum > currentMidLineNum){ // add line
//        auto toAddCount = targetMidLineNum - currentMidLineNum;
//        auto lastIndex = (linesCount-1);
//        
//        auto container = scrollView->getChildren().at(0);
//        
//        Sprite *lastLine = (Sprite *)(container->getChildByTag(LINE_BASE_TAG+lastIndex));
//        auto listLineCurrentPosition = lastLine->getPosition();
//        lastLine->setPosition(listLineCurrentPosition + cocos2d::Point(toAddCount * HOME_BOTTOM_LINE_MID.width, 0) );
//        
//        auto lineStart = cocos2d::Point(listLineCurrentPosition.x, HOME_BOTTOM_LINE_START.y);
//        for (int index = lastIndex; index < lastIndex + toAddCount; index++) {
//            auto line = Sprite::create("set_background_frame_line_mid.png");
//            line->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//            line->setPosition(lineStart);
//            line->setTag(LINE_BASE_TAG + index);
//            scrollView->addChild(line, TOUCHABLE_LAYER-5);
//            lineStart = lineStart + cocos2d::Point(line->getContentSize().width, 0);
//        }
//        
//        auto currentContentSize = scrollView->getContentSize();
//        scrollView->setContentSize(currentContentSize + cocos2d::Size(toAddCount * HOME_BOTTOM_LINE_MID.width, 0));
//        
//        lastLine->setTag(LINE_BASE_TAG + lastIndex + toAddCount);
//        
//        linesCount += toAddCount;
//    } else {
//        // no need to adjust
//    }
//    
//}

void SoundScrollView::scrollToLastItem()
{
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    this->scrollView->setContentOffsetInDuration(cocos2d::Point(-(scrollView->getContentSize().width-visibleSize.width), scrollOffset.y), 0.25f);
}

void SoundScrollView::scrollToFirstItem()
{
    if (!isScrollingToFirst) {
        isScrollingToFirst = true;
        this->scrollView->stopAllScroll();
        this->scrollView->setContentOffsetInDuration(cocos2d::Point::ZERO, 0.25f);
    }
}

void SoundScrollView::loadJsonToScrollView(ScrollView *scrollView)
{
    auto ud = UserDefault::getInstance();
    auto orderContentStr = ud->getStringForKey(PRE_LOAD_OBJ_ORDER_KEY);
    rapidjson::Document orderDocument;
    orderDocument.Parse<0>(orderContentStr.c_str());
    rapidjson::Value& order = orderDocument["sound_order"];
    
    // user order
    auto userOrderJsonString = ud->getStringForKey(ORDER_KEY, "");
    rapidjson::Document userOrderDocument;
    userOrderDocument.Parse<0>(userOrderJsonString.c_str());
    rapidjson::Value& userOrder = userOrderDocument["sound_order"];
    
    auto soundSize = order.Size() + userOrder.Size();
    
    // create merged book Key array
    auto mergedSoundKeyArray = __Array::createWithCapacity(soundSize);
    
    for(int index = 0; index < order.Size(); index++){
        mergedSoundKeyArray->addObject( __String::create(order[rapidjson::SizeType(index)].GetString()) );
    }
    
    for(int index = 0; index < userOrder.Size(); index++){
        mergedSoundKeyArray->addObject( __String::create(userOrder[rapidjson::SizeType(index)].GetString()) );
    }
    
    auto soundItemContentSize = HOME_BOTTOM_SCROLL_VIEW_MUSIC_ITEM_HOLDER_SIZE;
    
    for (int index = 0; index < soundSize; index++) {
        auto soundKey = ((__String*)mergedSoundKeyArray->getObjectAtIndex(index))->getCString();
        auto item = this->createSoundForScrollView(soundKey, index);
        items->setObject(item, soundKey);
        auto ud = UserDefault::getInstance();
        auto soundJsonString = ud->getStringForKey(soundKey);
        rapidjson::Document soundInfo;
        soundInfo.Parse<0>(soundJsonString.c_str());

        scrollView->addChild(item, TOUCHABLE_LAYER_WITH_DIM, SOUND_BASE_TAG+index);
//        if (soundInfo["custom"].GetBool()){
//            scrollView->addChild(item, TOUCHABLE_LAYER, SOUND_BASE_TAG+index);
//        } else {
//            scrollView->addChild(item, TOUCHABLE_LAYER_WITH_DIM, SOUND_BASE_TAG+index);
//        }
        
        auto spriteName = __String::createWithFormat("home_background_frame_pin_%02d.png", index%4+1);
        auto pin = Sprite::create(spriteName->getCString());
        pin->setAnchorPoint(cocos2d::Point::ANCHOR_TOP_LEFT);
        cocos2d::Point pos = cocos2d::Point(item->getPositionX() + (soundItemContentSize.width - pin->getContentSize().width)/2,
                                            soundItemContentSize.height + item->getPositionY() + HOME_BOTTOM_SCROLL_VIEW_CHARACTER_PIN_TOP_PADDING);
        pin->setPosition(pos);
        pins->addObject(pin);
//        if (soundInfo["custom"].GetBool() ){
//            scrollView->addChild(pin, TOUCHABLE_LAYER+1);
//        } else {
//            scrollView->addChild(pin, TOUCHABLE_LAYER-2);
//        }
        scrollView->addChild(pin, TOUCHABLE_LAYER-2);
    }
    
    std::vector<std::string> lineResources;
    lineResources.push_back("set_background_frame_line_left.png");
    lineResources.push_back("set_background_frame_line_mid.png");
    lineResources.push_back("set_background_frame_line_right.png");
    
    int whiteSpace = WHITESPACE_CHAR_DEFAULT;
    auto scrollViewWidth = HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_START_PADDING + soundItemContentSize.width*(soundSize + whiteSpace);
    auto midLineNum = std::ceil( (scrollViewWidth - HOME_BOTTOM_LINE_LEFT.width - HOME_BOTTOM_LINE_RIGHT.width) / HOME_BOTTOM_LINE_MID.width );
    
    cocos2d::Point startLinePosition = HOME_BOTTOM_LINE_START;
    float totalLineWidth = startLinePosition.x;
    
    int lineCount = 0;
    while (lineCount < midLineNum + 2) { // +2 -> include left, right line
        int resourceIndex = (lineCount == 0)? 0:(lineCount == (midLineNum+1))? 2:1;
        auto line = Sprite::create(lineResources.at(resourceIndex));
        line->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        line->setPosition(cocos2d::Point(totalLineWidth, startLinePosition.y));
        line->setTag(LINE_BASE_TAG + lineCount);
        lineCount++;
        totalLineWidth += line->getContentSize().width;
        scrollView->addChild(line, TOUCHABLE_LAYER-5);
    }
    linesCount = lineCount;
    scrollView->setContentSize(cocos2d::Size(totalLineWidth, HOME_BACKGROUND_SCROLL_VIEW_HEIGHT));
}

void SoundScrollView::reloadData()
{
    soundList->removeAllObjects();
//    deleteButtons->removeAllObjects();
    items->removeAllObjects();
    pins->removeAllObjects();
    
    auto container = scrollView->getChildren().at(0);
    container->removeAllChildren();
    
    this->loadJsonToScrollView(this->scrollView);
}
