//
//  TSDataStorage.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 8. 25..
//
//

#ifndef _TSDATASTORAGE_H_
#define _TSDATASTORAGE_H_

#include "cocos2d.h"
#include "TSDoleProtocol.h"

NS_CC_BEGIN

class TSDataStorage : public Ref, public TSDoleProtocolDelegate {
public:
    TSDataStorage();
    virtual ~TSDataStorage();
    
    static TSDataStorage *getInstance();
    
protected:
    void onRequestStarted(TSDoleProtocol *protocol, TSNetResult result);
    void onResponseEnded(TSDoleProtocol *protocol, TSNetResult result);
    void onProtocolFailed(TSDoleProtocol *protocol, TSNetResult result);
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    void onRequestCanceled(TSDoleProtocol *protocol);
//#endif
    
private:
    TSDoleGetReNewAuthKey *_reNewAuthKey;
};

NS_CC_END

#endif /* defined(_TSDATASTORAGE_H_) */
