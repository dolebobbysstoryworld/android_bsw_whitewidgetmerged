//
//  TextColorButton.cpp
//  bobby
//
//  Created by oasis on 2014. 6. 2..
//
//

#include "TextColorButton.h"

USING_NS_CC;

TextColorButton* TextColorButton::create(const std::string &normalImage,
                       const std::string& selectedImage ,
                       const std::string& disableImage,
                       cocos2d::ui::TextureResType texType)
{
    TextColorButton *btn = new TextColorButton;
    if (btn && btn->init(normalImage,selectedImage,disableImage,texType)) {
        btn->autorelease();
        return btn;
    }
    CC_SAFE_DELETE(btn);
    return nullptr;
}

bool TextColorButton::init(const std::string &normalImage,
                  const std::string& selectedImage ,
                  const std::string& disableImage,
                  cocos2d::ui::TextureResType texType)
{
    bool ret = true;
    do {
        if (!Widget::init()) {
            ret = false;
            break;
        }
        setTouchEnabled(true);
        this->loadTextures(normalImage, selectedImage, disableImage,texType);
        normalTitleColor = Color3B(0,0,0);
        pressTitleColor = Color3B(0,0,0);
        
    } while (0);
    return ret;
}

void TextColorButton::onPressStateChangedToNormal()
{
    if (_delegate != NULL) {
        _delegate->textColorButtonNormal(this);
    }
    this->setTitleColor(normalTitleColor);
    _buttonNormalRenderer->setVisible(true);
    _buttonClickedRenderer->setVisible(false);
    _buttonDisableRenderer->setVisible(false);
    if (_pressedTextureLoaded)
    {
        if (_pressedActionEnabled)
        {
            _buttonNormalRenderer->stopAllActions();
            _buttonClickedRenderer->stopAllActions();
            Action *zoomAction = ScaleTo::create(0.05f, _normalTextureScaleXInSize, _normalTextureScaleYInSize);
            _buttonNormalRenderer->runAction(zoomAction);
            _buttonClickedRenderer->setScale(_pressedTextureScaleXInSize, _pressedTextureScaleYInSize);
        }
    }
    else
    {
        if (_scale9Enabled)
        {
            updateTextureRGBA();
        }
        else
        {
            _buttonNormalRenderer->stopAllActions();
            _buttonNormalRenderer->setScale(_normalTextureScaleXInSize, _normalTextureScaleYInSize);
        }
    }
}

void TextColorButton::onPressStateChangedToPressed()
{
    if (_delegate != NULL) {
        _delegate->textColorButtonPressed(this);
    }
    this->setTitleColor(pressTitleColor);
    if (_pressedTextureLoaded)
    {
        _buttonNormalRenderer->setVisible(false);
        _buttonClickedRenderer->setVisible(true);
        _buttonDisableRenderer->setVisible(false);
        if (_pressedActionEnabled)
        {
            _buttonNormalRenderer->stopAllActions();
            _buttonClickedRenderer->stopAllActions();
            Action *zoomAction = ScaleTo::create(0.05f, _pressedTextureScaleXInSize + 0.1f, _pressedTextureScaleYInSize + 0.1f);
            _buttonClickedRenderer->runAction(zoomAction);
            _buttonNormalRenderer->setScale(_pressedTextureScaleXInSize + 0.1f, _pressedTextureScaleYInSize + 0.1f);
        }
    }
    else
    {
        _buttonNormalRenderer->setVisible(true);
        _buttonClickedRenderer->setVisible(true);
        _buttonDisableRenderer->setVisible(false);
        if (_scale9Enabled)
        {
            _buttonNormalRenderer->setColor(Color3B::GRAY);
        }
        else
        {
            _buttonNormalRenderer->stopAllActions();
            _buttonNormalRenderer->setScale(_normalTextureScaleXInSize + 0.1f, _normalTextureScaleYInSize + 0.1f);
        }
    }
}

void TextColorButton::setNormalTitleColor(const Color3B& color)
{
    _titleColor = normalTitleColor = color;
    _titleRenderer->updateDisplayedColor(color);
}

void TextColorButton::setPressTitleColor(const Color3B& color)
{
    pressTitleColor = color;
}
