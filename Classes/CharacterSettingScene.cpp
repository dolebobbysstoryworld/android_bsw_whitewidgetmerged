//
//  CharacterSettingScene.cpp
//  bobby
//
//  Created by oasis on 2014. 4. 17..
//
//

#include "CharacterSettingScene.h"
#include <cocosbuilder/CocosBuilder.h>
#include "BookMapScene.h"
#include "Character.h"
#include "ResourceManager.h"

#include "Define.h"

#include "external/json/document.h"
#include "external/json/prettywriter.h"
#include "external/json/stringbuffer.h"

#define TIME_SCALE          60 // 1/60 sec
#define TIME_DURATION       60 // 60 sec

Scene* CharacterSetting::createScene(__String* bookKey, int chapterIndex)
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = CharacterSetting::create();
    
    layer->bookKey = bookKey;
    layer->chapterIndex = chapterIndex;
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}


// on "init" you need to initialize your instance
bool CharacterSetting::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LayerColor::initWithColor( Color4B(0xba,0xea,0xed,0xff)) )
    {
        return false;
    }
    
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    cocos2d::Point origin = Director::getInstance()->getVisibleOrigin();
    
    touchNumber = 0;
    
    auto bgMap = Sprite::create("bg_map.png");
    bgMap->setPosition(origin.x + bgMap->getContentSize().width/2,  origin.y + visibleSize.height - bgMap->getContentSize().height/2);
    this->addChild(bgMap, 0);
    
    auto btnBack = MenuItemImage::create("btn_map_back_normal.png",
                                         "btn_map_back_press.png",
                                         CC_CALLBACK_1(CharacterSetting::btnBackCallback, this));
    btnBack->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
	btnBack->setPosition(cocos2d::Point::ZERO);
    
    auto backMenu = Menu::create(btnBack, NULL);
    backMenu->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    backMenu->setPosition(cocos2d::Point(BOOKMAP_BTN_BACK));
    this->addChild(backMenu, 2);
    
    
    auto menuItem = MenuItemImage::create("btn_home_menu_normal.png",
                                          "btn_home_menu_press.png",
                                          CC_CALLBACK_1(CharacterSetting::menuCallback, this));
    menuItem->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
	menuItem->setPosition(BOOKMAP_MENU_BTN);
    
    auto menu = Menu::create(menuItem, NULL);
    menu->setPosition(cocos2d::Point::ZERO);
    this->addChild(menu, 2);
    
//    std::string fileString = "front_longtap.ccbi";
//    const char *fileName = fileString.c_str();
//    lion = Character::create(fileName, "lion_front_test.png");
//    lion->setPosition(100,100);
//    this->addChild(lion,2);
    
    // TODO: filename obj should be changed to property of Character class
    __String* fileName = __String::create("lion_front_test.png");
    fileName->retain();
    lion = Character::create(fileName->getCString());
    lion->setUserData(fileName);
    lion->setPosition(300,300);
    this->addChild(lion,2);
    
    __String* fileName2 = __String::create("lion2_front_test.png");
    fileName2->retain();
    lion2 = Character::create(fileName2->getCString());
    lion2->setUserData(fileName2);
    lion2->setPosition(500,300);
    this->addChild(lion2,2);    
    
    log("size : %f, %f",lion->getContentSize().width, lion->getContentSize().height);
    
    this->scheduleUpdate();
    
    pressed = false;
    timeStamp = 0;
    timelines = __Array::create();
    timelines->retain();
    
    return true;
}

void CharacterSetting::menuCallback(Ref* sender)
{
    rapidjson::Document timelineDocument;
    timelineDocument.SetObject();
    rapidjson::Document::AllocatorType& allocator = timelineDocument.GetAllocator();

    rapidjson::Value timelinesArray(rapidjson::kArrayType);
    
    for(int i = 0; i < timelines->count(); i++){
        rapidjson::Value timelineDic(rapidjson::kObjectType);
        
        __Dictionary *dic = (__Dictionary *)timelines->getObjectAtIndex(i);
        int timeStamp = ((__Integer*)dic->objectForKey("timestamp"))->getValue();
        
        rapidjson::Value timeStampValue(rapidjson::kNumberType);
        timeStampValue.SetInt(timeStamp);
        timelineDic.AddMember("timestamp", timeStampValue, allocator);
        
        __String* charName = ((__String*)dic->objectForKey("char_name"));
        if (charName != NULL) {
//            log("timestamp : %d, charName : %s", timeStamp, charName->getCString());
            
            rapidjson::Value charNameValue(rapidjson::kStringType);
            charNameValue.SetString(charName->getCString());
            timelineDic.AddMember("char_name", charNameValue, allocator);

            __Array* charPos = ((__Array*)dic->objectForKey("char_pos"));

            rapidjson::Value charPosArray(rapidjson::kArrayType);
            
            rapidjson::Value charPosX(rapidjson::kNumberType);
            charPosX.SetDouble(((__Float*)charPos->getObjectAtIndex(0))->getValue());
            
            rapidjson::Value charPosY(rapidjson::kNumberType);
            charPosY.SetDouble(((__Float*)charPos->getObjectAtIndex(1))->getValue());

            charPosArray.PushBack(charPosX, allocator);
            charPosArray.PushBack(charPosY, allocator);
            timelineDic.AddMember("char_pos", charPosArray, allocator);
            
        } else {
//            log("timestamp : %d, no charName", timeStamp);
            rapidjson::Value nullValue(rapidjson::kNullType);
            timelineDic.AddMember("char_name", nullValue, allocator);
            timelineDic.AddMember("char_pos", nullValue, allocator);
        }
        timelinesArray.PushBack(timelineDic, allocator);
    }

    timelineDocument.AddMember("timelines", timelinesArray, allocator);
    
    rapidjson::StringBuffer strbuf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
    timelineDocument.Accept(writer);
    auto jsonString = strbuf.GetString();

//    log("json string : %s", jsonString);
//    log("string length : %zu", std::strlen( jsonString ));
//    log("end");

    if (this->bookKey) {
        auto ud = UserDefault::getInstance();
        auto bookJsonString = ud->getStringForKey(this->bookKey->getCString());
        if (bookJsonString != "") {
            unsigned long int timestamp= time(NULL);
            auto timelineKey = __String::createWithFormat("timeline_%ld", timestamp);
            ud->setStringForKey(timelineKey->getCString(), jsonString);

            rapidjson::Document bookDoc;
            
            bookDoc.Parse<0>(bookJsonString.c_str());
            rapidjson::Value& chapters = bookDoc["chapters"];
            __String *chapterName = __String::createWithFormat("chapter%d", chapterIndex);
            rapidjson::Value& chapterData = chapters[chapterName->getCString()];
            if (chapterData.IsNull()) {
                chapterData.SetObject();
                chapterData.AddMember("timeline_file", timelineKey->getCString(), bookDoc.GetAllocator());
            } else {
                auto oldtimeline = chapterData["timeline_file"].GetString();
                ud->setStringForKey(oldtimeline, "");
                chapterData["timeline_file"] = timelineKey->getCString();
            }
            
            rapidjson::StringBuffer strbuf;
            rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
            bookDoc.Accept(writer);
            auto resultJsonString = strbuf.GetString();
            
            ud->setStringForKey(this->bookKey->getCString(), resultJsonString);
            ud->flush();
        }
    }
    
    this->removeAllChildren();
    auto director = Director::getInstance();
    director->popScene();
}

void CharacterSetting::update(float delta)
{
    if (pressed) {
        if (timeStamp < TIME_DURATION * TIME_SCALE) {
            if (selectedSprite) {
                __Dictionary *timeline = __Dictionary::create();
                timeline->setObject(__Integer::create(timeStamp), "timestamp");
                
                __String *fileName = (__String*)selectedSprite->getUserData();
                timeline->setObject(fileName, "char_name");
                
                __Array *posArray = __Array::create();
                posArray->addObject(__Float::create(selectedSprite->getPositionX()));
                posArray->addObject(__Float::create(selectedSprite->getPositionY()));
                timeline->setObject(posArray, "char_pos");
                timelines->addObject(timeline);
                
                log("pressed - selected target %s(%f, %f)", fileName->getCString(), selectedSprite->getPositionX(), selectedSprite->getPositionY());
            } else {
                log("pressed - no target");
                
                __Dictionary *timeline = __Dictionary::create();
                timeline->setObject(__Integer::create(timeStamp), "timestamp");
                timelines->addObject(timeline);
            }
            timeStamp++;
        } else {
            log("timestamp - reached to end");
        }
    }
}

void CharacterSetting::onEnter()
{
    Layer::onEnter();
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(false);
    
    listener->onTouchBegan = [=](Touch* touch, Event* event) {
        log("touch began...");
        pressed = true;
        
        auto target = static_cast<Character*>(event->getCurrentTarget());
        Point locationInNode = target->convertToNodeSpace(touch->getLocation());

        if (target->pixelMaskContainsPoint(locationInNode)) {
            touchNumber++;
            touches.pushBack(touch);
            reZorder(target);
            log("sprite onTouchBegan... x = %f, y = %f", locationInNode.x, locationInNode.y);
            selectedSprite = target;

            return true;
        }
//        if (target->getBoundingBox().containsPoint(this->convertTouchToNodeSpace(touch))) {
//         
//        }

        return false;
    };
    
    listener->onTouchMoved = [=](Touch* touch, Event* event){
        cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
        auto target = static_cast<Character*>(event->getCurrentTarget());
//        Point locationInNode = target->convertToNodeSpace(touch->getLocation());
        if (touch->getLocation().x < 50 || touch->getLocation().x > visibleSize.width-50 || touch->getLocation().y < 50 || touch->getLocation().y > visibleSize.height-50) return;
        
        Character *character = (Character*)target;
        if(target->getLocalZOrder() == 1 && character->touchNumber == 1) {
            target->setPosition(target->getPosition() + touch->getDelta());
        }
        
//        if (target->pixelMaskContainsPoint(locationInNode)) {
//            
//        }
//        if(target->getBoundingBox().containsPoint(this->convertTouchToNodeSpace(touches.at(0))))
//        {
//        }
    };
    
    listener->onTouchEnded = [=](Touch* touch, Event* event){
        touchNumber--;
        touches.eraseObject(touch);
        selectedSprite = NULL;
        pressed = false;
        log("sprite onTouchesEnded...");
    };
    
    listener->onTouchCancelled = [=](Touch* touch, Event* event){
        selectedSprite = NULL;
        pressed = false;
        log("sprite onTouchCancelled...");
    };
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, lion);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener->clone(), lion2);
}

void CharacterSetting::onExit()
{
    _eventDispatcher->removeEventListenersForTarget(lion);
    _eventDispatcher->removeEventListenersForTarget(lion2);
    Layer::onExit();
}

void CharacterSetting::reZorder(Sprite* pSender)
{
    if(touchNumber > 1) return;
    
    lion->setLocalZOrder(0);
    lion2->setLocalZOrder(0);
    pSender->setZOrder(1);
}

void CharacterSetting::btnBackCallback(Ref* pSender)
{
#ifndef MACOS
    //    UIViewController *viewController = (UIViewController*)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
    //    for (UIView *subView in [viewController.view subviews]) {
    //        [subView removeFromSuperview];
    //        subView = nil;
    //    }
#endif
    this->removeAllChildren();
    auto director = Director::getInstance();
    director->popScene();
//    auto bookMapScene = BookMap::createScene(nullptr);
//    director->replaceScene(bookMapScene);
//    this->cleanup();
}