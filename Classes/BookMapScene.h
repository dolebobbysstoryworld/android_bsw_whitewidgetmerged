//
//  BookMapScene.h
//  bobby
//
//  Created by Jaemok Jeong on 4/8/14.
//
//

#ifndef __bobby__BookMapScene__
#define __bobby__BookMapScene__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "extensions/GUI/CCEditBox/CCEditBox.h"
#include "PopupLayer.h"

#include "external/json/document.h"
#include "external/json/prettywriter.h"
#include "external/json/stringbuffer.h"
#include <cocosbuilder/CocosBuilder.h>
#include "LabelExtension.h"
#include "CharacterRecordingScene.h"
#include "MenuListLayer.h"
#include "PlayAndRecordScene.h"
#include "share/Share.h"

USING_NS_CC;
USING_NS_CC_EXT;

class Home;

class BookMap;
class BookMapDelegate
{
public:
    virtual ~BookMapDelegate() {}
    virtual void updateCurrentBook(__String *bookKey) = 0;
    virtual void onExitBookMap(__String *bookKey, bool added) = 0;
    virtual void characterPhotoSavedInRecord(std::string characterKey) = 0;
    virtual void bgPhotoSavedInRecord(std::string bgKey) = 0;
};

class BookMap : public cocos2d::LayerColor, IMEDelegate, cocosbuilder::CCBAnimationManagerDelegate, PopupLayerDelegate, public EditBoxDelegate, CharacterRecordingDelegate, MenuListLayerDelegate, public Share::OnResultCallback, PlayAndRecordDelegate
{
public:
    BookMapDelegate *getDelegate() { return _delegate; }
    void setDelegate(BookMapDelegate *pDelegate) { _delegate = pDelegate; }
    
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene(__String* bookKey, bool addMode);
    static BookMap* create(__String* bookKey, bool addMode);
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    void menuCallback(cocos2d::Ref* pSender);
    void btnBackCallback(cocos2d::Ref* pSender);
    Node * addWrite(cocos2d::Point location);
    Sprite* addBooksObject(const std::string& imageName, cocos2d::Point location);
    void runFadeToAction(Sprite* object);
    
    virtual void onEnter();
    virtual void onEnterTransitionDidFinish();
    virtual void onExit();
    
    void checkChapterExist();
    void reloadChapterData();

    void completedAnimationSequenceNamed(const char *name);
    
    void bookButtonCallback(Ref* sender);
    void titleCallback(int area);
    Sprite* getCcbiSprite(const char* ccbiName, cocos2d::Point position);
    void runAnimationWithName(Sprite* object, const char* name);
    Menu* addMenu(Sprite* normal, Sprite* press, cocos2d::Point position, int tag, bool visible);
    void addItemBg(const char* ccbiName, cocos2d::Point position, int chapterTag);
    void runFeatherSequenceAnimation(cocos2d::Point position, int chapterTag);
    void runCharacterSequenceAnimation(const char *characterKey, const char *characterCcbiName, cocos2d::Point characterPos, int characterTag, const char*iconBgCcbiName, cocos2d::Point iconBgPos, const char *delBtnCcbiName, cocos2d::Point deletePos, int deleteBtnTag, int chapterTag);
    void addIconMenu(const char *characterKey, const char *ccbiName, cocos2d::Point position, int tag, int chapterTag);
    void addFeatherMenuAndRun(cocos2d::Point position, int tag, int chapterTag);
    __Array* getChapterSpriteArray(int chapterTag);

    // IMEDelegate
    void keyboardWillShow (IMEKeyboardNotificationInfo &info);
    void keyboardDidShow (IMEKeyboardNotificationInfo &info);
    void keyboardWillHide (IMEKeyboardNotificationInfo &info);
    void keyboardDidHide (IMEKeyboardNotificationInfo &info);
    
    void popupCallback(Ref* pSender);
    void popupButtonClicked(PopupLayer *popup, cocos2d::Ref *obj);
    void onSuccessCallback(Share::Client* client, int errorCode, Share::KeyJSONValueMap& resultMap);
    void onFailedCallback(Share::Client* client, int errorCode, Share::KeyJSONValueMap& resultMap);
    void onCancelledCallback(Share::Client* clien);
    void onShareCommentResult(bool isOK, const char* comment);
    void onShareCommentResult2(int shareType, bool isOK, const char* comment);
    void onFacebookShareComplete();
    
    void playAndRecordFinished(PlayAndRecordType type, bool success, std::string fileName);
    
    // implement the "static create()" method manually
    CREATE_FUNC(BookMap);
    
    __String *bookKey;
    rapidjson::Document bookJson;
    ScrollView *scrollView;
    EditBox *titleTextField;
    EditBox *authorTextField;
    int textFieldStatus;
    int removingChapter;
    
    void onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event);
    
    virtual void editBoxEditingDidBegin(EditBox* editBox);
    virtual void editBoxEditingDidEnd(EditBox* editBox);
    virtual void editBoxTextChanged(EditBox* editBox, const std::string& text);
    virtual void editBoxReturn(EditBox* editBox);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    void updateEditBoxText(const std::string& title, const std::string& author);
    void clearEditMode();
    void onFacebookActionResult(int result, int type);
    void showShareEditText();
    void onFacebookProgressUpdate(double progress);
    void cancelYoutubeShare();
    void doleGetInventoryListProtocol();
#endif
    void characterPhotoSavedInRecordScene(std::string characterKey);
    void bgPhotoSavedInRecordScene(std::string bgKey);
    
    void listButtonClicked(int buttonTag);
    void popupListClicked(PopupLayer *popup, std::string selectedTitle);
    void showVideoShareConfirmPopup();
    void selectVideoShareTarget();
    void videoDownloadToDevice();
    
protected:
    BookMapDelegate *_delegate;
private:
    __Array *chapter1SpritesArray;
    __Array *chapter2SpritesArray;
    __Array *chapter3SpritesArray;
    __Array *chapter4SpritesArray;
    __Array *chapter5SpritesArray;

    bool chapter1Exist;
    bool chapter2Exist;
    bool chapter3Exist;
    bool chapter4Exist;
    bool chapter5Exist;
    bool chapter1TempDataExist;
    bool chapter2TempDataExist;
    bool chapter3TempDataExist;
    bool chapter4TempDataExist;
    bool chapter5TempDataExist;
    bool textEdited; // title or author
    
    Sprite *featherNormal01;
    Sprite *featherNormal02;
    Sprite *featherNormal03;
    Sprite *featherNormal04;
    Sprite *featherNormal05;
    Sprite *featherPress01;
    Sprite *featherPress02;
    Sprite *featherPress03;
    Sprite *featherPress04;
    Sprite *featherPress05;

    Sprite *contentsSprite;
    
    Sprite *inputTitleSprite;
    std::string titleString;
    std::string authorString;
    
    bool cancelEdit;
    bool addMode;
    
    void removeInputTitleSpriteWithChildren();
    void setAndsaveTitle();
    void saveToJason();
    Label* getFlagLabel(const char* flagName, cocos2d::Size labelSize, cocos2d::Point position, int pos);
    void setEnabledForMenu(bool isEnabled);
    void setTitleAndNameUI(const std::string &title, const std::string &nameTitle);
    void addDimLayer();
    void removeDimLayer();
   
    bool isPlatformShown;
    
    MenuListLayer *menuListLayer;
    void runSettingView();

    bool finishedDownload;
    bool readyForShare;
    std::string movieFilePath;

    bool fromhome;
    Share::Client* client;
    Menu *playMenu;
    
    void setPlayMenuVisible();
    const char* getMapccbiName(__Array *bgMapKeys,int index, bool characterExist);

public:
    void checkRecall(int type);
    void setHome(Home *pHome) { _homeLayer = pHome; }
    Home *getHome() { return _homeLayer; }
    enum class ShareTarget {
        None,
        Youtube,
        Facebook,
    };
    void showShareCommentView();
    void startAuthorizedForVideoUpload();
private:
    Home *_homeLayer;
    ShareTarget _shareTarget;
    bool blockBackKey;
};

#endif /* defined(__bobby__BookMapScene__) */
