//
//  Define.h
//  bobby
//
//  Created by Dongwook, Kim on 14. 4. 23..
//
//

#ifndef bobby_Define_h
#define bobby_Define_h

const static char* PRE_LOAD_OBJ_ORDER_KEY   =           "pre_load_obj_order_key";
const static char* ORDER_KEY                =           "order_key";            // user object order key
const static char* LAST_UPDATE_KEY          =           "last_update_key";

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
const static char* FONT_NAME				=			"Roboto-Regular";
const static char* FONT_NAME_BOLD			=			"Roboto-Bold";
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
const static char* FONT_NAME				=			"Helvetica";
const static char* FONT_NAME_BOLD			=			"Helvetica-Bold";
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_MAC) 
const static char* FONT_NAME				=			"Helvetica";
const static char* FONT_NAME_BOLD			=			"Helvetica-Bold";
#endif

//#define XCODE_6
#define UI_TYPE_IOS8

#define FULL_SCREEN_ZOOM_SCALE      1.22f
#define TRANSITION_DURATION         1.0f
#define OPENING_DELAY_DURATION      1.65f

#define ENDING_DURATION             2.5f
#define OPENING_DURATION            3.0f
#define DURATION_SCENE01 25.375000f
#define DURATION_SCENE02 35.625000f
#define DURATION_SCENE03 29.209000f
#define DURATION_SCENE04 33.000000f
#define DURATION_SCENE05 33.125000f
#define DURATION_SCENE06 19.417000f
#define DURATION_SCENE07 12.959000f
#define DURATION_SCENE08 16.959000f
#define DURATION_SCENE09 20.084000f
#define DURATION_SCENE10 20.792000f
#define DURATION_TRANSITION 2

#define DEFAULT_VOLUME_BACKGROUND_SOUND 0.7f

const static int TAG_HOME		=	1000;
const static int TAG_HOME_SCENE		=	1001;
const static int TAG_BOOKMAP	=	2000;
const static int TAG_CAMERA     =   3000;
const static int TAG_VIDEO      =   4000;
const static int TAG_RECORDING  =   5000;
const static int TAG_RECORDING_SCENE  =   5001;
const static int TAG_PLAY_AND_RECORDING  =   6000;
const static int TAG_DIM		  	=   9999;

#define UDKEY_BOOL_APPFIRSTRUN      "AppFirstRun"
#define UDKEY_BOOL_FACEBOOKLOGIN    "FacebookLogin"
#define UDKEY_INTEGER_USERNO        "UserNumber"
#define UDKEY_STRING_AUTHKEY        "UserAuthKey"
#define UDKEY_STRING_LOGINID        "UserID"//==email?
#define UDKEY_STRING_LOGINPW        "UserPassword"
#define UDKEY_STRING_EMAIL          "UserEmail"
#define UDKEY_STRING_BIRTHDAY       "UserBirthDay"
#define UDKEY_INTEGER_GENDER        "UserGender"
#define UDKEY_STRING_LANGUAGE       "UserLanguage"
#define UDKEY_STRING_SNSCODE        "UserSNSCode"
#define UDKEY_STRING_SNSID          "UserSNSID"
#define UDKEY_STRING_SNSNAME        "UserSNSName"
#define UDKEY_STRING_COUNTRY        "UserContryCode"
#define UDKEY_STRING_UUID           "UserUUID"
#define UDKEY_STRING_CITY           "UserCity"
#define UDKEY_FLOAT_VOLUME          "SoundVolume"//0:mute / other:sound play...
#define UDKEY_DOUBLE_PROODUCTCACHEVER  "ProductCacheVersion"
#define UDKEY_STRING_PRODUCTSALES   "ProductSales"
#define UDKEY_INTEGER_BALANCE       "UserBalance"
#define UDKEY_STRING_PROMOTION_TM   "init_promotion_tm"
#define UDKEY_BOOL_DELETEACCOUNT    "deleteAccount"
#define UDKEY_BOOL_MUTEBGM          "mutebgm"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
const static int ANDROID_SCAN_QRCODE		= 0x800;
const static int ANDROID_PLAY_VIDEO			= 0x801;
const static int ANDROID_TAKE_PICTURE		= 0x802;
const static int ANDROID_LOGIN				= 0x804;
const static int ANDROID_DOLE_COIN			= 0x808;
const static int ANDROID_CREATE_CHARACTER	= 0x810;
const static int ANDROID_SETTING			= 0x811;
const static int ANDROID_SHARE					= 0x812;
const static int ANDROID_SHARE_FB				= 0x814;
const static int ANDROID_DOWNLOAD_TO_DEVICE		= 0x818;
const static int ANDROID_UNLOCK_ITEM_WITH_LOGIN	= 0x820;
const static int ANDROID_PENDING_PURCHASE		= 0x821;

const static int ANDROID_RECORD_START			= 0x8100;
const static int ANDROID_RECORD_STOP			= 0x8101;
const static int ANDROID_RECORD_PAUSE			= 0x8102;
const static int ANDROID_RECORD_RESUME			= 0x8103;
const static int ANDROID_RECORD_SAVE			= 0x8104;
const static int ANDROID_RECORD_DELETE			= 0x8105;

const static int ANDROID_PLAY_START				= 0x8800;
const static int ANDROID_PLAY_STOP				= 0x8801;
const static int ANDROID_PLAY_PAUSE				= 0x8802;
const static int ANDROID_PLAY_RESUME			= 0x8803;
#endif

const static int SCENE_COUNT_MAX				= 5;
#endif
