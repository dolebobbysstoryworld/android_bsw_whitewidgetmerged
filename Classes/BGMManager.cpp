//
//  BGMManager.cpp
//  bobby
//
//  Created by Lee mu hyeon on 2014. 9. 24..
//
//

#include "BGMManager.h"
#include "SimpleAudioEngine.h"
#include "Define.h"

USING_NS_CC;

#define FILENAME_BOBBY_BGM      "common/Bobby_BGM.mp3"

static TSBGMManager *g_bgmManager = nullptr;

TSBGMManager *TSBGMManager::getInstance() {
    if (g_bgmManager == nullptr) {
        g_bgmManager = new TSBGMManager();
    }
    return g_bgmManager;
}

TSBGMManager::TSBGMManager():
_isPlaying(false),
_isPaused(false) {
    this->setMute(UserDefault::getInstance()->getBoolForKey(UDKEY_BOOL_MUTEBGM));
}

void TSBGMManager::play() {
    log("===================================== enter play");
//    float volume = UserDefault::getInstance()->getFloatForKey(UDKEY_FLOAT_VOLUME);
//    if (volume == 0.0f) {
//        return;
//    }
    if (_isMute == true) {
        return;
    }
    
    if (_isPlaying == true) {
        return;
    }
    _isPlaying = true;
    _isPaused = false;
    log("===================================== bg play");
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(FILENAME_BOBBY_BGM, true);
}

void TSBGMManager::stop() {
    log("===================================== enter stop");
    if (_isPlaying == false) {
        return;
    }
    
    _isPlaying = false;
    _isPaused = false;
    log("===================================== bg stop");
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
}

void TSBGMManager::pause() {
    log("===================================== enter pause");
    if (_isPlaying == false) {
        return;
    }
    
    _isPaused = true;
    log("===================================== bg pause");
    CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

void TSBGMManager::resume() {
    log("===================================== enter resume");
    if (_isPlaying == false) {
        return;
    }

    if (_isPaused == false) {
        return;
    }
    _isPaused = false;
    log("===================================== bg resume");
    CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}

void TSBGMManager::setMute(bool mute) {
    _isMute = mute;
}