﻿#include <sys/time.h>

#include "AppDelegate.h"
#include "BGMManager.h"
#include "ResourceManager.h"
#include "Define.h"
#include "HomeScene.h"
#include "SimpleAudioEngine.h"
#include "external/json/document.h"
#include "external/json/prettywriter.h"
#include "external/json/stringbuffer.h"
#include "CharacterRecordingScene.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "CharacterRecordingScene.h"
#include "PlayAndRecordScene.h"
#include "JeVideoScene.h"

#include <platform/android/jni/JniHelper.h>
#endif

USING_NS_CC;
using namespace std;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
JNIEnv* thisEnv;
jobject thisObj;
int currentSceneId;

extern "C" {

	extern int mixAudio(const char* voice, const char* bgm, const char* outfile);

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_BobbysStoryWorldActivity_initJNIBridge(JNIEnv *env, jobject jobj) {
		thisEnv = env;
		thisObj = thisEnv->NewGlobalRef(jobj);
	}

    JNIEXPORT void JNICALL Java_org_cocos2dx_lib_Cocos2dxRenderer_nativeRenderForRecord(JNIEnv* env, jobject jobj, jboolean record) {
		auto director = Director::getInstance();
		auto scene = director->getRunningScene();
		if(scene != nullptr) {
			auto layer = scene->getChildByTag(TAG_PLAY_AND_RECORDING);
			if(layer != nullptr) {
				PlayAndRecord* current = dynamic_cast<PlayAndRecord*>(layer);
				if(current != nullptr) {
					current->changeRecordStatus(record == JNI_TRUE ? true : false);
				}
			}
		}
    }

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_BobbysStoryWorldActivity_nativeBackPress(JNIEnv *env, jobject jobj) {
		if(currentSceneId > 0) {
			auto director = Director::getInstance();
			auto layer = director->getRunningScene()->getChildByTag(currentSceneId);
			if(layer != nullptr) {
				Layer* current = dynamic_cast<Layer*>(layer);
				if(current != nullptr) {
					current->onKeyReleased(cocos2d::EventKeyboard::KeyCode::KEY_BACKSPACE, nullptr);
				}
			}
		}
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_PortraitActivity_nativeSaveAuthData(JNIEnv *env, jobject jobj, jstring auth, jint userNo) {
		const char *nativeAuth = env->GetStringUTFChars(auth, 0);

		UserDefault::getInstance()->setStringForKey(UDKEY_STRING_AUTHKEY, nativeAuth);
		UserDefault::getInstance()->setIntegerForKey(UDKEY_INTEGER_USERNO, (int)userNo);
        UserDefault::getInstance()->setBoolForKey(UDKEY_BOOL_DELETEACCOUNT, false);

		env->ReleaseStringUTFChars(auth, nativeAuth);
		
		auto director = Director::getInstance();
		switch(currentSceneId) {
			case TAG_HOME: {
				auto layerHome = director->getRunningScene()->getChildByTag(TAG_HOME);
				if(layerHome != nullptr) {
					Home* home = dynamic_cast<Home*>(layerHome);
					if(home != nullptr) {
						home->doleGetInventoryListProtocol();
					}
				}
			} break;

			case TAG_BOOKMAP: {
				auto layerBookmap = director->getRunningScene()->getChildByTag(TAG_BOOKMAP);
				if(layerBookmap != nullptr) {
					BookMap* bookmap = dynamic_cast<BookMap*>(layerBookmap);
					if(bookmap != nullptr) {
						bookmap->doleGetInventoryListProtocol();
					}
				}
			} break;

			case TAG_RECORDING: {
				auto layerRecord = director->getRunningScene()->getChildByTag(TAG_RECORDING);
				if(layerRecord != nullptr) {
					CharacterRecording* record = dynamic_cast<CharacterRecording*>(layerRecord);
					if(record != nullptr) {
						record->checkExecutePurchaseWithLogin();
					}
			}
			} break;
		}
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_BobbysStoryWorldActivity_nativeSaveAuthData(JNIEnv *env, jobject jobj, jstring auth, jint userNo) {
		const char *nativeAuth = env->GetStringUTFChars(auth, 0);

		UserDefault::getInstance()->setStringForKey(UDKEY_STRING_AUTHKEY, nativeAuth);
		UserDefault::getInstance()->setIntegerForKey(UDKEY_INTEGER_USERNO, (int)userNo);
        UserDefault::getInstance()->setBoolForKey(UDKEY_BOOL_DELETEACCOUNT, false);

		env->ReleaseStringUTFChars(auth, nativeAuth);
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_PortraitActivity_nativeClearAuthData(JNIEnv *env, jobject jobj) {
		
	}

	JNIEXPORT int JNICALL Java_com_dole_bobbysstoryworld_PortraitActivity_nativeDoleCoinSetBalance(JNIEnv *env, jobject jobj, jint balance) {
		// save dole coin balance?
		UserDefault::getInstance()->setIntegerForKey(UDKEY_INTEGER_BALANCE, (int)balance);

		int result = 0;
		auto director = Director::getInstance();
		auto scene = director->getRunningScene();
		if(scene != nullptr) {
			auto layer = scene->getChildByTag(TAG_HOME);
			if(layer != nullptr) {
				Home* current = dynamic_cast<Home*>(layer);
				if(current != nullptr) {
					current->renewDoleCoinAndroid((int)balance);
					result = 1;
				}
			}
		}

		return result;
	}

	JNIEXPORT int JNICALL Java_com_dole_bobbysstoryworld_PortraitActivity_nativeDoleCoinGetBalance(JNIEnv *env, jobject jobj) {
		return UserDefault::getInstance()->getIntegerForKey(UDKEY_INTEGER_BALANCE, -1);
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_PortraitActivity_nativeSyncData(JNIEnv *env, jobject jobj) {
		auto director = Director::getInstance();
		auto scene = director->getRunningScene();
		if(scene != nullptr) {
			auto layer = scene->getChildByTag(TAG_HOME);
			if(layer != nullptr) {
				Home* current = dynamic_cast<Home*>(layer);
				if(current != nullptr) {
//					current->doleCacheSyncVersionProtocol();
				}
			}
		}
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_BobbysStoryWorldActivity_nativeFinishEditAction(JNIEnv *env, jobject jobj, jstring title, jstring author) {
		const char *nativeTitle = env->GetStringUTFChars(title, 0);
		const char *nativeAuthor = env->GetStringUTFChars(author, 0);

		auto director = Director::getInstance();
		auto layer = director->getRunningScene()->getChildByTag(TAG_BOOKMAP);
		if(layer != nullptr) {
			BookMap* current = dynamic_cast<BookMap*>(layer);
			if(current != nullptr) {
				current->updateEditBoxText(nativeTitle, nativeAuthor);
			}
		}

		env->ReleaseStringUTFChars(title, nativeTitle);
		env->ReleaseStringUTFChars(author, nativeAuthor);
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_BobbysStoryWorldActivity_nativeFinishShareCommentEditAction(JNIEnv *env, jobject jobj,
									jint shareType, jstring comment) {
		const char *nativeComment = env->GetStringUTFChars(comment, 0);

		auto director = Director::getInstance();
		auto layer = director->getRunningScene()->getChildByTag(TAG_BOOKMAP);
		if(layer != nullptr) {
			BookMap* current = dynamic_cast<BookMap*>(layer);
			if(current != nullptr) {
				current->onShareCommentResult2((int)shareType, true /*nativeComment != nullptr && (strlen(nativeComment) > 0)*/, nativeComment);
			}
		}

		env->ReleaseStringUTFChars(comment, nativeComment);
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_BobbysStoryWorldActivity_nativeCancelEditAction(JNIEnv *env, jobject jobj) {
		auto director = Director::getInstance();
		auto layer = director->getRunningScene()->getChildByTag(TAG_BOOKMAP);
		if(layer != nullptr) {
			BookMap* current = dynamic_cast<BookMap*>(layer);
			if(current != nullptr) {
				current->clearEditMode();
			}
		}
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_BobbysStoryWorldActivity_nativeCustomCharacaterCallback(JNIEnv *env, jobject jobj, jint index, jstring path) {
		const char *nativePath = env->GetStringUTFChars(path, 0);
		auto director = Director::getInstance();
		auto layer = director->getRunningScene()->getChildByTag(currentSceneId);
		if(layer != nullptr) {
			if(currentSceneId == TAG_HOME) {
				Home* current = dynamic_cast<Home*>(layer);
				if(current != nullptr) {
					current->updateCustomCharacater((int)index, nativePath);
				}
			} else if(currentSceneId == TAG_RECORDING) {
				CharacterRecording* current = dynamic_cast<CharacterRecording*>(layer);
				if(current != nullptr) {
					current->updateCustomCharacater((int)index, nativePath);
				}
			}
		}

		env->ReleaseStringUTFChars(path, nativePath);
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_BobbysStoryWorldActivity_nativeCustomBackgroundCallback(JNIEnv *env, jobject jobj, jstring path) {
		const char *nativePath = env->GetStringUTFChars(path, 0);
		auto director = Director::getInstance();
		auto layer = director->getRunningScene()->getChildByTag(currentSceneId);
		if(layer != nullptr) {
			if(currentSceneId == TAG_HOME) {
				Home* current = dynamic_cast<Home*>(layer);
				if(current != nullptr) {
					current->updateCustomBackground(nativePath);
				}
			} else if(currentSceneId == TAG_RECORDING) {
				CharacterRecording* current = dynamic_cast<CharacterRecording*>(layer);
				if(current != nullptr) {
					current->updateCustomBackground(nativePath);
				}
			}
		}

		env->ReleaseStringUTFChars(path, nativePath);
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_BobbysStoryWorldActivity_nativeOnFinishVoiceRecording(JNIEnv *env, jobject jobj,
										jstring voice, jstring bgm, jstring output) {
		const char *nativeVoice = env->GetStringUTFChars(voice, 0);
		const char *nativeBgm = env->GetStringUTFChars(bgm, 0);
		const char *nativeOutput = env->GetStringUTFChars(output, 0);

		int ret = mixAudio(nativeVoice, nativeBgm, nativeOutput);

		env->ReleaseStringUTFChars(output, nativeOutput);
		env->ReleaseStringUTFChars(bgm, nativeBgm);
		env->ReleaseStringUTFChars(voice, nativeVoice);
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_BobbysStoryWorldActivity_nativeFacebookActionResult(JNIEnv *env, jobject jobj,
										jint result, jint type) {
		auto director = Director::getInstance();
		auto layer = director->getRunningScene()->getChildByTag(TAG_BOOKMAP);
		if(layer != nullptr) {
			BookMap* current = dynamic_cast<BookMap*>(layer);
			if(current != nullptr) {
				current->onFacebookActionResult((int)result, (int)type);
			}
		}
	}

	JNIEXPORT bool JNICALL Java_com_dole_bobbysstoryworld_PortraitActivity_nativeShowGuideHome(JNIEnv *env, jobject jobj) {
		bool result = false;
		auto director = Director::getInstance();
		auto scene = director->getRunningScene();
		if(scene != nullptr) {
			auto layer = scene->getChildByTag(TAG_HOME);
			if(layer != nullptr) {
				Home* current = dynamic_cast<Home*>(layer);
				if(current != nullptr) {
//					current->showGuide();
					current->pendingShowGuide();
					result = true;
				}
			}
		}

		return result;
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_BobbysStoryWorldActivity_nativeOnFinishVideoListPlay(JNIEnv *env, jobject jobj) {
		auto director = Director::getInstance();
		auto scene = director->getRunningScene();
		if(scene != nullptr) {
			auto layer = scene->getChildByTag(TAG_VIDEO);
			if(layer != nullptr) {
				JeVideo* current = dynamic_cast<JeVideo*>(layer);
				if(current != nullptr) {
					current->playEnding();
				}
			}
		}
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_BobbysStoryWorldActivity_nativeOnStartVideoListPlay(JNIEnv *env, jobject jobj) {
		auto director = Director::getInstance();
		auto scene = director->getRunningScene();
		if(scene != nullptr) {
			auto layer = scene->getChildByTag(TAG_VIDEO);
			if(layer != nullptr) {
				JeVideo* current = dynamic_cast<JeVideo*>(layer);
				if(current != nullptr) {
					current->changeTextureForEnding();
				}
			}
		}
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_BobbysStoryWorldActivity_nativeOnStopVideoListPlay(JNIEnv *env, jobject jobj) {
		auto director = Director::getInstance();
		auto scene = director->getRunningScene();
		if(scene != nullptr) {
			auto layer = scene->getChildByTag(TAG_VIDEO);
			if(layer != nullptr) {
				JeVideo* current = dynamic_cast<JeVideo*>(layer);
				if(current != nullptr) {
					current->movieDidFinishCallback();
				}
			}
		}
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_PortraitActivity_nativeHandleRecallType(JNIEnv *env, jobject jobj, jint type) {
		int recall = (int)type;
		auto director = Director::getInstance();
		auto scene = director->getRunningScene();
		Node* layer = nullptr;
		if(scene != nullptr) {
			switch(recall) {
				case ANDROID_SHARE:
				case ANDROID_DOWNLOAD_TO_DEVICE:
					layer = scene->getChildByTag(TAG_BOOKMAP);
					if(layer != nullptr) {
						BookMap* bookmap = dynamic_cast<BookMap*>(layer);
						if(bookmap != nullptr) {
							bookmap->checkRecall(recall);
						}
					}
					break;

				case ANDROID_PENDING_PURCHASE:
					layer = scene->getChildByTag(TAG_HOME);
					if(layer != nullptr) {
						Home* home = dynamic_cast<Home*>(layer);
						if(home != nullptr) {
							home->checkRecall(recall);
						}
					}
					break;
			}
		}
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_BobbysStoryWorldActivity_nativeOnFacebookShareComplete(JNIEnv *env, jobject jobj) {
		auto director = Director::getInstance();
		auto scene = director->getRunningScene();
		if(scene != nullptr) {
			auto layer = scene->getChildByTag(TAG_BOOKMAP);
			if(layer != nullptr) {
				BookMap* current = dynamic_cast<BookMap*>(layer);
				if(current != nullptr) {
					current->onFacebookShareComplete();
				}
			}
		}
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_BobbysStoryWorldActivity_nativeOnProgressUpdate(JNIEnv *env, jobject jobj, jlong progress) {
		auto director = Director::getInstance();
		auto scene = director->getRunningScene();
		if(scene != nullptr) {
			auto layer = scene->getChildByTag(TAG_BOOKMAP);
			if(layer != nullptr) {
				BookMap* current = dynamic_cast<BookMap*>(layer);
				if(current != nullptr) {
					current->onFacebookProgressUpdate((double)progress);
				}
			}
		}
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_BobbysStoryWorldActivity_nativeCancelYoutubeShare(JNIEnv *env, jobject jobj, jlong progress) {
		auto director = Director::getInstance();
		auto scene = director->getRunningScene();
		if(scene != nullptr) {
			auto layer = scene->getChildByTag(TAG_BOOKMAP);
			if(layer != nullptr) {
				BookMap* current = dynamic_cast<BookMap*>(layer);
				if(current != nullptr) {
					current->cancelYoutubeShare();
				}
			}
		}
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_BobbysStoryWorldActivity_nativeChangeBGMPlayStatus(JNIEnv *env, jobject jobj, jint status) {
		bool mute = (int)status == 0 ? false : true;
		UserDefault::getInstance()->setBoolForKey(UDKEY_BOOL_MUTEBGM, mute);
		TSBGMManager::getInstance()->setMute(mute);
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_PortraitActivity_nativeChangeBGMPlayStatus(JNIEnv *env, jobject jobj, jint status) {
		bool mute = (int)status == 0 ? false : true;
		UserDefault::getInstance()->setBoolForKey(UDKEY_BOOL_MUTEBGM, mute);
		TSBGMManager::getInstance()->setMute(mute);
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_BobbysStoryWorldActivity_nativePlayBGM(JNIEnv *env, jobject jobj) {
		TSBGMManager::getInstance()->play();
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_PortraitActivity_nativePlayBGM(JNIEnv *env, jobject jobj) {
		TSBGMManager::getInstance()->play();
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_BobbysStoryWorldActivity_nativeStopBGM(JNIEnv *env, jobject jobj) {
		TSBGMManager::getInstance()->stop();
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_PortraitActivity_nativeStopBGM(JNIEnv *env, jobject jobj) {
		TSBGMManager::getInstance()->stop();
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_PortraitActivity_nativeResumeBGM(JNIEnv *env, jobject jobj) {
		TSBGMManager::getInstance()->resume();
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_PortraitActivity_nativePauseBGM(JNIEnv *env, jobject jobj) {
		TSBGMManager::getInstance()->pause();
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_PortraitActivity_nativeResumeAudioEngine(JNIEnv *env, jobject jobj) {
		CocosDenshion::SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
	}

	JNIEXPORT void JNICALL Java_com_dole_bobbysstoryworld_PortraitActivity_nativePauseAudioEngine(JNIEnv *env, jobject jobj) {
		CocosDenshion::SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
	}
}
#endif

AppDelegate::AppDelegate() {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	_bNeedCheckUpdate = false;
#endif
}

AppDelegate::~AppDelegate() 
{
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    auto director = Director::getInstance();
    ResourceManager::getInstance();//릴리즈 모드
    
    // turn on display FPS
    // director->setDisplayStats(true);
    // set FPS. the default value is 1.0/60 if you don't call this
    // director->setAnimationInterval(1.0 / 60);

    
    this->loadInitialData();
    this->createInitialUserData();
    
    // create a scene. it's an autorelease object
    auto scene = Home::createScene();

    // run
    director->runWithScene(scene);
    
    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    CocosDenshion::SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
#endif
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    auto runningScene = Director::getInstance()->getRunningScene();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    if(runningScene == nullptr) {
    	_bNeedCheckUpdate = true;
    	return;
    }
#endif

    if (runningScene != nullptr) {
        if (runningScene->getTag() == TAG_HOME_SCENE) {
//            auto initPromotionTime = UserDefault::getInstance()->getStringForKey(UDKEY_STRING_PROMOTION_TM);
//            std::string initProStr(initPromotionTime);
//            struct tm initT;
//            strptime(initProStr.c_str(), "%Y-%m-%d %H:%M", &initT);
//            
//            time_t initProTm;
//            time(&initProTm);
//            struct tm *initTm = localtime(&initProTm);
//            
//            time_t currentT;
//            time(&currentT);
//            struct tm *currentTm = localtime(&currentT);
//            std::string currentTmStr(asctime(currentTm));
//            log("currentTm->tm_year : %d initTm.tm_year : %d", currentTm->tm_year,initTm->tm_year);
//            log("currentTm->tm_mon : %d initTm.tm_mon : %d", currentTm->tm_mon,initTm->tm_mon);
//            log("currentTm->tm_mday : %d initTm.tm_mday : %d", currentTm->tm_mday,initTm->tm_mday);
//            
//            Home *home = (Home*)runningScene->getChildByTag(TAG_HOME);
//            if (home) {
//                if (strcmp(initPromotionTime.c_str(), "") || currentTm->tm_year > initTm->tm_year || currentTm->tm_mon > initTm->tm_mon || currentTm->tm_mday > initTm->tm_mday) {
//                    home->showPromotionPopup();
//                }
//                home->getUpdateVersion();
//            }
            
            Home *home = (Home*)runningScene->getChildByTag(TAG_HOME);
            if (home) {
                home->getUpdateVersion();
                home->reloadScrollViews();
            }
        } else if (runningScene->getTag() == TAG_RECORDING_SCENE) {
            CharacterRecording *recording = (CharacterRecording*)runningScene->getChildByTag(TAG_RECORDING);
            if (recording) {
                recording->reloadScrollViews();
            }
        } else {
            
        }
    }
    
    // if you use SimpleAudioEngine, it must resume here
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    CocosDenshion::SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
#endif
}

void AppDelegate::loadInitialData()
{
    // read meta data
    std::string filePath = FileUtils::getInstance()->fullPathForFilename("json/meta.json");
    std::string contentStr = FileUtils::getInstance()->getStringFromFile(filePath);
    rapidjson::Document doc;
    doc.Parse<0>(contentStr.c_str());
    
    auto jsonLastUpdateTime = doc["last_update"].GetDouble();
    
    auto ud = UserDefault::getInstance();
    auto localUpdateTime = ud->getDoubleForKey(LAST_UPDATE_KEY, 0.0);
    if (localUpdateTime == 0.0 || localUpdateTime > jsonLastUpdateTime)
    {
        // need to insert initial json data to user default
        log("start to load initial json data");
        
        rapidjson::StringBuffer strbuf;
        rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
        
        // load order json
        std::string orderFilePath = FileUtils::getInstance()->fullPathForFilename("json/order.json");
        std::string orderContentStr = FileUtils::getInstance()->getStringFromFile(orderFilePath);
        rapidjson::Document order;
        order.Parse<0>(orderContentStr.c_str());
        
        // load books json
        rapidjson::Value& bookOrderArray = order["book_order"];
        std::string booksFilePath = FileUtils::getInstance()->fullPathForFilename("json/books.json");
        std::string booksContentStr = FileUtils::getInstance()->getStringFromFile(booksFilePath);
        rapidjson::Document books;
        books.Parse<0>(booksContentStr.c_str());
        
        auto bookOrderSize = bookOrderArray.Size();
        char* bookKey;
        for (int index = 0; index < bookOrderSize; index++) {
            bookKey = (char*)bookOrderArray[rapidjson::SizeType(index)].GetString();
            
            rapidjson::Value& booksData = books[bookKey];
            booksData.Accept(writer);
            ud->setStringForKey(bookKey, strbuf.GetString());
            strbuf.Clear();
        }

        // load resources json
        std::string resourcesFilePath = FileUtils::getInstance()->fullPathForFilename("json/resources.json");
        std::string resourcesContentStr = FileUtils::getInstance()->getStringFromFile(resourcesFilePath);
        rapidjson::Document resources;
        resources.Parse<0>(resourcesContentStr.c_str());
        
        vector<std::string> resourceKeys;
        resourceKeys.push_back("character");
        resourceKeys.push_back("custom_character");
        resourceKeys.push_back("background");
        resourceKeys.push_back("sound");
        
        for (int index = 0; index < resourceKeys.size(); index++) {
            // load character json
            std::string resourceName = resourceKeys.at(index);
            rapidjson::Value& arr = resources[resourceName.c_str()];
            
            std::string orderName = resourceName+"_order";
            rapidjson::Value& orderArray = order[orderName.c_str()];
            
            auto orderArraySize = orderArray.Size();
            char* key;
            for (int index = 0; index < orderArraySize; index++) {
                key = (char*)orderArray[rapidjson::SizeType(index)].GetString();
                rapidjson::Value& resourceData = arr[key];
                resourceData.Accept(writer);
                ud->setStringForKey(key, strbuf.GetString());
                strbuf.Clear();
            }
        }
        resourceKeys.clear();
        unsigned long int timestamp= time(NULL);
        log("update finished : %ld", timestamp);
        ud->setDoubleForKey(LAST_UPDATE_KEY, (double)timestamp);
        ud->setStringForKey(PRE_LOAD_OBJ_ORDER_KEY, orderContentStr);
        ud->flush();
    }
}

void AppDelegate::createInitialUserData()
{
    auto ud = UserDefault::getInstance();
    auto orderJsonString = ud->getStringForKey(ORDER_KEY, "");
    if (orderJsonString.length() == 0) {
        orderJsonString = "{ \
                            \"book_order\" : [], \
                            \"character_order\" : [], \
                            \"background_order\" : [], \
                            \"sound_order\" : [] \
                            }";
        ud->setStringForKey(ORDER_KEY, orderJsonString);
        ud->flush();
    }
}

void AppDelegate::resetToInitialData()
{
    // need to insert initial json data to user default
    log("reset to initial json data!");
 
    auto ud = UserDefault::getInstance();

    rapidjson::StringBuffer strbuf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
    
    // load order json
    std::string orderFilePath = FileUtils::getInstance()->fullPathForFilename("json/order.json");
    std::string orderContentStr = FileUtils::getInstance()->getStringFromFile(orderFilePath);
    rapidjson::Document order;
    order.Parse<0>(orderContentStr.c_str());
    
    // load resources json
    std::string resourcesFilePath = FileUtils::getInstance()->fullPathForFilename("json/resources.json");
    std::string resourcesContentStr = FileUtils::getInstance()->getStringFromFile(resourcesFilePath);
    rapidjson::Document resources;
    resources.Parse<0>(resourcesContentStr.c_str());
    
    vector<std::string> resourceKeys;
    resourceKeys.push_back("character");
    resourceKeys.push_back("custom_character");
    resourceKeys.push_back("background");
    
    for (int index = 0; index < resourceKeys.size(); index++) {
        // load character json
        std::string resourceName = resourceKeys.at(index);
        rapidjson::Value& arr = resources[resourceName.c_str()];
        
        std::string orderName = resourceName+"_order";
        rapidjson::Value& orderArray = order[orderName.c_str()];
        
        auto orderArraySize = orderArray.Size();
        char* key;
        for (int index = 0; index < orderArraySize; index++) {
            key = (char*)orderArray[rapidjson::SizeType(index)].GetString();
            rapidjson::Value& resourceData = arr[key];
            resourceData.Accept(writer);
            ud->setStringForKey(key, strbuf.GetString());
            strbuf.Clear();
        }
    }
    resourceKeys.clear();
    ud->flush();
}

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
void AppDelegate::showPlatformView(int type, int sceneId)
{
	currentSceneId = sceneId;
	JniMethodInfo info;
	bool foundMethod = false;
	switch(type) {
		case ANDROID_SCAN_QRCODE:
			foundMethod = JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "startQRCodeScan", "()V");
			break;

//		case ANDROID_PLAY_VIDEO:
//			foundMethod = JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "startPlayVideo", "()V");
//			break;

		case ANDROID_TAKE_PICTURE:
			foundMethod = JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "startTakePicture", "()V");
			break;

		case ANDROID_LOGIN:
			foundMethod = JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "showLogin", "()V");
			break;

		case ANDROID_DOLE_COIN:
			foundMethod = JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "showDoleCoin", "()V");
			break;

		case ANDROID_CREATE_CHARACTER:
			foundMethod = JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "startCreateCharacter",
					"([Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V");
			break;

		case ANDROID_SETTING:
			foundMethod = JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "showSetting", "()V");
			break;
	}

	if(foundMethod) {
		if(type == ANDROID_CREATE_CHARACTER) {
		    auto ud = UserDefault::getInstance();
		    auto orderContentStr = ud->getStringForKey(PRE_LOAD_OBJ_ORDER_KEY);
		    rapidjson::Document orderDocument;
		    orderDocument.Parse<0>(orderContentStr.c_str());
		    rapidjson::Value& order = orderDocument["character_order"];

		    int characterCount = order.Size();

			jobjectArray characterKeyArray = info.env->NewObjectArray(characterCount, info.env->FindClass("java/lang/String"), info.env->NewStringUTF(""));
			jobjectArray imageArray = info.env->NewObjectArray(characterCount, info.env->FindClass("java/lang/String"), info.env->NewStringUTF(""));
			jobjectArray lockArray = info.env->NewObjectArray(characterCount, info.env->FindClass("java/lang/String"), info.env->NewStringUTF(""));

		    for(int index = 0; index < characterCount ; index++){
		        auto characterKey = __String::create(order[rapidjson::SizeType(index)].GetString());
		        auto characterJsonString = ud->getStringForKey(characterKey->getCString());
		        rapidjson::Document characterInfo;
		        characterInfo.Parse<0>(characterJsonString.c_str());
		        bool isLock = characterInfo["locked"].GetBool();
		        auto image = "";
		        if(characterInfo.HasMember("custom_normal"))
		        	image = characterInfo["custom_normal"].GetString();

				info.env->SetObjectArrayElement(characterKeyArray, index, info.env->NewStringUTF(characterKey->getCString()));
				info.env->SetObjectArrayElement(imageArray, index, info.env->NewStringUTF(image));
				info.env->SetObjectArrayElement(lockArray, index, (isLock ? info.env->NewStringUTF("1") : info.env->NewStringUTF("0")));
		    }

			info.env->CallVoidMethod(thisObj, info.methodID, characterKeyArray, imageArray, lockArray);
			info.env->DeleteLocalRef(info.classID);
		} else {
			info.env->CallVoidMethod(thisObj, info.methodID);
			info.env->DeleteLocalRef(info.classID);
		}
	}
}

void AppDelegate::setPendingAction(int type)
{
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "setPendingAction", "(I)V")) {
		info.env->CallVoidMethod(thisObj, info.methodID, type);
		info.env->DeleteLocalRef(info.classID);
	}
}

void AppDelegate::createStoryStart(int tag, int finalCounter){
    JniMethodInfo info;
    if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "createStoryStart", "(II)V")) {
        info.env->CallVoidMethod(thisObj, info.methodID, tag, finalCounter);
        info.env->DeleteLocalRef(info.classID);
    }
}


void AppDelegate::createStorySuccess(){
    JniMethodInfo info;
    if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "createStorySuccess", "()V")) {
        info.env->CallVoidMethod(thisObj, info.methodID);
        info.env->DeleteLocalRef(info.classID);
    }
}

void AppDelegate::sharedVideoFacebook(){
    JniMethodInfo info;
    if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "sharedVideoFacebook", "()V")) {
        info.env->CallVoidMethod(thisObj, info.methodID);
        info.env->DeleteLocalRef(info.classID);
    }
}

void AppDelegate::sharedVideoYoutube(){
    JniMethodInfo info;
    if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "sharedVideoYoutube", "()V")) {
        info.env->CallVoidMethod(thisObj, info.methodID);
        info.env->DeleteLocalRef(info.classID);
    }
}

void AppDelegate::purchasedCharacter(const char* purchaseKey)
{
    JniMethodInfo info;
    if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "purchasedCharacter", "(Ljava/lang/String;)V")) {
        jstring jstrFilename = info.env->NewStringUTF(purchaseKey);
        info.env->CallVoidMethod(thisObj, info.methodID, jstrFilename);
        info.env->DeleteLocalRef(jstrFilename);
        info.env->DeleteLocalRef(info.classID);
    }
}

void AppDelegate::purchasedBackground(const char* purchaseKey)
{
    JniMethodInfo info;
    if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "purchasedBackground", "(Ljava/lang/String;)V")) {
        jstring jstrFilename = info.env->NewStringUTF(purchaseKey);
        info.env->CallVoidMethod(thisObj, info.methodID, jstrFilename);
        info.env->DeleteLocalRef(jstrFilename);
        info.env->DeleteLocalRef(info.classID);
    }
}

void AppDelegate::showBook(const char* bookKey)
{
    JniMethodInfo info;
    if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "didOpenBook", "(Ljava/lang/String;)V")) {
        jstring jstrFilename = info.env->NewStringUTF(bookKey);
        info.env->CallVoidMethod(thisObj, info.methodID, jstrFilename);
        info.env->DeleteLocalRef(jstrFilename);
        info.env->DeleteLocalRef(info.classID);
    }
}

void AppDelegate::showBookA(){
    JniMethodInfo info;
    if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "didOpenBookA", "()V")) {
        info.env->CallVoidMethod(thisObj, info.methodID);
        info.env->DeleteLocalRef(info.classID);
    }
}

void AppDelegate::showBookB(){
    JniMethodInfo info;
    if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "didOpenBookB", "()V")) {
        info.env->CallVoidMethod(thisObj, info.methodID);
        info.env->DeleteLocalRef(info.classID);
    }
}

void AppDelegate::createdCustomBook(){
    JniMethodInfo info;
    if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "didCreateCustomBook", "()V")) {
        info.env->CallVoidMethod(thisObj, info.methodID);
        info.env->DeleteLocalRef(info.classID);
    }
}

void AppDelegate::showPlatformVideoView(const char* filename, int sceneId)
{
	currentSceneId = sceneId;
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "startPlayVideo", "(Ljava/lang/String;)V")) {
		jstring jstrFilename = info.env->NewStringUTF(filename);
		info.env->CallVoidMethod(thisObj, info.methodID, jstrFilename);
		info.env->DeleteLocalRef(jstrFilename);
		info.env->DeleteLocalRef(info.classID);
	}
}

void AppDelegate::showPlatformVideoViewWithList(int count, const char* list[], int sceneId)
{
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "startPlayVideoList", "([Ljava/lang/String;)V")) {
		jobjectArray arrayList = info.env->NewObjectArray(count, info.env->FindClass("java/lang/String"), info.env->NewStringUTF(""));
		for(int i = 0 ; i < count ; i++) {
			info.env->SetObjectArrayElement(arrayList, i, info.env->NewStringUTF(list[i]));
		}
		info.env->CallVoidMethod(thisObj, info.methodID, arrayList);
		info.env->DeleteLocalRef(info.classID);
	}
}

void AppDelegate::playVideoList(bool start, int sceneId)
{
	currentSceneId = sceneId;
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "playVideoList", "(I)V")) {
		info.env->CallVoidMethod(thisObj, info.methodID, (start ? 1 : 0));
		info.env->DeleteLocalRef(info.classID);
	}
}

bool AppDelegate::hidePlatformView()
{
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "hideView", "()Z")) {
		jboolean hide = info.env->CallBooleanMethod(thisObj, info.methodID);
		info.env->DeleteLocalRef(info.classID);
		return hide;
	}
	return true;
}

void AppDelegate::setEditTextContents(const char* title, const char* author, int sceneId)
{
	currentSceneId = sceneId;
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "setEditTextContents", "([Ljava/lang/String;)V")) {
		jobjectArray array = info.env->NewObjectArray(2, info.env->FindClass("java/lang/String"), info.env->NewStringUTF(""));
		info.env->SetObjectArrayElement(array, 0, info.env->NewStringUTF(title));
		info.env->SetObjectArrayElement(array, 1, info.env->NewStringUTF(author));
		info.env->CallVoidMethod(thisObj, info.methodID, array);
		info.env->DeleteLocalRef(info.classID);
	}
}

void AppDelegate::changeVoiceRecordStatus(int status, const char* filename, int sceneId)
{
	currentSceneId = sceneId;
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "changeVoiceRecordStatus", "(Ljava/lang/String;I)V")) {
		jstring jstrFilename = info.env->NewStringUTF(filename);
		info.env->CallVoidMethod(thisObj, info.methodID, jstrFilename, status);
		info.env->DeleteLocalRef(jstrFilename);
		info.env->DeleteLocalRef(info.classID);
	}
}

void AppDelegate::changeVoicePlayStatus(int status, const char* filename, int sceneId)
{
	currentSceneId = sceneId;
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "changeVoicePlayStatus", "(Ljava/lang/String;I)V")) {
		jstring jstrFilename = info.env->NewStringUTF(filename);
		info.env->CallVoidMethod(thisObj, info.methodID, jstrFilename, status);
		info.env->DeleteLocalRef(jstrFilename);
		info.env->DeleteLocalRef(info.classID);
	}
}

void AppDelegate::changeBGMPlayStatus(int status, const char* filename, int sceneId)
{
	currentSceneId = sceneId;
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "changeBGMPlayStatus", "(Ljava/lang/String;I)V")) {
		jstring jstrFilename = info.env->NewStringUTF(filename);
		info.env->CallVoidMethod(thisObj, info.methodID, jstrFilename, status);
		info.env->DeleteLocalRef(jstrFilename);
		info.env->DeleteLocalRef(info.classID);
	}
}

void AppDelegate::startVideoRecord(int width, int height, int sceneId)
{
	currentSceneId = sceneId;
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "startVideoRecord", "(II)V")) {
		info.env->CallVoidMethod(thisObj, info.methodID, width, height);
		info.env->DeleteLocalRef(info.classID);
	}
}

void AppDelegate::stopVideoRecord()
{
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "stopVideoRecord", "()V")) {
		info.env->CallVoidMethod(thisObj, info.methodID);
		info.env->DeleteLocalRef(info.classID);
	}
}

void AppDelegate::pauseVideoRecord()
{
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "changeVideoRecordStatus", "(I)V")) {
		info.env->CallVoidMethod(thisObj, info.methodID, 1);
		info.env->DeleteLocalRef(info.classID);
	}
}

void AppDelegate::resumeVideoRecord()
{
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "changeVideoRecordStatus", "(I)V")) {
		info.env->CallVoidMethod(thisObj, info.methodID, 0);
		info.env->DeleteLocalRef(info.classID);
	}
}

void AppDelegate::feedAudioFileName(const char* voice[])
{
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "feedAudioFileName", "([Ljava/lang/String;)V")) {
		jobjectArray arrayVoice = info.env->NewObjectArray(SCENE_COUNT_MAX, info.env->FindClass("java/lang/String"), info.env->NewStringUTF(""));
//		jobjectArray arrayBgm = info.env->NewObjectArray(SCENE_COUNT_MAX, info.env->FindClass("java/lang/String"), info.env->NewStringUTF(""));
		for(int i = 0 ; i < SCENE_COUNT_MAX ; i++) {
			info.env->SetObjectArrayElement(arrayVoice, i, info.env->NewStringUTF(voice[i]));
//			info.env->SetObjectArrayElement(arrayBgm, i, info.env->NewStringUTF(bgm[i]));
		}
		info.env->CallVoidMethod(thisObj, info.methodID, arrayVoice);
		info.env->DeleteLocalRef(info.classID);
	}
}

int AppDelegate::getPendingDoleCoin()
{
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "getPendingDoleCoin", "()I")) {
		return (int)info.env->CallIntMethod(thisObj, info.methodID);
	}
	return -1;
}

bool AppDelegate::getPendingGuideHome()
{
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "getPendingGuideHome", "()Z")) {
		return (info.env->CallBooleanMethod(thisObj, info.methodID) == JNI_TRUE ? true : false);
	}
	return false;
}

bool AppDelegate::needCheckUpdate()
{
	if(_bNeedCheckUpdate) {
		_bNeedCheckUpdate = false;
		return true;
	}
	return false;
}

void AppDelegate::updateAuthKey(const char* authKey)
{
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "updateAuthKey", "(Ljava/lang/String;)V")) {
		jstring jstrAuthKey = info.env->NewStringUTF(authKey);
		info.env->CallVoidMethod(thisObj, info.methodID, jstrAuthKey);
		info.env->DeleteLocalRef(jstrAuthKey);
		info.env->DeleteLocalRef(info.classID);
	}
}

std::string AppDelegate::getRecordingFilename()
{
	std::string ret("");
	JniMethodInfo info;
	if(JniHelper::getStaticMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "getRecordingFilename", "()Ljava/lang/String;")) {
		jstring filename = (jstring)info.env->CallStaticObjectMethod(info.classID, info.methodID);
		info.env->DeleteLocalRef(info.classID);
        ret = JniHelper::jstring2string(filename);
        info.env->DeleteLocalRef(filename);
	}
	return ret;
}

void AppDelegate::saveVideoToDevice(const char* file, const char* title, const char* author)
{
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "saveVideoToDevice", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V")) {
		jstring jstrFile = info.env->NewStringUTF(file);
		jstring jstrTitle = info.env->NewStringUTF(title);
		jstring jstrAuthor = info.env->NewStringUTF(author);
		info.env->CallVoidMethod(thisObj, info.methodID, jstrFile, jstrTitle, jstrAuthor);
		info.env->DeleteLocalRef(jstrFile);
		info.env->DeleteLocalRef(jstrTitle);
		info.env->DeleteLocalRef(jstrAuthor);
		info.env->DeleteLocalRef(info.classID);
	}
}

void AppDelegate::onRecordSceneChange()
{
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "onRecordSceneChange", "()V")) {
		info.env->CallVoidMethod(thisObj, info.methodID);
		info.env->DeleteLocalRef(info.classID);
	}
}

void AppDelegate::checkFacebookPermission()
{
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "checkFacebookPermission", "()V")) {
		info.env->CallVoidMethod(thisObj, info.methodID);
		info.env->DeleteLocalRef(info.classID);
	}
}

void AppDelegate::uploadVideoToFacebook(const char* title, const char* description, const char* path)
{
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "uploadVideoToFacebook",
			"(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V")) {
		jstring jstrTitle = info.env->NewStringUTF(title);
		jstring jstrDescription = info.env->NewStringUTF(description);
		jstring jstrPath = info.env->NewStringUTF(path);
		info.env->CallVoidMethod(thisObj, info.methodID, jstrTitle, jstrDescription, jstrPath);
		info.env->DeleteLocalRef(jstrTitle);
		info.env->DeleteLocalRef(jstrDescription);
		info.env->DeleteLocalRef(jstrPath);
		info.env->DeleteLocalRef(info.classID);
	}
}

bool AppDelegate::showPromotionPopup()
{
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "showPromotionPopup", "()Z")) {
		bool ret = (info.env->CallBooleanMethod(thisObj, info.methodID) == JNI_TRUE ? true : false);
		info.env->DeleteLocalRef(info.classID);
		return ret;
	}
	return false;
}

void AppDelegate::loadPromotionPage(const char* url)
{
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "showPromotionScreen", "(Ljava/lang/String;)V")) {
		jstring jstrUrl = info.env->NewStringUTF(url);
		info.env->CallVoidMethod(thisObj, info.methodID, jstrUrl);
		info.env->DeleteLocalRef(jstrUrl);
		info.env->DeleteLocalRef(info.classID);
	}
}

void AppDelegate::removeLogo()
{
	JniMethodInfo info;
	if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "removeLogo", "()V")) {
		info.env->CallVoidMethod(thisObj, info.methodID);
		info.env->DeleteLocalRef(info.classID);
	}
}

void AppDelegate::mixpanelTrack(const char* eventName, const char* dictString)
{
    JniMethodInfo info;
    if(JniHelper::getMethodInfo(info, "com/dole/bobbysstoryworld/BobbysStoryWorldActivity", "mixpanelTrack", "(Ljava/lang/String;Ljava/lang/String;)V")) {
        jstring jstrEvent = info.env->NewStringUTF(eventName);
        jstring jstrDict = info.env->NewStringUTF(dictString);
        info.env->CallVoidMethod(thisObj, info.methodID, jstrEvent, jstrDict);
        info.env->DeleteLocalRef(jstrEvent);
        info.env->DeleteLocalRef(jstrDict);
        info.env->DeleteLocalRef(info.classID);
    }
}
#endif
