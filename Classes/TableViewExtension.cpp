//
//  TableViewExtension.cpp
//  bobby
//
//  Created by Dongwook, Kim on 14. 4. 10..
//
//

#include "TableViewExtension.h"

USING_NS_CC;

TableViewExtension* TableViewExtension::create(TableViewDataSource* dataSource, Size size)
{
    TableViewExtension *table = new TableViewExtension();
    table->initWithViewSize(size, NULL);
    table->autorelease();
    table->setDataSource(dataSource);
    table->_updateCellPositions();
    table->_updateContentSize();
    return table;
}

void TableViewExtension::onTouchEnded (Touch *pTouch, Event *pEvent)
{
    if (!this->isVisible()) {
        return;
    }
    
    if (_touchedCell){
		Rect bb = this->getBoundingBox();
		bb.origin = _parent->convertToWorldSpace(bb.origin);
        
		if (bb.containsPoint(pTouch->getLocation()) && _tableViewDelegate != NULL)
        {
            _tableViewDelegate->tableCellUnhighlight(this, _touchedCell);
            _tableViewDelegate->tableCellTouched(this, _touchedCell);
            if (_tableViewExtensionDelegate != NULL) {
                _tableViewExtensionDelegate->tableCellTouched(this, _touchedCell, pTouch, pEvent);
            }
        }
        
        _touchedCell = NULL;
    }
    
    ScrollView::onTouchEnded(pTouch, pEvent);
}

