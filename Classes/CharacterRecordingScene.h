//
//  CharacterRecordingScene.h
//  bobby
//
//  Created by Dongwook, Kim on 14. 5. 26..
//
//

#ifndef __bobby__CharacterRecordingScene__
#define __bobby__CharacterRecordingScene__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "Character.h"
#include "PopupLayer.h"
#include "ScrollViewExtension.h"
#include "ui/CocosGUI.h"
#include "CharacterScrollView.h"
#include "BackgroundScrollView.h"
#include "SoundScrollView.h"
#include <chrono>
#include "CustomCameraScene.h"
#include "TSDoleProtocol.h"

USING_NS_CC;
USING_NS_CC_EXT;

enum class RecordingViewMode {
    SetPosition,
    Recording,
    RecordingComplete,
    Playing
};

enum class UnlockItemType {
    ItemTypeCharacter,
    ItemTypeBG
};

class Home;
class CharacterRecording;
class CharacterRecordingDelegate
{
public:
    virtual ~CharacterRecordingDelegate() {}
    virtual void characterPhotoSavedInRecordScene(std::string characterKey) = 0;
    virtual void bgPhotoSavedInRecordScene(std::string bgKey) = 0;
};

class CharacterRecording : public cocos2d::Layer, PopupLayerDelegate, ScrollViewDelegate, CharacterScrollViewDelegate, BackgroundScrollViewDelegate, CustomCameraDelegate, SoundScrollViewDelegate, TSDoleProtocolDelegate
{
    
public:
    //    static cocos2d::Scene* createScene();
    static cocos2d::Scene* createScene(__String* bookKey, int chapterIndex);
    static CharacterRecording* create(__String* bookKey, int chapterIndex);

    virtual bool init();
    ~CharacterRecording();
    
    Node *createRecordingIcon();
    Node *createTrashItem();
    Menu *createRecordingMenu();
    Menu *createDoneMenu();
    Menu *createBottomMenu();
    Menu *createCompleteMenu();
    void createProgressBar();

    void unlockTalkBoxCallback(Ref* pSender);

    void btnBackCallback(Ref* pSender);
    void itemButtonClicked(Ref* obj);
    void doneMenuCallback(Ref* pSender);
    void rerecordMenuCallback(Ref* pSender);
    void saveMenuCallback(Ref* pSender);
    void playMenuCallback(Ref* pSender);

    void popupButtonClicked(PopupLayer *popup, cocos2d::Ref *obj);
    
    bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event *event);
    void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event *event);
    void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event *event);
    void onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event *event);

    cocos2d::Node *createResizableButton(const std::string& leftFileName, const std::string& centerFileName, const std::string& rightFileName,
                                         const std::string& iconFileName, const std::string& text, float fontSize, cocos2d::Point iconPos,
                                         float textLeftPadding, float textRightPadding);

    
    void setViewMode(RecordingViewMode viewMode);
    void saveCharacterProperty();
    void restoreCharacterProperty();
    void addMarkToProgress(Character *newChar);
    void markLastPseudoTouch();

    void saveRecordData(bool publish);
    void clearRecordData();
    void clearMark();

    void setProgress(float value);

    void finishRecording();
    void finishPlaying();
    void play();
    void pausePlay();
    void resumePlay();

    virtual void update(float delta);
    void bringToFront(cocos2d::Sprite* pSender);
    void recordingMenuCallback(Ref* sender);
    
    void showReUsePopup();

    void showUnlockTalkBox(UnlockItemType type, int period);
    void closeUnlockTalkbox();

    
    void showTransparentLayer();
    void removeTransparentLayer();
    void removeSelectedLockCharacter();
    void removeSelectedLockBackground();
    
    void scrollViewDidScroll(ScrollView* view);
    void scrollViewDidZoom(ScrollView* view);
    
    void showBackgroundScrollView();
    void showCharacterScrollView();
    void showSoundScrollView();
    void hideBottomScrollView();
    
    void onSelectCharacter(CharacterScrollView* view, Node* selectedItem, __String* characterKey, int index);
    void onSelectBackground(BackgroundScrollView* view, Node* selectedItem, __String* backgroundKey, int index);
    void onSelectSound(SoundScrollView* view, Node* selectedItem, __String* soundKey, int index);
    
    bool isScrollViewVisible();
    
    void addCharacterToScreen(__String* charKey);
    
    void framePressedCallback(Ref* pSender);
    void characterPressedCallback(Ref* pSender);

    bool loadPrevData(int chapterIndex);
    void clearPrevData();

    void lockMultiTouchAllCharacter();
    void unlockMultiTouchAllCharacter();
    void changeBackground(__String *backgroundKey);
    void changeSound(__String *soundKey);
    void onFinishedPurchaseProcess(bool success, char* item);

    void doleGetInventoryListProtocol();

    CREATE_FUNC(CharacterRecording);
    
    int chapterIndex;
    __String* bookKey;
    
    RecordingViewMode viewMode;

    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);

    virtual void onEnter();
    virtual void onExit();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    void updateCustomCharacater(int index, const char* path);
    void updateCustomBackground(const char* path);
    void checkExecutePurchaseWithLogin();
#endif
    
    void characterPhotoSaved(std::string characterKey);
    void bgPhotoSaved(std::string bgKey);
    void cameraScenePopped();
    
    CharacterRecordingDelegate *getDelegate() { return _delegate; }
    void setDelegate(CharacterRecordingDelegate *pDelegate) { _delegate = pDelegate; }
    
    void setHome(Home *pHome) { _homeLayer = pHome; }
    Home *getHome() { return _homeLayer; }
    
    void reloadScrollViews();
    
protected:
    CharacterRecordingDelegate *_delegate;

private:
    void recordTouch(cocos2d::Touch *touch, TouchStatus status);
    void playTouch();
    void onPlayTouch(float df);
    
    void showDimLayer(bool show);
    void deleteVoiceFile();
    void clearVoiceRecorder();

    bool isBackgroundScrollViewVisible;
    bool isCharacterScrollViewVisible;
    bool isSoundScrollViewVisible;
    
    bool scrollViewToggleAnimation;

    void createButtonClicked(Ref* obj, ui::TouchEventType type);
    Label*createTitle(int chapterIndex);

    int touchNumber;
    Vector<Touch*> touches;
    
    Layer *transparentLayer;
    Layer *mainLayer;
    LayerColor *dimLayer;

    cocos2d::Sprite *background;
    
    cocos2d::Sprite *selectedFrame;
    cocos2d::Sprite *frameSelectedBorder;

    Scale9Sprite *unlockTalkBox;

    cocos2d::Node *progressBarBg;
    cocos2d::Node *progressBar;
    cocos2d::Node *playProgressBarBg;
    cocos2d::Node *playProgressBar;
    cocos2d::Node *progressMarkHolder;

    cocos2d::MenuItemImage* playMenuItem;
    cocos2d::MenuItemImage* pauseMenuItem;

    cocos2d::Node* recordingIcon;
    
    cocos2d::Node* trashItem;
    cocos2d::Node* removeArea;

    cocos2d::MenuItemSprite* doneMenuItem;
    cocos2d::MenuItemSprite* disabledDoneMenuItem;

    cocos2d::Menu* backMenu;
    cocos2d::Menu* recordingMenu;
    cocos2d::Menu* bottomMenu;
    cocos2d::Menu* doneMenu;
    cocos2d::Menu* completeMenu;

    cocos2d::Sprite *currentProgressCharacter;
    Character *prevCharacter;
    Character *currentSelectedCharacter;
    
    Character *selectedLockedCharacter;
    
    __Array* characters; // screen character data
    __Dictionary* characterProperties;
    
    CharacterScrollView *characterScrollView;
    BackgroundScrollView *backgroundScrollView;
    SoundScrollView *soundScrollView;

    cocos2d::Point backgroundViewScrollOffset;

    cocos2d::__Dictionary *touchTargetMapping;
    cocos2d::__Dictionary *playingTouches;

    int playTouchTimeStampIndex;
    double playTouchElapsedTime;
    double lastTouchTimeStamp;
    double accumulatedTouchTimeStamp;
    float recordingTouchNum;
    std::chrono::milliseconds lastTouchTime;
    cocos2d::__Array *touchData;
    
    float playedTime;
    float recordedTime;

    bool openBgItem;
    bool openCharacterItem;
    bool openSoundItem;
    
    bool paused;
    bool playing;
    bool recording;

    Label *createButtonLabel;

    __String* voiceFileName;
    __String* bgmFileName;

    __String* lastUnlockBackgroundKey;
    __String* selectedBackgroundKey;
    __String* selectedSoundKey;
    __String* selectedLockedBackgroundKey;

    bool isPlatformShown;
    
    ui::Button *createBgButton;
    void setCreateButtons(bool visible);
    Scene *cameraScene;
    
    int localCharacterId;
    
    void showRecordingIcon(bool show);

    Sprite* getCcbiSprite(const char* ccbiName, cocos2d::Point position);
    void scrollViewButtonEnable(bool backgroundButtonEnable, bool isMusicScroll);

    Home* _homeLayer;
    TSDoleExecutePurchase *_protocolExecutePurchase;
    TSDoleGetInventoryList *_protocolInventoryList;
    
protected:
    void doExecutePurchase();
    void doleCheckInventoryListUpdate();
    void onRequestStarted(TSDoleProtocol *protocol, TSNetResult result);
    void onResponseEnded(TSDoleProtocol *protocol, TSNetResult result);
    void onProtocolFailed(TSDoleProtocol *protocol, TSNetResult result);
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    void onRequestCanceled(TSDoleProtocol *protocol);
//#endif

public:
    void checkRecall(int type);
    void checkExecutePurchase();
    void cancelExecutePurchase(bool netfail);
};

#endif /* defined(__bobby__CharacterRecordingScene__) */
