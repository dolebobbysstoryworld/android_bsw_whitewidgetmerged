//
//  CharacterSettingScene.h
//  bobby
//
//  Created by oasis on 2014. 4. 17..
//
//

#ifndef __bobby__CharacterSettingScene__
#define __bobby__CharacterSettingScene__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "Character.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CharacterSetting : public cocos2d::LayerColor
{
    
public:
//    static cocos2d::Scene* createScene();
    static cocos2d::Scene* createScene(__String* bookKey, int chapterIndex);

    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    Character *lion;
    Character *lion2;
    
    virtual bool init();
    
    void btnBackCallback(Ref* pSender);
    
    virtual void update(float delta);
    virtual void onEnter();
    virtual void onExit();
    void reZorder(cocos2d::Sprite* pSender);
    void menuCallback(Ref* sender);

    CREATE_FUNC(CharacterSetting);

    int chapterIndex;
    __String* bookKey;
    
private:
    int touchNumber;
    Vector<Touch*> touches;
    cocos2d::Sprite *selectedSprite;

    __Array* timelines;
    int timeStamp;
    bool pressed;
};


#endif /* defined(__bobby__CharacterSettingScene__) */
