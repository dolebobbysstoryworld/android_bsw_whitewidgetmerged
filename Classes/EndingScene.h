//
//  EndingScene.h
//  bobby
//
//  Created by Dongwook, Kim on 14. 7. 15..
//
//

#ifndef __bobby__EndingScene__
#define __bobby__EndingScene__

#include <iostream>
#include "cocos2d.h"
#include <cocosbuilder/CocosBuilder.h>

USING_NS_CC;

class Ending;

class EndingDelegate
{
public:
    virtual ~EndingDelegate() {}
    virtual void onEndingFinished(Ending* ending) = 0;
};

class Ending : public LayerColor, cocosbuilder::CCBAnimationManagerDelegate
{
public:
    virtual bool init();
    EndingDelegate* getDelegate() { return _delegate; }
    void setDelegate(EndingDelegate* pDelegate) { _delegate = pDelegate; }
    void completedAnimationSequenceNamed(const char *name);

    void pauseAllChildren(Node *node);
    void resumeAllChildren(Node *node);
    
    void startAnimation();
    void pauseAnimation();
    void resumeAnimation();
    
    bool isAnimationStarted() { return animationStarted; };
    bool isAnimationPaused() { return animationPaused; };
    
    CREATE_FUNC(Ending);
private:
    EndingDelegate *_delegate;
    
    void finishedEnding();
    
    bool animationStarted;
    bool animationPaused;
    
    std::chrono::milliseconds animationStartTime;
    double elapsedAnimationTime;
    double accElapsedAnimationTime;
    
    float endingAnimationDuration;
    void stopAction();
    
    void triggerFinish(float durationOffset);

    Node *endingCcbi;
};
#endif /* defined(__bobby__EndingScene__) */
