//
//  ResourceManager.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 4. 15..
//
//

#ifndef __bobby__ResourceManager__
#define __bobby__ResourceManager__

#include "cocos2d.h"

enum class ResolutionType {
    iphone35, iphone4, ipad, ipadhd, androidfullhd, androidhd // iphone35:3:2 iphone4:16:9 ipad:4:3, android:16:9(FullHD), android:16:9(HD)
};

enum class ResolutionMode {
    fixed, dynamic,
};

typedef struct tagResource
{
    cocos2d::Size size;
    char directory[30];
    ResolutionType type;
    cocos2d::Size designResolution;
} Resource;

void changePositionAtAnchor(cocos2d::Node *node, cocos2d::Point anchor);

class ResourceManager {
    
public:
    static ResourceManager* getInstance();
    ResolutionType getCurrentType() { return currentType; }
    
protected:
    ResolutionMode resolutionMode;
    cocos2d::Size fixedDesignResolution;
    Resource *currentResource;
    ResolutionType currentType;
    cocos2d::LanguageType currentLanguageType;
    
    ResourceManager();
    // ResourceManager(ResolutionMode mode, cocos2d::Size fixedDesignResolution = cocos2d::Size::ZERO);
    virtual ~ResourceManager();
    
public:
    // POS_H_CLASS_START **do not delete this line**
    cocos2d::Point home_test_menu_photo();
    cocos2d::Point home_test_menu_facebook();
    cocos2d::Point home_test_menu_popup();
    cocos2d::Point home_test_menu_qrcode();
    cocos2d::Point home_test_menu_toast();
    cocos2d::Point home_logo();
    cocos2d::Point home_logo_dole();
    cocos2d::Point home_logo_divider();
    cocos2d::Point home_menu_btn();
    float home_main_layer_hide_height();
    cocos2d::Point home_bg();
    cocos2d::Point home_bg_home_cloud_01();
    cocos2d::Point home_bg_home_cloud_02();
    cocos2d::Point home_coin();
    cocos2d::Point home_x();
    cocos2d::Point home_coin_num();
    cocos2d::Size home_coin_before_login_size();
    cocos2d::Size home_coin_num_size();
    float home_coin_num_fontsize();
    cocos2d::Point home_item_menu();
    cocos2d::Point home_item_btn();
    float home_item_scroll_height();
    cocos2d::Point home_new_book();
    cocos2d::Point home_new_book_layer();
    cocos2d::Size home_book_layer_size();
    cocos2d::Point home_book_bg();
    cocos2d::Point home_book_character_01();
    cocos2d::Point home_book_character_02();
    cocos2d::Point home_book_delete_button();
    cocos2d::Point home_book_preload_author();
    cocos2d::Point home_book_author();
    float home_book_author_fontsize();
    cocos2d::Size home_book_author_size();
    float home_book_start_y();
    cocos2d::Point home_book_list_view();
    cocos2d::Size home_book_size();
    float home_book_padding();
    float home_book_scroll_hieght();
    cocos2d::Point home_new_book_text();
    cocos2d::Point home_new_book_jp_text_first();
    cocos2d::Point home_new_book_jp_text_second();
    float home_new_book_title_fontsize();
    float home_new_book_jp_title_fontsize();
    cocos2d::Size home_new_book_text_size();
    cocos2d::Size home_new_book_jp_text_size();
    cocos2d::Size home_new_book_layer_size();
    cocos2d::Point home_book_title();
    cocos2d::Point home_book_title_a_line();
    cocos2d::Point home_pre_book_title();
    cocos2d::Size home_book_title_size();
    cocos2d::Size home_pre_book_title_size();
    float home_book_title_fontsize();
    float home_book_title_pre_fontsize();
    cocos2d::Point home_book_title_sub();
    cocos2d::Point home_background_scroll_view();
    float home_background_scroll_view_height();
    cocos2d::Point home_background_frame_type_01();
    cocos2d::Point home_background_frame_type_02();
    float home_background_scroll_view_startpadding();
    float home_background_scroll_coin_fontsize_01();
    float home_background_scroll_coin_fontsize_02();
    float home_background_scroll_coin_fontsize_03();
    cocos2d::Size home_background_scroll_coin_size();
    cocos2d::Point home_background_scroll_coin();
    float home_background_scroll_end_padding();
    cocos2d::Point home_background_line_start();
    cocos2d::Point home_background_check();
    cocos2d::Point home_background_coin_text();
    float home_bg_select_coin_fontsize_01();
    float home_bg_select_coin_fontsize_02();
    float home_bg_select_coin_fontsize_03();
    cocos2d::Point home_bg_select_coin_margin();
    cocos2d::Point home_background_unlock_talk_box();
    cocos2d::Size home_item_talk_box_size();
    cocos2d::Point home_item_talkbox_inset_right_tail();
    cocos2d::Size home_item_talkbox_inset_right_tail_size();
    cocos2d::Point home_item_talkbox_inset_left_tail();
    cocos2d::Size home_item_talkbox_inset_left_tail_size();
    cocos2d::Point home_item_select_coin_margin();
    float home_item_select_text_area_width();
    float home_item_select_text_bg_mid_height();
    cocos2d::Point home_item_talkbox();
    cocos2d::Size home_bg_select_coin_text_size();
    cocos2d::Size home_bottom_scroll_view_frame_size();
    float home_bottom_scroll_view_frame_start_padding();
    float home_bottom_scroll_view_frame_bottom_padding();
    float home_bottom_scroll_view_character_item_start_padding();
    cocos2d::Size home_bottom_scroll_view_character_item_holder_size();
    float home_bottom_scroll_view_character_item_bottom_padding1();
    float home_bottom_scroll_view_character_item_bottom_padding2();
    float home_bottom_scroll_view_character_item_bottom_padding3();
    float home_bottom_scroll_view_music_item_bottom_padding1();
    float home_bottom_scroll_view_music_item_bottom_padding2();
    float home_bottom_scroll_view_music_item_bottom_padding3();
    cocos2d::Size home_bottom_scroll_view_music_item_holder_size();
    float home_bottom_scroll_view_music_pin_top_padding();
    cocos2d::Point home_bottom_scroll_view_icon_in_character();
    float home_bottom_scroll_view_character_pin_top_padding();
    float home_bottom_scroll_view_center_icon_bottom_padding();
    cocos2d::Point home_bottom_scroll_view_coin();
    float home_bottom_scroll_view_coin_visible_height();
    float home_bottom_scroll_view_coin_font_size_digit1();
    float home_bottom_scroll_view_coin_font_size_digit2();
    float home_bottom_scroll_view_coin_font_size_digit3();
    float home_bottom_scroll_view_pin_top_padding();
    cocos2d::Point home_bottom_line_start();
    cocos2d::Size home_bottom_line_left();
    cocos2d::Size home_bottom_line_mid();
    cocos2d::Size home_bottom_line_right();
    cocos2d::Point home_talk_box();
    float home_talk_box_purchase_bottom_padding();
    float home_talk_box_fontsize();
    cocos2d::Size home_talk_box_size();
    cocos2d::Point home_talk_box_label_01();
    cocos2d::Point home_talk_box_label_02();
    float home_talk_box_max_width_short();
    float home_talk_box_max_width_long();
    float home_talk_box_bottom_margin();
    float home_talk_box_side_margin();
    float home_talk_box_top_margin();
    cocos2d::Size home_frame_sprite_size();
    cocos2d::Point home_delete_btn_type01();
    cocos2d::Point home_framebg();
    cocos2d::Point home_create_btn();
    cocos2d::Size home_create_btn_visible_rect();
    cocos2d::Point home_create_bg_btn();
    float home_create_fontsize();
    cocos2d::Size home_create_textsize();
    cocos2d::Point home_create_text();
    float home_delete_btn_padding();
    cocos2d::Size home_delete_btn_glow_size();
    float home_background_layer_shadow();
    cocos2d::Point home_background_layer_coin();
    float home_background_layer_coin_text_fontsize();
    float home_custom_bg_bottom_padding();
    cocos2d::Size home_custom_bg_size();
    cocos2d::Point home_background_item_bg_ani01();
    cocos2d::Point home_background_item_bg_ani02();
    cocos2d::Point home_background_item_bg_ani04();
    cocos2d::Point home_background_item_bg_ani06();
    cocos2d::Point home_background_item_bg_ani07();
    cocos2d::Point home_background_item_bg_ani08();
    cocos2d::Point home_background_item_bg_ani09();
    cocos2d::Point home_background_item_bg_ani10();
    cocos2d::Point home_background_item_bg_ani11();
    cocos2d::Point home_background_item_bg_ani12();
    cocos2d::Point home_custom_bg_big();
    cocos2d::Point home_custom_bg_book_thumb();
    float home_custom_bg_width_inbook();
    cocos2d::Size home_setting_bg_image_size();
    cocos2d::Point home_setting_bg_capinsets();
    cocos2d::Size home_setting_bg_capinsets_size();
    cocos2d::Size home_setting_bg_size();
    cocos2d::Point home_setting_bg();
    cocos2d::Size home_setting_list_press_image_size();
    cocos2d::Point home_setting_list_press_capInsets();
    cocos2d::Size home_setting_list_press_capInsets_size();
    cocos2d::Point home_setting_list_press();
    cocos2d::Size home_setting_list_press_size();
    cocos2d::Point home_setting_icon();
    float home_setting_label_fontsize();
    cocos2d::Point home_setting_label();
    float home_dim_limit_time_fontsize_01();
    float home_dim_limit_time_fontsize_02();
    cocos2d::Size home_dim_limit_time_lable_size();
    float home_dim_limit_time_y();
    float home_dim_limit_name_label_fontsize();
    cocos2d::Size home_dim_limit_name_label_size();
    float home_dim_limit_name_label_y();
    float home_dim_limit_char_talkbox_y();
    float home_frame_limit_fontsize_01();
    float home_frame_limit_fontsize_02();
    cocos2d::Size home_frame_limit_label_size();
    cocos2d::Point home_frame_limit_label();
    cocos2d::Point bookmap_bg_map();
    cocos2d::Point bookmap_btn_back();
    cocos2d::Point bookmap_title_label();
    float bookmap_title_label_fontsize();
    cocos2d::Size bookmap_single_title_label_size();
    cocos2d::Size bookmap_double_title_label_size();
    cocos2d::Point bookmap_single_title_label();
    cocos2d::Point bookmap_double_title_label_01();
    cocos2d::Point bookmap_double_title_label_02();
    float bookmap_name_label_fontsize();
    cocos2d::Size bookmap_single_name_label_size();
    cocos2d::Size bookmap_double_name_label_size();
    cocos2d::Point bookmap_single_name_label();
    cocos2d::Point bookmap_double_name_label_01();
    cocos2d::Point bookmap_double_name_label_02();
    cocos2d::Point bookmap_subtitle_label();
    float bookmap_subtitle_label_fontsize();
    cocos2d::Size bookmap_subtitle_label_size();
    cocos2d::Point bookmap_feather01();
    cocos2d::Point bookmap_feather02();
    cocos2d::Point bookmap_feather03();
    cocos2d::Point bookmap_feather04();
    cocos2d::Point bookmap_feather05();
    cocos2d::Point bookmap_menu_btn();
    cocos2d::Point bookmap_play_btn();
    cocos2d::Point bookmap_title_bg();
    cocos2d::Point bookmap_intro();
    cocos2d::Point bookmap_final();
    cocos2d::Point bookmap_scene_2();
    cocos2d::Point bookmap_scene_3();
    cocos2d::Point bookmap_scene_4();
    float bookmap_flag_fontsize_1();
    float bookmap_flag_fontsize_n();
    float bookmap_flag_fontsize_5();
    cocos2d::Point bookmap_flag_pos_type01();
    cocos2d::Point bookmap_flag_pos_type02();
    cocos2d::Size bookmap_flag_size_type01();
    cocos2d::Size bookmap_flag_size_type02();
    cocos2d::Point bookmap_character01();
    cocos2d::Point bookmap_character02();
    cocos2d::Point bookmap_character03();
    cocos2d::Point bookmap_character04();
    cocos2d::Point bookmap_character05();
    cocos2d::Point bookmap_delete_btn01();
    cocos2d::Point bookmap_delete_btn02();
    cocos2d::Point bookmap_delete_btn03();
    cocos2d::Point bookmap_delete_btn04();
    cocos2d::Point bookmap_delete_btn05();
    cocos2d::Point bookmap_icon_bg01();
    cocos2d::Point bookmap_icon_bg02();
    cocos2d::Point bookmap_icon_bg03();
    cocos2d::Point bookmap_icon_bg04();
    cocos2d::Point bookmap_icon_bg05();
    float bookmap_author_touch_width();
    float bookmap_edit_box_Y();
    float home_new_book_start_padding();
    float bookmap_title_y();
    cocos2d::Size bookmap_menulist_bg_size();
    cocos2d::Size bookmap_menulist_press_size();
    cocos2d::Size bookmap_menulist_label_size();
    float bookmap_menulist_button_height();
    cocos2d::Point bookmap_menulist_mid_label();
    cocos2d::Point bookmap_menulist_mid_icon();
    cocos2d::Point camera_btn_back();
    cocos2d::Size LoginLayer_background_Size();
    cocos2d::Point AccountLayer_Position_full_popup_character_01();
    cocos2d::Point AccountLayer_Position_full_popup_character_02();
    cocos2d::Point AccountLayer_Position_login_logo();
    cocos2d::Size AccountLayer_Size_btn_cancel_normal();
    cocos2d::Point AccountLayer_Position_btn_cancel_normal();
    cocos2d::Point LoginLayer_Position_ControlButton_ID();
    cocos2d::Point LoginLayer_Position_ControlButton_PW();
    cocos2d::Size LoginLayer_Size_Button_AccountControl();
    float LoginLayer_FontSize_AccountControl();
    cocos2d::Point LoginLayer_Position_Button_ForgotPassword();
    cocos2d::Point LoginLayer_Position_Button_Signup();
    cocos2d::Point LoginLayer_Position_Button_FacebookLogin();
    float AccountLayer_button_normal_01_Width();
    float AccountLayer_button_normal_01_Height();
    float AccountLayer_button_normal_01_InLeft();
    float AccountLayer_button_normal_01_InTop();
    float AccountLayer_button_normal_01_inWidth();
    float AccountLayer_button_normal_01_inHeight();
    float LoginLayer_FontSize_Button_Login();
    cocos2d::Size LoginLayer_Size_Button_Login();
    cocos2d::Point LoginLayer_Position_Button_Login();
    cocos2d::Size LoginLayer_Size_SkipButton();
    cocos2d::Point LoginLayer_Position_SkipButton();
    cocos2d::Size LoginLayer_Size_SkipLabel();
    float LoginLayer_Position_SkipLabel_x();
    float LoginLayer_Position_SkipLabel_y_Margin();
    float LoginLayer_FontSize_SkipLabel();
    float LoginLayer_FontSize_ForgotNoticeLayer();
    cocos2d::Size LoginLayer_Size_ForgotNoticeLabel();
    cocos2d::Point LoginLayer_Position_ForgotNoticeLabel();
    float AccountLayer_FontSize_RadioLabel();
    float TSIconEditBox_Size_Width();
    float TSIconEditBox_Size_Height();
    cocos2d::Point TSIconEditBox_Position_Icon();
    cocos2d::Size TSIconEditBox_Size_textFiled();
    cocos2d::Point TSIconEditBox_Position_textField();
    float TSIconEditBox_FontSize_textField();
    float TSIconEditBox_FontSize_ErrorComment();
    cocos2d::Size TSIconEditBox_Size_ErrorComment();
    float TSIconEditBox_Position_InputComment1_x();
    float TSIconEditBox_Position_InputComment2_x();
    float TSIconEditBox_Position_InputComment_y();
    float TSIconEditBox_Size_InputComment_Label_WidthDelta();
    float TSIconEditBox_Size_InputComment_Label_Height();
    float TSIconEditBox_FontSize_InputComment();
    cocos2d::Point SignupLayer_Position_IdEditBox();
    cocos2d::Point SignupLayer_Position_PWEditBox();
    cocos2d::Point SignupLayer_Position_CPWEditBox();
    cocos2d::Point SignupLayer_Position_BirthEditBox();
    cocos2d::Point SignupLayer_Position_CityEidtBox();
    cocos2d::Point SignupLayer_Position_MailRadio();
    cocos2d::Point SignupLayer_Position_FemailRadio();
    cocos2d::Point SignupLayer_Position_TermsCheckBox();
    cocos2d::Point SignupLayer_Position_AgreementTextLabel();
    float SignupLayer_FontSize_AgreementTextLabel();
    float SignupLayer_FontSize_Button_Login();
    cocos2d::Size SignupLayer_Size_SignupButton();
    cocos2d::Point SignupLayer_Position_SignupButton();
    cocos2d::Point SignTermsLayer_Position_TermTextTop();
    cocos2d::Point SignTermsLayer_Position_TermTextBottom();
    float SignTermsLayer_Delta_TermTextMid_Width();
    float SignTermsLayer_Delta_TermTextMid_Height();
    cocos2d::Point SignTermsLayer_Position_TermTextMid();
    cocos2d::Point SignTermsLayer_Position_TermText_Title();
    float SignTermsLayer_FontSize_TermText_Title();
    float SignTermsLayer_FontSize_TermText_Text_Label();
    float SignTermsLayer_Size_TermText_Text_Label_Width();
    cocos2d::Size SignTermsLayer_Size_TermText_ScrollView();
    cocos2d::Point SignTermsLayer_Position_TermText_ScrollView();
    cocos2d::Point SignTermsLayer_Position_PolicyTextTop();
    cocos2d::Point SignTermsLayer_Position_PolicyTextBottom();
    float SignTermsLayer_Delta_PolicyTextMid_Width();
    float SignTermsLayer_Delta_PolicyTextMid_Height();
    cocos2d::Point SignTermsLayer_Position_PolicyTextMid();
    cocos2d::Point SignTermsLayer_Position_PolicyText_Title();
    float SignTermsLayer_FontSize_PolicyText_Title();
    float SignTermsLayer_FontSize_PolicyText_Text_Label();
    float SignTermsLayer_Size_PolicyText_Text_Label_Width();
    cocos2d::Size SignTermsLayer_Size_PolicyText_ScrollView();
    cocos2d::Point SignTermsLayer_Position_PolicyText_ScrollView();
    cocos2d::Point SignTermsLayer_Position_PCheckBox();
    float SignTermsLayer_Position_TermsCheckBox_Label_x();
    float SignTermsLayer_Position_TermsCheckBox_Label_y();
    cocos2d::Point SignTermsLayer_Position_PrivacyCheckBox();
    float SignTermsLayer_Position_PrivacyCheckBox_Label_x();
    float SignTermsLayer_Position_PrivacyCheckBox_Label_y();
    float SignTermsLayer_FontSize_Cancel_Button();
    cocos2d::Size SignTermsLayer_Size_Cancel_Button();
    cocos2d::Point SignTermsLayer_Position_Cancel_Button();
    float SignTermsLayer_FontSize_Next_Button();
    cocos2d::Size SignTermsLayer_Size_Next_Button();
    cocos2d::Point SignTermsLayer_Position_Next_Button();
    cocos2d::Size popup_bottombg_size();
    cocos2d::Size popup_bottombg_corner_size();
    float popup_top_padding();
    float popup_middle_padding();
    float popup_bottom_padding();
    float popup_one_button_size();
    float popup_two_button_size();
    float popup_button_padding();
    cocos2d::Size popup_button_image_size();
    cocos2d::Size popup_button_corner_size();
    float popup_messege_fontsize();
    float popup_button_fontsize();
    float popup_button_twoline_fontsize();
    float popup_button_twoline_label_width();
    float popup_button_2line_fontsize();
    cocos2d::Size popup_list_topbg_size();
    cocos2d::Size popup_list_topbg_corner_size();
    float popup_width();
    float popup_title_fontsize();
    cocos2d::Size popup_title_label_size();
    cocos2d::Size popup_shared_cell_size();
    cocos2d::Point popup_title();
    cocos2d::Size popup_shared_tableview_size();
    float popup_shared_middle_height();
    cocos2d::Point popup_shared_icon();
    cocos2d::Size popup_shared_icon_size();
    float popup_shared_cell_fontsize();
    cocos2d::Size popup_shared_cell_label_size();
    cocos2d::Point popup_shared_cell_label();
    float popup_year_background_height();
    cocos2d::Size popup_year_cell_size();
    cocos2d::Size popup_year_cell_label_size();
    cocos2d::Point popup_year_cell_label();
    cocos2d::Point popup_year_picker_line_01();
    cocos2d::Point popup_year_picker_line_02();
    cocos2d::Size popup_year_picker_line_size();
    cocos2d::Point recording_bg_frame();
    float recording_item_right_padding();
    float recording_item_bottom_padding();
    float recording_item_padding();
    float recording_bottom_scroll_view_frame_start_padding();
    float recording_bottom_scroll_view_frame_bottom_padding();
    float recording_bottom_scroll_view_character_item_start_padding();
    cocos2d::Size recording_bottom_scroll_view_character_item_holder_size();
    float recording_bottom_scroll_view_character_item_bottom_padding1();
    float recording_bottom_scroll_view_character_item_bottom_padding2();
    float recording_bottom_scroll_view_character_item_bottom_padding3();
    cocos2d::Point recording_bottom_scroll_view_icon_in_character();
    float recording_bottom_scroll_view_character_pin_top_padding();
    float recording_bottom_scroll_view_center_icon_bottom_padding();
    cocos2d::Point recording_bottom_scroll_view_coin();
    float recording_bottom_scroll_view_coin_visible_height();
    float recording_bottom_scroll_view_coin_font_size_digit1();
    float recording_bottom_scroll_view_coin_font_size_digit2();
    float recording_bottom_scroll_view_coin_font_size_digit3();
    float recording_bottom_scroll_area_height();
    float recording_bottom_scroll_view_height();
    float recording_bottom_scroll_view_pin_bottom_padding();
    cocos2d::Point recording_bottom_line_start();
    cocos2d::Size recording_bottom_line_left();
    cocos2d::Size recording_bottom_line_mid();
    cocos2d::Size recording_bottom_line_right();
    cocos2d::Size recording_bg_create_btn_visible_rect();
    cocos2d::Point recording_record_icon();
    float recording_record_text_font_size();
    float recording_record_text_left_padding();
    float recording_record_text_right_padding();
    cocos2d::Point recording_record_button();
    cocos2d::Point recording_done_icon();
    float recording_done_text_font_size();
    float recording_done_text_left_padding();
    float recording_done_text_right_padding();
    cocos2d::Point recording_done_button();
    float recording_rerecord_text_font_size();
    float recording_rerecord_text_left_padding();
    float recording_rerecord_text_right_padding();
    cocos2d::Point recording_rerecord_icon();
    float recording_rerecord_button_padding();
    float recording_save_text_font_size();
    float recording_save_text_left_padding();
    float recording_save_text_right_padding();
    cocos2d::Point recording_save_icon();
    cocos2d::Point recording_save_button();
    cocos2d::Point recording_progress_bar();
    float recording_recording_text_font_size();
    float recording_recording_text_side_padding();
    cocos2d::Point recording_recording_icon();
    cocos2d::Size recording_remove_area();
    cocos2d::Size recording_progress_bar_size();
    float recording_progress_mark_bottom_margin();
    cocos2d::Size recording_clear_talk_box_size();
    cocos2d::Point recording_clear_talk_box();
    cocos2d::Point recording_clear_talk_box_inset();
    cocos2d::Size recording_clear_talk_box_ok_button_size();
    cocos2d::Point recording_clear_talk_box_ok_button_inset();
    cocos2d::Size recording_unlock_talk_box_size();
    cocos2d::Point recording_unlock_talk_box_inset();
    cocos2d::Size recording_unlock_talk_box_inset_size();
    cocos2d::Size recording_unlock_talk_box_unlock_button_size();
    cocos2d::Point recording_talk_box_unlock_button();
    float recording_talk_box_text_font_size();
    float recording_talk_box_background_max_width_long();
    float recording_talk_box_background_max_width_short();
    float recording_talk_box_character_max_width_long();
    float recording_talk_box_character_max_width_short();
    float recording_talk_box_text_left_margin();
    float recording_talk_box_text_bottom_margin();
    float recording_talk_box_text_top_margin();
    float recording_talk_box_unlock_button_text_max_width();
    float recording_talk_box_unlock_button_text_side_margin();
    float recording_talk_box_unlock_button_right_margin();
    float recording_talk_box_unlock_button_left_margin();
    float recording_talk_box_unlock_button_bottom_margin();
    float recording_talk_box_unlock_button_top_margin();
    cocos2d::Size recording_talk_box_text_label_size();
    cocos2d::Point recording_clear_talk_box_top_label();
    cocos2d::Point recording_clear_talk_box_second_label();
    cocos2d::Point recording_unlock_talk_box_top_label();
    cocos2d::Point recording_unlock_talk_box_second_label();
    cocos2d::Point recording_talk_box_ok_button();
    float recording_talk_box_unlock_button_font_size();
    float recording_sound_scroll_label_font_size();
    cocos2d::Size recording_sound_scroll_label_size();
    cocos2d::Size recording_sound_scroll_label_shodow();
    cocos2d::Point recording_sound_scroll_label();
    float share_cancel_text_font_size();
    cocos2d::Point share_cancel_icon();
    float share_cancel_text_left_padding();
    float share_cancel_text_right_padding();
    cocos2d::Point share_cancel_button();
    cocos2d::Size map_play_title_size();
    float map_play_title_bottom_margin();
    float map_play_title_top_padding();
    float map_play_title_text_size();
    float map_play_title_text_storke_width();
    cocos2d::Size toast_bg_size();
    cocos2d::Size toast_bg_corner_size();
    cocos2d::Size toast_bg_corner_inset();
    cocos2d::Size toast_content_size_type_01();
    cocos2d::Size toast_content_size_type_02();
    float toast_type_02_extra_width_ja();
    float toast_message_fontsize();
    float toast_message_ja_fontsize();
    cocos2d::Size toast_label_size_type_01();
    cocos2d::Size toast_label_size_type_02();
    float toast_bg_padding_top_bottom();
    float toast_bg_padding_left();
    float toast_bg_y();
    cocos2d::Size label_shadow_default_size();
    cocos2d::Size label_shadow_author_size();
    cocos2d::Size label_shadow_play_size();
    cocos2d::Size label_shadow_camera_size();
    cocos2d::Point custom_character_c001_photo();
    cocos2d::Point custom_character_c002_photo();
    cocos2d::Point custom_character_c003_photo();
    cocos2d::Point custom_character_c004_photo();
    cocos2d::Point custom_character_c005_photo();
    cocos2d::Point custom_character_c006_photo();
    cocos2d::Point custom_character_c007_photo();
    cocos2d::Point custom_character_c008_photo();
    cocos2d::Point custom_character_c009_photo();
    cocos2d::Point custom_character_c010_photo();
    cocos2d::Point custom_character_c011_photo();
    cocos2d::Point custom_character_c012_photo();
    cocos2d::Point custom_character_c013_photo();
    cocos2d::Point custom_character_c014_photo();
    cocos2d::Point custom_character_c001_side_photo();
    cocos2d::Point custom_character_c002_side_photo();
    cocos2d::Point custom_character_c003_side_photo();
    cocos2d::Point custom_character_c004_side_photo();
    cocos2d::Point custom_character_c005_side_photo();
    cocos2d::Point custom_character_c006_side_photo();
    cocos2d::Point custom_character_c007_side_photo();
    cocos2d::Point custom_character_c008_side_photo();
    cocos2d::Point custom_character_c009_side_photo();
    cocos2d::Point custom_character_c010_side_photo();
    cocos2d::Point custom_character_c011_side_photo();
    cocos2d::Point custom_character_c012_side_photo();
    cocos2d::Point custom_character_c013_side_photo();
    cocos2d::Point custom_character_c014_side_photo();
    cocos2d::Size custom_character_c001_side_photo_size();
    cocos2d::Size custom_character_c002_side_photo_size();
    cocos2d::Size custom_character_c003_side_photo_size();
    cocos2d::Size custom_character_c004_side_photo_size();
    cocos2d::Size custom_character_c005_side_photo_size();
    cocos2d::Size custom_character_c006_side_photo_size();
    cocos2d::Size custom_character_c007_side_photo_size();
    cocos2d::Size custom_character_c008_side_photo_size();
    cocos2d::Size custom_character_c009_side_photo_size();
    cocos2d::Size custom_character_c010_side_photo_size();
    cocos2d::Size custom_character_c011_side_photo_size();
    cocos2d::Size custom_character_c012_side_photo_size();
    cocos2d::Size custom_character_c013_side_photo_size();
    cocos2d::Size custom_character_c014_side_photo_size();
    float opening_single_title_label_font_size();
    float opening_double_title_label_font_size();
    float opening_author_label_font_size();
    cocos2d::Size opening_single_title_label_size();
    cocos2d::Size opening_double_title_label_size();
    cocos2d::Point opening_single_title_label();
    cocos2d::Point opening_double_title_label_01();
    cocos2d::Point opening_double_title_label_02();
    float opening_title_label_shadow_distance();
    float opening_author_label_side_margin();
    float opening_author_label_shadow_distance();
    cocos2d::Size opening_author_label_size();
    float opening_author_label_top_padding();
    float opening_author_label_star_bottom_padding();
    float opening_author_label_star_side_padding();
    float opening_title_text_max_width();
    float opening_author_text_max_width();
    float opening_title_center_min_width();
    float opening_title_center_max_width();
    float opening_title_node_top_padding();
    float guide_label_nanum_fontSize();
    float guide_label_normal_fontSize();
    float guide_label_big_fontsize();
    float guide_label_jp_big_fontsize();
    float guide_label_small_fontsize();
    float guide_label_jp_small_fontsize();
    float progress_label_fontsize();
    cocos2d::Size progress_label_size();
    cocos2d::Point progress_label_position();
    // POS_H_CLASS_END   **do not delete this line**
};

// POS_H_DEFINE_START    **do not delete this line**
#define HOME_TEST_MENU_PHOTO		ResourceManager::getInstance()->home_test_menu_photo()
#define HOME_TEST_MENU_FACEBOOK		ResourceManager::getInstance()->home_test_menu_facebook()
#define HOME_TEST_MENU_POPUP		ResourceManager::getInstance()->home_test_menu_popup()
#define HOME_TEST_MENU_QRCODE		ResourceManager::getInstance()->home_test_menu_qrcode()
#define HOME_TEST_MENU_TOAST		ResourceManager::getInstance()->home_test_menu_toast()
#define HOME_LOGO		ResourceManager::getInstance()->home_logo()
#define HOME_LOGO_DOLE		ResourceManager::getInstance()->home_logo_dole()
#define HOME_LOGO_DIVIDER		ResourceManager::getInstance()->home_logo_divider()
#define HOME_MENU_BTN		ResourceManager::getInstance()->home_menu_btn()
#define HOME_MAIN_LAYER_HIDE_HEIGHT		ResourceManager::getInstance()->home_main_layer_hide_height()
#define HOME_BG		ResourceManager::getInstance()->home_bg()
#define HOME_BG_HOME_CLOUD_01		ResourceManager::getInstance()->home_bg_home_cloud_01()
#define HOME_BG_HOME_CLOUD_02		ResourceManager::getInstance()->home_bg_home_cloud_02()
#define HOME_COIN		ResourceManager::getInstance()->home_coin()
#define HOME_X		ResourceManager::getInstance()->home_x()
#define HOME_COIN_NUM		ResourceManager::getInstance()->home_coin_num()
#define HOME_COIN_BEFORE_LOGIN_SIZE		ResourceManager::getInstance()->home_coin_before_login_size()
#define HOME_COIN_NUM_SIZE		ResourceManager::getInstance()->home_coin_num_size()
#define HOME_COIN_NUM_FONTSIZE		ResourceManager::getInstance()->home_coin_num_fontsize()
#define HOME_ITEM_MENU		ResourceManager::getInstance()->home_item_menu()
#define HOME_ITEM_BTN		ResourceManager::getInstance()->home_item_btn()
#define HOME_ITEM_SCROLL_HEIGHT		ResourceManager::getInstance()->home_item_scroll_height()
#define HOME_NEW_BOOK		ResourceManager::getInstance()->home_new_book()
#define HOME_NEW_BOOK_LAYER		ResourceManager::getInstance()->home_new_book_layer()
#define HOME_BOOK_LAYER_SIZE		ResourceManager::getInstance()->home_book_layer_size()
#define HOME_BOOK_BG		ResourceManager::getInstance()->home_book_bg()
#define HOME_BOOK_CHARACTER_01		ResourceManager::getInstance()->home_book_character_01()
#define HOME_BOOK_CHARACTER_02		ResourceManager::getInstance()->home_book_character_02()
#define HOME_BOOK_DELETE_BUTTON		ResourceManager::getInstance()->home_book_delete_button()
#define HOME_BOOK_PRELOAD_AUTHOR		ResourceManager::getInstance()->home_book_preload_author()
#define HOME_BOOK_AUTHOR		ResourceManager::getInstance()->home_book_author()
#define HOME_BOOK_AUTHOR_FONTSIZE		ResourceManager::getInstance()->home_book_author_fontsize()
#define HOME_BOOK_AUTHOR_SIZE		ResourceManager::getInstance()->home_book_author_size()
#define HOME_BOOK_START_Y		ResourceManager::getInstance()->home_book_start_y()
#define HOME_BOOK_LIST_VIEW		ResourceManager::getInstance()->home_book_list_view()
#define HOME_BOOK_SIZE		ResourceManager::getInstance()->home_book_size()
#define HOME_BOOK_PADDING		ResourceManager::getInstance()->home_book_padding()
#define HOME_BOOK_SCROLL_HIEGHT		ResourceManager::getInstance()->home_book_scroll_hieght()
#define HOME_NEW_BOOK_TEXT		ResourceManager::getInstance()->home_new_book_text()
#define HOME_NEW_BOOK_JP_TEXT_FIRST		ResourceManager::getInstance()->home_new_book_jp_text_first()
#define HOME_NEW_BOOK_JP_TEXT_SECOND		ResourceManager::getInstance()->home_new_book_jp_text_second()
#define HOME_NEW_BOOK_TITLE_FONTSIZE		ResourceManager::getInstance()->home_new_book_title_fontsize()
#define HOME_NEW_BOOK_JP_TITLE_FONTSIZE		ResourceManager::getInstance()->home_new_book_jp_title_fontsize()
#define HOME_NEW_BOOK_TEXT_SIZE		ResourceManager::getInstance()->home_new_book_text_size()
#define HOME_NEW_BOOK_JP_TEXT_SIZE		ResourceManager::getInstance()->home_new_book_jp_text_size()
#define HOME_NEW_BOOK_LAYER_SIZE		ResourceManager::getInstance()->home_new_book_layer_size()
#define HOME_BOOK_TITLE		ResourceManager::getInstance()->home_book_title()
#define HOME_BOOK_TITLE_A_LINE		ResourceManager::getInstance()->home_book_title_a_line()
#define HOME_PRE_BOOK_TITLE		ResourceManager::getInstance()->home_pre_book_title()
#define HOME_BOOK_TITLE_SIZE		ResourceManager::getInstance()->home_book_title_size()
#define HOME_PRE_BOOK_TITLE_SIZE		ResourceManager::getInstance()->home_pre_book_title_size()
#define HOME_BOOK_TITLE_FONTSIZE		ResourceManager::getInstance()->home_book_title_fontsize()
#define HOME_BOOK_TITLE_PRE_FONTSIZE		ResourceManager::getInstance()->home_book_title_pre_fontsize()
#define HOME_BOOK_TITLE_SUB		ResourceManager::getInstance()->home_book_title_sub()
#define HOME_BACKGROUND_SCROLL_VIEW		ResourceManager::getInstance()->home_background_scroll_view()
#define HOME_BACKGROUND_SCROLL_VIEW_HEIGHT		ResourceManager::getInstance()->home_background_scroll_view_height()
#define HOME_BACKGROUND_FRAME_TYPE_01		ResourceManager::getInstance()->home_background_frame_type_01()
#define HOME_BACKGROUND_FRAME_TYPE_02		ResourceManager::getInstance()->home_background_frame_type_02()
#define HOME_BACKGROUND_SCROLL_VIEW_STARTPADDING		ResourceManager::getInstance()->home_background_scroll_view_startpadding()
#define HOME_BACKGROUND_SCROLL_COIN_FONTSIZE_01		ResourceManager::getInstance()->home_background_scroll_coin_fontsize_01()
#define HOME_BACKGROUND_SCROLL_COIN_FONTSIZE_02		ResourceManager::getInstance()->home_background_scroll_coin_fontsize_02()
#define HOME_BACKGROUND_SCROLL_COIN_FONTSIZE_03		ResourceManager::getInstance()->home_background_scroll_coin_fontsize_03()
#define HOME_BACKGROUND_SCROLL_COIN_SIZE		ResourceManager::getInstance()->home_background_scroll_coin_size()
#define HOME_BACKGROUND_SCROLL_COIN		ResourceManager::getInstance()->home_background_scroll_coin()
#define HOME_BACKGROUND_SCROLL_END_PADDING		ResourceManager::getInstance()->home_background_scroll_end_padding()
#define HOME_BACKGROUND_LINE_START		ResourceManager::getInstance()->home_background_line_start()
#define HOME_BACKGROUND_CHECK		ResourceManager::getInstance()->home_background_check()
#define HOME_BACKGROUND_COIN_TEXT		ResourceManager::getInstance()->home_background_coin_text()
#define HOME_BG_SELECT_COIN_FONTSIZE_01		ResourceManager::getInstance()->home_bg_select_coin_fontsize_01()
#define HOME_BG_SELECT_COIN_FONTSIZE_02		ResourceManager::getInstance()->home_bg_select_coin_fontsize_02()
#define HOME_BG_SELECT_COIN_FONTSIZE_03		ResourceManager::getInstance()->home_bg_select_coin_fontsize_03()
#define HOME_BG_SELECT_COIN_MARGIN		ResourceManager::getInstance()->home_bg_select_coin_margin()
#define HOME_BACKGROUND_UNLOCK_TALK_BOX		ResourceManager::getInstance()->home_background_unlock_talk_box()
#define HOME_ITEM_TALK_BOX_SIZE		ResourceManager::getInstance()->home_item_talk_box_size()
#define HOME_ITEM_TALKBOX_INSET_RIGHT_TAIL		ResourceManager::getInstance()->home_item_talkbox_inset_right_tail()
#define HOME_ITEM_TALKBOX_INSET_RIGHT_TAIL_SIZE		ResourceManager::getInstance()->home_item_talkbox_inset_right_tail_size()
#define HOME_ITEM_TALKBOX_INSET_LEFT_TAIL		ResourceManager::getInstance()->home_item_talkbox_inset_left_tail()
#define HOME_ITEM_TALKBOX_INSET_LEFT_TAIL_SIZE		ResourceManager::getInstance()->home_item_talkbox_inset_left_tail_size()
#define HOME_ITEM_SELECT_COIN_MARGIN		ResourceManager::getInstance()->home_item_select_coin_margin()
#define HOME_ITEM_SELECT_TEXT_AREA_WIDTH		ResourceManager::getInstance()->home_item_select_text_area_width()
#define HOME_ITEM_SELECT_TEXT_BG_MID_HEIGHT		ResourceManager::getInstance()->home_item_select_text_bg_mid_height()
#define HOME_ITEM_TALKBOX		ResourceManager::getInstance()->home_item_talkbox()
#define HOME_BG_SELECT_COIN_TEXT_SIZE		ResourceManager::getInstance()->home_bg_select_coin_text_size()
#define HOME_BOTTOM_SCROLL_VIEW_FRAME_SIZE		ResourceManager::getInstance()->home_bottom_scroll_view_frame_size()
#define HOME_BOTTOM_SCROLL_VIEW_FRAME_START_PADDING		ResourceManager::getInstance()->home_bottom_scroll_view_frame_start_padding()
#define HOME_BOTTOM_SCROLL_VIEW_FRAME_BOTTOM_PADDING		ResourceManager::getInstance()->home_bottom_scroll_view_frame_bottom_padding()
#define HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_START_PADDING		ResourceManager::getInstance()->home_bottom_scroll_view_character_item_start_padding()
#define HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_HOLDER_SIZE		ResourceManager::getInstance()->home_bottom_scroll_view_character_item_holder_size()
#define HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_BOTTOM_PADDING1		ResourceManager::getInstance()->home_bottom_scroll_view_character_item_bottom_padding1()
#define HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_BOTTOM_PADDING2		ResourceManager::getInstance()->home_bottom_scroll_view_character_item_bottom_padding2()
#define HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_BOTTOM_PADDING3		ResourceManager::getInstance()->home_bottom_scroll_view_character_item_bottom_padding3()
#define HOME_BOTTOM_SCROLL_VIEW_MUSIC_ITEM_BOTTOM_PADDING1		ResourceManager::getInstance()->home_bottom_scroll_view_music_item_bottom_padding1()
#define HOME_BOTTOM_SCROLL_VIEW_MUSIC_ITEM_BOTTOM_PADDING2		ResourceManager::getInstance()->home_bottom_scroll_view_music_item_bottom_padding2()
#define HOME_BOTTOM_SCROLL_VIEW_MUSIC_ITEM_BOTTOM_PADDING3		ResourceManager::getInstance()->home_bottom_scroll_view_music_item_bottom_padding3()
#define HOME_BOTTOM_SCROLL_VIEW_MUSIC_ITEM_HOLDER_SIZE		ResourceManager::getInstance()->home_bottom_scroll_view_music_item_holder_size()
#define HOME_BOTTOM_SCROLL_VIEW_MUSIC_PIN_TOP_PADDING		ResourceManager::getInstance()->home_bottom_scroll_view_music_pin_top_padding()
#define HOME_BOTTOM_SCROLL_VIEW_ICON_IN_CHARACTER		ResourceManager::getInstance()->home_bottom_scroll_view_icon_in_character()
#define HOME_BOTTOM_SCROLL_VIEW_CHARACTER_PIN_TOP_PADDING		ResourceManager::getInstance()->home_bottom_scroll_view_character_pin_top_padding()
#define HOME_BOTTOM_SCROLL_VIEW_CENTER_ICON_BOTTOM_PADDING		ResourceManager::getInstance()->home_bottom_scroll_view_center_icon_bottom_padding()
#define HOME_BOTTOM_SCROLL_VIEW_COIN		ResourceManager::getInstance()->home_bottom_scroll_view_coin()
#define HOME_BOTTOM_SCROLL_VIEW_COIN_VISIBLE_HEIGHT		ResourceManager::getInstance()->home_bottom_scroll_view_coin_visible_height()
#define HOME_BOTTOM_SCROLL_VIEW_COIN_FONT_SIZE_DIGIT1		ResourceManager::getInstance()->home_bottom_scroll_view_coin_font_size_digit1()
#define HOME_BOTTOM_SCROLL_VIEW_COIN_FONT_SIZE_DIGIT2		ResourceManager::getInstance()->home_bottom_scroll_view_coin_font_size_digit2()
#define HOME_BOTTOM_SCROLL_VIEW_COIN_FONT_SIZE_DIGIT3		ResourceManager::getInstance()->home_bottom_scroll_view_coin_font_size_digit3()
#define HOME_BOTTOM_SCROLL_VIEW_PIN_TOP_PADDING		ResourceManager::getInstance()->home_bottom_scroll_view_pin_top_padding()
#define HOME_BOTTOM_LINE_START		ResourceManager::getInstance()->home_bottom_line_start()
#define HOME_BOTTOM_LINE_LEFT		ResourceManager::getInstance()->home_bottom_line_left()
#define HOME_BOTTOM_LINE_MID		ResourceManager::getInstance()->home_bottom_line_mid()
#define HOME_BOTTOM_LINE_RIGHT		ResourceManager::getInstance()->home_bottom_line_right()
#define HOME_TALK_BOX		ResourceManager::getInstance()->home_talk_box()
#define HOME_TALK_BOX_PURCHASE_BOTTOM_PADDING		ResourceManager::getInstance()->home_talk_box_purchase_bottom_padding()
#define HOME_TALK_BOX_FONTSIZE		ResourceManager::getInstance()->home_talk_box_fontsize()
#define HOME_TALK_BOX_SIZE		ResourceManager::getInstance()->home_talk_box_size()
#define HOME_TALK_BOX_LABEL_01		ResourceManager::getInstance()->home_talk_box_label_01()
#define HOME_TALK_BOX_LABEL_02		ResourceManager::getInstance()->home_talk_box_label_02()
#define HOME_TALK_BOX_MAX_WIDTH_SHORT		ResourceManager::getInstance()->home_talk_box_max_width_short()
#define HOME_TALK_BOX_MAX_WIDTH_LONG		ResourceManager::getInstance()->home_talk_box_max_width_long()
#define HOME_TALK_BOX_BOTTOM_MARGIN		ResourceManager::getInstance()->home_talk_box_bottom_margin()
#define HOME_TALK_BOX_SIDE_MARGIN		ResourceManager::getInstance()->home_talk_box_side_margin()
#define HOME_TALK_BOX_TOP_MARGIN		ResourceManager::getInstance()->home_talk_box_top_margin()
#define HOME_FRAME_SPRITE_SIZE		ResourceManager::getInstance()->home_frame_sprite_size()
#define HOME_DELETE_BTN_TYPE01		ResourceManager::getInstance()->home_delete_btn_type01()
#define HOME_FRAMEBG		ResourceManager::getInstance()->home_framebg()
#define HOME_CREATE_BTN		ResourceManager::getInstance()->home_create_btn()
#define HOME_CREATE_BTN_VISIBLE_RECT		ResourceManager::getInstance()->home_create_btn_visible_rect()
#define HOME_CREATE_BG_BTN		ResourceManager::getInstance()->home_create_bg_btn()
#define HOME_CREATE_FONTSIZE		ResourceManager::getInstance()->home_create_fontsize()
#define HOME_CREATE_TEXTSIZE		ResourceManager::getInstance()->home_create_textsize()
#define HOME_CREATE_TEXT		ResourceManager::getInstance()->home_create_text()
#define HOME_DELETE_BTN_PADDING		ResourceManager::getInstance()->home_delete_btn_padding()
#define HOME_DELETE_BTN_GLOW_SIZE		ResourceManager::getInstance()->home_delete_btn_glow_size()
#define HOME_BACKGROUND_LAYER_SHADOW		ResourceManager::getInstance()->home_background_layer_shadow()
#define HOME_BACKGROUND_LAYER_COIN		ResourceManager::getInstance()->home_background_layer_coin()
#define HOME_BACKGROUND_LAYER_COIN_TEXT_FONTSIZE		ResourceManager::getInstance()->home_background_layer_coin_text_fontsize()
#define HOME_CUSTOM_BG_BOTTOM_PADDING		ResourceManager::getInstance()->home_custom_bg_bottom_padding()
#define HOME_CUSTOM_BG_SIZE		ResourceManager::getInstance()->home_custom_bg_size()
#define HOME_BACKGROUND_ITEM_BG_ANI01		ResourceManager::getInstance()->home_background_item_bg_ani01()
#define HOME_BACKGROUND_ITEM_BG_ANI02		ResourceManager::getInstance()->home_background_item_bg_ani02()
#define HOME_BACKGROUND_ITEM_BG_ANI04		ResourceManager::getInstance()->home_background_item_bg_ani04()
#define HOME_BACKGROUND_ITEM_BG_ANI06		ResourceManager::getInstance()->home_background_item_bg_ani06()
#define HOME_BACKGROUND_ITEM_BG_ANI07		ResourceManager::getInstance()->home_background_item_bg_ani07()
#define HOME_BACKGROUND_ITEM_BG_ANI08		ResourceManager::getInstance()->home_background_item_bg_ani08()
#define HOME_BACKGROUND_ITEM_BG_ANI09		ResourceManager::getInstance()->home_background_item_bg_ani09()
#define HOME_BACKGROUND_ITEM_BG_ANI10		ResourceManager::getInstance()->home_background_item_bg_ani10()
#define HOME_BACKGROUND_ITEM_BG_ANI11		ResourceManager::getInstance()->home_background_item_bg_ani11()
#define HOME_BACKGROUND_ITEM_BG_ANI12		ResourceManager::getInstance()->home_background_item_bg_ani12()
#define HOME_CUSTOM_BG_BIG		ResourceManager::getInstance()->home_custom_bg_big()
#define HOME_CUSTOM_BG_BOOK_THUMB		ResourceManager::getInstance()->home_custom_bg_book_thumb()
#define HOME_CUSTOM_BG_WIDTH_INBOOK		ResourceManager::getInstance()->home_custom_bg_width_inbook()
#define HOME_SETTING_BG_IMAGE_SIZE		ResourceManager::getInstance()->home_setting_bg_image_size()
#define HOME_SETTING_BG_CAPINSETS		ResourceManager::getInstance()->home_setting_bg_capinsets()
#define HOME_SETTING_BG_CAPINSETS_SIZE		ResourceManager::getInstance()->home_setting_bg_capinsets_size()
#define HOME_SETTING_BG_SIZE		ResourceManager::getInstance()->home_setting_bg_size()
#define HOME_SETTING_BG		ResourceManager::getInstance()->home_setting_bg()
#define HOME_SETTING_LIST_PRESS_IMAGE_SIZE		ResourceManager::getInstance()->home_setting_list_press_image_size()
#define HOME_SETTING_LIST_PRESS_CAPINSETS		ResourceManager::getInstance()->home_setting_list_press_capInsets()
#define HOME_SETTING_LIST_PRESS_CAPINSETS_SIZE		ResourceManager::getInstance()->home_setting_list_press_capInsets_size()
#define HOME_SETTING_LIST_PRESS		ResourceManager::getInstance()->home_setting_list_press()
#define HOME_SETTING_LIST_PRESS_SIZE		ResourceManager::getInstance()->home_setting_list_press_size()
#define HOME_SETTING_ICON		ResourceManager::getInstance()->home_setting_icon()
#define HOME_SETTING_LABEL_FONTSIZE		ResourceManager::getInstance()->home_setting_label_fontsize()
#define HOME_SETTING_LABEL		ResourceManager::getInstance()->home_setting_label()
#define HOME_DIM_LIMIT_TIME_FONTSIZE_01		ResourceManager::getInstance()->home_dim_limit_time_fontsize_01()
#define HOME_DIM_LIMIT_TIME_FONTSIZE_02		ResourceManager::getInstance()->home_dim_limit_time_fontsize_02()
#define HOME_DIM_LIMIT_TIME_LABLE_SIZE		ResourceManager::getInstance()->home_dim_limit_time_lable_size()
#define HOME_DIM_LIMIT_TIME_Y		ResourceManager::getInstance()->home_dim_limit_time_y()
#define HOME_DIM_LIMIT_NAME_LABEL_FONTSIZE		ResourceManager::getInstance()->home_dim_limit_name_label_fontsize()
#define HOME_DIM_LIMIT_NAME_LABEL_SIZE		ResourceManager::getInstance()->home_dim_limit_name_label_size()
#define HOME_DIM_LIMIT_NAME_LABEL_Y		ResourceManager::getInstance()->home_dim_limit_name_label_y()
#define HOME_DIM_LIMIT_CHAR_TALKBOX_Y		ResourceManager::getInstance()->home_dim_limit_char_talkbox_y()
#define HOME_FRAME_LIMIT_FONTSIZE_01		ResourceManager::getInstance()->home_frame_limit_fontsize_01()
#define HOME_FRAME_LIMIT_FONTSIZE_02		ResourceManager::getInstance()->home_frame_limit_fontsize_02()
#define HOME_FRAME_LIMIT_LABEL_SIZE		ResourceManager::getInstance()->home_frame_limit_label_size()
#define HOME_FRAME_LIMIT_LABEL		ResourceManager::getInstance()->home_frame_limit_label()
#define BOOKMAP_BG_MAP		ResourceManager::getInstance()->bookmap_bg_map()
#define BOOKMAP_BTN_BACK		ResourceManager::getInstance()->bookmap_btn_back()
#define BOOKMAP_TITLE_LABEL		ResourceManager::getInstance()->bookmap_title_label()
#define BOOKMAP_TITLE_LABEL_FONTSIZE		ResourceManager::getInstance()->bookmap_title_label_fontsize()
#define BOOKMAP_SINGLE_TITLE_LABEL_SIZE		ResourceManager::getInstance()->bookmap_single_title_label_size()
#define BOOKMAP_DOUBLE_TITLE_LABEL_SIZE		ResourceManager::getInstance()->bookmap_double_title_label_size()
#define BOOKMAP_SINGLE_TITLE_LABEL		ResourceManager::getInstance()->bookmap_single_title_label()
#define BOOKMAP_DOUBLE_TITLE_LABEL_01		ResourceManager::getInstance()->bookmap_double_title_label_01()
#define BOOKMAP_DOUBLE_TITLE_LABEL_02		ResourceManager::getInstance()->bookmap_double_title_label_02()
#define BOOKMAP_NAME_LABEL_FONTSIZE		ResourceManager::getInstance()->bookmap_name_label_fontsize()
#define BOOKMAP_SINGLE_NAME_LABEL_SIZE		ResourceManager::getInstance()->bookmap_single_name_label_size()
#define BOOKMAP_DOUBLE_NAME_LABEL_SIZE		ResourceManager::getInstance()->bookmap_double_name_label_size()
#define BOOKMAP_SINGLE_NAME_LABEL		ResourceManager::getInstance()->bookmap_single_name_label()
#define BOOKMAP_DOUBLE_NAME_LABEL_01		ResourceManager::getInstance()->bookmap_double_name_label_01()
#define BOOKMAP_DOUBLE_NAME_LABEL_02		ResourceManager::getInstance()->bookmap_double_name_label_02()
#define BOOKMAP_SUBTITLE_LABEL		ResourceManager::getInstance()->bookmap_subtitle_label()
#define BOOKMAP_SUBTITLE_LABEL_FONTSIZE		ResourceManager::getInstance()->bookmap_subtitle_label_fontsize()
#define BOOKMAP_SUBTITLE_LABEL_SIZE		ResourceManager::getInstance()->bookmap_subtitle_label_size()
#define BOOKMAP_FEATHER01		ResourceManager::getInstance()->bookmap_feather01()
#define BOOKMAP_FEATHER02		ResourceManager::getInstance()->bookmap_feather02()
#define BOOKMAP_FEATHER03		ResourceManager::getInstance()->bookmap_feather03()
#define BOOKMAP_FEATHER04		ResourceManager::getInstance()->bookmap_feather04()
#define BOOKMAP_FEATHER05		ResourceManager::getInstance()->bookmap_feather05()
#define BOOKMAP_MENU_BTN		ResourceManager::getInstance()->bookmap_menu_btn()
#define BOOKMAP_PLAY_BTN		ResourceManager::getInstance()->bookmap_play_btn()
#define BOOKMAP_TITLE_BG		ResourceManager::getInstance()->bookmap_title_bg()
#define BOOKMAP_INTRO		ResourceManager::getInstance()->bookmap_intro()
#define BOOKMAP_FINAL		ResourceManager::getInstance()->bookmap_final()
#define BOOKMAP_SCENE_2		ResourceManager::getInstance()->bookmap_scene_2()
#define BOOKMAP_SCENE_3		ResourceManager::getInstance()->bookmap_scene_3()
#define BOOKMAP_SCENE_4		ResourceManager::getInstance()->bookmap_scene_4()
#define BOOKMAP_FLAG_FONTSIZE_1		ResourceManager::getInstance()->bookmap_flag_fontsize_1()
#define BOOKMAP_FLAG_FONTSIZE_N		ResourceManager::getInstance()->bookmap_flag_fontsize_n()
#define BOOKMAP_FLAG_FONTSIZE_5		ResourceManager::getInstance()->bookmap_flag_fontsize_5()
#define BOOKMAP_FLAG_POS_TYPE01		ResourceManager::getInstance()->bookmap_flag_pos_type01()
#define BOOKMAP_FLAG_POS_TYPE02		ResourceManager::getInstance()->bookmap_flag_pos_type02()
#define BOOKMAP_FLAG_SIZE_TYPE01		ResourceManager::getInstance()->bookmap_flag_size_type01()
#define BOOKMAP_FLAG_SIZE_TYPE02		ResourceManager::getInstance()->bookmap_flag_size_type02()
#define BOOKMAP_CHARACTER01		ResourceManager::getInstance()->bookmap_character01()
#define BOOKMAP_CHARACTER02		ResourceManager::getInstance()->bookmap_character02()
#define BOOKMAP_CHARACTER03		ResourceManager::getInstance()->bookmap_character03()
#define BOOKMAP_CHARACTER04		ResourceManager::getInstance()->bookmap_character04()
#define BOOKMAP_CHARACTER05		ResourceManager::getInstance()->bookmap_character05()
#define BOOKMAP_DELETE_BTN01		ResourceManager::getInstance()->bookmap_delete_btn01()
#define BOOKMAP_DELETE_BTN02		ResourceManager::getInstance()->bookmap_delete_btn02()
#define BOOKMAP_DELETE_BTN03		ResourceManager::getInstance()->bookmap_delete_btn03()
#define BOOKMAP_DELETE_BTN04		ResourceManager::getInstance()->bookmap_delete_btn04()
#define BOOKMAP_DELETE_BTN05		ResourceManager::getInstance()->bookmap_delete_btn05()
#define BOOKMAP_ICON_BG01		ResourceManager::getInstance()->bookmap_icon_bg01()
#define BOOKMAP_ICON_BG02		ResourceManager::getInstance()->bookmap_icon_bg02()
#define BOOKMAP_ICON_BG03		ResourceManager::getInstance()->bookmap_icon_bg03()
#define BOOKMAP_ICON_BG04		ResourceManager::getInstance()->bookmap_icon_bg04()
#define BOOKMAP_ICON_BG05		ResourceManager::getInstance()->bookmap_icon_bg05()
#define BOOKMAP_AUTHOR_TOUCH_WIDTH		ResourceManager::getInstance()->bookmap_author_touch_width()
#define BOOKMAP_EDIT_BOX_Y		ResourceManager::getInstance()->bookmap_edit_box_Y()
#define HOME_NEW_BOOK_START_PADDING		ResourceManager::getInstance()->home_new_book_start_padding()
#define BOOKMAP_TITLE_Y		ResourceManager::getInstance()->bookmap_title_y()
#define BOOKMAP_MENULIST_BG_SIZE		ResourceManager::getInstance()->bookmap_menulist_bg_size()
#define BOOKMAP_MENULIST_PRESS_SIZE		ResourceManager::getInstance()->bookmap_menulist_press_size()
#define BOOKMAP_MENULIST_LABEL_SIZE		ResourceManager::getInstance()->bookmap_menulist_label_size()
#define BOOKMAP_MENULIST_BUTTON_HEIGHT		ResourceManager::getInstance()->bookmap_menulist_button_height()
#define BOOKMAP_MENULIST_MID_LABEL		ResourceManager::getInstance()->bookmap_menulist_mid_label()
#define BOOKMAP_MENULIST_MID_ICON		ResourceManager::getInstance()->bookmap_menulist_mid_icon()
#define CAMERA_BTN_BACK		ResourceManager::getInstance()->camera_btn_back()
#define LOGINLAYER_BACKGROUND_SIZE		ResourceManager::getInstance()->LoginLayer_background_Size()
#define ACCOUNTLAYER_POSITION_FULL_POPUP_CHARACTER_01		ResourceManager::getInstance()->AccountLayer_Position_full_popup_character_01()
#define ACCOUNTLAYER_POSITION_FULL_POPUP_CHARACTER_02		ResourceManager::getInstance()->AccountLayer_Position_full_popup_character_02()
#define ACCOUNTLAYER_POSITION_LOGIN_LOGO		ResourceManager::getInstance()->AccountLayer_Position_login_logo()
#define ACCOUNTLAYER_SIZE_BTN_CANCEL_NORMAL		ResourceManager::getInstance()->AccountLayer_Size_btn_cancel_normal()
#define ACCOUNTLAYER_POSITION_BTN_CANCEL_NORMAL		ResourceManager::getInstance()->AccountLayer_Position_btn_cancel_normal()
#define LOGINLAYER_POSITION_CONTROLBUTTON_ID		ResourceManager::getInstance()->LoginLayer_Position_ControlButton_ID()
#define LOGINLAYER_POSITION_CONTROLBUTTON_PW		ResourceManager::getInstance()->LoginLayer_Position_ControlButton_PW()
#define LOGINLAYER_SIZE_BUTTON_ACCOUNTCONTROL		ResourceManager::getInstance()->LoginLayer_Size_Button_AccountControl()
#define LOGINLAYER_FONTSIZE_ACCOUNTCONTROL		ResourceManager::getInstance()->LoginLayer_FontSize_AccountControl()
#define LOGINLAYER_POSITION_BUTTON_FORGOTPASSWORD		ResourceManager::getInstance()->LoginLayer_Position_Button_ForgotPassword()
#define LOGINLAYER_POSITION_BUTTON_SIGNUP		ResourceManager::getInstance()->LoginLayer_Position_Button_Signup()
#define LOGINLAYER_POSITION_BUTTON_FACEBOOKLOGIN		ResourceManager::getInstance()->LoginLayer_Position_Button_FacebookLogin()
#define ACCOUNTLAYER_BUTTON_NORMAL_01_WIDTH		ResourceManager::getInstance()->AccountLayer_button_normal_01_Width()
#define ACCOUNTLAYER_BUTTON_NORMAL_01_HEIGHT		ResourceManager::getInstance()->AccountLayer_button_normal_01_Height()
#define ACCOUNTLAYER_BUTTON_NORMAL_01_INLEFT		ResourceManager::getInstance()->AccountLayer_button_normal_01_InLeft()
#define ACCOUNTLAYER_BUTTON_NORMAL_01_INTOP		ResourceManager::getInstance()->AccountLayer_button_normal_01_InTop()
#define ACCOUNTLAYER_BUTTON_NORMAL_01_INWIDTH		ResourceManager::getInstance()->AccountLayer_button_normal_01_inWidth()
#define ACCOUNTLAYER_BUTTON_NORMAL_01_INHEIGHT		ResourceManager::getInstance()->AccountLayer_button_normal_01_inHeight()
#define LOGINLAYER_FONTSIZE_BUTTON_LOGIN		ResourceManager::getInstance()->LoginLayer_FontSize_Button_Login()
#define LOGINLAYER_SIZE_BUTTON_LOGIN		ResourceManager::getInstance()->LoginLayer_Size_Button_Login()
#define LOGINLAYER_POSITION_BUTTON_LOGIN		ResourceManager::getInstance()->LoginLayer_Position_Button_Login()
#define LOGINLAYER_SIZE_SKIPBUTTON		ResourceManager::getInstance()->LoginLayer_Size_SkipButton()
#define LOGINLAYER_POSITION_SKIPBUTTON		ResourceManager::getInstance()->LoginLayer_Position_SkipButton()
#define LOGINLAYER_SIZE_SKIPLABEL		ResourceManager::getInstance()->LoginLayer_Size_SkipLabel()
#define LOGINLAYER_POSITION_SKIPLABEL_X		ResourceManager::getInstance()->LoginLayer_Position_SkipLabel_x()
#define LOGINLAYER_POSITION_SKIPLABEL_Y_MARGIN		ResourceManager::getInstance()->LoginLayer_Position_SkipLabel_y_Margin()
#define LOGINLAYER_FONTSIZE_SKIPLABEL		ResourceManager::getInstance()->LoginLayer_FontSize_SkipLabel()
#define LOGINLAYER_FONTSIZE_FORGOTNOTICELAYER		ResourceManager::getInstance()->LoginLayer_FontSize_ForgotNoticeLayer()
#define LOGINLAYER_SIZE_FORGOTNOTICELABEL		ResourceManager::getInstance()->LoginLayer_Size_ForgotNoticeLabel()
#define LOGINLAYER_POSITION_FORGOTNOTICELABEL		ResourceManager::getInstance()->LoginLayer_Position_ForgotNoticeLabel()
#define ACCOUNTLAYER_FONTSIZE_RADIOLABEL		ResourceManager::getInstance()->AccountLayer_FontSize_RadioLabel()
#define TSICONEDITBOX_SIZE_WIDTH		ResourceManager::getInstance()->TSIconEditBox_Size_Width()
#define TSICONEDITBOX_SIZE_HEIGHT		ResourceManager::getInstance()->TSIconEditBox_Size_Height()
#define TSICONEDITBOX_POSITION_ICON		ResourceManager::getInstance()->TSIconEditBox_Position_Icon()
#define TSICONEDITBOX_SIZE_TEXTFILED		ResourceManager::getInstance()->TSIconEditBox_Size_textFiled()
#define TSICONEDITBOX_POSITION_TEXTFIELD		ResourceManager::getInstance()->TSIconEditBox_Position_textField()
#define TSICONEDITBOX_FONTSIZE_TEXTFIELD		ResourceManager::getInstance()->TSIconEditBox_FontSize_textField()
#define TSICONEDITBOX_FONTSIZE_ERRORCOMMENT		ResourceManager::getInstance()->TSIconEditBox_FontSize_ErrorComment()
#define TSICONEDITBOX_SIZE_ERRORCOMMENT		ResourceManager::getInstance()->TSIconEditBox_Size_ErrorComment()
#define TSICONEDITBOX_POSITION_INPUTCOMMENT1_X		ResourceManager::getInstance()->TSIconEditBox_Position_InputComment1_x()
#define TSICONEDITBOX_POSITION_INPUTCOMMENT2_X		ResourceManager::getInstance()->TSIconEditBox_Position_InputComment2_x()
#define TSICONEDITBOX_POSITION_INPUTCOMMENT_Y		ResourceManager::getInstance()->TSIconEditBox_Position_InputComment_y()
#define TSICONEDITBOX_SIZE_INPUTCOMMENT_LABEL_WIDTHDELTA		ResourceManager::getInstance()->TSIconEditBox_Size_InputComment_Label_WidthDelta()
#define TSICONEDITBOX_SIZE_INPUTCOMMENT_LABEL_HEIGHT		ResourceManager::getInstance()->TSIconEditBox_Size_InputComment_Label_Height()
#define TSICONEDITBOX_FONTSIZE_INPUTCOMMENT		ResourceManager::getInstance()->TSIconEditBox_FontSize_InputComment()
#define SIGNUPLAYER_POSITION_IDEDITBOX		ResourceManager::getInstance()->SignupLayer_Position_IdEditBox()
#define SIGNUPLAYER_POSITION_PWEDITBOX		ResourceManager::getInstance()->SignupLayer_Position_PWEditBox()
#define SIGNUPLAYER_POSITION_CPWEDITBOX		ResourceManager::getInstance()->SignupLayer_Position_CPWEditBox()
#define SIGNUPLAYER_POSITION_BIRTHEDITBOX		ResourceManager::getInstance()->SignupLayer_Position_BirthEditBox()
#define SIGNUPLAYER_POSITION_CITYEIDTBOX		ResourceManager::getInstance()->SignupLayer_Position_CityEidtBox()
#define SIGNUPLAYER_POSITION_MAILRADIO		ResourceManager::getInstance()->SignupLayer_Position_MailRadio()
#define SIGNUPLAYER_POSITION_FEMAILRADIO		ResourceManager::getInstance()->SignupLayer_Position_FemailRadio()
#define SIGNUPLAYER_POSITION_TERMSCHECKBOX		ResourceManager::getInstance()->SignupLayer_Position_TermsCheckBox()
#define SIGNUPLAYER_POSITION_AGREEMENTTEXTLABEL		ResourceManager::getInstance()->SignupLayer_Position_AgreementTextLabel()
#define SIGNUPLAYER_FONTSIZE_AGREEMENTTEXTLABEL		ResourceManager::getInstance()->SignupLayer_FontSize_AgreementTextLabel()
#define SIGNUPLAYER_FONTSIZE_BUTTON_LOGIN		ResourceManager::getInstance()->SignupLayer_FontSize_Button_Login()
#define SIGNUPLAYER_SIZE_SIGNUPBUTTON		ResourceManager::getInstance()->SignupLayer_Size_SignupButton()
#define SIGNUPLAYER_POSITION_SIGNUPBUTTON		ResourceManager::getInstance()->SignupLayer_Position_SignupButton()
#define SIGNTERMSLAYER_POSITION_TERMTEXTTOP		ResourceManager::getInstance()->SignTermsLayer_Position_TermTextTop()
#define SIGNTERMSLAYER_POSITION_TERMTEXTBOTTOM		ResourceManager::getInstance()->SignTermsLayer_Position_TermTextBottom()
#define SIGNTERMSLAYER_DELTA_TERMTEXTMID_WIDTH		ResourceManager::getInstance()->SignTermsLayer_Delta_TermTextMid_Width()
#define SIGNTERMSLAYER_DELTA_TERMTEXTMID_HEIGHT		ResourceManager::getInstance()->SignTermsLayer_Delta_TermTextMid_Height()
#define SIGNTERMSLAYER_POSITION_TERMTEXTMID		ResourceManager::getInstance()->SignTermsLayer_Position_TermTextMid()
#define SIGNTERMSLAYER_POSITION_TERMTEXT_TITLE		ResourceManager::getInstance()->SignTermsLayer_Position_TermText_Title()
#define SIGNTERMSLAYER_FONTSIZE_TERMTEXT_TITLE		ResourceManager::getInstance()->SignTermsLayer_FontSize_TermText_Title()
#define SIGNTERMSLAYER_FONTSIZE_TERMTEXT_TEXT_LABEL		ResourceManager::getInstance()->SignTermsLayer_FontSize_TermText_Text_Label()
#define SIGNTERMSLAYER_SIZE_TERMTEXT_TEXT_LABEL_WIDTH		ResourceManager::getInstance()->SignTermsLayer_Size_TermText_Text_Label_Width()
#define SIGNTERMSLAYER_SIZE_TERMTEXT_SCROLLVIEW		ResourceManager::getInstance()->SignTermsLayer_Size_TermText_ScrollView()
#define SIGNTERMSLAYER_POSITION_TERMTEXT_SCROLLVIEW		ResourceManager::getInstance()->SignTermsLayer_Position_TermText_ScrollView()
#define SIGNTERMSLAYER_POSITION_POLICYTEXTTOP		ResourceManager::getInstance()->SignTermsLayer_Position_PolicyTextTop()
#define SIGNTERMSLAYER_POSITION_POLICYTEXTBOTTOM		ResourceManager::getInstance()->SignTermsLayer_Position_PolicyTextBottom()
#define SIGNTERMSLAYER_DELTA_POLICYTEXTMID_WIDTH		ResourceManager::getInstance()->SignTermsLayer_Delta_PolicyTextMid_Width()
#define SIGNTERMSLAYER_DELTA_POLICYTEXTMID_HEIGHT		ResourceManager::getInstance()->SignTermsLayer_Delta_PolicyTextMid_Height()
#define SIGNTERMSLAYER_POSITION_POLICYTEXTMID		ResourceManager::getInstance()->SignTermsLayer_Position_PolicyTextMid()
#define SIGNTERMSLAYER_POSITION_POLICYTEXT_TITLE		ResourceManager::getInstance()->SignTermsLayer_Position_PolicyText_Title()
#define SIGNTERMSLAYER_FONTSIZE_POLICYTEXT_TITLE		ResourceManager::getInstance()->SignTermsLayer_FontSize_PolicyText_Title()
#define SIGNTERMSLAYER_FONTSIZE_POLICYTEXT_TEXT_LABEL		ResourceManager::getInstance()->SignTermsLayer_FontSize_PolicyText_Text_Label()
#define SIGNTERMSLAYER_SIZE_POLICYTEXT_TEXT_LABEL_WIDTH		ResourceManager::getInstance()->SignTermsLayer_Size_PolicyText_Text_Label_Width()
#define SIGNTERMSLAYER_SIZE_POLICYTEXT_SCROLLVIEW		ResourceManager::getInstance()->SignTermsLayer_Size_PolicyText_ScrollView()
#define SIGNTERMSLAYER_POSITION_POLICYTEXT_SCROLLVIEW		ResourceManager::getInstance()->SignTermsLayer_Position_PolicyText_ScrollView()
#define SIGNTERMSLAYER_POSITION_PCHECKBOX		ResourceManager::getInstance()->SignTermsLayer_Position_PCheckBox()
#define SIGNTERMSLAYER_POSITION_TERMSCHECKBOX_LABEL_X		ResourceManager::getInstance()->SignTermsLayer_Position_TermsCheckBox_Label_x()
#define SIGNTERMSLAYER_POSITION_TERMSCHECKBOX_LABEL_Y		ResourceManager::getInstance()->SignTermsLayer_Position_TermsCheckBox_Label_y()
#define SIGNTERMSLAYER_POSITION_PRIVACYCHECKBOX		ResourceManager::getInstance()->SignTermsLayer_Position_PrivacyCheckBox()
#define SIGNTERMSLAYER_POSITION_PRIVACYCHECKBOX_LABEL_X		ResourceManager::getInstance()->SignTermsLayer_Position_PrivacyCheckBox_Label_x()
#define SIGNTERMSLAYER_POSITION_PRIVACYCHECKBOX_LABEL_Y		ResourceManager::getInstance()->SignTermsLayer_Position_PrivacyCheckBox_Label_y()
#define SIGNTERMSLAYER_FONTSIZE_CANCEL_BUTTON		ResourceManager::getInstance()->SignTermsLayer_FontSize_Cancel_Button()
#define SIGNTERMSLAYER_SIZE_CANCEL_BUTTON		ResourceManager::getInstance()->SignTermsLayer_Size_Cancel_Button()
#define SIGNTERMSLAYER_POSITION_CANCEL_BUTTON		ResourceManager::getInstance()->SignTermsLayer_Position_Cancel_Button()
#define SIGNTERMSLAYER_FONTSIZE_NEXT_BUTTON		ResourceManager::getInstance()->SignTermsLayer_FontSize_Next_Button()
#define SIGNTERMSLAYER_SIZE_NEXT_BUTTON		ResourceManager::getInstance()->SignTermsLayer_Size_Next_Button()
#define SIGNTERMSLAYER_POSITION_NEXT_BUTTON		ResourceManager::getInstance()->SignTermsLayer_Position_Next_Button()
#define POPUP_BOTTOMBG_SIZE		ResourceManager::getInstance()->popup_bottombg_size()
#define POPUP_BOTTOMBG_CORNER_SIZE		ResourceManager::getInstance()->popup_bottombg_corner_size()
#define POPUP_TOP_PADDING		ResourceManager::getInstance()->popup_top_padding()
#define POPUP_MIDDLE_PADDING		ResourceManager::getInstance()->popup_middle_padding()
#define POPUP_BOTTOM_PADDING		ResourceManager::getInstance()->popup_bottom_padding()
#define POPUP_ONE_BUTTON_SIZE		ResourceManager::getInstance()->popup_one_button_size()
#define POPUP_TWO_BUTTON_SIZE		ResourceManager::getInstance()->popup_two_button_size()
#define POPUP_BUTTON_PADDING		ResourceManager::getInstance()->popup_button_padding()
#define POPUP_BUTTON_IMAGE_SIZE		ResourceManager::getInstance()->popup_button_image_size()
#define POPUP_BUTTON_CORNER_SIZE		ResourceManager::getInstance()->popup_button_corner_size()
#define POPUP_MESSEGE_FONTSIZE		ResourceManager::getInstance()->popup_messege_fontsize()
#define POPUP_BUTTON_FONTSIZE		ResourceManager::getInstance()->popup_button_fontsize()
#define POPUP_BUTTON_TWOLINE_FONTSIZE		ResourceManager::getInstance()->popup_button_twoline_fontsize()
#define POPUP_BUTTON_TWOLINE_LABEL_WIDTH		ResourceManager::getInstance()->popup_button_twoline_label_width()
#define POPUP_BUTTON_2LINE_FONTSIZE		ResourceManager::getInstance()->popup_button_2line_fontsize()
#define POPUP_LIST_TOPBG_SIZE		ResourceManager::getInstance()->popup_list_topbg_size()
#define POPUP_LIST_TOPBG_CORNER_SIZE		ResourceManager::getInstance()->popup_list_topbg_corner_size()
#define POPUP_WIDTH		ResourceManager::getInstance()->popup_width()
#define POPUP_TITLE_FONTSIZE		ResourceManager::getInstance()->popup_title_fontsize()
#define POPUP_TITLE_LABEL_SIZE		ResourceManager::getInstance()->popup_title_label_size()
#define POPUP_SHARED_CELL_SIZE		ResourceManager::getInstance()->popup_shared_cell_size()
#define POPUP_TITLE		ResourceManager::getInstance()->popup_title()
#define POPUP_SHARED_TABLEVIEW_SIZE		ResourceManager::getInstance()->popup_shared_tableview_size()
#define POPUP_SHARED_MIDDLE_HEIGHT		ResourceManager::getInstance()->popup_shared_middle_height()
#define POPUP_SHARED_ICON		ResourceManager::getInstance()->popup_shared_icon()
#define POPUP_SHARED_ICON_SIZE		ResourceManager::getInstance()->popup_shared_icon_size()
#define POPUP_SHARED_CELL_FONTSIZE		ResourceManager::getInstance()->popup_shared_cell_fontsize()
#define POPUP_SHARED_CELL_LABEL_SIZE		ResourceManager::getInstance()->popup_shared_cell_label_size()
#define POPUP_SHARED_CELL_LABEL		ResourceManager::getInstance()->popup_shared_cell_label()
#define POPUP_YEAR_BACKGROUND_HEIGHT		ResourceManager::getInstance()->popup_year_background_height()
#define POPUP_YEAR_CELL_SIZE		ResourceManager::getInstance()->popup_year_cell_size()
#define POPUP_YEAR_CELL_LABEL_SIZE		ResourceManager::getInstance()->popup_year_cell_label_size()
#define POPUP_YEAR_CELL_LABEL		ResourceManager::getInstance()->popup_year_cell_label()
#define POPUP_YEAR_PICKER_LINE_01		ResourceManager::getInstance()->popup_year_picker_line_01()
#define POPUP_YEAR_PICKER_LINE_02		ResourceManager::getInstance()->popup_year_picker_line_02()
#define POPUP_YEAR_PICKER_LINE_SIZE		ResourceManager::getInstance()->popup_year_picker_line_size()
#define RECORDING_BG_FRAME		ResourceManager::getInstance()->recording_bg_frame()
#define RECORDING_ITEM_RIGHT_PADDING		ResourceManager::getInstance()->recording_item_right_padding()
#define RECORDING_ITEM_BOTTOM_PADDING		ResourceManager::getInstance()->recording_item_bottom_padding()
#define RECORDING_ITEM_PADDING		ResourceManager::getInstance()->recording_item_padding()
#define RECORDING_BOTTOM_SCROLL_VIEW_FRAME_START_PADDING		ResourceManager::getInstance()->recording_bottom_scroll_view_frame_start_padding()
#define RECORDING_BOTTOM_SCROLL_VIEW_FRAME_BOTTOM_PADDING		ResourceManager::getInstance()->recording_bottom_scroll_view_frame_bottom_padding()
#define RECORDING_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_START_PADDING		ResourceManager::getInstance()->recording_bottom_scroll_view_character_item_start_padding()
#define RECORDING_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_HOLDER_SIZE		ResourceManager::getInstance()->recording_bottom_scroll_view_character_item_holder_size()
#define RECORDING_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_BOTTOM_PADDING1		ResourceManager::getInstance()->recording_bottom_scroll_view_character_item_bottom_padding1()
#define RECORDING_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_BOTTOM_PADDING2		ResourceManager::getInstance()->recording_bottom_scroll_view_character_item_bottom_padding2()
#define RECORDING_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_BOTTOM_PADDING3		ResourceManager::getInstance()->recording_bottom_scroll_view_character_item_bottom_padding3()
#define RECORDING_BOTTOM_SCROLL_VIEW_ICON_IN_CHARACTER		ResourceManager::getInstance()->recording_bottom_scroll_view_icon_in_character()
#define RECORDING_BOTTOM_SCROLL_VIEW_CHARACTER_PIN_TOP_PADDING		ResourceManager::getInstance()->recording_bottom_scroll_view_character_pin_top_padding()
#define RECORDING_BOTTOM_SCROLL_VIEW_CENTER_ICON_BOTTOM_PADDING		ResourceManager::getInstance()->recording_bottom_scroll_view_center_icon_bottom_padding()
#define RECORDING_BOTTOM_SCROLL_VIEW_COIN		ResourceManager::getInstance()->recording_bottom_scroll_view_coin()
#define RECORDING_BOTTOM_SCROLL_VIEW_COIN_VISIBLE_HEIGHT		ResourceManager::getInstance()->recording_bottom_scroll_view_coin_visible_height()
#define RECORDING_BOTTOM_SCROLL_VIEW_COIN_FONT_SIZE_DIGIT1		ResourceManager::getInstance()->recording_bottom_scroll_view_coin_font_size_digit1()
#define RECORDING_BOTTOM_SCROLL_VIEW_COIN_FONT_SIZE_DIGIT2		ResourceManager::getInstance()->recording_bottom_scroll_view_coin_font_size_digit2()
#define RECORDING_BOTTOM_SCROLL_VIEW_COIN_FONT_SIZE_DIGIT3		ResourceManager::getInstance()->recording_bottom_scroll_view_coin_font_size_digit3()
#define RECORDING_BOTTOM_SCROLL_AREA_HEIGHT		ResourceManager::getInstance()->recording_bottom_scroll_area_height()
#define RECORDING_BOTTOM_SCROLL_VIEW_HEIGHT		ResourceManager::getInstance()->recording_bottom_scroll_view_height()
#define RECORDING_BOTTOM_SCROLL_VIEW_PIN_BOTTOM_PADDING		ResourceManager::getInstance()->recording_bottom_scroll_view_pin_bottom_padding()
#define RECORDING_BOTTOM_LINE_START		ResourceManager::getInstance()->recording_bottom_line_start()
#define RECORDING_BOTTOM_LINE_LEFT		ResourceManager::getInstance()->recording_bottom_line_left()
#define RECORDING_BOTTOM_LINE_MID		ResourceManager::getInstance()->recording_bottom_line_mid()
#define RECORDING_BOTTOM_LINE_RIGHT		ResourceManager::getInstance()->recording_bottom_line_right()
#define RECORDING_BG_CREATE_BTN_VISIBLE_RECT		ResourceManager::getInstance()->recording_bg_create_btn_visible_rect()
#define RECORDING_RECORD_ICON		ResourceManager::getInstance()->recording_record_icon()
#define RECORDING_RECORD_TEXT_FONT_SIZE		ResourceManager::getInstance()->recording_record_text_font_size()
#define RECORDING_RECORD_TEXT_LEFT_PADDING		ResourceManager::getInstance()->recording_record_text_left_padding()
#define RECORDING_RECORD_TEXT_RIGHT_PADDING		ResourceManager::getInstance()->recording_record_text_right_padding()
#define RECORDING_RECORD_BUTTON		ResourceManager::getInstance()->recording_record_button()
#define RECORDING_DONE_ICON		ResourceManager::getInstance()->recording_done_icon()
#define RECORDING_DONE_TEXT_FONT_SIZE		ResourceManager::getInstance()->recording_done_text_font_size()
#define RECORDING_DONE_TEXT_LEFT_PADDING		ResourceManager::getInstance()->recording_done_text_left_padding()
#define RECORDING_DONE_TEXT_RIGHT_PADDING		ResourceManager::getInstance()->recording_done_text_right_padding()
#define RECORDING_DONE_BUTTON		ResourceManager::getInstance()->recording_done_button()
#define RECORDING_RERECORD_TEXT_FONT_SIZE		ResourceManager::getInstance()->recording_rerecord_text_font_size()
#define RECORDING_RERECORD_TEXT_LEFT_PADDING		ResourceManager::getInstance()->recording_rerecord_text_left_padding()
#define RECORDING_RERECORD_TEXT_RIGHT_PADDING		ResourceManager::getInstance()->recording_rerecord_text_right_padding()
#define RECORDING_RERECORD_ICON		ResourceManager::getInstance()->recording_rerecord_icon()
#define RECORDING_RERECORD_BUTTON_PADDING		ResourceManager::getInstance()->recording_rerecord_button_padding()
#define RECORDING_SAVE_TEXT_FONT_SIZE		ResourceManager::getInstance()->recording_save_text_font_size()
#define RECORDING_SAVE_TEXT_LEFT_PADDING		ResourceManager::getInstance()->recording_save_text_left_padding()
#define RECORDING_SAVE_TEXT_RIGHT_PADDING		ResourceManager::getInstance()->recording_save_text_right_padding()
#define RECORDING_SAVE_ICON		ResourceManager::getInstance()->recording_save_icon()
#define RECORDING_SAVE_BUTTON		ResourceManager::getInstance()->recording_save_button()
#define RECORDING_PROGRESS_BAR		ResourceManager::getInstance()->recording_progress_bar()
#define RECORDING_RECORDING_TEXT_FONT_SIZE		ResourceManager::getInstance()->recording_recording_text_font_size()
#define RECORDING_RECORDING_TEXT_SIDE_PADDING		ResourceManager::getInstance()->recording_recording_text_side_padding()
#define RECORDING_RECORDING_ICON		ResourceManager::getInstance()->recording_recording_icon()
#define RECORDING_REMOVE_AREA		ResourceManager::getInstance()->recording_remove_area()
#define RECORDING_PROGRESS_BAR_SIZE		ResourceManager::getInstance()->recording_progress_bar_size()
#define RECORDING_PROGRESS_MARK_BOTTOM_MARGIN		ResourceManager::getInstance()->recording_progress_mark_bottom_margin()
#define RECORDING_CLEAR_TALK_BOX_SIZE		ResourceManager::getInstance()->recording_clear_talk_box_size()
#define RECORDING_CLEAR_TALK_BOX		ResourceManager::getInstance()->recording_clear_talk_box()
#define RECORDING_CLEAR_TALK_BOX_INSET		ResourceManager::getInstance()->recording_clear_talk_box_inset()
#define RECORDING_CLEAR_TALK_BOX_OK_BUTTON_SIZE		ResourceManager::getInstance()->recording_clear_talk_box_ok_button_size()
#define RECORDING_CLEAR_TALK_BOX_OK_BUTTON_INSET		ResourceManager::getInstance()->recording_clear_talk_box_ok_button_inset()
#define RECORDING_UNLOCK_TALK_BOX_SIZE		ResourceManager::getInstance()->recording_unlock_talk_box_size()
#define RECORDING_UNLOCK_TALK_BOX_INSET		ResourceManager::getInstance()->recording_unlock_talk_box_inset()
#define RECORDING_UNLOCK_TALK_BOX_INSET_SIZE		ResourceManager::getInstance()->recording_unlock_talk_box_inset_size()
#define RECORDING_UNLOCK_TALK_BOX_UNLOCK_BUTTON_SIZE		ResourceManager::getInstance()->recording_unlock_talk_box_unlock_button_size()
#define RECORDING_TALK_BOX_UNLOCK_BUTTON		ResourceManager::getInstance()->recording_talk_box_unlock_button()
#define RECORDING_TALK_BOX_TEXT_FONT_SIZE		ResourceManager::getInstance()->recording_talk_box_text_font_size()
#define RECORDING_TALK_BOX_BACKGROUND_MAX_WIDTH_LONG		ResourceManager::getInstance()->recording_talk_box_background_max_width_long()
#define RECORDING_TALK_BOX_BACKGROUND_MAX_WIDTH_SHORT		ResourceManager::getInstance()->recording_talk_box_background_max_width_short()
#define RECORDING_TALK_BOX_CHARACTER_MAX_WIDTH_LONG		ResourceManager::getInstance()->recording_talk_box_character_max_width_long()
#define RECORDING_TALK_BOX_CHARACTER_MAX_WIDTH_SHORT		ResourceManager::getInstance()->recording_talk_box_character_max_width_short()
#define RECORDING_TALK_BOX_TEXT_LEFT_MARGIN		ResourceManager::getInstance()->recording_talk_box_text_left_margin()
#define RECORDING_TALK_BOX_TEXT_BOTTOM_MARGIN		ResourceManager::getInstance()->recording_talk_box_text_bottom_margin()
#define RECORDING_TALK_BOX_TEXT_TOP_MARGIN		ResourceManager::getInstance()->recording_talk_box_text_top_margin()
#define RECORDING_TALK_BOX_UNLOCK_BUTTON_TEXT_MAX_WIDTH		ResourceManager::getInstance()->recording_talk_box_unlock_button_text_max_width()
#define RECORDING_TALK_BOX_UNLOCK_BUTTON_TEXT_SIDE_MARGIN		ResourceManager::getInstance()->recording_talk_box_unlock_button_text_side_margin()
#define RECORDING_TALK_BOX_UNLOCK_BUTTON_RIGHT_MARGIN		ResourceManager::getInstance()->recording_talk_box_unlock_button_right_margin()
#define RECORDING_TALK_BOX_UNLOCK_BUTTON_LEFT_MARGIN		ResourceManager::getInstance()->recording_talk_box_unlock_button_left_margin()
#define RECORDING_TALK_BOX_UNLOCK_BUTTON_BOTTOM_MARGIN		ResourceManager::getInstance()->recording_talk_box_unlock_button_bottom_margin()
#define RECORDING_TALK_BOX_UNLOCK_BUTTON_TOP_MARGIN		ResourceManager::getInstance()->recording_talk_box_unlock_button_top_margin()
#define RECORDING_TALK_BOX_TEXT_LABEL_SIZE		ResourceManager::getInstance()->recording_talk_box_text_label_size()
#define RECORDING_CLEAR_TALK_BOX_TOP_LABEL		ResourceManager::getInstance()->recording_clear_talk_box_top_label()
#define RECORDING_CLEAR_TALK_BOX_SECOND_LABEL		ResourceManager::getInstance()->recording_clear_talk_box_second_label()
#define RECORDING_UNLOCK_TALK_BOX_TOP_LABEL		ResourceManager::getInstance()->recording_unlock_talk_box_top_label()
#define RECORDING_UNLOCK_TALK_BOX_SECOND_LABEL		ResourceManager::getInstance()->recording_unlock_talk_box_second_label()
#define RECORDING_TALK_BOX_OK_BUTTON		ResourceManager::getInstance()->recording_talk_box_ok_button()
#define RECORDING_TALK_BOX_UNLOCK_BUTTON_FONT_SIZE		ResourceManager::getInstance()->recording_talk_box_unlock_button_font_size()
#define RECORDING_SOUND_SCROLL_LABEL_FONT_SIZE		ResourceManager::getInstance()->recording_sound_scroll_label_font_size()
#define RECORDING_SOUND_SCROLL_LABEL_SIZE		ResourceManager::getInstance()->recording_sound_scroll_label_size()
#define RECORDING_SOUND_SCROLL_LABEL_SHODOW		ResourceManager::getInstance()->recording_sound_scroll_label_shodow()
#define RECORDING_SOUND_SCROLL_LABEL		ResourceManager::getInstance()->recording_sound_scroll_label()
#define SHARE_CANCEL_TEXT_FONT_SIZE		ResourceManager::getInstance()->share_cancel_text_font_size()
#define SHARE_CANCEL_ICON		ResourceManager::getInstance()->share_cancel_icon()
#define SHARE_CANCEL_TEXT_LEFT_PADDING		ResourceManager::getInstance()->share_cancel_text_left_padding()
#define SHARE_CANCEL_TEXT_RIGHT_PADDING		ResourceManager::getInstance()->share_cancel_text_right_padding()
#define SHARE_CANCEL_BUTTON		ResourceManager::getInstance()->share_cancel_button()
#define MAP_PLAY_TITLE_SIZE		ResourceManager::getInstance()->map_play_title_size()
#define MAP_PLAY_TITLE_BOTTOM_MARGIN		ResourceManager::getInstance()->map_play_title_bottom_margin()
#define MAP_PLAY_TITLE_TOP_PADDING		ResourceManager::getInstance()->map_play_title_top_padding()
#define MAP_PLAY_TITLE_TEXT_SIZE		ResourceManager::getInstance()->map_play_title_text_size()
#define MAP_PLAY_TITLE_TEXT_STORKE_WIDTH		ResourceManager::getInstance()->map_play_title_text_storke_width()
#define TOAST_BG_SIZE		ResourceManager::getInstance()->toast_bg_size()
#define TOAST_BG_CORNER_SIZE		ResourceManager::getInstance()->toast_bg_corner_size()
#define TOAST_BG_CORNER_INSET		ResourceManager::getInstance()->toast_bg_corner_inset()
#define TOAST_CONTENT_SIZE_TYPE_01		ResourceManager::getInstance()->toast_content_size_type_01()
#define TOAST_CONTENT_SIZE_TYPE_02		ResourceManager::getInstance()->toast_content_size_type_02()
#define TOAST_TYPE_02_EXTRA_WIDTH_JA		ResourceManager::getInstance()->toast_type_02_extra_width_ja()
#define TOAST_MESSAGE_FONTSIZE		ResourceManager::getInstance()->toast_message_fontsize()
#define TOAST_MESSAGE_JA_FONTSIZE		ResourceManager::getInstance()->toast_message_ja_fontsize()
#define TOAST_LABEL_SIZE_TYPE_01		ResourceManager::getInstance()->toast_label_size_type_01()
#define TOAST_LABEL_SIZE_TYPE_02		ResourceManager::getInstance()->toast_label_size_type_02()
#define TOAST_BG_PADDING_TOP_BOTTOM		ResourceManager::getInstance()->toast_bg_padding_top_bottom()
#define TOAST_BG_PADDING_LEFT		ResourceManager::getInstance()->toast_bg_padding_left()
#define TOAST_BG_Y		ResourceManager::getInstance()->toast_bg_y()
#define LABEL_SHADOW_DEFAULT_SIZE		ResourceManager::getInstance()->label_shadow_default_size()
#define LABEL_SHADOW_AUTHOR_SIZE		ResourceManager::getInstance()->label_shadow_author_size()
#define LABEL_SHADOW_PLAY_SIZE		ResourceManager::getInstance()->label_shadow_play_size()
#define LABEL_SHADOW_CAMERA_SIZE		ResourceManager::getInstance()->label_shadow_camera_size()
#define CUSTOM_CHARACTER_C001_PHOTO		ResourceManager::getInstance()->custom_character_c001_photo()
#define CUSTOM_CHARACTER_C002_PHOTO		ResourceManager::getInstance()->custom_character_c002_photo()
#define CUSTOM_CHARACTER_C003_PHOTO		ResourceManager::getInstance()->custom_character_c003_photo()
#define CUSTOM_CHARACTER_C004_PHOTO		ResourceManager::getInstance()->custom_character_c004_photo()
#define CUSTOM_CHARACTER_C005_PHOTO		ResourceManager::getInstance()->custom_character_c005_photo()
#define CUSTOM_CHARACTER_C006_PHOTO		ResourceManager::getInstance()->custom_character_c006_photo()
#define CUSTOM_CHARACTER_C007_PHOTO		ResourceManager::getInstance()->custom_character_c007_photo()
#define CUSTOM_CHARACTER_C008_PHOTO		ResourceManager::getInstance()->custom_character_c008_photo()
#define CUSTOM_CHARACTER_C009_PHOTO		ResourceManager::getInstance()->custom_character_c009_photo()
#define CUSTOM_CHARACTER_C010_PHOTO		ResourceManager::getInstance()->custom_character_c010_photo()
#define CUSTOM_CHARACTER_C011_PHOTO		ResourceManager::getInstance()->custom_character_c011_photo()
#define CUSTOM_CHARACTER_C012_PHOTO		ResourceManager::getInstance()->custom_character_c012_photo()
#define CUSTOM_CHARACTER_C013_PHOTO		ResourceManager::getInstance()->custom_character_c013_photo()
#define CUSTOM_CHARACTER_C014_PHOTO		ResourceManager::getInstance()->custom_character_c014_photo()
#define CUSTOM_CHARACTER_C001_SIDE_PHOTO		ResourceManager::getInstance()->custom_character_c001_side_photo()
#define CUSTOM_CHARACTER_C002_SIDE_PHOTO		ResourceManager::getInstance()->custom_character_c002_side_photo()
#define CUSTOM_CHARACTER_C003_SIDE_PHOTO		ResourceManager::getInstance()->custom_character_c003_side_photo()
#define CUSTOM_CHARACTER_C004_SIDE_PHOTO		ResourceManager::getInstance()->custom_character_c004_side_photo()
#define CUSTOM_CHARACTER_C005_SIDE_PHOTO		ResourceManager::getInstance()->custom_character_c005_side_photo()
#define CUSTOM_CHARACTER_C006_SIDE_PHOTO		ResourceManager::getInstance()->custom_character_c006_side_photo()
#define CUSTOM_CHARACTER_C007_SIDE_PHOTO		ResourceManager::getInstance()->custom_character_c007_side_photo()
#define CUSTOM_CHARACTER_C008_SIDE_PHOTO		ResourceManager::getInstance()->custom_character_c008_side_photo()
#define CUSTOM_CHARACTER_C009_SIDE_PHOTO		ResourceManager::getInstance()->custom_character_c009_side_photo()
#define CUSTOM_CHARACTER_C010_SIDE_PHOTO		ResourceManager::getInstance()->custom_character_c010_side_photo()
#define CUSTOM_CHARACTER_C011_SIDE_PHOTO		ResourceManager::getInstance()->custom_character_c011_side_photo()
#define CUSTOM_CHARACTER_C012_SIDE_PHOTO		ResourceManager::getInstance()->custom_character_c012_side_photo()
#define CUSTOM_CHARACTER_C013_SIDE_PHOTO		ResourceManager::getInstance()->custom_character_c013_side_photo()
#define CUSTOM_CHARACTER_C014_SIDE_PHOTO		ResourceManager::getInstance()->custom_character_c014_side_photo()
#define CUSTOM_CHARACTER_C001_SIDE_PHOTO_SIZE		ResourceManager::getInstance()->custom_character_c001_side_photo_size()
#define CUSTOM_CHARACTER_C002_SIDE_PHOTO_SIZE		ResourceManager::getInstance()->custom_character_c002_side_photo_size()
#define CUSTOM_CHARACTER_C003_SIDE_PHOTO_SIZE		ResourceManager::getInstance()->custom_character_c003_side_photo_size()
#define CUSTOM_CHARACTER_C004_SIDE_PHOTO_SIZE		ResourceManager::getInstance()->custom_character_c004_side_photo_size()
#define CUSTOM_CHARACTER_C005_SIDE_PHOTO_SIZE		ResourceManager::getInstance()->custom_character_c005_side_photo_size()
#define CUSTOM_CHARACTER_C006_SIDE_PHOTO_SIZE		ResourceManager::getInstance()->custom_character_c006_side_photo_size()
#define CUSTOM_CHARACTER_C007_SIDE_PHOTO_SIZE		ResourceManager::getInstance()->custom_character_c007_side_photo_size()
#define CUSTOM_CHARACTER_C008_SIDE_PHOTO_SIZE		ResourceManager::getInstance()->custom_character_c008_side_photo_size()
#define CUSTOM_CHARACTER_C009_SIDE_PHOTO_SIZE		ResourceManager::getInstance()->custom_character_c009_side_photo_size()
#define CUSTOM_CHARACTER_C010_SIDE_PHOTO_SIZE		ResourceManager::getInstance()->custom_character_c010_side_photo_size()
#define CUSTOM_CHARACTER_C011_SIDE_PHOTO_SIZE		ResourceManager::getInstance()->custom_character_c011_side_photo_size()
#define CUSTOM_CHARACTER_C012_SIDE_PHOTO_SIZE		ResourceManager::getInstance()->custom_character_c012_side_photo_size()
#define CUSTOM_CHARACTER_C013_SIDE_PHOTO_SIZE		ResourceManager::getInstance()->custom_character_c013_side_photo_size()
#define CUSTOM_CHARACTER_C014_SIDE_PHOTO_SIZE		ResourceManager::getInstance()->custom_character_c014_side_photo_size()
#define OPENING_SINGLE_TITLE_LABEL_FONT_SIZE		ResourceManager::getInstance()->opening_single_title_label_font_size()
#define OPENING_DOUBLE_TITLE_LABEL_FONT_SIZE		ResourceManager::getInstance()->opening_double_title_label_font_size()
#define OPENING_AUTHOR_LABEL_FONT_SIZE		ResourceManager::getInstance()->opening_author_label_font_size()
#define OPENING_SINGLE_TITLE_LABEL_SIZE		ResourceManager::getInstance()->opening_single_title_label_size()
#define OPENING_DOUBLE_TITLE_LABEL_SIZE		ResourceManager::getInstance()->opening_double_title_label_size()
#define OPENING_SINGLE_TITLE_LABEL		ResourceManager::getInstance()->opening_single_title_label()
#define OPENING_DOUBLE_TITLE_LABEL_01		ResourceManager::getInstance()->opening_double_title_label_01()
#define OPENING_DOUBLE_TITLE_LABEL_02		ResourceManager::getInstance()->opening_double_title_label_02()
#define OPENING_TITLE_LABEL_SHADOW_DISTANCE		ResourceManager::getInstance()->opening_title_label_shadow_distance()
#define OPENING_AUTHOR_LABEL_SIDE_MARGIN		ResourceManager::getInstance()->opening_author_label_side_margin()
#define OPENING_AUTHOR_LABEL_SHADOW_DISTANCE		ResourceManager::getInstance()->opening_author_label_shadow_distance()
#define OPENING_AUTHOR_LABEL_SIZE		ResourceManager::getInstance()->opening_author_label_size()
#define OPENING_AUTHOR_LABEL_TOP_PADDING		ResourceManager::getInstance()->opening_author_label_top_padding()
#define OPENING_AUTHOR_LABEL_STAR_BOTTOM_PADDING		ResourceManager::getInstance()->opening_author_label_star_bottom_padding()
#define OPENING_AUTHOR_LABEL_STAR_SIDE_PADDING		ResourceManager::getInstance()->opening_author_label_star_side_padding()
#define OPENING_TITLE_TEXT_MAX_WIDTH		ResourceManager::getInstance()->opening_title_text_max_width()
#define OPENING_AUTHOR_TEXT_MAX_WIDTH		ResourceManager::getInstance()->opening_author_text_max_width()
#define OPENING_TITLE_CENTER_MIN_WIDTH		ResourceManager::getInstance()->opening_title_center_min_width()
#define OPENING_TITLE_CENTER_MAX_WIDTH		ResourceManager::getInstance()->opening_title_center_max_width()
#define OPENING_TITLE_NODE_TOP_PADDING		ResourceManager::getInstance()->opening_title_node_top_padding()
#define GUIDE_LABEL_NANUM_FONTSIZE		ResourceManager::getInstance()->guide_label_nanum_fontSize()
#define GUIDE_LABEL_NORMAL_FONTSIZE		ResourceManager::getInstance()->guide_label_normal_fontSize()
#define GUIDE_LABEL_BIG_FONTSIZE		ResourceManager::getInstance()->guide_label_big_fontsize()
#define GUIDE_LABEL_JP_BIG_FONTSIZE		ResourceManager::getInstance()->guide_label_jp_big_fontsize()
#define GUIDE_LABEL_SMALL_FONTSIZE		ResourceManager::getInstance()->guide_label_small_fontsize()
#define GUIDE_LABEL_JP_SMALL_FONTSIZE		ResourceManager::getInstance()->guide_label_jp_small_fontsize()
#define PROGRESS_LABEL_FONTSIZE		ResourceManager::getInstance()->progress_label_fontsize()
#define PROGRESS_LABEL_SIZE		ResourceManager::getInstance()->progress_label_size()
#define PROGRESS_LABEL_POSITION		ResourceManager::getInstance()->progress_label_position()
// POS_H_DEFINE_END      **do not delete this line**

#endif /* defined(__bobby__ResourceManager__) */
