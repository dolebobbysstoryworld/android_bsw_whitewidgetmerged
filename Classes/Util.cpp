//
//  Util.cpp
//  bobby
//
//  Created by oasis on 2014. 6. 20..
//
//

#include "Util.h"
#include "utf8.h"

#include "Define.h"
#include "ResourceManager.h"

#include "external/json/document.h"
#include "external/json/prettywriter.h"
#include "external/json/stringbuffer.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#import <UIKit/UIKit.h>
#endif

void Util::splitTitleString(const std::string &orgString, char delim, std::string &firstLine, std::string &secondLine, int width, const char* fontName, int fontSize) {
    std::vector<std::string> elems;
    std::stringstream ss(orgString);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
        //        log("item : %s", item.c_str());
    }
    
    std::string appendCheck;
    if (elems.size() > 1) {
        int stringWidth = Device::getTextWidth((elems.at(0)).c_str(), fontName, fontSize);
        if (stringWidth >= width-10) {
            Util::splitStringByletter(orgString, firstLine, secondLine, width, fontName, fontSize);
            return;
        }
        
        for (int i=0;i <elems.size(); i++) {
            auto currentWord = elems.at(i);
            appendCheck.append(currentWord);
            appendCheck.append(" ");
            int stringWidth = Device::getTextWidth(appendCheck.c_str(), fontName, fontSize);
            if (stringWidth < width) {
                firstLine.append(elems.at(i));
                firstLine.append(" ");
            } else {
                secondLine.append(elems.at(i));
                if (i <elems.size()) {
                    secondLine.append(" ");
                }
            }
        }
        
    } else {
        Util::splitStringByletter(orgString, firstLine, secondLine, width, fontName, fontSize);
    }
}

void Util::splitStringByletter(const std::string &orgString, std::string &firstLine, std::string &secondLine, int width, const char* fontName, int fontSize)
{
    std::vector<std::string> results;
    std::string strTmp = orgString;
//    fix_utf8_string(strTmp);
    //        string::iterator end_it = utf8::find_invalid(strTmp.begin(), strTmp.end());
    //        int length = utf8::distance(strTmp.begin(), end_it);
    
    char* str = (char*)strTmp.c_str();
    char* str_i = str;
    char* end = str+strlen(str)+1;
    
    unsigned char symbol[5] = {0,0,0,0,0};
    do {
        memset(symbol, 0, 5);
        uint32_t code = utf8::next(str_i,end);
        if (code == 0) {
            continue;
        }
        utf8::append(code, symbol);
        std::stringstream s;
        s << symbol;
        results.push_back((char*)symbol);
//        log("symbolstr : %s",(char*)symbol);
        
        } while(str_i < end);
        
        std::string appendstr;
//        fix_utf8_string(appendstr);
        for (int i=0;i <results.size(); i++) {
            auto currentWord = results.at(i);
            appendstr.append(results.at(i));
//            log("appendCheck : %s", appendstr.c_str() );
            int stringWidth = Device::getTextWidth(appendstr.c_str(), fontName, fontSize);
            
            if (stringWidth < width) {
                firstLine.append(results.at(i));
            } else {
                secondLine.append(results.at(i));
            }
        }
}

float Util::getCharacterDefulatScale()
{
    float scale = 0.25;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    ResolutionType type = ResourceManager::getInstance()->getCurrentType();
    switch(type) {
		case ResolutionType::androidfullhd:
		    scale = 0.25f;
			break;

		case ResolutionType::androidhd:
		default:
		    scale = 0.25f * 2.f / 3.f;
			break;
    }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        scale = 0.35;
    } else {
        scale = 0.15;
    }
#endif
    return scale * Util::getCharacterScaleFactor();
}
float Util::getCharacterMaxScale()
{
    float scale = 1.0f;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    ResolutionType type = ResourceManager::getInstance()->getCurrentType();
    switch(type) {
		case ResolutionType::androidfullhd:
		    scale = 1.0f;
			break;

		case ResolutionType::androidhd:
		default:
		    scale = 1.0f * 2.f / 3.f;
			break;
    }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {
        scale = 0.6;
    }
#endif
    return scale * Util::getCharacterScaleFactor();
}
float Util::getCharacterMinScale()
{
    float scale = 0.15;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    ResolutionType type = ResourceManager::getInstance()->getCurrentType();
    switch(type) {
		case ResolutionType::androidfullhd:
		    scale = 0.15f;
			break;

		case ResolutionType::androidhd:
		default:
		    scale = 0.15f * 2.f / 3.f;
			break;
    }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        scale = 0.15;
    } else {
        scale = 0.09;
    }
#endif
    return scale * Util::getCharacterScaleFactor();
}

float Util::getCharacterScaleFactor()
{
    float scale = 1.0f;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    ResolutionType type = ResourceManager::getInstance()->getCurrentType();
    switch(type) {
		case ResolutionType::androidfullhd:
		    scale = 2.0f;
			break;

		case ResolutionType::androidhd:
		default:
		    scale = 2.0f;// * 2.f / 3.f;
			break;
    }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
            ([UIScreen mainScreen].scale == 2.0)) {
            scale = 2.0; // retina pad
        }
    } else {
        scale = 2.0; // retina phone
    }
#endif
    return scale;
}
float Util::getCharacterExtendZoomScale()
{
    float scale = 0.5f;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    ResolutionType type = ResourceManager::getInstance()->getCurrentType();
    switch(type) {
		case ResolutionType::androidfullhd:
		    scale = 0.5f;
			break;

		case ResolutionType::androidhd:
		default:
		    scale = 0.5f * 2.f / 3.f;
			break;
    }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        scale = 0.7;
    } else {
        scale = 0.3;
    }
#endif
    return scale;
}

std::string Util::getMainPhotoPath(std::string &photoType)
{
    std::string result;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    NSString *photoDirStr = [NSString stringWithUTF8String:photoType.c_str()];
    NSString *mainPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/",
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0],
                               photoDirStr,
                               @"main"];
    std::string path = std::string([mainPhotoPath UTF8String]);
    result = path;
#endif
    return result;
}

std::string Util::getThumbnailPhotoPath(std::string &photoType)
{
    std::string result;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    NSString *photoDirStr = [NSString stringWithUTF8String:photoType.c_str()];
    NSString *mainPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/",
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0],
                               photoDirStr,
                               @"thumb"];
    std::string path = std::string([mainPhotoPath UTF8String]);
    result = path;
#endif
    return result;
}

std::string Util::getBookThumbnailPhotoPath(std::string &photoType)
{
    std::string result;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    NSString *photoDirStr = [NSString stringWithUTF8String:photoType.c_str()];
    NSString *mainPhotoPath = [NSString stringWithFormat:@"%@/%@/%@/",
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0],
                               photoDirStr,
                               @"bookthumb"];
    std::string path = std::string([mainPhotoPath UTF8String]);
    result = path;
#endif
    return result;
}

std::string Util::getBookCoverPhotoPath(std::string &photoType)
{
    std::string result;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    result = std::string("_cover");
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#endif
    return result;
}

long Util::getItemLeftTime(const char* registerTime, int expiredHour)
{
    struct tm tm;
    strptime(registerTime, "%Y-%m-%d %H:%M", &tm);
    
    tm.tm_sec = 0;
    tm.tm_wday = 0;
    tm.tm_yday = 0;
    tm.tm_isdst = -1;
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    time_t expireT = Util::timegm(&tm);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
    time_t expireT = timegm(&tm);
#endif
    expireT = expireT - (60*60*9) + (expiredHour*60);
    
    struct tm *local = localtime(&expireT);
    log("local mon - %i day - %i, hour -%i ", local->tm_mon, local->tm_mday, local->tm_hour);
    
    time_t currentT;
    time(&currentT);
    struct tm *currentTm = localtime(&currentT);
    log("day : %i hour - %i", currentTm->tm_mday, currentTm->tm_hour);
    
    log("expireT - currentT :%li", expireT - currentT);
    
    return expireT - currentT;
}

void Util::removeParseErrorObject(const char *orderKeyType, __String *objectKey)
{
    auto ud = UserDefault::getInstance();
    
    auto orderJsonString = ud->getStringForKey("order_key", "");
    rapidjson::Document orderDocument;
    orderDocument.Parse<0>(orderJsonString.c_str());
    rapidjson::Value& itemOrder = orderDocument[orderKeyType];
    rapidjson::Value newOrder(rapidjson::kArrayType);
    rapidjson::Document::AllocatorType& allocator = orderDocument.GetAllocator();
    
    for (rapidjson::SizeType i = 0; i < itemOrder.Size(); i++) {
        auto currentObjectKey = itemOrder[i].GetString();
        if (objectKey->compare(currentObjectKey) == 0) {
            log("delete item");
            continue; // pass
        }
        log("item %s  del: %s", currentObjectKey, objectKey->getCString());
        newOrder.PushBack(currentObjectKey, allocator);
    }
    
    orderDocument[orderKeyType] = newOrder;
    rapidjson::StringBuffer strbuf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
    orderDocument.Accept(writer);
    auto resultOrderJsonString = strbuf.GetString();
    ud->setStringForKey(ORDER_KEY, resultOrderJsonString);
    ud->setStringForKey(objectKey->getCString(), "");
    ud->flush();
}

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
std::string Util::saveCustomCharacterData(int index, const char* path)
{
	unsigned long int timestamp = time(NULL);
	auto baseCharName = __String::createWithFormat("C%03d_custom", index);
	auto customCharacterKey = __String::createWithFormat("user_char_%ld", timestamp);
	auto customCharacterString = __String::createWithFormat("{\"base_char\" : \"%s\", \"main_photo_file\" : \"%s\", \"thumbnail_photo_file\" : \"%s\" }",
			baseCharName->getCString(), path, __String::createWithFormat("%s%s", path, "_thumb")->getCString());

	auto ud = UserDefault::getInstance();
	auto orderJsonString = ud->getStringForKey(ORDER_KEY, "");
	rapidjson::Document orderDocument;
	rapidjson::Document::AllocatorType& allocator = orderDocument.GetAllocator();
	orderDocument.Parse<0>(orderJsonString.c_str());
	rapidjson::Value& bookOrder = orderDocument["character_order"];

	// add to user order
	rapidjson::Value charKey;
	charKey.SetString(customCharacterKey->getCString());
	bookOrder.PushBack(charKey, allocator);

	rapidjson::StringBuffer strbuf;
	rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
	orderDocument.Accept(writer);
	auto resultOrderJsonString = strbuf.GetString();

	// save to user default
	ud->setStringForKey(ORDER_KEY, resultOrderJsonString);
	ud->setStringForKey(customCharacterKey->getCString(), customCharacterString->getCString());
	ud->flush();

	return customCharacterKey->getCString();
}

std::string Util::saveCustomBackgroundData(const char* path)
{
	unsigned long int timestamp= time(NULL);
	auto customBgKey = __String::createWithFormat("user_bg_%ld", timestamp);
	auto customBgString = __String::createWithFormat("{\"custom\" : true, \"main_photo_file\" : \"%s\", \"thumbnail_photo_file\" : \"%s\" }",
			path, __String::createWithFormat("%s%s", path, "_thumb")->getCString());

	auto ud = UserDefault::getInstance();
	auto orderJsonString = ud->getStringForKey(ORDER_KEY, "");
	rapidjson::Document orderDocument;
	rapidjson::Document::AllocatorType& allocator = orderDocument.GetAllocator();
	orderDocument.Parse<0>(orderJsonString.c_str());
	rapidjson::Value& bookOrder = orderDocument["background_order"];

	// add to user order
	rapidjson::Value bgKey;
	bgKey.SetString(customBgKey->getCString());
	bookOrder.PushBack(bgKey, allocator);

	rapidjson::StringBuffer strbuf;
	rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
	orderDocument.Accept(writer);
	auto resultOrderJsonString = strbuf.GetString();

	// save to user default
	ud->setStringForKey(ORDER_KEY, resultOrderJsonString);
	ud->setStringForKey(customBgKey->getCString(), customBgString->getCString());
	ud->flush();
	
	return customBgKey->getCString();
}

time_t Util::timegm(struct tm* tm)
{
	time_t t = mktime(tm);
	return t + localtime(&t)->tm_gmtoff;
}
#endif