#include "cocos2d.h"

//
//  VideoScene.h
//  bobby
//
//  Created by oasis on 2014. 4. 16..
//
//

#ifndef __bobby__VideoScene__
#define __bobby__VideoScene__

#include "extensions/cocos-ext.h"
#include "OpeningScene.h"
#include "EndingScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

typedef enum play_type{
    PLAY_TYPE_ONE = 0,
    PLAY_TYPE_ALL = 1,
} play_type;

typedef enum book_type{
    BOOK_TYPE_BOBBY = 0,
    BOOK_TYPE_SPACE = 1,
} book_type;   

class JeVideo : public cocos2d::LayerColor, OpeningDelegate, EndingDelegate

{
    
public:
    static cocos2d::Scene* createScene(__String *bookTitle, __String *authorTitle, __Array *movieNames, play_type playType);
    static JeVideo* create(__String *bookTitle, __String *authorTitle, __Array *movieNames, play_type playType);
    virtual bool init(__String *bookTitle, __String *authorTitle, __Array *movieNames, play_type type);
    static cocos2d::Scene* createScene();
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual ~JeVideo(){}
    virtual bool init();
    
    virtual void onOpeningFinished(Opening* opening);
    virtual void onEndingFinished(Ending* ending);
    
    
    bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event *event);
    void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event *event);
    void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event *event);
    void onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event *event);
    void showUI(bool show);

    void btnBackCallback();
    
    void createProgressBar();
    Label *createTitle(int chapterIndex);
    void setProgress(float value);
    
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    void movieDidFinishCallback();
    void playEnding();
    void changeTextureForEnding();
    
    Label *createTitleLabel();

    void playMenuCallback(Ref* pSender);
    
    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);

    virtual void onEnter();
    virtual void onExit();
    virtual void onEnterTransitionDidFinish();
    
    virtual void update(float delta);
    
    float totalDuration;
    
    CREATE_FUNC(JeVideo);
private:
    void playAVPlayer();
    void closeScene();
    
    __Array *avMovieItems;
    play_type moviePlayType;
    
    Opening *opening;
    Ending *ending;
    
    Label *titleLabel;
    Label *titleLabel2;
    
    book_type bookType;
    
    cocos2d::MenuItemImage *playMenuItem;
    cocos2d::MenuItemImage *pauseMenuItem;
    cocos2d::MenuItemImage *btnBack;
    
    cocos2d::Menu *controlMenu;
    
    cocos2d::Node *playProgressBarBg;
    cocos2d::Node *playProgressBar;
    cocos2d::Node *playProgressBarHolder;
    
    void triggerHideUI();
    void cancelHideUI();
    __String *getTitle(int chapterIndex);

    cocos2d::Action *hideUIAction;

    bool paused;
    bool playing;
    
    bool visibleUI;
    float playedTime;
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    bool isPlatformShown;
#endif
};

#endif /* defined(__bobby__VideoScene__) */
