//
//  ChapterPlayer.h
//  bobby
//
//  Created by Dongwook, Kim on 14. 6. 27..
//
//

#ifndef __bobby__ChapterPlayer__
#define __bobby__ChapterPlayer__

#include <iostream>
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "Character.h"

USING_NS_CC;
USING_NS_CC_EXT;

class ChapterPlayerDelegate
{
public:
    virtual ~ChapterPlayerDelegate() { }
    virtual void onTransitionFinished(int playWillStartChapterIndex) = 0;
    virtual void chapterPlayFinished(int chapterIndex) = 0;
};

class ChapterPlayer : public cocos2d::Layer
{
public:
    virtual bool init();
    virtual void update(float delta);
    
    void restoreCharacterProperty();
    void btnBackCallback(Ref* pSender);
    void playMenuCallback(Ref* pSender);
    void menuCallback(Ref* sender);
    void stopBGAnimation();

    bool loadJsonData(const char* bookKey, int chapterIndex);
    void prepareTransition();
    void startTransition();
    void transitionFinished();

    void createProgressBar();
    Label *createTitle(int chapterIndex);
    
    bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event *event);
    void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event *event);
    void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event *event);
    void onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event *event);
    
    void play();
    void pausePlay();
    void resumePlay();
    void finishPlay();
    
    void clear();

    bool isPlaying() { return playing; };
    bool isPaused() { return paused; };
    float getDuration() { return duration; }; // second
    float getPlayProgress(); // 0.0 ~ 1.0
    
    void resetTransitionState();
    
    __String* getVoiceFileName() { return voiceFileName; };
    __String* getBgmFileName() { return bgmFileName; };
    
    void setDelegate(ChapterPlayerDelegate *delegate) { this->delegate = delegate; };
    
    CREATE_FUNC(ChapterPlayer);
    
private:
    int chapterIndex;
    char* bookKey;
    
    ChapterPlayerDelegate *delegate;
    
    RenderTexture* transitionTexture;
    Sprite* transitionSprite;

    __Array* characters;
    __Array* invisibleCharacters;
    
    __String* voiceFileName;
    __String* bgmFileName;

    __Dictionary* characterProperties;
    
    cocos2d::MenuItemImage *playMenuItem;
    cocos2d::MenuItemImage *pauseMenuItem;
    
    cocos2d::Label *pageTitle;
    cocos2d::Node *playProgressBarBg;
    cocos2d::Node *playProgressBar;

    cocos2d::Layer *currentLayer;
    cocos2d::Layer *prevLayer;

    cocos2d::__Dictionary *touchTargetMapping;
    cocos2d::__Dictionary *playingTouches;
    cocos2d::__Array *touchData;
    
    bool playing;
    bool paused;
    bool started;
    
    bool onTransition;
    
    float playedTime;
    float duration;

    void pauseTransition();
    void resumeTransition();
    void runTransition();
    
    int playTouchTimeStampIndex;
    double playTouchElapsedTime;
    double accumulatedTouchTimeStamp;
        
    void bringToFront(Sprite* pSender);
    void setProgress(float value);
    void playTouch();
    void onPlayTouch(float df);
};

#endif /* defined(__bobby__ChapterPlayer__) */
