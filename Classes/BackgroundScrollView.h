//
//  BackgroundScrollView.h
//  bobby
//
//  Created by Dongwook, Kim on 14. 7. 11..
//
//

#ifndef __bobby__BackgroundScrollView__
#define __bobby__BackgroundScrollView__

#include <iostream>

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ScrollViewExtension.h"
#include "ui/CocosGUI.h"
#include "PopupLayer.h"

USING_NS_CC;
USING_NS_CC_EXT;

#define TAG_SCROLLVIEW 0

typedef enum bg_super_type{
    BG_HOME = 0,
    BG_RECORD = 1,
} bg_super_type;

class BackgroundScrollView;

class BackgroundScrollViewDelegate
{
public:
    virtual ~BackgroundScrollViewDelegate() {}
    virtual void onSelectBackground(BackgroundScrollView* view, Node* selectedItem, __String* backgroundKey, int index) = 0;
};

class BackgroundScrollView : public cocos2d::Layer, ScrollViewExtensionDelegate, PopupLayerDelegate
{
public:
    static BackgroundScrollView* create(bg_super_type superViewType);
    bool init(bg_super_type superViewType);
    
    void setSelectBackground(int index);
    int getBackgroundIndex(std::string& charKey);
    
    bool addBackground(std::string bgKey);
    bool removeBackground(__String *bgKey);
    void rotateBackgrounds(float duration, float angle, bool animation);
    void reloadData();
    
    // scrollview extension delegate
    void scrollViewDidScroll(ScrollViewExtension* view);
    void scrollViewDidZoom(ScrollViewExtension* view);
    void scrollViewDidScrollEnd(ScrollViewExtension* view);
    void onSelectItem(ScrollViewExtension* view, Node* selectedItem, Touch *pTouch, Event *pEvent);
    
    BackgroundScrollViewDelegate* getDelegate() { return _scrollViewDelegate; }
    void setDelegate(BackgroundScrollViewDelegate* pDelegate) { _scrollViewDelegate = pDelegate; }
    
//    CREATE_FUNC(BackgroundScrollView);
    
    __Array *deleteButtons;
    void popupButtonClicked(PopupLayer *popup, cocos2d::Ref *obj);
//    LayerColor *deleteModeDim;
    void scrollToLastItem();
    void scrollToFirstItem();

private:
//    bool init();
    void bgCoinClicked(Ref* pSender);
    void bgPressedCallback(Ref* pSender);
    cocos2d::Node* createBackgroundForScrollView(std::string bgKey, int index);
    void adjustLine();
    void loadJsonToScrollView(ScrollView *scrollView);

    bool isScrollingToFirst;
    
    cocos2d::Point scrollOffset;
    BackgroundScrollViewDelegate *_scrollViewDelegate;
    ScrollViewExtension *createScrollView();
    ScrollViewExtension *scrollView;
    int linesCount;
    __Array* backgroundList;

    cocos2d::Sprite *frameSelectedBorder;
    
    float currentBackgroundAngle;
    bool scrollViewPressed;
    bool reachedToEnd;

    __Dictionary *items;
    __Array *pins;
    void deleteButtonClicked(Ref *obj, ui::TouchEventType type);
    void runRemoveBackgroundAnimation(__String* bookKey);
    
    __String *selectedBgKey;
    Node *selDeleteBg;
    bg_super_type superType;
    
    void setEnableDeleteButtons(bool enable);
    int fixItemCount;
    int parseErrorCount;
};

#endif /* defined(__bobby__BackgroundScrollView__) */
