//
//  DoleAPI.h
//  bobby
//
//  Created by Jaemok Jeong on 5/19/14.
//
//

#ifndef __bobby__DoleAPI__
#define __bobby__DoleAPI__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"

#include "external/json/document.h"
#include "external/json/prettywriter.h"
#include "external/json/stringbuffer.h"

#include "network/HttpRequest.h"
#include "network/HttpResponse.h"

#include "DoleProduct.h"

using namespace std;

USING_NS_CC;
USING_NS_CC_EXT;

class DoleAPI : public Ref {
public:
    static DoleAPI *getInstance();

#pragma mark - Shop API
    void getProductSales(const cocos2d::network::ccHttpRequestCallback& callback);

protected:
    DoleAPI() {}
    ~DoleAPI() {}

    cocos2d::network::ccHttpRequestCallback _productsRequestCallback;
    void onProductSalesRequestCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);

//    void onHttpRequestCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);
};


#endif /* defined(__bobby__DoleAPI__) */
