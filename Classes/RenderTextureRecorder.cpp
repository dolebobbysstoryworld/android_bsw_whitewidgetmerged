//
//  RenderTextureRecorder.cpp
//  bobby
//
//  Created by Dongwook, Kim on 14. 5. 12..
//
//

#include "RenderTextureRecorder.h"
#include "ccUtils.h"
#include "NameTexture2D.h"

RenderTextureRecorder * RenderTextureRecorder::create()
{
    RenderTextureRecorder *ret = new RenderTextureRecorder();
    
    if(ret)
    {
        ret->autorelease();
        return ret;
    }
    CC_SAFE_DELETE(ret);
    return nullptr;
}

void RenderTextureRecorder::update(float delta)
{
    cocos2d::RenderTexture::update(delta);
    if (this->drawCallback) {
        this->drawCallback();
    }
}

bool RenderTextureRecorder::initWithWidthAndHeight(int w, int h, cocos2d::Texture2D::PixelFormat format, GLuint depthStencilFormat, const void* textureData, GLuint textureName, GLint oldFBO, GLint FBO, GLint oldRBO)
{
    CCASSERT(format != cocos2d::Texture2D::PixelFormat::A8, "only RGB and RGBA formats are valid for a render texture");
    
    bool ret = false;
    void *data = nullptr;
    do
    {
        _fullRect = _rtTextureRect = cocos2d::Rect(0,0,w,h);
        cocos2d::Size size = cocos2d::Director::getInstance()->getWinSizeInPixels();
        _fullviewPort = cocos2d::Rect(0,0,size.width,size.height);
        w = (int)(w * cocos2d::CC_CONTENT_SCALE_FACTOR());
        h = (int)(h * cocos2d::CC_CONTENT_SCALE_FACTOR());
        
        _oldFBO = oldFBO;
        _FBO = FBO;
        
        // textures must be power of two squared
        int powW = 0;
        int powH = 0;
        
        if (cocos2d::Configuration::getInstance()->supportsNPOT())
        {
            powW = w;
            powH = h;
        }
        else
        {
            powW = cocos2d::ccNextPOT(w);
            powH = cocos2d::ccNextPOT(h);
        }
        
        auto dataLen = powW * powH * 4;

        _pixelFormat = format;
        
        NameTexture2D *texture = new NameTexture2D();
        if (texture)
        {
            texture->initWithData(textureData, dataLen, (cocos2d::Texture2D::PixelFormat)_pixelFormat, powW, powH, cocos2d::Size((float)w, (float)h));
        }
        else
        {
            break;
        }
        texture->setTextureName(textureName);
        _texture = (cocos2d::Texture2D *)texture;
        
        if (cocos2d::Configuration::getInstance()->checkForGLExtension("GL_QCOM"))
        {
            _textureCopy = new cocos2d::Texture2D();
            if (_textureCopy)
            {
                _textureCopy->initWithData(data, dataLen, (cocos2d::Texture2D::PixelFormat)_pixelFormat, powW, powH, cocos2d::Size((float)w, (float)h));
            }
            else
            {
                break;
            }
        }
        
        if (depthStencilFormat != 0)
        {
            //create and attach depth buffer
            glGenRenderbuffers(1, &_depthRenderBufffer);
            glBindRenderbuffer(GL_RENDERBUFFER, _depthRenderBufffer);
            glRenderbufferStorage(GL_RENDERBUFFER, depthStencilFormat, (GLsizei)powW, (GLsizei)powH);
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _depthRenderBufffer);
            
            // if depth format is the one with stencil part, bind same render buffer as stencil attachment
            if (depthStencilFormat == GL_DEPTH24_STENCIL8)
            {
                glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, _depthRenderBufffer);
            }
        }
        
        // check if it worked (probably worth doing :) )
        CCASSERT(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE, "Could not attach texture to framebuffer");
        
        _texture->setAliasTexParameters();
        
        // retained
        setSprite(cocos2d::Sprite::createWithTexture(_texture));
        
        _texture->release();
        _sprite->setFlippedY(true);
        
        _sprite->setBlendFunc( cocos2d::BlendFunc::ALPHA_PREMULTIPLIED );
        
        glBindRenderbuffer(GL_RENDERBUFFER, oldRBO);
        glBindFramebuffer(GL_FRAMEBUFFER, _oldFBO);
        
        // Diabled by default.
        _autoDraw = false;
        
        // add sprite for backward compatibility
        addChild(_sprite);
        
        ret = true;
    } while (0);
    
    CC_SAFE_FREE(data);
    
    this->scheduleUpdate();
    return ret;
}