//
//  SoundScrollView.h
//  bobby
//
//  Created by oasis on 2014. 7. 23..
//
//

#ifndef __bobby__SoundScrollView__
#define __bobby__SoundScrollView__

#include <iostream>

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ScrollViewExtension.h"
#include "ui/CocosGUI.h"
#include "PopupLayer.h"

USING_NS_CC;
USING_NS_CC_EXT;

#define TAG_SCROLLVIEW 0

class SoundScrollView;

class SoundScrollViewDelegate
{
public:
    virtual ~SoundScrollViewDelegate() {}
    virtual void onSelectSound(SoundScrollView* view, Node* selectedItem, __String* soundKey, int index) = 0;
};

class SoundScrollView : public cocos2d::Layer, ScrollViewExtensionDelegate//, PopupLayerDelegate
{
public:
    void setSelectSound(int index);
    int getSoundIndex(std::string& charKey);
    
//    bool addSound(std::string soundItemKey);
//    bool removeSound(__String *soundItemKey);
    void rotateSounds(float duration, float angle, bool animation);
    void reloadData();
    
    // scrollview delegate
    void scrollViewDidScroll(ScrollViewExtension* view);
    void scrollViewDidZoom(ScrollViewExtension* view);
    void scrollViewDidScrollEnd(ScrollViewExtension* view);
    
    // scrollview extension delegate
    void onSelectItem(ScrollViewExtension* view, Node* selectedItem, Touch *pTouch, Event *pEvent);
    
    SoundScrollViewDelegate* getDelegate() { return _scrollViewDelegate; }
    void setDelegate(SoundScrollViewDelegate* pDelegate) { _scrollViewDelegate = pDelegate; }
    
    CREATE_FUNC(SoundScrollView);
    
//    __Array *deleteButtons;
//    void popupButtonClicked(PopupLayer *popup, cocos2d::Ref *obj);
    void scrollToLastItem();
    void scrollToFirstItem();

private:
    bool init();
//    void soundItemCoinClicked(Ref* pSender);
    void soundItemPressedCallback(Ref* pSender);
    const char *loadLocalizedSoundName(std::string soundItemKey);
    cocos2d::Node* createSoundForScrollView(std::string soundItemKey, int index);
//    void adjustLine();
    void loadJsonToScrollView(ScrollView *scrollView);
    
    cocos2d::Point scrollOffset;
    SoundScrollViewDelegate *_scrollViewDelegate;
    ScrollViewExtension *createScrollView();
    ScrollViewExtension *scrollView;
    int linesCount;
    __Array* soundList;
    
    cocos2d::Sprite *frameSelectedBorder;
    
    bool isScrollingToFirst;
    
    float currentSoundAngle;
    bool scrollViewPressed;
    bool reachedToEnd;
    
    __Dictionary *items;
    __Array *pins;
//    void deleteButtonClicked(Ref *obj, ui::TouchEventType type);
//    void runRemoveSoundAnimation(__String* bookKey);
    
//    __String *selectedSoundItemKey;
//    Node *selDeleteSoundItem;
};

#endif /* defined(__bobby__SoundScrollView__) */

