//
//  DoleProduct.cpp
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 16..
//
//

#include "DoleProduct.h"
#include "external/tinyxml2/tinyxml2.h"

USING_NS_CC;

#pragma mark - xml Utility
const char* getStringValue(const char *tag, tinyxml2::XMLElement *parent) {
    tinyxml2::XMLElement *child = nullptr;
    child = parent->FirstChildElement(tag);
    if (child) {
        const char *textProperety = child->GetText();
        if (textProperety) {
            return textProperety;
        }
    }
    return NULL;
}

int getIntValue(const char *tag, tinyxml2::XMLElement *parent) {
    tinyxml2::XMLElement *child = nullptr;
    child = parent->FirstChildElement(tag);
    if (child) {
        int target;
        child->QueryIntText(&target);
        return target;
    }
    
    return INT32_MIN;
}

bool getBoolValue(const char *tag, tinyxml2::XMLElement *parent) {
    tinyxml2::XMLElement *child = nullptr;
    child = parent->FirstChildElement(tag);
    if (child) {
        bool target;
        child->QueryBoolText(&target);
        return target;
    }
    return false;
}

#pragma mark - DoleProduct construct/diconstruct
DoleProduct::DoleProduct():
_productNo(-1),
_productPrice(-1),
_salePrice(-1),
_isPurchase(false),
_remainQuantity(-1) {

}

DoleProduct::~DoleProduct() {
    log("DoleProduct destructor call");
}

#pragma mark - class initialized
DoleProduct* DoleProduct::create(tinyxml2::XMLElement *parent) {
    DoleProduct *product = new DoleProduct();
    if (product && product->init(parent)) {
        product->autorelease();
        return product;
    }
    CC_SAFE_DELETE(product);
    return nullptr;
}

bool DoleProduct::init(tinyxml2::XMLElement *parent) {
    this->_productSaleNo = getIntValue("d:ProductSaleNo", parent);// m:type="Edm.Int32">7</> : 프로덕트 판매 설정 번호
    this->_productNo = getIntValue("d:ProductNo", parent);// m:type="Edm.Int32">8</> : Item No
    this->_serviceCode = getStringValue("d:ServiceCode", parent);//>SVR004</>
    this->_saleStatus = getIntValue("d:SaleStatus", parent);// m:type="Edm.Byte">10</> : 판매중인것 10 (필터 설정해서 조회하면 됩니다.)
    this->_saleStartDatetime = getStringValue("d:SaleStartDatetime", parent);// m:type="Edm.DateTime">2014-06-30T11:55:44</> : 판매 종료일
    this->_salsEndDatetime = getStringValue("d:SalsEndDatetime", parent);// m:type="Edm.DateTime">9999-12-31T00:00:00</> : 판매시작일
    this->_productPrice = getIntValue("d:ProductPrice", parent);// m:type="Edm.Int32">0</> : 아이템 원가
    this->_salePrice = getIntValue("d:SalePrice", parent);// m:type="Edm.Int32">0</> : 아이템 판매가격
    this->_productName = getStringValue("d:ProductName", parent);//>Banana 1</> : 아이템 이름
    this->_productCode = getStringValue("d:ProductCode", parent);//>C001</> : Item Key
    this->_productType = getStringValue("d:ProductType", parent);//>ITT001</> : Item Type
    this->_itemSaleType = getStringValue("d:ItemSaleType", parent);//>IST001</> : 구매시 파라메터값, 조회된 값을 이용해도 되고 고정해도 됨
    this->_expiredHour = getIntValue("d:ProductHour", parent);
    {
        char name[3];
        memset(name, 0, 3);
        strncpy(name, this->_productCode.c_str(), 1);
        this->_category = Category::Undefined;
        if (strcmp(name, "C") == 0) {
            this->_category = Category::Character;
        } else if(strcmp(name, "B") == 0) {
            this->_category = Category::Background;
        } else if(strcmp(name, "I") == 0) {
            this->_category = Category::Item;
        } else if(strcmp(name, "M") == 0) {
            this->_category = Category::BGM;
        }
    }

    return true;
}

const char * DoleProduct::description() {
    if (this->_category == Category::Undefined) {
        return "Undefined DoleProduct Object";
    }
    if (this->_productNo == -1) {
        return "Undefined DoleProduct Object";
    }
    
    __String *descriptionString = __String::create("\nDoleProduct Object {");
    descriptionString->appendWithFormat("\n\tProductNo : %d", this->_productNo);
    if (this->_productName.length() > 0) {
        descriptionString->appendWithFormat("\n\tProductName : %s", this->_productName.c_str());
    }
    if (this->_productCode.length() > 0) {
        descriptionString->appendWithFormat("\n\tProductCode : %s", this->_productCode.c_str());
    }
    if (this->_productType.length() > 0) {
        descriptionString->appendWithFormat("\n\tProductType : %s", this->_productType.c_str());
    }
    if (this->_itemSaleType.length() > 0) {
        descriptionString->appendWithFormat("\n\tItemSaleType : %s", this->_itemSaleType.c_str());
    }
    descriptionString->appendWithFormat("\n\tProductPrice : %d / SalePrice : %d", this->_productPrice, this->_salePrice);
    if (this->_saleStartDatetime.length() > 0) {
        descriptionString->appendWithFormat("\n\tSaleStartDatetime : %s", this->_saleStartDatetime.c_str());
    }
    if (this->_salsEndDatetime.length() > 0) {
        descriptionString->appendWithFormat("\n\tSalsEndDatetime : %s", this->_salsEndDatetime.c_str());
    }
    descriptionString->appendWithFormat("\n\tExpiredHour : %d", this->_expiredHour);
//    descriptionString->appendWithFormat("\n\tPurchase status : %s", this->_isPurchase?"Purchased":"Not Purchased");
    if (this->_isPurchase == true) {
        descriptionString->append("\n\tPurchase status : Purchased");
        if (this->_registerDateTime.length() > 0) {
            descriptionString->appendWithFormat("\n\tRegisterDateTime : %s", this->_registerDateTime.c_str());
        }
        descriptionString->appendWithFormat("\n\tRemainQuantity : %d", this->_remainQuantity);
    } else {
        descriptionString->append("\n\tPurchase status : Not Purchased");
    }
    descriptionString->append("\n}");
    return descriptionString->getCString();
}
