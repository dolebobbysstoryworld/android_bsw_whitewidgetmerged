//
//  ToastLayer.cpp
//  bobby
//
//  Created by oasis on 2014. 6. 13..
//
//

#include "ToastLayer.h"
#include "ResourceManager.h"
#include "Define.h"

#define FADE_TYPE_IN 0
#define FADE_TYPE_OUT 1
#define FADE_DURATION 0.5f

ToastLayer::ToastLayer():
_delegate(nullptr) {
    
}

ToastLayer::~ToastLayer() {}

ToastLayer* ToastLayer::create(const std::string &message, float seconds)
{
    ToastLayer *layer = new ToastLayer();
    if (layer && layer->initWithMessage(message, seconds))
    {
        layer->autorelease();
        return layer;
    }
    CC_SAFE_DELETE(layer);
    return nullptr;
}

bool ToastLayer::initWithMessage(const std::string &message, float seconds)
{
    if ( !LayerColor::initWithColor(Color4B(0, 0, 0, 0)) )
    {
        return false;
    }
    
    log("message : %s", message.c_str());
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    removeDuration = 0;
    int messageStringWidth = Device::getTextWidth(message.c_str(), FONT_NAME, TOAST_MESSAGE_FONTSIZE);
    Size messageSize;

    LanguageType currentLanguageType = Application::getInstance()->getCurrentLanguage();
    float extraWidth = 0;

    if (messageStringWidth > TOAST_LABEL_SIZE_TYPE_01.width) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    	std::stringstream stringStream(message);
    	std::string line;
    	std::vector<std::string> lines;
    	messageStringWidth = 0;
    	while (std::getline(stringStream, line, '\n')) { // split string by '\n'
    		lines.push_back(line);
    		int newLineWidth = Device::getTextWidth(line.c_str(), FONT_NAME, TOAST_MESSAGE_FONTSIZE);
    		messageStringWidth = (messageStringWidth > newLineWidth) ? messageStringWidth : newLineWidth;
    	}
    	int lineCount = (int)lines.size();
    	int lineWidth = TOAST_LABEL_SIZE_TYPE_02.width;
    	if(lineCount > 1 && messageStringWidth > lineWidth) {
    		lineWidth = messageStringWidth;
    	} else if(messageStringWidth > lineWidth) {
    		lineCount = 2;
    	}

//        if(currentLanguageType == LanguageType::JAPANESE)
        	extraWidth = TOAST_TYPE_02_EXTRA_WIDTH_JA;
        messageSize = Size(lineWidth + extraWidth,
        		lineCount == 1 ? TOAST_LABEL_SIZE_TYPE_01.height : TOAST_LABEL_SIZE_TYPE_02.height);
#else
        messageSize = Size(TOAST_LABEL_SIZE_TYPE_02.width, 0);
#endif
        removeDuration = 4.0f;
#ifdef MACOS
        messageSize = Size(TOAST_LABEL_SIZE_TYPE_02.width, 70);
#endif
    } else {
        messageSize = Size(messageStringWidth+(TOAST_BG_PADDING_LEFT*2), TOAST_LABEL_SIZE_TYPE_01.height);
        if (messageStringWidth > TOAST_LABEL_SIZE_TYPE_01.width/2) {
            removeDuration = 3.0f;
        } else {
            removeDuration = 2.0f;
        }
    }
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    auto messageLabel = Label::createWithSystemFont(message, FONT_NAME, TOAST_MESSAGE_FONTSIZE,messageSize,TextHAlignment::CENTER, TextVAlignment::CENTER);
#else
    
    float messageFontSize = 0;
    
    switch (currentLanguageType) {
        case LanguageType::JAPANESE: {
            messageFontSize = TOAST_MESSAGE_JA_FONTSIZE;
            break;
        }
        default: {
            messageFontSize = TOAST_MESSAGE_FONTSIZE;
            break;
        }
    }
    
    auto messageLabel = Label::createWithSystemFont(message, FONT_NAME, messageFontSize,messageSize,TextHAlignment::CENTER, TextVAlignment::TOP);
#endif
    messageLabel->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    messageLabel->setColor(Color3B(0xff,0xff,0xff));
    messageLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    messageLabel->setOpacity(0);
    
    auto toastBg = cocos2d::extension::Scale9Sprite::create("popup_toast.png", Rect(0,0,TOAST_BG_SIZE.width,TOAST_BG_SIZE.height),Rect(TOAST_BG_CORNER_SIZE.width,TOAST_BG_CORNER_SIZE.height,TOAST_BG_CORNER_INSET.width,TOAST_BG_CORNER_INSET.height));
    toastBg->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    if (messageStringWidth > TOAST_LABEL_SIZE_TYPE_01.width) {
        toastBg->setContentSize(Size(messageLabel->getContentSize().width, messageLabel->getContentSize().height+TOAST_BG_PADDING_TOP_BOTTOM));
        messageLabel->setPosition(Point(toastBg->getContentSize().width/2-(messageLabel->getContentSize().width)/2, toastBg->getContentSize().height/2-messageLabel->getContentSize().height/2));
    } else {
        toastBg->setContentSize(Size(messageLabel->getContentSize().width,TOAST_CONTENT_SIZE_TYPE_01.height));
        messageLabel->setPosition(Point(toastBg->getContentSize().width/2-messageLabel->getContentSize().width/2, toastBg->getContentSize().height/2-messageLabel->getContentSize().height/2));
    }
    
    toastBg->setPosition(Point(visibleSize.width/2-toastBg->getContentSize().width/2, TOAST_BG_Y));
    toastBg->setOpacity(0);
    toastBg->addChild(messageLabel);
    this->addChild(toastBg);
    
    this->fadeAction(FADE_TYPE_IN, messageLabel, FADE_DURATION);
    this->fadeAction(FADE_TYPE_IN, toastBg, FADE_DURATION);
    
//    this->setContentSize(toastBg->getContentSize());
    this->schedule(schedule_selector(ToastLayer::removeAction), removeDuration);
    
    return true;
}

void ToastLayer::removeAction(float dt)
{    
    const auto& children = this->getChildren();
    for (const auto& child : children) {
        this->fadeAction(FADE_TYPE_OUT, child, FADE_DURATION);
        
        const auto& children2 = child->getChildren();
        for (const auto& child2 : children2) {
            this->fadeAction(FADE_TYPE_OUT, child2, FADE_DURATION);
        }
    }
    auto sequence = Sequence::create(DelayTime::create(FADE_DURATION), CallFunc::create(CC_CALLBACK_0(ToastLayer::removeToastLayer, this)), NULL);
    this->runAction(sequence);
}

void ToastLayer::removeToastLayer()
{
    this->removeFromParentAndCleanup(true);
    if (_delegate) {
        _delegate->onToastRemoved(this);
    }
}

void ToastLayer::fadeAction(int fadeType, Node* obj, float duration)
{
    switch (fadeType) {
        case FADE_TYPE_IN:
        {
            auto fadeIn = FadeIn::create(duration);
            obj->runAction(fadeIn);
            break;
        }
        case FADE_TYPE_OUT:
        {
            auto fadeOut = FadeOut::create(duration);
            obj->runAction(fadeOut);
            break;
        }
        default:
            break;
    }
}

void ToastLayer::onEnter()
{
    Layer::onEnter();
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(false);
    
    listener->onTouchBegan = [=](Touch* touch, Event* event) {
        return true;
    };
    
    listener->onTouchMoved = [=](Touch* touch, Event* event){};
    
    listener->onTouchEnded = [=](Touch* touch, Event* event){
        this->removeAction(removeDuration);
    };
    
    listener->onTouchCancelled = [=](Touch* touch, Event* event){ };
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

void ToastLayer::onExit()
{
    _eventDispatcher->removeEventListenersForTarget(this);
    Layer::onExit();
}

