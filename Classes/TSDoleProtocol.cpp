//
//  TSDolesProtocol.cpp
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 30..
//
//

#include "TSDoleProtocol.h"
#include "external/json/prettywriter.h"
#include "external/json/stringbuffer.h"
#include "Define.h"
#include <cstring>

USING_NS_CC;
using namespace rapidjson;


//#define SERVICE_URL             "https://%s.ithinkcore.net/%s/Api.svc/%s"
//#define SERVER_TYPE_ALPHA       "dole-alpha"
//#define SERVER_TYPE_PRODUCTION  "dole"

//JP added
#define SERVICE_URL             "http://doleserver.com:%s/api/v2/%s/Api.svc/%s"
#define SERVER_TYPE_ALPHA       "4000"
#define SERVER_TYPE_PRODUCTION  "3000"

#if COCOS2D_DEBUG
//#define CURRENT_SERVER_TYPE     SERVER_TYPE_ALPHA
#define CURRENT_SERVER_TYPE     SERVER_TYPE_PRODUCTION
#else
#define CURRENT_SERVER_TYPE     SERVER_TYPE_PRODUCTION
#endif

#define SERVICE_CODE            "SVR004"

static inline const char *getPlatformName() {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    return "Android";
#else
    return "iOS";
#endif
}

//#define TSDEBUG_ENABLE
#ifdef TSDEBUG_ENABLE
#define TSLog   log
#else
#define TSLog   (void)
#endif


#pragma mark - TSDoleProtocol

TSDoleProtocol::TSDoleProtocol():
_delegate(nullptr),
_httpRequest(nullptr),
_updateAuthKey(false),
_protocolUpdateAuthKey(nullptr)
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
,_status(TSProtocolStatus::Normal)
//#endif
{
    
}

TSDoleProtocol::~TSDoleProtocol() {
    TSLog("TSDoleProtocol destructor call");
}

void TSDoleProtocol::init() {
    _httpRequest = new cocos2d::network::HttpRequest();
    _httpRequest->setRequestType(cocos2d::network::HttpRequest::Type::POST);
    _httpRequest->setResponseCallback(CC_CALLBACK_2(TSDoleProtocol::onHttpRequestCompleted, this));
    std::vector<std::string> headers;
    headers.push_back("Content-Type: application/json; charset=utf-8");
    _httpRequest->setHeaders(headers);
}

void TSDoleProtocol::setRequestData(rapidjson::Document *postJson, std::string apiGroup, std::string apiName) {
    __String *urlString = __String::createWithFormat(SERVICE_URL, CURRENT_SERVER_TYPE, apiGroup.c_str(), apiName.c_str());
    TSLog("URL:%s", urlString->getCString());
    _httpRequest->setUrl(urlString->getCString());
    _httpRequest->setTag(apiName.c_str());

	GenericStringBuffer< UTF8<> > buffer;
	Writer< GenericStringBuffer< UTF8<> > > writer(buffer);
	postJson->Accept(writer);
	const char* str = buffer.GetString();
	TSLog("JSON: %s\n", str);

//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	if(_status == TSProtocolStatus::Normal) {
    _httpRequest->setRequestData(str, strlen(str));
	} else {
		TSLog("Return on setRequestData. Status is %d", _status);
		_status = TSProtocolStatus::Finished;
        if (_delegate) {
            _delegate->onRequestCanceled(this);
        }
	}
//#else
//    _httpRequest->setRequestData(str, strlen(str));
//#endif
}

bool TSDoleProtocol::request() {
    if (_updateAuthKey == true) {
        return true;
    }

//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	if(_status != TSProtocolStatus::Normal) {
		TSLog("Return onrequest. Status is %d", _status);
		_status = TSProtocolStatus::Finished;
        if (_delegate) {
            _delegate->onRequestCanceled(this);
        }
		return false;
	}
//#endif

    if (Device::getNetworkAvailable() == false) {
//    if (true) {
        if (_delegate) {
            _delegate->onProtocolFailed(this, TSNetResult::ErrorNetworkDisable);
        }
        return false;
    }
    cocos2d::network::HttpClient::getInstance()->setTimeoutForConnect(15);
    cocos2d::network::HttpClient::getInstance()->send(_httpRequest);
    _httpRequest->release();
    if (_delegate) {
        _delegate->onRequestStarted(this, TSNetResult::Success);
    }
    return true;
}

void TSDoleProtocol::onHttpRequestCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response) {
    if (!response) {
        TSLog("response null!!!!");
        if (_delegate) {
            _delegate->onProtocolFailed(this, TSNetResult::ErrorNotAllowed);
        }
        return;
    }
    
    log("onHttpRequestCompleted");
    
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	if(_status != TSProtocolStatus::Normal) {
        TSLog("Return before parse result. Status is %d", _status);
        _status = TSProtocolStatus::Finished;
        if (_delegate) {
            _delegate->onRequestCanceled(this);
        }
        return;
	}
//#endif

    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) {
        TSLog("%s completed", response->getHttpRequest()->getTag());
        TSLog("HTTP Status Code: %ld, tag = %s", response->getResponseCode(), response->getHttpRequest()->getTag());
    }
    if (response->getResponseCode() == -1) {
        if (_delegate) {
            _delegate->onResponseEnded(this, TSNetResult::ErrorTimeout);
        }
        return;
    }
    
    if (!response->isSucceed()) {
        TSLog("response failed");
        TSLog("error buffer: %s", response->getErrorBuffer());
        if (_delegate) {
            _delegate->onResponseEnded(this, TSNetResult::ErrorServiceNotFound);
        }
        return;
    }
    
    std::vector<char> *buffer = response->getResponseData();
    std::string str(buffer->begin(), buffer->end());
    TSLog("Json: %s\n", str.c_str());

    if (strlen(str.c_str()) <= 0) {
        if (_delegate) {
            _delegate->onResponseEnded(this, TSNetResult::ErrorWrongMessage);
        }
        return;
    }
    
    rapidjson::Document ret;
    ret.Parse<0>(str.c_str());
    if ( ret.HasParseError() == true ) {
        TSLog("ret.HasParseError() == true");
        if (_delegate) {
            _delegate->onResponseEnded(this, TSNetResult::ErrorWrongMessage);
        }
        return;
    }
    
    // JP hardcoded
    // Convert JSON document to string
    StringBuffer strbuf;
    Writer<StringBuffer> writer(strbuf);
    ret.Accept(writer);
    // string str = buffer.GetString();
    printf("%s Returns\n%s\n--\n", response->getHttpRequest()->getUrl(), strbuf.GetString());
    
    //_returnValue = ret["Return"].GetBool();
    _returnValue = ret["Return"].GetBool();
    _returnCode = ret["ReturnCode"].GetInt();
    TSLog("Parsed Data: retValue: %d, retCode: %d\n", _returnValue, _returnCode);

//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	if(_status != TSProtocolStatus::Normal) {
        TSLog("Return after parse result. Status is %d", _status);
        _status = TSProtocolStatus::Finished;
        if (_delegate) {
            _delegate->onRequestCanceled(this);
        }
        return;
	}
//#endif

    TSNetResult netResult = TSNetResult::Success;
    if (_returnValue == true) {
        bool result = this->processResponsData(str);
        if (!result) {
            netResult = TSNetResult::ErrorResultFail;
        }
    } else {
        netResult = TSNetResult::ErrorResultFail;
    }
    if (_delegate) {
        _delegate->onResponseEnded(this, netResult);
    }

//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    _status = TSProtocolStatus::Finished;
//#endif
}

void TSDoleProtocol::updateAuthkey() {
    _updateAuthKey = true;

//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	if(_protocolUpdateAuthKey != nullptr) {
		if(_protocolUpdateAuthKey->_status == TSProtocolStatus::Finished) {
			CC_SAFE_RELEASE_NULL(_protocolUpdateAuthKey);
		} else {
			_protocolUpdateAuthKey->_status = TSProtocolStatus::Canceled;
		}
	}
//#else
//    CC_SAFE_RELEASE_NULL(_protocolUpdateAuthKey);
//#endif
    _protocolUpdateAuthKey = new TSDoleGetReNewAuthKey();
    _protocolUpdateAuthKey->setDelegate(this);
    _protocolUpdateAuthKey->init(UserDefault::getInstance()->getIntegerForKey(UDKEY_INTEGER_USERNO),
                                UserDefault::getInstance()->getStringForKey(UDKEY_STRING_AUTHKEY));
    _protocolUpdateAuthKey->request();
}

void TSDoleProtocol::onRequestStarted(TSDoleProtocol *protocol, TSNetResult result) {
    if (result != TSNetResult::Success) {
        TSLog("TSDoleProtocol::onRequestStarted::auto update Authkey fail");
        if (this->_delegate != nullptr) {
            _returnCode = protocol->_returnCode;
            _returnValue = protocol->_returnValue;
            this->_delegate->onRequestStarted(this, result);
            _updateAuthKey = false;
        }
    } else {
        if (this->_delegate != nullptr) {
            this->_delegate->onRequestStarted(this, result);
        }
    }
}

void TSDoleProtocol::onResponseEnded(TSDoleProtocol *protocol, TSNetResult result) {
    if (result != TSNetResult::Success) {
        TSLog("TSDoleProtocol::onResponseEnded::auto update Authkey fail");
        if (this->_delegate != nullptr) {
            _returnCode = protocol->_returnCode;
            _returnValue = protocol->_returnValue;
            this->_delegate->onResponseEnded(this, result);
            _updateAuthKey = false;
        }
    } else {
        _updateAuthKey = false;
        this->onUpdateAuthkey();
        this->request();
    }
}

void TSDoleProtocol::onProtocolFailed(TSDoleProtocol *protocol, TSNetResult result) {
    _updateAuthKey = false;
    if (this->_delegate != nullptr) {
        TSLog("TSDoleProtocol::onProtocolFailed::auto update Authkey fail");
        _returnCode = protocol->_returnCode;
        _returnValue = protocol->_returnValue;
        this->_delegate->onProtocolFailed(this, result);
    }
}

//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
void TSDoleProtocol::onRequestCanceled(TSDoleProtocol *protocol) {
    if (this->_delegate != nullptr) {
        TSLog("TSDoleProtocol::onRequestCanceled::auto update Authkey canceled");
        _returnCode = 0;
        _returnValue = false;
        this->_delegate->onRequestCanceled(this);
    }
}
//#endif

#pragma mark - Profile API
void TSDoleGetAppVersion::init() {
    TSDoleProtocol::init();
    
	Document doc;
	doc.SetObject();
    
    // rapidjson::Value value_obj(kObjectType);
    doc.AddMember("ServiceCode", SERVICE_CODE, doc.GetAllocator());
    doc.AddMember("MobileOS", getPlatformName(), doc.GetAllocator());
    doc.AddMember("ClientIP", Device::getClientIPString(), doc.GetAllocator());//tuilise_todo : client ip 얻어오기 구현
    
    this->setRequestData(&doc, "Profile", "GetAppVersion");
}

bool TSDoleGetAppVersion::processResponsData(std::string responseJson) {
    rapidjson::Document ret;
    ret.Parse<0>(responseJson.c_str());
    if (!ret["AppURL"].IsNull()) {
        _appUrl = ret["AppURL"].GetString();
    }
    if (!ret["MajorVersion"].IsNull()) {
        _appVersion = ret["MajorVersion"].GetString();
    }
    return true;
}

#pragma mark - Auth API
void TSDoleGetReNewAuthKey::init(int userNo, std::string authKey) {
    TSDoleProtocol::init();
    
	Document doc;
	doc.SetObject();
    doc.AddMember("UserNo", userNo, doc.GetAllocator());
    doc.AddMember("AuthKey", authKey.c_str(), doc.GetAllocator());
    doc.AddMember("ServiceCode", SERVICE_CODE, doc.GetAllocator());
    doc.AddMember("MobileOS", getPlatformName(), doc.GetAllocator());
    doc.AddMember("DeviceToken", Device::getDeviceTokenString(), doc.GetAllocator());
    doc.AddMember("UUID", Device::getUUIDString(), doc.GetAllocator());
    doc.AddMember("ClientIP", Device::getClientIPString(), doc.GetAllocator());
    
    this->setRequestData(&doc, "Auth", "GetReNewAuthKey");
}

bool TSDoleGetReNewAuthKey::processResponsData(std::string responseJson) {
    rapidjson::Document ret;
    ret.Parse<0>(responseJson.c_str());
    std::string authKey = ret["AuthKey"].GetString();
    int userNo = ret["UserNo"].GetInt();
    TSLog("Update AuthKey : %s, UserNo : %d", authKey.c_str(), userNo);
    UserDefault::getInstance()->setStringForKey(UDKEY_STRING_AUTHKEY, authKey);
    return true;
}

#pragma mark - Billing API
void TSDoleGetBalance::init(int userNo) {
    TSDoleProtocol::init();
    
	Document doc;
	doc.SetObject();
    
    // rapidjson::Value value_obj(kObjectType);
    doc.AddMember("UserNo", userNo, doc.GetAllocator());
    doc.AddMember("ClientIP", Device::getClientIPString(), doc.GetAllocator());//tuilise_todo : client ip 얻어오기 구현
    
    this->setRequestData(&doc, "Billing/Wallet", "GetBalance");
}

bool TSDoleGetBalance::processResponsData(std::string responseJson) {
    rapidjson::Document ret;
    ret.Parse<0>(responseJson.c_str());
    
    _balance = ret["EventBalance"].GetInt();
    UserDefault::getInstance()->setIntegerForKey(UDKEY_INTEGER_BALANCE, _balance);
    UserDefault::getInstance()->flush();
    return true;
}

void TSDoleChargeFreeCash::init(int coin) {
    _coin = coin;
    TSDoleProtocol::updateAuthkey();
}

void TSDoleChargeFreeCash::onUpdateAuthkey() {
    TSDoleProtocol::init();
    
	Document doc;
	doc.SetObject();
    
    // rapidjson::Value value_obj(kObjectType);
    doc.AddMember("ServiceCode", SERVICE_CODE, doc.GetAllocator());
    int userNo = UserDefault::getInstance()->getIntegerForKey(UDKEY_INTEGER_USERNO);
    doc.AddMember("UserNo", userNo, doc.GetAllocator());
    std::string authKey = UserDefault::getInstance()->getStringForKey(UDKEY_STRING_AUTHKEY);
    doc.AddMember("AuthKey", authKey.c_str(), doc.GetAllocator());
    long currentItem = time(NULL);
    doc.AddMember("OrderID", __String::createWithFormat("%d_%ld", userNo, currentItem)->getCString(), doc.GetAllocator());
    
    doc.AddMember("PaymentMethodCode", "PMC003", doc.GetAllocator());
    doc.AddMember("PaymentAmount", 0, doc.GetAllocator());
    doc.AddMember("ChargeAmount", _coin, doc.GetAllocator());
    doc.AddMember("ValidityPeriod", 32000, doc.GetAllocator());
    
    doc.AddMember("Description", "", doc.GetAllocator());
    doc.AddMember("UUID", Device::getUUIDString(), doc.GetAllocator());
    doc.AddMember("ClientIP", Device::getClientIPString(), doc.GetAllocator());
    
    this->setRequestData(&doc, "Billing/Charge", "ChargeFreeCash");
}

bool TSDoleChargeFreeCash::processResponsData(std::string responseJson) {
    return true;
}

#pragma mark - CMS API
void TSDoleGetEventNoticeList::init() {
    TSDoleProtocol::init();
    
	Document doc;
	doc.SetObject();
    
    // rapidjson::Value value_obj(kObjectType);
    doc.AddMember("ServiceCode", "SVR004", doc.GetAllocator());
    doc.AddMember("ContentGroupNo", 5, doc.GetAllocator());//bobby 용 Group Number 고정
    doc.AddMember("LanguageNo", 1, doc.GetAllocator());
    std::string country = UserDefault::getInstance()->getStringForKey(UDKEY_STRING_COUNTRY);
    int contentGroupClassNo = -1;
    if (country.compare("PH") == 0) {
        contentGroupClassNo = 153;
    } else if(country.compare("KR") == 0) {
        contentGroupClassNo = 156;
    } else if(country.compare("JP") == 0) {
        contentGroupClassNo = 157;
    } else if(country.compare("NZ") == 0) {
        contentGroupClassNo = 155;
    } else if(country.compare("SG") == 0) {
        contentGroupClassNo = 154;
    }
    doc.AddMember("ContentGroupClassNo", contentGroupClassNo, doc.GetAllocator());
    doc.AddMember("NoticeStatus", 20, doc.GetAllocator());
    doc.AddMember("ClientIP", Device::getClientIPString(), doc.GetAllocator());
    
//    this->setRequestData(&doc, "Portal/Game", "GetEventNoticeList");
}

bool TSDoleGetEventNoticeList::processResponsData(std::string responseJson) {
    rapidjson::Document ret;
    ret.Parse<0>(responseJson.c_str());

    int totalCount = ret["TotalRowCount"].GetInt();
    if (totalCount <= 0) {
        return true;
    }
    
    _eventList = true;
    
    rapidjson::Value& noticeList = ret["NoticeItems"];
    auto listSize = noticeList.Size();
    if (listSize == 0) {
        _eventList = false;
        return true;
    }
    
    rapidjson::Value& noticeItem = noticeList[rapidjson::SizeType(0)];
    
    _eventURL = noticeItem["Summary"].GetString();
    _beginDate = noticeItem["TargetBeginDateTime"].GetString();
    _endDate = noticeItem["TargetEndDateTime"].GetString();
    return true;
}

#pragma mark - Shop API
void TSDoleGetInventoryList::init(std::string authKey, __Array *productList) {
    TSDoleProtocol::init();
    
    _products = productList;
    
	Document doc;
	doc.SetObject();
    
    // rapidjson::Value value_obj(kObjectType);
    doc.AddMember("UserNo", UserDefault::getInstance()->getIntegerForKey(UDKEY_INTEGER_USERNO), doc.GetAllocator());
    std::string loginID = UserDefault::getInstance()->getStringForKey(UDKEY_STRING_LOGINID);
    doc.AddMember("UserID", loginID.c_str(), doc.GetAllocator());
    doc.AddMember("ServiceCode", SERVICE_CODE, doc.GetAllocator());
    doc.AddMember("AuthKey", authKey.c_str(), doc.GetAllocator());
    doc.AddMember("ExpandFlag", 0, doc.GetAllocator());
    doc.AddMember("PageIndex", 1, doc.GetAllocator());
    doc.AddMember("RowPerPage", 50, doc.GetAllocator());
    doc.AddMember("ClientIP", Device::getClientIPString(), doc.GetAllocator());
    
    this->setRequestData(&doc, "Shop/Main", "GetInventoryList");
}

bool TSDoleGetInventoryList::processResponsData(std::string responseJson) {
    rapidjson::Document ret;
    ret.Parse<0>(responseJson.c_str());
    rapidjson::Value& purchaseInventories = ret["PurchaseInventories"];

    auto inventorySize = purchaseInventories.Size();
    for (int index = 0; index < inventorySize; index++) {
        rapidjson::Value& purchaseProduct = purchaseInventories[rapidjson::SizeType(index)];
        // JP Commented out itemNo-based, changed this to itemCode-based
        
        std::string itemCode = purchaseProduct["ItemCode"].GetString();
        ssize_t count = (_products ? _products->count() : 0);
        if (count == 0) {
            return true;
        }
        for (ssize_t index = 0; index<count; index++) {
            DoleProduct* product = (DoleProduct*)_products->getObjectAtIndex(index);
            std::string productCode = product->getProductCode();
            //int no = product->getProductNumber();
            if (product && (productCode.compare(itemCode) == 0) ) {
                std::string itemCode = purchaseProduct["ItemCode"].GetString();
                product->setPurchase(true);
                int remainQuantity = purchaseProduct["RemainQuantity"].GetInt();
                product->setRemainQuantity(remainQuantity);
                std::string registerDateTime = purchaseProduct["RegisterDateTime"].GetString();
                product->setRegisterDateTime(registerDateTime.c_str());
                TSLog("product : %s / remainQuantity : %d / registerDateTime : %s", itemCode.c_str(), remainQuantity, registerDateTime.c_str());
            }
        }

//        int itemNo = purchaseProduct["ItemNo"].GetInt();
//        ssize_t count = (_products ? _products->count() : 0);
//        if (count == 0) {
//            return true;
//        }
//        for (ssize_t index = 0; index<count; index++) {
//            DoleProduct* product = (DoleProduct*)_products->getObjectAtIndex(index);
//            int no = product->getProductNumber();
//            if (product && (no == itemNo) ) {
//                std::string itemCode = purchaseProduct["ItemCode"].GetString();
//                product->setPurchase(true);
//                int remainQuantity = purchaseProduct["RemainQuantity"].GetInt();
//                product->setRemainQuantity(remainQuantity);
//                std::string registerDateTime = purchaseProduct["RegisterDateTime"].GetString();
//                product->setRegisterDateTime(registerDateTime.c_str());
//                TSLog("product : %s / remainQuantity : %d / registerDateTime : %s", itemCode.c_str(), remainQuantity, registerDateTime.c_str());
//            }
//        }
    }
    
    return true;
}

void TSDoleGetCacheSyncVersion::init(std::string authKey) {
    TSDoleProtocol::init();
    
	Document doc;
	doc.SetObject();
    
    // rapidjson::Value value_obj(kObjectType);
//    doc.AddMember("UserNo", UserDefault::getInstance()->getIntegerForKey(UDKEY_INTEGER_USERNO), doc.GetAllocator());
    doc.AddMember("AuthKey", authKey.c_str(), doc.GetAllocator());
    doc.AddMember("ServiceCode", SERVICE_CODE, doc.GetAllocator());
    doc.AddMember("ClientIP", Device::getClientIPString(), doc.GetAllocator());
    
//    this->setRequestData(&doc, "Shop/Main", "GetCacheSyncVersion");
    this->setRequestData(&doc, "Shop/Main", "GetSimpleCacheSyncVersion");
}

bool TSDoleGetCacheSyncVersion::processResponsData(std::string responseJson) {
    rapidjson::Document ret;
    ret.Parse<0>(responseJson.c_str());
    _cacheVersion = ret["Version"].GetDouble();//long type은 int로 처리하면 오류난다.
    return true;
}

void TSDoleExecutePurchase::init(DoleProduct *product) {
    _product = product;
    TSDoleProtocol::updateAuthkey();
}

void TSDoleExecutePurchase::onUpdateAuthkey() {
    TSDoleProtocol::init();

	Document doc;
	doc.SetObject();
    
    // rapidjson::Value value_obj(kObjectType);
    long currentItem = time(NULL);
    int userNo = UserDefault::getInstance()->getIntegerForKey(UDKEY_INTEGER_USERNO);
    doc.AddMember("OrderID", __String::createWithFormat("%d_%ld", userNo, currentItem)->getCString(), doc.GetAllocator());
    std::string loginID = UserDefault::getInstance()->getStringForKey(UDKEY_STRING_LOGINID);
    doc.AddMember("UserID", loginID.c_str(), doc.GetAllocator());
    doc.AddMember("UserNo", userNo, doc.GetAllocator());
    doc.AddMember("ServiceCode", SERVICE_CODE, doc.GetAllocator());
    std::string authKey = UserDefault::getInstance()->getStringForKey(UDKEY_STRING_AUTHKEY);
    doc.AddMember("AuthKey", authKey.c_str(), doc.GetAllocator());
    doc.AddMember("ItemNo", _product->getProductNumber(), doc.GetAllocator());
    doc.AddMember("ItemCode", _product->getProductCode(), doc.GetAllocator());
    doc.AddMember("OrderQuantity", 1, doc.GetAllocator());
    doc.AddMember("ItemType", _product->getProductType().c_str(), doc.GetAllocator());
    doc.AddMember("ItemSaleType", _product->getItemSaleType(), doc.GetAllocator());
    doc.AddMember("PurchaseType", "PST001", doc.GetAllocator());
    doc.AddMember("PurchasePrice", _product->getSalePrice(), doc.GetAllocator());
    doc.AddMember("ExpandFlag", 1, doc.GetAllocator());
    doc.AddMember("Description", "", doc.GetAllocator());
    doc.AddMember("UUID", Device::getUUIDString(), doc.GetAllocator());
    doc.AddMember("ClientIP", Device::getClientIPString(), doc.GetAllocator());
    
    log("TSDoleExecutePurchase::onUpdateAuthkey()");
    this->setRequestData(&doc, "Shop/Main", "ExecutePurchase");
}

bool TSDoleExecutePurchase::processResponsData(std::string responseJson) {
    return true;
}
