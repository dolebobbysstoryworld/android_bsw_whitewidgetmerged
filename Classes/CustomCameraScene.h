//
//  CustomCameraScene.h
//  bobby
//
//  Created by oasis on 2014. 4. 14..
//
//

#ifndef __bobby__CustomCameraScene__
#define __bobby__CustomCameraScene__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

typedef enum camera_type{
    camera_type_bg = 0,
    camera_type_character = 1,
} camera_type;

class CustomCamera;
class CustomCameraDelegate
{
public:
    virtual ~CustomCameraDelegate() {}
    virtual void characterPhotoSaved(std::string characterKey) = 0;
    virtual void bgPhotoSaved(std::string bgKey) = 0;
    virtual void cameraScenePopped() = 0;
};

class CustomCamera : public cocos2d::LayerColor

{
    
public:
//    static cocos2d::Scene* createScene();
    virtual ~CustomCamera(){}
    static cocos2d::Scene* createWithViewType(camera_type type);
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    
//    virtual bool init();
    virtual bool initWithViewType(camera_type type);
    
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    void btnBackCallback();
    
    CREATE_FUNC(CustomCamera);
    
    void customCharacterPhotoSaved(std::string baseCharacterKey, std::string mainPhotoName, std::string thumbnailPhotoName);
    void customBgPhotoSaved(std::string baseBgKey, std::string mainPhotoName, std::string thumbnailPhotoName);

    CustomCameraDelegate *getDelegate() { return _delegate; }
    void setDelegate(CustomCameraDelegate *pDelegate) { _delegate = pDelegate; }
protected:
    CustomCameraDelegate *_delegate;
    
};

#endif /* defined(__bobby__CustomCameraScene__) */
