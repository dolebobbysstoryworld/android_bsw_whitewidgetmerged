//
//  GuideLayer.cpp
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 3..
//
//

#include "GuideLayer.h"

USING_NS_CC;


GuideLayer::GuideLayer():
_delegate(nullptr),
_onceMode(false),
_transparentLayer(nullptr),
_transparentLayerSize(Size::ZERO) {

}

GuideLayer::~GuideLayer() {
    log("GuideLayer release");
}

//GuideLayer* GuideLayer::create()
//{
//    GuideLayer *layer = new GuideLayer();
//    if (layer && layer->init()) {
//        layer->autorelease();
//        return layer;
//    }
//    CC_SAFE_DELETE(layer);
//    return nullptr;
//}
//
void GuideLayer::onEnter() {
    Layer::onEnter();
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [=](Touch* touch, Event* event) {return true;};
    listener->onTouchMoved = [=](Touch* touch, Event* event){};
    listener->onTouchEnded = CC_CALLBACK_2(GuideLayer::onTouchEnded, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    if (_delegate) {
        _delegate->didEnterGuideLayer();
    }
}

void GuideLayer::onExit() {
    _eventDispatcher->removeEventListenersForTarget(this);
    Layer::onExit();
    
    if (_delegate) {
        _delegate->didExitGuideLayer();
    }
}

bool GuideLayer::init() {
//    if ( !LayerColor::initWithColor(Color4B(0, 0, 0, 179)) ) return false;
    if ( !Layer::init() ) return false;

    if (_transparentLayerSize.height>0) {
        Size visibleSize = Director::getInstance()->getVisibleSize();
        _dimLayer = LayerColor::create(Color4B(0, 0, 0, 179), visibleSize.width, visibleSize.height - _transparentLayerSize.height);
        _dimLayer->setPosition(Point(0, _transparentLayerSize.height));
        _transparentLayer = LayerColor::create(Color4B(0, 0, 0, 0),
                                               visibleSize.width, _transparentLayerSize.height);
        _transparentLayer->setPosition(Point::ZERO);
    } else {
        _dimLayer = LayerColor::create(Color4B(0, 0, 0, 179));
    }
    
    this->addChild(_dimLayer, -1);
    if (_transparentLayer) {
        this->addChild(_transparentLayer, -1);
    }
    
    this->setTag(TAG_GUIDE_LAYER);
    _parentLayer->addChild(this, 100);
    currentLanguageType = Application::getInstance()->getCurrentLanguage();
    
    return true;
}

void GuideLayer::onTouchEnded(cocos2d::Touch* touch, cocos2d::Event  *event) {
    log("GuideLayer::onTouchEnded");
//    if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()) {
//        CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
//    }
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
#else
    CocosDenshion::SimpleAudioEngine::getInstance()->stopEffect(_effectID);
#endif
    _effectID = 0;
    _parentLayer->removeChildByTag(TAG_GUIDE_LAYER);
}
//88pix
Point GuideLayer::GetPosition(GuidePosition position) {
    ResolutionType _resType = ResourceManager::getInstance()->getCurrentType();
    LanguageType languageType = Application::getInstance()->getCurrentLanguage();;
    switch (position) {
        case GuidePosition::HomeBobby: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point(1106/2, 260/2); } break;
                case ResolutionType::ipadhd:        { return Point(1106, 260); } break;
                case ResolutionType::iphone4:       { return Point(648, 94); } break;
                case ResolutionType::iphone35:      { return Point(960-(194+24+146+124), 94); } break;
                case ResolutionType::androidfullhd: { return Point((1123-25), 179); } break;
                case ResolutionType::androidhd:     { return Point((1123-25)*2/3, 179*2/3); } break;
            }
        } break;
        case GuidePosition::HomeAction: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point((1106+412+20)/2, (260+34-12)/2); } break;
                case ResolutionType::ipadhd:        { return Point(1106+412+20, 260+34-12); } break;
                case ResolutionType::iphone4:       { return Point(648+194-16, 94+16); } break;
                case ResolutionType::iphone35:      { return Point(960-16-(24+146+124), 94+16); } break;
                case ResolutionType::androidfullhd: { return Point((1123+318-25), (179+72)); } break;
                case ResolutionType::androidhd:     { return Point((1123+318-25)*2/3, (179+72)*2/3); } break;
            }
        } break;
        case GuidePosition::HomeTalkBox: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point(864/2, (260+34+552+68+38)/2); } break;
                case ResolutionType::ipadhd:        { return Point(864, (260+34+552+68+38)); } break;
                case ResolutionType::iphone4:       { return Point(546, 94+18+256+42+16); } break;
                case ResolutionType::iphone35:      { return Point(960-(406+184), (94+18+256+42+16)); } break;
                case ResolutionType::androidfullhd:   { return Point((929-25), (1080-215-35-76-65+30)); } break;
                case ResolutionType::androidhd: { return Point((929-25)*2/3, (1080-215-35-76-65+30)*2/3); } break;
            }
        } break;
        case GuidePosition::HomeTalkBoxArrow: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point((864+(892-32)/2)/2, (260+34+552+68)/2); } break;
                case ResolutionType::ipadhd:        { return Point(864+(892-32)/2, (260+34+552+68)); } break;
                case ResolutionType::iphone4:       { return Point(546+(406-14)/2, 94+18+256+42); } break;
                case ResolutionType::iphone35:      { return Point(960-(14+(406-14)/2+184), (94+18+256+42)); } break;
                case ResolutionType::androidfullhd: { return Point((929-25+(644+30+30-26)/2), (1080-215-35-76-65)); } break;
                case ResolutionType::androidhd:     { return Point((929-25+(644+30+30-26)/2)*2/3, (1080-215-35-76-65)*2/3); } break;
            }
        } break;
        case GuidePosition::HomeLabel: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point((864+50)/2, (260+34+552+68+84)/2); } break;
                case ResolutionType::ipadhd:        { return Point((864+50), (260+34+552+68+84)); } break;
                case ResolutionType::iphone4:       { return Point(546+14, 94+18+256+42+36); } break;
                case ResolutionType::iphone35:      { return Point(960-(406+184)+14, (94+18+256+42+36)); } break;
                case ResolutionType::androidfullhd: { return Point((929+30-25), (1080-215-32-76-68+65-32-50/2)); } break;
                case ResolutionType::androidhd:     { return Point((929+30-25)*2/3, (1080-215-32-76-68+65-32-50/2)*2/3); } break;
            }
        } break;
        case GuidePosition::MapSelectBobby: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point(636/2, 190/2); } break;
                case ResolutionType::ipadhd:        { return Point(636, 190); } break;
                case ResolutionType::iphone4:       { return Point(402, 66); } break;
                case ResolutionType::iphone35:      { return Point(402-88, 66); } break;
                case ResolutionType::androidfullhd: { return Point(677, 101); } break;
                case ResolutionType::androidhd:     { return Point(677*2/3, 101*2/3); } break;
            }
        } break;
        case GuidePosition::MapSelectAction: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point(326/2, 510/2); } break;
                case ResolutionType::ipadhd:        { return Point(326, 510); } break;
                case ResolutionType::iphone4:       { return Point(264, 192); } break;
                case ResolutionType::iphone35:      { return Point(264-88, 192); } break;
                case ResolutionType::androidfullhd: { return Point(460, 373); } break;
                case ResolutionType::androidhd:     { return Point(460*2/3, 373*2/3); } break;
            }
        } break;
        case GuidePosition::MapSelectTalkBox: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point((326+310+20)/2, (190+712+30)/2); } break;
                case ResolutionType::ipadhd:        { return Point((326+310+20), (190+712+30)); } break;
                case ResolutionType::iphone4:       { return Point((264+138+18), 66+320); } break;
                case ResolutionType::iphone35:      { return Point((264+138+18)-88, 66+320); } break;
                case ResolutionType::androidfullhd: { return Point((460+217+37), (1080-216-173)); } break;
                case ResolutionType::androidhd:     { return Point((460+217+37)*2/3, (1080-216-173)*2/3); } break;
            }
        } break;
        case GuidePosition::MapSelectLabel: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point((326+310+20+50)/2, (190+712+30+80)/2); } break;
                case ResolutionType::ipadhd:        { return Point((326+310+20+50), (190+712+30+80)); } break;
                case ResolutionType::iphone4:       { return Point((264+138+18+14), 66+320+34); } break;
                case ResolutionType::iphone35:      { return Point((264+138+18+14)-88, 66+320+34); } break;
                case ResolutionType::androidfullhd: { return Point((460+217+37+30), (1080-216-173+65-32)); } break;
                case ResolutionType::androidhd:     { return Point((460+217+37+30)*2/3, (1080-216-173+65-32)*2/3); } break;
            }
        } break;
        case GuidePosition::MapPlayBobby: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point(1268/2, 16/2); } break;
                case ResolutionType::ipadhd:        { return Point(1268, 16); } break;
                case ResolutionType::iphone4:       { return Point(740, 4); } break;
                case ResolutionType::iphone35:      { return Point(740-88*2, 4); } break;
                case ResolutionType::androidfullhd: { return Point((1248+42), 20); } break;
                case ResolutionType::androidhd:     { return Point((1248+42)*2/3, 20*2/3); } break;
            }
        } break;
        case GuidePosition::MapPlayAction: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point((2048-20-(72+350))/2, 0); } break;
                case ResolutionType::ipadhd:        { return Point(2048-20-(72+350), 0); } break;
                case ResolutionType::iphone4:       { return Point(1136+20-(66+190), 0); } break;
                case ResolutionType::iphone35:      { return Point(960+20-(66+190), 0); } break;
                case ResolutionType::androidfullhd: { return Point((1248+305+42), 0); } break;
                case ResolutionType::androidhd:     { return Point((1248+305+42)*2/3, 0); } break;
            }
        } break;
        case GuidePosition::MapPlayTalkBox: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point(1072/2, (340+356+38)/2); } break;
                case ResolutionType::ipadhd:        { return Point(1072, (340+356+38)); } break;
                case ResolutionType::iphone4:       { return Point(652, 168+166+6+16); } break;
                case ResolutionType::iphone35:      { return Point(652-88*2, 168+166+6+16); } break;
                case ResolutionType::androidfullhd: { return Point((1076+42), (1080-276-230+30)); } break;
                case ResolutionType::androidhd:     { return Point((1076+42)*2/3, (1080-276-230+30)*2/3); } break;
            }
        } break;
        case GuidePosition::MapPlayTalkBoxArrow: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point((1072+(802-32)/2)/2, (340+356)/2); } break;
                case ResolutionType::ipadhd:        { return Point(1072+(802-32)/2, (340+356)); } break;
                case ResolutionType::iphone4:       { return Point(652+(360-14)/2, 168+166+6); } break;
                case ResolutionType::iphone35:      { return Point(652+(360-14)/2-88*2, 168+166+6); } break;
                case ResolutionType::androidfullhd: { return Point((1076+42+(624-26)/2), (1080-276-230)); } break;
                case ResolutionType::androidhd:     { return Point((1076+42+(624-26)/2)*2/3, (1080-276-230)*2/3); } break;
            }
        } break;
        case GuidePosition::MapPlayLabel: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point((1072+50)/2, (340+356+86)/2); } break;
                case ResolutionType::ipadhd:        { return Point(1072+50, (340+356+86)); } break;
                case ResolutionType::iphone4:       { return Point(652+14, 168+166+6+28); } break;
                case ResolutionType::iphone35:      { return Point(652+14-88*2, 168+166+6+32); } break;
                case ResolutionType::androidfullhd: { return Point((1076+30+42), (1080-276-230+54-24)); } break;
                case ResolutionType::androidhd:     { return Point((1076+30+42)*2/3, (1080-276-230+54-24)*2/3); } break;
            }
        } break;
        case GuidePosition::RecordingBobby: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point(130/2, 104/2); } break;
                case ResolutionType::ipadhd:        { return Point(130, 104); } break;
                case ResolutionType::iphone4:       { return Point(122, 26); } break;
                case ResolutionType::iphone35:      { return Point(122-88, 26); } break;
                case ResolutionType::androidfullhd: { return Point(249, 64); } break;
                case ResolutionType::androidhd:     { return Point(249*2/3, 64*2/3); } break;
            }
        } break;
        case GuidePosition::RecordingAction: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point(848/2, (420+260+94)/2); } break;
                case ResolutionType::ipadhd:        { return Point(848, (420+260+94)); } break;
                case ResolutionType::iphone4:       { return Point(470, 180+84+34); } break;
                case ResolutionType::iphone35:      { return Point(470-88, 180+84+34); } break;
                case ResolutionType::androidfullhd: { return Point(808, (307+140+60)); } break;
                case ResolutionType::androidhd:     { return Point(808*2/3, (307+140+60)*2/3); } break;
            }
        } break;
        case GuidePosition::RecordingTalkBox: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point((130+382)/2, 420/2); } break;
                case ResolutionType::ipadhd:        { return Point((130+382), 420); } break;
                case ResolutionType::iphone4:       { return Point(122+182, 180); } break;
                case ResolutionType::iphone35:      { return Point(122+182-88, 180); } break;
                case ResolutionType::androidfullhd: { return Point((249+289), 307); } break;
                case ResolutionType::androidhd:     { return Point((249+289)*2/3, 307*2/3); } break;
            }
        } break;
        case GuidePosition::RecordingLabel: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point((130+382+37+50)/2, (420+32)/2); } break;
                case ResolutionType::ipadhd:        { return Point((130+382+37+50), 420+32); } break;
                case ResolutionType::iphone4:       { return Point(122+182+16+14, 180+20); } break;
                case ResolutionType::iphone35:      { return Point(122+182-88+16+14, 180+20); } break;
                case ResolutionType::androidfullhd: { return Point((249+289+30+30), (307)); } break;
                case ResolutionType::androidhd:     { return Point((249+289+30+30)*2/3, (307)*2/3); } break;
            }
        } break;
        case GuidePosition::SetPosMusicBobby: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point(402/2, 516/2); } break;
                case ResolutionType::ipadhd:        { return Point(402, 516); } break;
                case ResolutionType::iphone4:       { return Point(330, 222); } break;
                case ResolutionType::iphone35:      { return Point(330-88, 222); } break;
                case ResolutionType::androidfullhd: { return Point(582, 371); } break;
                case ResolutionType::androidhd:     { return Point(582*2/3, 371*2/3); } break;
            }
        } break;
        case GuidePosition::SetPosMusicAction: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point(1228/2, (516+26+14)/2); } break;
                case ResolutionType::ipadhd:        { return Point(1228, (516+26+14)); } break;
                case ResolutionType::iphone4:       { return Point((886-20-106), 230); } break;
                case ResolutionType::iphone35:      { return Point((886-20-106)-176, 230); } break;
                case ResolutionType::androidfullhd: { return Point(1300, 391); } break;
                case ResolutionType::androidhd:     { return Point(1300*2/3, 391*2/3); } break;
            }
        } break;
        case GuidePosition::SetPosMusicArrow: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point(1228/2, 732/2); } break;
                case ResolutionType::ipadhd:        { return Point(1228, 732); } break;
                case ResolutionType::iphone4:       { return Point(760, 308); } break;
                case ResolutionType::iphone35:      { return Point(760-88*2, 308); } break;
                case ResolutionType::androidfullhd: { return Point(1291, 522); } break;
                case ResolutionType::androidhd:     { return Point(1291*2/3, 522*2/3); } break;
            }
        } break;
        case GuidePosition::SetPosMusicTalkBox: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point((402+600+26)/2, (516+26+256+100)/2); } break;
                case ResolutionType::ipadhd:        { return Point(402+600+26, 516+26+256+100); } break;
                case ResolutionType::iphone4:       { return Point(330+300, 230+100+66); } break;
                case ResolutionType::iphone35:      { return Point(330+300-88*2, 230+100+66); } break;
                case ResolutionType::androidfullhd: { return Point((582+492), (391+188+74)); } break;
                case ResolutionType::androidhd:     { return Point((582+492)*2/3, (391+188+74)*2/3); } break;
            }
        } break;
        case GuidePosition::SetPosMusicLabel: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point((402+600+26+50-4)/2, (516+26+256+100+80)/2); } break;
                case ResolutionType::ipadhd:        { return Point(402+600+26+50-4, 516+26+256+100+80); } break;
                case ResolutionType::iphone4:       { return Point(330+300+14, 230+100+66+36); } break;
                case ResolutionType::iphone35:      { return Point(330+300-88*2+14, 230+100+66+36); } break;
                case ResolutionType::androidfullhd: { return Point((582+492+30), (391+188+74+65-32)); } break;
                case ResolutionType::androidhd:     { return Point((582+492+30)*2/3, (391+188+74+65-32)*2/3); } break;
            }
        } break;
        case GuidePosition::SetPosBGBobby: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point(482/2, 516/2); } break;
                case ResolutionType::ipadhd:        { return Point(482, 516); } break;
                case ResolutionType::iphone4:       { return Point(400, 222); } break;
                case ResolutionType::iphone35:      { return Point(400-88*2, 222); } break;
                case ResolutionType::androidfullhd: { return Point(682, 371); } break;
                case ResolutionType::androidhd:     { return Point(682*2/3, 371*2/3); } break;
            }
        } break;
        case GuidePosition::SetPosBGAction: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point(1498/2, (516+26+14)/2); } break;
                case ResolutionType::ipadhd:        { return Point(1498, 516+26+14); } break;
                case ResolutionType::iphone4:       { return Point(1136-(106+144), 230); } break;
                case ResolutionType::iphone35:      { return Point(960-(106+144), 230); } break;
                case ResolutionType::androidfullhd: { return Point(1510, 391); } break;
                case ResolutionType::androidhd:     { return Point(1510*2/3, 391*2/3); } break;
            }
        } break;
        case GuidePosition::SetPosBGArrow: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point((1498+118)/2, 742/2); } break;
                case ResolutionType::ipadhd:        { return Point(1498+118, 742); } break;
                case ResolutionType::iphone4:       { return Point(1136-(60+136), 312); } break;
                case ResolutionType::iphone35:      { return Point(960-(60+136), 312); } break;
                case ResolutionType::androidfullhd: { return Point((1510+83), 529); } break;
                case ResolutionType::androidhd:     { return Point((1510+83)*2/3, 529*2/3); } break;
            }
        } break;
        case GuidePosition::SetPosBGTalkBox: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point((482+600)/2, (516+26+256+100)/2); } break;
                case ResolutionType::ipadhd:        { return Point((482+600), (516+26+256+100)); } break;
                case ResolutionType::iphone4:       { return Point(400+300, 230+100+66); } break;
                case ResolutionType::iphone35:      { return Point(400+300-88*2, 230+100+66); } break;
                case ResolutionType::androidfullhd: { return Point((682+492), (1080-200-227)); } break;
                case ResolutionType::androidhd:     { return Point((682+492)*2/3, (1080-200-227)*2/3); } break;
            }
        } break;
        case GuidePosition::SetPosBGLabel: {
            switch (languageType) {
                case LanguageType::KOREAN: {
                    switch (_resType) {
                        case ResolutionType::ipad:          { return Point((482+600+50)/2, (516+26+256+100+80)/2); } break;
                        case ResolutionType::ipadhd:        { return Point((482+600+50), (516+26+256+100+80)); } break;
                        case ResolutionType::iphone4:       { return Point(400+300+14, 230+100+66+36); } break;
                        case ResolutionType::iphone35:      { return Point(400+300-88*2+14, 230+100+66+36); } break;
                        case ResolutionType::androidfullhd: { return Point((682+492+30), (1080-200-227+51-24-54)); } break;
                        case ResolutionType::androidhd:     { return Point((682+492+30)*2/3, (1080-200-227+51-24-54)*2/3); } break;
                    }
                } break;
                default: {
                    switch (_resType) {
                        case ResolutionType::ipad:          { return Point((482+600+50)/2, (516+26+256+100+66)/2); } break;
                        case ResolutionType::ipadhd:        { return Point((482+600+50), (516+26+256+100+66)); } break;
                        case ResolutionType::iphone4:       { return Point(400+300+14, 230+100+66+30); } break;
                        case ResolutionType::iphone35:      { return Point(400+300-88*2+14, 230+100+66+30); } break;
                        case ResolutionType::androidfullhd: { return Point((682+492+30), (1080-200-227+51-24)); } break;
                        case ResolutionType::androidhd:     { return Point((682+492+30)*2/3, (1080-200-227+51-24)*2/3); } break;
                    }
                } break;
            }
        } break;
        case GuidePosition::SetPosCharBobby: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point(482/2, 516/2); } break;
                case ResolutionType::ipadhd:        { return Point(482, 516); } break;
                case ResolutionType::iphone4:       { return Point(400, 222); } break;
                case ResolutionType::iphone35:      { return Point(400-88*2, 222); } break;
                case ResolutionType::androidfullhd: { return Point(682, 371); } break;
                case ResolutionType::androidhd:     { return Point(682*2/3, 371*2/3); } break;
            }
        } break;
        case GuidePosition::SetPosCharAction: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point(1768/2, (516+26+14)/2); } break;
                case ResolutionType::ipadhd:        { return Point(1768, (516+26+14)); } break;
                case ResolutionType::iphone4:       { return Point(1136-(106+18), 230); } break;
                case ResolutionType::iphone35:      { return Point(960-(106+18), 230); } break;
                case ResolutionType::androidfullhd: { return Point(1720, 391); } break;
                case ResolutionType::androidhd:     { return Point(1720*2/3, 391*2/3); } break;
            }
        } break;
        case GuidePosition::SetPosCharArrow: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point((1768+126)/2, 742/2); } break;
                case ResolutionType::ipadhd:        { return Point((1768+126), 742); } break;
                case ResolutionType::iphone4:       { return Point(1136-(60+12), 316); } break;
                case ResolutionType::iphone35:      { return Point(960-(60+12), 316); } break;
                case ResolutionType::androidfullhd: { return Point((1720+83), 519); } break;
                case ResolutionType::androidhd:     { return Point((1720+83)*2/3, 519*2/3); } break;
            }
        } break;
        case GuidePosition::SetPosCharTalkBox: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point((482+600)/2, (516+26+256+100)/2); } break;
                case ResolutionType::ipadhd:        { return Point(482+600, 516+26+256+100); } break;
                case ResolutionType::iphone4:       { return Point(400+300, 230+100+66); } break;
                case ResolutionType::iphone35:      { return Point(400+300-88*2, 230+100+66); } break;
                case ResolutionType::androidfullhd: { return Point((682+492), (1080-254-173)); } break;
                case ResolutionType::androidhd:     { return Point((682+492)*2/3, (1080-254-173)*2/3); } break;
            }
        } break;
        case GuidePosition::SetPosCharLabel: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point((482+600+50)/2, (516+26+256+100+80)/2); } break;
                case ResolutionType::ipadhd:        { return Point(482+600+50, 516+26+256+100+80); } break;
                case ResolutionType::iphone4:       { return Point(400+300+14, 230+100+66+36); } break;
                case ResolutionType::iphone35:      { return Point(400+300-88*2+14, 230+100+66+36); } break;
                case ResolutionType::androidfullhd: { return Point((682+492+30), (1080-254-173+65-32)); } break;
                case ResolutionType::androidhd:     { return Point((682+492+30)*2/3, (1080-254-173+65-32)*2/3); } break;
            }
        } break;
        case GuidePosition::SetPosDeleteBobby: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point((2048-599)/2, 0); } break;
                case ResolutionType::ipadhd:        { return Point(2048-599, 0); } break;
                case ResolutionType::iphone4:       { return Point(384+366+18, 0); } break;
                case ResolutionType::iphone35:      { return Point(384+366+18-88*2, 0); } break;
                case ResolutionType::androidfullhd: { return Point((652+616+34), 0); } break;
                case ResolutionType::androidhd:     { return Point((652+616+34)*2/3, 0); } break;
            }
        } break;
        case GuidePosition::SetPosDeleteAction: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point(464/2, (294+122)/2); } break;
                case ResolutionType::ipadhd:        { return Point(464, 294+122); } break;
                case ResolutionType::iphone4:       { return Point(310, 130+60); } break;
                case ResolutionType::iphone35:      { return Point(310-88, 130+60); } break;
                case ResolutionType::androidfullhd: { return Point(525, 326); } break;
                case ResolutionType::androidhd:     { return Point(525*2/3, 326*2/3); } break;
            }
        } break;
        case GuidePosition::SetPosDeleteArrow: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point(588/2, 152/2); } break;
                case ResolutionType::ipadhd:        { return Point(588, 152); } break;
                case ResolutionType::iphone4:       { return Point(342, 66); } break;
                case ResolutionType::iphone35:      { return Point(342-88, 66); } break;
                case ResolutionType::androidfullhd: { return Point(608, 108); } break;
                case ResolutionType::androidhd:     { return Point(608*2/3, 108*2/3); } break;
            }
        } break;
        case GuidePosition::SetPosDeleteTalkBox: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point((464+344)/2, 754/2); } break;
                case ResolutionType::ipadhd:        { return Point(464+344, 754); } break;
                case ResolutionType::iphone4:       { return Point(310+194+20, 378); } break;
                case ResolutionType::iphone35:      { return Point(310+194+20-88*2, 378); } break;
                case ResolutionType::androidfullhd: { return Point((525+324+142), 640); } break;
                case ResolutionType::androidhd:     { return Point((525+324+142)*2/3, 640*2/3); } break;
            }
        } break;
        case GuidePosition::SetPosDeleteLabel: {
            switch (languageType) {
                case LanguageType::JAPANESE:
                case LanguageType::KOREAN: {
                    switch (_resType) {
                        case ResolutionType::ipad:          { return Point((464+344+50)/2, (754+66)/2); } break;
                        case ResolutionType::ipadhd:        { return Point(464+344+50, 754+66); } break;
                        case ResolutionType::iphone4:       { return Point(310+194+20+14, 378+30); } break;
                        case ResolutionType::iphone35:      { return Point(310+194+20-88*2+14, 378+30); } break;
                        case ResolutionType::androidfullhd: { return Point((525+324+142+30), (640+65-32)); } break;
                        case ResolutionType::androidhd:     { return Point((525+324+142+30)*2/3, (640+65-32)*2/3); } break;
                    }
                } break;
                default: {
                    switch (_resType) {
                        case ResolutionType::ipad:          { return Point((464+344+50)/2, (754+80)/2); } break;
                        case ResolutionType::ipadhd:        { return Point(464+344+50, 754+80); } break;
                        case ResolutionType::iphone4:       { return Point(310+194+20+14, 378+34); } break;
                        case ResolutionType::iphone35:      { return Point(310+194+20-88*2+14, 378+34); } break;
                        case ResolutionType::androidfullhd: { return Point((525+324+142+30), (640+65-32)); } break;
                        case ResolutionType::androidhd:     { return Point((525+324+142+30)*2/3, (640+65-32)*2/3); } break;
                    }
                } break;
            }
        } break;
        case GuidePosition::SetPosDeleteTrash: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point( ((2048-378)/2)/2, 0 ); } break;
                case ResolutionType::ipadhd:        { return Point( (2048-378)/2, 0 ); } break;
                case ResolutionType::iphone4:       { return Point( (1136-208)/2, 0 ); } break;
                case ResolutionType::iphone35:      { return Point( (960-208)/2, 0 ); } break;
                case ResolutionType::androidfullhd: { return Point( (1920-300)/2, 0 ); } break;
                case ResolutionType::androidhd: { return Point( ((1920-300)/2)*2/3, 0 ); } break;
            }
        } break;
        case GuidePosition::SetPosDeleteTrashBG: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point( ((2048-850)/2)/2, 0 ); } break;
                case ResolutionType::ipadhd:        { return Point( (2048-850)/2, 0 ); } break;
                case ResolutionType::iphone4:       { return Point( (1136-366)/2, 0 ); } break;
                case ResolutionType::iphone35:      { return Point( (960-366)/2, 0 ); } break;
                case ResolutionType::androidfullhd: { return Point( (1920-616)/2, 0 ); } break;
                case ResolutionType::androidhd: { return Point( ((1920-616)/2)*2/3, 0 ); } break;
            }
        } break;
        default: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Point(0, 0); } break;
                case ResolutionType::ipadhd:        { return Point(0, 0); } break;
                case ResolutionType::iphone4:       { return Point(0, 0); } break;
                case ResolutionType::iphone35:      { return Point(0, 0); } break;
                case ResolutionType::androidfullhd: { return Point(0, 0); } break;
                case ResolutionType::androidhd:     { return Point(0, 0); } break;
            }
        } break;
    }
    return Point::ZERO;
}

Size GuideLayer::GetSize(GuideSize size) {
    ResolutionType _resType = ResourceManager::getInstance()->getCurrentType();
    LanguageType languageType = Application::getInstance()->getCurrentLanguage();;

    switch (size) {
        case GuideSize::HomeTalkbox: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Size(892/2, 220/2); } break;
                case ResolutionType::ipadhd:        { return Size(892, 220); } break;
                case ResolutionType::iphone4:       { return Size(406, 100); } break;
                case ResolutionType::iphone35:      { return Size(406, 100); } break;
                case ResolutionType::androidfullhd: { return Size((644+30+30), (35+76+65-30)); } break;
                case ResolutionType::androidhd:     { return Size((644+30+30)*2/3, (35+76+65-30)*2/3); } break;
            }
        } break;
        case GuideSize::HomeLabel: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Size(792/2, (96+16)/2); } break;
                case ResolutionType::ipadhd:        { return Size(792, 96+16); } break;
                case ResolutionType::iphone4:       { return Size(378, 44+8); } break;
                case ResolutionType::iphone35:      { return Size(378, 44+8); } break;
                case ResolutionType::androidfullhd: { return Size(644, (76+32+32+50)); } break;
                case ResolutionType::androidhd:     { return Size(644*2/3, (76+32+32+50)*2/3); } break;
            }
        } break;
        case GuideSize::MapSelectTalkbox: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Size(1120/2, 220/2); } break;
                case ResolutionType::ipadhd:        { return Size(1120, 220); } break;
                case ResolutionType::iphone4:
                case ResolutionType::iphone35:      { return Size(506, 98); } break;
                case ResolutionType::androidfullhd: { return Size(884, 173); } break;
                case ResolutionType::androidhd:     { return Size(884*2/3, 173*2/3); } break;
            }
        } break;
        case GuideSize::MapSelectLabel: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Size(1020/2, 96/2); } break;
                case ResolutionType::ipadhd:        { return Size(1020, 96); } break;
                case ResolutionType::iphone4:
                case ResolutionType::iphone35:      { return Size(478, 44); } break;
                case ResolutionType::androidfullhd: { return Size((884-30-30), (76+32+32)); } break;
                case ResolutionType::androidhd:     { return Size((884-30-30)*2/3, (76+32+32)*2/3); } break;
            }
        } break;
        case GuideSize::MapPlayTalkbox: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Size(802/2, (276+32)/2); } break;
                case ResolutionType::ipadhd:        { return Size(802, 276+32); } break;
                case ResolutionType::iphone4:
                case ResolutionType::iphone35:      { return Size(360, 118+16); } break;
                case ResolutionType::androidfullhd: { return Size(624, 200); } break;
                case ResolutionType::androidhd:     { return Size(624*2/3, 200*2/3); } break;
            }
        } break;
        case GuideSize::MapPlayLabel: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Size(702/2, (96+16)*2/2); } break;
                case ResolutionType::ipadhd:        { return Size(702, (96+16)*2); } break;//줄당 16px씩 늘렸다.
                case ResolutionType::iphone4:
                case ResolutionType::iphone35:      { return Size(332, (44+8)*2); } break;//줄당 8px씩 늘렸다.
                case ResolutionType::androidfullhd: { return Size(564, (76+24)*2); } break;
                case ResolutionType::androidhd:     { return Size(564*2/3, (76+24)*2*2/3); } break;
            }
        } break;
        case GuideSize::SetPositionTransparentLayer: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Size( 2048/2, 514/2); } break;
                case ResolutionType::ipadhd:        { return Size( 2048, 514); } break;
                case ResolutionType::iphone4:       { return Size( 1136, 222 ); } break;//( 1136, 232 )
                case ResolutionType::iphone35:      { return Size( 960, 222 ); } break;//( 960, 232 )
                case ResolutionType::androidfullhd: { return Size( 1920, 371 ); } break;
                case ResolutionType::androidhd:     { return Size( 1920*2/3, 371*2/3 ); } break;
            }
        } break;
        case GuideSize::RecordingTalkBox: {
//            switch (languageType) {
//                case LanguageType::KOREAN:
//                case LanguageType::JAPANESE: {
                    switch (_resType) {
                        case ResolutionType::ipad:          { return Size(1148/2, (254+32)/2); } break;
                        case ResolutionType::ipadhd:        { return Size(1148, 254+32); } break;
                        case ResolutionType::iphone4:
                        case ResolutionType::iphone35:      { return Size(536, 118+16); } break;
                        case ResolutionType::androidfullhd: { return Size(845, 200); } break;
                        case ResolutionType::androidhd:     { return Size(845*2/3, 200*2/3); } break;
                    }
//                } break;
//                default:
//                    switch (_resType) {
//                        case ResolutionType::ipad:          { return Size(1148/2, 160/2); } break;
//                        case ResolutionType::ipadhd:        { return Size(1148, 160); } break;
//                        case ResolutionType::iphone4:
//                        case ResolutionType::iphone35:      { return Size(536, 84); } break;
//                        case ResolutionType::androidfullhd: { return Size(845, 140); } break;
//                        case ResolutionType::androidhd:     { return Size(845*2/3, 140*2/3); } break;
//                    }
//                    break;
//            }
        } break;
        case GuideSize::RecordingLabel: {
//            switch (languageType) {
//                case LanguageType::KOREAN:
//                case LanguageType::JAPANESE: {
                    switch (_resType) {
                        case ResolutionType::ipad:          { return Size(1011/2, (96+16)*2/2); } break;
                        case ResolutionType::ipadhd:        { return Size(1011, (96+16)*2); } break;
                        case ResolutionType::iphone4:
                        case ResolutionType::iphone35:      { return Size(492, (44+8)*2); } break;
                        case ResolutionType::androidfullhd: { return Size((845-60-30), (200)); } break;
                        case ResolutionType::androidhd:     { return Size((845-60-30)*2/3, (200)*2/3); } break;
                    }
//                } break;
//                default: {
//                    switch (_resType) {
//                        case ResolutionType::ipad:          { return Size(1011/2, 96/2); } break;
//                        case ResolutionType::ipadhd:        { return Size(1011, 96); } break;
//                        case ResolutionType::iphone4:
//                        case ResolutionType::iphone35:      { return Size(492, 44); } break;
//                        case ResolutionType::androidfullhd: { return Size((845-60-30), (140)); } break;
//                        case ResolutionType::androidhd:     { return Size((845-60-30)*2/3, (140)*2/3); } break;
//                    }
//                } break;
//            }
        } break;
        case GuideSize::SetPosMusicTalkBox: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Size(1000/2, 220/2); } break;
                case ResolutionType::ipadhd:        { return Size(1000, 220); } break;
                case ResolutionType::iphone4:
                case ResolutionType::iphone35:      { return Size(490, 100); } break;
                case ResolutionType::androidfullhd: { return Size(827, 173); } break;
                case ResolutionType::androidhd:     { return Size(827*2/3, 173*2/3); } break;
            }
        } break;
        case GuideSize::SetPosMusicLabel: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Size((900+8)/2, 96/2); } break;
                case ResolutionType::ipadhd:        { return Size((900+8), 96); } break;
                case ResolutionType::iphone4:
                case ResolutionType::iphone35:      { return Size(462, 44); } break;
                case ResolutionType::androidfullhd: { return Size((827-30-30), (173-(65-32))); } break;
                case ResolutionType::androidhd:     { return Size((827-30-30)*2/3, (173-(65-32))*2/3); } break;
            }
        } break;
        case GuideSize::SetPosBGTalkBox: {
            switch (languageType) {
                case LanguageType::KOREAN: {
                    switch (_resType) {
                        case ResolutionType::ipad:          { return Size(920/2, 220/2); } break;
                        case ResolutionType::ipadhd:        { return Size(920, 220); } break;
                        case ResolutionType::iphone4:
                        case ResolutionType::iphone35:      { return Size(420, 100); } break;
                        case ResolutionType::androidfullhd: { return Size(727, 173); } break;
                        case ResolutionType::androidhd:     { return Size(727*2/3, 173*2/3); } break;
                    }
                } break;
                default: {
                    switch (_resType) {
                        case ResolutionType::ipad:          { return Size(920/2, (290+32)/2); } break;
                        case ResolutionType::ipadhd:        { return Size(920, 290+32); } break;
                        case ResolutionType::iphone4:
                        case ResolutionType::iphone35:      { return Size(420, 134+8*2); } break;
                        case ResolutionType::androidfullhd: { return Size(727, 227); } break;
                        case ResolutionType::androidhd:     { return Size(727*2/3, 227*2/3); } break;
                    }
                } break;
            }
        } break;
        case GuideSize::SetPosBGLabel: {
            switch (languageType) {
                case LanguageType::KOREAN: {
                    switch (_resType) {
                        case ResolutionType::ipad:          { return Size(820/2, 96/2); } break;
                        case ResolutionType::ipadhd:        { return Size(820, 96); } break;
                        case ResolutionType::iphone4:
                        case ResolutionType::iphone35:      { return Size(392, 44); } break;
                        case ResolutionType::androidfullhd: { return Size((727-30*2), (76+24)*2+54); } break;
                        case ResolutionType::androidhd:     { return Size((727-30*2)*2/3, ((76+24)*2+54)*2/3); } break;
                    }
                } break;
                default: {
                    switch (_resType) {
                        case ResolutionType::ipad:          { return Size(820/2, (96+16)*2/2); } break;
                        case ResolutionType::ipadhd:        { return Size(820, (96+16)*2); } break;
                        case ResolutionType::iphone4:
                        case ResolutionType::iphone35:      { return Size(392, (44+8)*2); } break;
                        case ResolutionType::androidfullhd: { return Size((727-30*2), (76+24)*2); } break;
                        case ResolutionType::androidhd:     { return Size((727-30*2)*2/3, (76+24)*2*2/3); } break;
                    }
                } break;
            }
        } break;
        case GuideSize::SetPosCharTalkBox: {
//            switch (languageType) {
//                case LanguageType::KOREAN:
//                case LanguageType::JAPANESE: {
                    switch (_resType) {
                        case ResolutionType::ipad:          { return Size(920/2, (290+32)/2); } break;
                        case ResolutionType::ipadhd:        { return Size(920, (290+32)); } break;
                        case ResolutionType::iphone4:
                        case ResolutionType::iphone35:      { return Size(420, 134+16); } break;
                        case ResolutionType::androidfullhd: { return Size(727, 227); } break;
                        case ResolutionType::androidhd:     { return Size(727*2/3, 227*2/3); } break;
                    }
//                } break;
//                default: {
//                    switch (_resType) {
//                        case ResolutionType::ipad:          { return Size(920/2, 220/2); } break;
//                        case ResolutionType::ipadhd:        { return Size(920, 220); } break;
//                        case ResolutionType::iphone4:
//                        case ResolutionType::iphone35:      { return Size(420, 100); } break;
//                        case ResolutionType::androidfullhd: { return Size(727, 173); } break;
//                        case ResolutionType::androidhd:     { return Size(727*2/3, 173*2/3); } break;
//                    }
//                } break;
//            }
        } break;
        case GuideSize::SetPosCharLabel: {
//            switch (languageType) {
//                case LanguageType::KOREAN:
//                case LanguageType::JAPANESE: {
                    switch (_resType) {
                        case ResolutionType::ipad:          { return Size(820/2, (96+16)*2/2); } break;
                        case ResolutionType::ipadhd:        { return Size(820, (96+16)*2); } break;
                        case ResolutionType::iphone4:
                        case ResolutionType::iphone35:      { return Size(392, (44+8)*2); } break;
                        case ResolutionType::androidfullhd: { return Size((727-30-30), (227-(65-32))); } break;
                        case ResolutionType::androidhd:     { return Size((727-30-30)*2/3, (227-(65-32))*2/3); } break;
                    }
//                } break;
//                default: {
//                    switch (_resType) {
//                        case ResolutionType::ipad:          { return Size(820/2, 96/2); } break;
//                        case ResolutionType::ipadhd:        { return Size(820, 96); } break;
//                        case ResolutionType::iphone4:
//                        case ResolutionType::iphone35:      { return Size(392, 44); } break;
//                        case ResolutionType::androidfullhd: { return Size((727-30-30), (173-(65-32))); } break;
//                        case ResolutionType::androidhd:     { return Size((727-30-30)*2/3, (173-(65-32))*2/3); } break;
//                    }
//                } break;
//            }
        } break;
        case GuideSize::SetPosDeleteTalkBox: {
            switch (languageType) {
                case LanguageType::KOREAN:
                case LanguageType::JAPANESE: {
                    switch (_resType) {
                        case ResolutionType::ipad:          { return Size(1120/2, (290+32)/2); } break;
                        case ResolutionType::ipadhd:        { return Size(1120, 290+32); } break;
                        case ResolutionType::iphone4:
                        case ResolutionType::iphone35:      { return Size(506, 134+16); } break;
                        case ResolutionType::androidfullhd: { return Size(854, 227); } break;
                        case ResolutionType::androidhd:     { return Size(854*2/3, 227*2/3); } break;
                    }
                } break;
                default: {
                    switch (_resType) {
                        case ResolutionType::ipad:          { return Size(1120/2, 220/2); } break;
                        case ResolutionType::ipadhd:        { return Size(1120, 220); } break;
                        case ResolutionType::iphone4:
                        case ResolutionType::iphone35:      { return Size(506, 100); } break;
                        case ResolutionType::androidfullhd: { return Size(854, 173); } break;
                        case ResolutionType::androidhd:     { return Size(854*2/3, 173*2/3); } break;
                    }
                } break;
            }
        } break;
        case GuideSize::SetPosDeleteLabel: {
            switch (languageType) {
                case LanguageType::KOREAN:
                case LanguageType::JAPANESE: {
                    switch (_resType) {
                        case ResolutionType::ipad:          { return Size(1020/2, (96+16)*2/2); } break;
                        case ResolutionType::ipadhd:        { return Size(1020, (96+16)*2); } break;
                        case ResolutionType::iphone4:
                        case ResolutionType::iphone35:      { return Size(478, (44+8)*2); } break;
                        case ResolutionType::androidfullhd: { return Size((854-30-30), (227-(65-32))); } break;
                        case ResolutionType::androidhd:     { return Size((854-30-30)*2/3, (227-(65-32))*2/3); } break;
                    }
                } break;
                default: {
                    switch (_resType) {
                        case ResolutionType::ipad:          { return Size(1020/2, 96/2); } break;
                        case ResolutionType::ipadhd:        { return Size(1020, 96); } break;
                        case ResolutionType::iphone4:
                        case ResolutionType::iphone35:      { return Size(478, 44); } break;
                        case ResolutionType::androidfullhd: { return Size((854-30-30), (173-(65-32))); } break;
                        case ResolutionType::androidhd:     { return Size((854-30-30)*2/3, (173-(65-32))*2/3); } break;
                    }
                } break;
            }
        } break;
        default: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Size(0, 0); } break;
                case ResolutionType::ipadhd:        { return Size(0, 0); } break;
                case ResolutionType::iphone4:       { return Size(0, 0); } break;
                case ResolutionType::iphone35:      { return Size(0, 0); } break;
                case ResolutionType::androidfullhd: { return Size(0, 0); } break;
                case ResolutionType::androidhd:     { return Size(0, 0); } break;
            }
        } break;
    }
    return Size::ZERO;
}

Rect GuideLayer::GetRect(GuideRect rect) {
    ResolutionType _resType = ResourceManager::getInstance()->getCurrentType();
    switch (rect) {
        case GuideRect::HomeTalkBox: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Rect(0, 0, 317/2, 184/2); } break;
                case ResolutionType::ipadhd:        { return Rect(0, 0, 317, 184); } break;
                case ResolutionType::iphone4:
                case ResolutionType::iphone35:      { return Rect(0, 0, 106, 84); } break;
                case ResolutionType::androidfullhd:   { return Rect(0, 0, 144, 146); } break;
                case ResolutionType::androidhd: { return Rect(0, 0, 96, 97); } break;
            }
        } break;
        case GuideRect::HomeTalkBoxInset: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Rect(50/2, 50/2, 217/2, 84/2); } break;
                case ResolutionType::ipadhd:        { return Rect(50, 50, 217, 84); } break;
                case ResolutionType::iphone4:
                case ResolutionType::iphone35:      { return Rect(15, 15, 76, 50); } break;
                case ResolutionType::androidfullhd:   { return Rect(25, 30, 94, 92); } break;
                case ResolutionType::androidhd: { return Rect(20, 24, 56, 54); } break;
            }
        } break;
        case GuideRect::MapSelectTalkBox: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Rect(0, 0, 640/2, 220/2); } break;
                case ResolutionType::ipadhd:        { return Rect(0, 0, 640, 220); } break;
                case ResolutionType::iphone4:
                case ResolutionType::iphone35:      { return Rect(0, 0, 106, 98); } break;
                case ResolutionType::androidfullhd: { return Rect(0, 0, 554, 173); } break;
                case ResolutionType::androidhd:     { return Rect(0, 0, 369, 115); } break;
            }
        } break;
        case GuideRect::MapSelectTalkBoxInset: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Rect(350/2, 50/2, 230/2, 80/2); } break;
                case ResolutionType::ipadhd:        { return Rect(350, 50, 230, 80); } break;
                case ResolutionType::iphone4:
                case ResolutionType::iphone35:      { return Rect(60, 25, 30, 35); } break;
                case ResolutionType::androidfullhd: { return Rect(300, 60, 1, 1); } break;
                case ResolutionType::androidhd:     { return Rect(200, 40, 1, 1); } break;
            }
        } break;
        case GuideRect::MapPlayTalkBox: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Rect(0, 0, 317/2, 184/2); } break;
                case ResolutionType::ipadhd:        { return Rect(0, 0, 317, 184); } break;
                case ResolutionType::iphone4:
                case ResolutionType::iphone35:      { return Rect(0, 0, 106, 84); } break;
                case ResolutionType::androidfullhd: { return Rect(0, 0, 144, 146); } break;
                case ResolutionType::androidhd:     { return Rect(0, 0, 96, 97); } break;
            }
        } break;
        case GuideRect::MapPlayTalkBoxInset: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Rect(50/2, 50/2, 217/2, 84/2); } break;
                case ResolutionType::ipadhd:        { return Rect(50, 50, 217, 84); } break;
                case ResolutionType::iphone4:
                case ResolutionType::iphone35:      { return Rect(15, 15, 76, 50); } break;
                case ResolutionType::androidfullhd:   { return Rect(25, 30, 94, 92); } break;
                case ResolutionType::androidhd: { return Rect(20, 24, 56, 54); } break;
            }
        } break;
        case GuideRect::RecordingTalkBox: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Rect(0, 0, 300/2, 160/2); } break;
                case ResolutionType::ipadhd:        { return Rect(0, 0, 300, 160); } break;
                case ResolutionType::iphone4:
                case ResolutionType::iphone35:      { return Rect(0, 0, 116, 84); } break;
                case ResolutionType::androidfullhd: { return Rect(0, 0, 235, 140); } break;
                case ResolutionType::androidhd:     { return Rect(0, 0, 157, 94); } break;
            }
        } break;
        case GuideRect::RecordingTalkBoxInset: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Rect(90/2, 30/2, 160/2, 20/2); } break;
                case ResolutionType::ipadhd:        { return Rect(90, 30, 160, 20); } break;
                case ResolutionType::iphone4:
                case ResolutionType::iphone35:      { return Rect(40, 25, 50, 10); } break;
                case ResolutionType::androidfullhd: { return Rect(90, 30, 1, 1); } break;
                case ResolutionType::androidhd:     { return Rect(60, 20, 1, 1); } break;
            }
        } break;
        case GuideRect::SetPosCharTalkBox:
        case GuideRect::SetPosMusicTalkBox: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Rect(0, 0, 300/2, 220/2); } break;
                case ResolutionType::ipadhd:        { return Rect(0, 0, 300, 220); } break;
                case ResolutionType::iphone4:
                case ResolutionType::iphone35:      { return Rect(0, 0, 150, 100); } break;
                case ResolutionType::androidfullhd: { return Rect(0, 0, 238, 173); } break;
                case ResolutionType::androidhd:     { return Rect(0, 0, 159, 116); } break;
            }
        } break;
        case GuideRect::SetPosCharTalkBoxInset:
        case GuideRect::SetPosMusicTalkBoxInset: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Rect(140/2, 40/2, 120/2, 100/2); } break;
                case ResolutionType::ipadhd:        { return Rect(140, 40, 120, 100); } break;
                case ResolutionType::iphone4:
                case ResolutionType::iphone35:      { return Rect(70, 25, 50, 40); } break;
                case ResolutionType::androidfullhd: { return Rect(110, 20, 98, 123); } break;
                case ResolutionType::androidhd:     { return Rect(70, 10, 69, 81); } break;
            }
        } break;
        case GuideRect::SetPosBGTalkBox: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Rect(0, 0, 380/2, 290/2); } break;
                case ResolutionType::ipadhd:        { return Rect(0, 0, 380, 290); } break;
                case ResolutionType::iphone4:
                case ResolutionType::iphone35:      { return Rect(0, 0, 130, 134); } break;
                case ResolutionType::androidfullhd: { return Rect(0, 0, 238, 227); } break;
                case ResolutionType::androidhd:     { return Rect(0, 0, 159, 152); } break;
            }
        } break;
        case GuideRect::SetPosBGTalkBoxInset: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Rect(150/2, 50/2, 160/2, 150/2); } break;
                case ResolutionType::ipadhd:        { return Rect(150, 50, 160, 150); } break;
                case ResolutionType::iphone4:
                case ResolutionType::iphone35:      { return Rect(70, 25, 30, 60); } break;
                case ResolutionType::androidfullhd: { return Rect(105, 20, 103, 167); } break;
                case ResolutionType::androidhd:     { return Rect(70, 20, 69, 102); } break;
            }
        } break;
        case GuideRect::SetPosDeleteTalkBox: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Rect(0, 0, 580/2, 220/2); } break;
                case ResolutionType::ipadhd:        { return Rect(0, 0, 580, 220); } break;
                case ResolutionType::iphone4:
                case ResolutionType::iphone35:      { return Rect(0, 0, 116, 100); } break;
                case ResolutionType::androidfullhd: { return Rect(0, 0, 614, 173); } break;
                case ResolutionType::androidhd:     { return Rect(0, 0, 409, 115); } break;
            }
        } break;
        case GuideRect::SetPosDeleteTalkBoxInset: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Rect(50/2, 50/2, 200/2, 80/2); } break;
                case ResolutionType::ipadhd:        { return Rect(50, 50, 200, 80); } break;
                case ResolutionType::iphone4:
                case ResolutionType::iphone35:      { return Rect(25, 25, 30, 30); } break;
                case ResolutionType::androidfullhd: { return Rect(60, 31, 242, 87); } break;
                case ResolutionType::androidhd:     { return Rect(27, 23, 165, 55); } break;
            }
        } break;
        default: {
            switch (_resType) {
                case ResolutionType::ipad:          { return Rect(0, 0, 0, 0); } break;
                case ResolutionType::ipadhd:        { return Rect(0, 0, 0, 0); } break;
                case ResolutionType::iphone4:       { return Rect(0, 0, 0, 0); } break;
                case ResolutionType::iphone35:      { return Rect(0, 0, 0, 0); } break;
                case ResolutionType::androidfullhd: { return Rect(0, 0, 0, 0); } break;
                case ResolutionType::androidhd:     { return Rect(0, 0, 0, 0); } break;
            }
        } break;
    }
    return Rect::ZERO;
}

float GuideLayer::GetExtra(GuideExtra extra) {
    ResolutionType _resType = ResourceManager::getInstance()->getCurrentType();
    LanguageType languageType = Application::getInstance()->getCurrentLanguage();
    if(languageType != LanguageType::JAPANESE)
    	return 0;

    switch (extra) {
        case GuideExtra::HomeLongSizeExtra: {
            switch (_resType) {
                case ResolutionType::ipad:          { return 200/2; } break;
                case ResolutionType::ipadhd:        { return 200; } break;
                case ResolutionType::iphone4:
                case ResolutionType::iphone35:      { return 80; } break;
                case ResolutionType::androidfullhd: { return 150; } break;
                case ResolutionType::androidhd:     { return 150*2/3; } break;
            }
        } break;
        case GuideExtra::MapSelectLongSizeExtra: {
            switch (_resType) {
                case ResolutionType::ipad:          { return 160/2; } break;
                case ResolutionType::ipadhd:        { return 160; } break;
                case ResolutionType::iphone4:
                case ResolutionType::iphone35:      { return 60; } break;
                case ResolutionType::androidfullhd: { return 100; } break;
                case ResolutionType::androidhd:     { return 100*2/3; } break;
            }
        } break;
    }
    return 0;
}

//float GuideLayer::GetGDelta(int delta) {
//    return -1;
//}
