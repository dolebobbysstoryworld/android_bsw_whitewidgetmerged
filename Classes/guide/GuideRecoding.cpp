//
//  GuideRecoding.cpp
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 10..
//
//

#include "GuideRecoding.h"

USING_NS_CC;

GuideRecoding::GuideRecoding(Layer *parentLayer) {
    _parentLayer = parentLayer;
}

GuideRecoding::~GuideRecoding() {
    log("GuideRecoding destructor call");
}

GuideRecoding* GuideRecoding::create(Layer *parentLayer)
{
#ifndef _DEBUG_GUIDE_
    const char *key = "showguide_recording";
    if (UserDefault::getInstance()->getBoolForKey(key, false) == true) {
        return nullptr;
    }
    UserDefault::getInstance()->setBoolForKey(key, true);
#endif
    GuideRecoding *layer = new GuideRecoding(parentLayer);
    if (layer && layer->init()) {
        layer->autorelease();
        return layer;
    }
    CC_SAFE_DELETE(layer);
    return nullptr;
}

void GuideRecoding::onEnter() {
    GuideLayer::onEnter();
}

void GuideRecoding::onExit() {
    GuideLayer::onExit();
}

bool GuideRecoding::init()
{
    if ( !GuideLayer::init() ) {
        return false;
    }
    
    switch (currentLanguageType) {
        case LanguageType::KOREAN: {
            _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/ko/09_recording.mp3", false);
            break;
        }
        case LanguageType::JAPANESE: {
            _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/ja/09_recording.mp3", false);
            break;
        }
        case LanguageType::ENGLISH:
        default: {
            _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/en/09_recording.mp3", false);
            break;
        }
    }

    Sprite *bobby = Sprite::create("guide_photo_edit_bobby.png");
    bobby->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    bobby->setPosition(GetPosition(GuidePosition::RecordingBobby));
    this->addChild(bobby, 0);
    
    Sprite *action = Sprite::create("guide_recording.png");
    action->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    action->setPosition(GetPosition(GuidePosition::RecordingAction));
    this->addChild(action);
    
    Scale9Sprite *talkBox = Scale9Sprite::create("guide_recording_talkbox.png", GetRect(GuideRect::RecordingTalkBox), GetRect(GuideRect::RecordingTalkBoxInset));
    talkBox->setContentSize(GetSize(GuideSize::RecordingTalkBox));
    talkBox->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    talkBox->setPosition(GetPosition(GuidePosition::RecordingTalkBox));
    this->addChild(talkBox);
    
    Label *textLabel = nullptr;
    const char *labelString = Device::getResString(Res::Strings::CC_GUIDE_SETPOSITION_RECORDING);
    switch (currentLanguageType) {
        case LanguageType::KOREAN:
        case LanguageType::ENGLISH: {
            textLabel = cocos2d::Label::createWithTTF(labelString, FONT_NANUM_PEN, GUIDE_LABEL_NANUM_FONTSIZE, GetSize(GuideSize::RecordingLabel));
        } break;
        case LanguageType::JAPANESE:
        default: {
            textLabel = cocos2d::Label::createWithSystemFont(labelString, FONT_NAME, GUIDE_LABEL_NORMAL_FONTSIZE, GetSize(GuideSize::RecordingLabel));
            textLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
        } break;
    }
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    textLabel->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::CENTER);
#else
    switch (currentLanguageType) {
        case LanguageType::JAPANESE: {
            textLabel->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::CENTER);
        } break;
        default: {
            textLabel->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::TOP);
        } break;
    }
#endif
    textLabel->setColor(Color3B(0xff, 0xff, 0xff));
    
    textLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    textLabel->setPosition(GetPosition(GuidePosition::RecordingLabel));//Point(1228/2, 190/2)
    
//    guideLabel->enableShadow(Color4B(0,0,0,255), LABEL_SHADOW_DEFAULT_SIZE); //LABEL_SHADOW_PLAY_SIZE
    this->addChild(textLabel, 15);

    return true;
}
