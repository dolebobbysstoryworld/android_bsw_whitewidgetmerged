//
//  GuideLayer.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 3..
//
//

#ifndef _GUIDELAYER_H_
#define _GUIDELAYER_H_

#include "GuideLayerProtocol.h"
#include "Define.h"
#include "extensions/cocos-ext.h"
#include "SimpleAudioEngine.h"

#define _GUIDE_ENABLE_
#if COCOS2D_DEBUG
//#define _DEBUG_GUIDE_
#endif

#define FONT_NANUM_PEN "fonts/NanumPen.ttf"

#define TAG_GUIDE_LAYER             	98989898

NS_CC_BEGIN


enum class GuidePosition {
    HomeBobby,
    HomeAction,
    HomeTalkBox,
    HomeTalkBoxArrow,
    HomeLabel,
    MapSelectBobby,
    MapSelectAction,
    MapSelectTalkBox,
    MapSelectLabel,
    MapPlayBobby,
    MapPlayAction,
    MapPlayTalkBox,
    MapPlayTalkBoxArrow,
    MapPlayLabel,
    RecordingBobby,
    RecordingAction,
    RecordingTalkBox,
    RecordingLabel,
    SetPosMusicBobby,
    SetPosMusicAction,
    SetPosMusicArrow,
    SetPosMusicTalkBox,
    SetPosMusicLabel,
    SetPosBGBobby,
    SetPosBGAction,
    SetPosBGArrow,
    SetPosBGTalkBox,
    SetPosBGLabel,
    SetPosCharBobby,
    SetPosCharAction,
    SetPosCharArrow,
    SetPosCharTalkBox,
    SetPosCharLabel,
    SetPosDeleteBobby,
    SetPosDeleteAction,
    SetPosDeleteArrow,
    SetPosDeleteTalkBox,
    SetPosDeleteLabel,
    SetPosDeleteTrash,
    SetPosDeleteTrashBG,
};

enum class GuideSize {
    HomeTalkbox,
    HomeLabel,
    MapSelectTalkbox,
    MapSelectLabel,
    MapPlayTalkbox,
    MapPlayLabel,
    RecordingTalkBox,
    RecordingLabel,
    SetPosMusicTalkBox,
    SetPosMusicLabel,
    SetPosBGTalkBox,
    SetPosBGLabel,
    SetPosCharTalkBox,
    SetPosCharLabel,
    SetPosDeleteTalkBox,
    SetPosDeleteLabel,
    SetPositionTransparentLayer
};

enum class GuideRect {
    HomeTalkBox,
    HomeTalkBoxInset,
    MapSelectTalkBox,
    MapSelectTalkBoxInset,
    MapPlayTalkBox,
    MapPlayTalkBoxInset,
    RecordingTalkBox,
    RecordingTalkBoxInset,
    SetPosMusicTalkBox,//guide_select_talkbox_01.png
    SetPosMusicTalkBoxInset,//guide_select_talkbox_01.png
    SetPosBGTalkBox,//guide_select_talkbox_02.png
    SetPosBGTalkBoxInset,//guide_select_talkbox_02.png
    SetPosCharTalkBox,//guide_select_talkbox_01.png
    SetPosCharTalkBoxInset,//guide_select_talkbox_01.png
    SetPosDeleteTalkBox,
    SetPosDeleteTalkBoxInset,
};

enum class GuideExtra {
	HomeLongSizeExtra,
	MapSelectLongSizeExtra,
};


using namespace extension;

class GuideLayer : public Layer
{
public:
    GuideLayer();
    virtual ~GuideLayer();
//    static GuideLayer* create();
    bool init();
    virtual void onEnter();
    virtual void onExit();
    virtual void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event *event);
    
    void setDelegate(GuideLayerProtocol * pDelegate) { _delegate = pDelegate; }

    static Point GetPosition(GuidePosition position);
    static Size GetSize(GuideSize size);
    static Rect GetRect(GuideRect rect);
    static float GetExtra(GuideExtra extra);
//    float GetDelta(GuideDelta delta);
    LanguageType currentLanguageType;
protected:
    GuideLayerProtocol *_delegate;
    bool _onceMode;
    Layer *_parentLayer;
    LayerColor *_dimLayer;
    LayerColor *_transparentLayer;
    Size _transparentLayerSize;
    int _effectID;
};

NS_CC_END

#endif /* defined(_GUIDELAYER_H_) */
