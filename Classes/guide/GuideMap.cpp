//
//  GuideMap.cpp
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 10..
//
//

#include "GuideMap.h"
#include "BGMManager.h"

USING_NS_CC;

GuideMap::GuideMap(Layer *parentLayer) {
    _parentLayer = parentLayer;
}

GuideMap::~GuideMap() {
    log("GuideMap destructor call");
}

GuideMap* GuideMap::create(Layer *parentLayer, GuideMap::Type type)
{
#ifndef _DEBUG_GUIDE_
    const char *key = "guide_map_select";
    if (type == GuideMap::Type::Play) {
        key = "guide_map_play";
    }
    if (UserDefault::getInstance()->getBoolForKey(key, false) == true) {
        return nullptr;
    }
    UserDefault::getInstance()->setBoolForKey(key, true);
#endif
    GuideMap *layer = new GuideMap(parentLayer);
    if (layer && layer->init(type)) {
        layer->autorelease();
        return layer;
    }
    CC_SAFE_DELETE(layer);
    return nullptr;
}

void GuideMap::onEnter() {
    TSBGMManager::getInstance()->pause();
    GuideLayer::onEnter();
}

void GuideMap::onExit() {
    GuideLayer::onExit();
    TSBGMManager::getInstance()->resume();
}

bool GuideMap::init(GuideMap::Type type)
{
    if ( !GuideLayer::init() ) {
        return false;
    }
    switch (type) {
        case GuideMap::Type::Select: {
        	float extraWidth = GetExtra(GuideExtra::MapSelectLongSizeExtra);

            switch (currentLanguageType) {
                case LanguageType::KOREAN: {
                    _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/ko/03_journey_map.mp3", false);
                    break;
                }
                case LanguageType::JAPANESE: {
                    _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/ja/03_journey_map.mp3", false);
                    break;
                }
                case LanguageType::ENGLISH:
                default: {
                    _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/en/03_journey_map.mp3", false);
                    break;
                }
            }
            Sprite *bobby = Sprite::create("guide_map_bobby.png");
            bobby->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
            bobby->setPosition(GetPosition(GuidePosition::MapSelectBobby));
            this->addChild(bobby, 0);
            
            Sprite *action = Sprite::create("guide_map_select.png");
            action->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
            action->setPosition(GetPosition(GuidePosition::MapSelectAction));
            this->addChild(action);
            
            Scale9Sprite *talkBox = Scale9Sprite::create("guide_map_talkbox.png", GetRect(GuideRect::MapSelectTalkBox), GetRect(GuideRect::MapSelectTalkBoxInset));
            auto bgSize = GetSize(GuideSize::MapSelectTalkbox);
            bgSize.width += extraWidth;
            talkBox->setContentSize(bgSize);
            talkBox->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
            auto bgPos = GetPosition(GuidePosition::MapSelectTalkBox);
            if(extraWidth > 0)
            	bgPos.x -= extraWidth / 2;
            talkBox->setPosition(bgPos);
            this->addChild(talkBox);
            
            
            Label *textLabel = nullptr;
            const char *labelString = Device::getResString(Res::Strings::CC_GUIDE_JOURNEYMAP_NEWPAGE);
            auto labelSize = GetSize(GuideSize::MapSelectLabel);
            labelSize.width += extraWidth;
            switch (currentLanguageType) {
                case LanguageType::KOREAN:
                case LanguageType::ENGLISH: {
                    int delta = 0;
                    if (Application::getInstance()->getTargetPlatform() == Application::Platform::OS_IPAD) {
                        delta = 4;
                    }
                    textLabel = cocos2d::Label::createWithTTF(labelString, FONT_NANUM_PEN, GUIDE_LABEL_NANUM_FONTSIZE-delta, labelSize);
                } break;
                case LanguageType::JAPANESE:
                default: {
                    textLabel = cocos2d::Label::createWithSystemFont(labelString, FONT_NAME, GUIDE_LABEL_NORMAL_FONTSIZE, labelSize);
                    textLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
                } break;
            }
            
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
            textLabel->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::CENTER);
#else
            switch (currentLanguageType) {
                case LanguageType::JAPANESE: {
                    textLabel->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::CENTER);
                } break;
                default: {
                    textLabel->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::TOP);
                } break;
            }
#endif
            textLabel->setColor(Color3B(0xff, 0xff, 0xff));
            
            textLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
            auto labelPos = GetPosition(GuidePosition::MapSelectLabel);
        	if(extraWidth > 0)
        		labelPos.x -= extraWidth / 2;
            textLabel->setPosition(labelPos);//Point(1228/2, 190/2)
            
            //    guideLabel->enableShadow(Color4B(0,0,0,255), LABEL_SHADOW_DEFAULT_SIZE); //LABEL_SHADOW_PLAY_SIZE
            this->addChild(textLabel, 15);
        } break;
        case GuideMap::Type::Play: {
            Sprite *bobby = Sprite::create("guide_photo_edit_bobby.png");
            bobby->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
            bobby->setPosition(GetPosition(GuidePosition::MapPlayBobby));
            this->addChild(bobby, 0);
            
            const char *createImage;
            switch (currentLanguageType) {
                case LanguageType::KOREAN: {
                    createImage = "guide_map_create_ko.png";
                    _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/ko/04_journey_map_all.mp3", false);
                } break;
                case LanguageType::JAPANESE: {
                    createImage = "guide_map_create_jp.png";
                    _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/ja/04_journey_map_all.mp3", false);
                } break;
                case LanguageType::ENGLISH:
                default: {
                    createImage = "guide_map_create.png";
                    _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/en/04_journey_map_all.mp3", false);
                } break;
            }
            Sprite *action = Sprite::create(createImage);
            action->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
            action->setPosition(GetPosition(GuidePosition::MapPlayAction));
            this->addChild(action);
            
            Scale9Sprite *talkBox = Scale9Sprite::create("guide_home_talkbox.png", GetRect(GuideRect::MapPlayTalkBox), GetRect(GuideRect::MapPlayTalkBoxInset));
            talkBox->setContentSize(GetSize(GuideSize::MapPlayTalkbox));
            talkBox->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
            talkBox->setPosition(GetPosition(GuidePosition::MapPlayTalkBox));
            this->addChild(talkBox);
            
            Sprite *talkBoxArrow = Sprite::create("guide_home_talkbox_arrow.png");
            talkBoxArrow->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
            talkBoxArrow->setPosition(GetPosition(GuidePosition::MapPlayTalkBoxArrow));
            this->addChild(talkBoxArrow);

            Label *textLabel = nullptr;
            const char *labelString = Device::getResString(Res::Strings::CC_GUIDE_JOURNEYMAP_ALLPLAY);
            switch (currentLanguageType) {
                case LanguageType::KOREAN:
                case LanguageType::ENGLISH: {
                    textLabel = cocos2d::Label::createWithTTF(labelString, FONT_NANUM_PEN, GUIDE_LABEL_NANUM_FONTSIZE, GetSize(GuideSize::MapPlayLabel));
                } break;
                case LanguageType::JAPANESE:
                default: {
                    textLabel = cocos2d::Label::createWithSystemFont(labelString, FONT_NAME, GUIDE_LABEL_NORMAL_FONTSIZE, GetSize(GuideSize::MapPlayLabel));
                    textLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
                } break;
            }
            
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
            textLabel->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::CENTER);
#else
            switch (currentLanguageType) {
                case LanguageType::JAPANESE: {
                    textLabel->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::CENTER);
                } break;
                default: {
                    textLabel->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::TOP);
                } break;
            }
#endif
            textLabel->setColor(Color3B(0xff, 0xff, 0xff));
            
            textLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
            textLabel->setPosition(GetPosition(GuidePosition::MapPlayLabel));//Point(1228/2, 190/2)
            
            //    guideLabel->enableShadow(Color4B(0,0,0,255), LABEL_SHADOW_DEFAULT_SIZE); //LABEL_SHADOW_PLAY_SIZE
            this->addChild(textLabel, 15);
        } break;
        default:
            break;
    }
    
    return true;
}
