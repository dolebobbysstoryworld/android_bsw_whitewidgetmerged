//
//  GuideSetPosition.cpp
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 11..
//
//

#include "GuideSetPosition.h"

USING_NS_CC;

GuideSetPosition::GuideSetPosition(Layer *parentLayer) {
    _parentLayer = parentLayer;
}

GuideSetPosition::~GuideSetPosition() {
    log("GuideSetPosition destructor call");
}

GuideSetPosition* GuideSetPosition::create(Layer *parentLayer, GuideMode mode)
{
#ifndef _DEBUG_GUIDE_
    __String *key = __String::createWithFormat("showguide_setposition_%d", mode);
    if (UserDefault::getInstance()->getBoolForKey(key->getCString(), false) == true) {
        return nullptr;
    }
    UserDefault::getInstance()->setBoolForKey(key->getCString(), true);
#endif
    GuideSetPosition *layer = new GuideSetPosition(parentLayer);
    if (layer) {
        layer->SetMode(mode);
    }
    if (layer && layer->init()) {
        layer->autorelease();
        return layer;
    }
    CC_SAFE_DELETE(layer);
    return nullptr;
}

void GuideSetPosition::onEnter() {
    GuideLayer::onEnter();
}

void GuideSetPosition::onExit() {
    GuideLayer::onExit();
}

bool GuideSetPosition::init() {
    if ( _guideMode == GuideMode::Music || _guideMode == GuideMode::Background || _guideMode == GuideMode::Character ) {
        _transparentLayerSize = GetSize(GuideSize::SetPositionTransparentLayer);
    }
    if ( !GuideLayer::init() ) {
        return false;
    }
    const char *bobbyFile;
    Point bobbyPosition;
    const char *actionFile;
    Point actionPosition;
    const char *arrowFile;
    Point arrowPosition;
    const char *talkBoxFile;
    Rect talkBoxRect;
    Rect talkBoxInset;
    Size talkBoxSize;
    Point talkBoxPosition;
    const char *labelString;
    Size labelSize;
    Point labelPosition;
    
    float customFontSize = -1;

    switch (_guideMode) {
        case GuideMode::Music: {
            switch (currentLanguageType) {
                case LanguageType::KOREAN: {
                    if (Application::getInstance()->getTargetPlatform() == Application::Platform::OS_IPAD) {
                        customFontSize = GUIDE_LABEL_NANUM_FONTSIZE - 4;
                    }
                    _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/ko/07_select_music.mp3", false);
                    break;
                }
                case LanguageType::JAPANESE: {
                    _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/ja/07_select_music.mp3", false);
                    break;
                }
                case LanguageType::ENGLISH:
                default: {
                    _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/en/07_select_music.mp3", false);
                    break;
                }
            }
            bobbyFile = "guide_select_bobby.png";
            bobbyPosition = GetPosition(GuidePosition::SetPosMusicBobby);
            actionFile = "btn_set_position_music_selected.png";
            actionPosition = GetPosition(GuidePosition::SetPosMusicAction);
            arrowFile = "guide_select_music_arrow.png";
            arrowPosition = GetPosition(GuidePosition::SetPosMusicArrow);
            talkBoxFile = "guide_select_talkbox_01.png";
            talkBoxRect = GetRect(GuideRect::SetPosMusicTalkBox);
            talkBoxInset = GetRect(GuideRect::SetPosMusicTalkBoxInset);
            talkBoxSize = GetSize(GuideSize::SetPosMusicTalkBox);
            talkBoxPosition = GetPosition(GuidePosition::SetPosMusicTalkBox);
            labelString = Device::getResString(Res::Strings::CC_GUIDE_SETPOSITION_SELECT_MUSIC);
            labelSize = GetSize(GuideSize::SetPosMusicLabel);
            labelPosition = GetPosition(GuidePosition::SetPosMusicLabel);
        } break;
        case GuideMode::Background: {
            switch (currentLanguageType) {
                case LanguageType::KOREAN: {
                    _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/ko/06_select_background.mp3", false);
                    break;
                }
                case LanguageType::JAPANESE: {
                    _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/ja/06_select_background.mp3", false);
                    break;
                }
                case LanguageType::ENGLISH:
                default: {
                    _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/en/06_select_background.mp3", false);
                    break;
                }
            }
            bobbyFile = "guide_select_bobby.png";
            bobbyPosition = GetPosition(GuidePosition::SetPosBGBobby);
            actionFile = "btn_home_bg_selected.png";
            actionPosition = GetPosition(GuidePosition::SetPosBGAction);
            arrowFile = "guide_select_bg_arrow.png";
            arrowPosition = GetPosition(GuidePosition::SetPosBGArrow);
            talkBoxFile = "guide_select_talkbox_02.png";
            talkBoxRect = GetRect(GuideRect::SetPosBGTalkBox);
            talkBoxInset = GetRect(GuideRect::SetPosBGTalkBoxInset);
            talkBoxSize = GetSize(GuideSize::SetPosBGTalkBox);
            talkBoxPosition = GetPosition(GuidePosition::SetPosBGTalkBox);
            labelString = Device::getResString(Res::Strings::CC_GUIDE_SETPOSITION_SELECT_BACKGROUND);
            labelSize = GetSize(GuideSize::SetPosBGLabel);
            labelPosition = GetPosition(GuidePosition::SetPosBGLabel);
        } break;
        case GuideMode::Character: {
            switch (currentLanguageType) {
                case LanguageType::KOREAN: {
                    _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/ko/05_select_item.mp3", false);
                    break;
                }
                case LanguageType::JAPANESE: {
                    _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/ja/05_select_item.mp3", false);
                    break;
                }
                case LanguageType::ENGLISH:
                default: {
                    _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/en/05_select_item.mp3", false);
                    break;
                }
            }
            bobbyFile = "guide_select_bobby.png";
            bobbyPosition = GetPosition(GuidePosition::SetPosCharBobby);
            actionFile = "btn_home_item_selected.png";
            actionPosition = GetPosition(GuidePosition::SetPosCharAction);
            arrowFile = "guide_select_bg_arrow.png";
            arrowPosition = GetPosition(GuidePosition::SetPosCharArrow);
            talkBoxFile = "guide_select_talkbox_01.png";
            talkBoxRect = GetRect(GuideRect::SetPosCharTalkBox);
            talkBoxInset = GetRect(GuideRect::SetPosCharTalkBoxInset);
            talkBoxSize = GetSize(GuideSize::SetPosCharTalkBox);
            talkBoxPosition = GetPosition(GuidePosition::SetPosCharTalkBox);
            labelString = Device::getResString(Res::Strings::CC_GUIDE_SETPOSITION_SELECT_ITEM);
            labelSize = GetSize(GuideSize::SetPosCharLabel);
            labelPosition = GetPosition(GuidePosition::SetPosCharLabel);
        } break;
        case GuideMode::Delete: {
            switch (currentLanguageType) {
                case LanguageType::KOREAN: {
                    _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/ko/08_set_position.mp3", false);
                    break;
                }
                case LanguageType::JAPANESE: {
                    _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/ja/08_set_position.mp3", false);
                    break;
                }
                case LanguageType::ENGLISH:
                default: {
                    _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/en/08_set_position.mp3", false);
                    break;
                }
            }
            bobbyFile = "guide_set_position_tap_hold_bobby.png";
            bobbyPosition = GetPosition(GuidePosition::SetPosDeleteBobby);
            actionFile = "guide_set_position_tap_hold.png";
            actionPosition = GetPosition(GuidePosition::SetPosDeleteAction);
            arrowFile = "guide_set_position_tab_hold_arrow.png";
            arrowPosition = GetPosition(GuidePosition::SetPosDeleteArrow);
            talkBoxFile = "guide_set_position_tap_hold_talkbox.png";
            talkBoxRect = GetRect(GuideRect::SetPosDeleteTalkBox);
            talkBoxInset = GetRect(GuideRect::SetPosDeleteTalkBoxInset);
            talkBoxSize = GetSize(GuideSize::SetPosDeleteTalkBox);
            talkBoxPosition = GetPosition(GuidePosition::SetPosDeleteTalkBox);
            labelString = Device::getResString(Res::Strings::CC_GUIDE_SETPOSITION_DELETE_ITEM);
            labelSize = GetSize(GuideSize::SetPosDeleteLabel);
            labelPosition = GetPosition(GuidePosition::SetPosDeleteLabel);
            
            Sprite *trash_bg = Sprite::create("set_position_item_remove_pressed_bg.png");
            trash_bg->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
            trash_bg->setPosition(GetPosition(GuidePosition::SetPosDeleteTrashBG));
            this->addChild(trash_bg, 0);

            Sprite *trash = Sprite::create("set_position_item_remove_pressed_01.png");
            trash->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
            trash->setPosition(GetPosition(GuidePosition::SetPosDeleteTrash));
            this->addChild(trash, 0);
        } break;
        default: { } break;
    }

    Sprite *bobby = Sprite::create(bobbyFile);
    bobby->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    bobby->setPosition(bobbyPosition);
    this->addChild(bobby, 0);
    
    Sprite *action = Sprite::create(actionFile);
    action->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    action->setPosition(actionPosition);
    this->addChild(action);

    
    Sprite *arrow = Sprite::create(arrowFile);
    arrow->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    arrow->setPosition(arrowPosition);
    this->addChild(arrow);

    Scale9Sprite *talkBox = Scale9Sprite::create(talkBoxFile, talkBoxRect, talkBoxInset);
    talkBox->setContentSize(talkBoxSize);
    talkBox->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    talkBox->setPosition(talkBoxPosition);
    this->addChild(talkBox);
    
    
    Label *textLabel = nullptr;
    
    switch (currentLanguageType) {
        case LanguageType::KOREAN: {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
            textLabel = cocos2d::Label::createWithTTF(labelString, FONT_NANUM_PEN, GUIDE_LABEL_NANUM_FONTSIZE, labelSize);
#else
            float fontSize = GUIDE_LABEL_NANUM_FONTSIZE;
            if (customFontSize != -1) {
                fontSize = customFontSize;
            }
            textLabel = cocos2d::Label::createWithTTF(labelString, FONT_NANUM_PEN, fontSize, labelSize);
//            float width = Director::getInstance()->getOpenGLView()->getFrameSize().width;
//            if ((width == 1024 || width == 2048) && IsIOS() == true) {
//                bobby->setPosition(cocos2d::Point(bobbyPosition.x - 40, bobbyPosition.y));
//                textLabel = cocos2d::Label::createWithTTF(labelString, FONT_NANUM_PEN, fontSize, cocos2d::Size(labelSize.width+10, labelSize.height));
//            } else {
//                textLabel = cocos2d::Label::createWithTTF(labelString, FONT_NANUM_PEN, fontSize, labelSize);
//            }
//            
#endif
        } break;
        case LanguageType::ENGLISH: {
            textLabel = cocos2d::Label::createWithTTF(labelString, FONT_NANUM_PEN, GUIDE_LABEL_NANUM_FONTSIZE, labelSize);
        } break;
        case LanguageType::JAPANESE:
        default: {
            textLabel = cocos2d::Label::createWithSystemFont(labelString, FONT_NAME, GUIDE_LABEL_NORMAL_FONTSIZE, labelSize);
            textLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
        } break;
    }
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    textLabel->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::CENTER);
#else
    switch (currentLanguageType) {
        case LanguageType::JAPANESE: {
            textLabel->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::CENTER);
        } break;
        default: {
            textLabel->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::TOP);
        } break;
    }
#endif
    textLabel->setColor(Color3B(0xff, 0xff, 0xff));
    
    textLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    textLabel->setPosition(labelPosition);
    
//    guideLabel->enableShadow(Color4B(0,0,0,255), LABEL_SHADOW_DEFAULT_SIZE); //LABEL_SHADOW_PLAY_SIZE
    this->addChild(textLabel, 15);

    return true;
}