//
//  GuideMap.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 10..
//
//

#ifndef _GUIDEMAP_H_
#define _GUIDEMAP_H_

#include "GuideLayer.h"

NS_CC_BEGIN

class GuideMap : public GuideLayer
{
public:
    enum class Type {
        Select,
        Play
    };
    
    GuideMap(Layer *parentLayer);
    virtual ~GuideMap();
    static GuideMap* create(Layer *parentLayer, GuideMap::Type type);
    bool init(GuideMap::Type type);
    virtual void onEnter();
    virtual void onExit();
    
protected:
};

NS_CC_END

#endif /* defined(_GUIDEMAP_H_) */
