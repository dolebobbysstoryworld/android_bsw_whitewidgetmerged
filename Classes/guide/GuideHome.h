//
//  GuideHome.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 3..
//
//

#ifndef _GUIDEHOME_H_
#define _GUIDEHOME_H_

#include "GuideLayer.h"

NS_CC_BEGIN

class GuideHome : public GuideLayer
{
public:
    GuideHome(Layer *parentLayer);
    virtual ~GuideHome();
    static GuideHome* create(Layer *parentLayer);
    bool init();
    virtual void onEnter();
    virtual void onExit();
    
protected:

};

NS_CC_END


#endif /* defined(_GUIDEHOME_H_) */
