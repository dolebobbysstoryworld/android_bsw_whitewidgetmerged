//
//  GuideRecoding.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 10..
//
//

#ifndef _GUIDERECODING_H_
#define _GUIDERECODING_H_

#include "GuideLayer.h"

NS_CC_BEGIN

class GuideRecoding : public GuideLayer
{
public:
    GuideRecoding(Layer *parentLayer);
    virtual ~GuideRecoding();
    static GuideRecoding* create(Layer *parentLayer);
    bool init();
    virtual void onEnter();
    virtual void onExit();
    
protected:
    
};

NS_CC_END

#endif /* defined(_GUIDERECODING_H_) */
