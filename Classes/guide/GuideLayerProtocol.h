//
//  GuideLayerProtocol.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 03..
//
//

#ifndef _GUIDELAYERPROTOCOL_H_
#define _GUIDELAYERPROTOCOL_H_

#include "cocos2d.h"
#include "ResourceManager.h"

class GuideLayerProtocol
{
public:
    virtual ~GuideLayerProtocol() {}
    
    virtual void didEnterGuideLayer() = 0;
    virtual void didExitGuideLayer() = 0;
};

static inline bool IsIOS() {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    return true;
#else
    return false;
#endif
}

static inline bool IsAndroid() {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    return true;
#else
    return false;
#endif
}

#endif ///_GUIDELAYERPROTOCOL_H_
