//
//  GuideHome.cpp
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 3..
//
//

#include "GuideHome.h"
#include "BGMManager.h"

USING_NS_CC;


GuideHome::GuideHome(Layer *parentLayer) {
    _parentLayer = parentLayer;
}

GuideHome::~GuideHome() {
    log("GuideHome destructor call");
}

GuideHome* GuideHome::create(Layer *parentLayer)
{
#ifndef _DEBUG_GUIDE_
    const char *key = "showguide_home";
    if (UserDefault::getInstance()->getBoolForKey(key, false) == true) {
        return nullptr;
    }
    UserDefault::getInstance()->setBoolForKey(key, true);
#endif
    GuideHome *layer = new GuideHome(parentLayer);
    if (layer && layer->init()) {
        layer->autorelease();
        return layer;
    }
    CC_SAFE_DELETE(layer);
    return nullptr;
}

void GuideHome::onEnter() {
    TSBGMManager::getInstance()->pause();
    GuideLayer::onEnter();
}

void GuideHome::onExit() {
    GuideLayer::onExit();
    TSBGMManager::getInstance()->resume();
}

bool GuideHome::init()
{
    if ( !GuideLayer::init() ) {
        return false;
    }
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    float extraWidth = GetExtra(GuideExtra::HomeLongSizeExtra);
#else
    float extraWidth = 0;
    switch (currentLanguageType) {
        case LanguageType::JAPANESE: {
            extraWidth = GetExtra(GuideExtra::HomeLongSizeExtra);
        } break;
        default: {
            extraWidth = 0;
        } break;
    }
#endif

    switch (currentLanguageType) {
        case LanguageType::KOREAN: {
            _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/ko/02_home.mp3", false);
            break;
        }
        case LanguageType::JAPANESE: {
            _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/ja/02_home.mp3", false);
            break;
        }
        case LanguageType::ENGLISH:
        default: {
            _effectID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("guide_sounds/en/02_home.mp3", false);
            break;
        }
    }
    
    Sprite *bobby = Sprite::create("guide_home_bobby.png");
    bobby->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    bobby->setPosition(GetPosition(GuidePosition::HomeBobby));
    this->addChild(bobby, 0);
    
    Sprite *action = Sprite::create("guide_home_create.png");
    action->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    action->setPosition(GetPosition(GuidePosition::HomeAction));
    this->addChild(action);
    
    Scale9Sprite *talkBox = Scale9Sprite::create("guide_home_talkbox.png", GetRect(GuideRect::HomeTalkBox), GetRect(GuideRect::HomeTalkBoxInset));
    auto bgSize = GetSize(GuideSize::HomeTalkbox);
    bgSize.width += extraWidth;
    talkBox->setContentSize(bgSize);
    talkBox->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    auto bgPos = GetPosition(GuidePosition::HomeTalkBox);
    if(extraWidth > 0)
    	bgPos.x -= extraWidth / 2;
    talkBox->setPosition(bgPos);
    this->addChild(talkBox);

    Sprite *talkBoxArrow = Sprite::create("guide_home_talkbox_arrow.png");
    talkBoxArrow->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    talkBoxArrow->setPosition(GetPosition(GuidePosition::HomeTalkBoxArrow));
    this->addChild(talkBoxArrow);

    Label *textLabel = nullptr;
    const char *labelString = Device::getResString(Res::Strings::CC_GUIDE_HOME);
    auto labelSize = GetSize(GuideSize::HomeLabel);
    labelSize.width += extraWidth;
    switch (currentLanguageType) {
        case LanguageType::KOREAN:
        case LanguageType::ENGLISH: {
            textLabel = cocos2d::Label::createWithTTF(labelString, FONT_NANUM_PEN, GUIDE_LABEL_NANUM_FONTSIZE, labelSize);
        } break;
        case LanguageType::JAPANESE:
        default: {
            textLabel = cocos2d::Label::createWithSystemFont(labelString, FONT_NAME, GUIDE_LABEL_NORMAL_FONTSIZE, labelSize);
            textLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
        } break;
    }
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    textLabel->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::CENTER);
#else
    switch (currentLanguageType) {
        case LanguageType::JAPANESE: {
            textLabel->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::CENTER);
        } break;
        default: {
            textLabel->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::TOP);
        } break;
    }
#endif
    textLabel->setColor(Color3B(0xff, 0xff, 0xff));

    textLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    auto labelPos = GetPosition(GuidePosition::HomeLabel);
	if(extraWidth > 0)
		labelPos.x -= extraWidth / 2;
    textLabel->setPosition(labelPos);//Point(1228/2, 190/2)
    
//    guideLabel->enableShadow(Color4B(0,0,0,255), LABEL_SHADOW_DEFAULT_SIZE); //LABEL_SHADOW_PLAY_SIZE
    this->addChild(textLabel, 15);

    return true;
}
