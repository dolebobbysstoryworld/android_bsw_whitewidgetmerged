//
//  GuideSetPosition.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 11..
//
//

#ifndef _GUIDESETPOSITION_H_
#define _GUIDESETPOSITION_H_

#include "GuideLayer.h"

NS_CC_BEGIN

class GuideSetPosition : public GuideLayer
{
public:
    enum class GuideMode {
        Music,
        Background,
        Character,
        Delete
    };

    GuideSetPosition(Layer *parentLayer);
    virtual ~GuideSetPosition();
    static GuideSetPosition* create(Layer *parentLayer, GuideMode mode);
    bool init();
    virtual void onEnter();
    virtual void onExit();
    void SetMode(GuideMode mode) { _guideMode = mode; };
    
protected:
    GuideMode _guideMode;
};

NS_CC_END

#endif /* defined(_GUIDESETPOSITION_H_) */
