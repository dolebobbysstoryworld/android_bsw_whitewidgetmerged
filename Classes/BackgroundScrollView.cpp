//
//  BackgroundScrollView.cpp
//  bobby
//
//  Created by Dongwook, Kim on 14. 7. 11..
//
//

#include "BackgroundScrollView.h"
#include "ResourceManager.h"
#include "Define.h"
#include "Util.h"

#include "external/json/document.h"
#include "external/json/prettywriter.h"
#include "external/json/stringbuffer.h"

#define BACKGROUND_BASE_TAG                  20000
#define LINE_BASE_TAG                       30000

#define TAG_FRAME_BG            300
#define TAG_BG_SELECT_COIN      400

#define BACKGROUND_ANCHOR_Y                  0.95
#define DELETE_BG_POPUP           0

#define MAX_ROTATION        5.0f

#define WHITESPACE_CHAR_DEFAULT     1

bool BackgroundScrollView::init(bg_super_type superViewType)
{
    if (Layer::init()) {
        superType = superViewType;
        linesCount = 0;
        
        currentBackgroundAngle = 0.0f;
        scrollViewPressed = false;
        reachedToEnd = false;
        
        backgroundList = __Array::create();
        backgroundList->retain();
        
        deleteButtons = __Array::create();
        deleteButtons->retain();
        
        items = __Dictionary::create();
        items->retain();
        
        pins = __Array::create();
        pins->retain();
        
        isScrollingToFirst = false;
        
        this->scrollView = this->createScrollView();
        this->loadJsonToScrollView(this->scrollView);
        
        this->setAnchorPoint(this->scrollView->getAnchorPoint());
        this->setContentSize(this->scrollView->getViewSize());
        this->setPosition(this->scrollView->getPosition());
        this->addChild(this->scrollView, 0);
        
        if (superType == bg_super_type::BG_RECORD){
            this->setSelectBackground(0);
        }
        
        scrollOffset = cocos2d::Point::ZERO;
        return true;
    }
    return false;
}

BackgroundScrollView* BackgroundScrollView::create(bg_super_type superViewType)
{
    BackgroundScrollView *pRet = new BackgroundScrollView();
    if (pRet && pRet->init(superViewType))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

ScrollViewExtension *BackgroundScrollView::createScrollView()
{
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    
    ScrollViewExtension *newScrollView = ScrollViewExtension::create(cocos2d::Size(visibleSize.width, HOME_BACKGROUND_SCROLL_VIEW_HEIGHT), nullptr);
    newScrollView->setBounceable(false);
    newScrollView->setAnchorPoint(cocos2d::Point(0, 0));
    newScrollView->setTag(TAG_SCROLLVIEW);
    newScrollView->setExtensionDelegate(this);
    newScrollView->setBounceable(true);
    
    return newScrollView;
}

void BackgroundScrollView::bgCoinClicked(Ref* pSender)
{
    log("coin clicked");
}

void BackgroundScrollView::rotateBackgrounds(float duration, float angle, bool animation)
{
    auto container = scrollView->getChildren().at(0);
    auto backgroundCount = backgroundList->count();
    currentBackgroundAngle = angle;

    for (int index = 0; index < backgroundCount ; index++) {
        Sprite *frame = (Sprite *)(container->getChildByTag(BACKGROUND_BASE_TAG+index));
        
        if (frame == NULL) continue;

         changePositionAtAnchor(frame, cocos2d::Point(0.5, BACKGROUND_ANCHOR_Y));
        
        if (animation) {
            auto rotateAction = RotateTo::create(duration, angle);
            auto rotateReverseAction = RotateTo::create(duration, 0);
            auto frameSequence = Sequence::create(rotateAction, rotateReverseAction, NULL);
            frame->runAction(frameSequence->clone());
        } else {
            frame->setRotation(angle);
        }
    }
}

void BackgroundScrollView::scrollViewDidScroll(ScrollViewExtension* view)
{
    auto oldOffset = scrollOffset.x;
    scrollOffset = view->getContentOffset();

    float rotationDuration = 0.1f;
    int direction = 0;
    
    if ( view->getContentOffset().x == view->minContainerOffset().x ){
        if (oldOffset == view->minContainerOffset().x){
            if (view->isDragging()){
                if (!reachedToEnd) {
                    reachedToEnd = true;
                    return;
                }
                direction = -1;
            } else {
                reachedToEnd = false;
            }
        }
    } else if (view->getContentOffset().x == view->maxContainerOffset().x) {
        if (oldOffset == view->maxContainerOffset().x){
            if (view->isDragging()){
                if (!reachedToEnd) {
                    reachedToEnd = true;
                    return;
                }
                direction = 1;
            } else {
                reachedToEnd = false;
            }
        }
        isScrollingToFirst = false;
    } else {
        if((oldOffset-view->getContentOffset().x) < 0) {
            direction = 1;
        } else {
            direction = -1;
        }
    }
    
    if (view->isDragging() && !scrollViewPressed) {
        scrollViewPressed = true;
        currentBackgroundAngle = 0; // clear old angle
    }
    
    float nextAngle = currentBackgroundAngle;
    if (direction == 1) {
        if (currentBackgroundAngle < MAX_ROTATION) {
            nextAngle = currentBackgroundAngle + 0.5f;
        }
    } else {
        if (currentBackgroundAngle > -MAX_ROTATION) {
            nextAngle = currentBackgroundAngle - 0.5f;
        }
    }

    if (view->isDragging()){
        this->rotateBackgrounds(rotationDuration, nextAngle, false);
    } else {
        this->rotateBackgrounds(rotationDuration, nextAngle, true);
        scrollViewPressed = false;
    }
}

void BackgroundScrollView::scrollViewDidZoom(ScrollViewExtension* view)
{
    
}

void BackgroundScrollView::scrollViewDidScrollEnd(ScrollViewExtension* view)
{
    isScrollingToFirst = false;
}

void BackgroundScrollView::popupButtonClicked(PopupLayer *popup, cocos2d::Ref *obj)
{
    Director::getInstance()->resume();
    auto clickedButton = (MenuItem*)obj;
    int tag = clickedButton->getTag();
    
    if (popup->getTag() == DELETE_BG_POPUP) {
        if (tag == 0) {
            log("delete cancelButton clicked!!");
        } else {
            scrollView->isAnimate = true;
            this->setEnableDeleteButtons(false);
            log("delete okButton clicked!!");
            float scaleDuration = 0.2f;
            float shrinkRatio = 0.97;
            auto scaleAction = ScaleTo::create(scaleDuration, shrinkRatio);
            auto reverse = ScaleTo::create(scaleDuration, 1.0f);
            
            auto scaleCallback = CallFunc::create([&](){
                log("selectedCharKey popup :%s", selectedBgKey->getCString());
                this->runRemoveBackgroundAnimation(selectedBgKey);
                auto runningScene = Director::getInstance()->getRunningScene();
                if (runningScene->getTag() == TAG_RECORDING_SCENE) {
                    __NotificationCenter::getInstance()->postNotification("deleteBgItemInRecordScene");
                }
            });
            auto scaleSequence = Sequence::create(scaleAction, reverse, scaleCallback, NULL);
            selDeleteBg->runAction(scaleSequence);
        }
    } else {
        if (tag == 0) {
            log("cancelButton clicked!");
        } else {
            log("okButton clicked!!");
        }
    }
}

void BackgroundScrollView::onSelectItem(ScrollViewExtension* view, Node* selectedItem, Touch *pTouch, Event *pEvent)
{
    log("onselectitem");
    auto background = (Sprite *)selectedItem;
    log("getTag : %i", background->getTag());
    float scaleDuration = 0.05f;
    float shrinkRatio = 0.97;
    auto scaleAction = ScaleTo::create(scaleDuration, shrinkRatio);
    auto reverse = ScaleTo::create(scaleDuration, 1.0f);
    auto afterCallBack = CallFuncN::create(CC_CALLBACK_1(BackgroundScrollView::bgPressedCallback, this));
    auto actions = Sequence::create(scaleAction, reverse, afterCallBack, NULL);
    background->runAction(actions);
}

void BackgroundScrollView::bgPressedCallback(Ref* pSender)
{
    Sprite *bg = (Sprite *)pSender;
    auto index = bg->getTag() % BACKGROUND_BASE_TAG;
    log("bg pressed : %d", index);
    auto bgKey = (__String*)backgroundList->getObjectAtIndex(index);
    if (superType == bg_super_type::BG_RECORD){
        this->setSelectBackground(index);
    }
    
    if (_scrollViewDelegate) {
        _scrollViewDelegate->onSelectBackground(this, bg, bgKey, index);
    }
}

void BackgroundScrollView::setSelectBackground(int index)
{
    if (frameSelectedBorder == NULL) {
        frameSelectedBorder = Sprite::create("background_frame_select.png");
        frameSelectedBorder->retain();

        frameSelectedBorder->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        frameSelectedBorder->setPosition(cocos2d::Point::ZERO);
        
        auto frameCheck = Sprite::create("background_frame_check.png");
        frameCheck->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        frameCheck->setPosition(cocos2d::Point((frameSelectedBorder->getContentSize().width - frameCheck->getContentSize().width)/2,
                                               RECORDING_BOTTOM_SCROLL_VIEW_CENTER_ICON_BOTTOM_PADDING));
        frameSelectedBorder->addChild(frameCheck);
    } else {
        frameSelectedBorder->removeFromParentAndCleanup(false);
    }
    
    auto targetFrame = scrollView->getContainer()->getChildByTag(BACKGROUND_BASE_TAG + index);
    auto frameBG = targetFrame->getChildByTag(TAG_FRAME_BG);
    frameBG->addChild(frameSelectedBorder, 1);
}

int BackgroundScrollView::getBackgroundIndex(std::string& bgKey)
{
    int result = CC_INVALID_INDEX;
    for (int index = 0; index <backgroundList->count(); index++){
        auto bKey = (__String *)backgroundList->getObjectAtIndex(index);
        if (bgKey.compare(bKey->getCString())== 0) {
            result = index;
            break;
        }
    }
    return result;
}

cocos2d::Node* BackgroundScrollView::createBackgroundForScrollView(std::string bgKey, int index)
{
    auto ud = UserDefault::getInstance();
    auto backgroundJsonString = ud->getStringForKey(bgKey.c_str());
    rapidjson::Document background;
    background.Parse<0>(backgroundJsonString.c_str());
    
    //select background
//        auto selectFrame = Sprite::create("background_frame_select.png");
//        selectFrame->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//        selectFrame->setPosition(cocos2d::Point::ZERO);
//        frameBg->addChild(selectFrame);
//
//        auto selectCheck = Sprite::create("background_frame_check.png");
//        selectCheck->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
//        selectCheck->setPosition(cocos2d::Point(selectFrame->getContentSize().width/2, selectFrame->getContentSize().height/2));
//        frameBg->addChild(selectCheck);
    
    Sprite *frame = Sprite::create();
    if (!background["custom"].GetBool()){
        
        auto backgroundFileName = background["filename"].GetString();
        
        auto frameBg = Sprite::create(backgroundFileName);
        frameBg->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        frameBg->setPosition(cocos2d::Point::ZERO);
        frameBg->setTag(TAG_FRAME_BG);
        
        auto frameContentsSize = frameBg->getContentSize();
        frame->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        frame->setContentSize(frameContentsSize);
        
        float bottomPadding = 0;
        if((index%2 == 1)) {
            bottomPadding = RECORDING_BOTTOM_SCROLL_VIEW_FRAME_BOTTOM_PADDING;
        }

        cocos2d::Point currentPoint = cocos2d::Point(HOME_BACKGROUND_SCROLL_VIEW_STARTPADDING + frameContentsSize.width*index, bottomPadding);
        frame->setPosition(currentPoint);
        frame->addChild(frameBg);
        
        auto backgroundKey = bgKey.c_str();
        
        auto backgroundKeyStr = __String::create(backgroundKey);
//        log("charKey  - %s", backgroundKeyStr->getCString());
        auto backgroundKeyClone = backgroundKeyStr->clone();
        backgroundKeyClone->retain();
        frame->setUserData(backgroundKeyClone);
        backgroundList->addObject(__String::create(backgroundKey));
        
        bool isLock = background["locked"].GetBool();

        if (isLock) {
            int price = background["price"].GetInt();
            auto lock = Sprite::create("background_frame_lock.png");
            lock->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
            lock->setPosition(cocos2d::Point(frameBg->getContentSize().width/2-lock->getContentSize().width/2, RECORDING_BOTTOM_SCROLL_VIEW_CENTER_ICON_BOTTOM_PADDING));
            frameBg->addChild(lock, 1);
            
            auto coinBtn = cocos2d::ui::Button::create("background_frame_dolecoin.png", "background_frame_dolecoin.png");
            coinBtn->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
            coinBtn->setPosition(RECORDING_BOTTOM_SCROLL_VIEW_COIN);
//            coinBtn->addTouchEventListener(this, (cocos2d::ui::SEL_TouchEvent)(&BackgroundScrollView::bgCoinClicked));
            frameBg->addChild(coinBtn, 2);
            
            std::stringstream ss;
            ss << price;
            
            auto coinText = ss.str();
            auto coinFontSize = coinText.length() == 1 ? RECORDING_BOTTOM_SCROLL_VIEW_COIN_FONT_SIZE_DIGIT1 :
            coinText.length() == 2 ? RECORDING_BOTTOM_SCROLL_VIEW_COIN_FONT_SIZE_DIGIT2 : RECORDING_BOTTOM_SCROLL_VIEW_COIN_FONT_SIZE_DIGIT3;
            auto coinNum = Label::createWithSystemFont(coinText, FONT_NAME_BOLD, coinFontSize);
            coinNum->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::CENTER);
            coinNum->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
            coinNum->setColor(Color3B(0xff,0x54,0x11));
            coinNum->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
            coinNum->enableShadow(Color4B(255,255,255,255.0f*0.51f),cocos2d::Size(1.0,-0.866), 0);
            //        coinNum->setPosition(HOME_BACKGROUND_COIN_TEXT);
            coinNum->setPosition(cocos2d::Point(coinBtn->getContentSize().width/2,
                                                coinBtn->getContentSize().height - RECORDING_BOTTOM_SCROLL_VIEW_COIN_VISIBLE_HEIGHT+RECORDING_BOTTOM_SCROLL_VIEW_COIN_VISIBLE_HEIGHT/2));
            coinBtn->addChild(coinNum);
        } else {
//            log("backgroundkey  - %s", backgroundKeyStr->getCString());
            bool free = background["free"].GetBool();
            if (!free) {
                const char *registerTimeChar;
                if (background.HasMember("expiration_date"))
                {
                    registerTimeChar = background["expiration_date"].GetString();
//                    log("backgroundscroll - registerTime : %s", registerTimeChar);
                }
                
                if (background.HasMember("expired_hour")) {                  
                    long leftTime = Util::getItemLeftTime((const char*)registerTimeChar ,background["expired_hour"].GetInt());
                    int day = (leftTime/60/60)/24;
                    int hour = leftTime/60/60;
                    int minute = leftTime/60;
                    
                    std::string bgfileName;
                    std::string leftTimeStr;
                    Color3B numcolor;
                    if (day >= 1) {
                        if (hour == 23 && minute > 30) hour = 24;
                        
                        std::stringstream daystream;
                        daystream << day;
                        std::string dayStr = daystream.str();
                        leftTimeStr = dayStr;
                        
                        std::string fileName("background_frame_limited_days.png");
                        bgfileName = fileName;
                        
                        numcolor = Color3B(0x03, 0x3e, 0xa1);
                    } else {
                        if (hour < 30) hour = 1;
                        
                        std::stringstream hourtream;
                        hourtream << hour;
                        std::string hourStr = hourtream.str();
                        leftTimeStr = hourStr;
                        
                        std::string fileName("background_frame_limited_hours.png");
                        bgfileName = fileName;
                        
                        numcolor = Color3B(0xff, 0x00, 0x00);
                    }
                    
                    float timeFontSize = (leftTimeStr.length() > 1)?HOME_FRAME_LIMIT_FONTSIZE_01:HOME_FRAME_LIMIT_FONTSIZE_02;
                    
                    auto limitBackground = Sprite::create(bgfileName);
                    limitBackground->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                    limitBackground->setPosition(RECORDING_BOTTOM_SCROLL_VIEW_COIN);
                    frameBg->addChild(limitBackground, 2);
                    
                    auto timeLabel = Label::createWithSystemFont(leftTimeStr, FONT_NAME_BOLD, timeFontSize,HOME_FRAME_LIMIT_LABEL_SIZE,TextHAlignment::CENTER, TextVAlignment::CENTER);
                    timeLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                    timeLabel->setColor(numcolor);
                    timeLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
                    timeLabel->enableShadow(Color4B(255,255,255,255.0f*0.51f),cocos2d::Size(1.0,-0.866), 0);
                    timeLabel->setPosition(HOME_FRAME_LIMIT_LABEL);
                    limitBackground->addChild(timeLabel);
                }
                
            }//free
        }
    } else {
        auto thumbnail = (char *)background["thumbnail_photo_file"].GetString();
        auto thumbnailImage = __String::create(thumbnail);
        
        if (thumbnailImage != nullptr) {
            std::string photoType("bg");
            std::string path = Util::getThumbnailPhotoPath(photoType);
            path.append(thumbnailImage->getCString());
            log("path : %s", path.c_str());
            if (cocos2d::FileUtils::getInstance()->isFileExist(path) ){
                cocos2d::Data imageData = cocos2d::FileUtils::getInstance()->getDataFromFile(path.c_str());
                unsigned long nSize = imageData.getSize();
                
                cocos2d::Image *image = new cocos2d::Image();
                image->initWithImageData(imageData.getBytes(), nSize);
                
                cocos2d::Texture2D *texture = new cocos2d::Texture2D();
                texture->initWithImage(image);
                
                cocos2d::Point imageStartPos = cocos2d::Point(0,0);
                
                auto customPhoto = Sprite::createWithTexture(texture);
                customPhoto->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                customPhoto->setPosition(cocos2d::Point(HOME_BOTTOM_SCROLL_VIEW_FRAME_SIZE.width/2-HOME_CUSTOM_BG_SIZE.width/2, HOME_CUSTOM_BG_BOTTOM_PADDING));
                
                auto frameBg = Sprite::create("home_background_frame_custom.png");
                frameBg->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                frameBg->setPosition(cocos2d::Point::ZERO);
                frameBg->setTag(TAG_FRAME_BG);
                
                frameBg->addChild(customPhoto);
                
                auto frameContentsSize = frameBg->getContentSize();
                frame->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                frame->setContentSize(frameContentsSize);
                
                float bottomPadding = 0;
                if((index%2 == 1)) {
                    bottomPadding = RECORDING_BOTTOM_SCROLL_VIEW_FRAME_BOTTOM_PADDING;
                }
                
                cocos2d::Point currentPoint = cocos2d::Point(HOME_BACKGROUND_SCROLL_VIEW_STARTPADDING + frameContentsSize.width*index, bottomPadding);
                frame->setPosition(currentPoint);
                frame->addChild(frameBg);
                
                auto backgroundKey = bgKey.c_str();
                
                auto backgroundKeyStr = __String::create(backgroundKey);
//                log("charKey  - %s", backgroundKeyStr->getCString());
                auto backgroundKeyClone = backgroundKeyStr->clone();
                backgroundKeyClone->retain();
                frame->setUserData(backgroundKeyClone);
                backgroundList->addObject(__String::create(backgroundKey));
            } else {
                log("error! no file : %s", path.c_str());
            }
        }
        
    }
    
    return frame;
}

void BackgroundScrollView::deleteButtonClicked(Ref *obj, ui::TouchEventType type)
{
    switch (type) {
        case ui::TOUCH_EVENT_ENDED: {
            auto selDelButton = ((ui::Button*)obj);
            selectedBgKey = (__String *)selDelButton->getUserData();
            auto holderNode = (Node *)selDelButton->getParent();
            selDeleteBg = holderNode;
            
            Director::getInstance()->pause();
            PopupLayer *modal = PopupLayer::create(POPUP_TYPE_WARNING,
                                                   Device::getResString(Res::Strings::CC_SETPOSTION_POPUP_WARNING_DELETE_BACKGROUND),//"Selected background will be deleted. Deleted records can't be restored.",
                                                   Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL),//"Cancel",
                                                   Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK));//"OK");
            
            modal->setTag(DELETE_BG_POPUP);
            modal->setDelegate(this);
            auto runningScene = Director::getInstance()->getRunningScene();
            runningScene->addChild(modal, 1000);
            //            auto btn = (ui::Button*)obj;
            //            auto deleteCharIndex = btn->getTag();
            //            auto delCharKey = (__String*)characterList->getObjectAtIndex(deleteCharIndex);
            //            log("delete button clicked tag : %i charKey : %s", deleteCharIndex, delCharKey->getCString());
            ////            characterList->removeObjectAtIndex(btnTag);
            //
            //            runRemoveCharacterAnimation(delCharKey);
            //
            //            this->adjustLine();
        } break;
        case ui::TOUCH_EVENT_BEGAN: break;
        case ui::TOUCH_EVENT_MOVED: break;
        case ui::TOUCH_EVENT_CANCELED: break;
        default: break;
    }
}

bool BackgroundScrollView::addBackground(std::string bgKey)
{
    log("add bg %s", bgKey.c_str());
    int index = backgroundList->count();
    auto item = createBackgroundForScrollView(bgKey, index);
    
    auto deleteButton = cocos2d::ui::Button::create("home_create_delete_btn_normal.png", "home_create_delete_btn_press.png");
    deleteButton->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    deleteButton->setPosition(cocos2d::Point(HOME_BOTTOM_SCROLL_VIEW_FRAME_SIZE.width - (HOME_BOTTOM_SCROLL_VIEW_FRAME_SIZE.width-HOME_CUSTOM_BG_SIZE.width)/2 -deleteButton->getContentSize().width/2, HOME_CUSTOM_BG_BOTTOM_PADDING-deleteButton->getContentSize().height/2));
    deleteButton->addTouchEventListener(this, (cocos2d::ui::SEL_TouchEvent)(&BackgroundScrollView::deleteButtonClicked));
    deleteButton->setUserData(backgroundList->getObjectAtIndex(index));
    deleteButtons->addObject(deleteButton);
    
    scrollView->addChild(item, TOUCHABLE_LAYER, BACKGROUND_BASE_TAG+index);
    item->setScale(0.0f);
    
    auto spriteName = __String::createWithFormat("home_background_frame_pin_%02d.png", index%4+1);
    auto pin = Sprite::create(spriteName->getCString());
    pin->setAnchorPoint(cocos2d::Point::ANCHOR_TOP_LEFT);
    cocos2d::Point pos = cocos2d::Point(item->getPositionX() + (HOME_BOTTOM_SCROLL_VIEW_FRAME_SIZE.width - pin->getContentSize().width)/2,
                                        HOME_BOTTOM_SCROLL_VIEW_FRAME_SIZE.height + item->getPositionY() + HOME_BOTTOM_SCROLL_VIEW_PIN_TOP_PADDING);
//    cocos2d::Point pos = cocos2d::Point(item->getPositionX() - (pin->getContentSize().width/2),
//                                        item->getPositionY() + HOME_BOTTOM_SCROLL_VIEW_CHARACTER_PIN_TOP_PADDING);
    pin->setPosition(pos);
    scrollView->addChild(pin, TOUCHABLE_LAYER+1);
    
    __String *bgKeyClone = __String::create(bgKey)->clone();
    bgKeyClone->retain();
    
    log("backgroundList : %zi", backgroundList->count());
    items->setObject(item, bgKeyClone->getCString());
    log("items :%i", items->count());
    pins->addObject(pin);
    
    float moveDuration = 0.25f;
    this->scrollView->setTouchEnabled(false);

    auto newCharacterCallback = CallFunc::create([=](){
        this->scrollView->setTouchEnabled(true);
        auto fadeIn = FadeIn::create(1.0f);
        deleteButton->setOpacity(0.0f);
        item->addChild(deleteButton, 2);
        deleteButton->runAction(fadeIn);
        
        if (superType == BG_RECORD) {
            if (_scrollViewDelegate) {
                _scrollViewDelegate->onSelectBackground(this, item, bgKeyClone, index);
            }
        }
        
    });
    
    // scale animation
    auto startDelay = DelayTime::create(moveDuration);
    auto scaleTo = ScaleTo::create(moveDuration, 1.0f);
    auto appearAction = Sequence::create(startDelay, scaleTo, newCharacterCallback, NULL);
    item->runAction(appearAction);
    
    this->adjustLine();
    
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    cocos2d::Point scrollOffset = scrollView->getContentOffset();
    this->scrollView->setContentOffsetInDuration(cocos2d::Point(-(scrollView->getContentSize().width-visibleSize.width), scrollOffset.y), 0.25f);
    
    return true;
}

bool BackgroundScrollView::removeBackground(__String *bgKey)
{
    log("bgKey : %s", bgKey->getCString());
    auto ud = UserDefault::getInstance();
    
//  remain background data
//    ud->setStringForKey(bgKey->getCString(), "");
    
    auto orderJsonString = ud->getStringForKey(ORDER_KEY, "");
    rapidjson::Document orderDocument;
    orderDocument.Parse<0>(orderJsonString.c_str());
    rapidjson::Value& itemOrder = orderDocument["background_order"];
    rapidjson::Value newOrder(rapidjson::kArrayType);
    rapidjson::Document::AllocatorType& allocator = orderDocument.GetAllocator();
    
    for (rapidjson::SizeType i = 0; i < itemOrder.Size(); i++) {
        auto backgroundKey = itemOrder[i].GetString();
        if (bgKey->compare(backgroundKey) == 0) {
            log("delete item");
            continue; // pass
        }
        log("item %s  del: %s", backgroundKey, bgKey->getCString());
        newOrder.PushBack(backgroundKey, allocator);
    }
    
    orderDocument["background_order"] = newOrder;
    rapidjson::StringBuffer strbuf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
    orderDocument.Accept(writer);
    auto resultOrderJsonString = strbuf.GetString();
    ud->setStringForKey(ORDER_KEY, resultOrderJsonString);
    ud->flush();
    
    return true;
}

void BackgroundScrollView::runRemoveBackgroundAnimation(__String* bgKey)
{
    log("removeBackgroundAnimation");
    std::string keyStr(bgKey->getCString());
    auto backgroundIndex = this->getBackgroundIndex(keyStr);
    
    this->removeBackground(bgKey);
    
    if (backgroundIndex == CC_INVALID_INDEX) {
        log("not found");
    } else {
        cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
        auto bgToBeDelete = (Node*)items->objectForKey(bgKey->getCString());
        //        auto pinToBeDelete = (Sprite*)pins->objectForKey(charKey->getCString());
        
        float moveDuration = 0.25f;
        auto scaleTo = ScaleTo::create(moveDuration, 0.5f);
        auto placeTo = Place::create( cocos2d::Point(-visibleSize.width*2, -visibleSize.height*2));
        auto removeCallback = CallFunc::create([&, visibleSize, backgroundIndex, moveDuration](){
            
            cocos2d::Point scrollOffset = scrollView->getContentOffset();
            auto moveByPoint = cocos2d::Point(-HOME_BOTTOM_SCROLL_VIEW_FRAME_SIZE.width,0);
            this->scrollView->setContentOffsetInDuration(cocos2d::Point(scrollOffset.x - moveByPoint.x, scrollOffset.y), moveDuration);
            
            auto moveBy = MoveBy::create(moveDuration, moveByPoint);
            auto currentBgCount = backgroundList->count();
            
            int nextIndex = currentBgCount;
            bool processNearBg = false;
            
            for (int index = backgroundIndex; index < currentBgCount; index++) {
                float bottomPadding = 0;
                if((index%2 == 0)) {
                    bottomPadding = RECORDING_BOTTOM_SCROLL_VIEW_FRAME_BOTTOM_PADDING;
                } else {
                    bottomPadding = -RECORDING_BOTTOM_SCROLL_VIEW_FRAME_BOTTOM_PADDING;
                }
                
                moveByPoint = cocos2d::Point(-HOME_BOTTOM_SCROLL_VIEW_FRAME_SIZE.width,bottomPadding);
                moveBy = nullptr;
                moveBy = MoveBy::create(moveDuration, moveByPoint);
                
                auto bgKey = (__String*)backgroundList->getObjectAtIndex(index);
                auto holder = (Node*)items->objectForKey(bgKey->getCString());
                holder->setTag(BACKGROUND_BASE_TAG + index-1);
                auto boundingRect = holder->getBoundingBox();
                auto originXInScreen = boundingRect.origin.x + scrollOffset.x;
                
                if (index == currentBgCount) {
                    auto lastAnimationBookCallback = CallFunc::create([&](){
                        this->scrollView->setTouchEnabled(true);
                    });
                    auto lastBookAnimationSequence = Sequence::create(moveBy->clone(), lastAnimationBookCallback, NULL);
                    holder->runAction(lastBookAnimationSequence);
                } else {
                    holder->runAction(moveBy->clone());
                }
                
//                Sprite *pinSprite = (Sprite*)pins->objectForKey(bgKey->getCString());
//                cocos2d::Rect delBoundingRect;
//                float originDelXInScreen = 0;
//                if (pinSprite) {
//                    delBoundingRect = pinSprite->getBoundingBox();
//                    originDelXInScreen = delBoundingRect.origin.x + scrollOffset.x;
//                }
//                
//                if (originXInScreen < visibleSize.width) {
//                    if (index == currentBgCount) {
//                        auto lastAnimationBookCallback = CallFunc::create([&](){
//                            this->scrollView->setTouchEnabled(true);
//                            auto currentContentsSize = this->scrollView->getContentSize();
//                            this->scrollView->setContentSize( cocos2d::Size(currentContentsSize.width-HOME_BOTTOM_SCROLL_VIEW_FRAME_SIZE.width, currentContentsSize.height ));
//                        });
//                        auto lastBookAnimationSequence = Sequence::create(moveBy->clone(), lastAnimationBookCallback, NULL);
//                        holder->runAction(lastBookAnimationSequence);
//                        if (pinSprite) {
//                            pinSprite->runAction(moveBy->clone());
//                        }
//                    } else {
//                        holder->runAction(moveBy->clone());
//                        if (pinSprite) {
//                            pinSprite->runAction(moveBy->clone());
//                        }
//                    }
//                } else {
//                    if (!processNearBg) { // Will be moved in screen but now in out screen.
//                        processNearBg = true;
//                        auto lastAnimationCallback = CallFunc::create([&](){
//                            this->scrollView->setTouchEnabled(true);
//                            auto currentContentsSize = this->scrollView->getContentSize();
//                            this->scrollView->setContentSize( cocos2d::Size(currentContentsSize.width - HOME_BOTTOM_SCROLL_VIEW_FRAME_SIZE.width, currentContentsSize.height ));
//                        });
//                        auto lastAnimationSequence = Sequence::create(moveBy->clone(), lastAnimationCallback, NULL);
//                        holder->runAction(lastAnimationSequence);
//                        
//                        if (pinSprite) {
//                            pinSprite->runAction(moveBy->clone());
//                        }
//                    } else {
//                        nextIndex = index;
//                        break;
//                    }
//                }
            }
            
            for (int index = nextIndex; index < currentBgCount; index++) {
                auto bgKey = (__String *)backgroundList->getObjectAtIndex(index);
                auto holder = (Node*)items->objectForKey(bgKey->getCString());
                auto originPosition = holder->getPosition();
                holder->setPosition( cocos2d::Point(originPosition.x+moveByPoint.x, originPosition.y+moveByPoint.y));
                
//                Sprite *pinSprite = (Sprite*)pins->objectForKey(bgKey->getCString());
//                if (pinSprite) {
//                    auto pinOriginPosition = pinSprite->getPosition();
//                    pinSprite->setPosition(cocos2d::Point(pinOriginPosition.x+moveByPoint.x, pinOriginPosition.y+moveByPoint.y));
//                }
            }
        });
        
        auto doneCallback = CallFunc::create([=](){
            auto selDeletePin = (Sprite*)pins->getLastObject();
            selDeletePin->removeFromParentAndCleanup(true);
            selDeleteBg->removeFromParentAndCleanup(true);
            items->removeObjectForKey(selectedBgKey->getCString());
//            pins->removeObjectForKey(selectedBgKey->getCString());
            pins->removeLastObject();
            std::string bgStr(selectedBgKey->getCString());
            log("deleteButtons->count() :%zd fixItemCount :%i",deleteButtons->count(), fixItemCount);
            if (deleteButtons->count() > 0) {
                deleteButtons->removeObjectAtIndex(this->getBackgroundIndex(bgStr)-fixItemCount);
            }
            backgroundList->removeObjectAtIndex(this->getBackgroundIndex(bgStr));
            log("after items->count : %i %zi %zi", items->count(), pins->count(), backgroundList->count());
            this->adjustLine();
            this->setEnableDeleteButtons(true);
            scrollView->isAnimate = false;
        });
        
        auto disappearAction = Sequence::create(scaleTo, placeTo, removeCallback, doneCallback, NULL);
        bgToBeDelete->runAction(disappearAction);
    }
}

void BackgroundScrollView::adjustLine()
{
    std::vector<std::string> lineResources;
    if (superType == BG_HOME) {
        lineResources.push_back("home_background_frame_line_left.png");
    } else {
        lineResources.push_back("set_background_frame_line_left.png");
    }
    lineResources.push_back("set_background_frame_line_mid.png");
    if (superType == BG_HOME) {
        lineResources.push_back("home_background_frame_line_right.png");
    } else {
        lineResources.push_back("set_background_frame_line_right.png");
    }
    
    auto backgroundCount = backgroundList->count();
    auto backgroundContentSize = HOME_BOTTOM_SCROLL_VIEW_FRAME_SIZE;
    int whiteSpace = WHITESPACE_CHAR_DEFAULT;
    auto scrollViewWidth = HOME_BACKGROUND_SCROLL_VIEW_STARTPADDING + backgroundContentSize.width*(backgroundCount + whiteSpace);
    
    auto targetMidLineNum = std::ceil( (scrollViewWidth - HOME_BOTTOM_LINE_LEFT.width - HOME_BOTTOM_LINE_RIGHT.width) / HOME_BOTTOM_LINE_MID.width );
    auto currentMidLineNum = linesCount-2;
    if (targetMidLineNum < currentMidLineNum) { // remove line
        auto moveByPoint = cocos2d::Point(-HOME_BOTTOM_LINE_MID.width+(HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_HOLDER_SIZE.width*2),0);
        auto toRemoveCount = currentMidLineNum - targetMidLineNum;
        
        auto container = scrollView->getChildren().at(0);
        Sprite *lastLine = (Sprite *)(container->getChildByTag(LINE_BASE_TAG+(linesCount-1)));
        auto listLineCurrentPosition = lastLine->getPosition();
        lastLine->setPosition(listLineCurrentPosition - cocos2d::Point(toRemoveCount * HOME_BOTTOM_LINE_MID.width, 0) );
        
        for (int index = currentMidLineNum; index > currentMidLineNum - toRemoveCount; index--) {
            Sprite *line = (Sprite *)(container->getChildByTag(LINE_BASE_TAG+index));
            line->removeFromParent();
        }
        
        Sprite *lastLineForNewTag = (Sprite *)(container->getChildByTag(LINE_BASE_TAG+(linesCount-1)));
        lastLineForNewTag->setTag(LINE_BASE_TAG+(linesCount-2));
        
        auto currentContentSize = scrollView->getContentSize();
        scrollView->setContentSize(currentContentSize - cocos2d::Size(toRemoveCount * HOME_BOTTOM_LINE_MID.width, 0));
        
        linesCount -= toRemoveCount;
        
        cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
        cocos2d::Point scrollOffset = scrollView->getContentOffset();
        if (scrollOffset.x < -(scrollView->getContentSize().width-visibleSize.width+backgroundContentSize.width)) {
            this->scrollView->setContentOffsetInDuration(cocos2d::Point(-(scrollView->getContentSize().width-visibleSize.width+backgroundContentSize.width), scrollOffset.y), 0.25f);
        }
        
    } else if (targetMidLineNum > currentMidLineNum){ // add line
        auto toAddCount = targetMidLineNum - currentMidLineNum;
        auto lastIndex = (linesCount-1);
        
        auto container = scrollView->getChildren().at(0);
        
        Sprite *lastLine = (Sprite *)(container->getChildByTag(LINE_BASE_TAG+lastIndex));
        auto listLineCurrentPosition = lastLine->getPosition();
        lastLine->setPosition(listLineCurrentPosition + cocos2d::Point(toAddCount * HOME_BOTTOM_LINE_MID.width, 0) );
        
        auto lineStart = cocos2d::Point(listLineCurrentPosition.x, HOME_BOTTOM_LINE_START.y);
        for (int index = lastIndex; index < lastIndex + toAddCount; index++) {
            auto line = Sprite::create("set_background_frame_line_mid.png");
            line->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
            line->setPosition(lineStart);
            line->setTag(LINE_BASE_TAG + index);
            scrollView->addChild(line, TOUCHABLE_LAYER-5);
            lineStart = lineStart + cocos2d::Point(line->getContentSize().width, 0);
        }
        
        auto currentContentSize = scrollView->getContentSize();
        scrollView->setContentSize(currentContentSize + cocos2d::Size(toAddCount * HOME_BOTTOM_LINE_MID.width, 0));
        
        lastLine->setTag(LINE_BASE_TAG + lastIndex + toAddCount);
        
        linesCount += toAddCount;
    } else {
        // no need to adjust
    }

}

void BackgroundScrollView::scrollToLastItem()
{
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    this->scrollView->setContentOffsetInDuration(cocos2d::Point(-(scrollView->getContentSize().width-visibleSize.width), scrollOffset.y), 0.25f);
}

void BackgroundScrollView::scrollToFirstItem()
{
    if (!isScrollingToFirst) {
        isScrollingToFirst = true;
        this->scrollView->stopAllScroll();
        this->scrollView->setContentOffsetInDuration(cocos2d::Point::ZERO, 0.25f);
    }
}

void BackgroundScrollView::loadJsonToScrollView(ScrollView *scrollView)
{
    auto ud = UserDefault::getInstance();
    auto orderContentStr = ud->getStringForKey(PRE_LOAD_OBJ_ORDER_KEY);
    rapidjson::Document orderDocument;
    orderDocument.Parse<0>(orderContentStr.c_str());
    rapidjson::Value& order = orderDocument["background_order"];
    
    // user order
    auto userOrderJsonString = ud->getStringForKey(ORDER_KEY, "");
    rapidjson::Document userOrderDocument;
    userOrderDocument.Parse<0>(userOrderJsonString.c_str());
    rapidjson::Value& userOrder = userOrderDocument["background_order"];
    
    auto backgroundSize = order.Size() + userOrder.Size();
    
    // create merged book Key array
    auto mergedBackgroundKeyArray = __Array::createWithCapacity(backgroundSize);
    
    for(int index = 0; index < order.Size(); index++){
        mergedBackgroundKeyArray->addObject( __String::create(order[rapidjson::SizeType(index)].GetString()) );
    }
    
    for(int index = 0; index < userOrder.Size(); index++){
        mergedBackgroundKeyArray->addObject( __String::create(userOrder[rapidjson::SizeType(index)].GetString()) );
    }
    
    auto bachgroundItemContentSize = HOME_BOTTOM_SCROLL_VIEW_FRAME_SIZE;
    
    fixItemCount = 0;
    parseErrorCount = 0;
    
    for (int index = 0; index < backgroundSize; index++) {
        auto backgroundKey = ((__String*)mergedBackgroundKeyArray->getObjectAtIndex(index))->getCString();

        auto bgJsonKeyStr = ud->getStringForKey(backgroundKey);
        rapidjson::Document bgInfo;
        bgInfo.Parse<0>(bgJsonKeyStr.c_str());
        
        int newIndex = index - parseErrorCount;
        if (bgInfo.HasParseError()) {
            log("parse error - bgKey : %s", backgroundKey);
            auto bgKeyStr = __String::create(backgroundKey);
            Util::removeParseErrorObject("background_order", bgKeyStr);
            parseErrorCount++;
        } else {
            auto item = this->createBackgroundForScrollView(backgroundKey, newIndex);
            items->setObject(item, backgroundKey);
            
            auto ud = UserDefault::getInstance();
            auto backgroundJsonString = ud->getStringForKey(backgroundKey);
            rapidjson::Document backgroundInfo;
            backgroundInfo.Parse<0>(backgroundJsonString.c_str());
            
            if (backgroundInfo["custom"].GetBool()){
                scrollView->addChild(item, TOUCHABLE_LAYER, BACKGROUND_BASE_TAG+newIndex);
                auto deleteButton = cocos2d::ui::Button::create("home_create_delete_btn_normal.png", "home_create_delete_btn_press.png");
                deleteButton->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                deleteButton->setPosition(cocos2d::Point(HOME_BOTTOM_SCROLL_VIEW_FRAME_SIZE.width - (HOME_BOTTOM_SCROLL_VIEW_FRAME_SIZE.width-HOME_CUSTOM_BG_SIZE.width)/2 -deleteButton->getContentSize().width/2, HOME_CUSTOM_BG_BOTTOM_PADDING-deleteButton->getContentSize().height/2));
                deleteButton->addTouchEventListener(this, (cocos2d::ui::SEL_TouchEvent)(&BackgroundScrollView::deleteButtonClicked));
                deleteButton->setUserData(backgroundList->getObjectAtIndex(newIndex));
                item->addChild(deleteButton, 2);
                deleteButtons->addObject(deleteButton);
            } else {
                fixItemCount++;
                scrollView->addChild(item, TOUCHABLE_LAYER_WITH_DIM, BACKGROUND_BASE_TAG+newIndex);
            }
            
            auto spriteName = __String::createWithFormat("home_background_frame_pin_%02d.png", newIndex%4+1);
            auto pin = Sprite::create(spriteName->getCString());
            pin->setAnchorPoint(cocos2d::Point::ANCHOR_TOP_LEFT);
            cocos2d::Point pos = cocos2d::Point(item->getPositionX() + (bachgroundItemContentSize.width - pin->getContentSize().width)/2,
                                                bachgroundItemContentSize.height + item->getPositionY() + HOME_BOTTOM_SCROLL_VIEW_PIN_TOP_PADDING);
            pin->setPosition(pos);
            pins->addObject(pin);
            if (backgroundInfo["custom"].GetBool() ){
                scrollView->addChild(pin, TOUCHABLE_LAYER+1);
            } else {
                scrollView->addChild(pin, TOUCHABLE_LAYER-2);
            }
        }
    }
    
    std::vector<std::string> lineResources;
    if (superType == BG_HOME) {
        lineResources.push_back("home_background_frame_line_left.png");
    } else {
        lineResources.push_back("set_background_frame_line_left.png");
    }
    lineResources.push_back("set_background_frame_line_mid.png");
    if (superType == BG_HOME) {
        lineResources.push_back("home_background_frame_line_right.png");
    } else {
        lineResources.push_back("set_background_frame_line_right.png");
    }
    
    int whiteSpace = WHITESPACE_CHAR_DEFAULT;
    auto scrollViewWidth = HOME_BACKGROUND_SCROLL_VIEW_STARTPADDING + bachgroundItemContentSize.width*(backgroundSize + whiteSpace);
    auto midLineNum = std::ceil( (scrollViewWidth - HOME_BOTTOM_LINE_LEFT.width - HOME_BOTTOM_LINE_RIGHT.width) / HOME_BOTTOM_LINE_MID.width );
    
    cocos2d::Point startLinePosition = HOME_BOTTOM_LINE_START;
    float totalLineWidth = startLinePosition.x;
    
    int lineCount = 0;
    while (lineCount < midLineNum + 2) { // +2 -> include left, right line
        int resourceIndex = (lineCount == 0)? 0:(lineCount == (midLineNum+1))? 2:1;
        auto line = Sprite::create(lineResources.at(resourceIndex));
        line->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        line->setPosition(cocos2d::Point(totalLineWidth, startLinePosition.y));
        line->setTag(LINE_BASE_TAG + lineCount);
        lineCount++;
        totalLineWidth += line->getContentSize().width;
        scrollView->addChild(line, TOUCHABLE_LAYER-5);
    }
    linesCount = lineCount;
    scrollView->setContentSize(cocos2d::Size(totalLineWidth, HOME_BACKGROUND_SCROLL_VIEW_HEIGHT));
}

void BackgroundScrollView::reloadData()
{
    backgroundList->removeAllObjects();
    deleteButtons->removeAllObjects();
    items->removeAllObjects();
    pins->removeAllObjects();
    
    auto container = scrollView->getChildren().at(0);
    container->removeAllChildren();
    
    this->loadJsonToScrollView(this->scrollView);
//    this->scrollView->setContentOffset(cocos2d::Point(0,0), true);
}

void BackgroundScrollView::setEnableDeleteButtons(bool enable)
{
    for(int index = 0; index < deleteButtons->count(); index++){
        ui::Button *deleteBtn = (ui::Button*)deleteButtons->getObjectAtIndex(index);
        deleteBtn->setTouchEnabled(enable);
    }
}