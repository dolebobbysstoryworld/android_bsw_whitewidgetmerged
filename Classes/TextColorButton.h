//
//  TextColorButton.h
//  bobby
//
//  Created by oasis on 2014. 6. 2..
//
//

#ifndef __bobby__TextColorButton__
#define __bobby__TextColorButton__

#include "ui/CocosGUI.h"
#include "cocos2d.h"

USING_NS_CC;

class TextColorButton;
class TextColorButtonDelegate
{
public:
    virtual ~TextColorButtonDelegate() {}
    virtual void textColorButtonPressed(TextColorButton* button) = 0;
    virtual void textColorButtonNormal(TextColorButton* button) = 0;
};


class TextColorButton : public cocos2d::ui::Button
{
public:
    TextColorButton():_delegate(nullptr){}
    static TextColorButton* create(const std::string& normalImage,
                          const std::string& selectedImage = "",
                          const std::string& disableImage = "",
                          cocos2d::ui::TextureResType texType = cocos2d::ui::UI_TEX_TYPE_LOCAL);
    bool init(const std::string &normalImage,
                               const std::string& selectedImage ,
                               const std::string& disableImage,
              cocos2d::ui::TextureResType texType);
    virtual void onPressStateChangedToNormal() override;
    virtual void onPressStateChangedToPressed() override;
    void setNormalTitleColor(const Color3B& color);
    void setPressTitleColor(const Color3B& color);
    TextColorButtonDelegate *getDelegate() { return _delegate; }
    void setDelegate(TextColorButtonDelegate *pDelegate) { _delegate = pDelegate; }
protected:
    TextColorButtonDelegate *_delegate;
    
private:
    Color3B normalTitleColor;
    Color3B pressTitleColor;
};

#endif /* defined(__bobby__TextColorButton__) */
