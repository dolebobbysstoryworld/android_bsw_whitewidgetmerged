//
//  ChapterPlayer.cpp
//  bobby
//
//  Created by Dongwook, Kim on 14. 6. 27..
//
//

#include "ChapterPlayer.h"
#include "ResourceManager.h"
#include "Character.h"
#include "Define.h"
#include "AppDelegate.h"
#include "Util.h"

#include "external/json/document.h"
#include "external/json/prettywriter.h"
#include "external/json/stringbuffer.h"

#include "SimpleAudioEngine.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
#include "VoiceRecorder.h"
#endif
#endif

#define TRANSITION_TEMP_FILE    "transition_temp.png"

#define ZORDER_CURRENT_LAYER     1
#define ZORDER_PREV_LAYER        2

#define TAG_BG_FRAME        110
#define TAG_OLD_BG_FRAME    TAG_BG_FRAME+1
#define TAG_BG_ANI_OBJ      200

#define MAX_TIME_DURATION       60 * 1000 // 60 sec
#define MAX_PLAY_FPS            60.0

// on "init" you need to initialize your instance
bool ChapterPlayer::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    playing = false;
    paused = false;
    started = false;
    onTransition = false;
    
    playedTime = 0;
    playTouchTimeStampIndex = 0;
    playTouchElapsedTime = 0;
    accumulatedTouchTimeStamp = 0;
    
    delegate = NULL;
        
    characters = __Array::create();
    characters->retain();
    
    characterProperties = __Dictionary::create();
    characterProperties->retain();
    
    touchTargetMapping = __Dictionary::create();
    touchTargetMapping->retain();
    
    touchData = __Array::create();
    touchData->retain();
    
    playingTouches = __Dictionary::create();
    playingTouches->retain();
    
    invisibleCharacters = __Array::create();
    invisibleCharacters->retain();

    cocos2d::Size size = Director::getInstance()->getWinSize();
    
    transitionTexture = RenderTexture::create((int)size.width, (int)size.height);
    transitionTexture->getSprite()->setAnchorPoint( cocos2d::Point(0.5f,0.5f) );
    transitionTexture->setPosition( cocos2d::Point(size.width/2, size.height/2) );
    transitionTexture->setAnchorPoint( cocos2d::Point(0.5f,0.5f) );
    transitionTexture->retain();
    
    currentLayer = nullptr;
    prevLayer = nullptr;
    
//    this->loadJsonData();

    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(ChapterPlayer::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(ChapterPlayer::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(ChapterPlayer::onTouchEnded, this);
    listener->onTouchCancelled = CC_CALLBACK_2(ChapterPlayer::onTouchCancelled, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    this->scheduleUpdate();
    
    return true;
}

bool ChapterPlayer::loadJsonData(const char* bookKey, int chapterIndex)
{
    this->bookKey = const_cast<char*>(bookKey);
    this->chapterIndex = chapterIndex;
    
    auto ud = UserDefault::getInstance();
    auto bookJsonString = ud->getStringForKey(bookKey);
    
    __String *backgroundKey;
    
    bool hasPrevChapter = false;
    if (currentLayer != nullptr && currentLayer->isVisible()) {
        if (prevLayer != nullptr) {
            prevLayer->removeFromParent();
            prevLayer->release();
        }
        prevLayer = currentLayer;
        prevLayer->setLocalZOrder(ZORDER_PREV_LAYER);
        hasPrevChapter = true;
    }
    
    currentLayer = cocos2d::Layer::create();
    currentLayer->retain();
    currentLayer->setLocalZOrder(ZORDER_CURRENT_LAYER);
    this->addChild(currentLayer);
    
    if(hasPrevChapter) {
        currentLayer->setVisible(false);
        cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
        currentLayer->setPosition(cocos2d::Point(visibleSize.width, 0));
    }
    
    if (bookJsonString != "") {
        rapidjson::Document bookDoc;
        bookDoc.Parse<0>(bookJsonString.c_str());
        rapidjson::Value& chapters = bookDoc["chapters"];
        __String *chapterName = __String::createWithFormat("chapter%d", chapterIndex);
        
        rapidjson::Value& chapterData = chapters[chapterName->getCString()];
        if (!chapterData.IsNull() && chapterData.HasMember("char_datas")) {

            
            auto backgroundKeyString = chapterData["background"].GetString();
            
            auto backgroundString = ud->getStringForKey(backgroundKeyString);
            rapidjson::Document backgroundInfo;
            backgroundInfo.Parse<0>(backgroundString.c_str());
            auto isCustom = backgroundInfo["custom"].GetBool();
            
            Sprite *selectedFrame = nullptr;
            if (backgroundKeyString) {
                auto bg = currentLayer->getChildByTag(TAG_BG_FRAME);
                if (bg != nullptr) {
                    bg->setTag(TAG_OLD_BG_FRAME);
                }
                
                backgroundKey = __String::create(backgroundKeyString);
                cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
                auto backgroundJsonString = ud->getStringForKey(backgroundKey->getCString());
                rapidjson::Document backgroundInfo;
                backgroundInfo.Parse<0>(backgroundJsonString.c_str());
                const char *bgFileName = nullptr;
                if (isCustom) {
                    auto borderBg = "home_background_select_custom.png";
                    bgFileName = borderBg;
                } else {
                    bgFileName = backgroundInfo["select"].GetString();
                }
                selectedFrame = Sprite::create(bgFileName);
                
                auto hasAni = backgroundInfo.HasMember("ani_ccbi");
                if (hasAni) {
                    auto bgID = backgroundInfo["bg_id"].GetInt();
                    cocos2d::Point aniItemPos;
                    auto aniCcbi = backgroundInfo["ani_ccbi"].GetString();
                    
                    switch (bgID) {
                        case 1:// B001
                            aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI01;
                            break;
                        case 2:// B002
                            aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI02;
                            break;
                        case 4:// B004
                            aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI04;
                            break;
                        case 6:// B006
                            aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI06;
                            break;
//                        case 7:// B007
//                            aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI07;
//                            break;
                        case 8:// B008
                            aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI08;
                            break;
                        case 9:// B009
                            aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI09;
                            break;
//                        case 10:// B010
//                            aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI10;
//                            break;
                        case 11:// B011
                            aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI11;
                            break;
                        case 12:// B012
                            aniItemPos = HOME_BACKGROUND_ITEM_BG_ANI12;
                            break;
                        default:
                            break;
                    }
                    auto scaleFactor = Util::getCharacterScaleFactor();
                    
                    cocosbuilder::NodeLoaderLibrary *nodeLoaderLibrary = cocosbuilder::NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
                    cocosbuilder::CCBReader *ccbReader = new cocosbuilder::CCBReader(nodeLoaderLibrary);
                    Node *ccbiObj = ccbReader->readNodeGraphFromFile(aniCcbi, this);
                    auto ccbiSprite = (Sprite*)ccbiObj;
                    ccbiSprite->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                    
                    ccbiSprite->setPosition(cocos2d::Point(aniItemPos.x/scaleFactor, aniItemPos.y/scaleFactor));
                    ccbReader->release();
                    
                    if (ccbiSprite) {
                        ccbiSprite->setTag(TAG_BG_ANI_OBJ);
                        selectedFrame->addChild(ccbiSprite);
                    }
                }
                
                selectedFrame->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
                selectedFrame->setPosition(cocos2d::Point(visibleSize.width/2, visibleSize.height/2));
                selectedFrame->setTag(TAG_BG_FRAME);
                
                currentLayer->addChild(selectedFrame);
            }
            
            if (isCustom) {
                auto bgPhoto = (char *)backgroundInfo["main_photo_file"].GetString();
                auto bgPhotoImage = __String::create(bgPhoto);
                if (bgPhotoImage != nullptr) {
                    std::string photoType("bg");
                    std::string path = Util::getMainPhotoPath(photoType);
                    path.append(bgPhotoImage->getCString());
                    if (cocos2d::FileUtils::getInstance()->isFileExist(path) ){
                        
                        cocos2d::Data imageData = cocos2d::FileUtils::getInstance()->getDataFromFile(path.c_str());
                        unsigned long nSize = imageData.getSize();
                        
                        cocos2d::Image *image = new cocos2d::Image();
                        image->initWithImageData(imageData.getBytes(), nSize);
                        
                        cocos2d::Texture2D *texture = new cocos2d::Texture2D();
                        texture->initWithImage(image);
                        
                        auto picture = Sprite::createWithTexture(texture);
                        picture->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                        float scaleFactor = Util::getCharacterScaleFactor();
                        picture->setPosition(cocos2d::Point(HOME_CUSTOM_BG_BIG.x/scaleFactor,HOME_CUSTOM_BG_BIG.y/scaleFactor));
                        selectedFrame->addChild(picture);
                    } else {
                        log("error! no file : %s", path.c_str());
                    }
                }
            }
            
            auto scaleFactor = Util::getCharacterScaleFactor() * FULL_SCREEN_ZOOM_SCALE;
            selectedFrame->setScale(scaleFactor);
            
            auto originSize = selectedFrame->getContentSize() * Util::getCharacterScaleFactor();
            auto zoomedSize = originSize * FULL_SCREEN_ZOOM_SCALE;
            
            float startXOffet = (zoomedSize.width - originSize.width)/2;
            float startYOffet = (zoomedSize.height - originSize.height)/2;
            
            __Array *zOrderList = __Array::create();
            rapidjson::Value& charPropertyArray = chapterData["char_datas"];
            
            for (int index = 0; index < charPropertyArray.Size(); index++) {
                rapidjson::Value& charPropertyDic = charPropertyArray[rapidjson::SizeType(index)];
                
                __Dictionary* propertyDic = __Dictionary::create();
                
                int charID = charPropertyDic["char_id"].GetInt();
                
                std::string charKey =  charPropertyDic["char_key"].GetString();
                
                __String *charKeyString = __String::create(charKey);
                
                float orgPosX = (float)charPropertyDic["pos_x"].GetDouble();
                orgPosX -= startXOffet;
                orgPosX *= FULL_SCREEN_ZOOM_SCALE;
                __Float* posX = __Float::create(orgPosX);

                float orgPosY = (float)charPropertyDic["pos_y"].GetDouble();
                orgPosY -= startYOffet;
                orgPosY *= FULL_SCREEN_ZOOM_SCALE;
                __Float* posY = __Float::create(orgPosY);
                
                __Float* scale = __Float::create((float)charPropertyDic["scale"].GetDouble() * FULL_SCREEN_ZOOM_SCALE);
                __Float* angle = __Float::create((float)charPropertyDic["angle"].GetDouble());
                __Integer* direction = __Integer::create(charPropertyDic["direction"].GetInt());
                __Integer* zOrder = __Integer::create(charPropertyDic["z_order"].GetInt());
                
                propertyDic->setObject(charKeyString, "char_key");
                propertyDic->setObject(posX, "pos_x");
                propertyDic->setObject(posY, "pos_y");
                propertyDic->setObject(scale, "scale");
                propertyDic->setObject(angle, "angle");
                propertyDic->setObject(direction, "direction");
                propertyDic->setObject(zOrder, "z_order");
                
                characterProperties->setObject(propertyDic, charID);
                
                Character *character = Character::create(charKey.c_str());
                character->characterID = charID;
                character->setScale(scale->getValue());
                character->setPosition(posX->getValue(), posY->getValue());
                character->setRotation(angle->getValue());
                character->setDirection((CharacterDirection)direction->getValue());
                character->lockMultiTouch(); // limit scaling and rotation
                currentLayer->addChild(character, zOrder->getValue());
                characters->addObject(character);
            }
            
            for (int i = 0; i < zOrderList->count(); i++) { //  sort by z order
                Character *character = (Character *)zOrderList->getObjectAtIndex(i);
                character->setLocalZOrder(1);
            }
            
            auto voiceFile = chapterData["voice_file"].GetString();
            if (voiceFile) {
                voiceFileName = __String::create(voiceFile);
                voiceFileName->retain();
            }
            
            if (chapterData.HasMember("sound") && strlen(chapterData["sound"].GetString()) > 0 ){
                auto soundKey = chapterData["sound"].GetString();
                auto soundString = ud->getStringForKey(soundKey);
                rapidjson::Document soundInfo;
                soundInfo.Parse<0>(soundString.c_str());
                if (soundInfo.HasMember("sound_filename")) {
                    auto soundFile = soundInfo["sound_filename"].GetString();
                    if (soundFile) {
                        bgmFileName = __String::create(soundFile);
                        bgmFileName->retain();
                    }
                }
            }
            
            auto timelineKey = chapterData["timeline_file"].GetString();
            auto timelineJsonString = ud->getStringForKey(timelineKey);
            
            rapidjson::Document timelineDocument;
            timelineDocument.Parse<0>(timelineJsonString.c_str());
            
            rapidjson::Value& timelinesValue = timelineDocument["timelines"];
                        
            for (rapidjson::SizeType i = 0; i < timelinesValue.Size(); i++) {
                rapidjson::Value& timeline = timelinesValue[rapidjson::SizeType(i)];
                rapidjson::Value& timestamp = timeline["timestamp"];
                
                rapidjson::Value& touchID = timeline["touch_id"];
                rapidjson::Value& locationX = timeline["location_x"];
                rapidjson::Value& locationY = timeline["location_y"];
                rapidjson::Value& touchStatus = timeline["touch_status"];
                
                float posX = locationX.GetDouble();
                posX -= startXOffet;
                posX *= FULL_SCREEN_ZOOM_SCALE;
                
                float posY = locationY.GetDouble();
                posY -= startYOffet;
                posY *= FULL_SCREEN_ZOOM_SCALE;
                
                __Dictionary *timelineDic = __Dictionary::create();
                timelineDic->setObject(__Double::create(timestamp.GetDouble()), "timestamp");
                timelineDic->setObject(__Integer::create(touchID.GetInt()), "touch_id");
                timelineDic->setObject(__Float::create(posX), "location_x");
                timelineDic->setObject(__Float::create(posY), "location_y");
                timelineDic->setObject(__Integer::create(touchStatus.GetInt()), "touch_status");
                touchData->addObject(timelineDic);
            }
            
            __Dictionary *lastTouchData = (__Dictionary *)touchData->getLastObject();
            __Double *lastTimeStamp = (__Double *)lastTouchData->objectForKey("timestamp");
            duration = ((lastTimeStamp->getValue())/1000);

            started = false;
            
            return true;
        }
    }
    
    return false;
}

bool ChapterPlayer::onTouchBegan(cocos2d::Touch* touch, cocos2d::Event  *event)
{
    if (event != NULL) { // real touch
        return false;
    }
    
    auto childList = currentLayer->getChildren();
    auto childCount = (int)childList.size();
    
    for (int index = childCount-1; index >= 0; index--) { // by z order
        auto child = childList.at(index);
        
        if (!child->isVisible()) continue;
        
        auto characterNode = dynamic_cast<Character *>(child);
        
        if (characterNode) {
            cocos2d::Point locationInNode = child->convertToNodeSpace(touch->getLocation());
            bool result = characterNode->getBoundingBox().containsPoint(touch->getLocation()); // temp code
            if (result) {
                this->bringToFront(characterNode);
                log("sprite onTouchBegan... x = %f, y = %f", locationInNode.x, locationInNode.y);
                
                int touchID = touch->getID();
                touchTargetMapping->setObject(characterNode, touchID);
                
                characterNode->onTouchBegan(touch, event);
                
                return true;
            }
        }
    }
    return false;
}

void ChapterPlayer::onTouchMoved(cocos2d::Touch* touch, cocos2d::Event  *event)
{
    if (event != NULL) { // real touch
        return;
    }
    
    int touchID = touch->getID();
    Ref* target = touchTargetMapping->objectForKey(touchID);
    if (target != NULL) {
        Character *character = (Character*)target;
        character->onTouchMoved(touch, event);
    }
}

void ChapterPlayer::onTouchEnded(cocos2d::Touch* touch, cocos2d::Event  *event)
{
    if (event != NULL) { // real touch
        return;
    }
    
    int touchID = touch->getID();
    Ref* target = touchTargetMapping->objectForKey(touchID);
    if (target != NULL) {
        Character *character = (Character*)target;
        character->onTouchEnded(touch, event);
        touchTargetMapping->removeObjectForKey(touchID);
    }
    
    log("sprite onTouchesEnded...");
}

void ChapterPlayer::onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event  *event)
{
    int touchID = touch->getID();
    Ref* target = touchTargetMapping->objectForKey(touchID);
    if (target != NULL) {
        Character *character = (Character*)target;
        character->onTouchCancelled(touch, event);
    }
    log("sprite onTouchCancelled...");
}

void ChapterPlayer::play()
{
    if(bookKey == NULL) {
        log("there no data to play!");
        return;
    }
    
    playing = true;
    started = true;
    paused = false;
    
    this->restoreCharacterProperty();
    this->playTouch();
    
    if(CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
    	CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    
    if(bgmFileName != nullptr)
    	CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(bgmFileName->getCString());

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	AppDelegate* app = (AppDelegate*)Application::getInstance();
	if(app != nullptr) {
		if(voiceFileName != nullptr)
		app->changeVoicePlayStatus(ANDROID_PLAY_START, voiceFileName->getCString(), -1);
	}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
    float volume = UserDefault::getInstance()->getFloatForKey(UDKEY_FLOAT_VOLUME);
    if (volume > 0.0f && voiceFileName){
        NSString *fileName = [NSString stringWithUTF8String:voiceFileName->getCString()];
        VoiceRecorder *voiceRecorder = [VoiceRecorder sharedRecorder];
        [voiceRecorder play:fileName];
    }
#endif
#endif
}

void ChapterPlayer::pausePlay()
{
    playing = false;
    paused = true;
    
    for (int index = 0; index < characters->count(); index++) {
        auto character = (Character *)characters->getObjectAtIndex(index);
        character->pause();
    }
    
    
    if (onTransition) {
        pauseTransition();
    }
    
    if(CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
        CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    AppDelegate* app = (AppDelegate*)Application::getInstance();
    if(app != nullptr) {
    		app->changeVoicePlayStatus(ANDROID_PLAY_PAUSE, voiceFileName->getCString(), -1);
    }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
    VoiceRecorder *voiceRecorder = [VoiceRecorder sharedRecorder];
    [voiceRecorder pause];
#endif
#endif
}

void ChapterPlayer::resumePlay()
{
//    if (!started) {
//        play();
//        return;
//    }
    
    playing = true;
    paused = false;
    
    
    if (onTransition) {
        resumeTransition();
    }
    
    for (int index = 0; index < characters->count(); index++) {
        auto character = (Character *)characters->getObjectAtIndex(index);
        character->resume();
    }
    
//        if(CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
    CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    AppDelegate* app = (AppDelegate*)Application::getInstance();
    if(app != nullptr) {
    		app->changeVoicePlayStatus(ANDROID_PLAY_RESUME, voiceFileName->getCString(), -1);
    }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
    VoiceRecorder *voiceRecorder = [VoiceRecorder sharedRecorder];
    [voiceRecorder resume];
#endif
#endif
}

void ChapterPlayer::finishPlay()
{
    playing = false;
    paused = false;
    started = false;

    playedTime = 0;
    
    playTouchTimeStampIndex = 0;
    playTouchElapsedTime = 0;
    accumulatedTouchTimeStamp = 0;
    
//    this->restoreCharacterProperty();
    
    if(CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
    	CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	AppDelegate* app = (AppDelegate*)Application::getInstance();
	if(app != nullptr) {
		app->changeVoicePlayStatus(ANDROID_PLAY_STOP, voiceFileName->getCString(), -1);
	}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
    VoiceRecorder *voiceRecorder = [VoiceRecorder sharedRecorder];
    [voiceRecorder stop];
#endif
#endif
}

void ChapterPlayer::resetTransitionState()
{
    currentLayer->setVisible(false);
}

void ChapterPlayer::clear()
{
    bgmFileName = nullptr;
    this->finishPlay();
    
    bookKey = NULL;
    chapterIndex = -1;
    
//    for (int index = 0; index < characters->count(); index++) {
//        Character *character = (Character *)characters->getObjectAtIndex(index);
//        // workaround for waiting cocos recorder
//        // removing character from parent causes crash...
//        character->setVisible(false);
//    }
    characters->removeAllObjects();
    
    characterProperties->removeAllObjects();
    touchTargetMapping->removeAllObjects();
    touchData->removeAllObjects();
    playingTouches->removeAllObjects();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
    VoiceRecorder *voiceRecorder = [VoiceRecorder sharedRecorder];
    [voiceRecorder clear];
#endif
#endif
}

float ChapterPlayer::getPlayProgress()
{
    float currentProgress = playedTime / duration;
    return currentProgress;
}

void ChapterPlayer::update(float delta)
{
    if (playing && !paused) {
        playedTime += delta;
    }
}

void ChapterPlayer::bringToFront(Sprite* pSender)
{
    for (int index = 0; index < characters->count(); index++) {
        Character *character = (Character *)characters->getObjectAtIndex(index);
        character->setLocalZOrder(0);
    }
    
    pSender->setLocalZOrder(1);
}

void ChapterPlayer::restoreCharacterProperty()
{
    for(int i = 0; i < characters->count(); i++){
        Character *character = (Character *)characters->getObjectAtIndex(i);
        __Dictionary *propertyDic = (__Dictionary *)characterProperties->objectForKey(character->characterID);
        float posX = ((__Float *)propertyDic->objectForKey("pos_x"))->getValue();
        float posY = ((__Float *)propertyDic->objectForKey("pos_y"))->getValue();
        float angle = ((__Float *)propertyDic->objectForKey("angle"))->getValue();
        float scale = ((__Float *)propertyDic->objectForKey("scale"))->getValue();
        int direction = ((__Integer *)propertyDic->objectForKey("direction"))->getValue();
        int localZOrder = ((__Integer *)propertyDic->objectForKey("z_order"))->getValue();
        
        character->setLocalZOrder(localZOrder);
        character->setPosition(posX, posY);
        character->setRotation(angle);
        character->setScale(scale);
        character->setDirection((CharacterDirection)direction);
    }
}

void ChapterPlayer::playTouch()
{
    this->schedule(schedule_selector(ChapterPlayer::onPlayTouch), (1.0/MAX_PLAY_FPS));
}

void ChapterPlayer::onPlayTouch(float df)
{
    if (!playing || paused) {
//        log ("onPlayTouch : paused");
        return;
    }
    
//    log ("onPlayTouch : %f", df);
    double elapsedMs = df * 1000;
    playTouchElapsedTime += elapsedMs;
    
    while (playTouchTimeStampIndex < touchData->count()) {
        __Dictionary *touchInfo = (__Dictionary *)touchData->getObjectAtIndex(playTouchTimeStampIndex);
        __Double *timeStamp = (__Double *)touchInfo->objectForKey("timestamp");
        if (timeStamp->getValue() < playTouchElapsedTime) {
            __Integer *touchID = (__Integer *)touchInfo->objectForKey("touch_id");
            __Integer *touchStatus = (__Integer *)touchInfo->objectForKey("touch_status");
            
            __Float *posX = (__Float *)touchInfo->objectForKey("location_x");
            __Float *posY = (__Float *)touchInfo->objectForKey("location_y");
            
            Touch *touch = (Touch *)playingTouches->objectForKey(touchID->getValue());
            if (touch == NULL) {
                touch = new Touch();
                touch->autorelease();
                playingTouches->setObject(touch, touchID->getValue());
            }
            touch->setTouchInfo(touchID->getValue(), posX->getValue(), posY->getValue());
            
            TouchStatus status = (TouchStatus)touchStatus->getValue();
            
//            log("play touch(%f, %f) ! type: %d", posX->getValue(), posY->getValue(), touchStatus->getValue());
            
            switch (status) {
                case TouchStatus::Down:
                    this->onTouchBegan(touch, nullptr);
                    break;
                case TouchStatus::Up:
                {
                    this->onTouchEnded(touch, nullptr);
                    playingTouches->removeObjectForKey(touchID->getValue());
                }
                    break;
                case TouchStatus::Move:
                    this->onTouchMoved(touch, nullptr);
                    break;
                case TouchStatus::Cancel:
                {
                    this->onTouchCancelled(touch, nullptr);
                    playingTouches->removeObjectForKey(touchID->getValue());
                }
                    break;
                default:
                    break;
            }
            playTouchTimeStampIndex++;
        } else {
            break;
        }
    }
    
    if ( playTouchTimeStampIndex >= touchData->count() ) {
        log ("touchPlay end! : %f", df);
        this->unschedule(schedule_selector(ChapterPlayer::onPlayTouch));
        this->finishPlay();
        
        if (delegate != NULL) {
            delegate->chapterPlayFinished(this->chapterIndex);
        }
    }
}

void ChapterPlayer::prepareTransition()
{
    this->stopBGAnimation();
    
    transitionTexture->beginWithClear(0, 0, 0, 0);
    currentLayer->visit();
    
//    bg->visit();
//    for (int index = 0; index < characters->count(); index++) {
//        Character *character = (Character *)characters->getObjectAtIndex(index);
//        if (character->isVisible()) {
//            character->visit();
//        }
//    }
//    this->visit();
    
    transitionTexture->end();
    
    cocos2d::Size size = Director::getInstance()->getWinSize();
    
    // create prev chapter sprite
    transitionSprite = Sprite::createWithTexture(transitionTexture->getSprite()->getTexture());
    transitionSprite->setFlippedY(true);
    transitionSprite->setPosition(cocos2d::Point::ZERO);
    transitionSprite->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    transitionSprite->setLocalZOrder(20);
    this->addChild(transitionSprite);
}

void ChapterPlayer::stopBGAnimation()
{
    auto bg = currentLayer->getChildByTag(TAG_BG_FRAME);
    auto bgAniObj = bg->getChildByTag(TAG_BG_ANI_OBJ);
    if (bgAniObj != nullptr){
        bgAniObj->setVisible(false);
    }
}

void ChapterPlayer::startTransition()
{
    playing = true;
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("preload_sounds/scene_transition.wav", false);
    runTransition();
    onTransition = true;
    log("start transition");
}

void ChapterPlayer::runTransition()
{
    log("runTransition");

    auto moveAction = CallFunc::create([&](){
//        float transitionDuration = TRANSITION_DURATION - durationOffset;
        cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
        
        if (prevLayer != nullptr) {
            prevLayer->setVisible(false);
        }
        
        auto currentPosition = transitionSprite->getPositionX();
        auto progress = currentPosition / -visibleSize.width;
        auto remainTime = TRANSITION_DURATION * (1.0f - progress);
        
        log("remain time : %f", remainTime);
        auto prevLayerAction = MoveTo::create(remainTime, cocos2d::Point(-visibleSize.width, 0));
        transitionSprite->runAction(prevLayerAction);
        
        if (currentLayer != nullptr) {
            currentLayer->setVisible(true);
            
            auto currentLayerAction = Sequence::create(MoveTo::create(remainTime, cocos2d::Point::ZERO),
                                                       CallFunc::create(CC_CALLBACK_0(ChapterPlayer::transitionFinished,this)),
                                                       nullptr);
            currentLayer->runAction(currentLayerAction);
        }
    });
    
    this->runAction(moveAction);
}

void ChapterPlayer::pauseTransition()
{
    this->stopAllActions();
    
    if (transitionSprite != nullptr) {
        transitionSprite->stopAllActions();
    }
    
    if (currentLayer != nullptr) {
        currentLayer->stopAllActions();
    }
    CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

void ChapterPlayer::resumeTransition()
{
    CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
    runTransition();
}

void ChapterPlayer::transitionFinished()
{
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();

    log("transition finished");
    playing = false;
    onTransition = false;
    
    if (transitionSprite) {
        transitionSprite->removeFromParent();
        transitionSprite = nullptr;
    }

    if (delegate != nullptr) {
        delegate->onTransitionFinished(this->chapterIndex);
    }
}
