//
//  Character.cpp
//  PixelCollision
//
//  Created by oasis on 2014. 5. 8..
//
//

#include "Character.h"
#include "Util.h"
#include "ResourceManager.h"

#include <cocosbuilder/CocosBuilder.h>
#include <SimpleAudioEngine.h>

#include "external/json/document.h"
#include "external/json/prettywriter.h"
#include "external/json/stringbuffer.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "AUIPlayer.h"
#endif

USING_NS_CC;

#define EXPANSION_SCALE         1.08
#define EXPANSION_DURATION      0.5f

#define REPEAT_ACTION_TAG       1
#define STOP_ACTION_TAG       2

#define SNAP_ANGLE_THRESHOLD    5

//int Character::isPowerOfTwo(unsigned int x)
//{
//	return ((x != 0) && !(x & (x - 1)));
//}

bool Character::initWithFile(const std::string& filename)
{
    CCASSERT(filename.size()>0, "Invalid filename for sprite");
    
    Texture2D *texture = Director::getInstance()->getTextureCache()->addImage(filename);
    if (texture)
    {
        cocos2d::Rect rect = cocos2d::Rect::ZERO;
        rect.size = texture->getContentSize();
        return initWithTexture(texture, rect);
    }
    
    return false;
}

bool Character::initWithCCbi(const char* ccbiFileName, const char* charKey, const std::string& charName, const std::string& photoFile, const std::string& indicator,
                             bool isCustom, bool flipOnMove, bool isMoveSoundAllDirection, const std::string& moveSoundFile, const std::string& tapSoundFile, GLubyte alphaThreshold)
{
    if (this->init()) {
        paused = false;
        ignoreAnchorPointForPosition(false);
        touchNumber = 0;
        lastDistance = 0;
        characterID = -1;
        scaleStartDistance = 0;
        secondTouchBegin = false;
        isEnableExpansionEffect = true;
        isExpansionEffectIsOn = false;
        isMoving = false;
        moveByRealTouch = true;
        pausedExpansion = false;
        moveSoundPlaying = false;
        lockedInScreen = false;
        _isCustom = isCustom;
        soundID = 0;
        moveSoundAllDirection = isMoveSoundAllDirection;
        pressStatus = CharacterPressStatus::None;
        this->charName = charName;
        this->charKey = charKey;
        this->photoFilePath = photoFile;
        this->indicatorFileName = indicator;
        this->moveSoundFileName = moveSoundFile;
        this->tapSoundFileName = tapSoundFile;
        this->flipOnMove = flipOnMove;

        this->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
        
        cocosbuilder::NodeLoaderLibrary *nodeLoaderLibrary = cocosbuilder::NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
        cocosbuilder::CCBReader *ccbReaderChar1 = new cocosbuilder::CCBReader(nodeLoaderLibrary);
        Node *obj = ccbReaderChar1->readNodeGraphFromFile(ccbiFileName, this);
        Sprite *character = (Sprite*)obj;
        character->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
        character->setPosition(cocos2d::Point(character->getContentSize().width/2,character->getContentSize().height/2));
        ccbReaderChar1->release();
        
        this->setContentSize(character->getContentSize());
        
        if (character != NULL) {
            
            if (isCustom) {
                std::string photoType("character");
                std::string path = Util::getMainPhotoPath(photoType);
                path.append(photoFilePath.c_str());
                
                if (cocos2d::FileUtils::getInstance()->isFileExist(path) ){
                    cocos2d::Data imageData = cocos2d::FileUtils::getInstance()->getDataFromFile(path.c_str());
                    unsigned long nSize = imageData.getSize();
                    
                    cocos2d::Image *image = new cocos2d::Image();
                    image->initWithImageData(imageData.getBytes(), nSize);
                    
                    cocos2d::Texture2D *texture = new cocos2d::Texture2D();
                    texture->initWithImage(image);
                    
                    Sprite *face = (Sprite*)obj->getChildByTag(100);
                    if (face) {
                        face->setTexture(texture);
                    }
                } else {
                    log("error! no file : %s", path.c_str());
                }
            }
            
            this->addChild(character);
            characterObj = obj;
            this->setDirection(CharacterDirection::Front);
        }
        return true;
    }
    return false;
}


//Sprite* Character::initWithFile(const std::string& filename, GLubyte alphaThreshold)
//{
//    if (this->initWithFile(filename)) {
//        auto fullpath = FileUtils::getInstance()->fullPathForFilename(filename);
//        auto image = new Image;
//        image->initWithImageFile(fullpath);
//        this->initWithImage(image, alphaThreshold);
//    }
//    return this;
//}
//
//Sprite* Character::initWithFile(const char* ccbiFileName, const std::string& filename, GLubyte alphaThreshold)
//{
//    if (this->init()) {
//        cocosbuilder::NodeLoaderLibrary *nodeLoaderLibrary = cocosbuilder::NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
//        cocosbuilder::CCBReader *ccbReaderChar1 = new cocosbuilder::CCBReader(nodeLoaderLibrary);
//        Node *obj = ccbReaderChar1->readNodeGraphFromFile(ccbiFileName, this);
//        Sprite *character = (Sprite*)obj;
//        character->setAnchorPoint(Point(0,0));
//        character->setPosition(Point(0,0));
//        ccbReaderChar1->release();
//        
//        this->setContentSize(character->getContentSize());
//        
//        if (character != NULL) {
//            this->addChild(character);
//            characterObj = obj;
//        }
//
//        auto fullpath = FileUtils::getInstance()->fullPathForFilename(filename);
//        auto image = new Image;
//        image->initWithImageFile(fullpath);
//        this->initWithImage(image, alphaThreshold);
//    }
//    return this;
//}

void Character::initWithImage(cocos2d::Image *image, GLubyte alphaThreshold)
{
    ignoreAnchorPointForPosition(false);
    touchNumber = 0;
    lastDistance = 0;
    secondTouchBegin = false;
    
//    this->setScale((203.0f/507.0f), (241.0f/603.0f));
    this->setAnchorPoint(cocos2d::Point(0.5,0.5));
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(Character::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(Character::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(Character::onTouchEnded, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    pixelMaskWidth = image->getWidth();
    pixelMaskHeight = image->getHeight();
    pixelMaskSize = pixelMaskWidth * pixelMaskHeight;
    
#if USE_BITARRAY
    pixelMask = BitArrayCreate(pixelMaskSize);
    BitArrayClearAll(pixelMask);
#else
    pixelMask = (unsigned char*)malloc(pixelMaskSize * sizeof(unsigned char));
    memset(pixelMask, 0, pixelMaskSize * sizeof(unsigned char));
#endif
   
    const uint32_t* imagePixels = (const uint32_t*)image->getData();
    uint32_t alphaValue = 0, x = 0, y = pixelMaskHeight - 1;
    uint8_t alpha = 0;
    
    for (int i = 0; i < pixelMaskSize; i++)
    {
        int index = y * pixelMaskWidth + x;
        x++;
        if (x == pixelMaskWidth)
        {
            x = 0;
            y--;
        }
        
        alphaValue = imagePixels[i] & 0xff000000;
        if (alphaValue > 0)
        {
            alpha = (uint8_t)(alphaValue >> 24);
            if (alpha >= alphaThreshold)
            {
#if USE_BITARRAY
                BitArraySetBit(pixelMask, index);
#else
                pixelMask[index] = true;
#endif
            }
        }
    }
}

Character* Character::create(const char* charKey)
{
    return Character::create(charKey, 255);
}

Character* Character::create(const char* charKey, uint8_t alphaThreshold)
{
    auto ud = UserDefault::getInstance();
    auto charString = ud->getStringForKey(charKey);
    
    rapidjson::Document characterInfo;
    characterInfo.Parse<0>(charString.c_str());

    __String *ccbiFileName;
    __String *charName;
    __String *indicatorFileName;

    bool flip = true;
    bool isCustom = false;
    std::string photoFileName;
    std::string moveSoundFile;
    std::string tapSoundFile;
    if (characterInfo.HasMember("base_char")) { // custom character
        isCustom = true;
        auto baseChar = characterInfo["base_char"].GetString();
        auto baseCharacterJsonString = ud->getStringForKey(baseChar);
        rapidjson::Document baseCharacterInfo;
        baseCharacterInfo.Parse<0>(baseCharacterJsonString.c_str());
        charName = __String::create(baseCharacterInfo["charname"].GetString());
        ccbiFileName = __String::create(baseCharacterInfo["ccbi"].GetString());
        indicatorFileName = __String::create(baseCharacterInfo["indicator"].GetString());
        flip = baseCharacterInfo["flip_on_move"].GetBool();

        auto photoFile = characterInfo["main_photo_file"].GetString();
        photoFileName = photoFile;
        
        if (baseCharacterInfo.HasMember("move_sound")) {
            moveSoundFile = baseCharacterInfo["move_sound"].GetString();
        }
        
        if (baseCharacterInfo.HasMember("tap_sound")) {
            tapSoundFile = baseCharacterInfo["tap_sound"].GetString();
        }
    } else {
        charName = __String::create(characterInfo["charname"].GetString());
        ccbiFileName = __String::create(characterInfo["ccbi"].GetString());
        indicatorFileName = __String::create(characterInfo["indicator"].GetString());
        flip = characterInfo["flip_on_move"].GetBool();
        
        if (characterInfo.HasMember("move_sound")) {
            moveSoundFile = characterInfo["move_sound"].GetString();
        }
        
        if (characterInfo.HasMember("tap_sound")) {
            tapSoundFile = characterInfo["tap_sound"].GetString();
        }
    }
    
    bool moveSoundAllDirection = false;
    if (characterInfo.HasMember("move_sound_all_direction")) {
        moveSoundAllDirection = characterInfo["move_sound_all_direction"].GetBool();
    }
    
    Character *sprite = new Character();
    
    if (sprite && sprite->initWithCCbi(ccbiFileName->getCString(), charKey, charName->getCString(), photoFileName, indicatorFileName->getCString(),
                                       isCustom, flip, moveSoundAllDirection,  moveSoundFile, tapSoundFile, alphaThreshold) );
    {
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return nullptr;
}

bool Character::pixelMaskBitAt(cocos2d::Point point)
{
	int x = (int)(point.x + 0.5f);
	int y = (int)(point.y + 0.5f);
    
	if (x < 0 || y < 0 || x >= pixelMaskWidth || y >= pixelMaskHeight)
	{
		return false;
	}
    
	int index = y * pixelMaskWidth + x;
   
#if USE_BITARRAY
	return BitArrayTestBit(pixelMask, index);
#else
	return pixelMask[index];
#endif
    
}

bool Character::pixelMaskContainsPoint(cocos2d::Point point)
{
    point = point * CC_CONTENT_SCALE_FACTOR();
    return this->pixelMaskBitAt(point);
}

bool Character::pixelMaskContainsPointInChild(Node *node, cocos2d::Point point)
{
    size_t childCount = node->getChildrenCount();
    if (childCount == 0) {
        if (!node->isVisible() || node->getOpacity() == 0) return false;

        cocos2d::Rect boundingBox = node->getBoundingBox();
        cocos2d::Point currentPoint = point * CC_CONTENT_SCALE_FACTOR();
        if (boundingBox.containsPoint(currentPoint)) {
            cocos2d::Point worldSpace = this->convertToWorldSpace(currentPoint);
            cocos2d::Point locationInNode = node->convertToNodeSpace(worldSpace);
//            Point locationInNode = Point(currentPoint.x - boundingBox.origin.x, currentPoint.y - boundingBox.origin.y);
            int tag = node->getTag();
            if (tag == -1) return false;
            std::string resourceName = this->getResourceName(this->charName, tag);
            if (&resourceName != NULL) {
                clock_t start = clock();

                auto fullpath = FileUtils::getInstance()->fullPathForFilename(resourceName);
                auto image = new cocos2d::Image;
                image->initWithImageFile(fullpath);
                
                clock_t end = clock();
                int elapsed = double(end - start) / CLOCKS_PER_SEC * 1000;
                log("time elpased after image load (%s) : %dms", resourceName.c_str(), elapsed);
                
                auto pixelMaskWidth = image->getWidth();
                auto pixelMaskHeight = image->getHeight();
                
                int locationX = (int)(locationInNode.x + 0.5f);
                int locationY = (int)(locationInNode.y + 0.5f);

                if (locationX < 0 || locationY < 0 || locationX >= pixelMaskWidth || locationY >= pixelMaskHeight)
                {
                    return false;
                }
                
                int index = (pixelMaskHeight - locationY) * pixelMaskWidth + locationX; // y coordination is inversed

                const uint32_t* imagePixels = (const uint32_t*)image->getData();
                uint32_t alphaValue = 0;
                uint8_t alpha = 0;
                
                alphaValue = imagePixels[index] & 0xff000000;

                bool ret = false;
                
                if (alphaValue > 0)
                {
                    alpha = (uint8_t)(alphaValue >> 24);
                    if (alpha >= alphaThreshold)
                    {
                        ret = true;
                    }
                }
                
                clock_t end2 = clock();
                int elapsed2 = double(end - end2) / CLOCKS_PER_SEC * 1000;
                log("time elpased after image check (%s) : %dms", resourceName.c_str(), elapsed2);
                
                delete image;
                
                return ret;
            }
            return false;
        }
        return false;
    } else {
        bool ret = false;
        for (int index = 0; index < childCount; index++) {
            Node *child = node->getChildren().at(index);
            ret = this->pixelMaskContainsPointInChild(child, point);
            if (ret) {
                break;
            }
        }
        return ret;
    }
}


bool Character::containsPoint(cocos2d::Point point)
{
    cocos2d::Rect bbox = cocos2d::Rect(0,0, this->getContentSize().width, this->getContentSize().height);
    cocos2d::Point locationInNodeSpace = this->convertToNodeSpace(point);
    return bbox.containsPoint(locationInNodeSpace);
}

#ifndef MACOS
bool Character::containsTouch(Touch* touch)
{
    Director *director = Director::getInstance();
    cocos2d::Point locationGL = director->convertToGL(touch->getLocationInView());
    return this->containsPoint(locationGL);
}
#endif

bool Character::onTouchBegan(Touch* touch, Event *event)
{
    touchNumber++;
    touches.pushBack(touch);
    
//    pressStatus = CharacterPressStatus::Pressed;
    setPressStatus(CharacterPressStatus::Pressed);

    if (touches.size() == 1) {
        this->startLongPressTrigger();
    } else if (touches.size() == 2) {
        this->cancelLongPressTrigger();
        this->showExpansionEffect(false);
    }
    log("char onTouchBegan : %zd", touches.size());
    return true;
}

void Character::onTouchMoved(Touch* touch, Event  *event)
{
    if (touches.size() >= 2 && !isMultiTouchLocked) {
        this->cancelLongPressTrigger();

        auto touch1 = touches.at(0);
        auto touch2 = touches.at(1);
        //        auto target = static_cast<Sprite*>(event->getCurrentTarget());
        //        if (target->getBoundingBox().containsPoint(this->convertTouchToNodeSpace(touch1)) && target->getBoundingBox().containsPoint(this->convertTouchToNodeSpace(touch2))) {
        //        }
        
        // Get current and previous positions of the touches
        cocos2d::Point curPosTouch1 = Director::getInstance()->convertToGL(touch1->getLocationInView());
        cocos2d::Point curPosTouch2 = Director::getInstance()->convertToGL(touch2->getLocationInView());
        cocos2d::Point prevPosTouch1 = Director::getInstance()->convertToGL(touch1->getPreviousLocationInView());
        cocos2d::Point prevPosTouch2 = Director::getInstance()->convertToGL(touch2->getPreviousLocationInView());
        
        auto distance = curPosTouch1.getDistance(curPosTouch2) / prevPosTouch1.getDistance(prevPosTouch2);
        
        if (scaleStartDistance == 0) {
            scaleStartDistance = distance;
        } else {
            // Calculate new scale
            float curScaleX = this->getScaleX() * distance;
            float curScaleY = this->getScaleY() * distance;
            
            float minScale = Util::getCharacterMinScale();
            float maxScale = Util::getCharacterMaxScale();
            
            if ((minScale < curScaleX) && (curScaleX <= maxScale) && (minScale < curScaleY) && (curScaleY <= maxScale) ) {
                this->setScale(curScaleX, curScaleY);
            }
        }
        
        auto firstVector = prevPosTouch1 - this->getPosition();
        auto firstRotateAngle = -1*firstVector.getAngle();
        auto previousTouch = CC_RADIANS_TO_DEGREES(firstRotateAngle);
        
        auto vector = curPosTouch1 - this->getPosition();
        auto rotateAngle = -1*vector.getAngle();
        auto currentTouch = CC_RADIANS_TO_DEGREES(rotateAngle);
        
        this->setRotation(this->getRotation()+(currentTouch - previousTouch));
        
//        log("char onTouchMoved : %zd", touches.size());
    } else {
        cocos2d::Point currentPos = getPosition();
        cocos2d::Point nextPos = currentPos + touch->getDelta();
        
        moveByRealTouch = (event != NULL);
        
        if (lockedInScreen) {
            float filteredX = nextPos.x;
            float filteredY = nextPos.y;
            
            cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
            
            if (nextPos.x >= visibleSize.width) {
                filteredX = visibleSize.width;
            } else if (nextPos.x <= 0) {
                filteredX = 0;
            }
            
            if (nextPos.y >= visibleSize.height) {
                filteredY = visibleSize.height;
            } else if (nextPos.y <= 0) {
                filteredY = 0;
            }
            
            
            setPosition(cocos2d::Point(filteredX, filteredY));
        } else {
            setPosition(cocos2d::Point(nextPos.x, nextPos.y));
        }
    }
//    secondTouchBegin = false;
}

void Character::onTouchEnded(Touch* touch, Event  *event)
{
    lastDistance = 0;
    touchNumber--;
    touches.eraseObject(touch);
//    secondTouchBegin = false;
    if (touchNumber == 0) {
        if (longPressTriggerAction != NULL) {
            this->cancelLongPressTrigger();
        }
        
        if (pressStatus != CharacterPressStatus::LongPressed) {
            cocos2d::Point start = touch->getStartLocationInView();
            cocos2d::Point prev = touch->getPreviousLocationInView();
            cocos2d::Point current = touch->getLocationInView();
            
            float step1 = start.getDistance(prev);
            float step2 = current.getDistance(prev);
            
            if (abs(step1) < TAP_THRESHOLD && abs(step2) < TAP_THRESHOLD) {
                this->pressed();
            }
        } else {
            setPressStatus(CharacterPressStatus::None);
//            pressStatus = CharacterPressStatus::None;
//            this->runAnimationByDirection();
        }
        if (isExpansionEffectIsOn) {
            showExpansionEffect(false);
        }
        stopEffect();
        
    } else if ( touchNumber == 1 ){
        scaleStartDistance = 0;
        snapRotation();
    }
    log("char onTouchEnded : %zd", touches.size());
}

void Character::onTouchCancelled(Touch* touch, Event *event)
{
    lastDistance = 0;
    scaleStartDistance = 0;
    
    touchNumber--;
    touches.eraseObject(touch);
    
    if (touchNumber == 0) {
        if (longPressTriggerAction != NULL) {
            this->cancelLongPressTrigger();
        }
        
        stopEffect();
        snapRotation();
        setPressStatus(CharacterPressStatus::None);
    }
}

void Character::clearTouch()
{
    touchNumber = 0;
    scaleStartDistance = 0;
    touches.clear();
    setPressStatus(CharacterPressStatus::None);
    snapRotation();
    stopEffect();
//    pressStatus = CharacterPressStatus::None;
//    this->runAnimationByDirection();
}

void Character::snapRotation()
{
    if (fabs(this->getRotation()) < SNAP_ANGLE_THRESHOLD) {
        this->setRotation(0);
    }
}

void Character::showExpansionEffect(bool show)
{
    if (!isEnableExpansionEffect) return;
    
    if(show) {
        if (expansionAction == NULL) {
            oldScale = this->getScale();
//            log("start expansion current scale : %f", oldScale);
            
            isExpansionEffectIsOn = true;
            auto expand = ScaleTo::create(EXPANSION_DURATION, oldScale * EXPANSION_SCALE);
            auto reduce = ScaleTo::create(EXPANSION_DURATION, oldScale);
            auto sequece = Sequence::create(expand, reduce, NULL);
            expansionAction = RepeatForever::create(sequece);
            expansionAction->setTag(REPEAT_ACTION_TAG);
            this->runAction(expansionAction);
        }
    } else {
//        log("stop expansion current scale : %f", oldScale);
        
        if (expansionAction != NULL) {
            isExpansionEffectIsOn = false;

            this->stopActionByTag(REPEAT_ACTION_TAG);
            expansionAction = NULL;
            this->setScale(oldScale);
        }
    }
}

float Character::distanceBetweenPoints(cocos2d::Point p1, cocos2d::Point p2)
{
    float deltaX = p2.x-p1.x;
    float deltaY = p2.y-p1.y;
    return fabs(sqrtf(deltaX*deltaX+deltaY*deltaY));
}

std::string Character::getResourceName(const std::string& characterName, int tag)
{
    CharacterTag cTag = (CharacterTag)tag;
    switch (cTag) {
        case CharacterTag::TagFrontHead:
            return characterName + "_" + "front_head_normal.png";
        case CharacterTag::TagFrontBody:
            return characterName + "_" + "front_stomach.png";
        case CharacterTag::TagFrontLeftArm:
            return characterName + "_" + "front_hand_left.png";
        case CharacterTag::TagFrontRightArm:
            return characterName + "_" + "front_hand_right.png";
        case CharacterTag::TagFrontLeftLeg:
            return characterName + "_" + "front_foot_left.png";
        case CharacterTag::TagFrontRightLeg:
            return characterName + "_" + "front_foot_right.png";
        case CharacterTag::TagFrontTail:
            return characterName + "_" + "front_tail.png";
        case CharacterTag::TagSideHead:
            return characterName + "_" + "side_head_normal.png";
        case CharacterTag::TagSideBody:
            return characterName + "_" + "side_stomach.png";
        case CharacterTag::TagSideLeftArm:
            return characterName + "_" + "side_hand_left.png";
        case CharacterTag::TagSideRightArm:
            return characterName + "_" + "side_hand_right.png";
        case CharacterTag::TagSideLeftLeg:
            return characterName + "_" + "side_foot_left.png";
        case CharacterTag::TagSideRightLeg:
            return characterName + "_" + "side_foot_right.png";
        case CharacterTag::TagSideTail:
            return characterName + "_" + "side_tail.png";
        case CharacterTag::TagFrontItem1:
        case CharacterTag::TagFrontItem2:
        case CharacterTag::TagSideItem1:
        case CharacterTag::TagSideItem2:
            return this->getItemResourceName(characterName, tag);
        default:
            return NULL;
    }
}

std::string Character::getItemResourceName(const std::string& characterName, int tag)
{
    CharacterTag cTag = (CharacterTag)tag;
    
    if ( characterName.compare("lion") == 0 ) {
        switch (cTag) {
            case CharacterTag::TagFrontItem1:
                return characterName + "_" + "front_spear.png";
            case CharacterTag::TagFrontItem2:
            case CharacterTag::TagSideItem1:
            case CharacterTag::TagSideItem2:
                return NULL;
            default:
                return NULL;
        }
    }
    
    return NULL;
}

void Character::setDirection(CharacterDirection direction)
{
    currentDirection = direction;
    this->runAnimationByDirection();
}

void Character::runAnimationByDirection()
{
    if (paused) {
        return;
    }
    
    std::string animationName;
    auto currentScaleX = characterObj->getScaleX();
    
    switch (currentDirection) {
        case CharacterDirection::Front:
        {
            if (pressStatus == CharacterPressStatus::LongPressed) {
                animationName = this->charName + "_" + "front_longtap";
            } else if (pressStatus == CharacterPressStatus::Tap) {
                animationName = this->charName + "_" + "front_tap";
            } else {
                animationName = this->charName + "_" + "front_normal";
            }
            
            if (currentScaleX < 0 && flipOnMove) {
                characterObj->setScaleX(currentScaleX * -1);
            }
        }
            break;
        case CharacterDirection::Left:
        {
            if (pressStatus == CharacterPressStatus::LongPressed) {
                if (isMoving) {
                    animationName = this->charName + "_" + "side_longtap_walk";
                } else {
                    animationName = this->charName + "_" + "side_longtap";
                }
            } else if (pressStatus == CharacterPressStatus::Tap) {
                animationName = this->charName + "_" + "side_tap";
            } else {
                animationName = this->charName + "_" + "side_normal";
            }
            if (currentScaleX < 0 && flipOnMove) {
                characterObj->setScaleX(currentScaleX * -1);
            }
        }
            break;
        case CharacterDirection::Right:
        {
            if (pressStatus == CharacterPressStatus::LongPressed) {
                if (isMoving) {
                    animationName = this->charName + "_" + "side_longtap_walk";
                } else {
                    animationName = this->charName + "_" + "side_longtap";
                }
            } else if (pressStatus == CharacterPressStatus::Tap) {
                animationName = this->charName + "_" + "side_tap";
            } else {
                animationName = this->charName + "_" + "side_normal";
            }
            if (currentScaleX > 0 && flipOnMove) {
                characterObj->setScaleX(currentScaleX * -1);
            }
        }
            break;
        default:
            break;
    }
    cocosbuilder::CCBAnimationManager *manager = (cocosbuilder::CCBAnimationManager*)characterObj->getUserObject();
    manager->setDelegate(this);
    auto aid = manager->getSequenceId(animationName.c_str());
    if (aid != -1){
        manager->runAnimationsForSequenceNamed(animationName.c_str());
    } else {
        // TODO: separate item class?
        // item
        if (pressStatus == CharacterPressStatus::LongPressed) {
            animationName = this->charName + "_" + "longtap";
        } else if (pressStatus == CharacterPressStatus::Tap) {
            animationName = this->charName + "_" + "tap";
        } else {
            animationName = this->charName + "_" + "default";
        }
        aid = manager->getSequenceId(animationName.c_str());
        
        if (aid != -1){
            manager->runAnimationsForSequenceNamed(animationName.c_str());
        } else {
            log("no animation : %s", animationName.c_str());
        }
    }
//    log("run animation : %s", animationName.c_str());
}

void Character::pause()
{
    paused = true;
    cocosbuilder::CCBAnimationManager *manager = (cocosbuilder::CCBAnimationManager*)characterObj->getUserObject();
    if (manager) {
        auto runningSequence = manager->getRunningSequenceName();
        if (runningSequence != nullptr) {
            pausedAnimation = runningSequence;
        }
    }
    pausedExpansion = isExpansionEffectIsOn;
    showExpansionEffect(false);
    this->pauseAllChild(this);
}

void Character::resume()
{
    paused = false;
    this->resumeAllChild(this);
    if (!pausedAnimation.empty()) {
        cocosbuilder::CCBAnimationManager *manager = (cocosbuilder::CCBAnimationManager*)characterObj->getUserObject();
        manager->setDelegate(this);
        manager->runAnimationsForSequenceNamed(pausedAnimation.c_str());
        pausedAnimation.clear();
    }
    
    if (pausedExpansion) {
        pausedExpansion = false;
        showExpansionEffect(true);
    }
}

void Character::pauseAllChild(Node *node)
{
    size_t childCount = node->getChildrenCount();
    if (childCount == 0) {
        node->pause();
    } else {
        for (int index = 0 ; index < childCount; index++) {
            this->pauseAllChild(node->getChildren().at(index));
        }
    }
}

void Character::resumeAllChild(Node *node)
{
    size_t childCount = node->getChildrenCount();
    if (childCount == 0) {
        node->resume();
    } else {
        for (int index = 0 ; index < childCount; index++) {
            this->resumeAllChild(node->getChildren().at(index));
        }
    }
}

void Character::completedAnimationSequenceNamed(const char *name)
{
//    log("completedAnimationSequenceNamed : %s", name);
    
    if (pressStatus == CharacterPressStatus::Tap) {
        pressStatus = CharacterPressStatus::None;
    }
    
    if (paused) return;
    
    
    this->runAnimationByDirection();
    
//    if (paused) {
//        paused = false;
//    } else {
//        cocosbuilder::CCBAnimationManager *manager = (cocosbuilder::CCBAnimationManager*)characterObj->getUserObject();
//        manager->setDelegate(this);
//        manager->runAnimationsForSequenceNamed("lion_side_longtap");
//        
//        //TODO: replay timeline
//        //    if (strcmp(name, "lion_side_longtap") == 0) return;
//    }
}

void Character::startLongPressTrigger()
{
    if(longPressTriggerAction == NULL) {
        auto delay = DelayTime::create(LONG_PRESS_THRESHOLD_SEC);
        longPressTriggerAction = Sequence::create(delay, CallFunc::create(CC_CALLBACK_0(Character::longPressed, this)), NULL);
        this->runAction(longPressTriggerAction);
    }
}

void Character::cancelLongPressTrigger()
{
    if (longPressTriggerAction != NULL){
        this->stopAction(longPressTriggerAction);
        longPressTriggerAction = NULL;
        log("long tap cancel");
    }
}

void Character::longPressed()
{
    log("long tap");
    setPressStatus(CharacterPressStatus::LongPressed);
    if (!paused){
        showExpansionEffect(true);
    }
    
//    pressStatus = CharacterPressStatus::LongPressed;
//    this->runAnimationByDirection();
    longPressTriggerAction = NULL;
}

void Character::pressed()
{
    log("tap");
    setPressStatus(CharacterPressStatus::Tap);
    
    if (!this->tapSoundFileName.empty()) {
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect(this->tapSoundFileName.c_str());
    }
    
//    pressStatus = CharacterPressStatus::Tap;
//    this->runAnimationByDirection();
//    pressStatus = CharacterPressStatus::None;
}

void Character::setPressStatus(CharacterPressStatus pressStatus)
{
    if (this->pressStatus == pressStatus) return;
    this->pressStatus = pressStatus;
    this->runAnimationByDirection();
}


void Character::setPosition(const cocos2d::Point& position)
{
    if (touchNumber == 1) {

        cocos2d::Point oldPosition =  convertToNodeSpace(this->getPosition());
        cocos2d::Point newPosition = convertToNodeSpace(position);
        
        auto deltaX = newPosition.x - oldPosition.x;
        auto deltaY = newPosition.y - oldPosition.y;
        
        CharacterDirection oldDirection = this->getDirection();
        CharacterDirection newDirection;
        float targetDelta;
        if (abs(deltaX) >= abs(deltaY)) {
            if (deltaX < 0) { // left
                newDirection = CharacterDirection::Left;
            } else { //right
                newDirection = CharacterDirection::Right;
            }
            targetDelta = deltaX;
            
            if (abs(targetDelta) > 0) {
                this->startHorizontalMove();
            }
        } else {
            // top or bottom
            newDirection = CharacterDirection::Front;
            targetDelta = deltaY;
            
            if(moveSoundAllDirection) {
                startVerticalMove();
            }
        }
        
        if(abs(targetDelta) > DIRECTION_CHANGE_THRESHOLD && oldDirection != newDirection) {
            this->setDirection(newDirection);
        }
    }
    Node::setPosition(position);
}

void Character::setPosition(float x, float y)
{
    setPosition(cocos2d::Point(x, y));
}

void Character::startVerticalMove()
{
    if(stopMoveTriggerAction != NULL){
        this->stopActionByTag(STOP_ACTION_TAG);
    }
    
//    if (oldScale == 0) {
//        oldScale = this->getScale();
//    }
//    
//    this->showExpansionEffect(false);
//    isMoving = true;
    
    auto delay = DelayTime::create(MOVE_STOP_THRESHOLD_SEC);
    stopMoveTriggerAction = Sequence::create(delay, CallFunc::create(CC_CALLBACK_0(Character::stopMove, this)), NULL);
    stopMoveTriggerAction->setTag(STOP_ACTION_TAG);
    this->runAction(stopMoveTriggerAction);
    
    playEffect();
}

void Character::startHorizontalMove()
{
    if(stopMoveTriggerAction != NULL){
        this->stopActionByTag(STOP_ACTION_TAG);
    }
    
    if (oldScale == 0) {
        oldScale = this->getScale();
    }
    
    this->showExpansionEffect(false);
    isMoving = true;
    auto delay = DelayTime::create(MOVE_STOP_THRESHOLD_SEC);
    stopMoveTriggerAction = Sequence::create(delay, CallFunc::create(CC_CALLBACK_0(Character::stopMove, this)), NULL);
    stopMoveTriggerAction->setTag(STOP_ACTION_TAG);
    this->runAction(stopMoveTriggerAction);
    
    playEffect();
}

bool Character::isPlayingEffect()
{
    return soundID != 0;
}

void Character::playEffect()
{
    if (!isPlayingEffect() && !this->moveSoundFileName.empty() && moveByRealTouch) {
        soundID = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect(this->moveSoundFileName.c_str(), true);
        log("sound ID : %d", soundID);
    }
}

void Character::stopEffect()
{
    if (isPlayingEffect() && moveByRealTouch) {
        CocosDenshion::SimpleAudioEngine::getInstance()->stopEffect(soundID);
        soundID = 0;
    }
}

void Character::stopMove()
{
    if (pressStatus == CharacterPressStatus::LongPressed && !paused) {
        this->showExpansionEffect(true);
    }
    
    stopEffect();
    
    isMoving = false;
    runAnimationByDirection();
}
