//
//  ChapterPlayScene.h
//  bobby
//
//  Created by Dongwook, Kim on 14. 5. 29..
//
//

#ifndef __bobby__ChapterPlayScene__
#define __bobby__ChapterPlayScene__

#include <iostream>
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "Character.h"
#include "ChapterPlayer.h"

USING_NS_CC;
USING_NS_CC_EXT;

class ChapterPlay : public cocos2d::LayerColor, ChapterPlayerDelegate
{
public:
    static cocos2d::Scene* createScene(__String* bookKey, int chapterIndex);
    static ChapterPlay* create(__String* bookKey, int chapterIndex);

    virtual bool init();
    virtual void update(float delta);
    void restoreCharacterProperty();
    void btnBackCallback(Ref* pSender);
    void playMenuCallback(Ref* pSender);
    
    void createProgressBar();
    Label *createTitle(int chapterIndex);
    void setProgress(float value);
    
    virtual void chapterPlayFinished(int chapterIndex);
    virtual void onTransitionFinished(int chapterIndex);

    bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event *event);
    void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event *event);
    void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event *event);
    void onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event *event);
    
    void play();
    void pausePlay();
    void resumePlay();
    void finishPlaying();

    void showUI(bool show);
    void triggerHideUI();
    void cancelHideUI();
    
    void onEnter();
    void onExit();
    
    virtual void onEnterTransitionDidFinish();

    CREATE_FUNC(ChapterPlay);

    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);

    int chapterIndex;
    __String* bookKey;
private:
    ChapterPlayer *player;
    
    __Array* characters;
    __String* voiceFileName;
    
    cocos2d::Action *hideUIAction;
    
    __Dictionary *characterDic;
    __Dictionary* characterProperties;
    
    cocos2d::MenuItemImage *playMenuItem;
    cocos2d::MenuItemImage *pauseMenuItem;
    
    cocos2d::MenuItemImage *btnBack;
    cocos2d::Menu *controlMenu;
    
//    cocos2d::Label *pageTitle;
    cocos2d::Node *playProgressBarBg;
    cocos2d::Node *playProgressBar;
    cocos2d::Node *playProgressBarHolder;

    cocos2d::__Dictionary *touchTargetMapping;
    cocos2d::__Dictionary *playingTouches;
    cocos2d::__Array *touchData;
    
    bool playing;
    bool paused;
    bool visibleUI;
    
    float playedTime;
    int playTouchTimeStampIndex;
    double playTouchElapsedTime;
    double accumulatedTouchTimeStamp;
    
    float zoomScaleX;
    float zoomScaleY;
};

#endif /* defined(__bobby__ChapterPlayScene__) */
