//
//  PopupLayer.h
//  Popup
//
//  Created by oasis on 2014. 5. 9..
//
//

#ifndef __Popup__PopupLayer__
#define __Popup__PopupLayer__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

#define NO_BUTTON_NAME "noButtonName"

typedef enum popup_type{
    POPUP_TYPE_WARNING = 0,
    POPUP_TYPE_COINS = 1,
    POPUP_TYPE_SHARED = 2,
    POPUP_TYPE_YEARS = 3,
} popup_type;

class PopupLayer;
class PopupLayerDelegate
{
public:
    virtual ~PopupLayerDelegate() {}
    virtual void popupButtonClicked(PopupLayer *popup, cocos2d::Ref *obj) = 0;
    virtual void popupListClicked(PopupLayer *popup, std::string selectedTitle) { };
};


class PopupLayer : public cocos2d::LayerColor, public TableViewDelegate, public TableViewDataSource
{
public:
    PopupLayer();
    virtual ~PopupLayer();
    static PopupLayer* create(popup_type type, const std::string &message, const std::string &btn1Name, const std::string &btn2Name = NO_BUTTON_NAME);
    PopupLayerDelegate *getDelegate() { return _delegate; }
    void setDelegate(PopupLayerDelegate *pDelegate) { _delegate = pDelegate; }
    void preventCloseSelf() { _preventClose = true; };

protected:
    PopupLayerDelegate *_delegate;

private:
    virtual void onEnter();
    virtual void onExit();
    virtual bool initWithMessage(popup_type type, const std::string &message, const std::string &btn1Name, const std::string &btn2Name);
    cocos2d::MenuItemSprite* getButton(const std::string& buttonName, float width);
	Layer* contentsLayer;
	void buttonClicked(Ref *pSender);
    cocos2d::extension::Scale9Sprite* getBottomBg();
    Sprite* getMiddleBackground(float height, cocos2d::Point bgPosition);
    cocos2d::extension::Scale9Sprite* getTopBg();
    Label* getTopLabel(const std::string &name);
    LayerColor* getPickerLine(cocos2d::Point position);
    
    __Array *listArr;
    popup_type popupType;
    
    //tableView
    virtual cocos2d::Size tableCellSizeForIndex(TableView *table, ssize_t idx);
    
    virtual TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);
    virtual ssize_t numberOfCellsInTableView(TableView *table);
    virtual void tableCellTouched(TableView* table, TableViewCell* cell);
    
    void scrollViewDidScroll(cocos2d::extension::ScrollView *view);
    void scrollViewDidZoom(cocos2d::extension::ScrollView *view);
    
    bool _pendingClose;
    bool _preventClose;

//#ifdef MACOS
//    int getMessageLabelHeight(const char* message, const char* fontName, int fontSize, int width);
//#endif
};

#endif /* defined(__Popup__PopupLayer__) */
