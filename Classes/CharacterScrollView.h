//
//  CharacterScrollView.h
//  bobby
//
//  Created by Dongwook, Kim on 14. 7. 4..
//
//

#ifndef __bobby__CharacterScrollView__
#define __bobby__CharacterScrollView__

#include <iostream>

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ScrollViewExtension.h"
#include "ui/CocosGUI.h"
#include "PopupLayer.h"

USING_NS_CC;
USING_NS_CC_EXT;

#define TAG_SCROLLVIEW 0

typedef enum char_super_type{
    CHAR_HOME = 0,
    CHAR_RECORD = 1,
} char_super_type;

class CharacterScrollView;

class CharacterScrollViewDelegate
{
public:
    virtual ~CharacterScrollViewDelegate() {}
    virtual void onSelectCharacter(CharacterScrollView* view, Node* selectedItem, __String* characterKey, int index) = 0;
};

class CharacterScrollView : public cocos2d::Layer, ScrollViewExtensionDelegate, PopupLayerDelegate
{
public:
    static CharacterScrollView* create(char_super_type superViewType);
    bool init(char_super_type superViewType);
    bool setSelectChracter(int index, bool select);
    int getCharacterIndex(std::string& charKey);

    bool addCharacter(std::string charKey);
    bool removeCharacter(__String *charKey);
    
    void rotateCharacters(float duration, float angle, bool animation);
    void reloadData();
    
    // scrollview extension delegate
    void scrollViewDidScroll(ScrollViewExtension* view);
    void scrollViewDidZoom(ScrollViewExtension* view);
    void scrollViewDidScrollEnd(ScrollViewExtension* view);
    void onSelectItem(ScrollViewExtension* view, Node* selectedItem, Touch *pTouch, Event *pEvent);
    
    CharacterScrollViewDelegate* getDelegate() { return _scrollViewDelegate; }
    void setDelegate(CharacterScrollViewDelegate* pDelegate) { _scrollViewDelegate = pDelegate; }
    
//    CREATE_FUNC(CharacterScrollView);
    
    __Array *deleteButtons;
    void popupButtonClicked(PopupLayer *popup, cocos2d::Ref *obj);
//    LayerColor *deleteModeDim;
    
    void scrollToLastItem();
    void scrollToFirstItem();

private:
//    bool init();
    void characterCoinClicked(Ref* pSender);
    void characterPressedCallback(Ref* pSender);
    cocos2d::Node* createCharacterForScrollView(std::string charKey, int index);
    void adjustLine();
    void loadJsonToScrollView(ScrollView *scrollView);

    cocos2d::Point scrollOffset;
    CharacterScrollViewDelegate *_scrollViewDelegate;
    ScrollViewExtension *createScrollView();
    ScrollViewExtension *scrollView;
    int linesCount;
    __Array* characterList;
    
    float currentCharacterAngle;

    bool scrollViewPressed;
    bool reachedToEnd;
    bool isScrollingToFirst;

    __Dictionary *items;
    __Array *pins;
    void deleteButtonClicked(Ref *obj, ui::TouchEventType type);
    void runRemoveCharacterAnimation(__String* bookKey);
    
    __String *selectedCharKey;
    Node *selDeleteChar;
    
    char_super_type superType;
    void setEnableDeleteButtons(bool enable);
    
    int fixItemCount;
    int parseErrorCount;
};

#endif /* defined(__bobby__CharacterScrollView__) */
