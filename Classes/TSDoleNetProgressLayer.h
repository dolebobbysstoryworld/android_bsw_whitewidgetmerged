//
//  TSDoleNetProgressLayer.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 30..
//
//

#ifndef _TSDOLENETPROGRESSLAYER_H_
#define _TSDOLENETPROGRESSLAYER_H_

#include "cocos2d.h"
#include "ToastLayer.h"
#include "share/Share.h"
#include "PopupLayer.h"
#include "TSDoleProtocol.h"
#include "HomeScene.h"

NS_CC_BEGIN

#define TAG_NET_PROGRESS            321123321

enum ShareTarget {
    None,
    Youtube,
    Facebook,
};

class TSDoleNetProgressDelegate {
public:
    virtual ~TSDoleNetProgressDelegate() {}
    
    virtual void OnProcessEnded(bool result, void *userObject) = 0;
};

class TSDoleNetProgressLayer : public LayerColor, public ToastLayerDelegate, public Share::OnResultCallback,
public Share::OnProgressCallback, public PopupLayerDelegate, public TSDoleProtocolDelegate
{
public:
    TSDoleNetProgressLayer();
    virtual ~TSDoleNetProgressLayer();
    static TSDoleNetProgressLayer* create();
    void setDelegate(TSDoleNetProgressDelegate * pDelegate) { _delegate = pDelegate; }
    
    void activityStart();
    void activityStop(const char *message, float closetime = 2, bool autocloseForProgressLayer = true);


    void startYouTubeShare(const char *path, const char* bookTitle, const char *comment, Home *home);
    void startFacebookShare(const char *path, const char* bookTitle, const char *comment, Home *home);
    
    void initProgress(int total);
    void showProgress(double current);
    void endProgress(const char *completed, float closetime = 2, bool autocloseForProgressLayer = true);
    void popupButtonClicked(PopupLayer *popup, cocos2d::Ref *obj);
    void showMessagePopup(const char* message);
    void onPostUploadVideo();
    void onSuccessCallback(Share::Client* client, int errorCode, Share::KeyJSONValueMap& resultMap);
    void onFailedCallback(Share::Client* client, int errorCode, Share::KeyJSONValueMap& resultMap);
    void onCancelledCallback(Share::Client* client);
    void onProgressCallback(Share::Client* client, double uploadTotal, double uploadedNow);

protected:
    bool init();
    virtual void onEnter();
    virtual void onExit();
    void removeProgressLayer();
    virtual void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event *event);
    void onToastRemoved(ToastLayer *toast);

    void onRequestStarted(TSDoleProtocol *protocol, TSNetResult result);
    void onResponseEnded(TSDoleProtocol *protocol, TSNetResult result);
    void onProtocolFailed(TSDoleProtocol *protocol, TSNetResult result);
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    void onRequestCanceled(TSDoleProtocol *protocol);
//#endif
    enum class ShareTarget {
        None,
        Youtube,
        Facebook,
    };
    ShareTarget _shareTarget;

protected:
    TSDoleNetProgressDelegate *_delegate;
    bool _animated;
    
    bool _progressed;
    int _total;
    ProgressTimer *_progressTimer;
    std::string _videoPath;
    TSDoleChargeFreeCash *_protocol;
    Home *_homeLayer;
};

NS_CC_END


#endif /* defined(_TSDOLENETPROGRESSLAYER_H_) */
