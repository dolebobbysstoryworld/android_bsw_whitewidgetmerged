//
//  TSDoleProtocol.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 30..
//
//

#ifndef _TSDOLEPROTOCOL_H_
#define _TSDOLEPROTOCOL_H_

#include "cocos2d.h"
#include "external/json/document.h"
#include "network/HttpClient.h"
#include "DoleProduct.h"
#include "ToastLayer.h"

NS_CC_BEGIN

class TSNetToastLayer : public ToastLayer {
public:
    TSNetToastLayer() {};
    virtual ~TSNetToastLayer() {};
    static inline void show(Node *parent) {
        auto toastLayer = ToastLayer::create(Device::getResString(Res::Strings::CM_NET_WARNING_NETWORK_UNSTABLE), 2);// "The network is unstable.\nPlease try again"
        toastLayer->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        toastLayer->setPosition(cocos2d::Point::ZERO);
        parent->addChild(toastLayer);
    }
};

enum class TSNetResult {
    Success = 0,
    ErrorResultFail,
    ErrorNetworkDisable,
    ErrorConnection,
    ErrorNotAllowed,
    ErrorServiceNotFound,
    ErrorWrongMessage,
    ErrorTimeout,
    ErrorBadURL
};

//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
enum class TSProtocolStatus {
	Finished = 0,
	Normal,
	Canceled
};
//#endif

class TSDoleProtocol;
class TSDoleGetReNewAuthKey;

class TSDoleProtocolDelegate {
public:
    virtual ~TSDoleProtocolDelegate() {}
    
    virtual void onRequestStarted(TSDoleProtocol *protocol, TSNetResult result) = 0;
    virtual void onResponseEnded(TSDoleProtocol *protocol, TSNetResult result) = 0;
    virtual void onProtocolFailed(TSDoleProtocol *protocol, TSNetResult result) = 0;
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    virtual void onRequestCanceled(TSDoleProtocol *protocol) = 0;
//#endif
};

class TSDoleProtocol : public Ref, public TSDoleProtocolDelegate {
public:
    TSDoleProtocol();
    virtual ~TSDoleProtocol();

protected:
    virtual void init();
    
public:
    inline void setDelegate(TSDoleProtocolDelegate *delegate) { _delegate = delegate; }
    bool request();
    inline void setTag(int tag) { _tag = tag; }
    inline int getTag() { return _tag; }
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    inline void setStatus(TSProtocolStatus status) { _status = status; };
    inline TSProtocolStatus getStatus() { return _status; };
//#endif
    inline int getReturnCode() { return _returnCode; }
    inline int getReturnValue() { return _returnValue; }
    
protected:
    void setRequestData(rapidjson::Document *postJson, std::string apiGroup, std::string apiName);
    virtual bool processResponsData(std::string responseJson) = 0;
    
    void updateAuthkey();
    virtual void onUpdateAuthkey() { };
    void onRequestStarted(TSDoleProtocol *protocol, TSNetResult result);
    void onResponseEnded(TSDoleProtocol *protocol, TSNetResult result);
    void onProtocolFailed(TSDoleProtocol *protocol, TSNetResult result);
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    void onRequestCanceled(TSDoleProtocol *protocol);
//#endif
    
private:
    void onHttpRequestCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);
    
private:
    int _tag;
    cocos2d::network::HttpRequest *_httpRequest;
    
protected:
    TSDoleProtocolDelegate *_delegate;

    bool _returnValue;
    int _returnCode;
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    TSProtocolStatus _status;
//#endif

    bool _updateAuthKey;
    TSDoleGetReNewAuthKey *_protocolUpdateAuthKey;
};

#pragma mark - Profile API
class TSDoleGetAppVersion : public TSDoleProtocol {
public:
    TSDoleGetAppVersion() {};
    virtual ~TSDoleGetAppVersion() {};
    
    void init();
    std::string getAppUrl() { return _appUrl; }
    std::string getAppVersion() { return _appVersion; }
protected:
    bool processResponsData(std::string responseJson);
    
private:
    std::string _appUrl;
    std::string _appVersion;
};


#pragma mark - Auth API
class TSDoleGetReNewAuthKey : public TSDoleProtocol {
public:
    TSDoleGetReNewAuthKey() {};
    virtual ~TSDoleGetReNewAuthKey() {};
    
    void init(int userNo, std::string authKey);
    
protected:
    bool processResponsData(std::string responseJson);
    
private:
};

#pragma mark - Billing API
class TSDoleGetBalance : public TSDoleProtocol {
public:
    TSDoleGetBalance() {};
    virtual ~TSDoleGetBalance() {};
    
    void init(int userNo);
    inline int getBalance() { return _balance; }
    
protected:
    bool processResponsData(std::string responseJson);
    
private:
    int _balance;
};

class TSDoleChargeFreeCash : public TSDoleProtocol {
public:
    TSDoleChargeFreeCash() {};
    virtual ~TSDoleChargeFreeCash() {};
    
    void init(int coin);
    void onUpdateAuthkey();
    bool processResponsData(std::string responseJson);

private:
    int _coin;
};

#pragma mark - CMS API
class TSDoleGetEventNoticeList : public TSDoleProtocol {
public:
    TSDoleGetEventNoticeList() { _eventList = false; };
    virtual ~TSDoleGetEventNoticeList() {};
    
    void init();
    bool processResponsData(std::string responseJson);
    
    inline std::string getEventURL() { return _eventURL; }
    inline std::string getEventBeginDate() { return _beginDate; }
    inline std::string getEventEndDate() { return _endDate; }
    inline bool isEventList() { return _eventList; }
    
private:
    std::string _eventURL;
    std::string _beginDate;
    std::string _endDate;
    bool _eventList;
};

#pragma mark - Shop API
//GetInventoryList
class TSDoleGetInventoryList : public TSDoleProtocol {
public:
    TSDoleGetInventoryList() {};
    virtual ~TSDoleGetInventoryList() {};
    
    void init(std::string authKey, __Array *productList);
    bool processResponsData(std::string responseJson);
    
private:
    __Array *_products;
};

class TSDoleGetCacheSyncVersion : public TSDoleProtocol {
public:
    TSDoleGetCacheSyncVersion() {};
    virtual ~TSDoleGetCacheSyncVersion() {};
    
    void init(std::string authKey);
    bool processResponsData(std::string responseJson);
    
    double getVersion() { return _cacheVersion; }
    
private:
    double _cacheVersion;
};

class TSDoleExecutePurchase : public TSDoleProtocol {
public:
    TSDoleExecutePurchase() {};
    virtual ~TSDoleExecutePurchase() {};
    
    void init(DoleProduct *product);
    void onUpdateAuthkey();
    bool processResponsData(std::string responseJson);
    
public:
    DoleProduct *_product;
};

NS_CC_END

#endif /* defined(_TSDOLEPROTOCOL_H_) */
