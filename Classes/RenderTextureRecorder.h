//
//  RenderTextureRecorder.h
//  bobby
//
//  Created by Dongwook, Kim on 14. 5. 12..
//
//

#ifndef __bobby__RenderTextureRecorder__
#define __bobby__RenderTextureRecorder__

#include <iostream>
#include "cocos2d.h"

class RenderTextureRecorder : public cocos2d::RenderTexture
{
public:
    static RenderTextureRecorder * create();
    
    bool initWithWidthAndHeight(int w, int h, cocos2d::Texture2D::PixelFormat format, GLuint depthStencilFormat, const void* textureData, GLuint textureName, GLint oldFBO, GLint FBO, GLint oldRBO);

    virtual void update(float delta);
    void(*drawCallback)();
};

#endif /* defined(__bobby__RenderTextureRecorder__) */
