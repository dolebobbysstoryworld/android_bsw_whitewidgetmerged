//
//  Util.h
//  bobby
//
//  Created by oasis on 2014. 6. 20..
//
//

#ifndef __bobby__Util__
#define __bobby__Util__

#include "cocos2d.h"
USING_NS_CC;

class Util {
public:
    static void splitTitleString(const std::string &orgString, char delim, std::string &firstLine, std::string &secondLine, int width, const char* fontName, int fontSize);
    static float getCharacterDefulatScale();
    static float getCharacterMaxScale();
    static float getCharacterMinScale();

    static float getCharacterScaleFactor();
    static float getCharacterExtendZoomScale();

    static std::string getMainPhotoPath(std::string &photoType);
    static std::string getThumbnailPhotoPath(std::string &photoType);
    static std::string getBookThumbnailPhotoPath(std::string &photoType);
    static std::string getBookCoverPhotoPath(std::string &photoType);

    static void removeParseErrorObject(const char *orderKeyType, __String *objectKey);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    static std::string saveCustomCharacterData(int index, const char* path);
    static std::string saveCustomBackgroundData(const char* path);
    static time_t timegm(struct tm* tm);
#endif
    
    static long getItemLeftTime(const char* registerTime, int expiredHour);

private:
    static void splitStringByletter(const std::string &orgString, std::string &firstLine, std::string &secondLine, int width, const char* fontName, int fontSize);
};
#endif /* defined(__bobby__Util__) */
