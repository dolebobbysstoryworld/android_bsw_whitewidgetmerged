//
//  OpeningScene.cpp
//  bobby
//
//  Created by Dongwook, Kim on 14. 7. 10..
//
//

#include "OpeningScene.h"
#include "ResourceManager.h"
#include "Define.h"
#include "LabelExtension.h"
#include "Util.h"
#include "SimpleAudioEngine.h"

#define ZOOM_DURATION   0.5f
#define SHIRINK_RATIO   (100.0f/61.0f)
#define OPEN_DELAY_RATE  0.8f
#define CURTAIN_REDANDUNCY  0.016f;

Opening* Opening::createOpening(__String *title, __String *author)
{
    Opening * opening = new Opening();
    if( opening && opening->init(title, author))
    {
        opening->autorelease();
        return opening;
    }
    CC_SAFE_DELETE(opening);
    return nullptr;
}

bool Opening::init(__String *title, __String *author)
{
    if (!LayerColor::initWithColor(cocos2d::Color4B(0,0,0,0))) {
        return false;
    }
    
    this->title = title;
    this->author = author;
    
    animationStarted = false;
    animationPaused = false;
    
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    this->setContentSize(visibleSize);

    // create bg
    bgSprite = Sprite::create("opening_bg_01.png");
    bgSprite->setAnchorPoint(cocos2d::Point::ANCHOR_TOP_LEFT);
    bgSprite->setPosition(cocos2d::Point(0, visibleSize.height));
    bgSprite->setScale(Util::getCharacterScaleFactor());
    this->addChild(bgSprite, 1);

    // create title
    titleNode = this->createTitleNode(title, author);
    titleNode->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_TOP);
    titleNode->setPosition(cocos2d::Point(visibleSize.width/2, visibleSize.height - OPENING_TITLE_NODE_TOP_PADDING));
    this->addChild(titleNode, 2);

    // create curtain
    cocosbuilder::NodeLoaderLibrary *nodeLoaderLibrary = cocosbuilder::NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
    cocosbuilder::CCBReader *ccbReader = new cocosbuilder::CCBReader(nodeLoaderLibrary);
    curtain = (Sprite*)ccbReader->readNodeGraphFromFile("curtain.ccbi", this);
    this->addChild(curtain, 0);
    ccbReader->release();
    
    cocosbuilder::CCBAnimationManager *manager = (cocosbuilder::CCBAnimationManager*)curtain->getUserObject();
    curtainAnimationDuration = manager->getSequenceDuration("curtain") + CURTAIN_REDANDUNCY;
    
    bgAction = nullptr;
    curtainAction = nullptr;
    finishAction = nullptr;
    
    // prepare animation
    initAnimation();
    
    return true;
}

void Opening::playBackgroundMusic()
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("preload_sounds/scene_opening.wav", false);
}

Node *Opening::createTitleNode(__String *title, __String *author)
{
    // create bg
    Node *titleNode = Node::create();
    
    // create title label
    std::string stripTitle = title->getCString();
    while(std::isspace(*stripTitle.rbegin()))
        stripTitle.erase(stripTitle.length()-1);
    
    int titleStringWidth = Device::getTextWidth(stripTitle.c_str(), FONT_NAME, OPENING_DOUBLE_TITLE_LABEL_FONT_SIZE);
    bool isDoubleLine = (titleStringWidth > OPENING_DOUBLE_TITLE_LABEL_SIZE.width)?true:false;
    
    int titleStringWidth2 = Device::getTextWidth(stripTitle.c_str(), FONT_NAME, OPENING_SINGLE_TITLE_LABEL_FONT_SIZE);
    isDoubleLine = isDoubleLine || (titleStringWidth2 > OPENING_SINGLE_TITLE_LABEL_SIZE.width)?true:false;
    
    auto fontSize = isDoubleLine?OPENING_DOUBLE_TITLE_LABEL_FONT_SIZE:OPENING_SINGLE_TITLE_LABEL_FONT_SIZE;
    auto dimensions = isDoubleLine?OPENING_DOUBLE_TITLE_LABEL_SIZE:OPENING_SINGLE_TITLE_LABEL_SIZE;
    
    auto titleLabel = LabelExtension::create(stripTitle, FONT_NAME, fontSize, dimensions, TextHAlignment::CENTER, TextVAlignment::CENTER);
    titleLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    titleLabel->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_BOTTOM);
    titleLabel->setColor(Color3B(0x4b,0x3b,0x3b));
    auto shadowDistance = OPENING_TITLE_LABEL_SHADOW_DISTANCE;
    titleLabel->enableShadow(Color4B(77,62,62,255.0f*0.4f), cocos2d::Size(0,-1*shadowDistance), 0);
    
    LabelExtension *subTitleLabel = nullptr;
    float maxTitleWidth = 0;
    
    if (isDoubleLine) {
        std::string openingTitleMain, openingTitleSub;
        Util::splitTitleString(stripTitle, ' ', openingTitleMain, openingTitleSub, OPENING_DOUBLE_TITLE_LABEL_SIZE.width, FONT_NAME_BOLD, fontSize);
        
        titleLabel->setString(openingTitleMain);
        
        subTitleLabel = LabelExtension::create(openingTitleSub, FONT_NAME, fontSize, OPENING_DOUBLE_TITLE_LABEL_SIZE,
                                                              TextHAlignment::CENTER, TextVAlignment::CENTER);
        subTitleLabel->ellipsis(FONT_NAME, fontSize);
        subTitleLabel->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_BOTTOM);
        subTitleLabel->setColor(Color3B(0x4b,0x3b,0x3b));
        subTitleLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
        subTitleLabel->enableShadow(Color4B(77,62,62,255.0f*0.4f), cocos2d::Size(0,-1*shadowDistance), 0);
        
        maxTitleWidth = MAX(Device::getTextWidth(titleLabel->getString().c_str(), FONT_NAME, OPENING_DOUBLE_TITLE_LABEL_FONT_SIZE),
                            Device::getTextWidth(subTitleLabel->getString().c_str(), FONT_NAME, OPENING_DOUBLE_TITLE_LABEL_FONT_SIZE));
    } else {
        maxTitleWidth = Device::getTextWidth(titleLabel->getString().c_str(), FONT_NAME, OPENING_SINGLE_TITLE_LABEL_FONT_SIZE);
    }
    
    // create author label
    std::string stripAuthor = author->getCString();
    while(std::isspace(*stripAuthor.rbegin()))
        stripAuthor.erase(stripAuthor.length()-1);
    
    auto authorStar = Sprite::create("opening_title_star.png");
    
    auto authorShadowDistance = OPENING_AUTHOR_LABEL_SHADOW_DISTANCE;
    auto marginInAuthorTextArea = authorStar->getContentSize().width + OPENING_AUTHOR_LABEL_STAR_SIDE_PADDING * 2;
    auto authorLabelSize = cocos2d::Size(OPENING_AUTHOR_TEXT_MAX_WIDTH - marginInAuthorTextArea * 2, OPENING_AUTHOR_LABEL_SIZE.height);
    auto authorLabel = LabelExtension::create(stripAuthor, FONT_NAME_BOLD, OPENING_AUTHOR_LABEL_FONT_SIZE, authorLabelSize,
                                              TextHAlignment::CENTER, TextVAlignment::CENTER);
    authorLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    authorLabel->setColor(Color3B(0xfc,0xa6,0x1e));
    authorLabel->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_TOP);
    authorLabel->ellipsis(FONT_NAME_BOLD, OPENING_AUTHOR_LABEL_FONT_SIZE);
    authorLabel->enableShadow(Color4B(77,62,62,255.0f*0.3f), cocos2d::Size(0,-1*authorShadowDistance), 0);

    // calc max text width
    float authorTextWidth = Device::getTextWidth(authorLabel->getString().c_str(), FONT_NAME_BOLD, OPENING_AUTHOR_LABEL_FONT_SIZE);
    
    log("authorText : %s, width : %f", authorLabel->getString().c_str(), authorTextWidth);
    
    float textSideMargin = 0;
    float theLongestTextWidth = 0;
    if (maxTitleWidth > authorTextWidth) { // title is longer than author
        theLongestTextWidth = maxTitleWidth;
        textSideMargin = isDoubleLine? OPENING_DOUBLE_TITLE_LABEL_01.x:OPENING_SINGLE_TITLE_LABEL.x;
    } else {
        theLongestTextWidth = authorTextWidth;
        textSideMargin = OPENING_AUTHOR_LABEL_SIDE_MARGIN + marginInAuthorTextArea;
    }
    
    cocosbuilder::NodeLoaderLibrary *nodeLoaderLibrary = cocosbuilder::NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
    
    cocosbuilder::CCBReader *ccbReader = new cocosbuilder::CCBReader(nodeLoaderLibrary);
    Sprite *titleBgLeft = (Sprite*)ccbReader->readNodeGraphFromFile("title_bg_left.ccbi", this);
    titleBgLeft->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    titleBgLeft->setScale(Util::getCharacterScaleFactor());
    ccbReader->release();
    
    cocosbuilder::CCBReader *ccbReader2 = new cocosbuilder::CCBReader(nodeLoaderLibrary);
    Sprite *titleBgCenter = (Sprite*)ccbReader2->readNodeGraphFromFile("title_bg_center.ccbi", this);
    titleBgCenter->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    titleBgCenter->setScale(Util::getCharacterScaleFactor());
    ccbReader2->release();
    
    cocosbuilder::CCBReader *ccbReader3 = new cocosbuilder::CCBReader(nodeLoaderLibrary);
    Sprite *titleBgRight = (Sprite*)ccbReader3->readNodeGraphFromFile("title_bg_right.ccbi", this);
    titleBgRight->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    titleBgRight->setScale(Util::getCharacterScaleFactor());
    ccbReader3->release();
    
    float totalWidth = theLongestTextWidth + textSideMargin * 2;

    float leftWidth =  titleBgLeft->getContentSize().width * Util::getCharacterScaleFactor();
    float rightWidth =  titleBgRight->getContentSize().width * Util::getCharacterScaleFactor();
    float targetCenterWidth = totalWidth - leftWidth - rightWidth;
    
    if (targetCenterWidth <= OPENING_TITLE_CENTER_MIN_WIDTH) {
        targetCenterWidth = OPENING_TITLE_CENTER_MIN_WIDTH;
        totalWidth = targetCenterWidth + leftWidth + rightWidth;
    } else if (targetCenterWidth >= OPENING_TITLE_CENTER_MAX_WIDTH) {
        targetCenterWidth = OPENING_TITLE_CENTER_MAX_WIDTH;
        totalWidth = targetCenterWidth + leftWidth + rightWidth;
    }

    // composite bg
    titleBgLeft->setPosition(cocos2d::Point::ZERO);
    titleNode->addChild(titleBgLeft, 0);
    
    auto currentCenterPosition = cocos2d::Point(leftWidth, 0);
    
    titleBgCenter->setPosition(currentCenterPosition);
    titleNode->addChild(titleBgCenter, 0);
    
    float centerWidth = titleBgCenter->getContentSize().width * Util::getCharacterScaleFactor();
    
    auto centerSpriteNum = ceil(targetCenterWidth / centerWidth);
    
    for (int index = 1 ; index < centerSpriteNum ; index++)
    {
        cocosbuilder::CCBReader *ccbReader = new cocosbuilder::CCBReader(nodeLoaderLibrary);
        Sprite *titleBgCenter = (Sprite*)ccbReader->readNodeGraphFromFile("title_bg_center.ccbi", this);
        currentCenterPosition = currentCenterPosition + cocos2d::Point(centerWidth, 0);
        titleBgCenter->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        titleBgCenter->setPosition(currentCenterPosition);
        titleBgCenter->setScale(Util::getCharacterScaleFactor());
        titleNode->addChild(titleBgCenter, 0);
        ccbReader->release();
    }
    auto bgRightPosition = currentCenterPosition + cocos2d::Point(centerWidth, 0);
    titleBgRight->setPosition(bgRightPosition);
    titleNode->addChild(titleBgRight, 0);
    
    float nodeTotalWidth = leftWidth + (centerWidth * centerSpriteNum) + rightWidth;
    titleNode->setContentSize(cocos2d::Size(nodeTotalWidth, titleBgLeft->getContentSize().height * Util::getCharacterScaleFactor()));

    // add all label
    titleLabel->setPosition(cocos2d::Point(nodeTotalWidth/2, isDoubleLine?OPENING_DOUBLE_TITLE_LABEL_01.y:OPENING_SINGLE_TITLE_LABEL.y));
    titleNode->addChild(titleLabel, 1);

    if (subTitleLabel != nullptr) {
        subTitleLabel->setPosition(cocos2d::Point(nodeTotalWidth/2, OPENING_DOUBLE_TITLE_LABEL_02.y));
        titleNode->addChild(subTitleLabel, 1);
    }
    
    authorLabel->setPosition(cocos2d::Point(nodeTotalWidth/2, titleNode->getContentSize().height - OPENING_AUTHOR_LABEL_TOP_PADDING));
    titleNode->addChild(authorLabel, 1);
    
    // create author label side stars
    auto leftStar = Sprite::create("opening_title_star.png");
    leftStar->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
    leftStar->setPosition(cocos2d::Point(nodeTotalWidth/2 - authorTextWidth/2 - OPENING_AUTHOR_LABEL_STAR_SIDE_PADDING,
                                         authorLabel->getPosition().y - authorLabel->getContentSize().height + OPENING_AUTHOR_LABEL_STAR_BOTTOM_PADDING));
    titleNode->addChild(leftStar, 1);
    
    auto rightStar = Sprite::create("opening_title_star.png");
    rightStar->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    rightStar->setPosition(cocos2d::Point(nodeTotalWidth/2 + authorTextWidth/2 + OPENING_AUTHOR_LABEL_STAR_SIDE_PADDING,
                                          authorLabel->getPosition().y - authorLabel->getContentSize().height + OPENING_AUTHOR_LABEL_STAR_BOTTOM_PADDING));
    titleNode->addChild(rightStar, 1);
    
    return titleNode;
}

void Opening::initAnimation()
{
    elapsedAnimationTime = 0;
    accElapsedAnimationTime = 0;
    
    animationStarted = false;
    
    bgSprite->setVisible(true);
    
    this->setPosition(cocos2d::Point::ZERO);
    this->setScale(1.0f);
    
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();

    curtain->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_TOP);
    float scale = 0.61 * Util::getCharacterScaleFactor();
    curtain->setScale(scale);
    curtain->setVisible(true);
    curtain->setPosition(cocos2d::Point(visibleSize.width/2, visibleSize.height - OPENING_TITLE_NODE_TOP_PADDING - titleNode->getContentSize().height));
    
    cocosbuilder::CCBAnimationManager *manager = (cocosbuilder::CCBAnimationManager*)curtain->getUserObject();
    manager->runAnimationsForSequenceNamed("curtain_ready");
}

void Opening::runAnimationDurationOffset(float durationOffset)
{
    log("runAnimationDurationOffset : %f", durationOffset);
    
    Vector<FiniteTimeAction*> actions;
    
    float startDelay = OPENING_DELAY_DURATION - durationOffset;
    
    if (startDelay > 0) {
        auto delayAction = DelayTime::create(startDelay);
        actions.pushBack(delayAction);
    }
    
    float zoomDuration = ZOOM_DURATION - (durationOffset - OPENING_DELAY_DURATION);
    if (zoomDuration > 0) {
        zoomDuration = MIN(ZOOM_DURATION, zoomDuration);
        auto scaleAnimation = ScaleTo::create(zoomDuration, SHIRINK_RATIO);
        
        cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
        float expandedHeight = curtain->getPositionY() * SHIRINK_RATIO - curtain->getPositionY();
        float expandedWidth = curtain->getPositionX() * SHIRINK_RATIO - curtain->getPositionX();

        float destY = (visibleSize.height - curtain->getPositionY() - expandedHeight);

        auto moveAnimation = MoveTo::create(zoomDuration, cocos2d::Point(-expandedWidth, destY));
        auto spawn = Spawn::create(scaleAnimation, moveAnimation, NULL);
        auto afterCallback = CallFunc::create([&](){
            log("bg move callback : paused : %d", animationPaused);
            if (animationPaused) return;
            bgSprite->setVisible(false);
        });
        
        auto spawnSequence = Sequence::create(spawn, afterCallback, NULL);
        actions.pushBack(spawnSequence);
    }

    if (actions.size() > 0) {
        auto bgAction = Sequence::create(actions);
        this->cocos2d::Node::runAction(bgAction);
    }
    
    auto openDelay = (OPENING_DELAY_DURATION + ZOOM_DURATION * OPEN_DELAY_RATE) - durationOffset;
    
    if (openDelay > 0) {
        log("try to start curtain delay remained : %f", openDelay);

        auto openDelayAction = DelayTime::create(openDelay);
        auto openCallback = CallFunc::create([&](){
            log("curtain open action callback.");
            if (animationPaused) return;
            cocosbuilder::CCBAnimationManager *manager = (cocosbuilder::CCBAnimationManager*)curtain->getUserObject();
            manager->setDelegate(this);
            manager->runAnimationsForSequenceNamed("curtain");
        });
        auto curtainAction = Sequence::create(openDelayAction, openCallback, NULL);
        this->cocos2d::Node::runAction(curtainAction);
    } else {
        log("curtain open action is started already.");
        
        resumeAllChildren(curtain);
        
//        auto openCallback = CallFunc::create([&](){
//            log("curtain open action callback2.");
//            if (animationPaused) return;
//            cocosbuilder::CCBAnimationManager *manager = (cocosbuilder::CCBAnimationManager*)curtain->getUserObject();
//            manager->setDelegate(this);
//            manager->runAnimationsForSequenceNamedTweenDuration("curtain", -openDelay);
//        });
//        auto curtainAction = Sequence::create(openCallback, NULL);
//        this->cocos2d::Node::runAction(curtainAction);
    }
    
    float totalAnimationDuration = OPENING_DELAY_DURATION + (ZOOM_DURATION * OPEN_DELAY_RATE) + curtainAnimationDuration;
    float remainAnimationDuration = totalAnimationDuration - elapsedAnimationTime;
    if (remainAnimationDuration > 0) {
        auto delay = DelayTime::create(remainAnimationDuration);
        auto finishAction = Sequence::create(delay, CallFunc::create(CC_CALLBACK_0(Opening::finishedOpening, this)), NULL);
        this->cocos2d::Node::runAction(finishAction);
    }
}

void Opening::stopAction()
{
    this->stopAllActions();
    pauseAllChildren(titleNode);
    pauseAllChildren(curtain);
}

void Opening::startAnimation()
{
    log("start opening");
    runAnimationDurationOffset(0);
    animationStarted = true;
    animationStartTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch());
}

void Opening::pauseAnimation()
{
    log("pause opening");
    if (animationStarted) {
        CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
        animationPaused = true;
        auto now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch());
        std::chrono::duration<double> diff = now - animationStartTime;
        elapsedAnimationTime = diff.count();
        accElapsedAnimationTime += elapsedAnimationTime;
        stopAction();
    }
}

void Opening::resumeAnimation()
{
    log("resume opening");
    if (animationPaused) {
        CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
        animationPaused = false;
        runAnimationDurationOffset(accElapsedAnimationTime);
        resumeAllChildren(titleNode);
        animationStartTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch());
//        resumeAllChildren(curtain);
    }
}

void Opening::pauseAllChildren(Node *node)
{
    auto children = node->getChildren();
    auto childrenCount = node->getChildrenCount();
    
    for (size_t index = 0; index< childrenCount; index++) {
        Node* child = (Node*)children.at(index);
        child->pause();
    }
}

void Opening::resumeAllChildren(Node *node)
{
    auto children = node->getChildren();
    auto childrenCount = node->getChildrenCount();
    
    for (size_t index = 0; index< childrenCount; index++) {
        Node* child = (Node*)children.at(index);
        child->resume();
    }
}

void Opening::completedAnimationSequenceNamed(const char *name)
{
    
}

void Opening::finishedOpening()
{
    log("finishedOpening! ispaused : %d", animationPaused);

    if (animationPaused) return;
    
    animationStarted = false;
    curtain->setVisible(false);
    if(_delegate != nullptr){
        CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
        _delegate->onOpeningFinished(this);
    }
}

float Opening::getTotalDuration()
{
    return OPENING_DELAY_DURATION + (ZOOM_DURATION * OPEN_DELAY_RATE) + curtainAnimationDuration;
}
