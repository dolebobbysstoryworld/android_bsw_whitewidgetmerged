//
//  MenuListLayer.cpp
//  bobby
//
//  Created by oasis on 2014. 7. 29..
//
//

#include "MenuListLayer.h"
#include "ResourceManager.h"
#include "Define.h"
#include "SimpleAudioEngine.h"

#define SPACE 3.2

MenuListLayer::~MenuListLayer() {}

MenuListLayer* MenuListLayer::create(list_type listType)
{
    MenuListLayer *layer = new MenuListLayer();
    if (layer && layer->initWithType(listType))
    {
        layer->autorelease();
        return layer;
    }
    CC_SAFE_DELETE(layer);
    return nullptr;
}

bool MenuListLayer::initWithType(list_type listType)
{
    if ( !LayerColor::initWithColor(Color4B(0, 0, 0, 0)) )
    {
        return false;
    }
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    
    if (listType == LIST_TYPE_01) {
        int stringWidth = Device::getTextWidth(Device::getResString(Res::Strings::CC_HOME_MENUTITLE_SETTING), FONT_NAME, HOME_SETTING_LABEL_FONTSIZE);
        
        auto settingImg = Sprite::create("home_option_menu_icon_settings.png");
        settingImg->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        settingImg->setPosition(HOME_SETTING_ICON);
        
        listBg = cocos2d::extension::Scale9Sprite::create("home_option_menu_bg.png", cocos2d::Rect(0,0,HOME_SETTING_BG_IMAGE_SIZE.width,HOME_SETTING_BG_IMAGE_SIZE.height),cocos2d::Rect(HOME_SETTING_BG_CAPINSETS.x,HOME_SETTING_BG_CAPINSETS.y,HOME_SETTING_BG_CAPINSETS_SIZE.width,HOME_SETTING_BG_CAPINSETS_SIZE.height)); //52,66,110,70 / 32,34,38,38
//        listBg->setContentSize(HOME_SETTING_BG_SIZE);
        listBg->setContentSize(cocos2d::Size(settingImg->getContentSize().width*SPACE+stringWidth,HOME_SETTING_BG_SIZE.height));
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        listBg->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
        listBg->setPosition(cocos2d::Point(visibleSize.width-HOME_SETTING_BG.x,HOME_SETTING_BG.y - HOME_SETTING_BG_SIZE.height));
#else
        listBg->setAnchorPoint(cocos2d::Point::ANCHOR_TOP_RIGHT);
        listBg->setPosition(cocos2d::Point(visibleSize.width-HOME_SETTING_BG.x,HOME_SETTING_BG.y));
#endif
        this->addChild(listBg);
        
//        auto normalSprite = cocos2d::extension::Scale9Sprite::create("home_option_menu_bg_1line_normal.png", cocos2d::Rect(0,0,HOME_SETTING_LIST_PRESS_IMAGE_SIZE.width,HOME_SETTING_LIST_PRESS_IMAGE_SIZE.height),cocos2d::Rect(HOME_SETTING_LIST_PRESS_CAPINSETS.x,HOME_SETTING_LIST_PRESS_CAPINSETS.y,HOME_SETTING_LIST_PRESS_CAPINSETS_SIZE.width,HOME_SETTING_LIST_PRESS_CAPINSETS_SIZE.height));
//        
//        auto pressSprite = cocos2d::extension::Scale9Sprite::create("home_option_menu_bg_1line_pressed.png", cocos2d::Rect(0,0,HOME_SETTING_LIST_PRESS_IMAGE_SIZE.width,HOME_SETTING_LIST_PRESS_IMAGE_SIZE.height),cocos2d::Rect(HOME_SETTING_LIST_PRESS_CAPINSETS.x,HOME_SETTING_LIST_PRESS_CAPINSETS.y,HOME_SETTING_LIST_PRESS_CAPINSETS_SIZE.width,HOME_SETTING_LIST_PRESS_CAPINSETS_SIZE.height)); //imgsize 138|138 68|68   pad(32,32,138-32-32,138-32-32)
//        
//        auto settingBtn = cocos2d::extension::ControlButton::create(normalSprite);
//        settingBtn->setBackgroundSpriteForState(pressSprite, Control::State::HIGH_LIGHTED);
//        settingBtn->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//        settingBtn->setPosition(HOME_SETTING_LIST_PRESS);
//        settingBtn->setContentSize(cocos2d::Size(settingImg->getContentSize().width*3+stringWidth-(HOME_SETTING_LIST_PRESS.x*2),HOME_SETTING_LIST_PRESS_SIZE.height));
//        settingBtn->setPreferredSize(cocos2d::Size(settingImg->getContentSize().width*3+stringWidth-(HOME_SETTING_LIST_PRESS.x*2),HOME_SETTING_LIST_PRESS_SIZE.height));
//        settingBtn->addTargetWithActionForControlEvents(this, cccontrol_selector(MenuListLayer::buttonClicked), Control::EventType::TOUCH_UP_INSIDE);
//        settingBtn->setTag(BUTTON_TYPE_SETTINGS);
        
        auto settingItem = this->getMenuButton("home_option_menu_bg_1line_normal.png", "home_option_menu_bg_1line_pressed.png", cocos2d::Size(settingImg->getContentSize().width*SPACE+stringWidth-(HOME_SETTING_LIST_PRESS.x*2),BOOKMAP_MENULIST_BUTTON_HEIGHT), HOME_SETTING_LIST_PRESS, BUTTON_TYPE_SETTINGS);
        listBg->addChild(settingItem);
        
        listBg->addChild(settingImg);
        
        auto settingLabel = cocos2d::Label::createWithSystemFont(Device::getResString(Res::Strings::CC_HOME_MENUTITLE_SETTING),//"Settings",
                                                                 FONT_NAME, HOME_SETTING_LABEL_FONTSIZE);
        settingLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        settingLabel->setPosition(HOME_SETTING_LABEL);
        settingLabel->setColor(cocos2d::Color3B(0x78,0x78,0x78));
        settingLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
        listBg->addChild(settingLabel);
    } else if (listType == LIST_TYPE_02) {
        int stringWidth = Device::getTextWidth(Device::getResString(Res::Strings::CC_JOURNEYMAP_MENU_TITLE_DOWNLOADDEVICE), FONT_NAME, HOME_SETTING_LABEL_FONTSIZE);
        
        auto downLoadImg = Sprite::create("home_option_menu_icon_download.png");
        downLoadImg->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        downLoadImg->setPosition(BOOKMAP_MENULIST_MID_ICON);
        
        listBg = cocos2d::extension::Scale9Sprite::create("home_option_menu_bg.png", cocos2d::Rect(0,0,HOME_SETTING_BG_IMAGE_SIZE.width,HOME_SETTING_BG_IMAGE_SIZE.height),cocos2d::Rect(HOME_SETTING_BG_CAPINSETS.x,HOME_SETTING_BG_CAPINSETS.y,HOME_SETTING_BG_CAPINSETS_SIZE.width,HOME_SETTING_BG_CAPINSETS_SIZE.height)); //52,66,110,70 / 32,34,38,38
//        listBg->setContentSize(BOOKMAP_MENULIST_BG_SIZE);
        listBg->setContentSize(cocos2d::Size(downLoadImg->getContentSize().width*SPACE+stringWidth, BOOKMAP_MENULIST_BG_SIZE.height ));
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        listBg->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
        listBg->setPosition(cocos2d::Point(visibleSize.width-HOME_SETTING_BG.x, HOME_SETTING_BG.y - BOOKMAP_MENULIST_BG_SIZE.height));
#else
        listBg->setAnchorPoint(cocos2d::Point::ANCHOR_TOP_RIGHT);
        listBg->setPosition(cocos2d::Point(visibleSize.width-HOME_SETTING_BG.x,HOME_SETTING_BG.y));
#endif
        this->addChild(listBg);
        
//        auto normalTop = cocos2d::extension::Scale9Sprite::create("home_option_menu_bg_top_normal.png", cocos2d::Rect(0,0,HOME_SETTING_LIST_PRESS_IMAGE_SIZE.width,HOME_SETTING_LIST_PRESS_IMAGE_SIZE.height),cocos2d::Rect(HOME_SETTING_LIST_PRESS_CAPINSETS.x,HOME_SETTING_LIST_PRESS_CAPINSETS.y,HOME_SETTING_LIST_PRESS_CAPINSETS_SIZE.width,HOME_SETTING_LIST_PRESS_CAPINSETS_SIZE.height));
//        auto normalMiddle = cocos2d::extension::Scale9Sprite::create("home_option_menu_bg_mid_normal.png", cocos2d::Rect(0,0,HOME_SETTING_LIST_PRESS_IMAGE_SIZE.width,HOME_SETTING_LIST_PRESS_IMAGE_SIZE.height),cocos2d::Rect(HOME_SETTING_LIST_PRESS_CAPINSETS.x,HOME_SETTING_LIST_PRESS_CAPINSETS.y,HOME_SETTING_LIST_PRESS_CAPINSETS_SIZE.width,HOME_SETTING_LIST_PRESS_CAPINSETS_SIZE.height));
//        auto normalBottom = cocos2d::extension::Scale9Sprite::create("home_option_menu_bg_bottom_normal.png", cocos2d::Rect(0,0,HOME_SETTING_LIST_PRESS_IMAGE_SIZE.width,HOME_SETTING_LIST_PRESS_IMAGE_SIZE.height),cocos2d::Rect(HOME_SETTING_LIST_PRESS_CAPINSETS.x,HOME_SETTING_LIST_PRESS_CAPINSETS.y,HOME_SETTING_LIST_PRESS_CAPINSETS_SIZE.width,HOME_SETTING_LIST_PRESS_CAPINSETS_SIZE.height));
//        
//        auto pressTop = cocos2d::extension::Scale9Sprite::create("home_option_menu_bg_top_pressed.png", cocos2d::Rect(0,0,HOME_SETTING_LIST_PRESS_IMAGE_SIZE.width,HOME_SETTING_LIST_PRESS_IMAGE_SIZE.height),cocos2d::Rect(HOME_SETTING_LIST_PRESS_CAPINSETS.x,HOME_SETTING_LIST_PRESS_CAPINSETS.y,HOME_SETTING_LIST_PRESS_CAPINSETS_SIZE.width,HOME_SETTING_LIST_PRESS_CAPINSETS_SIZE.height));
//        auto pressMiddle = cocos2d::extension::Scale9Sprite::create("home_option_menu_bg_mid_pressed.png", cocos2d::Rect(0,0,HOME_SETTING_LIST_PRESS_IMAGE_SIZE.width,HOME_SETTING_LIST_PRESS_IMAGE_SIZE.height),cocos2d::Rect(HOME_SETTING_LIST_PRESS_CAPINSETS.x,HOME_SETTING_LIST_PRESS_CAPINSETS.y,HOME_SETTING_LIST_PRESS_CAPINSETS_SIZE.width,HOME_SETTING_LIST_PRESS_CAPINSETS_SIZE.height));
//        auto pressBottom = cocos2d::extension::Scale9Sprite::create("home_option_menu_bg_bottom_pressed.png", cocos2d::Rect(0,0,HOME_SETTING_LIST_PRESS_IMAGE_SIZE.width,HOME_SETTING_LIST_PRESS_IMAGE_SIZE.height),cocos2d::Rect(HOME_SETTING_LIST_PRESS_CAPINSETS.x,HOME_SETTING_LIST_PRESS_CAPINSETS.y,HOME_SETTING_LIST_PRESS_CAPINSETS_SIZE.width,HOME_SETTING_LIST_PRESS_CAPINSETS_SIZE.height));
        
        //first
        auto first = Node::create();
        first->setContentSize(cocos2d::Size(downLoadImg->getContentSize().width*SPACE+stringWidth, BOOKMAP_MENULIST_PRESS_SIZE.height ));
        first->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        first->setPosition(cocos2d::Point(HOME_SETTING_LIST_PRESS.x,HOME_SETTING_LIST_PRESS.y+(BOOKMAP_MENULIST_BUTTON_HEIGHT*2)));
                           
//        auto downLoadBtn = cocos2d::extension::ControlButton::create(normalTop);
//        downLoadBtn->setBackgroundSpriteForState(pressTop, Control::State::HIGH_LIGHTED);
//        downLoadBtn->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//        downLoadBtn->setPosition(cocos2d::Point::ZERO);
//        downLoadBtn->setContentSize(cocos2d::Size(downLoadImg->getContentSize().width*3+stringWidth-(HOME_SETTING_LIST_PRESS.x*2),BOOKMAP_MENULIST_BUTTON_HEIGHT));
//        downLoadBtn->setPreferredSize(cocos2d::Size(downLoadImg->getContentSize().width*3+stringWidth-(HOME_SETTING_LIST_PRESS.x*2),BOOKMAP_MENULIST_BUTTON_HEIGHT));
//        downLoadBtn->addTargetWithActionForControlEvents(this, cccontrol_selector(MenuListLayer::buttonClicked), Control::EventType::TOUCH_UP_INSIDE);
//        downLoadBtn->setTag(BUTTON_TYPE_DOWNLOAD);
//        first->addChild(downLoadBtn);
        
        auto downLoadMenuItem = this->getMenuButton("home_option_menu_bg_top_normal.png", "home_option_menu_bg_top_pressed.png", cocos2d::Size(downLoadImg->getContentSize().width*SPACE+stringWidth-(HOME_SETTING_LIST_PRESS.x*2),BOOKMAP_MENULIST_BUTTON_HEIGHT), cocos2d::Point::ZERO, BUTTON_TYPE_DOWNLOAD);
        first->addChild(downLoadMenuItem);

        first->addChild(downLoadImg);
        
        auto downLoadLabel = cocos2d::Label::createWithSystemFont(Device::getResString(Res::Strings::CC_JOURNEYMAP_MENU_TITLE_DOWNLOADDEVICE),//"Download to device",
                                                                  FONT_NAME, HOME_SETTING_LABEL_FONTSIZE);
        downLoadLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        downLoadLabel->setPosition(BOOKMAP_MENULIST_MID_LABEL);
        downLoadLabel->setPosition(cocos2d::Point(BOOKMAP_MENULIST_MID_LABEL.x, BOOKMAP_MENULIST_LABEL_SIZE.height/2 - downLoadLabel->getContentSize().height/2));
//        downLoadLabel->setContentSize(BOOKMAP_MENULIST_LABEL_SIZE);
//        downLoadLabel->setPosition(cocos2d::Point::ZERO);
//        downLoadLabel->setVerticalAlignment(cocos2d::TextVAlignment::CENTER);
        downLoadLabel->setColor(cocos2d::Color3B(0x78,0x78,0x78));
        downLoadLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
        first->addChild(downLoadLabel);
        
        //second
        auto second = Node::create();
        second->setContentSize(cocos2d::Size(downLoadImg->getContentSize().width*SPACE+stringWidth, BOOKMAP_MENULIST_PRESS_SIZE.height ));
        second->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        second->setPosition(cocos2d::Point(HOME_SETTING_LIST_PRESS.x,HOME_SETTING_LIST_PRESS.y+BOOKMAP_MENULIST_BUTTON_HEIGHT));
        
//        auto shareBtn = cocos2d::extension::ControlButton::create(normalMiddle);
//        shareBtn->setBackgroundSpriteForState(pressMiddle, Control::State::HIGH_LIGHTED);
//        shareBtn->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//        shareBtn->setPosition(cocos2d::Point::ZERO);
//        shareBtn->setContentSize(cocos2d::Size(downLoadImg->getContentSize().width*3+stringWidth-(HOME_SETTING_LIST_PRESS.x*2),BOOKMAP_MENULIST_BUTTON_HEIGHT));
//        shareBtn->setPreferredSize(cocos2d::Size(downLoadImg->getContentSize().width*3+stringWidth-(HOME_SETTING_LIST_PRESS.x*2),BOOKMAP_MENULIST_BUTTON_HEIGHT));
//        shareBtn->addTargetWithActionForControlEvents(this, cccontrol_selector(MenuListLayer::buttonClicked), Control::EventType::TOUCH_UP_INSIDE);
//        shareBtn->setTag(BUTTON_TYPE_SHARE);
        
        auto shareMenuItem = this->getMenuButton("home_option_menu_bg_mid_normal.png", "home_option_menu_bg_mid_pressed.png", cocos2d::Size(downLoadImg->getContentSize().width*SPACE+stringWidth-(HOME_SETTING_LIST_PRESS.x*2),BOOKMAP_MENULIST_BUTTON_HEIGHT), cocos2d::Point::ZERO, BUTTON_TYPE_SHARE);
        second->addChild(shareMenuItem);
        
//        second->addChild(shareBtn);
        
        auto shareImg = Sprite::create("home_option_menu_icon_share.png");
        shareImg->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        shareImg->setPosition(BOOKMAP_MENULIST_MID_ICON);
        second->addChild(shareImg);
        
        auto shareLabel = cocos2d::Label::createWithSystemFont(Device::getResString(Res::Strings::CC_JOURNEYMAP_MENU_TITLE_SHARE),//"Share",
                                                               FONT_NAME, HOME_SETTING_LABEL_FONTSIZE);
        shareLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        shareLabel->setPosition(BOOKMAP_MENULIST_MID_LABEL);
        shareLabel->setPosition(cocos2d::Point(BOOKMAP_MENULIST_MID_LABEL.x, BOOKMAP_MENULIST_LABEL_SIZE.height/2 - shareLabel->getContentSize().height/2));
//        shareLabel->setContentSize(BOOKMAP_MENULIST_LABEL_SIZE);
//        shareLabel->setPosition(cocos2d::Point::ZERO);
//        shareLabel->setVerticalAlignment(cocos2d::TextVAlignment::CENTER);
        shareLabel->setColor(cocos2d::Color3B(0x78,0x78,0x78));
        shareLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
        second->addChild(shareLabel);
        
        //third
        auto third = Node::create();
        third->setContentSize(cocos2d::Size(downLoadImg->getContentSize().width*SPACE+stringWidth, BOOKMAP_MENULIST_PRESS_SIZE.height ));
        third->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        third->setPosition(HOME_SETTING_LIST_PRESS);
        
//        auto settingBtn = cocos2d::extension::ControlButton::create(normalBottom);
//        settingBtn->setBackgroundSpriteForState(pressBottom, Control::State::HIGH_LIGHTED);
//        settingBtn->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//        settingBtn->setPosition(cocos2d::Point::ZERO);
//        settingBtn->setContentSize(cocos2d::Size(downLoadImg->getContentSize().width*3+stringWidth-(HOME_SETTING_LIST_PRESS.x*2),BOOKMAP_MENULIST_BUTTON_HEIGHT));
//        settingBtn->setPreferredSize(cocos2d::Size(downLoadImg->getContentSize().width*3+stringWidth-(HOME_SETTING_LIST_PRESS.x*2),BOOKMAP_MENULIST_BUTTON_HEIGHT));
//        settingBtn->addTargetWithActionForControlEvents(this, cccontrol_selector(MenuListLayer::buttonClicked), Control::EventType::TOUCH_UP_INSIDE);
//        settingBtn->setTag(BUTTON_TYPE_SETTINGS);
//        third->addChild(settingBtn);

        auto settingMenuItem = this->getMenuButton("home_option_menu_bg_bottom_normal.png", "home_option_menu_bg_bottom_pressed.png", cocos2d::Size(downLoadImg->getContentSize().width*SPACE+stringWidth-(HOME_SETTING_LIST_PRESS.x*2),BOOKMAP_MENULIST_BUTTON_HEIGHT), cocos2d::Point::ZERO, BUTTON_TYPE_SETTINGS);
        third->addChild(settingMenuItem);
        
        auto settingImg = Sprite::create("home_option_menu_icon_settings.png");
        settingImg->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        settingImg->setPosition(BOOKMAP_MENULIST_MID_ICON);
        third->addChild(settingImg);
        
        auto settingLabel = cocos2d::Label::createWithSystemFont(Device::getResString(Res::Strings::CC_HOME_MENUTITLE_SETTING),//"Settings",
                                                                 FONT_NAME, HOME_SETTING_LABEL_FONTSIZE);
        settingLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        settingLabel->setPosition(cocos2d::Point(BOOKMAP_MENULIST_MID_LABEL.x, BOOKMAP_MENULIST_LABEL_SIZE.height/2 - settingLabel->getContentSize().height/2));
//        settingLabel->setContentSize(BOOKMAP_MENULIST_LABEL_SIZE);
//        settingLabel->setPosition(cocos2d::Point::ZERO);
//        settingLabel->setVerticalAlignment(cocos2d::TextVAlignment::CENTER);
        settingLabel->setColor(cocos2d::Color3B(0x78,0x78,0x78));
        settingLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
        third->addChild(settingLabel);
        
        listBg->addChild(first);
        listBg->addChild(second);
        listBg->addChild(third);
    }
    
    this->menuListControl(false);
    this->setContentSize(listBg->getContentSize());
    return true;
}

Menu* MenuListLayer::getMenuButton(const std::string& normal, const std::string& press, cocos2d::Size contentSize, cocos2d::Point position, int tag)
{
    auto normalSprite = cocos2d::extension::Scale9Sprite::create(normal, cocos2d::Rect(0,0,HOME_SETTING_LIST_PRESS_IMAGE_SIZE.width,HOME_SETTING_LIST_PRESS_IMAGE_SIZE.height),cocos2d::Rect(HOME_SETTING_LIST_PRESS_CAPINSETS.x,HOME_SETTING_LIST_PRESS_CAPINSETS.y,HOME_SETTING_LIST_PRESS_CAPINSETS_SIZE.width,HOME_SETTING_LIST_PRESS_CAPINSETS_SIZE.height));
    normalSprite->setContentSize(contentSize);
    auto pressSprite = cocos2d::extension::Scale9Sprite::create(press, cocos2d::Rect(0,0,HOME_SETTING_LIST_PRESS_IMAGE_SIZE.width,HOME_SETTING_LIST_PRESS_IMAGE_SIZE.height),cocos2d::Rect(HOME_SETTING_LIST_PRESS_CAPINSETS.x,HOME_SETTING_LIST_PRESS_CAPINSETS.y,HOME_SETTING_LIST_PRESS_CAPINSETS_SIZE.width,HOME_SETTING_LIST_PRESS_CAPINSETS_SIZE.height));
    pressSprite->setContentSize(contentSize);
    
    MenuItemSprite *menuItem = MenuItemSprite::create(normalSprite, pressSprite, CC_CALLBACK_1(MenuListLayer::buttonClicked, this));
    menuItem->setTag(tag);
    menuItem->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    
    auto menu = Menu::create(menuItem, NULL);
    menu->alignItemsVerticallyWithPadding(0);
    menu->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    menu->setPosition(position);
    
    return menu;
}

void MenuListLayer::buttonClicked(Ref *pSender)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Tap.wav", false);
    this->menuListControl(false);
    
    auto button = (MenuItemSprite*)pSender;
    if (_delegate != NULL) {
        _delegate->listButtonClicked(button->getTag());
    }
}

void MenuListLayer::menuListControl(bool visible)
{
    menuListVisible = visible;
    this->setVisible(visible);
    const auto& children = listBg->getChildren();
    for (const auto& child : children)
    {
        if (child->getTag() == BUTTON_TYPE_SETTINGS ||
            child->getTag() == BUTTON_TYPE_DOWNLOAD ||
            child->getTag() == BUTTON_TYPE_SHARE ) {
            auto button = (cocos2d::extension::ControlButton*)child;
            button->setEnabled(visible);
        }
    }
    listBg->setVisible(visible);
}