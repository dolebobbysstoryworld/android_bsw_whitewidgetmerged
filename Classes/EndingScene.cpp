//
//  EndingScene.cpp
//  bobby
//
//  Created by Dongwook, Kim on 14. 7. 15..
//
//

#include "EndingScene.h"
#include "Util.h"
#include "SimpleAudioEngine.h"

bool Ending::init()
{
    if (!LayerColor::initWithColor(cocos2d::Color4B(0,0,0,0))) {
        return false;
    }
    
    cocosbuilder::NodeLoaderLibrary *nodeLoaderLibrary = cocosbuilder::NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
    cocosbuilder::CCBReader *ccbReader = new cocosbuilder::CCBReader(nodeLoaderLibrary);
    endingCcbi = (Sprite*)ccbReader->readNodeGraphFromFile("ending.ccbi", this);
    endingCcbi->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    endingCcbi->setPosition(cocos2d::Point::ZERO);
    float scaleFactor = Util::getCharacterScaleFactor();
    log("ending scaleFactor for ccbi : %f", scaleFactor);
    endingCcbi->setScale(scaleFactor);
    
    this->addChild(endingCcbi, 0);
    ccbReader->release();

    cocosbuilder::CCBAnimationManager *manager = (cocosbuilder::CCBAnimationManager*)endingCcbi->getUserObject();
    manager->runAnimationsForSequenceNamed("ending_ready");
    endingAnimationDuration = manager->getSequenceDuration("ending");
    
    return true;
}

void Ending::startAnimation()
{
    LanguageType currentLanguageType = Application::getInstance()->getCurrentLanguage();
    switch (currentLanguageType) {
        case LanguageType::KOREAN:
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("preload_sounds/kor/scene_ending.wav", false);
            break;
        }
        case LanguageType::JAPANESE: {
            CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("preload_sounds/ja/scene_ending.wav", false);
            break;
        }
        case LanguageType::ENGLISH:
        default: {
            CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("preload_sounds/en/scene_ending.wav", false);
            break;
        }
    }
    
    cocosbuilder::CCBAnimationManager *manager = (cocosbuilder::CCBAnimationManager*)endingCcbi->getUserObject();
//    manager->setDelegate(this);
    manager->runAnimationsForSequenceNamed("ending_ready");
    manager->runAnimationsForSequenceNamed("ending");
    
    animationStarted = true;
    animationStartTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch());
    
    triggerFinish(0);
}

void Ending::triggerFinish(float durationOffset)
{
    float totalAnimationDuration = endingAnimationDuration;
    float remainAnimationDuration = totalAnimationDuration - elapsedAnimationTime;
    if (remainAnimationDuration > 0) {
        auto delay = DelayTime::create(remainAnimationDuration);
        auto finishAction = Sequence::create(delay, CallFunc::create(CC_CALLBACK_0(Ending::finishedEnding, this)), NULL);
        this->cocos2d::Node::runAction(finishAction);
    }
}

void Ending::stopAction()
{
    this->stopAllActions();
    pauseAllChildren(endingCcbi);
}

void Ending::pauseAnimation()
{
    log("pause opening");
    if (animationStarted) {
        CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
        animationPaused = true;
        auto now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch());
        std::chrono::duration<double> diff = now - animationStartTime;
        elapsedAnimationTime = diff.count();
        accElapsedAnimationTime += elapsedAnimationTime;
        stopAction();
    }
}

void Ending::resumeAnimation()
{
    log("resume opening");
    if (animationPaused) {
        CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
        animationPaused = false;
        resumeAllChildren(endingCcbi);
        triggerFinish(accElapsedAnimationTime);
        animationStartTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch());
    }
}

void Ending::pauseAllChildren(Node *node)
{
    auto children = node->getChildren();
    auto childrenCount = node->getChildrenCount();
    
    for (size_t index = 0; index< childrenCount; index++) {
        Node* child = (Node*)children.at(index);
        child->pause();
    }
}

void Ending::resumeAllChildren(Node *node)
{
    auto children = node->getChildren();
    auto childrenCount = node->getChildrenCount();
    
    for (size_t index = 0; index< childrenCount; index++) {
        Node* child = (Node*)children.at(index);
        child->resume();
    }
}

void Ending::finishedEnding()
{
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    
    if (animationPaused) return;
    animationStarted = false;

    if(_delegate != nullptr){
        _delegate->onEndingFinished(this);
    }
}

void Ending::completedAnimationSequenceNamed(const char *name)
{

}