//
//  CharacterScrollView.cpp
//  bobby
//
//  Created by Dongwook, Kim on 14. 7. 4..
//
//

#include "CharacterScrollView.h"
#include "ResourceManager.h"
#include "Define.h"
#include "Util.h"

#include "external/json/document.h"
#include "external/json/prettywriter.h"
#include "external/json/stringbuffer.h"
#include "DoleProduct.h"

#define CHARACTER_BASE_TAG                  20000
#define LINE_BASE_TAG                       30000

#define TAG_CHARACTER_ITEM_NORMAL           500
#define TAG_CHARACTER_ITEM_SELECT           501
#define TAG_DELETE_BUTTON                   503

#define CHARACTER_ANCHOR_Y                  0.95
#define DELETE_CHAR_POPUP           0

#define WHITESPACE_CHAR_DEFAULT     1

#define MAX_ROTATION        5.0f


bool CharacterScrollView::init(char_super_type superViewType)
{
    if (Layer::init()) {
        
        linesCount = 0;
        currentCharacterAngle = 0.0f;
        scrollViewPressed = false;
        reachedToEnd = true;
        
        superType = superViewType;
        characterList = __Array::create();
        characterList->retain();
        
        deleteButtons = __Array::create();
        deleteButtons->retain();
        
        items = __Dictionary::create();
        items->retain();
        
        pins = __Array::create();
        pins->retain();
        
        this->scrollView = this->createScrollView();
        this->loadJsonToScrollView(this->scrollView);
        
        this->setAnchorPoint(this->scrollView->getAnchorPoint());
        this->setContentSize(this->scrollView->getViewSize());
        this->setPosition(this->scrollView->getPosition());
        this->addChild(this->scrollView, 0);
        
        scrollOffset = cocos2d::Point::ZERO;
        // TODO: create menus
        return true;
    }
    return false;
}

CharacterScrollView* CharacterScrollView::create(char_super_type superViewType)
{
    CharacterScrollView *pRet = new CharacterScrollView();
    if (pRet && pRet->init(superViewType))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

ScrollViewExtension *CharacterScrollView::createScrollView()
{
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    
    ScrollViewExtension *newScrollView = ScrollViewExtension::create(cocos2d::Size(visibleSize.width, HOME_BACKGROUND_SCROLL_VIEW_HEIGHT), nullptr);
    newScrollView->setDirection(cocos2d::extension::ScrollView::Direction::HORIZONTAL);
    newScrollView->setBounceable(false);
    newScrollView->setAnchorPoint(cocos2d::Point(0, 0));
    newScrollView->setTag(TAG_SCROLLVIEW);
    newScrollView->setExtensionDelegate(this);
    newScrollView->setBounceable(true);
    
//    deleteModeDim = LayerColor::create(Color4B(0,0,0,255.0f*0.6f), newScrollView->getContentSize().width+HOME_BOTTOM_LINE_MID.width, newScrollView->getContentSize().height);
//    deleteModeDim->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
//    deleteModeDim->setPosition(cocos2d::Point::ZERO);
//    deleteModeDim->setLocalZOrder(TOUCHABLE_LAYER-1);
//    newScrollView->addChild(deleteModeDim);
//    deleteModeDim->setVisible(false);
    
    return newScrollView;
}

void CharacterScrollView::characterCoinClicked(Ref* pSender)
{
//    if (this->scrollView->deleteMode) return;
    
    log("coin clicked");
}

void CharacterScrollView::rotateCharacters(float duration, float angle, bool animation)
{
    auto container = scrollView->getChildren().at(0);
    auto itemCount = characterList->count();
    currentCharacterAngle = angle;
    for (int index = 0; index < itemCount ; index++) {
        Sprite *characterHolder = (Sprite *)(container->getChildByTag(CHARACTER_BASE_TAG+index));
        
        if (characterHolder == NULL) continue;
        
        changePositionAtAnchor(characterHolder, cocos2d::Point(0.5, CHARACTER_ANCHOR_Y) );
        
        if (animation) {
            auto rotateAction = RotateTo::create(duration, angle);
            auto rotateReverseAction = RotateTo::create(duration, 0);
            auto actionSequence = Sequence::create(rotateAction, rotateReverseAction, NULL);
            characterHolder->runAction(actionSequence->clone());
        } else {
            characterHolder->setRotation(angle);
        }
    }
}

void CharacterScrollView::scrollViewDidScroll(ScrollViewExtension* view)
{
    auto oldOffset = scrollOffset.x;
    scrollOffset = view->getContentOffset();

//    log("scrollViewDidScroll old = %f, new = %f, drag = %d", oldOffset, view->getContentOffset().x, view->isDragging());
    
    float rotationDuration = 0.1f;
    int direction = 0;

    if ( view->getContentOffset().x == view->minContainerOffset().x ){
        if (oldOffset == view->minContainerOffset().x){
            if (view->isDragging()){
                if (!reachedToEnd) {
                    reachedToEnd = true;
                    return;
                }
                direction = -1;
            } else {
                reachedToEnd = false;
            }
        }
    } else if (view->getContentOffset().x == view->maxContainerOffset().x) {
        if (oldOffset == view->maxContainerOffset().x){
            if (view->isDragging()){
                if (!reachedToEnd) {
                    reachedToEnd = true;
                    return;
                }
                direction = 1;
            } else {
                reachedToEnd = false;
            }
        }
        isScrollingToFirst = false;
    } else {
        if((oldOffset-view->getContentOffset().x) < 0) {
            direction = 1;
        } else {
            direction = -1;
        }
    }
    
    if (view->isDragging() && !scrollViewPressed) {
        scrollViewPressed = true;
        currentCharacterAngle = 0; // clear old angle
    }
    
    float nextAngle = currentCharacterAngle;
    if (direction == 1) {
        if (currentCharacterAngle < MAX_ROTATION) {
            nextAngle = currentCharacterAngle + 0.5f;
        }
    } else {
        if (currentCharacterAngle > -MAX_ROTATION) {
            nextAngle = currentCharacterAngle - 0.5f;
        }
    }
    
    if (view->isDragging()){
        this->rotateCharacters(rotationDuration, nextAngle, false);
    } else {
        this->rotateCharacters(rotationDuration, nextAngle, true);
        scrollViewPressed = false;
    }
    
//    if ( !view->isDragging()){ // scroll did end
//        cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
//        auto endOffset = visibleSize.width - view->getContentSize().width;
//
//        if ( !((oldOffset == 0 && view->getContentOffset().x == 0) || (oldOffset == endOffset && view->getContentOffset().x == endOffset)) ){
//            float rotationDuration = 0.1f;
//            int direction = 0;
//
//            auto container = view->getChildren().at(0);
//            auto itemCount = characterList->count();
//            
//            for (int index = 0; index < itemCount ; index++) {
//                Sprite *characterHolder = (Sprite *)(container->getChildByTag(CHARACTER_BASE_TAG+index));
//                
//                if (characterHolder == NULL) continue;
//                
//                changePositionAtAnchor(characterHolder, cocos2d::Point(0.5, CHARACTER_ANCHOR_Y) );
//                
//                if((oldOffset-view->getContentOffset().x) < 0) {
//                    direction = -1;
//                } else {
//                    direction = 1;
//                }
//                
//                float rotationAngle = direction * 5.0f;
//                
//                auto rotateAction = RotateTo::create(rotationDuration, rotationAngle);
//                auto rotateReverseAction = RotateTo::create(rotationDuration, 0);
//                auto actionSequence = Sequence::create(rotateAction, rotateReverseAction, NULL);
//                characterHolder->runAction(actionSequence->clone());
//            }
//        }
//        scrollOffset = view->getContentOffset();
//    }
}
void CharacterScrollView::scrollViewDidZoom(ScrollViewExtension* view)
{
    
}

void CharacterScrollView::scrollViewDidScrollEnd(ScrollViewExtension* view)
{
    isScrollingToFirst = false;
}

void CharacterScrollView::popupButtonClicked(PopupLayer *popup, cocos2d::Ref *obj)
{
    Director::getInstance()->resume();
    auto clickedButton = (MenuItem*)obj;
    int tag = clickedButton->getTag();
    
    if (popup->getTag() == DELETE_CHAR_POPUP) {
        if (tag == 0) {
            log("delete cancelButton clicked!!");
        } else {
            scrollView->isAnimate = true;
            log("popup isAnimate true");
            this->setEnableDeleteButtons(false);
            
            log("delete okButton clicked!!");
            float scaleDuration = 0.2f;
            float shrinkRatio = 0.97;
            auto scaleAction = ScaleTo::create(scaleDuration, shrinkRatio);
            auto reverse = ScaleTo::create(scaleDuration, 1.0f);
            
            auto scaleCallback = CallFunc::create([&](){
//                log("selectedCharKey popup :%s", selectedCharKey->getCString());
                this->runRemoveCharacterAnimation(selectedCharKey);
                auto runningScene = Director::getInstance()->getRunningScene();
//                log("get Scene Tag : %d",runningScene->getTag());
                if (runningScene->getTag() == TAG_RECORDING_SCENE) {
                    __NotificationCenter::getInstance()->postNotification("deleteCharItemInRecordScene");
                }
            });
            auto scaleSequence = Sequence::create(scaleAction, reverse, scaleCallback, NULL);
            selDeleteChar->runAction(scaleSequence);
        }
    } else {
        if (tag == 0) {
            log("cancelButton clicked!");
        } else {
            log("okButton clicked!!");
        }
    }
}

void CharacterScrollView::onSelectItem(ScrollViewExtension* view, Node* selectedItem, Touch *pTouch, Event *pEvent)
{
    log("onselectitem");
    
//    auto holderNode = (Node *)selectedItem;
//    selectedCharKey = (__String *)holderNode->getUserData();
//    log("selectedCharKey : %s", selectedCharKey->getCString());
//    selDeleteChar = holderNode;
//    
//    Director::getInstance()->pause();
//    PopupLayer *modal = PopupLayer::create(POPUP_TYPE_WARNING, "Selected character will be deleted. Deleted records can't be restored.", "Cancel", "OK");
//    
//    modal->setTag(DELETE_CHAR_POPUP);
//    modal->setDelegate(this);
//    auto runningScene = Director::getInstance()->getRunningScene();
//    runningScene->addChild(modal, 1000);
    
    auto character = (Sprite *)selectedItem;
    log("getTag : %i", character->getTag());
    float scaleDuration = 0.05f;
    float shrinkRatio = 0.97;
    auto scaleAction = ScaleTo::create(scaleDuration, shrinkRatio);
    auto reverse = ScaleTo::create(scaleDuration, 1.0f);
    auto afterCallBack = CallFuncN::create(CC_CALLBACK_1(CharacterScrollView::characterPressedCallback, this));
    auto actions = Sequence::create(scaleAction, reverse, afterCallBack, NULL);
    character->runAction(actions);
}

void CharacterScrollView::characterPressedCallback(Ref* pSender)
{
    Sprite *character = (Sprite *)pSender;
    auto index = character->getTag() % CHARACTER_BASE_TAG;
    log("character pressed : %d", index);
    auto charKey = (__String*)characterList->getObjectAtIndex(index);
    
    if (_scrollViewDelegate) {
        _scrollViewDelegate->onSelectCharacter(this, character, charKey, index);
    }
}

bool CharacterScrollView::setSelectChracter(int index, bool select)
{
    auto targetCharacter = scrollView->getContainer()->getChildByTag(CHARACTER_BASE_TAG + index);
    auto normalSprite = targetCharacter->getChildByTag(TAG_CHARACTER_ITEM_NORMAL);
    auto selectSprite = targetCharacter->getChildByTag(TAG_CHARACTER_ITEM_SELECT);

    bool result = false;
    
    if (normalSprite->isVisible() != !select) {
        normalSprite->setVisible(!select);
        result = true;
    }
    
    if (selectSprite->isVisible() != select) {
        selectSprite->setVisible(select);
        result = true;
    }
    
    return result;
}

int CharacterScrollView::getCharacterIndex(std::string& charKey)
{
    int result = CC_INVALID_INDEX;
    for (int index = 0; index <characterList->count(); index++){
        auto cKey = (__String *)characterList->getObjectAtIndex(index);
        if (charKey.compare(cKey->getCString())== 0) {
            result = index;
            break;
        }
    }
    return result;
}

cocos2d::Node* CharacterScrollView::createCharacterForScrollView(std::string charKey, int index)
{
    auto characterItemHolder = Node::create();
    auto characterItemContentSize = HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_HOLDER_SIZE;
    characterItemHolder->setContentSize(characterItemContentSize);
    characterItemHolder->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    
    auto characterKey = charKey.c_str();
    
    auto characterKeyStr = __String::create(characterKey);
    auto charKeyClone = characterKeyStr->clone();
    charKeyClone->retain();
    
    characterItemHolder->setUserData(charKeyClone);
    
    characterList->addObject(__String::create(characterKey));
    
    auto ud = UserDefault::getInstance();
    auto characterJsonString = ud->getStringForKey(characterKey);
    rapidjson::Document characterInfo;
    characterInfo.Parse<0>(characterJsonString.c_str());
    
    if (characterInfo.HasParseError()) {
        log("parse error - characterKey : %s", characterJsonString.c_str());
        auto charKeyStr = __String::create(characterJsonString.c_str());
        Util::removeParseErrorObject("character_order", charKeyStr);
        parseErrorCount++;
    } else {
        __String *normalImage;
        __String *selectImage;
        __String *thumbnailImage = nullptr;
        
        bool customCharacter = characterInfo.HasMember("base_char");
        cocos2d::Point photoPosition;
        if (customCharacter){
            auto baseChar = characterInfo["base_char"].GetString();
            auto baseCharacterJsonString = ud->getStringForKey(baseChar);
            rapidjson::Document baseCharacterInfo;
            baseCharacterInfo.Parse<0>(baseCharacterJsonString.c_str());
            auto normal = baseCharacterInfo["item_normal"].GetString();
            normalImage = __String::create(normal);
            
            auto select = (char *)baseCharacterInfo["item_selected"].GetString();
            selectImage = __String::create(select);
            
            auto thumbnail = (char *)characterInfo["thumbnail_photo_file"].GetString();
            thumbnailImage = __String::create(thumbnail);
            
            auto customID = baseCharacterInfo["custom_id"].GetInt();
            switch (customID) {
                case 10: // C010_custom
                    photoPosition = CUSTOM_CHARACTER_C010_PHOTO;
                    break;
                case 11: // C011_custom
                    photoPosition = CUSTOM_CHARACTER_C011_PHOTO;
                    break;
                case 12: // C012_custom
                    photoPosition = CUSTOM_CHARACTER_C012_PHOTO;
                    break;
                case 5: // C005_custom
                    photoPosition = CUSTOM_CHARACTER_C005_PHOTO;
                    break;
                case 9: // C009_custom
                    photoPosition = CUSTOM_CHARACTER_C009_PHOTO;
                    break;
                case 6: // C006_custom
                    photoPosition = CUSTOM_CHARACTER_C006_PHOTO;
                    break;
                case 7: // C007_custom
                    photoPosition = CUSTOM_CHARACTER_C007_PHOTO;
                    break;
                case 8: // C008_custom
                    photoPosition = CUSTOM_CHARACTER_C008_PHOTO;
                    break;
                case 1: // C001_custom
                    photoPosition = CUSTOM_CHARACTER_C001_PHOTO;
                    break;
                case 2: // C002_custom
                    photoPosition = CUSTOM_CHARACTER_C002_PHOTO;
                    break;
                case 3: // C003_custom
                    photoPosition = CUSTOM_CHARACTER_C003_PHOTO;
                    break;
                case 4: // C004_custom
                    photoPosition = CUSTOM_CHARACTER_C004_PHOTO;
                    break;
                case 15: // C015_custom, currently json declare 13, 14 (boy, girl image) as custom 15, 16
                    photoPosition = CUSTOM_CHARACTER_C013_PHOTO;
                    break;
                case 16: // C016_custom, currently json declare 13, 14 (boy, girl image) as custom 15, 16
                    photoPosition = CUSTOM_CHARACTER_C014_PHOTO;
                    break;
                default:
                    photoPosition = CUSTOM_CHARACTER_C010_PHOTO;
                    break;
            }
        } else {
            auto normal = (char *)characterInfo["item_normal"].GetString();
            normalImage = __String::create(normal);
            
            auto select = (char *)characterInfo["item_selected"].GetString();
            selectImage = __String::create(select);
        }
        
        auto characterItemNormal = Sprite::create(normalImage->getCString());
        characterItemNormal->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_TOP);
        characterItemNormal->setPosition(cocos2d::Point(characterItemContentSize.width/2, characterItemContentSize.height));
        characterItemNormal->setTag(TAG_CHARACTER_ITEM_NORMAL);
        
        auto characterItemSelect = Sprite::create(selectImage->getCString());
        characterItemSelect->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_TOP);
        characterItemSelect->setPosition(cocos2d::Point(characterItemContentSize.width/2, characterItemContentSize.height));
        characterItemSelect->setTag(TAG_CHARACTER_ITEM_SELECT);
        
        if (thumbnailImage != nullptr) {
            std::string photoType("character");
            std::string path = Util::getThumbnailPhotoPath(photoType);
            path.append(thumbnailImage->getCString());
            if (cocos2d::FileUtils::getInstance()->isFileExist(path) ){
                
                cocos2d::Data imageData = cocos2d::FileUtils::getInstance()->getDataFromFile(path.c_str());
                unsigned long nSize = imageData.getSize();
                
                cocos2d::Image *image = new cocos2d::Image();
                image->initWithImageData(imageData.getBytes(), nSize);
                
                cocos2d::Texture2D *texture = new cocos2d::Texture2D();
                texture->initWithImage(image);
                
                cocos2d::Point imageStartPos = cocos2d::Point ( (characterItemContentSize.width-characterItemNormal->getContentSize().width)/2,
                                                               characterItemContentSize.height-characterItemNormal->getContentSize().height);
                
                auto picture = Sprite::createWithTexture(texture);
                if(picture) {
                    picture->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                    picture->setPosition(imageStartPos + photoPosition);
                    characterItemHolder->addChild(picture);
                }
            } else {
                log("error! no file : %s", path.c_str());
            }
        }
        
        characterItemHolder->addChild(characterItemNormal);
        characterItemHolder->addChild(characterItemSelect);
        characterItemSelect->setVisible(false);
        
        int newIndex = index-parseErrorCount;
        
        float bottomPadding = 0;
        if((newIndex%3 == 0)) {
            bottomPadding = HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_BOTTOM_PADDING1;
        } else if((newIndex%3 == 1)) {
            bottomPadding = HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_BOTTOM_PADDING2;
        } else if ((newIndex%3 == 2)){
            bottomPadding = HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_BOTTOM_PADDING3;
        }
        
        cocos2d::Point currentPoint = cocos2d::Point(HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_START_PADDING + characterItemContentSize.width*newIndex, bottomPadding);
        characterItemHolder->setPosition(currentPoint);
        
        cocos2d::Rect characterRect = cocos2d::Rect((HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_HOLDER_SIZE.width-characterItemNormal->getContentSize().width)/2,
                                                    HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_HOLDER_SIZE.height-characterItemNormal->getContentSize().height,
                                                    characterItemNormal->getContentSize().width,
                                                    characterItemNormal->getContentSize().height);
        
        if (!customCharacter) {
            bool isLock = characterInfo["locked"].GetBool();
            
            if (isLock) {
                int price = characterInfo["price"].GetInt();
                auto lock = Sprite::create("background_frame_lock.png");
                lock->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
                lock->setPosition(cocos2d::Point(characterRect.origin.x+characterRect.size.width/2,
                                                 characterRect.origin.y+characterRect.size.height/2));
                characterItemHolder->addChild(lock, 1);
                
                auto coinBtn = cocos2d::ui::Button::create("background_frame_dolecoin.png", "background_frame_dolecoin.png");
                coinBtn->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
                coinBtn->setPosition(cocos2d::Point(characterRect.origin.x+characterRect.size.width + HOME_BOTTOM_SCROLL_VIEW_ICON_IN_CHARACTER.x,
                                                    characterRect.origin.y+HOME_BOTTOM_SCROLL_VIEW_ICON_IN_CHARACTER.y));
                //            coinBtn->addTouchEventListener(this, (cocos2d::ui::SEL_TouchEvent)(&CharacterScrollView::characterCoinClicked));
                characterItemHolder->addChild(coinBtn, 2);
                
                std::stringstream ss;
                ss << price;
                
                auto coinText = ss.str();
                auto coinFontSize = coinText.length() == 1 ? HOME_BOTTOM_SCROLL_VIEW_COIN_FONT_SIZE_DIGIT1 :
                coinText.length() == 2 ? HOME_BOTTOM_SCROLL_VIEW_COIN_FONT_SIZE_DIGIT2 : HOME_BOTTOM_SCROLL_VIEW_COIN_FONT_SIZE_DIGIT3;
                auto coinNum = Label::createWithSystemFont(coinText, FONT_NAME_BOLD, coinFontSize);
                coinNum->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::CENTER);
                coinNum->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
                coinNum->setColor(Color3B(0xff,0x54,0x11));
                coinNum->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
                coinNum->enableShadow(Color4B(255,255,255,255.0f*0.51f),cocos2d::Size(1.0,-0.866), 0);
                coinNum->setPosition(cocos2d::Point(coinBtn->getContentSize().width/2,
                                                    coinBtn->getContentSize().height - HOME_BOTTOM_SCROLL_VIEW_COIN_VISIBLE_HEIGHT+HOME_BOTTOM_SCROLL_VIEW_COIN_VISIBLE_HEIGHT/2));
                coinBtn->addChild(coinNum);
            } else {
//                log("charKey  - %s", characterKeyStr->getCString());
                bool free = characterInfo["free"].GetBool();
                if (!free) {
                    const char *registerTimeChar;
                    if (characterInfo.HasMember("expiration_date"))
                    {
                        registerTimeChar = characterInfo["expiration_date"].GetString();
//                        log("characterscroll - registerTime : %s", registerTimeChar);
                    }
                    
                    if (characterInfo.HasMember("expired_hour")) {
                        auto leftTime = Util::getItemLeftTime(registerTimeChar,characterInfo["expired_hour"].GetInt());
                        int day = (leftTime/60/60)/24;
                        int hour = leftTime/60/60;
                        int minute = leftTime/60;
                        
                        std::string bgfileName;
                        std::string leftTimeStr;
                        Color3B numcolor;
                        if (day >= 1) {
                            if (hour == 23 && minute > 30) hour = 24;
                            
                            std::stringstream daystream;
                            daystream << day;
                            std::string dayStr = daystream.str();
                            leftTimeStr = dayStr;
                            
                            std::string fileName("background_frame_limited_days.png");
                            bgfileName = fileName;
                            
                            numcolor = Color3B(0x03, 0x3e, 0xa1);
                        } else {
                            if (hour < 30) hour = 1;
                            
                            std::stringstream hourtream;
                            hourtream << hour;
                            std::string hourStr = hourtream.str();
                            leftTimeStr = hourStr;
                            
                            std::string fileName("background_frame_limited_hours.png");
                            bgfileName = fileName;
                            
                            numcolor = Color3B(0xff, 0x00, 0x00);
                        }
                        
                        float timeFontSize = (leftTimeStr.length() > 1)?HOME_FRAME_LIMIT_FONTSIZE_01:HOME_FRAME_LIMIT_FONTSIZE_02;
                        
                        auto limitBackground = Sprite::create(bgfileName);
                        limitBackground->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
                        limitBackground->setPosition(cocos2d::Point(characterRect.origin.x+characterRect.size.width + HOME_BOTTOM_SCROLL_VIEW_ICON_IN_CHARACTER.x,
                                                                    characterRect.origin.y+HOME_BOTTOM_SCROLL_VIEW_ICON_IN_CHARACTER.y));
                        characterItemHolder->addChild(limitBackground,2);
                        
                        auto timeLabel = Label::createWithSystemFont(leftTimeStr, FONT_NAME_BOLD, timeFontSize,HOME_FRAME_LIMIT_LABEL_SIZE,TextHAlignment::CENTER, TextVAlignment::CENTER);
                        timeLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
                        timeLabel->setColor(numcolor);
                        timeLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
                        timeLabel->enableShadow(Color4B(255,255,255,255.0f*0.51f),cocos2d::Size(1.0,-0.866), 0);
                        timeLabel->setPosition(HOME_FRAME_LIMIT_LABEL);
                        limitBackground->addChild(timeLabel);
                    }
                    
                    
                }
                
            }
        }
    }
    
    return characterItemHolder;
}

void CharacterScrollView::deleteButtonClicked(Ref *obj, ui::TouchEventType type)
{
    switch (type) {
        case ui::TOUCH_EVENT_ENDED: {
            auto selDelButton = ((ui::Button*)obj);
            selectedCharKey = (__String *)selDelButton->getUserData();
            auto holderNode = (Node *)selDelButton->getParent();
            selDeleteChar = holderNode;
            
            Director::getInstance()->pause();
            PopupLayer *modal = PopupLayer::create(POPUP_TYPE_WARNING,
                                                   Device::getResString(Res::Strings::CC_SETPOSTION_POPUP_WARNING_DELETE_CHARACTER),//"Selected character will be deleted. Deleted records can't be restored.",
                                                   Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL),//"Cancel",
                                                   Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK));//"OK");
            
            modal->setTag(DELETE_CHAR_POPUP);
            modal->setDelegate(this);
            auto runningScene = Director::getInstance()->getRunningScene();
            runningScene->addChild(modal, 1000);
//            auto btn = (ui::Button*)obj;
//            auto deleteCharIndex = btn->getTag();
//            auto delCharKey = (__String*)characterList->getObjectAtIndex(deleteCharIndex);
//            log("delete button clicked tag : %i charKey : %s", deleteCharIndex, delCharKey->getCString());
////            characterList->removeObjectAtIndex(btnTag);
//            
//            runRemoveCharacterAnimation(delCharKey);
//
//            this->adjustLine();
        } break;
        case ui::TOUCH_EVENT_BEGAN: break;
        case ui::TOUCH_EVENT_MOVED: break;
        case ui::TOUCH_EVENT_CANCELED: break;
        default: break;
    }
}

bool CharacterScrollView::addCharacter(std::string charKey)
{
    log("add character %s", charKey.c_str());
    int index = characterList->count();
    auto item = createCharacterForScrollView(charKey, index);
    
    auto ud = UserDefault::getInstance();
    auto characterJsonString = ud->getStringForKey(charKey.c_str());
    rapidjson::Document characterInfo;
    characterInfo.Parse<0>(characterJsonString.c_str());
    
    auto baseChar = characterInfo["base_char"].GetString();
    auto baseCharacterJsonString = ud->getStringForKey(baseChar);
    rapidjson::Document baseCharacterInfo;
    baseCharacterInfo.Parse<0>(baseCharacterJsonString.c_str());
    
    auto normal = (char *)baseCharacterInfo["item_normal"].GetString();
    auto normalImage = __String::create(normal);
    auto characterItemNormal = Sprite::create(normalImage->getCString());
    
    cocos2d::Rect characterRect = cocos2d::Rect((HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_HOLDER_SIZE.width-characterItemNormal->getContentSize().width)/2,
                                                HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_HOLDER_SIZE.height-characterItemNormal->getContentSize().height,
                                                characterItemNormal->getContentSize().width,
                                                characterItemNormal->getContentSize().height);
    
    auto deleteButton = cocos2d::ui::Button::create("home_create_delete_btn_normal.png", "home_create_delete_btn_press.png");
    deleteButton->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
    deleteButton->setPosition(cocos2d::Point(characterRect.origin.x+characterRect.size.width + HOME_BOTTOM_SCROLL_VIEW_ICON_IN_CHARACTER.x, characterRect.origin.y+HOME_BOTTOM_SCROLL_VIEW_ICON_IN_CHARACTER.y));
    deleteButton->addTouchEventListener(this, (cocos2d::ui::SEL_TouchEvent)(&CharacterScrollView::deleteButtonClicked));
    deleteButton->setUserData(characterList->getObjectAtIndex(index));
    deleteButtons->addObject(deleteButton);
    
    changePositionAtAnchor(item, cocos2d::Point::ANCHOR_MIDDLE_TOP);
    item->setScale(0.5f);
    
    scrollView->addChild(item, TOUCHABLE_LAYER, CHARACTER_BASE_TAG+index);
    
    auto spriteName = __String::createWithFormat("home_background_frame_pin_%02d.png", index%4+1);
    auto pin = Sprite::create(spriteName->getCString());
    pin->setAnchorPoint(cocos2d::Point::ANCHOR_TOP_LEFT);
//    cocos2d::Point pos = cocos2d::Point(item->getPositionX() + (HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_HOLDER_SIZE.width - pin->getContentSize().width)/2,
//                                        HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_HOLDER_SIZE.height + item->getPositionY() + HOME_BOTTOM_SCROLL_VIEW_CHARACTER_PIN_TOP_PADDING);

    cocos2d::Point pos = cocos2d::Point(item->getPositionX() - (pin->getContentSize().width/2),
                                        item->getPositionY() + HOME_BOTTOM_SCROLL_VIEW_CHARACTER_PIN_TOP_PADDING);
    pin->setPosition(pos);
    scrollView->addChild(pin, TOUCHABLE_LAYER+1);
    
    __String *charKeyClone = __String::create(charKey)->clone();
    charKeyClone->retain();
    
    items->setObject(item, charKeyClone->getCString());
    pins->addObject(pin);
//    pins->setObject(pin, charKeyClone->getCString());
    
//    auto moveByPoint = cocos2d::Point(HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_HOLDER_SIZE.width, 0);
    
    float moveDuration = 0.25f;
    
    this->scrollView->setTouchEnabled(false);
//    this->scrollView->setContentOffsetInDuration(cocos2d::Point(scrollOffset.x - moveByPoint.x, scrollOffset.y), moveDuration);
    
    auto newCharacterCallback = CallFunc::create([=](){
        this->scrollView->setTouchEnabled(true);
        auto fadeIn = FadeIn::create(1.0f);
        deleteButton->setOpacity(0.0f);
        item->addChild(deleteButton, 2, TAG_DELETE_BUTTON);
        deleteButton->runAction(fadeIn);
        
        if (superType == CHAR_RECORD) {
            if (_scrollViewDelegate) {
                _scrollViewDelegate->onSelectCharacter(this, item, charKeyClone, index);
            }
        }
    });
    
    // scale animation
    auto startDelay = DelayTime::create(moveDuration);
    auto scaleTo = ScaleTo::create(moveDuration, 1.0f);
    auto appearAction = Sequence::create(startDelay, scaleTo, newCharacterCallback, NULL);
    item->runAction(appearAction);

    this->adjustLine();
    
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    cocos2d::Point scrollOffset = scrollView->getContentOffset();
    this->scrollView->setContentOffsetInDuration(cocos2d::Point(-(scrollView->getContentSize().width-visibleSize.width), scrollOffset.y), 0.25f);
    return true;
}

bool CharacterScrollView::removeCharacter(__String *charKey)
{
//    log("charkey : %s", charKey->getCString());
    auto ud = UserDefault::getInstance();
    
// remain character data
//    ud->setStringForKey(charKey->getCString(), "");
    
    auto orderJsonString = ud->getStringForKey(ORDER_KEY, "");
    rapidjson::Document orderDocument;
    orderDocument.Parse<0>(orderJsonString.c_str());
    rapidjson::Value& itemOrder = orderDocument["character_order"];
    rapidjson::Value newOrder(rapidjson::kArrayType);
    rapidjson::Document::AllocatorType& allocator = orderDocument.GetAllocator();
    
    for (rapidjson::SizeType i = 0; i < itemOrder.Size(); i++) {
        auto charaterKey = itemOrder[i].GetString();
        if (charKey->compare(charaterKey) == 0) {
            log("delete item");
            continue; // pass
        }
        log("item %s  del: %s", charaterKey, charKey->getCString());
        newOrder.PushBack(charaterKey, allocator);
    }
    
    orderDocument["character_order"] = newOrder;
    rapidjson::StringBuffer strbuf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
    orderDocument.Accept(writer);
    auto resultOrderJsonString = strbuf.GetString();
    ud->setStringForKey(ORDER_KEY, resultOrderJsonString);
    ud->flush();
    
    return true;
}

void CharacterScrollView::runRemoveCharacterAnimation(__String* charKey)
{
    std::string keyStr(charKey->getCString());
    auto characterIndex = this->getCharacterIndex(keyStr);
    
    this->removeCharacter(charKey);
    
    if (characterIndex == CC_INVALID_INDEX) {
        log("not found");
    } else {
        cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
        auto charToBeDelete = (Node*)items->objectForKey(charKey->getCString());
//        auto pinToBeDelete = (Sprite*)pins->objectForKey(charKey->getCString());
        
        float moveDuration = 0.25f;
        auto scaleTo = ScaleTo::create(moveDuration, 0.5f);
        auto placeTo = Place::create( cocos2d::Point(-visibleSize.width*2, -visibleSize.height*2));
        auto removeCallback = CallFunc::create([&, visibleSize, characterIndex, moveDuration](){
            
            cocos2d::Point scrollOffset = scrollView->getContentOffset();
            auto moveByPoint = cocos2d::Point(-HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_HOLDER_SIZE.width,0);
            this->scrollView->setContentOffsetInDuration(cocos2d::Point(scrollOffset.x - moveByPoint.x, scrollOffset.y), moveDuration);
            
            auto moveBy = MoveBy::create(moveDuration, moveByPoint);
            auto currentCharCount = characterList->count();
            
            int nextIndex = currentCharCount;
            bool processNearChar = false;
            
            for (int index = characterIndex; index < currentCharCount; index++) {
                float bottomPadding = 0;
                if((index%3 == 0)) {
                    bottomPadding = - HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_BOTTOM_PADDING1 + HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_BOTTOM_PADDING3;
                } else if((index%3 == 1)) {
                    bottomPadding = HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_BOTTOM_PADDING1 -HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_BOTTOM_PADDING2;
                } else if ((index%3 == 2)){
                    bottomPadding = HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_BOTTOM_PADDING2 - HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_BOTTOM_PADDING3;
                }

                moveByPoint = cocos2d::Point(-HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_HOLDER_SIZE.width,bottomPadding);
                moveBy = nullptr;
                moveBy = MoveBy::create(moveDuration, moveByPoint);

                auto charKey = (__String*)characterList->getObjectAtIndex(index);
                auto holder = (Node*)items->objectForKey(charKey->getCString());
                holder->setTag(CHARACTER_BASE_TAG + index-1);
                auto boundingRect = holder->getBoundingBox();
                auto originXInScreen = boundingRect.origin.x + scrollOffset.x;
                
                if (index == currentCharCount) {
                    auto lastAnimationBookCallback = CallFunc::create([&](){
                        this->scrollView->setTouchEnabled(true);
                    });
                    auto lastBookAnimationSequence = Sequence::create(moveBy->clone(), lastAnimationBookCallback, NULL);
                    holder->runAction(lastBookAnimationSequence);
                } else {
                    holder->runAction(moveBy->clone());
                }
                
//                Sprite *pinSprite = (Sprite*)pins->objectForKey(charKey->getCString());
//                cocos2d::Rect delBoundingRect;
//                float originDelXInScreen = 0;
//                if (pinSprite) {
//                    delBoundingRect = pinSprite->getBoundingBox();
//                    originDelXInScreen = delBoundingRect.origin.x + scrollOffset.x;
//                }
                
//                if (originXInScreen < visibleSize.width) {
//                    if (index == currentCharCount) {
//                        auto lastAnimationBookCallback = CallFunc::create([&](){
//                            this->scrollView->setTouchEnabled(true);
////                            auto currentContentsSize = this->scrollView->getContentSize();
////                            this->scrollView->setContentSize( cocos2d::Size(currentContentsSize.width-HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_HOLDER_SIZE.width, currentContentsSize.height ));
//                        });
//                        auto lastBookAnimationSequence = Sequence::create(moveBy->clone(), lastAnimationBookCallback, NULL);
//                        holder->runAction(lastBookAnimationSequence);
////                        if (pinSprite) {
////                            pinSprite->runAction(moveBy->clone());
////                        }
//                    } else {
//                        holder->runAction(moveBy->clone());
////                        if (pinSprite) {
////                            pinSprite->runAction(moveBy->clone());
////                        }
//                    }
//                } else {
//                    if (!processNearChar) { // Will be moved in screen but now in out screen.
//                        processNearChar = true;
//                        auto lastAnimationCallback = CallFunc::create([&](){
//                            this->scrollView->setTouchEnabled(true);
////                            auto currentContentsSize = this->scrollView->getContentSize();
////                            this->scrollView->setContentSize( cocos2d::Size(currentContentsSize.width-HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_HOLDER_SIZE.width, currentContentsSize.height ));
//                        });
//                        auto lastAnimationSequence = Sequence::create(moveBy->clone(), lastAnimationCallback, NULL);
//                        holder->runAction(lastAnimationSequence);
//                        
////                        if (pinSprite) {
////                            pinSprite->runAction(moveBy->clone());
////                        }
//                    } else {
//                        nextIndex = index;
//                        break;
//                    }
//                }
            }
            
            
            for (int index = nextIndex; index < currentCharCount; index++) {
                auto charKey = (__String *)characterList->getObjectAtIndex(index);
                auto holder = (Node*)items->objectForKey(charKey->getCString());
                auto originPosition = holder->getPosition();
                holder->setPosition( cocos2d::Point(originPosition.x+moveByPoint.x, originPosition.y+moveByPoint.y));
                
//                Sprite *pinSprite = (Sprite*)pins->objectForKey(charKey->getCString());
//                if (pinSprite) {
//                    auto pinOriginPosition = pinSprite->getPosition();
//                    pinSprite->setPosition(cocos2d::Point(pinOriginPosition.x+moveByPoint.x, pinOriginPosition.y+moveByPoint.y));
//                }
            }
        });
        
        auto doneCallback = CallFunc::create([=](){
//            auto selDeletePin = (Sprite*)pins->objectForKey(selectedCharKey->getCString());
            auto selDeletePin = (Sprite*)pins->getLastObject();
            selDeletePin->removeFromParentAndCleanup(true);
            selDeleteChar->removeFromParentAndCleanup(true);
            items->removeObjectForKey(selectedCharKey->getCString());
//            pins->removeObjectForKey(selectedCharKey->getCString());
            pins->removeLastObject();
            std::string charStr(selectedCharKey->getCString());
            if (deleteButtons->count() > 0) {
                deleteButtons->removeObjectAtIndex(this->getCharacterIndex(charStr)-fixItemCount);
            }
            characterList->removeObjectAtIndex(this->getCharacterIndex(charStr));
            log("after items->count : %i %zi %zi", items->count(), pins->count(), characterList->count());
            this->adjustLine();
            this->setEnableDeleteButtons(true);
            scrollView->isAnimate = false;
        });
        
        auto disappearAction = Sequence::create(scaleTo, placeTo, removeCallback, doneCallback, NULL);
        charToBeDelete->runAction(disappearAction);
    }
    
}

void CharacterScrollView::adjustLine()
{
    std::vector<std::string> lineResources;
    if (superType == CHAR_HOME) {
        lineResources.push_back("home_background_frame_line_left.png");
    } else {
        lineResources.push_back("set_background_frame_line_left.png");
    }
    lineResources.push_back("set_background_frame_line_mid.png");
    if (superType == CHAR_HOME) {
        lineResources.push_back("home_background_frame_line_right.png");
    } else {
        lineResources.push_back("set_background_frame_line_right.png");
    }
    
    auto characterCount = characterList->count();
    auto characterItemContentSize = HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_HOLDER_SIZE;
    int whiteSpace = WHITESPACE_CHAR_DEFAULT;
    auto scrollViewWidth = HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_START_PADDING + characterItemContentSize.width*(characterCount + whiteSpace);
    
    auto targetMidLineNum = std::ceil( (scrollViewWidth - HOME_BOTTOM_LINE_LEFT.width - HOME_BOTTOM_LINE_RIGHT.width) / HOME_BOTTOM_LINE_MID.width );
    auto currentMidLineNum = linesCount-2;
    if (targetMidLineNum < currentMidLineNum) { // remove line
        auto moveByPoint = cocos2d::Point(-HOME_BOTTOM_LINE_MID.width+(HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_HOLDER_SIZE.width*2),0);
        auto toRemoveCount = currentMidLineNum - targetMidLineNum;
        auto container = scrollView->getChildren().at(0);
        Sprite *lastLine = (Sprite *)(container->getChildByTag(LINE_BASE_TAG+(linesCount-1)));
        auto listLineCurrentPosition = lastLine->getPosition();
        lastLine->setPosition(listLineCurrentPosition - cocos2d::Point(toRemoveCount * HOME_BOTTOM_LINE_MID.width, 0) );
        
        for (int index = currentMidLineNum; index > currentMidLineNum - toRemoveCount; index--) {
            Sprite *line = (Sprite *)(container->getChildByTag(LINE_BASE_TAG+index));
            line->removeFromParent();
        }
        
        Sprite *lastLineForNewTag = (Sprite *)(container->getChildByTag(LINE_BASE_TAG+(linesCount-1)));
        lastLineForNewTag->setTag(LINE_BASE_TAG+(linesCount-2));

        auto currentContentSize = scrollView->getContentSize();
        scrollView->setContentSize(currentContentSize - cocos2d::Size(toRemoveCount * HOME_BOTTOM_LINE_MID.width, 0));
        linesCount -= toRemoveCount;
        
        cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
        cocos2d::Point scrollOffset = scrollView->getContentOffset();
        if (scrollOffset.x < -(scrollView->getContentSize().width-visibleSize.width+characterItemContentSize.width)) {
            this->scrollView->setContentOffsetInDuration(cocos2d::Point(-(scrollView->getContentSize().width-visibleSize.width+characterItemContentSize.width), scrollOffset.y), 0.25f);
        }
    } else if (targetMidLineNum > currentMidLineNum){ // add line
        auto toAddCount = targetMidLineNum - currentMidLineNum;
        auto lastIndex = (linesCount-1);
        
        auto container = scrollView->getChildren().at(0);

        Sprite *lastLine = (Sprite *)(container->getChildByTag(LINE_BASE_TAG+lastIndex));
        auto listLineCurrentPosition = lastLine->getPosition();
        lastLine->setPosition(listLineCurrentPosition + cocos2d::Point(toAddCount * HOME_BOTTOM_LINE_MID.width, 0) );
        
        auto lineStart = cocos2d::Point(listLineCurrentPosition.x, HOME_BOTTOM_LINE_START.y);
        for (int index = lastIndex; index < lastIndex + toAddCount; index++) {
            auto line = Sprite::create("set_background_frame_line_mid.png");
            line->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
            line->setPosition(lineStart);
            line->setTag(LINE_BASE_TAG + index);
            scrollView->addChild(line, TOUCHABLE_LAYER-5);
            lineStart = lineStart + cocos2d::Point(line->getContentSize().width, 0);
        }
        
        auto currentContentSize = scrollView->getContentSize();
        scrollView->setContentSize(currentContentSize + cocos2d::Size(toAddCount * HOME_BOTTOM_LINE_MID.width, 0));
        
        lastLine->setTag(LINE_BASE_TAG + lastIndex + toAddCount);
        
        linesCount += toAddCount;
    } else {
        // no need to adjust
    }
//    
//    deleteModeDim->setContentSize(cocos2d::Size(scrollView->getContentSize().width + HOME_BOTTOM_LINE_MID.width, scrollView->getContentSize().height));
}

void CharacterScrollView::scrollToLastItem()
{
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    this->scrollView->setContentOffsetInDuration(cocos2d::Point(-(scrollView->getContentSize().width-visibleSize.width), scrollOffset.y), 0.25f);
}

void CharacterScrollView::scrollToFirstItem()
{
    if (!isScrollingToFirst) {
        isScrollingToFirst = true;
        this->scrollView->stopAllScroll();
        this->scrollView->setContentOffsetInDuration(cocos2d::Point::ZERO, 0.25f);
    }
}

void CharacterScrollView::loadJsonToScrollView(ScrollView *scrollView)
{
    auto ud = UserDefault::getInstance();
    auto orderContentStr = ud->getStringForKey(PRE_LOAD_OBJ_ORDER_KEY);
    rapidjson::Document orderDocument;
    orderDocument.Parse<0>(orderContentStr.c_str());
    rapidjson::Value& order = orderDocument["character_order"];
    
    // user order
    auto userOrderJsonString = ud->getStringForKey(ORDER_KEY, "");
    rapidjson::Document userOrderDocument;
    userOrderDocument.Parse<0>(userOrderJsonString.c_str());
    rapidjson::Value& userOrder = userOrderDocument["character_order"];
    
    auto characterSize = order.Size() + userOrder.Size();
    
    // create merged book Key array
    auto mergedCharacterKeyArray = __Array::createWithCapacity(characterSize);
    
    for(int index = 0; index < order.Size(); index++){
        mergedCharacterKeyArray->addObject( __String::create(order[rapidjson::SizeType(index)].GetString()) );
    }
    
    for(int index = 0; index < userOrder.Size(); index++){
        mergedCharacterKeyArray->addObject( __String::create(userOrder[rapidjson::SizeType(index)].GetString()) );
    }
    
    auto characterItemContentSize = HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_HOLDER_SIZE;
    
    parseErrorCount = 0;
    fixItemCount = 0;
    for (int index = 0; index < characterSize; index++) {
        auto characterKey = ((__String*)mergedCharacterKeyArray->getObjectAtIndex(index))->getCString();
        
        auto item = this->createCharacterForScrollView(characterKey, index);
        
        if (item != NULL) {
            items->setObject(item, characterKey);
            
            auto characterJsonString = ud->getStringForKey(characterKey);
            rapidjson::Document characterInfo;
            characterInfo.Parse<0>(characterJsonString.c_str());
            
            if (characterInfo.HasMember("base_char") ){
                scrollView->addChild(item, TOUCHABLE_LAYER, CHARACTER_BASE_TAG+index);
                
                auto baseChar = characterInfo["base_char"].GetString();
                auto baseCharacterJsonString = ud->getStringForKey(baseChar);
                rapidjson::Document baseCharacterInfo;
                baseCharacterInfo.Parse<0>(baseCharacterJsonString.c_str());
                
                auto normal = (char *)baseCharacterInfo["item_normal"].GetString();
                auto normalImage = __String::create(normal);
                auto characterItemNormal = Sprite::create(normalImage->getCString());
                
                cocos2d::Rect characterRect = cocos2d::Rect((HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_HOLDER_SIZE.width-characterItemNormal->getContentSize().width)/2,
                                                            HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_HOLDER_SIZE.height-characterItemNormal->getContentSize().height,
                                                            characterItemNormal->getContentSize().width,
                                                            characterItemNormal->getContentSize().height);
                
                auto deleteButton = cocos2d::ui::Button::create("home_create_delete_btn_normal.png", "home_create_delete_btn_press.png");
                deleteButton->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
                deleteButton->setPosition(cocos2d::Point(characterRect.origin.x+characterRect.size.width + HOME_BOTTOM_SCROLL_VIEW_ICON_IN_CHARACTER.x, characterRect.origin.y+HOME_BOTTOM_SCROLL_VIEW_ICON_IN_CHARACTER.y));
                deleteButton->addTouchEventListener(this, (cocos2d::ui::SEL_TouchEvent)(&CharacterScrollView::deleteButtonClicked));
                deleteButton->setUserData(characterList->getObjectAtIndex(index));
                item->addChild(deleteButton, 2, TAG_DELETE_BUTTON);
                deleteButtons->addObject(deleteButton);
                
            } else {
                fixItemCount++;
                scrollView->addChild(item, TOUCHABLE_LAYER_WITH_DIM, CHARACTER_BASE_TAG+index);
            }
            
            auto spriteName = __String::createWithFormat("home_background_frame_pin_%02d.png", index%4+1);
            auto pin = Sprite::create(spriteName->getCString());
            pin->setAnchorPoint(cocos2d::Point::ANCHOR_TOP_LEFT);
            cocos2d::Point pos = cocos2d::Point(item->getPositionX() + (characterItemContentSize.width - pin->getContentSize().width)/2,
                                                characterItemContentSize.height + item->getPositionY() + HOME_BOTTOM_SCROLL_VIEW_CHARACTER_PIN_TOP_PADDING);
            pin->setPosition(pos);
            //        pins->setObject(pin, characterKey);
            pins->addObject(pin);
            if (characterInfo.HasMember("base_char") ){
                scrollView->addChild(pin, TOUCHABLE_LAYER+1);
            } else {
                scrollView->addChild(pin, TOUCHABLE_LAYER-2);
            }
        }
        
    }
    
    std::vector<std::string> lineResources;
    if (superType == CHAR_HOME) {
        lineResources.push_back("home_background_frame_line_left.png");
    } else {
        lineResources.push_back("set_background_frame_line_left.png");
    }
    lineResources.push_back("set_background_frame_line_mid.png");
    if (superType == CHAR_HOME) {
        lineResources.push_back("home_background_frame_line_right.png");
    } else {
        lineResources.push_back("set_background_frame_line_right.png");
    }
    
    int whiteSpace = WHITESPACE_CHAR_DEFAULT;
    auto scrollViewWidth = HOME_BOTTOM_SCROLL_VIEW_CHARACTER_ITEM_START_PADDING + characterItemContentSize.width*(characterSize+whiteSpace);
    auto midLineNum = std::ceil( (scrollViewWidth - HOME_BOTTOM_LINE_LEFT.width - HOME_BOTTOM_LINE_RIGHT.width) / HOME_BOTTOM_LINE_MID.width );
    
    cocos2d::Point startLinePosition = HOME_BOTTOM_LINE_START;
    float totalLineWidth = startLinePosition.x;
    
    int lineCount = 0;
    while (lineCount < midLineNum + 2) { // +2 -> include left, right line
        int resourceIndex = (lineCount == 0)? 0:(lineCount == (midLineNum+1))? 2:1;
        auto line = Sprite::create(lineResources.at(resourceIndex));
        line->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        line->setPosition(cocos2d::Point(totalLineWidth, startLinePosition.y));
        line->setTag(LINE_BASE_TAG + lineCount);
        lineCount++;
        totalLineWidth += line->getContentSize().width;
        scrollView->addChild(line, TOUCHABLE_LAYER-5);
    }
    linesCount = lineCount;
    scrollView->setContentSize(cocos2d::Size(totalLineWidth, HOME_BACKGROUND_SCROLL_VIEW_HEIGHT));
}

void CharacterScrollView::reloadData()
{
    characterList->removeAllObjects();
    deleteButtons->removeAllObjects();
    items->removeAllObjects();
    pins->removeAllObjects();
    
    auto container = scrollView->getChildren().at(0);
    container->removeAllChildren();
    
    this->loadJsonToScrollView(this->scrollView);
//    this->scrollView->setContentOffset(cocos2d::Point(0,0), true);
}

void CharacterScrollView::setEnableDeleteButtons(bool enable)
{
    for(int index = 0; index < deleteButtons->count(); index++){
        ui::Button *deleteBtn = (ui::Button*)deleteButtons->getObjectAtIndex(index);
        deleteBtn->setTouchEnabled(enable);
    }
}

