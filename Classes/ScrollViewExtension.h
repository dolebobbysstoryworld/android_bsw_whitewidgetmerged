//
//  ScrollViewExtension.h
//  bobby
//
//  Created by Dongwook, Kim on 14. 4. 14..
//
//

#ifndef __bobby__ScrollViewExtension__
#define __bobby__ScrollViewExtension__

#include <iostream>
#include "cocos2d.h"
#include "extensions/cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

#define SELECT_THRESHOLD    5.0f
#define TOUCHABLE_LAYER     10
#define TOUCHABLE_LAYER_WITH_DIM    (TOUCHABLE_LAYER-3)
#define TOUCHABLE_DELETE    20

class ScrollViewExtension;
class ScrollViewExtensionDelegate
{
public:
    virtual ~ScrollViewExtensionDelegate() {}
    virtual void scrollViewDidScroll(ScrollViewExtension* view) = 0;
    virtual void scrollViewDidScrollEnd(ScrollViewExtension* view) = 0;
    virtual void scrollViewDidZoom(ScrollViewExtension* view) = 0;
    virtual void onSelectItem(ScrollViewExtension* view, Node* selectedItem, Touch *pTouch, Event *pEvent) = 0;
};

class ScrollViewExtension : public ScrollView, ScrollViewDelegate
{
public:
    static ScrollViewExtension* create(cocos2d::Size size, Node* container);
    static ScrollViewExtension* create();
    
    virtual bool onTouchBegan(Touch *touch, Event *event);
    virtual void onTouchMoved(Touch *touch, Event *event);
    virtual void onTouchEnded(Touch *touch, Event *event);
    virtual void onTouchCancelled(Touch *touch, Event *event);

    void scrollViewDidScroll(ScrollView* view);
    void scrollViewDidZoom(ScrollView* view);
    
    ScrollViewExtensionDelegate* getExtensionDelegate() { return _scrollViewExtensionDelegate; }
    void setExtensionDelegate(ScrollViewExtensionDelegate* pDelegate) { _scrollViewExtensionDelegate = pDelegate; }
    
    bool isScrollEnd();
    bool isAnimate;
    
//    bool deleteMode = false;
protected:
    bool isMoving;
    Touch *_touchBegan;
    Node *_selectedItem;
    ScrollViewExtensionDelegate *_scrollViewExtensionDelegate;
};

#endif /* defined(__bobby__ScrollViewExtension__) */
