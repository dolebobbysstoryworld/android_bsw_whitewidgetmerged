//
//  DoleProduct.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 16..
//
//

#ifndef _DOLEPRODUCT_H_
#define _DOLEPRODUCT_H_

#include "cocos2d.h"
#include "external/tinyxml2/tinyxml2.h"

NS_CC_BEGIN

class DoleProduct : public Ref
{
    enum class Category {
        Undefined = -1,
        Character = 1,/*C001*/
        Item,/*I001*/
        Background,/*B001*/
        BGM
    };
public:
    DoleProduct();
    virtual ~DoleProduct();
    static DoleProduct* create(tinyxml2::XMLElement *parent);
    bool init(tinyxml2::XMLElement *parent);
    const char *description();
    
    int getProductNumber() { return _productNo; }
    std::string getProductType() { return _productType; }
    int getSalePrice() { return _salePrice; }
    const char* getProductName() { return _productName.c_str(); }
    const char* getItemSaleType() { return _itemSaleType.c_str(); }
    const char* getProductCode() { return _productCode.c_str(); }
    
    void setPurchase(bool purchase) { _isPurchase = purchase; }
    bool getPurchase() { return _isPurchase; }
    void setRegisterDateTime(const char *time) { _registerDateTime = time; }
    const char* getRegisterDateTime() { return _registerDateTime.c_str(); }
    void setRemainQuantity(int quantity) { _remainQuantity = quantity; }
    int getRemainQuantity() { return _remainQuantity; }
    
    int getExpiredHour() {return _expiredHour;}
    
protected:
    Category _category;
    bool _isPurchase;

    int _productSaleNo;// m:type="Edm.Int32">7</d:ProductSaleNo> : 프로덕트 판매 설정 번호
    int _productNo;// m:type="Edm.Int32">8</d:ProductNo> : Item No
    std::string _serviceCode;//>SVR004</d:ServiceCode>
    int _saleStatus;// m:type="Edm.Byte">10</d:SaleStatus> : 판매중인것 10 (필터 설정해서 조회하면 됩니다.)
    std::string _saleStartDatetime;// m:type="Edm.DateTime">2014-06-30T11:55:44</d:SaleStartDatetime> : 판매 종료일
    std::string _salsEndDatetime;// m:type="Edm.DateTime">9999-12-31T00:00:00</d:SalsEndDatetime> : 판매시작일
    int _productPrice;// m:type="Edm.Int32">0</d:ProductPrice> : 아이템 원가
    int _salePrice;// m:type="Edm.Int32">0</d:SalePrice> : 아이템 판매가격
    std::string _productName;//>Banana 1</d:ProductName> : 아이템 이름
    std::string _productCode;//>C001</d:ProductCode> : Item Key
    std::string _productType;//>ITT001</d:ProductType> : Item Type
    std::string _itemSaleType;//>IST001</d:ItemSaleType> : 구매시 파라메터값, 조회된 값을 이용해도 되고 고정해도 됨
    int _expiredHour;// m:type="Edm.Int32">0</d:ProductHour>
    std::string _registerDateTime;
    int _remainQuantity;
    
//old property for products api
//public:
//    int getProductNumber() { return _ProductNo; }
//    __String *getProductType() { return _ProductType; }
//    int getProductCost() { return _ProductCost; }
//protected:
//    int _ProductNo;// m:type="Edm.Int32">41</d:ProductNo>
//    __String* _ProductRowGuid;// m:type="Edm.Guid">3f630beb-81fc-4752-90d6-26b392222a0e</d:ProductRowGuid>
//    __String* _ServiceCode;//>SVR004</d:ServiceCode>
//    __String* _ProductName;//>Background 12 - Space</d:ProductName>
//    __String* _ProductCode;//>B012</d:ProductCode>
//    __String* _ProductType;//>ITT001</d:ProductType>
//    __String* _SaleType;//>SLT002</d:SaleType>
//    __String* _ItemSaleType;//>IST001</d:ItemSaleType>
//    bool _ISIncludeBonus;// m:type="Edm.Boolean">false</d:ISIncludeBonus>
//    int _ProductCost;// m:type="Edm.Int32">500</d:ProductCost>
//    int _ProductHour;// m:type="Edm.Int32">0</d:ProductHour>
//    int _ProductQuantity;// m:type="Edm.Int16">1</d:ProductQuantity>
//    int _TotalQuantity;// m:type="Edm.Int32">0</d:TotalQuantity>
//    int _ReOrderTerms;// m:type="Edm.Int16">0</d:ReOrderTerms>
//    int _ReorderCount;// m:type="Edm.Int16">0</d:ReorderCount>
//    int _PurchaseAvailableQuantity;// m:type="Edm.Int16">0</d:PurchaseAvailableQuantity>
//    int _AvailableMinLevel;// m:type="Edm.Int32">1</d:AvailableMinLevel>
//    int _AvailableMaxLevel;// m:type="Edm.Int32">32767</d:AvailableMaxLevel>
//    int _AvailableGender;// m:type="Edm.Byte">99</d:AvailableGender>
//    int _InventoryKeepDays;// m:type="Edm.Int16">365</d:InventoryKeepDays>
//    __String* _Description;//></d:Description>
//    __String* _Property0;//></d:Property0>
//    __String* _Property1;//></d:Property1>
//    __String* _Property2;//></d:Property2>
//    __String* _Property3;//></d:Property3>
//    __String* _Property4;//></d:Property4>
//    bool _ISPropertyItems;// m:type="Edm.Boolean">false</d:ISPropertyItems>
//    bool _ISPackageItems;// m:type="Edm.Boolean">false</d:ISPackageItems>
//    bool _ISLotteryItems;// m:type="Edm.Boolean">false</d:ISLotteryItems>
//    bool _ISBonusItems;// m:type="Edm.Boolean">false</d:ISBonusItems>
//    bool _ISLinkageItems;// m:type="Edm.Boolean">false</d:ISLinkageItems>
//    bool _ISRecommendItems;// m:type="Edm.Boolean">true</d:ISRecommendItems>
//    bool _ISImages;// m:type="Edm.Boolean">false</d:ISImages>
};

NS_CC_END

#endif /* defined(_DOLEPRODUCT_H_) */
