//
//  TSDolesNetProgressLayer.cpp
//  bobby
//
//  Created by Lee mu hyeon on 2014. 7. 30..
//
//

#include "TSDoleNetProgressLayer.h"
#include <cocosbuilder/CocosBuilder.h>
#include "ResourceManager.h"
#include "Define.h"
#include "2d/CCProgressTimer.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "AppDelegate.h"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#import <UIKit/UIKit.h>
#ifndef MACOS
#include "TSFacebookVideoUploadManager.h"
#endif//MACOS
#endif

#define CLOSE_DURATION 0.2f

#define TAG_CCBI_ACTIVITY           900001000
#define TAG_CCBI_BOBBYPROGRESS      900001001
#define TAG_IMAGE_CIRCLEPROGRESS    900001002
#define TAG_LABEL_MESSAGE           900001003
#define TAG_POPUP_RECEIVECOIN       900001004

USING_NS_CC;

TSDoleNetProgressLayer::TSDoleNetProgressLayer():
_delegate(nullptr),
_animated(false),
_progressed(false),
_shareTarget(ShareTarget::None) {

}

TSDoleNetProgressLayer::~TSDoleNetProgressLayer() {
    log("TSDoleNetProgressLayer release");
}

TSDoleNetProgressLayer* TSDoleNetProgressLayer::create()
{
    TSDoleNetProgressLayer *layer = new TSDoleNetProgressLayer();
    if (layer && layer->init()) {
        layer->autorelease();
        return layer;
    }
    CC_SAFE_DELETE(layer);
    return nullptr;
}

void TSDoleNetProgressLayer::onEnter() {
    Layer::onEnter();
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [=](Touch* touch, Event* event) {return true;};
    listener->onTouchMoved = [=](Touch* touch, Event* event){};
    listener->onTouchEnded = CC_CALLBACK_2(TSDoleNetProgressLayer::onTouchEnded, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

void TSDoleNetProgressLayer::onExit() {
    _eventDispatcher->removeEventListenersForTarget(this);
    Layer::onExit();
}

bool TSDoleNetProgressLayer::init() {
    if ( !LayerColor::initWithColor(Color4B(0, 0, 0, 100)) ) return false;
    this->setTag(TAG_NET_PROGRESS);
    return true;
}

void TSDoleNetProgressLayer::removeProgressLayer() {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (_shareTarget == ShareTarget::Facebook) {
        [TSFacebookVideoUploadManager releaseSharedObject];
    }
#endif
    bool delResult = Device::deleteFile(_videoPath.c_str());
    if (delResult) {
        log("movie file delete success");
    } else {
        log("movie file delete fail");
    }
    this->removeFromParentAndCleanup(true);
}

void TSDoleNetProgressLayer::onTouchEnded(cocos2d::Touch* touch, cocos2d::Event  *event) {
    log("TSDoleNetProgressLayer::onTouchEnded");

}

void TSDoleNetProgressLayer::activityStart() {
    if (_animated == true) {
        return;
    }

    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();

    cocosbuilder::NodeLoaderLibrary *nodeLoaderLibrary = cocosbuilder::NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
    cocosbuilder::CCBReader *ccbReader = new cocosbuilder::CCBReader(nodeLoaderLibrary);
    Node *ccbiObj = ccbReader->readNodeGraphFromFile("loading.ccbi", this);
    auto ccbiSprite = (Sprite*)ccbiObj;
    ccbiSprite->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
    ccbiSprite->setPosition(Point(visibleSize.width/2, visibleSize.height/2));
    ccbReader->release();
    ccbiSprite->setTag(TAG_CCBI_ACTIVITY);
    if (ResourceManager::getInstance()->getCurrentType() != ResolutionType::ipadhd) {
        ccbiSprite->setScale(0.5, 0.5);
    }
    this->addChild(ccbiSprite);
    _animated = true;
}

void TSDoleNetProgressLayer::activityStop(const char *message, float closetime, bool autocloseForProgressLayer) {
    if (_animated == false) {
        return;
    }
    auto ccbiSprite = this->getChildByTag(TAG_CCBI_ACTIVITY);
    ccbiSprite->removeFromParentAndCleanup(true);
    _animated = false;
    
    if (message == NULL) {
        auto sequence = Sequence::create(DelayTime::create(CLOSE_DURATION), CallFunc::create(CC_CALLBACK_0(TSDoleNetProgressLayer::removeProgressLayer, this)), NULL);
        this->runAction(sequence);
        return;
    }

    auto toastLayer = ToastLayer::create(message, closetime);
    toastLayer->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    toastLayer->setPosition(cocos2d::Point::ZERO);
    if (autocloseForProgressLayer) {
        toastLayer->setDelegate(this);
    }
    this->addChild(toastLayer);
}

void TSDoleNetProgressLayer::onToastRemoved(ToastLayer *toast) {
    auto sequence = Sequence::create(DelayTime::create(CLOSE_DURATION), CallFunc::create(CC_CALLBACK_0(TSDoleNetProgressLayer::removeProgressLayer, this)), NULL);
    this->runAction(sequence);
}

void TSDoleNetProgressLayer::startYouTubeShare(const char *path, const char* bookTitle, const char *comment, Home *home) {
    _homeLayer = home;
    _shareTarget = ShareTarget::Youtube;
#ifndef MACOS
    Share::YoutubeUploadClient* youtubeUploadClient = new Share::YoutubeUploadClient();
    youtubeUploadClient->setOnResultCallback(this);
    youtubeUploadClient->setOnProgressCallback(this);
    std::list<std::string> tags;
    tags.push_back("#Bobby'sStoryWorld");
    tags.push_back("Uncensored");
//    tags.push_back("4+");

    _videoPath = path;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    Share::startYoutubeUpload(youtubeUploadClient, path, bookTitle, comment, tags);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    NSURL *fileURL = [NSURL URLWithString:[NSString stringWithCString:path encoding:NSUTF8StringEncoding]];
    NSString *pathString = [fileURL path];
    Share::startYoutubeUpload(youtubeUploadClient, [pathString UTF8String], bookTitle, comment, tags);
#endif
    size_t totalSize = Device::getFileSize(path);
    this->initProgress(totalSize);
#endif
}

void TSDoleNetProgressLayer::startFacebookShare(const char *path, const char* bookTitle, const char *comment, Home *home) {
    _homeLayer = home;
    _shareTarget = ShareTarget::Facebook;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	AppDelegate* app = (AppDelegate*)Application::getInstance();
	if(app != nullptr) {
		app->uploadVideoToFacebook(bookTitle, comment, path);
	}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    NSURL *fileURL = [NSURL URLWithString:[NSString stringWithCString:path encoding:NSUTF8StringEncoding]];
    NSString *pathString = [fileURL path];
#ifdef __FBUPLODTESTMODE_
    [[TSFacebookVideoUploadManager sharedObject] shareOnFacebook:pathString
                                                           title:[NSString stringWithCString:bookTitle encoding:NSUTF8StringEncoding]
                                                         comment:[NSString stringWithCString:comment encoding:NSUTF8StringEncoding]
                                                        targetUI:(void *)this];
#else//__FBUPLODTESTMODE_
    [[TSFacebookVideoUploadManager sharedObject] uploadFacebookVideo:pathString
                                                               title:[NSString stringWithCString:bookTitle encoding:NSUTF8StringEncoding]
                                                             comment:[NSString stringWithCString:comment encoding:NSUTF8StringEncoding]
                                                            targetUI:(void *)this];
#endif//__FBUPLODTESTMODE_
#endif//(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#if TARGET_IPHONE_SIMULATOR
    NSInteger totalSize = [[TSFacebookVideoUploadManager sharedObject] videolength];
    this->initProgress((int)totalSize);
#else
    size_t totalSize = Device::getFileSize(path);
    this->initProgress(totalSize);
#endif
}


void TSDoleNetProgressLayer::initProgress(int total) {
    if (_progressed == true) {
        return;
    }
    
    _total = total;
    
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();

    _progressTimer = ProgressTimer::create(Sprite::create("loading_progressbar.png"));
    _progressTimer->setType( ProgressTimer::Type::RADIAL );
    _progressTimer->setTag(TAG_IMAGE_CIRCLEPROGRESS);
    _progressTimer->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
    _progressTimer->setPosition(Point(visibleSize.width/2, visibleSize.height/2));
    this->addChild(_progressTimer);
    
    cocosbuilder::NodeLoaderLibrary *nodeLoaderLibrary = cocosbuilder::NodeLoaderLibrary::newDefaultNodeLoaderLibrary();
    cocosbuilder::CCBReader *ccbReader = new cocosbuilder::CCBReader(nodeLoaderLibrary);
    Node *ccbiObj = ccbReader->readNodeGraphFromFile("bobbys_loading.ccbi", this);
    auto ccbiSprite = (Sprite*)ccbiObj;
    ccbiSprite->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
    ccbiSprite->setPosition(Point(visibleSize.width/2, visibleSize.height/2));
    ccbReader->release();
    ccbiSprite->setTag(TAG_CCBI_BOBBYPROGRESS);
    this->addChild(ccbiSprite);

    Label *message = Label::createWithSystemFont(Device::getResString(Res::Strings::CC_JOURNEYMAP_SHARE_PROGRESS_NOTICE), FONT_NAME_BOLD, PROGRESS_LABEL_FONTSIZE);
    message->setTag(TAG_LABEL_MESSAGE);
    message->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::CENTER);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    message->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE);
#else
    message->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    message->setWidth(visibleSize.width);
#endif
    message->setPosition(PROGRESS_LABEL_POSITION); // guide :android:((1920-340)/2)|((1080/2)-(176/2+26+48)) pad :((2048-492)/2)|(1536/2-(252/2+38+66)), phone((1136-206)/2)|(640/2-(104/2+14+30))
    message->setColor(Color3B(0xfc,0x8b,0x27));
    message->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    message->enableShadow(Color4B(0,0,0,255*0.3f),LABEL_SHADOW_DEFAULT_SIZE, 0);
    this->addChild(message);
  
    _progressed = true;
}

void TSDoleNetProgressLayer::showProgress(double current) {
    if (_progressed == false) {
        return;
    }
    int labelText = (int)(((float)current / (float)_total)*100.0f);
    log("showProgress : %d %%", labelText);
    _progressTimer->setPercentage(((float)current / (float)_total)*100.0f);
}

void TSDoleNetProgressLayer::endProgress(const char *completed, float closetime, bool autocloseForProgressLayer) {
    if (_progressed == false) {
        return;
    }
    this->removeChildByTag(TAG_CCBI_BOBBYPROGRESS);
    this->removeChildByTag(TAG_IMAGE_CIRCLEPROGRESS);
    this->removeChildByTag(TAG_LABEL_MESSAGE);
    _progressed = false;
    
    if (completed == NULL) {
        return;
    }
    
    auto toastLayer = ToastLayer::create(completed, closetime);
    toastLayer->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    toastLayer->setPosition(cocos2d::Point::ZERO);
    if (autocloseForProgressLayer) {
        toastLayer->setDelegate(this);
    }
    this->addChild(toastLayer);
}

void TSDoleNetProgressLayer::onSuccessCallback(Share::Client* client, int errorCode, Share::KeyJSONValueMap& resultMap) {
    if (_shareTarget == ShareTarget::Youtube) {
        Share::YoutubeUploadClient* youtubeUploadClient = static_cast<Share::YoutubeUploadClient*>(client);
        Share::UploadState status = youtubeUploadClient->getUploadState();
        if (status == Share::UploadState::INIT_STARTED ) {
            return;
        }
    } else {
        if (errorCode != 0 ) {
            return;
        }
    }

    onPostUploadVideo();
}

void TSDoleNetProgressLayer::onPostUploadVideo()
{
    bool result = Device::deleteFile(_videoPath.c_str());
    if (result) {
        log("movie file delete success");
    } else {
        log("movie file delete fail");
    }
    /// TODO::tuilise 공유 적립 처리 해라
    _protocol = new TSDoleChargeFreeCash();
    _protocol->setDelegate(this);
    _protocol->init(100);
    _protocol->request();
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    if (_shareTarget == ShareTarget::Youtube)
    {
        AppDelegate* app = (AppDelegate*)Application::getInstance();
        app->sharedVideoYoutube();
    }
    else if(_shareTarget == ShareTarget::Facebook)
    {
        AppDelegate* app = (AppDelegate*)Application::getInstance();
        app->sharedVideoFacebook();
    }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#   ifndef MACOS
    
#   endif
#endif
}

void TSDoleNetProgressLayer::onFailedCallback(Share::Client* client, int errorCode, Share::KeyJSONValueMap& resultMap) {
    bool result = Device::deleteFile(_videoPath.c_str());
    if (result) {
        log("movie file delete success");
    } else {
        log("movie file delete fail");
    }
    PopupLayer *sharePopup =
    PopupLayer::create(POPUP_TYPE_WARNING,
                       Device::getResString(Res::Strings::CC_JOURNEYMAP_POPUP_ERROR_SHARE),
                       //"Failed to share on social network.\nPlease check network connection or social network status and try again.",
                       Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK));//"OK");
    sharePopup->setDelegate(this);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    sharePopup->preventCloseSelf();
#endif
//    sharePopup->setTag(TAG_POPUP_SHAREERROR);
    this->addChild(sharePopup, 1000);
}

void TSDoleNetProgressLayer::onCancelledCallback(Share::Client* client) {
    //TODO
}

void TSDoleNetProgressLayer::onProgressCallback(Share::Client* client, double uploadTotal, double uploadedNow) {
    log("TSDoleNetProgressLayer::onProgressCallback progress=%f", uploadedNow);
    this->showProgress(uploadedNow);
}

void TSDoleNetProgressLayer::popupButtonClicked(PopupLayer *popup, cocos2d::Ref *obj) {
    if (popup->getTag() == TAG_POPUP_RECEIVECOIN) {
        this->endProgress(Device::getResString(Res::Strings::CC_JOURNEYMAP__SHARE_COMPLETE));
        return;
    }
    
    auto sequence = Sequence::create(DelayTime::create(CLOSE_DURATION), CallFunc::create(CC_CALLBACK_0(TSDoleNetProgressLayer::removeProgressLayer, this)), NULL);
    this->runAction(sequence);
}

void TSDoleNetProgressLayer::showMessagePopup(const char* message) {
    PopupLayer *sharePopup =
    PopupLayer::create(POPUP_TYPE_WARNING, message, Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK));//"OK"
    sharePopup->setDelegate(this);
    this->addChild(sharePopup, 1000);
}

void TSDoleNetProgressLayer::onRequestStarted(TSDoleProtocol *protocol, TSNetResult result) {
    
}

void TSDoleNetProgressLayer::onResponseEnded(TSDoleProtocol *protocol, TSNetResult result) {
    bool delResult = Device::deleteFile(_videoPath.c_str());
    if (delResult) {
        log("movie file delete success");
    } else {
        log("movie file delete fail");
    }
    
    switch (result) {
        case TSNetResult::Success : {
            __String *message = __String::createWithFormat(Device::getResString(Res::Strings::PF_POPUP_RECEIVED_DOLECOIN), 100);
            PopupLayer *modal = PopupLayer::create(POPUP_TYPE_COINS, message->getCString(),
                                                   Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK));//"OK");
            modal->setDelegate(this);
            modal->setTag(TAG_POPUP_RECEIVECOIN);
            this->addChild(modal, 1000);
            _homeLayer->doleCoinGetBalanceProtocol();
        } break;
        default:
            // TODO::tuilise - 저장 후 다시 적립하는 코드 필요?
            if (protocol->getReturnCode() == 90001) {//JIRA 379 이슈에 다른 서버 오류 예외 처리/ 1000 코인 이상 정립 후에는 적립 오류 표시 안함
                log("protocol->getReturnCode() == 90001 / charge cache fail!!! 1000 coin / 1 day over");
                this->endProgress(Device::getResString(Res::Strings::CC_JOURNEYMAP__SHARE_COMPLETE));
            } else {
                PopupLayer *sharePopup =
                PopupLayer::create(POPUP_TYPE_WARNING,
                                   Device::getResString(Res::Strings::CC_JOURNEYMAP_POPUP_ERROR_SHARE),
                                   //"Failed to share on social network.\nPlease check network connection or social network status and try again.",
                                   Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK));//"OK");
                sharePopup->setDelegate(this);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
                sharePopup->preventCloseSelf();
#endif
                //    sharePopup->setTag(TAG_POPUP_SHAREERROR);
                this->addChild(sharePopup, 1000);
            }
            break;
    }
}

void TSDoleNetProgressLayer::onProtocolFailed(TSDoleProtocol *protocol, TSNetResult result) {
    bool delResult = Device::deleteFile(_videoPath.c_str());
    if (delResult) {
        log("movie file delete success");
    } else {
        log("movie file delete fail");
    }
    // TODO::tuilise - 저장 후 다시 적립하는 코드 필요?
    PopupLayer *sharePopup =
    PopupLayer::create(POPUP_TYPE_WARNING,
                       Device::getResString(Res::Strings::CC_JOURNEYMAP_POPUP_ERROR_SHARE),
                       //"Failed to share on social network.\nPlease check network connection or social network status and try again.",
                       Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK));//"OK");
    sharePopup->setDelegate(this);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    sharePopup->preventCloseSelf();
#endif
    //    sharePopup->setTag(TAG_POPUP_SHAREERROR);
    this->addChild(sharePopup, 1000);
}

//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
void TSDoleNetProgressLayer::onRequestCanceled(TSDoleProtocol *protocol) {

}
//#endif
