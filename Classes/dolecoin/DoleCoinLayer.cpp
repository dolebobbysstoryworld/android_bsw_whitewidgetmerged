//
//  DoleCoinLayer.cpp
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 29..
//
//

#include "DoleCoinLayer.h"
#include "DoleCoinStatusLayer.h"

USING_NS_CC;

DoleCoinLayer::DoleCoinLayer() {

}

DoleCoinLayer::~DoleCoinLayer() {

}

DoleCoinLayer* DoleCoinLayer::create()
{
    DoleCoinLayer *layer = new DoleCoinLayer();
    if (layer && layer->init())
    {
        layer->autorelease();
        return layer;
    }
    CC_SAFE_DELETE(layer);
    return nullptr;
}

void DoleCoinLayer::onEnter() {
    Layer::onEnter();
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [=](Touch* touch, Event* event) {return true;};
    listener->onTouchMoved = [=](Touch* touch, Event* event){};
    listener->onTouchEnded = [=](Touch* touch, Event* event){};
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

void DoleCoinLayer::onExit() {
    _eventDispatcher->removeEventListenersForTarget(this);
    Layer::onExit();
}

bool DoleCoinLayer::init() {
    if ( !LayerColor::initWithColor(Color4B(0, 0, 0, 179)) ) return false;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    auto doleCoinStatusLayer = DoleCoinStatusLayer::create();
    doleCoinStatusLayer->setPosition( Point( (visibleSize.width - getLayerSize().width)/2,
                                            (visibleSize.height - getLayerSize().height)/2 ) );
    this->addChild(doleCoinStatusLayer);
    
    return true;
}
