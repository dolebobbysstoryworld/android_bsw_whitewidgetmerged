//
//  DoleCoinStatusLayer.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 29..
//
//

#ifndef __bobby__DoleCoinStatusLayer__
#define __bobby__DoleCoinStatusLayer__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "DoleAPI.h"

NS_CC_BEGIN

class DoleCoinStatusLayer : public LayerColor
{
#pragma mark - class init
public:
    DoleCoinStatusLayer();
    virtual ~DoleCoinStatusLayer();
    static DoleCoinStatusLayer* create();
    bool init();
    virtual void onEnter();
    virtual void onExit();

    void characterImageLayoutSetting();
    void imageLayoutSetting();

    void touchEventForButton(Ref *pSender, ui::TouchEventType type);
    void doleCoinGetBalanceProtocol();
    void onHttpRequestCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);
};

NS_CC_END


#endif /* defined(__bobby__DoleCoinStatusLayer__) */
