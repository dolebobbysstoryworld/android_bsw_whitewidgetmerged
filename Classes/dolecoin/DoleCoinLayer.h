//
//  DoleCoinLayer.h
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 29..
//
//

#ifndef __bobby__DoleCoinLayer__
#define __bobby__DoleCoinLayer__

#include "cocos2d.h"
#include "ResourceManager.h"

NS_CC_BEGIN

class DoleCoinLayer : public LayerColor
{
public:
    DoleCoinLayer();
    virtual ~DoleCoinLayer();
    static DoleCoinLayer* create();
    bool init();
    virtual void onEnter();
    virtual void onExit();
    static inline cocos2d::Size getLayerSize() { return LOGINLAYER_BACKGROUND_SIZE; }//Size(828,1224);
    
protected:
};

static inline bool IsIOS() {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    return true;
#else
    return false;
#endif
}

static inline bool IsAndroid() {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    return true;
#else
    return false;
#endif
}


NS_CC_END


#endif /* defined(__bobby__DoleCoinLayer__) */
