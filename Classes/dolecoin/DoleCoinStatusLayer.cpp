//
//  DoleCoinStatusLayer.cpp
//  bobby
//
//  Created by Lee mu hyeon on 2014. 5. 29..
//
//

#include "DoleCoinStatusLayer.h"
#include "DoleCoinLayer.h"
#include "Define.h"

USING_NS_CC;
using namespace ui;

DoleCoinStatusLayer::DoleCoinStatusLayer() {
    
}

DoleCoinStatusLayer::~DoleCoinStatusLayer() {}

DoleCoinStatusLayer* DoleCoinStatusLayer::create()
{
    DoleCoinStatusLayer *layer = new DoleCoinStatusLayer();
    if (layer && layer->init())
    {
        layer->autorelease();
        return layer;
    }
    CC_SAFE_DELETE(layer);
    return nullptr;
}

void DoleCoinStatusLayer::onEnter() {
    Layer::onEnter();
    this->doleCoinGetBalanceProtocol();
}

void DoleCoinStatusLayer::onExit() {
    Layer::onExit();
}

bool DoleCoinStatusLayer::init() {
    if ( !LayerColor::initWithColor(Color4B(0, 0, 0, 0), DoleCoinLayer::getLayerSize().width, DoleCoinLayer::getLayerSize().height) ) return false;
    
    return true;
}

void DoleCoinStatusLayer::characterImageLayoutSetting() {
    float width = Director::getInstance()->getOpenGLView()->getFrameSize().width;
    if ((width == 1024 || width == 2048) && IsIOS() == true) {//pad 계열만 표시
        Sprite *lionSprite = Sprite::create("full_popup_character_01.png");
        lionSprite->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        lionSprite->setPosition(ACCOUNTLAYER_POSITION_FULL_POPUP_CHARACTER_01);//Point(-143, 0)
        this->addChild(lionSprite);
        
        Sprite *racoonSprite = Sprite::create("full_popup_character_02.png");
        racoonSprite->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        racoonSprite->setPosition(ACCOUNTLAYER_POSITION_FULL_POPUP_CHARACTER_02);//Point(680, 472+210)
        this->addChild(racoonSprite);
    }
}

void DoleCoinStatusLayer::imageLayoutSetting() {
    Sprite *backgroundMap = Sprite::create("bg_map_full.png");
    backgroundMap->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    backgroundMap->setPosition(Point::ZERO);
    this->addChild(backgroundMap, 0);
    
    Sprite *backgroundFactor = Sprite::create("bg_map_factor.png");
    backgroundFactor->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    backgroundFactor->setPosition(Point::ZERO);
    this->addChild(backgroundFactor, 0);
    
    this->characterImageLayoutSetting();
    
    Button *closeButton = Button::create("btn_cancel_normal.png", "btn_cancel_press.png");
    closeButton->setContentSize(ACCOUNTLAYER_SIZE_BTN_CANCEL_NORMAL);//Size(80, 80)
    closeButton->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    closeButton->setPosition(ACCOUNTLAYER_POSITION_BTN_CANCEL_NORMAL);//Point(692,1085)
//    closeButton->setTag(TAG_BTN_CLOSE);
    closeButton->addTouchEventListener(this, toucheventselector(DoleCoinStatusLayer::touchEventForButton));
    this->addChild(closeButton);
}

void DoleCoinStatusLayer::touchEventForButton(Ref *pSender, TouchEventType type) {
    switch (type) {
        case TOUCH_EVENT_ENDED: {
            log("Button Touch Up");
        } break;
            
        case TOUCH_EVENT_BEGAN: log("Button Touch Down"); break;
        case TOUCH_EVENT_MOVED: log("Button Touch Move"); break;
        case TOUCH_EVENT_CANCELED: log("Button Touch Cancelled"); break;
        default: break;
    }
}

void DoleCoinStatusLayer::doleCoinGetBalanceProtocol() {
//    DoleAPI *api = DoleAPI::getInstance();
//    int userNo = UserDefault::getInstance()->getIntegerForKey(UDKEY_INTEGER_USERNO, -1);
//    if (userNo > 0) {
//        api->getBalance(userNo, CC_CALLBACK_2(DoleCoinStatusLayer::onHttpRequestCompleted, this));
//    }
}


void DoleCoinStatusLayer::onHttpRequestCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response) {
    if (!response) return;
    
    printf("Tag: %s\n", response->getHttpRequest()->getTag());
    std::vector<char>*buffer = response->getResponseData();
    std::string str(buffer->begin(), buffer->end());
    
    printf("Json: %s\n", str.c_str());
    
    if (strlen(str.c_str()) > 0) {
        rapidjson::Document ret;
        ret.Parse<0>(str.c_str());
        bool retValue = ret["Return"].GetBool();
        int retCode = ret["ReturnCode"].GetInt();
        printf("Parsed Data: retValue: %d, retCode: %d\n", retValue, retCode);
        if (retValue) {
            int eventBalance = ret["EventBalance"].GetInt();
            log("GetBalance success EventBalance : %d\n", eventBalance);
        } else {
        }
    }
}
