//
//  PlayAndRecordScene.cpp
//  bobby
//
//  Created by Dongwook, Kim on 14. 5. 21..
//
//

#include "PlayAndRecordScene.h"
#include "ResourceManager.h"
#include "AppDelegate.h"
#include "LabelExtension.h"
#include "SimpleAudioEngine.h"
#include "BGMManager.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
#include "CocosRecorder.h"
#endif
#endif

#define TAG_PROGRESS_LEFT   10
#define TAG_PROGRESS_CENTER 11
#define TAG_PROGRESS_RIGHT  12

#define TAG_PROGRESS_01     100
#define TAG_PROGRESS_02     101

#define TAG_POPUP_CANCEL    200

#define UI_HIDE_DELAY   2.0f

Scene* PlayAndRecord::createScene(__String* bookKey, PlayAndRecordType type)
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = PlayAndRecord::create(bookKey, type);
    layer->setTag(TAG_PLAY_AND_RECORDING);
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

PlayAndRecord* PlayAndRecord::create(__String* bookKey, PlayAndRecordType type)
{
    PlayAndRecord *pRet = new PlayAndRecord();
    pRet->bookKey = bookKey;
    pRet->_type = type;
    
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

// on "init" you need to initialize your instance
bool PlayAndRecord::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !LayerColor::initWithColor( Color4B(0,0,0,0xff)) )
    {
        return false;
    }
    
    _delegate = nullptr;
    
    cocos2d::Size visibleSize = Director::getInstance()->getVisibleSize();
    
    
    playMenuItem = MenuItemImage::create("btn_set_record_play_normal.png", "btn_set_record_play_pressed.png",
                                         CC_CALLBACK_1(PlayAndRecord::playMenuCallback, this));
    playMenuItem->setPosition(cocos2d::Point(visibleSize.width/2, visibleSize.height/2));
    playMenuItem->setVisible(false);

    titleLabel = this->createTitleLabel();
    titleLabel->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_BOTTOM);
    titleLabel->setPosition(cocos2d::Point(playMenuItem->getContentSize().width/2, MAP_PLAY_TITLE_BOTTOM_MARGIN));
    playMenuItem->addChild(titleLabel);

    pauseMenuItem = MenuItemImage::create("btn_set_record_pause_normal.png", "btn_set_record_pause_pressed.png",
                                          CC_CALLBACK_1(PlayAndRecord::playMenuCallback, this));
    pauseMenuItem->setPosition(cocos2d::Point(visibleSize.width/2, visibleSize.height/2));
    
    titleLabel2 = this->createTitleLabel();
    titleLabel2->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_BOTTOM);
    titleLabel2->setPosition(cocos2d::Point(playMenuItem->getContentSize().width/2, MAP_PLAY_TITLE_BOTTOM_MARGIN));
    pauseMenuItem->addChild(titleLabel2);
    
    controlMenu = Menu::create(playMenuItem, pauseMenuItem, NULL);
    controlMenu->setPosition(cocos2d::Point::ZERO);
    this->addChild(controlMenu, 3);
    
    Vector<MenuItem*> itemList;

    switch (_type) {
        case PlayAndRecordType::PlayOnly:
        {
            auto btnBack = MenuItemImage::create("btn_map_back_normal.png",
                                                 "btn_map_back_press.png",
                                                 CC_CALLBACK_1(PlayAndRecord::btnBackCallback, this));
            btnBack->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
            btnBack->setPosition(cocos2d::Point(BOOKMAP_BTN_BACK));
            itemList.pushBack(btnBack);
        }
            break;
        case PlayAndRecordType::RecordForDownload:
        case PlayAndRecordType::RecordForShare:
        {
            auto cancelPress = this->createResizableButton("btn_share_cancel_normal_left.png",
                                                           "btn_share_cancel_normal_center.png",
                                                           "btn_share_cancel_normal_right.png",
                                                           "btn_share_cancel_icon.png",
                                                           Device::getResString(Res::Strings::CC_JOURNEYMAP_BUTTON_TITLE_CANCEL),//"cancel",
                                                           SHARE_CANCEL_TEXT_FONT_SIZE,
                                                           SHARE_CANCEL_ICON,
                                                           SHARE_CANCEL_TEXT_LEFT_PADDING,
                                                           SHARE_CANCEL_TEXT_RIGHT_PADDING);
            
            auto cancelNormal = this->createResizableButton("btn_share_cancel_press_left.png",
                                                            "btn_share_cancel_press_center.png",
                                                            "btn_share_cancel_press_right.png",
                                                            "btn_share_cancel_icon.png",
                                                            Device::getResString(Res::Strings::CC_JOURNEYMAP_BUTTON_TITLE_CANCEL),//"cancel",
                                                            SHARE_CANCEL_TEXT_FONT_SIZE,
                                                            SHARE_CANCEL_ICON,
                                                            SHARE_CANCEL_TEXT_LEFT_PADDING,
                                                            SHARE_CANCEL_TEXT_RIGHT_PADDING);
            
            auto cancelMenuItem = MenuItemSprite::create(cancelNormal,
                                                            cancelPress,
                                                            CC_CALLBACK_1(PlayAndRecord::cancelMenuCallback, this));
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
            cancelMenuItem->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
            cancelMenuItem->setPosition(cocos2d::Point(visibleSize.width-SHARE_CANCEL_BUTTON.x, SHARE_CANCEL_BUTTON.y));
#else
            cancelMenuItem->setAnchorPoint(cocos2d::Point::ANCHOR_TOP_RIGHT);
            cancelMenuItem->setPosition(cocos2d::Point(visibleSize.width-SHARE_CANCEL_BUTTON.x, visibleSize.height - SHARE_CANCEL_BUTTON.y));
#endif
            itemList.pushBack(cancelMenuItem);
        }
            break;
        default:
        {
            log("invalid type! : %d", _type);
            return false;
        }
            break;
    }
    
    menu = Menu::createWithArray(itemList);
    menu->setPosition(cocos2d::Point::ZERO);
    this->addChild(menu, 3);

    if (_type == PlayAndRecordType::PlayOnly) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        this->createProgressBar();
#else
        this->createPlayProgressBar();
#endif
    } else {
        this->createProgressBar();
    }
    
    showUI(false);

//    auto menu = Menu::create(btnBack, playMenuItem, pauseMenuItem, NULL);
//    menu->setPosition(cocos2d::Point::ZERO);
//    this->addChild(menu, 3);

    recording = false;
    onOpeningEnding = false;
    onTransition = false;
    visibleUI = false;
    
    playing = false;
    paused = false;
    
    hideUIAction = nullptr;
    
    
    _chapterIndex = 0;
    playedTime = 0;

    player = ChapterPlayer::create();
    this->addChild(player, 0);
    player->setDelegate(this);
    
    bool chapterExist = false;
    do {
        _chapterIndex++;
        chapterExist = isChapterDataExist(_chapterIndex);
    } while (!chapterExist && (_chapterIndex < 6) );
    
    player->loadJsonData(this->bookKey->getCString(), _chapterIndex);
    
    // fixed title (Intro) at opening
    auto titleString = this->getTitle(1);
    if (titleString != nullptr) {
        titleLabel->setString(titleString->getCString());
        titleLabel2->setString(titleString->getCString());
    }
    
    if (this->bookKey) {
        auto ud = UserDefault::getInstance();
        auto bookJsonString = ud->getStringForKey(this->bookKey->getCString());
        if (bookJsonString != "") {
            rapidjson::Document bookJson;
            bookJson.Parse<0>(bookJsonString.c_str());
            auto bookTitle = bookJson["title"].GetString();
            auto authorTitle = bookJson["author"].GetString();
            openingLayer = Opening::createOpening(__String::create(bookTitle), __String::create(authorTitle));
            openingLayer->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
            openingLayer->setPosition(cocos2d::Point(0, 0));
            player->addChild(openingLayer, 10);
            onOpeningEnding = true;
            openingLayer->setDelegate(this);
            openingLayer->retain();
        }
    }
    
    totalDuration = getTotalDuration();
    
    // add opening duration
    if(openingLayer != nullptr) {
        totalDuration += (openingLayer->getTotalDuration() * 1000);
    }
    
    // add ending durtaion
    totalDuration += (ENDING_DURATION * 1000);
    
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (_type != PlayAndRecordType::PlayOnly) {
        flipRenderTexture = RenderTexture::create(visibleSize.width, visibleSize.height);
        flipRenderTexture->retain();
        
        auto s = Director::getInstance()->getWinSize();
        
        auto sprite = flipRenderTexture->getSprite();
        sprite->setPosition(cocos2d::Point(s.width/2, s.height/2));
        sprite->setFlippedY(false);
        
        flipRenderTexture->begin();
        player->visit();
        flipRenderTexture->end();
        
        this->createCocosRecorder();
        
        CocosRecorder *cocosRecorder = [CocosRecorder sharedRecorder];
        [cocosRecorder renderTextureRecorder]->begin();
        flipRenderTexture->getSprite()->visit();
        [cocosRecorder renderTextureRecorder]->end();
    }
#endif
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(PlayAndRecord::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(PlayAndRecord::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(PlayAndRecord::onTouchEnded, this);
    listener->onTouchCancelled = CC_CALLBACK_2(PlayAndRecord::onTouchCancelled, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    this->scheduleUpdate();
    
    return true;
}

PlayAndRecord::~PlayAndRecord()
{
    openingLayer->removeFromParent();
    endingLayer->removeFromParent();
    transition->release();
    player->release();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    if (_type != PlayOnly){
        flipRenderTexture->release();
    }
#endif
}

void PlayAndRecord::onEnterTransitionDidFinish()
{
    log("enter transition finished");
    startPlay();
}

void PlayAndRecord::doneRecording()
{
    recording = false;
    log("recording finished");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	AppDelegate* app = (AppDelegate*)Application::getInstance();
	if(app != nullptr) {
		app->stopVideoRecord();
	}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
    if (_type != PlayAndRecordType::PlayOnly){
        CocosRecorder *cocosRecorder = [CocosRecorder sharedRecorder];
        [cocosRecorder finishRecording];
        [cocosRecorder renderTextureRecorder]->removeFromParentAndCleanup(false);
    }
#endif
#endif
}

float PlayAndRecord::getTotalDuration()
{
    auto ud = UserDefault::getInstance();
    auto bookJsonString = ud->getStringForKey(this->bookKey->getCString());

    if (bookJsonString != "") {
        rapidjson::Document bookDoc;
        bookDoc.Parse<0>(bookJsonString.c_str());
        rapidjson::Value& chapters = bookDoc["chapters"];

        auto existChapters = 0;
        float totalDuration = 0;
        for (int index = 1; index <= 5; index++) {
            __String *chapterName = __String::createWithFormat("chapter%d", index);
            rapidjson::Value& chapterData = chapters[chapterName->getCString()];
            if (!chapterData.IsNull() && chapterData.HasMember("char_datas")) {
                auto timelineKey = chapterData["timeline_file"].GetString();
                auto timelineJsonString = ud->getStringForKey(timelineKey);
                
                rapidjson::Document timelineDocument;
                timelineDocument.Parse<0>(timelineJsonString.c_str());

                rapidjson::Value& timelinesValue = timelineDocument["timelines"];
                auto timeLineCount = timelinesValue.Size();
                rapidjson::Value& timeline = timelinesValue[rapidjson::SizeType(timeLineCount-1)];
                rapidjson::Value& timestamp = timeline["timestamp"];
                double duration = timestamp.GetDouble(); // ms
                totalDuration += duration;
                existChapters++;
            }
        }
        
        if(existChapters > 0) {
            totalDuration += (existChapters - 1) * (TRANSITION_DURATION * 1000);
        }
        log("total duration : %f", totalDuration);
        return totalDuration;
    } return -1;
}

void PlayAndRecord::createCocosRecorder()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
    CocosRecorder *cocosRecorder = [CocosRecorder sharedRecorder];
    [cocosRecorder createWriter];
    auto s = Director::getInstance()->getWinSize();
    [cocosRecorder renderTextureRecorder]->setPosition(cocos2d::Point(s.width+s.width/2, s.height+s.height/2));
    [cocosRecorder createFBO];
//    this->addChild([cocosRecorder renderTextureRecorder], -1);
#endif
#endif
}

void PlayAndRecord::chapterPlayFinished(int chapterIndex)
{
    if (chapterIndex == 5) {
        this->showEnding();
    } else {
        bool chapterExist = false;
        do {
            _chapterIndex++;
            chapterExist = isChapterDataExist(_chapterIndex);
        } while (!chapterExist && (_chapterIndex < 6) );
        
        if (chapterExist) {
            player->prepareTransition();
            player->clear();
            player->loadJsonData(this->bookKey->getCString(), _chapterIndex);
            auto titleString = this->getTitle(_chapterIndex);
            if (titleString != nullptr) {
                titleLabel->setString(titleString->getCString());
                titleLabel2->setString(titleString->getCString());
            }
            player->startTransition();
            onTransition = true;
            
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
            CocosRecorder *cocosRecorder = [CocosRecorder sharedRecorder];
            if ( cocosRecorder.isWriting ){
                [cocosRecorder muxAudio:nil withBgm:@"preload_sounds/scene_transition.wav" duration:TRANSITION_DURATION*1000];
            }
#endif
        } else {
            this->showEnding();
        }
    }

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    AppDelegate* app = (AppDelegate*)Application::getInstance();
    if(app != nullptr) {
    	app->onRecordSceneChange();
    }
#endif
}

void PlayAndRecord::onTransitionFinished(int chapterIndex)
{
//    onTransition = false;

    player->play();
    this->startAudioRecord();
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    AppDelegate* app = (AppDelegate*)Application::getInstance();
    if(app != nullptr) {
    	app->onRecordSceneChange();
    }
#endif

//    if (pendingPause) {
//        pendingPause = false;
//        pauseRecording();
//    } else if (pendingResume) {
//        pendingResume = false;
//        resumeRecording();
//    } else {
//        player->play();
//        this->startAudioRecord();
//    }
}

void PlayAndRecord::showEnding()
{
    if (endingLayer == nullptr) {
        endingLayer = Ending::create();
        player->addChild(endingLayer, 10);
        endingLayer->setDelegate(this);
        endingLayer->retain();
    }
    
    endingLayer->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    endingLayer->setPosition(cocos2d::Point(0, 0));
    endingLayer->setVisible(true);
    endingLayer->startAnimation();
    
    onOpeningEnding = true;
    
    // fixed title (Finale)
    auto titleString = this->getTitle(5);
    if (titleString != nullptr) {
        titleLabel->setString(titleString->getCString());
        titleLabel2->setString(titleString->getCString());
    }
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    CocosRecorder *cocosRecorder = [CocosRecorder sharedRecorder];
    if ( cocosRecorder.isWriting ){
        NSString *soundFileName;
        LanguageType currentLanguageType = Application::getInstance()->getCurrentLanguage();
        switch (currentLanguageType) {
            case LanguageType::KOREAN:
            {
                soundFileName = @"preload_sounds/kor/scene_ending.wav";
                break;
            }
            case LanguageType::JAPANESE: {
                soundFileName = @"preload_sounds/ja/scene_ending.wav";
                break;
            }
            case LanguageType::ENGLISH:
            default: {
                soundFileName = @"preload_sounds/en/scene_ending.wav";;
                break;
            }
        }
        [cocosRecorder muxAudio:nil withBgm:soundFileName duration:ENDING_DURATION*1000];
    }
#endif
}

bool PlayAndRecord::isChapterDataExist(int chapterIndex)
{
    auto ud = UserDefault::getInstance();
    auto bookJsonString = ud->getStringForKey(this->bookKey->getCString());
    
    if (bookJsonString != "") {
        rapidjson::Document bookDoc;
        bookDoc.Parse<0>(bookJsonString.c_str());
        rapidjson::Value& chapters = bookDoc["chapters"];
        __String *chapterName = __String::createWithFormat("chapter%d", chapterIndex);
        
        rapidjson::Value& chapterData = chapters[chapterName->getCString()];
        if (!chapterData.IsNull() && chapterData.HasMember("char_datas")) {
            return true;
        }
    }
    return false;
}

__String** PlayAndRecord::getAudioFileList()
{
	__String* voice[SCENE_COUNT_MAX];
	int index = 0;
	auto ud = UserDefault::getInstance();
    auto bookJsonString = ud->getStringForKey(this->bookKey->getCString());

    if (bookJsonString != "") {
        rapidjson::Document bookDoc;
        bookDoc.Parse<0>(bookJsonString.c_str());
        rapidjson::Value& chapters = bookDoc["chapters"];
        for(int i = 0 ; i < SCENE_COUNT_MAX ; i++) {
        	voice[i] = nullptr;
            __String *chapterName = __String::createWithFormat("chapter%d", i + 1);
    		rapidjson::Value& chapterData = chapters[chapterName->getCString()];
    		if (!chapterData.IsNull() && chapterData.HasMember("char_datas")) {
				auto voiceFile = chapterData["voice_file"].GetString();
				if(voiceFile) {
					voice[i] = __String::create(voiceFile);
				}
    		}
	}
    }

    return voice;
}

void PlayAndRecord::onOpeningFinished(Opening* opening)
{
    auto titleString = this->getTitle(_chapterIndex);
    if (titleString != nullptr) {
        titleLabel->setString(titleString->getCString());
        titleLabel2->setString(titleString->getCString());
    }
    
    player->play();
    this->startAudioRecord();
    
//    openingLayer->release();
//    openingLayer = nullptr;
    
    onOpeningEnding = false;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    AppDelegate* app = (AppDelegate*)Application::getInstance();
    if(app != nullptr) {
    	app->onRecordSceneChange();
    }
#endif
}

void PlayAndRecord::onEndingFinished(Ending* ending)
{
    if (_type != PlayAndRecordType::PlayOnly){
        this->doneRecording();
    }

    playing = false;
    onOpeningEnding = false;

    bool returnToOpening = false;
    if (visibleUI && _type == PlayAndRecordType::PlayOnly) {
        // return to start
        
        _chapterIndex = 0;
        playedTime = 0;
        
        player->clear();
        player->resetTransitionState();
        
        bool chapterExist = false;
        do {
            _chapterIndex++;
            chapterExist = isChapterDataExist(_chapterIndex);
        } while (!chapterExist && (_chapterIndex < 6) );
        
        player->loadJsonData(this->bookKey->getCString(), _chapterIndex);
        
        // fixed title (Intro) at opening
        auto titleString = this->getTitle(1);
        if (titleString != nullptr) {
            titleLabel->setString(titleString->getCString());
            titleLabel2->setString(titleString->getCString());
        }
        
        endingLayer->setVisible(false);
        openingLayer->initAnimation();
        
        openingLayer->startAnimation();
        openingLayer->playBackgroundMusic();
        
        openingLayer->pauseAnimation();

        playMenuItem->setVisible(true);
        pauseMenuItem->setVisible(false);
        showUI(true);

        setProgress(0.0);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	    playProgressBarBg->setVisible(false);
	    playProgressBarParent->setVisible(false);
#else
        if (_type == PlayAndRecordType::PlayOnly) {
            playProgressBarHolder->setVisible(false);
        } else {
            playProgressBarParent->setVisible(false);
        }
#endif
        
//        playing = true;
        paused = false;
        
        returnToOpening = true;
    }
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    std::string recordFileName("");
    AppDelegate* app = (AppDelegate*)Application::getInstance();
    if(app != nullptr) {
    	recordFileName = app->getRecordingFilename();
    }
    if(_type == PlayAndRecordType::RecordForDownload) {
    if (this->bookKey) {
        auto ud = UserDefault::getInstance();
        auto bookJsonString = ud->getStringForKey(this->bookKey->getCString());
        if (bookJsonString != "") {
            rapidjson::Document bookJson;
            bookJson.Parse<0>(bookJsonString.c_str());
            auto bookTitle = bookJson["title"].GetString();
            auto authorTitle = bookJson["author"].GetString();
        	if(app != nullptr) {
            		app->saveVideoToDevice(recordFileName.c_str(), bookTitle, authorTitle);
            	}
        	}
        }
    }

    if (!returnToOpening) {
        if (_delegate != nullptr) {
            _delegate->playAndRecordFinished(_type, true, recordFileName);
        }
        this->btnBackCallback(nullptr);
    }
#else
    if (!returnToOpening && _type == PlayAndRecordType::PlayOnly) {
        if (_delegate != nullptr) {
            _delegate->playAndRecordFinished(_type, true, "");
        }
        this->btnBackCallback(nullptr);
    }
#endif
}

void PlayAndRecord::startAudioRecord()
{
    __String *voiceFileName = player->getVoiceFileName();
    __String *bgmFileName = player->getBgmFileName();
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
    if (voiceFileName != nullptr) {
        CocosRecorder *cocosRecorder = [CocosRecorder sharedRecorder];
        NSString *voiceFile = [NSString stringWithUTF8String:voiceFileName->getCString()];
        NSString *bgmFile = (bgmFileName != nullptr) ? [NSString stringWithUTF8String:bgmFileName->getCString()] : nil;
        [cocosRecorder muxAudio:voiceFile withBgm:bgmFile duration:player->getDuration()*1000];
    }
#endif
#endif
}

void PlayAndRecord::update(float delta)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
    CocosRecorder *cocosRecorder = [CocosRecorder sharedRecorder];

    if (cocosRecorder.isWriting && (playing && !paused)) {
        if (_type != PlayAndRecordType::PlayOnly) {
            // make flipped sprite
            flipRenderTexture->begin();
            player->visit();
            flipRenderTexture->end();
            
            // recording from flipped sprite
            [cocosRecorder renderTextureRecorder]->begin();
            flipRenderTexture->getSprite()->visit();
            [cocosRecorder renderTextureRecorder]->end();
        }
    }
#endif
#endif
    
    if (playing && !paused) {
        playedTime += delta;
        float currentProgress = playedTime / (totalDuration / 1000);
        
        
        if (_type == PlayAndRecordType::PlayOnly) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
            this->setProgress(currentProgress);
#else
            this->setPlayProgress(currentProgress);
#endif
        } else {
            this->setProgress(currentProgress);
        }
    }
}

void PlayAndRecord::setPlayProgress(float value)
{
    if (value > 1.0f) {
        log("progress bar - reached to end");
        return;
    }
    
    if (value == 0) {
        playProgressBar->setVisible(false);
    } else {
        playProgressBar->setVisible(true);
    }
    
    Node *targetBar = playProgressBar;
    std::string centerFileName = "recording_play_progress_center.png";
    
    auto center = targetBar->getChildByTag(TAG_PROGRESS_CENTER);
    
    auto progressBarLeft = targetBar->getChildByTag(TAG_PROGRESS_LEFT);
    auto progressBarRight = targetBar->getChildByTag(TAG_PROGRESS_RIGHT);
    
    auto progressWidth = RECORDING_PROGRESS_BAR_SIZE.width - progressBarLeft->getContentSize().width - progressBarRight->getContentSize().width;
    auto centerWidth = std::ceil(progressWidth * value);
    
    if (center) {
        center->removeFromParentAndCleanup(true);
    }
    
    if (centerWidth > 0) {
        auto newCenter = Sprite::create(centerFileName, cocos2d::Rect(0, 0, centerWidth, RECORDING_PROGRESS_BAR_SIZE.height));
        newCenter->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        newCenter->setPosition(cocos2d::Point(progressBarLeft->getContentSize().width, 0));
        newCenter->setTag(TAG_PROGRESS_CENTER);
        targetBar->addChild(newCenter);
    }
    progressBarRight->setPosition(cocos2d::Point(progressBarLeft->getContentSize().width + centerWidth, 0));
}

void PlayAndRecord::btnBackCallback(Ref* pSender)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Tap.wav", false);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	AppDelegate* app = (AppDelegate*)Application::getInstance();
	if(app != nullptr) {
		app->stopVideoRecord();
	}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS    
    CocosRecorder *cocosRecorder = [CocosRecorder sharedRecorder];
    if ( cocosRecorder.isWriting ){
        [cocosRecorder cancelRecording];
//        [cocosRecorder renderTextureRecorder]->removeFromParentAndCleanup(false);
    }

#endif
#endif
    player->clear();
    
//    this->removeAllChildren();
    
    auto director = Director::getInstance();
    director->popSceneWithTransition([&](Scene *scene) {
        this->transition = TransitionProgressOutIn::create(0.4f, scene);
        transition->retain();
        return transition;
    });
    
    log("back from playAndRecord");
}

void PlayAndRecord::onEnter()
{
    Layer::onEnter();

    TSBGMManager::getInstance()->stop();
    auto keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyReleased = CC_CALLBACK_2(PlayAndRecord::onKeyReleased, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(keyboardListener, this);
}

void PlayAndRecord::onExit()
{
    _eventDispatcher->removeEventListenersForTarget(this);
    log("on exit");
    Layer::onExit();
}

void PlayAndRecord::onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	if(keyCode == cocos2d::EventKeyboard::KeyCode::KEY_BACKSPACE) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		btnBackCallback(nullptr);
#endif
	}
}

void PlayAndRecord::playAndRecordFinished(bool success, std::string fileName)
{
    if (_delegate != nullptr) {
        _delegate->playAndRecordFinished(_type, success, fileName);
    }
    
     this->btnBackCallback(nullptr);
}

void PlayAndRecord::startRecording()
{
    // start record
    if (!recording) {
        recording = true;
        
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		AppDelegate* app = (AppDelegate*)Application::getInstance();
		if(app != nullptr) {
			__String** list = getAudioFileList();
			const char* voice[SCENE_COUNT_MAX];
            //			const char* bgm[SCENE_COUNT_MAX];
			for(int i = 0 ; i < SCENE_COUNT_MAX ; i++) {
				voice[i] = (list[i] == nullptr ? nullptr : list[i]->getCString());
                //				bgm[i] = (list[SCENE_COUNT_MAX + i] == nullptr ? nullptr : list[SCENE_COUNT_MAX + i]->getCString());
			}
			app->feedAudioFileName(voice);
            
			auto s = Director::getInstance()->getWinSize();
			app->startVideoRecord((int)s.width, (int)s.height, -1);
		}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
        CocosRecorder *cocosRecorder = [CocosRecorder sharedRecorder];
        
        if ( !cocosRecorder.isWriting && !cocosRecorder.isPaused){
            this->addChild([cocosRecorder renderTextureRecorder], -1);
            [cocosRecorder startRecording];
            [cocosRecorder muxAudio:nil withBgm:@"preload_sounds/scene_opening.wav" duration:openingLayer->getTotalDuration() * 1000];
        }
#endif
#endif
    }
}

void PlayAndRecord::startPlay()
{
    playing = true;
    
    playMenuItem->setVisible(false);
    pauseMenuItem->setVisible(true);
    showUI(false);

    if (openingLayer != nullptr) {
        if (openingLayer->isAnimationStarted()) {
            if (openingLayer->isAnimationPaused()) {
                openingLayer->resumeAnimation();
            } else {
                openingLayer->pauseAnimation();
            }
        } else {
            openingLayer->startAnimation();
            openingLayer->playBackgroundMusic();
        }
    }
    if (_type != PlayAndRecordType::PlayOnly) {
        startRecording();
    }
}


void PlayAndRecord::pausePlay()
{
    paused = true;
    if (!openingLayer->isAnimationStarted()) {
        player->pausePlay();
    } else {
        openingLayer->pauseAnimation();
    }
    
    if (endingLayer && endingLayer->isAnimationStarted()) {
        endingLayer->pauseAnimation();
    }
    
    if (_type != PlayAndRecordType::PlayOnly) {
        pauseRecording();
    }
    
    playMenuItem->setVisible(true);
    pauseMenuItem->setVisible(false);
    
    showUI(true);
}

void PlayAndRecord::resumePlay()
{
    paused = false;
    if (!openingLayer->isAnimationStarted()) {
        player->resumePlay();
    } else {
        if (openingLayer->isAnimationPaused()) {
            openingLayer->resumeAnimation();
        }
    }
    
    if (endingLayer && endingLayer->isAnimationStarted()) {
        endingLayer->resumeAnimation();
    }
    
    if (_type != PlayAndRecordType::PlayOnly) {
        resumeRecording();
    }
    
    playMenuItem->setVisible(false);
    pauseMenuItem->setVisible(true);
    showUI(false);
}

void PlayAndRecord::pauseRecording()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	AppDelegate* app = (AppDelegate*)Application::getInstance();
	if(app != nullptr) {
		app->pauseVideoRecord();
	}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
    CocosRecorder *cocosRecorder = [CocosRecorder sharedRecorder];
    if ( cocosRecorder.isWriting ){
        [cocosRecorder pauseRecording];
    }
#endif
#endif
}

void PlayAndRecord::resumeRecording()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	AppDelegate* app = (AppDelegate*)Application::getInstance();
	if(app != nullptr) {
		app->resumeVideoRecord();
	}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#ifndef MACOS
    CocosRecorder *cocosRecorder = [CocosRecorder sharedRecorder];
    [cocosRecorder resumeRecording];
#endif
#endif
}

bool PlayAndRecord::onTouchBegan(cocos2d::Touch* touch, cocos2d::Event  *event)
{
    if (visibleUI) {
        showUI(false);
    } else {
        showUI(true);
        triggerHideUI();
    }
    
    return true;
}

cocos2d::Node * PlayAndRecord::createResizableButton(const std::string& leftFileName, const std::string& centerFileName, const std::string& rightFileName,
                                                          const std::string& iconFileName, const std::string& text, float fontSize, cocos2d::Point iconPos,
                                                          float textLeftPadding, float textRightPadding)
{
    auto leftSprite = Sprite::create(leftFileName);
    leftSprite->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    leftSprite->setPosition(cocos2d::Point(0, 0));
    
    auto rightSprite = Sprite::create(rightFileName);
    rightSprite->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
    
    auto iconSprite = Sprite::create(iconFileName);
    iconSprite->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    iconSprite->setPosition(iconPos);
    
    auto textLabel = LabelExtension::create(text, FONT_NAME_BOLD, fontSize);
    textLabel->setColor(cocos2d::Color3B(255,255,255));
    textLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    auto textWidth = textLabel->getTextWidth();
    
    auto leftPadding = iconPos.x + iconSprite->getContentSize().width + textLeftPadding;
    textLabel->setAnchorPoint(cocos2d::Point::ANCHOR_MIDDLE_LEFT);
    textLabel->setPosition(cocos2d::Point(leftPadding, leftSprite->getContentSize().height/2));
    
    auto buttonTotalWidth = leftPadding + textWidth + textRightPadding;
    auto centerWidth = 0;
    
    if (buttonTotalWidth < (leftSprite->getContentSize().width + rightSprite->getContentSize().width)) {
        buttonTotalWidth = leftSprite->getContentSize().width + rightSprite->getContentSize().width;
    } else {
        centerWidth = buttonTotalWidth - leftSprite->getContentSize().width - rightSprite->getContentSize().width;
    }
    
    auto buttonSize = cocos2d::Size(buttonTotalWidth, leftSprite->getContentSize().height);
    rightSprite->setPosition(cocos2d::Point(buttonSize.width, 0));
    
    auto button = Node::create();
    
    if (centerWidth > 0) {
        auto centerSprite = Sprite::create(centerFileName, cocos2d::Rect(0, 0, centerWidth, leftSprite->getContentSize().height));
        centerSprite->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        centerSprite->setPosition(cocos2d::Point(leftSprite->getContentSize().width, 0));
        button->addChild(centerSprite);
    }
    
    button->setContentSize(buttonSize);
    button->addChild(leftSprite);
    button->addChild(rightSprite);
    button->addChild(iconSprite);
    button->addChild(textLabel);
    
    return button;
}

void PlayAndRecord::onTouchMoved(cocos2d::Touch* touch, cocos2d::Event  *event)
{
}

void PlayAndRecord::onTouchEnded(cocos2d::Touch* touch, cocos2d::Event  *event)
{
}

void PlayAndRecord::onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event  *event)
{
}

void PlayAndRecord::showUI(bool show)
{
    visibleUI = show;
    
    menu->setVisible(show);
    controlMenu->setVisible(show);
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	playProgressBarBg->setVisible(show);
	playProgressBarParent->setVisible(show);
#else
    if (_type == PlayAndRecordType::PlayOnly) {
        playProgressBarHolder->setVisible(show);
    } else {
        playProgressBarBg->setVisible(show);
        playProgressBarParent->setVisible(show);
    }
#endif
    
    if (show) {
        cancelHideUI();
    }
}

void PlayAndRecord::triggerHideUI()
{
    if(hideUIAction == NULL) {
        auto delay = DelayTime::create(UI_HIDE_DELAY);
        hideUIAction = Sequence::create(delay, CallFunc::create(CC_CALLBACK_0(PlayAndRecord::showUI, this, false)), NULL);
        this->runAction(hideUIAction);
    }
}

void PlayAndRecord::cancelHideUI()
{
    if (hideUIAction != NULL){
        this->stopAction(hideUIAction);
        hideUIAction = NULL;
        log("hideUI cancel");
    }
}

void PlayAndRecord::playMenuCallback(Ref* pSender)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Tap.wav", false);

    if (playing){
        if (paused) {
            resumePlay();
        } else {
            pausePlay();
        }
    } else {
        startPlay();
    }
}

void PlayAndRecord::cancelMenuCallback(Ref* pSender)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Tap.wav", false);
    
    Director::getInstance()->pause();
    PopupLayer *modal = PopupLayer::create(POPUP_TYPE_WARNING,
                                           Device::getResString(Res::Strings::CC_JOURNEYMAP_POPUP_WARNING_CANCEL_CONVERT),//"Are you sure\nyou want to cancel?\nCancelled work will not be saved."
                                           Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_CANCEL),//"Cancel",
                                           Device::getResString(Res::Strings::CM_POPUP_BUTTON_TITLE_OK));//"OK");
    modal->setTag(TAG_POPUP_CANCEL);
    modal->setDelegate(this);
    this->addChild(modal, 1000);
}

void PlayAndRecord::createPlayProgressBar()
{
    playProgressBarHolder = Node::create();
    this->addChild(playProgressBarHolder, 15);
    
    // play progress bar bg
    playProgressBarBg = cocos2d::Node::create();
    playProgressBarBg->setContentSize(RECORDING_PROGRESS_BAR_SIZE);
    playProgressBarBg->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarBg->setPosition(RECORDING_PROGRESS_BAR);
    auto playProgressBarBgLeft = Sprite::create("recording_play_progress_bg_left.png");
    playProgressBarBgLeft->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    
    auto playProgressBarBgRight = Sprite::create("recording_play_progress_bg_right.png");
    
    auto playCenterBgWidth = RECORDING_PROGRESS_BAR_SIZE.width - playProgressBarBgLeft->getContentSize().width - playProgressBarBgRight->getContentSize().width;
    auto playProgressBarBgCenter = Sprite::create("recording_play_progress_bg_center.png", cocos2d::Rect(0, 0, playCenterBgWidth, RECORDING_PROGRESS_BAR_SIZE.height));
    playProgressBarBgCenter->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarBgCenter->setPosition(cocos2d::Point(playProgressBarBgLeft->getContentSize().width, 0));
    
    playProgressBarBgRight->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarBgRight->setPosition(cocos2d::Point(playProgressBarBgLeft->getContentSize().width + playCenterBgWidth, 0));
    
    playProgressBarBg->addChild(playProgressBarBgLeft);
    playProgressBarBg->addChild(playProgressBarBgRight);
    playProgressBarBg->addChild(playProgressBarBgCenter);
    //    playProgressBarBg->setVisible(true);
    
    playProgressBarHolder->addChild(playProgressBarBg, 15);
    
    // play progress bar
    playProgressBar = cocos2d::Node::create();
    playProgressBar->setContentSize(RECORDING_PROGRESS_BAR_SIZE);
    playProgressBar->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBar->setPosition(RECORDING_PROGRESS_BAR);
    playProgressBar->setVisible(false);
    auto playProgressBarLeft = Sprite::create("recording_play_progress_left.png");
    playProgressBarLeft->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarLeft->setTag(TAG_PROGRESS_LEFT);
    
    auto playProgressBarRight = Sprite::create("recording_play_progress_right.png");
    playProgressBarRight->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarRight->setPosition(cocos2d::Point(playProgressBarLeft->getContentSize().width, 0));
    playProgressBarRight->setTag(TAG_PROGRESS_RIGHT);
    
    playProgressBar->addChild(playProgressBarLeft);
    playProgressBar->addChild(playProgressBarRight);
    //    playProgressBar->setVisible(true);
    
    playProgressBarHolder->addChild(playProgressBar, 15);
}

void PlayAndRecord::createProgressBar()
{
    // play progress bar bg
    playProgressBarBg = cocos2d::Node::create();
    playProgressBarBg->setContentSize(RECORDING_PROGRESS_BAR_SIZE);
    playProgressBarBg->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarBg->setPosition(RECORDING_PROGRESS_BAR);
    auto playProgressBarBgLeft = Sprite::create("recording_play_progress_bg_left.png");
    playProgressBarBgLeft->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    
    auto playProgressBarBgRight = Sprite::create("recording_play_progress_bg_right.png");
    
    auto playCenterBgWidth = RECORDING_PROGRESS_BAR_SIZE.width - playProgressBarBgLeft->getContentSize().width - playProgressBarBgRight->getContentSize().width;
    auto playProgressBarBgCenter = Sprite::create("recording_play_progress_bg_center.png", cocos2d::Rect(0, 0, playCenterBgWidth, RECORDING_PROGRESS_BAR_SIZE.height));
    playProgressBarBgCenter->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarBgCenter->setPosition(cocos2d::Point(playProgressBarBgLeft->getContentSize().width, 0));
    
    playProgressBarBgRight->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarBgRight->setPosition(cocos2d::Point(playProgressBarBgLeft->getContentSize().width + playCenterBgWidth, 0));
    
    playProgressBarBg->addChild(playProgressBarBgLeft);
    playProgressBarBg->addChild(playProgressBarBgRight);
    playProgressBarBg->addChild(playProgressBarBgCenter);
    playProgressBarBg->setVisible(true);
    
    this->addChild(playProgressBarBg, 15);
    
    playProgressBarParent = Node::create();
    this->addChild(playProgressBarParent, 15);
    
    // play progress bar1
    playProgressBar1 = cocos2d::Node::create();
    playProgressBar1->setTag(TAG_PROGRESS_01);
    playProgressBar1->setContentSize(RECORDING_PROGRESS_BAR_SIZE);
    playProgressBar1->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBar1->setPosition(RECORDING_PROGRESS_BAR);
    auto playProgressBarLeft = Sprite::create("share_progress_left_01.png");
    playProgressBarLeft->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarLeft->setTag(TAG_PROGRESS_LEFT);
    
    auto playProgressBarRight = Sprite::create("share_progress_right_01.png");
    playProgressBarRight->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarRight->setPosition(cocos2d::Point(playProgressBarLeft->getContentSize().width, 0));
    playProgressBarRight->setTag(TAG_PROGRESS_RIGHT);
    
    playProgressBar1->addChild(playProgressBarLeft);
    playProgressBar1->addChild(playProgressBarRight);
    playProgressBar1->setVisible(true);
    
    playProgressBarParent->addChild(playProgressBar1, 0);
    
    // play progress bar2
    playProgressBar2 = cocos2d::Node::create();
    playProgressBar2->setTag(TAG_PROGRESS_02);
    playProgressBar2->setContentSize(RECORDING_PROGRESS_BAR_SIZE);
    playProgressBar2->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBar2->setPosition(RECORDING_PROGRESS_BAR);
    auto playProgressBarLeft2 = Sprite::create("share_progress_left_02.png");
    playProgressBarLeft2->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarLeft2->setTag(TAG_PROGRESS_LEFT);
    
    auto playProgressBarRight2 = Sprite::create("share_progress_right_02.png");
    playProgressBarRight2->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
    playProgressBarRight2->setPosition(cocos2d::Point(playProgressBarLeft2->getContentSize().width, 0));
    playProgressBarRight2->setTag(TAG_PROGRESS_RIGHT);
    
    playProgressBar2->addChild(playProgressBarLeft2);
    playProgressBar2->addChild(playProgressBarRight2);
    playProgressBar2->setVisible(false);
    
    playProgressBarParent->addChild(playProgressBar2, 0);
    
    auto switchDuration = 1.0f;
    auto switchAnimation = Sequence::create(DelayTime::create(switchDuration),
                                            CallFunc::create([&](){
        playProgressBar1->setVisible(!playProgressBar1->isVisible());
        playProgressBar2->setVisible(!playProgressBar2->isVisible());
    }), nullptr);
    
    
    auto repeatAni = RepeatForever::create(switchAnimation);
    this->runAction(repeatAni);
}

void PlayAndRecord::setProgress(float value)
{
    setProgress(playProgressBar1, value);
    setProgress(playProgressBar2, value);
}

void PlayAndRecord::setProgress(Node *progressBar, float value)
{
    if (  1.0f < value || value < 0) {
//        log("out of range! : %f", value);
        return;
    }
    
    Node *targetBar = progressBar;
    std::string centerFileName;
    if (targetBar->getTag() == TAG_PROGRESS_01) {
        centerFileName = "share_progress_center_01.png";
    } else if (targetBar->getTag() == TAG_PROGRESS_02){
        centerFileName= "share_progress_center_02.png";
    } else {
        return;
    }
    
    auto centerSprite = Sprite::create(centerFileName.c_str());
    auto centerSpriteWidth = centerSprite->getContentSize().width;
    
    auto progressBarLeft = targetBar->getChildByTag(TAG_PROGRESS_LEFT);
    auto progressBarRight = targetBar->getChildByTag(TAG_PROGRESS_RIGHT);
    
    auto progressTotalWidth = RECORDING_PROGRESS_BAR_SIZE.width - progressBarLeft->getContentSize().width - progressBarRight->getContentSize().width;
    
    auto targetWidth = std::ceil(progressTotalWidth * value);
    auto targetCenterNum = std::floor(targetWidth / centerSpriteWidth);
    
    auto centerSpriteCount = targetBar->getChildrenCount() - 2;
    
    auto diff = targetCenterNum - centerSpriteCount;
    int increaseCount = 0;
//    log("targetCenterNum : %f, centerSpriteCount : %ld, targetWidth : %f, targetCenterNum : %f, value : %f", targetCenterNum, centerSpriteCount, targetWidth, targetCenterNum, value);
    if (diff < 0) {
        // remove center sprite
        
        // remove all center
        size_t childCount = targetBar->getChildrenCount();
        auto children = targetBar->getChildren();
        for (int index = (int)childCount-1; index >= 0; index--) {
            auto child = children.at(index);
            auto tag = child->getTag();
            if (tag == TAG_PROGRESS_LEFT || tag == TAG_PROGRESS_RIGHT) {
                continue;
            } else {
                child->removeFromParent();
            }
        }
        centerSpriteCount = 0;
        increaseCount = targetCenterNum;
    } else if (diff > 0) {
        // add more center sprite
        
        increaseCount = diff;
    } else {
        // same width
        return;
    }
    
    float startPos = progressBarLeft->getContentSize().width + (centerSpriteCount * centerSpriteWidth);
    float lastPos = startPos + centerSprite->getContentSize().width * diff;
    for (int index = 0; index < increaseCount; index++) {
        auto center = Sprite::create(centerFileName.c_str());
        center->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        center->setPosition(cocos2d::Point(startPos, 0));
        targetBar->addChild(center);
        startPos += center->getContentSize().width;
    }
    
    progressBarRight->setPosition(cocos2d::Point(lastPos, 0));
}

void PlayAndRecord::popupButtonClicked(PopupLayer *popup, cocos2d::Ref *obj)
{
    Director::getInstance()->resume();
    auto clickedButton = (MenuItem*)obj;
    int tag = clickedButton->getTag();
    
    if (popup->getTag() == TAG_POPUP_CANCEL) {
        if (tag == 0) {
            log("cancel");
        } else {
            this->btnBackCallback(nullptr);
        }
    }
}

Label *PlayAndRecord::createTitleLabel()
{
    auto pageTitle = cocos2d::Label::createWithSystemFont("", FONT_NAME_BOLD, MAP_PLAY_TITLE_TEXT_SIZE);
    pageTitle->setDimensions(MAP_PLAY_TITLE_SIZE.width, MAP_PLAY_TITLE_SIZE.height);
    pageTitle->setAlignment(cocos2d::TextHAlignment::CENTER, cocos2d::TextVAlignment::CENTER);
    pageTitle->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
    pageTitle->setColor(cocos2d::Color3B(255, 255, 255));
    
    return pageTitle;
}

__String *PlayAndRecord::getTitle(int chapterIndex)
{
    __String *chapterTitle = NULL;
    
    switch (chapterIndex) {
        case 1:
            chapterTitle = __String::create(Device::getResString(Res::Strings::CC_PLAY_PAGE_TITLE_INTRO));//"Intro"
            break;
        case 2:
        case 3:
        case 4:
            chapterTitle = __String::createWithFormat(Device::getResString(Res::Strings::CC_PLAY_PAGE_TITLE_PAGE_N), chapterIndex);//"Page %d"
            break;
        case 5:
            chapterTitle = __String::create(Device::getResString(Res::Strings::CC_PLAY_PAGE_TITLE_FINAL));//"Final"
            break;
        default:
            break;
    }
    
    return chapterTitle;
}

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
void PlayAndRecord::changeRecordStatus(bool record) {
	bool change = (record ? false : visibleUI);
    menu->setVisible(change);
    controlMenu->setVisible(change);
    playProgressBarBg->setVisible(change);
    playProgressBarParent->setVisible(change);
}
#endif
