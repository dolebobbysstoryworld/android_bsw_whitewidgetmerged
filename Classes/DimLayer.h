//
//  DimLayer.h
//  bobby
//
//  Created by Dongwook, Kim on 14. 4. 14..
//
//

#ifndef __bobby__DimLayer__
#define __bobby__DimLayer__

#include <iostream>
#include "cocos2d.h"

USING_NS_CC;
class DimLayer;
class DimLayerDelegate
{
public:
    virtual ~DimLayerDelegate() {}
    virtual void dimLayerRemoved() = 0;
};

typedef enum dimlayer_type{
    DIM_TYPE_BG = 0,
    DIM_TYPE_CHAR = 1,
} dimlayer_type;

class DimLayer : public LayerColor
{
public:
    DimLayer():_delegate(nullptr){}
    static DimLayer * create(const Color4B& color, GLfloat width, GLfloat height, dimlayer_type layerType);
    static DimLayer * create(const Color4B& color);
    bool init();
    bool initWithColor(const Color4B& color, GLfloat w, GLfloat h);
    bool initWithColor(const Color4B& color, GLfloat w, GLfloat h, dimlayer_type layerType);
    bool initWithColor(const Color4B& color);

    void closeFrameCallback();
    
    virtual bool onTouchBegan(Touch *touch, Event *event);
    virtual void onTouchMoved(Touch *touch, Event *event);
    virtual void onTouchEnded(Touch *touch, Event *event);
    virtual void onTouchCancelled(Touch *touch, Event *event);
    
    DimLayerDelegate *getDelegate() { return _delegate; }
    void setDelegate(DimLayerDelegate *pDelegate) { _delegate = pDelegate; }
    void enableDelete();
    void removeItem();
    void displayItemUnlock(long leftTime);
    
protected:
    DimLayerDelegate *_delegate;
    bool canBeDeleted;
    bool closed;
    dimlayer_type dimLayerType;

};

#endif /* defined(__bobby__DimLayer__) */
