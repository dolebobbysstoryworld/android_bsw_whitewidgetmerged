//
//  DimLayer.cpp
//  bobby
//
//  Created by Dongwook, Kim on 14. 4. 14..
//
//

#include "DimLayer.h"
#include "Character.h"
#include "ResourceManager.h"
#include "Define.h"
#include "SimpleAudioEngine.h"
#include <sys/time.h>
#include "LabelExtension.h"
#include "Util.h"

#define TAG_EXPANDED_ITEM_COIN          700
#define TAG_EXPANDED_ITEM_TALKBOX       800
#define TAG_ITEM_TALKBOX_LABEL 54321

#define TAG_BG_SELECT_COIN      400
DimLayer * DimLayer::create(const Color4B& color, GLfloat width, GLfloat height, dimlayer_type layerType)
{
    DimLayer * layer = new DimLayer();
    if( layer && layer->initWithColor(color,width,height, layerType))
    {
        layer->autorelease();
        return layer;
    }
    CC_SAFE_DELETE(layer);
    return nullptr;
}

DimLayer * DimLayer::create(const Color4B& color)
{
    DimLayer * layer = new DimLayer();
    if(layer && layer->initWithColor(color))
    {
        layer->autorelease();
        return layer;
    }
    CC_SAFE_DELETE(layer);
    return nullptr;
}

bool DimLayer::init()
{
    cocos2d::Size s = Director::getInstance()->getWinSize();
    return initWithColor(Color4B(0,0,0,0), s.width, s.height);
}

bool DimLayer::initWithColor(const Color4B& color)
{
    cocos2d::Size s = Director::getInstance()->getWinSize();
    this->initWithColor(color, s.width, s.height);
    return true;
}

bool DimLayer::initWithColor(const Color4B& color, GLfloat w, GLfloat h)
{
    if (LayerColor::initWithColor( color, w, h) )
    {
        auto listener = EventListenerTouchOneByOne::create();
        listener->setSwallowTouches(true);
        listener->onTouchBegan = CC_CALLBACK_2(DimLayer::onTouchBegan, this);
        listener->onTouchMoved = CC_CALLBACK_2(DimLayer::onTouchMoved, this);
        listener->onTouchEnded = CC_CALLBACK_2(DimLayer::onTouchEnded, this);
        listener->onTouchCancelled = CC_CALLBACK_2(DimLayer::onTouchCancelled, this);
        _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
        _touchListener = listener;
        
        canBeDeleted = false;
        closed = false;
        return true;
    }
    return false;
}

bool DimLayer::initWithColor(const Color4B& color, GLfloat w, GLfloat h, dimlayer_type layerType)
{
    if (LayerColor::initWithColor( color, w, h) )
    {
        dimLayerType = layerType;
        auto listener = EventListenerTouchOneByOne::create();
        listener->setSwallowTouches(true);
        listener->onTouchBegan = CC_CALLBACK_2(DimLayer::onTouchBegan, this);
        listener->onTouchMoved = CC_CALLBACK_2(DimLayer::onTouchMoved, this);
        listener->onTouchEnded = CC_CALLBACK_2(DimLayer::onTouchEnded, this);
        listener->onTouchCancelled = CC_CALLBACK_2(DimLayer::onTouchCancelled, this);
        _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
        _touchListener = listener;
        
        canBeDeleted = false;
        closed = false;
        return true;
    }
    return false;
}

bool DimLayer::onTouchBegan(Touch *touch, Event *event)
{
    log("swallow");
    if (closed) return false;
    
    auto childAtFirst = this->getChildren().at(0);
    
    auto childCount = (int)childAtFirst->getChildrenCount();
    for (int index = childCount-1; index >= 0; index--) { // by z order
        auto child = childAtFirst->getChildren().at(index);
        
        if (!child->isVisible()) continue;
        
        auto characterNode = dynamic_cast<Character *>(child);
        
        if (characterNode) {
            cocos2d::Point touchInNode = characterNode->convertToNodeSpace(touch->getLocation());
            
            auto boundingRect = characterNode->getBoundingBox();
            auto visibleRect = cocos2d::Rect(0, 0, boundingRect.size.width / characterNode->getScale(), boundingRect.size.height / characterNode->getScale());
            bool result = visibleRect.containsPoint(touchInNode); // temp code
            
            if (result) {
                characterNode->onTouchBegan(touch, event);
            }
        }
    }
    
    return true;
}

void DimLayer::onTouchMoved(Touch *touch, Event *event)
{
    
}

void DimLayer::onTouchEnded(Touch *touch, Event *event)
{
    if (closed) return;
    
    if (!canBeDeleted) return;
    
    bool characterClicked = false;
    
    auto childAtFirst = this->getChildren().at(0);
    
    auto childCount = (int)childAtFirst->getChildrenCount();
    for (int index = childCount-1; index >= 0; index--) { // by z order
        auto child = childAtFirst->getChildren().at(index);
        
        if (!child->isVisible()) continue;
        
        auto characterNode = dynamic_cast<Character *>(child);
        
        if (characterNode) {
            cocos2d::Point touchInNode = characterNode->convertToNodeSpace(touch->getLocation());
            
            auto boundingRect = characterNode->getBoundingBox();
            auto visibleRect = cocos2d::Rect(0, 0, boundingRect.size.width / characterNode->getScale(), boundingRect.size.height / characterNode->getScale());
            bool result = visibleRect.containsPoint(touchInNode); // temp code
            
            if (result) {
                characterNode->onTouchEnded(touch, event);
//                std::string animationName = characterNode->getCharName() + "_" + "front_tap";
//                cocosbuilder::CCBAnimationManager *manager = (cocosbuilder::CCBAnimationManager*)characterNode->characterObj->getUserObject();
//                manager->runAnimationsForSequenceNamed(animationName.c_str());
                characterClicked = true;
            }
        }
    }
    
    if (!characterClicked) {
    	removeItem();
        closed = true;
	}
}

void DimLayer::removeItem() {
    auto children = this->getChildren();
    auto item = children.at(0);
    
//    auto childCount = (int)item->getChildrenCount();
//    for (int index = childCount-1; index >= 0; index--) {
//        auto child = item->getChildren().at(index);
//        
//        if (!child->isVisible()) continue;
//        
//        auto characterNode = dynamic_cast<Character *>(child);
//        if (characterNode) {
//            characterNode->pause();
//        }
//    }
    
    auto coin = item->getChildByTag(TAG_EXPANDED_ITEM_COIN);
    if (coin) {
        auto scaleAniFinished = CallFunc::create([=](){
            coin->setVisible(false);
        });
        auto scaleAnimation = ScaleTo::create(0.2f, 0.1f);
        auto animationSequence = Sequence::create(scaleAnimation, scaleAniFinished, NULL);
        coin->runAction(animationSequence);
    }
    
    auto talkBox = item->getChildByTag(TAG_EXPANDED_ITEM_TALKBOX);
    if (talkBox) {
        auto scaleAniFinished = CallFunc::create([=](){
            talkBox->setVisible(false);
            
            auto reverseAction = (Spawn *)item->getUserObject();
            auto afterCallBack = CallFunc::create(CC_CALLBACK_0(DimLayer::closeFrameCallback, this));
            auto scaleBy = ScaleBy::create(0.1f, 1.02f);
            auto reserseChain = Sequence::create(scaleBy, reverseAction, afterCallBack, NULL);
            item->runAction(reserseChain);
        });
        auto scaleAnimation = ScaleTo::create(0.2f, 0.1f);
        auto animationSequence = Sequence::create(scaleAnimation, scaleAniFinished, NULL);
        talkBox->runAction(animationSequence);
    }
    
    if (coin == nullptr && talkBox == nullptr) {
        auto reverseAction = (Spawn *)item->getUserObject();
        auto afterCallBack = CallFunc::create(CC_CALLBACK_0(DimLayer::closeFrameCallback, this));
        auto scaleBy = ScaleBy::create(0.1f, 1.02f);
        auto reserseChain = Sequence::create(scaleBy, reverseAction, afterCallBack, NULL);
        item->runAction(reserseChain);
    }
    
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Smaller.wav", false);
}

void DimLayer::displayItemUnlock(long leftTime)
{
    auto children = this->getChildren();
    auto item = children.at(0);
    
    auto childCount = (int)item->getChildrenCount();
    for (int index = childCount-1; index >= 0; index--) {
        auto child = item->getChildren().at(index);
        
        if (!child->isVisible()) continue;
        
        auto characterNode = dynamic_cast<Character *>(child);
        if (characterNode) {
//            characterNode->pause();
        }
    }
    
    auto coin = item->getChildByTag(TAG_EXPANDED_ITEM_COIN);
    auto talkBox = item->getChildByTag(TAG_EXPANDED_ITEM_TALKBOX);
    LabelExtension* label;
    if (talkBox) {
        label = (LabelExtension*)talkBox->getChildByTag(TAG_ITEM_TALKBOX_LABEL);
    }

    LabelExtension *textLabel;
    if (leftTime > 0) {
        auto day = (leftTime/60/60)/24;
        auto hour = leftTime/60/60;
        auto minute = leftTime/60;
        
        std::string bgfileName;
        std::string leftTimeStr;
        std::string nameLabelStr;
        Color3B numcolor;
        Color3B namecolor;
        if (day >= 1) {
            if (hour == 23 && minute > 30) hour = 24;
            
            std::stringstream daystream;
            daystream << day;
            std::string dayStr = daystream.str();
            leftTimeStr = dayStr;
            
            std::string fileName("home_bg_frame_limited_days.png");
            bgfileName = fileName;
            
            std::string nameStr = Device::getResString(Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_DAYS);//"days";
            nameLabelStr = nameStr;
            
            numcolor = Color3B(0x03, 0x3e, 0xa1);
            namecolor = Color3B(0x2a, 0x64, 0xc5);
        } else {
            if (hour < 30) hour = 1;
            
            std::stringstream hourtream;
            hourtream << hour;
            std::string hourStr = hourtream.str();
            leftTimeStr = hourStr;
            
            std::string fileName("home_bg_frame_limited_hours.png");
            bgfileName = fileName;
            
            std::string nameStr;
            if (hour == 1) {
                nameStr = Device::getResString(Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_HOUR);
            } else {
                nameStr = Device::getResString(Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD_HOURS);//"hours";
            }
            
            nameLabelStr = nameStr;
            
            numcolor = Color3B(0xff, 0x00, 0x00);
            namecolor = Color3B(0xff, 0x24, 0x00);
        }
        
        float timeFontSize = (leftTimeStr.length() > 1)?HOME_DIM_LIMIT_TIME_FONTSIZE_01:HOME_DIM_LIMIT_TIME_FONTSIZE_02;
        
        auto limitBackground = Sprite::create(bgfileName);
        limitBackground->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
        //                        limitBackground->setPosition(cocos2d::Point(characterSize.width+HOME_ITEM_SELECT_COIN_MARGIN.x, HOME_ITEM_SELECT_COIN_MARGIN.y));
        limitBackground->setPosition(cocos2d::Point(item->getContentSize().width+HOME_ITEM_SELECT_COIN_MARGIN.x, HOME_ITEM_SELECT_COIN_MARGIN.y));
        item->addChild(limitBackground, 0, TAG_EXPANDED_ITEM_COIN);
        
        auto timeLabel = Label::createWithSystemFont(leftTimeStr, FONT_NAME_BOLD, timeFontSize,HOME_DIM_LIMIT_TIME_LABLE_SIZE,TextHAlignment::CENTER, TextVAlignment::CENTER);
        timeLabel->setTag(TAG_BG_SELECT_COIN);
        timeLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        timeLabel->setColor(numcolor);
        timeLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
        timeLabel->enableShadow(Color4B(255,255,255,255.0f*0.51f),cocos2d::Size(0,-1), 0);
        timeLabel->setPosition(cocos2d::Point(limitBackground->getContentSize().width/2 - timeLabel->getContentSize().width/2,HOME_DIM_LIMIT_TIME_Y));
        limitBackground->addChild(timeLabel);
        
        auto nameTextLabel = Label::createWithSystemFont(nameLabelStr, FONT_NAME_BOLD,HOME_DIM_LIMIT_NAME_LABEL_FONTSIZE, HOME_DIM_LIMIT_NAME_LABEL_SIZE,TextHAlignment::CENTER,TextVAlignment::CENTER);
        nameTextLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        nameTextLabel->setColor(namecolor);
        nameTextLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
        nameTextLabel->enableShadow(Color4B(255,255,255,255.0f*0.51f),cocos2d::Size(0,-1), 0);
        nameTextLabel->setPosition(cocos2d::Point(limitBackground->getContentSize().width/2 - nameTextLabel->getContentSize().width/2,HOME_DIM_LIMIT_NAME_LABEL_Y));
        limitBackground->addChild(nameTextLabel);
        
        std::string originString = Device::getResString(Res::Strings::CC_EXPANDEDITEM_NOTICE_LIMITED_PERIOD);
        if (Application::getInstance()->getCurrentLanguage() == LanguageType::ENGLISH) {
            originString = __String::createWithFormat(originString.c_str(), day)->getCString();
        }
        
        std::stringstream stringStream(originString);
        std::string line;
        std::vector<std::string> lines;
        while (std::getline(stringStream, line, '\n')) { // split string by '\n'
            lines.push_back(line);
        }
        
        int lineCount = (int)lines.size();
        float maxWidth = 0;
        std::vector<std::string>::iterator itr;
        for(itr = lines.begin(); itr != lines.end(); itr++)
        {
            auto textWidth = Device::getTextWidth(itr->c_str(), FONT_NAME, HOME_TALK_BOX_FONTSIZE);
            maxWidth = MAX(maxWidth, textWidth);
        }
        
        float textLimit = (lineCount > 2) ? HOME_TALK_BOX_MAX_WIDTH_LONG : HOME_TALK_BOX_MAX_WIDTH_SHORT;
        float textWidth = MIN(maxWidth, textLimit);
        
        float textHeight = 0;
#ifdef MACOS
        textHeight = 58;
#endif
        textLabel = LabelExtension::create(originString, FONT_NAME, HOME_TALK_BOX_FONTSIZE, cocos2d::Size(textWidth, textHeight),
                                                TextHAlignment::LEFT, TextVAlignment::BOTTOM);
        float textAreaHeight = textLabel->getContentSize().height;
        
        textLabel->setColor(Color3B(0xff,0xff,0xff));
        textLabel->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
        textLabel->setPosition(cocos2d::Point(HOME_TALK_BOX_SIDE_MARGIN, HOME_TALK_BOX_BOTTOM_MARGIN));
        textLabel->setBlendFunc(BlendFunc::ALPHA_PREMULTIPLIED);
        
        cocos2d::extension::Scale9Sprite *talkBoxChange = nullptr;
        if (dimLayerType == DIM_TYPE_CHAR) {
            cocos2d::Rect talkBoxInset = cocos2d::Rect(HOME_ITEM_TALKBOX_INSET_LEFT_TAIL.x, HOME_ITEM_TALKBOX_INSET_LEFT_TAIL.y,
                                                       HOME_ITEM_TALKBOX_INSET_LEFT_TAIL_SIZE.width, HOME_ITEM_TALKBOX_INSET_LEFT_TAIL_SIZE.height);
            talkBoxChange = cocos2d::extension::Scale9Sprite::create(talkBoxInset, "home_character_lock_talk_box_bg.png");
            
            auto talkBoxSize = cocos2d::Size(textWidth + HOME_TALK_BOX_SIDE_MARGIN * 2, textAreaHeight + HOME_TALK_BOX_BOTTOM_MARGIN + HOME_TALK_BOX_TOP_MARGIN);
            
            talkBoxChange->setContentSize(talkBoxSize);
            talkBoxChange->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_LEFT);
            talkBoxChange->setPosition(cocos2d::Point(item->getContentSize().width+HOME_ITEM_SELECT_COIN_MARGIN.x -(limitBackground->getContentSize().width-HOME_ITEM_TALKBOX.x), limitBackground->getPositionY() + limitBackground->getContentSize().height + HOME_ITEM_TALKBOX.y));
        } else {
            cocos2d::Rect talkBoxInset = cocos2d::Rect(HOME_ITEM_TALKBOX_INSET_RIGHT_TAIL.x, HOME_ITEM_TALKBOX_INSET_RIGHT_TAIL.y,
                                                       HOME_ITEM_TALKBOX_INSET_RIGHT_TAIL_SIZE.width, HOME_ITEM_TALKBOX_INSET_RIGHT_TAIL_SIZE.height);
            talkBoxChange = cocos2d::extension::Scale9Sprite::create(talkBoxInset, "home_talk_box_bg.png");
            
            auto talkBoxSize = cocos2d::Size(textWidth + HOME_TALK_BOX_SIDE_MARGIN * 2, textAreaHeight + HOME_TALK_BOX_BOTTOM_MARGIN + HOME_TALK_BOX_TOP_MARGIN);
            
            talkBoxChange->setContentSize(talkBoxSize);
            talkBoxChange->setAnchorPoint(cocos2d::Point::ANCHOR_BOTTOM_RIGHT);
            talkBoxChange->setPosition(cocos2d::Point(item->getContentSize().width + HOME_TALK_BOX.x, HOME_TALK_BOX.y + HOME_TALK_BOX_PURCHASE_BOTTOM_PADDING));
        }
        talkBoxChange->addChild(textLabel);
        
        item->addChild(talkBoxChange, 0, TAG_EXPANDED_ITEM_TALKBOX);
        
//        if (coin) {
//            coin->removeFromParent();
//        }
//        if (talkBox) {
//            if (label) {
//                label->removeFromParent();
//            }
//            talkBox->addChild(textLabel, 0, TAG_ITEM_TALKBOX_LABEL);
//        }
    }
    
    if (coin) {
        coin->removeFromParent();
    }
    if (talkBox) {
        talkBox->removeFromParent();
    }
}

void DimLayer::onTouchCancelled(Touch *touch, Event *event)
{
    
}

void DimLayer::closeFrameCallback()
{
    if (_delegate != NULL) {
        canBeDeleted = false;
        _delegate->dimLayerRemoved();
    }
    this->removeFromParentAndCleanup(true);
}

void DimLayer::enableDelete()
{
    canBeDeleted = true;
}
